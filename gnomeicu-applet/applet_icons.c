#include <libgnome/gnome-util.h>

#include "applet_icons.h"
#include "control.h"
#include "applet.h"

static gchar *
gnomeicu_applet_icons_get_filename (const gchar *icon_name, const gchar *theme_name);

static void
gnomeicu_applet_icons_setup_icon (const gchar *icon_name, const gchar *theme_name, GtkIconFactory *factory);

static gchar *
gnomeicu_applet_icons_get_filename (const gchar *icon_name, const gchar *theme_name)
{
  char *file;

  if (!theme_name){
    file = g_strconcat (GNOMEICU_APPLET_DATADIR, "/gnomeicu/icons/Default/", icon_name, ".png", NULL);
  }else{
    
    /* Local themes */
    file = g_strconcat (g_get_home_dir (), "/.icq/icons/", theme_name, "/", icon_name, ".png", NULL);
  
    if (!g_file_exists (file)) { /* Local theme does not exist */
      /* Global themes */
      gchar *theme_path = GNOMEICU_APPLET_DATADIR "/gnomeicu/icons/";
      
      g_free (file);
      file = g_strconcat (theme_path, theme_name, "/", icon_name, ".png", NULL);
      
      if (!g_file_exists (file)){ /* Theme does not exist, use default */
	g_free (file);
	file = g_strconcat (theme_path, "Default/", icon_name, ".png", NULL);
	/* Assume that Default exists for now */
      }
    }
  }
  return file;

}

static void
gnomeicu_applet_icons_setup_icon (const gchar *icon_name, const gchar *theme_name, GtkIconFactory *factory)
{
  GtkIconSource *source;
  GtkIconSet *set;
  gchar *file_name;

  file_name = gnomeicu_applet_icons_get_filename (icon_name, theme_name);

  set = gtk_icon_set_new ();
  source = gtk_icon_source_new ();
  gtk_icon_source_set_filename (source, file_name);
  gtk_icon_set_add_source (set, source);
  gtk_icon_factory_add (factory,
			icon_name,
			set);
  gtk_icon_source_free (source);
  gtk_icon_set_unref (set);
  g_free (file_name);
}


void
gnomeicu_applet_icons_setup (GnomeICUApplet *applet)
{
  applet->icons_factory = gtk_icon_factory_new ();
  gnomeicu_applet_icons_update (applet);
  gtk_icon_factory_add_default (applet->icons_factory);
}

void
gnomeicu_applet_icons_update (GnomeICUApplet *applet)
{

  /* Online Icon */
  gnomeicu_applet_icons_setup_icon (GNOMEICU_APPLET_STOCK_ONLINE, applet->theme, applet->icons_factory);
  /* Away Icon */
  gnomeicu_applet_icons_setup_icon (GNOMEICU_APPLET_STOCK_AWAY, applet->theme, applet->icons_factory);
  /* Not Available Icon */
  gnomeicu_applet_icons_setup_icon (GNOMEICU_APPLET_STOCK_NOT_AVAILABLE, applet->theme, applet->icons_factory);
  /* Free For Chat Icon */
  gnomeicu_applet_icons_setup_icon (GNOMEICU_APPLET_STOCK_FREE_FOR_CHAT, applet->theme, applet->icons_factory);
  /* Occupied Icon */
  gnomeicu_applet_icons_setup_icon (GNOMEICU_APPLET_STOCK_OCCUPIED, applet->theme, applet->icons_factory);
  /* Do Not Disturb Icon */
  gnomeicu_applet_icons_setup_icon (GNOMEICU_APPLET_STOCK_DO_NOT_DISTURB, applet->theme, applet->icons_factory);
  /* Invisible Icon */
  gnomeicu_applet_icons_setup_icon (GNOMEICU_APPLET_STOCK_INVISIBLE, applet->theme, applet->icons_factory);
  /* Offline Icon */
  gnomeicu_applet_icons_setup_icon (GNOMEICU_APPLET_STOCK_OFFLINE, applet->theme, applet->icons_factory);
  /* Message Icon */
  gnomeicu_applet_icons_setup_icon (GNOMEICU_APPLET_STOCK_MESSAGE, applet->theme, applet->icons_factory);
  /* Blank Icon */
  gnomeicu_applet_icons_setup_icon (GNOMEICU_APPLET_STOCK_BLANK, applet->theme, applet->icons_factory);

  gnomeicu_applet_icons_setup_icon (GNOMEICU_APPLET_STOCK_NOT_RUNNING, applet->theme, applet->icons_factory);


}
