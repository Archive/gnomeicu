#ifndef __GNOMEICU_APPLET_ICONS_H__
#define __GNOMEICU_APPLET_ICONS_H__

#include <gtk/gtk.h>

#include "applet.h"

#define GNOMEICU_APPLET_STOCK_ONLINE		"gnomeicu-online"
#define GNOMEICU_APPLET_STOCK_AWAY		"gnomeicu-away"
#define GNOMEICU_APPLET_STOCK_NOT_AVAILABLE	"gnomeicu-na"
#define GNOMEICU_APPLET_STOCK_FREE_FOR_CHAT	"gnomeicu-ffc"
#define GNOMEICU_APPLET_STOCK_OCCUPIED		"gnomeicu-occ"
#define GNOMEICU_APPLET_STOCK_DO_NOT_DISTURB	"gnomeicu-dnd"
#define GNOMEICU_APPLET_STOCK_INVISIBLE		"gnomeicu-inv"
#define GNOMEICU_APPLET_STOCK_OFFLINE		"gnomeicu-offline"
#define GNOMEICU_APPLET_STOCK_MESSAGE		"gnomeicu-message"
#define GNOMEICU_APPLET_STOCK_NOT_RUNNING	"gnomeicu-cancel"
#define GNOMEICU_APPLET_STOCK_BLANK             "gnomeicu-blank"

void
gnomeicu_applet_icons_setup (GnomeICUApplet *applet);

void
gnomeicu_applet_icons_update (GnomeICUApplet *applet);

#endif /* __GNOMEICU_APPLET_ICONS_H__ */
