#include <config.h>
#include <string.h>

#include <gtk/gtk.h>
#include <gnome.h>
#include <gconf/gconf-client.h>

#include "applet.h"
#include "applet_icons.h"
#include "control.h"

static gboolean gnomeicu_applet_clicked (GtkWidget	*event_box,
					 GdkEventButton	*event,
					 gpointer	data);

static GType gnomeicu_applet_get_type (void);

gboolean gnomeicu_applet_factory (GnomeICUApplet	*applet,
				  const gchar	*iid,
				  gpointer	data);
static gboolean gnomeicu_applet_build (GnomeICUApplet *applet);

static void gnomeicu_applet_set_label (GnomeICUApplet *applet);

static void gnomeicu_applet_set_icon (GnomeICUApplet *applet);

static void gnomeicu_applet_change_size (PanelApplet    *applet,
				gint            size,
				GnomeICUApplet *icuapplet);
static void gnomeicu_applet_change_orient (PanelApplet       *applet,
					   PanelAppletOrient  orient,
					   GnomeICUApplet    *icuapplet);
static void gnomeicu_applet_boxit (GnomeICUApplet    *applet,
				   PanelAppletOrient  orient,
				   gint               size);
static const gchar *gnomeicu_applet_get_status_str(GnomeICUApplet *applet);

void theme_changed (GConfClient *client, guint cnxn_id, GConfEntry *entry,
		    gpointer data);

/* Sets the applet's type */
GType
gnomeicu_applet_get_type (void)
{
  static GType type = 0;

  if (!type) {
    static const GTypeInfo info = {
      sizeof (PanelAppletClass),
      NULL, NULL, NULL, NULL, NULL,
      sizeof (GnomeICUApplet),
      0, NULL, NULL
    };

    type = g_type_register_static (
				   PANEL_TYPE_APPLET, "GnomeICUApplet",
				   &info, 0);
  }

  return type;
} /* gnomeicu_applet_get_type */


/* Applet creation */
gboolean
gnomeicu_applet_factory (GnomeICUApplet	*applet,
			 const gchar	*iid,
			 gpointer	data)
{
  gboolean retval = FALSE;
  GConfClient* client = gconf_client_get_default();



  applet->about = NULL;
  applet->theme = gconf_client_get_string(client, 
					  "/apps/gnomeicu/general/themes/icons", 
					  NULL);
  applet->status = applet->usercount = 0;
  applet->msgcount = applet->showmsg = 0;

  gconf_client_add_dir(client,
		       "/apps/gnomeicu",
		       GCONF_CLIENT_PRELOAD_NONE,
		       NULL);

  gconf_client_notify_add(client,
			  "/apps/gnomeicu/general/themes/icons",
                          theme_changed,
                          applet,
                          NULL, NULL);

  if (!strcmp (iid, "OAFIID:GNOME_GnomeICUApplet"))
    retval = gnomeicu_applet_build (applet);

  return retval;
} /* gnomeicu_applet_factory */


void theme_changed (GConfClient *client, guint cnxn_id, GConfEntry *entry,
		    gpointer data)
{
  GnomeICUApplet *applet = data;

  applet->theme = gconf_value_get_string(entry->value);

  gnomeicu_applet_icons_update(applet);


}

/* Macro for applet setup and main function */
PANEL_APPLET_BONOBO_FACTORY ("OAFIID:GNOME_GnomeICUApplet_Factory",
			     gnomeicu_applet_get_type (),
			     PACKAGE,
			     VERSION,
			     (PanelAppletFactoryCallback) gnomeicu_applet_factory,
			     NULL)


/* Handler for mouse clicks */
gboolean
gnomeicu_applet_clicked (GtkWidget	*event_box,
			 GdkEventButton	*event,
			 gpointer	data)
{

  GnomeICUApplet *applet;
  applet = data;

  /* Handle left mouse button */
  if (event->button == 1 && event->type == GDK_2BUTTON_PRESS) {
    if (applet->status == GNOMEICU_APPLET_STATUS_NOT_RUNNING)
      gnomeicu_applet_control_set_status(GNOMEICU_APPLET_STATUS_ONLINE);
    else
      if (applet->msgcount > 0) {
	if (--applet->msgcount == 0) 
	  applet->showmsg = FALSE;
	gnomeicu_applet_control_pop_event();
      }
      else
	gnomeicu_applet_control_toggle_window ();
    return TRUE;
  }

  /* Let other buttons through */
  return FALSE;
} /* gnomeicu_applet_clicked */


/* Menu handler */
void
gnomeicu_applet_menu (BonoboUIComponent *uic,
		      gpointer           data,
		      const gchar       *verbname)
{
  GdkPixbuf *pixbuf = NULL;
  gchar *file;
  GnomeICUApplet *applet;

  applet = data;

  if (!strcmp (verbname, "GnomeICUAppletOnline")){
    gnomeicu_applet_control_set_status (GNOMEICU_APPLET_STATUS_ONLINE);
  }else if (!strcmp (verbname, "GnomeICUAppletAway")){
    gnomeicu_applet_control_set_status (GNOMEICU_APPLET_STATUS_AWAY);
  } else if (!strcmp (verbname, "GnomeICUAppletNotAvailable")){
    gnomeicu_applet_control_set_status (GNOMEICU_APPLET_STATUS_NOT_AVAILABLE);
  } else if (!strcmp (verbname, "GnomeICUAppletFreeForChat")){
    gnomeicu_applet_control_set_status (GNOMEICU_APPLET_STATUS_FREE_FOR_CHAT);
  } else if (!strcmp (verbname, "GnomeICUAppletOccupied")){
    gnomeicu_applet_control_set_status (GNOMEICU_APPLET_STATUS_OCCUPIED);
  } else if (!strcmp (verbname, "GnomeICUAppletDoNotDisturb")){
    gnomeicu_applet_control_set_status (GNOMEICU_APPLET_STATUS_DO_NOT_DISTURB);
  } else if (!strcmp (verbname, "GnomeICUAppletInvisible")){
    gnomeicu_applet_control_set_status (GNOMEICU_APPLET_STATUS_INVISIBLE);
  } else if (!strcmp (verbname, "GnomeICUAppletOffline")){
    gnomeicu_applet_control_set_status (GNOMEICU_APPLET_STATUS_OFFLINE);
  } else {

    
    const gchar *authors[] =
      {
	"Daniel Romberg <daniel@k1q.net>",
	"Olivier Crete <tester@tester.ca>",
	"Thanks to GnomeICU Author Jeremy Wise <jwise@pathwaynet.com>",
	NULL
      };
    
    const gchar *documenters[] =
      {
	NULL
      };
    
    /* Translator credits */
    gchar *translator_credits = _("translator_credits");
    
    if (applet->about && GTK_IS_WINDOW(applet->about))
      {
	gtk_window_present (GTK_WINDOW(applet->about));
	return;
      }
    
    file = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_PIXMAP,
				      "gnomeicu.png", TRUE, NULL);
    if (file != NULL)
      {
	pixbuf = gdk_pixbuf_new_from_file (file, NULL);
	g_free (file);
      }

    applet->about = gnome_about_new ("GnomeICU Applet", VERSION,
			     "Copyright \xc2\xa9 2002-2003 Daniel Romberg",
			     _("GnomeICU Applet is designed to be used with " 
			       "GnomeICU: a small, fast and functional "
			       "ICQ/AIM instant messaging program, specifically "
			       "designed for GNOME."),
			     authors,
			     documenters,
			     ((strcmp (translator_credits, "translator_credits") != 0) ? translator_credits : NULL),
			     pixbuf);
    
    gtk_widget_show (applet->about);
    
    g_object_add_weak_pointer (G_OBJECT (applet->about),
			       (void**)&applet->about);

  }
} /* gnomeicu_applet_menu */


/* Set the applet label */
void
gnomeicu_applet_set_label (GnomeICUApplet *applet)
{
  gchar *labeltext, *tooltiptext;

  if (applet->status == GNOMEICU_APPLET_STATUS_NOT_RUNNING) {
    tooltiptext = g_strdup(gnomeicu_applet_get_status_str(applet));
    labeltext = g_strdup("-");
  }
  else if (applet->msgcount) {
    labeltext = g_strdup_printf ("%d", applet->msgcount);
    tooltiptext = g_strdup_printf(_("%s\n%d users online\n%d new messages"), 
				  gnomeicu_applet_get_status_str(applet),
				  applet->usercount, applet->msgcount);
  } else {
    labeltext = g_strdup_printf ("%d", applet->usercount);
    tooltiptext = g_strdup_printf(_("%s\n%d users online"), 
				  gnomeicu_applet_get_status_str(applet),
				  applet->usercount);
  }
  
  gtk_label_set_text (GTK_LABEL (applet->label), labeltext);
  gtk_tooltips_set_tip(applet->tooltips, GTK_WIDGET(applet),
		       tooltiptext, NULL);

  g_free (labeltext);
  g_free (tooltiptext);

} /* gnomeicu_applet_set_label */


/* Set the applet icon */
void
gnomeicu_applet_set_icon (GnomeICUApplet *applet)
{
  if (applet->msgcount && applet->showmsg) 
    gtk_image_set_from_stock (GTK_IMAGE (applet->icon), GNOMEICU_APPLET_STOCK_MESSAGE, GTK_ICON_SIZE_MENU);
  else if (applet->msgcount)
    gtk_image_set_from_stock (GTK_IMAGE (applet->icon), GNOMEICU_APPLET_STOCK_BLANK, GTK_ICON_SIZE_MENU);
  else
    switch (applet->status){
    case GNOMEICU_APPLET_STATUS_ONLINE:
      gtk_image_set_from_stock (GTK_IMAGE (applet->icon), GNOMEICU_APPLET_STOCK_ONLINE, GTK_ICON_SIZE_MENU);
      break;
    case GNOMEICU_APPLET_STATUS_AWAY:
      gtk_image_set_from_stock (GTK_IMAGE (applet->icon), GNOMEICU_APPLET_STOCK_AWAY, GTK_ICON_SIZE_MENU);
      break;
    case GNOMEICU_APPLET_STATUS_NOT_AVAILABLE:
      gtk_image_set_from_stock (GTK_IMAGE (applet->icon), GNOMEICU_APPLET_STOCK_NOT_AVAILABLE, GTK_ICON_SIZE_MENU);
      break;
    case GNOMEICU_APPLET_STATUS_FREE_FOR_CHAT:
      gtk_image_set_from_stock (GTK_IMAGE (applet->icon), GNOMEICU_APPLET_STOCK_FREE_FOR_CHAT, GTK_ICON_SIZE_MENU);
      break;
    case GNOMEICU_APPLET_STATUS_OCCUPIED:
      gtk_image_set_from_stock (GTK_IMAGE (applet->icon), GNOMEICU_APPLET_STOCK_OCCUPIED, GTK_ICON_SIZE_MENU);
      break;
    case GNOMEICU_APPLET_STATUS_DO_NOT_DISTURB:
      gtk_image_set_from_stock (GTK_IMAGE (applet->icon), GNOMEICU_APPLET_STOCK_DO_NOT_DISTURB, GTK_ICON_SIZE_MENU);
      break;
    case GNOMEICU_APPLET_STATUS_INVISIBLE:
      gtk_image_set_from_stock (GTK_IMAGE (applet->icon), GNOMEICU_APPLET_STOCK_INVISIBLE, GTK_ICON_SIZE_MENU);
      break;
    case GNOMEICU_APPLET_STATUS_OFFLINE:
      gtk_image_set_from_stock (GTK_IMAGE (applet->icon), GNOMEICU_APPLET_STOCK_OFFLINE, GTK_ICON_SIZE_MENU);
      break;
    case GNOMEICU_APPLET_STATUS_NOT_RUNNING:
      gtk_image_set_from_stock (GTK_IMAGE (applet->icon), GNOMEICU_APPLET_STOCK_NOT_RUNNING, GTK_ICON_SIZE_MENU);
      break;
    }
} /* gnomeicu_applet_set_icon */


/* Check the status and usercount  and change icon and label if something has changed */
void
gnomeicu_applet_update_status (GnomeICUApplet *applet)
{

  if (applet->showmsg)
    applet->showmsg = FALSE;
  else if (applet->msgcount)
    applet->showmsg = TRUE;

  gnomeicu_applet_icons_update(applet);

  gnomeicu_applet_set_label (applet);
  gnomeicu_applet_set_icon (applet);

} /* gnomeicu_applet_update_status */


/* Build the applet */
gboolean
gnomeicu_applet_build (GnomeICUApplet *applet)
{

  gnomeicu_applet_icons_setup (applet);

  panel_applet_set_flags(PANEL_APPLET(applet),
			 panel_applet_get_flags(PANEL_APPLET(applet)) | 
			 PANEL_APPLET_EXPAND_MINOR);

  applet->icon = gtk_image_new_from_stock (GNOMEICU_APPLET_STOCK_OFFLINE, GTK_ICON_SIZE_MENU);

  applet->label = gtk_label_new ("0");

  applet->box = gtk_hbox_new (TRUE, 2);
  gtk_container_add (GTK_CONTAINER (applet), applet->box);

  gtk_box_pack_start (GTK_BOX (applet->box), applet->icon, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (applet->box), applet->label, TRUE, TRUE, 0);
  
  gnomeicu_applet_boxit(applet, panel_applet_get_orient(PANEL_APPLET(applet)),
			panel_applet_get_size(PANEL_APPLET(applet)));


  gtk_widget_show_all (GTK_WIDGET (applet));
  panel_applet_setup_menu_from_file (PANEL_APPLET (applet),
				     GNOMEICU_APPLET_UIDIR,
				     "GNOME_GnomeICUApplet.xml",
				     PACKAGE,
				     gnomeicu_applet_menu_verbs,
				     applet);

  applet->tooltips = gtk_tooltips_new();

  gnomeicu_applet_control_ask (applet);

  /* Handle mouse clicks */
  g_signal_connect (G_OBJECT (applet),
		    "button_press_event",
		    G_CALLBACK (gnomeicu_applet_clicked),
		    applet);
		    
  g_signal_connect (G_OBJECT (applet),
		    "change_orient",
		    G_CALLBACK (gnomeicu_applet_change_orient),
		    applet);
  
  g_signal_connect (G_OBJECT (applet),
		    "change_size",
		    G_CALLBACK (gnomeicu_applet_change_size),
		    applet);


  /* Add event to periodically check status and update the applet */
  gtk_timeout_add (500,
		   (GtkFunction) gnomeicu_applet_control_ask,
		   applet);

  return TRUE;
} /* gnomeicu_applet_build */


static void gnomeicu_applet_change_size (PanelApplet    *applet,
				gint            size,
				GnomeICUApplet *icuapplet)
{
  PanelAppletOrient orient = panel_applet_get_orient(applet);
  gnomeicu_applet_boxit(icuapplet, orient, size);
}

static void gnomeicu_applet_change_orient (PanelApplet       *applet,
					   PanelAppletOrient  orient,
					   GnomeICUApplet    *icuapplet)
{
  gint size = panel_applet_get_size(applet);
  gnomeicu_applet_boxit(icuapplet, orient, size);
}

static void gnomeicu_applet_boxit(GnomeICUApplet    *applet,
				  PanelAppletOrient  orient,
				  gint               size)
{
  GtkWidget *newbox;

  switch (orient) {
  case PANEL_APPLET_ORIENT_UP:
  case PANEL_APPLET_ORIENT_DOWN:
    if (size < 40)
      newbox = gtk_hbox_new (TRUE, 2);
    else 
      newbox = gtk_vbox_new (TRUE, 2);

    break;
  case PANEL_APPLET_ORIENT_LEFT:
  case PANEL_APPLET_ORIENT_RIGHT:
    if (size < 36) 
      newbox = gtk_vbox_new (TRUE, 1);
    else
      newbox = gtk_hbox_new (TRUE, 1);
  
      break;
  }
  gtk_widget_reparent(applet->icon, newbox);
  gtk_widget_reparent(applet->label, newbox);
  gtk_widget_destroy(applet->box);
  applet->box = newbox;
  gtk_container_add (GTK_CONTAINER (applet), applet->box);
  
  gtk_widget_show_all(GTK_WIDGET(applet));
}

static const gchar *gnomeicu_applet_get_status_str (GnomeICUApplet *applet)
{
  switch( applet->status ) {
  case GNOMEICU_APPLET_STATUS_OFFLINE:
    return _("Offline");
  case GNOMEICU_APPLET_STATUS_ONLINE:
    return _("Online");
  case GNOMEICU_APPLET_STATUS_AWAY:
    return _("Away");
  case GNOMEICU_APPLET_STATUS_NOT_AVAILABLE:
    return _("Not Available");
  case GNOMEICU_APPLET_STATUS_FREE_FOR_CHAT:
    return _("Free For Chat");
  case GNOMEICU_APPLET_STATUS_OCCUPIED:
    return _("Occupied");
  case GNOMEICU_APPLET_STATUS_DO_NOT_DISTURB:
    return _("Do Not Disturb");
  case GNOMEICU_APPLET_STATUS_INVISIBLE:
    return _("Invisible");
  case GNOMEICU_APPLET_STATUS_NOT_RUNNING:
    return _("GnomeICU not running");
  }
  return NULL;
}
