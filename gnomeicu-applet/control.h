#ifndef __GNOMEICU_APPLET_CONTROL_H__
#define __GNOMEICU_APPLET_CONTROL_H__

#include <glib.h>

#include "applet.h"

void
gnomeicu_applet_control_ask (gpointer data); 
/* data must be a GnomeICUApplet* */

void
gnomeicu_applet_control_set_status (GnomeICUAppletStatus status);

void
gnomeicu_applet_control_toggle_window (void);

void
gnomeicu_applet_control_pop_event (void);

#endif /* __GNOMEICU_APPLET_CONTROL_H__ */
