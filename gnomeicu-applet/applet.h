#ifndef __GNOMEICU_APPLET_H__
#define __GNOMEICU_APPLET_H__

#include <config.h>

#include <panel-applet.h>
#include <libbonoboui.h>

typedef enum {
  GNOMEICU_APPLET_STATUS_NOT_RUNNING,
  GNOMEICU_APPLET_STATUS_ONLINE,
  GNOMEICU_APPLET_STATUS_AWAY,
  GNOMEICU_APPLET_STATUS_NOT_AVAILABLE,
  GNOMEICU_APPLET_STATUS_FREE_FOR_CHAT,
  GNOMEICU_APPLET_STATUS_OCCUPIED,
  GNOMEICU_APPLET_STATUS_DO_NOT_DISTURB,
  GNOMEICU_APPLET_STATUS_INVISIBLE,
  GNOMEICU_APPLET_STATUS_OFFLINE
} GnomeICUAppletStatus;

typedef struct {
  PanelApplet	 base;
  GtkWidget	*label;
  GtkWidget	*icon;
  GtkWidget	*box;
  GtkWidget     *about;
  GnomeICUAppletStatus status;
  gint		 usercount;
  gint		 msgcount;
  const gchar   *theme;
  GtkIconFactory *icons_factory;
  gboolean       showmsg;
  gint           errors;
  GtkTooltips   *tooltips;
} GnomeICUApplet;


void gnomeicu_applet_update_status (GnomeICUApplet *applet);


void gnomeicu_applet_menu (BonoboUIComponent *uic,
			   gpointer           user_data,
			   const gchar       *verbname);

/* Menu callbacks */
static const BonoboUIVerb gnomeicu_applet_menu_verbs [] = {
        BONOBO_UI_VERB ("GnomeICUAppletOnline", gnomeicu_applet_menu),
        BONOBO_UI_VERB ("GnomeICUAppletAway", gnomeicu_applet_menu),
        BONOBO_UI_VERB ("GnomeICUAppletNotAvailable", gnomeicu_applet_menu),
        BONOBO_UI_VERB ("GnomeICUAppletFreeForChat", gnomeicu_applet_menu),
        BONOBO_UI_VERB ("GnomeICUAppletOccupied", gnomeicu_applet_menu),
        BONOBO_UI_VERB ("GnomeICUAppletDoNotDisturb", gnomeicu_applet_menu),
        BONOBO_UI_VERB ("GnomeICUAppletInvisible", gnomeicu_applet_menu),
        BONOBO_UI_VERB ("GnomeICUAppletOffline", gnomeicu_applet_menu),
        BONOBO_UI_VERB ("GnomeICUAppletToggleWindow", gnomeicu_applet_menu),
        BONOBO_UI_VERB ("GnomeICUAppletAbout", gnomeicu_applet_menu),

        BONOBO_UI_VERB_END
};

#endif /* __GNOMEICU_APPLET_H__ */
