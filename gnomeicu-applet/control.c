#include <glib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pwd.h>
#include <netinet/in.h>

#include "control.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/* Internal functions */
static gint
gnomeicu_applet_control_connect ();

static void
gnomeicu_applet_control_send_command (gint socket, const gchar *command);

gboolean
gnomeicu_applet_control_read (GIOChannel *source,
			      GIOCondition condition,
			      gpointer data);

gboolean    
gnomeicu_applet_control_timedout (gpointer data);

gint
gnomeicu_applet_control_connect ()
{
  gint socket_fd;
  struct sockaddr_un	addr;
  struct passwd		*pw;

  pw = getpwuid (getuid ());
  if (!pw) {
    fprintf (stderr, "Unable to get current login name, naming service down?\n"); 
    return (-1);
  }


  sprintf (addr.sun_path, "/tmp/.gnomeicu-%s/ctl", pw->pw_name);
  addr.sun_family = AF_UNIX;

  if(access(addr.sun_path, F_OK) != 0)
    return -1;
  
  socket_fd = socket(AF_UNIX, SOCK_STREAM, 0);

  if(socket_fd >= 0) {
    if(connect(socket_fd, (struct sockaddr *)&addr,
	       sizeof(addr.sun_family) + strlen(addr.sun_path) + 1) == 0){
      return socket_fd;
    }else{
      close(socket_fd);
      return -1;
    }
  }else{
    perror ("socket");
  }

  return -1;
}


void
gnomeicu_applet_control_set_status (GnomeICUAppletStatus status)
{
  gint socket_fd;
  gchar *argv[] = {"gnomeicu", "-s", NULL, NULL};

  if ((socket_fd = gnomeicu_applet_control_connect())!=-1){
    switch (status){
    case GNOMEICU_APPLET_STATUS_ONLINE:
      gnomeicu_applet_control_send_command (socket_fd, "status online");
      break;
    case GNOMEICU_APPLET_STATUS_AWAY:
      gnomeicu_applet_control_send_command (socket_fd, "status away");
      break;
    case GNOMEICU_APPLET_STATUS_NOT_AVAILABLE:
      gnomeicu_applet_control_send_command (socket_fd, "status na");
      break;
    case GNOMEICU_APPLET_STATUS_FREE_FOR_CHAT:
      gnomeicu_applet_control_send_command (socket_fd, "status freechat");
      break;
    case GNOMEICU_APPLET_STATUS_OCCUPIED:
      gnomeicu_applet_control_send_command (socket_fd, "status occupied");
      break;
    case GNOMEICU_APPLET_STATUS_DO_NOT_DISTURB:
      gnomeicu_applet_control_send_command (socket_fd, "status dnd");
      break;
    case GNOMEICU_APPLET_STATUS_INVISIBLE:
      gnomeicu_applet_control_send_command (socket_fd, "status invisible");
      break;
    case GNOMEICU_APPLET_STATUS_OFFLINE:
      gnomeicu_applet_control_send_command (socket_fd, "status offline"); 
      break;
    }
    close (socket_fd);
  }   
  else
    if (status != GNOMEICU_APPLET_STATUS_OFFLINE){
      switch (status) {
      case GNOMEICU_APPLET_STATUS_ONLINE:
	argv[2] = "online";
	break;
      case GNOMEICU_APPLET_STATUS_AWAY:
	argv[2] = "away";
	break;
      case GNOMEICU_APPLET_STATUS_NOT_AVAILABLE:
	argv[2] = "na";
	break;
      case GNOMEICU_APPLET_STATUS_FREE_FOR_CHAT:
	argv[2] = "freechat";
	break;
      case GNOMEICU_APPLET_STATUS_OCCUPIED:
	argv[2] = "occupied";
	break;
      case GNOMEICU_APPLET_STATUS_DO_NOT_DISTURB:
	argv[2] = "dnd";
	break;
      case GNOMEICU_APPLET_STATUS_INVISIBLE:
	argv[2] = "invisible";
	break;
      case GNOMEICU_APPLET_STATUS_OFFLINE:
	argv[2] = "offline";
      }
      g_spawn_async(NULL, argv, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, NULL, 
		    NULL);
      }
}


void
gnomeicu_applet_control_toggle_window (void)
{
  gint socket_fd;

  if ((socket_fd = gnomeicu_applet_control_connect())!=-1){

    gnomeicu_applet_control_send_command (socket_fd, "showhide");

    close (socket_fd);
  }
}


void
gnomeicu_applet_control_pop_event (void)
{
  gint socket_fd;

  if ((socket_fd = gnomeicu_applet_control_connect())!=-1){

    gnomeicu_applet_control_send_command (socket_fd, "readevent");

    close (socket_fd);
  }
}


void
gnomeicu_applet_control_ask (gpointer data)
{
  GnomeICUApplet *applet = data;
  gint socket_fd;
  GIOChannel *channel;
  gint timeout;

  if ((socket_fd = gnomeicu_applet_control_connect())!=-1){

    gnomeicu_applet_control_send_command (socket_fd, "icuask");

    channel = g_io_channel_unix_new (socket_fd);
    timeout = g_timeout_add(3000, gnomeicu_applet_control_timedout, channel);
    g_io_add_watch(channel, G_IO_IN|G_IO_PRI, gnomeicu_applet_control_read,
		   applet);
    applet->errors = 0;
  }  else {
    if (applet->errors) {
      applet->status = GNOMEICU_APPLET_STATUS_NOT_RUNNING;
      applet->msgcount = applet->usercount = 0;
      gnomeicu_applet_update_status (applet);   
      applet->errors = 0;
    } else
      applet->errors++;
  }
}


gboolean
gnomeicu_applet_control_read (GIOChannel *source,
			      GIOCondition condition,
			      gpointer data)
{
  GnomeICUApplet *applet = data;
  gint socket_fd = g_io_channel_unix_get_fd(source);
  gchar *buffer, **temporary;
  gulong u;
  gchar c = 1;

  
    
  read (socket_fd, &u, sizeof (gulong));
  u = ntohl (u);
  if (u < 16384 && u){
    buffer = g_malloc0 (u + 1);
    read (socket_fd, buffer, u);
  } else {
    if (applet->errors) {
      applet->status = GNOMEICU_APPLET_STATUS_NOT_RUNNING;
      applet->msgcount = applet->usercount = 0;
      gnomeicu_applet_update_status (applet);   
      applet->errors = 0;
    } else
      applet->errors++;
    /*     g_io_channel_shutdown(source, TRUE, NULL);
	   Will be shut down by timer */
    return FALSE;
  }
  
  /*
  g_io_channel_shutdown(source, TRUE, NULL);
  Will be shut down by the timer too
  */

  if (!buffer)
    return FALSE;

  temporary = g_strsplit (buffer, " ", 4);

  g_free(buffer);

  if (!temporary)
    return FALSE;

  if (!strcmp (temporary[0], "Online")){
    applet->status = GNOMEICU_APPLET_STATUS_ONLINE;
  } else if (!strcmp (temporary[0], "Away")){
    applet->status = GNOMEICU_APPLET_STATUS_AWAY;
  } else if (!strcmp (temporary[0], "NotAvailable")){
    applet->status = GNOMEICU_APPLET_STATUS_NOT_AVAILABLE;
  } else if (!strcmp (temporary[0], "FreeForChat")){
    applet->status = GNOMEICU_APPLET_STATUS_FREE_FOR_CHAT;
  } else if (!strcmp (temporary[0], "Occupied")){
    applet->status = GNOMEICU_APPLET_STATUS_OCCUPIED;
  } else if (!strcmp (temporary[0], "DoNotDisturb")){
    applet->status = GNOMEICU_APPLET_STATUS_DO_NOT_DISTURB;
  } else if (!strcmp (temporary[0], "Invisible")){
    applet->status = GNOMEICU_APPLET_STATUS_INVISIBLE;
  } else if (!strcmp (temporary[0], "Offline")){
    applet->status = GNOMEICU_APPLET_STATUS_OFFLINE;
  } else {
      applet->status = GNOMEICU_APPLET_STATUS_NOT_RUNNING;
  }

  applet->msgcount = atoi (temporary[1]);
  
  applet->usercount = atoi (temporary[2]);
  
  g_strfreev (temporary);
  
  gnomeicu_applet_update_status (applet);

  return FALSE;
}


void
gnomeicu_applet_control_send_command (gint socket_fd, const gchar *command)
{
  gulong	u;
  gchar		c = 1;

  if (!socket_fd){
    return;
  }

  if (!strcmp (command, "getstatus")){
    c = 4;
  }else if (!strcmp (command, "icuask")){
    c = 9;
  }

  u = htonl (strlen (command)+1);

  write (socket_fd, &c, 1);
  write (socket_fd, &u, sizeof (gulong));
  write (socket_fd, command, strlen (command)+1);
}

gboolean    
gnomeicu_applet_control_timedout (gpointer data)
{
  GIOChannel *channel = data;
  g_io_channel_shutdown(channel, TRUE, NULL);
  g_io_channel_unref(channel);

  return FALSE;
}
