<!DOCTYPE Article PUBLIC "-//GNOME//DTD DocBook PNG Variant V1.1//EN"[
 <!ENTITY version "0.94">
 <!ENTITY manual-version "0.2">
]>

<!-- please do not change the id -->

<article id="index"> <!-- please do not change the id -->

 <artheader>
  <title>GnomeICU</title>
  <copyright><year>2000</year> <holder>Thomas Canty, Ilya Konstantinov</holder></copyright>

  <revhistory><revision><revnumber>0.2</revnumber> <date>2000 urria 07</date> <authorinitials>ik</authorinitials> <revremark>Garapen berrietara egokitzeko estilo-zuzenketak, iradokizunak eta aldaketak</revremark></revision></revhistory>
    
  <!-- translators: uncomment this:

  <copyright>
   <year>2000</year>
   <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
  </copyright>

   -->

  <!-- do not put authorname in the header except in copyright - use
  section "authors" below -->

  <legalnotice><para>Baimena ematen da dokumentu hau kopiatu, banatu edota aldatzeko Free Software Foundation-ek argitaratutako <citetitle>GNU Dokumentazio Librearen Lizentzia</citetitle>ren 1.1 bertsioan edo berriago batean ezarritako baldintzak betetzen badira; Atal Aldaezinik, Aurreko azaleko testurik eta Atzeko azaleko testurik gabe. <citetitle>GNU Dokumentazio Librearen Lizentzia</citetitle>ren kopia Free Software Foundation-en bidez ere lor dezakezu <ulink type="http"     url="http://www.fsf.org">haren Web gunea</ulink> bisitatuz edo hona idatziz: Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.</para> <para>Enpresek euren produktu eta zerbitzuak bereizteko erabiltzen dituzten izen asko marka erregistratu moduan hartu behar dira. Izen horiek GNOMEren edozein agiritan agertzen direnean, eta GNOMEren dokumentazio-proiektuko kideak marka erregistratuak direla konturatu badira, izen horiek osorik maiuskulaz edo hasierako letra maiuskulaz idatzi dituzte.</para></legalnotice>

  <releaseinfo>GnomeICUren eskuliburuaren $eskuliburu-bertsioa bertsioa da.</releaseinfo>

 </artheader>

 <!-- ============= Document Body ============================= -->

 <!-- ============= Introduction ============================== --> <sect1 id="intro">
  <title>Sarrera</title>

  <para><application>GnomeICU</application> <ulink url="http://www.gnome.org">GNOME</ulink>ren aplikazioa da, AOLen <ulink url="http://www.icq.com">ICQ&reg; berehalako mezularitza-zerbitzuarekin</ulink> edo ICQren beste zerbitzari batekin konektatzeko. <application>GnomeICU</application> ICQ Inc.-ren jatorrizko <application>ICQ</application> aplikazioaren alternatiba da eta horrekiko bateragarritasuna lortu nahi du. Une honetan, ICQ v5 protokoloan oinarriturik dago (Matthew Smith-en zehazpenei jarraituz). Protokolo hori <application>ICQ99b</application> aplikazioaren baliokidea da.</para>
  
  <para>ICQren berehalako mezularitza-zerbitzua erabiltzaile guztiek erabil dezakezue. Mundu guztiko pertsonekin jar zaitezkete harremanetan haiek berehala jasotzen dituzten mezuen bitartez, linean badaude (&quot;orrialdekatzaile&quot; gailu tradizionalaren antzekoa da).

   <application>GnomeICUk</application> zure lagunak linean daudela jakinarazi eta haiekin komunikatzea ahalbidetzen dizu, testu-mezuen bidez, berriketen bidez eta fitxategiak bidaliz.</para>

  <para><application>GnomeICU</application> exekutatzeko, hautatu <guimenuitem>GnomeICU</guimenuitem> <guimenu>Menu nagusiko</guimenu> <guisubmenu>Internet</guisubmenu> azpimenuan, edo idatzi <command>gnomeicu</command> komando-lerroan.  <application>GnomeICU</application> paneleko applet-ik gabe hasteko, idatzi <command>gnomeicu -a</command> komando-lerroan.</para>

  <para>Dokumentu honek <application>GnomeICUren</application> &amp;bertsioa bertsioa deskribatzen du.</para>

 </sect1>


 <!-- ================ Usage ================================ --> <!-- This section should describe basic usage of the application. -->

 <sect1 id="starting">
  <title>Abiarazten</title>
  <para>Atal honetan <application>GnomeICU</application> erabiltzeko egin behar dituzun lehen urratsak zehazten dira.</para>

  <para>ICQ erabiltzaile bakoitzak berarentzat bakarrik den Interneteko Zenbaki Unibertsala edo UINa dauka. Erabiltzailea identifikatzeko erabiltzen da, bestelako informazioaz gain, esaterako goitizena eta helbide elektronikoa. ICQren beste erabiltzaileekin komunikatzeko, erregistratu eta ICQren UINa ere jaso behar duzu.</para>
  
  <para><note><para>Atal hau AOLen ICQ zerbitzari ofizialari buruz arituko da.  ICQren zerbitzari autonomo bat erabili nahi baduzu, adibidez, zure enpresak emandakoa nazioarteko komunikazioetarako, prozedura desberdina izan daiteke.</para></note>
     
   ICQren zerbitzua sinatzeko, ireki <guimenu>ICQ</guimenu> eta hautatu <guisubmenu>(EGITEKOAK)</guisubmenu>.
   
   EGITEKOAK</para>

  <para>Orain erabil ditzakezu ICQko eginbide guztiak. Nola erabili jakiteko, irakurri hurrengo atala.</para></sect1>
  
 <sect1 id="basics">
  <title>GnomeICUren oinarriak</title>

  <para><application>GnomeICU</application> hastean <interface>Leiho nagusia</interface> irekiko da, hemen erakusten den bezala: <xref
   linkend="mainwindow-fig">.

   <figure id="mainwindow-fig">
    <title>GnomeICUren Leiho nagusia</title>
     <screenshot><screeninfo>GnomeICUren Leiho nagusia</screeninfo> <graphic fileref="images/mainwindow.png" format="png"
      srccredit="Ilya Konstantinov"></graphic></screenshot></figure></para>
   
  <sect2 id="icq-basics">
   <title>Glosarioa</title>
   
   <para><itemizedlist>

     <listitem><para><emphasis>Kontaktu-zerrenda</emphasis> &mdash; <interface>Leiho nagusian</interface>, pertsona batzuen (edo&quot;kontaktuen&quot;) zerrenda ikusiko duzu, &quot;Kontaktu-zerrenda&quot; deitua. Normalean, ezagutzen dituzun pertsonak dira eta haiekin harremanetan segitu nahi duzu ('buddy zerrenda'  ere deitua, antzeko berehalako mezularitzako aplikazioetan).</para></listitem>

     <listitem><para><emphasis>Egoera</emphasis> &mdash; Erabiltzaile bat ICQra konektaturik ez badago, bere egoera <emphasis>Lineaz kanpo</emphasis> izango da. Erabiltzaile bat ICQra konektaturik dagoenean, bere egoera hauetako bat izango da: <emphasis>Linean</emphasis> (erabiltzailearekin kontakta  dezakezula esan nahi du), <emphasis>Berriketarako libre</emphasis>, <emphasis>Kanpoan</emphasis> (une honetan erabiltzailea ordenagailuan ez dagoela esan nahi du), <emphasis>E/E</emphasis> (denbora luzean erabiltzailea ordenagailuan ez dela egongo esan nahi du ), <emphasis>Okupatuta</emphasis>, <emphasis>Ez molestatu</emphasis> eta <emphasis>Ikusezina</emphasis>.</para>
      
      <para>Izen bakoitzaren aldamenean dauden ikonoek pertsonaren egoera erakusten dute. Kontaktu-zerrendak fitxa hauek ditu: <interface>Linean</interface>, lineako kontaktuak bakarrik dituena, <interface>Lineaz kanpo</interface>, lineaz kanpoko kontaktuak bakarrik dituena, eta <interface>Ez dago zerrendan</interface>, mezuak bidaltzen dizkizuten, baina zure zerrendan ez dauden erabiltzaileak dituena.</para></listitem>

     <listitem><para><emphasis>Erabiltzaile ikusezina</emphasis> &mdash; Egoera Ikusezina ezarriz, besteek ez dute jakingo linean zaudela, gertaera bat bidaltzen diezun arte (adib, mezu bat, berriketarako eskaera, etab.). Erabiltzaile bat Ikusezina egoeran ikusten baduzu, beste erabiltzailea ere Ikusezina moduan dagoela eta zurekin kontaktatu duela edo bere Ikusgai dagoen zerrendan zaudela esan nahi du.</para></listitem>

    </itemizedlist></para></sect2>
  
  <sect2>
   <title>ICQren eragiketak egiten </title>
   <para>ICQren eragiketa gehienak saguaren eskuineko botoiaz kontaktuan klik eginez eta laster-menutik elementu bat hartuz egin daitezke, hemen erakusten den bezala: <xref linkend="popup-menu-fig">.

    <figure id="popup-menu-fig">
     <title>Kontaktuaren laster-menua </title>
     <screenshot><screeninfo>Kontaktuaren laster-menua</screeninfo> <graphic fileref="images/popup-menu.png" format="png"
       srccredit="Ilya Konstantinov"></graphic></screenshot></figure>
    
    <tip>
     <title>ICQren eragiketak teklatuarekin bakarrik egiten</title>
     <para>ICQren eragiketa gehienak teklatuarekin egin ditzakezu kontaktu bat zerrendatik hautatuz, <keycap>Gora</keycap> eta <keycap>Behera</keycap> botoiekin, eta orduan, <keycombo><keycap>Alt</keycap><keycap>Zuriune-barra</keycap></keycombo> sakatuz laster-menua irekitzeko, eta menu-elementu bat hautatuz <keycap>Gora</keycap> eta <keycap>Behera</keycap> teklekin.</para></tip></para>

   <para><itemizedlist><listitem><para>Zerrendari kontaktu bat gehitzeko, ikus <xref linkend="contact">.</para></listitem>

     <listitem><para>Mezu bat ICQren bidez bidaltzeko, ikus <xref linkend="message"></para></listitem>

     <listitem><para>Fitxategi bat ICQren bidez bidaltzeko, ikus <xref linkend="file">.</para></listitem>

     <listitem><para>URL bat ICQren bidez bidaltzeko, ikus <xref linkend="URL">.</para></listitem>

     <listitem><para>Norbaitekin berriketan ICQren bidez egiteko, ikus <xref linkend="chat">.</para></listitem></itemizedlist></para></sect2>

  <sect2 id="panel-applet">
   <title>Paneleko applet-a</title>
   <para><application>GnomeICUk</application> paneleko applet-a dauka, hemen erakusten den bezala: <xref linkend="panel-fig">. Paneleko applet honek zure uneko egoera eta zure kontaktu-zerrendan une honetan linean dagoen erabiltzaile-kopurua erakusten du.  Paneleko applet honetan klik egiten baduzu, leiho nagusia ezkutatuta badago, bistaratu egingo da eta ikusgai badago ezkutatu egingo da.</para>

   <figure id="panel-fig">
    <title>GnomeICUren paneleko applet-a</title>
    <screenshot><screeninfo>GnomeICUren paneleko applet-a</screeninfo> <graphic fileref="images/panel.png" format="png"
       srccredit="Ilya Konstantinov"></graphic></screenshot></figure>

   <para>Mezu bat jasotzen denean, paneleko applet-aren ikonoa bere egoera arrunteko ikonotik, jasotako mezu-mota adierazten duen ikonora aldatzen da (ikus <xref linkend="panel-mesg-fig">). Bere ondoko zenbakiak zenbat mezu dauden irakurri gabe erakusten du.</para>
 
   <figure id="panel-mesg-fig">
    <title>GnomeICUren paneleko applet-a - Jasotako mezua</title>
    <screenshot><screeninfo>GnomeICUren paneleko applet-a - Jasotako mezua</screeninfo> <graphic fileref="images/panel-message.png" format="png"
       srccredit="Ilya Konstantinov"></graphic></screenshot></figure>
   
   <tip>
    <title>Applet-aren argibideak </title>
    <para>Saiatu saguaren kurtsorea <application>GnomeICU</application>ren paneleko applet-aren gainean pare bat segundotan mugitzen, egoerari buruzko informazio bizkorra jasotzeko (leihorik ireki gabe edo bertan klik egin gabe). Saguaren kurtsorea applet-etik kentzen duzun bezain laster, &quot;argibidea&quot; desagertu egingo da. Modu arruntean, kontaktu-zerrendan une horretan linean dauden pertsonen zerrenda ikusiko duzu.</para></tip></sect2>


  <!-- This section should also describe main elements of graphic user
       interface, such as menus and buttons, unless this description
       is too long - in that case, it should be moved to a separate
       section. For example -->


  <!-- ========= Menus =========================== --> <sect2 id="menubar">
   <title>Menuak</title>
   <para> <interface>Leiho nagusiaren</interface> goiko aldean dagoen menu-barrak ondorengo menuak ditu:</para>

   <variablelist><varlistentry><term><guimenu>ICQ</guimenu></term> <listitem><para>Menu honen edukia: <itemizedlist><listitem><para><guimenuitem>Gehitu kontaktua</guimenuitem> &mdash; komando honek <interface><link linkend="contact">Gehitu kontaktua</link></interface> elkarrizketa-koadroan erakusten du. Bertan erabiltzaileak kontaktu-zerrendari kontaktu bat gehi diezaioke.</para></listitem> <listitem><para><guimenuitem>Konexioaren historia</guimenuitem> &mdash; komando honek <interface>Konexioaren historia</interface> elkarrizketa-koadroa bistaratzen du, eta ICQ saioan gertatzen diren sareko transakzioen zerrenda erakusten du. Ez da Mezuaren historia arrunta eta erabiltzaile arruntarentzat erabilgarria ez den informazioa dauka.</para></listitem> <listitem><para><guimenuitem>Hobespenak...</guimenuitem> &mdash; komando honek <interface><link linkend="preferences">Hobespenak</link></interface> elkarrizketa-koadroa bistaratzen du. Bertan <application>GnomeICU</application> konfigura dezakezu.</para></listitem> <listitem><para><guimenuitem>Aldatu erabiltzaileari buruzko informazioa</guimenuitem> &mdash; komando honek <interface><link linkend="userinfo">Erabiltzaileari buruzko informazioa</link></interface> elkarrizketa-koadroa bistaratzen du. Bertan, ICQ erabiltzailearen direktorioko informazio pertsonala aldatu edo gehi dezakezu.</para></listitem> <listitem><para><guimenuitem>Ezkutatu leiho nagusia</guimenuitem> &mdash; komando honek leiho nagusia ezkutatzen du. Paneleko applet-ean klik eginez, leiho nagusia berriro bistaratuko da.</para></listitem> <listitem><para><guimenuitem>Irten</guimenuitem> &mdash; komando hau programatik irteteko da eta <application>GnomeICUren</application> applet-a kentzen du GNOMEren paneletik.</para></listitem></itemizedlist></para></listitem></varlistentry>

    <varlistentry><term><guimenu>Zerrendak</guimenu></term> <listitem><para>Menu honen edukia: <itemizedlist><listitem><para><guimenuitem>Ezikusi zerrenda</guimenuitem> &mdash; komando honek Ezikusi zerrenda elkarrizketa-koadroa erakusten du, eta erabiltzaileei Ezikusi zerrendari kontaktuak gehitzen uzten die.</para></listitem> <listitem><para><guimenuitem>Ikusgai dagoen zerrenda</guimenuitem> &mdash; komando honek Ikusgai dagoen zerrenda elkarrizketa-koadroa erakusten du, eta erabiltzaileei Ikusgai dagoen zerrendari kontaktuak gehitzen uzten die.</para></listitem> <listitem><para><guimenuitem>Zerrenda ikusezina</guimenuitem> &mdash; komando honek zerrenda ikusezina erakusten du, eta erabiltzaileei zerrenda ikusezinean kontaktuak gehitzen uzten die.</para></listitem> <listitem><para><guimenuitem>Lineako jakinarazpenaren zerrenda</guimenuitem> &mdash; komando honek Lineako jakinarazpenaren zerrenda erakusten du, eta erabiltzaileei Lineako jakinarazpenaren zerrendari kontaktuak gehitzen uzten die.</para></listitem></itemizedlist></para></listitem></varlistentry>

    <varlistentry><term><guimenu>Laguntza</guimenu></term> <listitem><para>Menu honen edukia: <itemizedlist><listitem><para><guimenuitem>GnomeICUren dokumentazioa</guimenuitem> &mdash; eskuliburu hau erakusten du.</para></listitem>

        <listitem><para><guimenuitem>Honi buruz</guimenuitem> &mdash; <application>GnomeICUri</application> buruzko informazio orokorra erakusten du, esaterako egilearen izena eta aplikazioaren bertsio-zenbakia.</para></listitem></itemizedlist>

      </para></listitem></varlistentry></variablelist></sect2></sect1>

 <sect1 id="operations">
  <title>ICQren eragiketak</title>
  <sect2 id="message">
   <title>Mezua bidaltzen</title>
 
   <para>ICQren bidez mezu bat bidaltzeko, bi aldiz klik egin kontaktuan edo eskuineko botoiarekin egin klik eta hautatu <guimenuitem>Mezua...</guimenuitem>. <xref linkend="message-fig"> atalean erakusten den <interface>Bidali mezua elkarrizketa-koadroa</interface> agertuko da. Idatzi mezu bat eta egin klik <guibutton>Bidali</guibutton> botoian mezua bidaltzeko.

    <tip><para> <keycombo><keycap>Ktrl</keycap><keycap>Sartu</keycap></keycombo> saka dezakezu mezua lehenbailehen bidaltzeko. Laster-tekla horrek Bidali abiarazten du <application>GnomeICUren</application> elkarrizketa-koadro guztietan.</para></tip>

   </para>

   <figure id="message-fig">
    <title>Mezua elkarrizketa-koadroa</title>
    <screenshot><screeninfo>Mezua elkarrizketa-koadroa</screeninfo> <graphic fileref="images/message.png" format="png"       srccredit="Ilya Konstantinov"></graphic></screenshot></figure></sect2>
   
  <sect2 id="file">
   <title>Fitxategia bidaltzen</title>

   <para>Fitxategi bat ICQren bidez bidaltzeko, eskuineko botoiarekin klik egin kontaktuan eta hautatu <guimenuitem>Fitxategi-eskaera...</guimenuitem>. <xref linkend="file-fig"> atalean erakusten den <interface>Bidali fitxategia elkarrizketa-koadroa</interface> agertuko da. <guibutton>Gehitu fitxategiak...</guibutton> zerrendara bidali nahi duzun fitxategi bakoitza gehitzeko. Fitxategiak zerrendatik kentzeko, hautatu bidali nahi ez dituzun fitxategiak eta egin klik <guibutton>Kendu fitxategiak</guibutton> botoian. Bukatzeko, beharrezkoa bada, sartu fitxategiak bidaltzeko arrazoia. Egin klik <guibutton>Bidali</guibutton> botoian fitxategiak bidaltzeko. Fitxategiak zerrendan dauden ordenan bidaliko dira, eta <interface>Fitxategi-transferentzia</interface> elkarrizketa-koadroak transferentziaren progresioa bistaratuko du.</para>
 
   <figure id="file-fig">
    <title>Bidali fitxategia elkarrizketa-koadroa</title>
    <screenshot><screeninfo>Bidali fitxategia elkarrizketa-koadroa</screeninfo> <graphic fileref="images/file.png" format="png"       srccredit="Ilya Konstantinov"></graphic></screenshot></figure>


  </sect2>

  <sect2 id="URL">
   <title>URLa bidaltzen</title>

   <para>Funtzionaltasun hau erabil dezakezu <acronym>URLa</acronym> (Universal Resource Locator - web guneko helbidearentzat beste izen bat) beste erabiltzaileei bidaltzeko. URLak bidaltzeko modurik gogokoena da, jasotzen duen erabiltzaileak klik behin eginez ikus baitezake URLa (ICQren aplikazio gehienetan).</para>

   <para>Fitxategia ICQren bidez bidaltzeko, eskuineko botoiarekin klik egin kontaktuan eta hautatu <guimenuitem>Fitxategiaren eskaera...</guimenuitem>. <xref linkend="url-fig"> atalean erakusten den <interface>Biladi mezua elkarrizketa-koadroa</interface> agertuko da. Idatzi edo itsatsi URLa, eta beharrezkoa bada, gehitu deskribapena. Egin klik <guibutton>Bidali</guibutton> botoian URLa bidaltzeko.
     
     <tip>
        <title>Esteka arrastatzen</title>
        <para>Web arakatzaileko esteka <application>GnomeICUko</application> kontaktura arrasta dezakezu, <interface>Bidali URLa elkarrizketa-koadroa</interface> idatzitako estekaren URLarekin irekitzeko.</para></tip></para>
 
   <figure id="url-fig">
    <title>Bidali URLa elkarrizketa-koadroa</title>
    <screenshot><screeninfo>Bidali URLa elkarrizketa-koadroa</screeninfo> <graphic fileref="images/URL.png" format="png"       srccredit="Ilya Konstantinov"></graphic></screenshot></figure></sect2>

  <sect2 id="chat">
   <title>Berriketan</title>
   <para>ICQren zerrendako norbaitekin berriketan egin nahi baduzu, eskuineko botoiarekin klik egin kontaktuan eta hautatu <guimenuitem>Berriketarako eskaera</guimenuitem>. <xref linkend="chatrequest-fig"> atalean erakusten den <interface>Berriketarako eskaera elkarrizketa-koadroa</interface> agertuko da.</para>

   <figure id="chatrequest-fig">
    <title>Berriketarako eskaera elkarrizketa-koadroa</title>
    <screenshot><screeninfo>Berriketarako eskaera elkarrizketa-koadroa</screeninfo> <graphic fileref="images/chatrequest.png" format="png"        srccredit="Ilya Konstantinov"></graphic></screenshot></figure>

   <para>Idatzi berriketarako arrazoi bat eta sakatu <guibutton>Bidali</guibutton> botoia berriketarako eskaera bidaltzeko. Beste erabiltzaileak berriketa onartzen badu, <xref linkend="chat-fig"> atalean erakusten den <interface>Berriketa elkarrizketa-koadroa</interface> agertuko da. Bestela, berriketarako eskaera zergatik ez duten onartu azalduko da.</para>

   <figure id="chat-fig">
    <title>Berriketa elkarrizketa-koadroa</title>
    <screenshot><screeninfo>Berriketa elkarrizketa-koadroa</screeninfo> <graphic fileref="images/chat.png" format="png"       srccredit="Ilya Konstantinov"></graphic></screenshot></figure></sect2>
  
 </sect1>

 <sect1 id="contact">
  <title>Kontaktua gehitzen</title>
  <para>Kontaktu-zerrendari kontaktu bat gehitzeko, hautatu <guimenu>ICQ</guimenu> menuko <guimenuitem>Gehitu kontaktua</guimenuitem>. <xref    linkend="contact-fig"> atalean erakusten den <interface>Gehitu kontaktua</interface> druid-a agertuko da.

  <figure id="contact-fig">
   <title>Gehitu kontaktuaren druid-a</title>
   <screenshot><screeninfo>Gehitu kontaktuaren druid-a</screeninfo> <graphic fileref="images/addcontact.png" format="png"
     srccredit="Ilya Konstantinov"></graphic></screenshot></figure>

  <para>Kontaktu bat gehitzeko, sartu erabiltzaileari buruzko informazioa gutxienez eremu batean (adib. UINa). Informazio hori sartu ondoren, egin klik <guibutton>Hurrengoa</guibutton> botoian. Zure irizpidearekin bat datozen kontaktu guztien zerrenda erakutsiko zaizu. Zerrendari gehitu nahi diozun kontaktua hautatu. Egin klik <guibutton>Hurrengoa</guibutton> botoian kontaktua zerrendari gehitzeko. 
    
    <tip>
     <title>Bilaketa murrizten</title>
     <para>ICQ erabiltzailea kokatzeko modurik onena bere UINa erabiltzea da. UINa esklusiboa da eta bilaketa-emaitza bakarra bidaliko du. Bestela, <guilabel>Kontaktuari buruzko informazioa</guilabel> fitxa erabili eta ahalik eta xehetasun gehien eman ditzakezu bilaketa murrizteko (segur aski izen arruntentzat emaitza ugari jasoko dituzu).</para></tip></para></sect1>


 <!-- ============= Customization ============================= --> <sect1 id="preferences">
  <title>GnomeICU pertsonalizatzen</title>

  <para> <interface>Hobespenak elkarrizketa-koadroan</interface> sartzeko, hautatu <guimenuitem>Hobespenak...</guimenuitem>  <guimenu>ICQ</guimenu> menuan. <xref    linkend="preferences-fig"> atalean erakusten den <interface>Hobespenak elkarrizketa-koadroa</interface> agertuko zaizu.

  <figure id="preferences-fig">
   <title>Hobespenak elkarrizketa-koadroa</title>
   <screenshot><screeninfo>Hobespenak elkarrizketa-koadroa</screeninfo> <graphic fileref="images/preferences.png" format="png"
     srccredit="Ilya Konstantinov"></graphic></screenshot></figure>

  <para> <guilabel>Interfazea</guilabel> fitxak propietate hauek ditu: <itemizedlist>

    <listitem><para><guilabel>Koloreak</guilabel> Aukera honekin hainbat egoeratan dauden erabiltzaileen kontaktu-zerrendan erabilitako koloreak alda ditzakezu.</para></listitem>
    
    <listitem><para><guilabel>Gaitu soinuak</guilabel> &mdash; Aukera honen bidez, soinuak gai eta desgai ditzakezu. <note>
       <title>Soinuak pertsonalizatzen</title>
       <para>Soinu lehenetsiak aldatzeko eta hainbat gertaeratarako soinuak konfiguratzeko, erabili <application>GNOMEren kontrol-zentroaren</application> <interface><guilabel>Multimedia</guilabel> -&gt; <guilabel>Soinua</guilabel></interface> applet-a.</para></note></para></listitem>
    
    <listitem><para><guilabel>Gaitu soinuak linean zaudenean bakarrik</guilabel> &mdash; Aukera honen bidez, soinuen jakinarazpena Linean egoeran ez zaudenean (adib. Okupatuta egoeran) desgai dezakezu, ez molestatzeko edo mezuak jaso ezin dituzunean.</para></listitem>
    
    <listitem><para><guilabel>Gaitu 'bip' egitea</guilabel> &mdash; Aukera honen bidez, <application>GnomeICUk</application> ordenagailuan inkorporatuta dagoen bozgorailutik 'bip' laburrak sortzen ditu.   Hori erabilgarria izan daiteke soinu-gailu errealik ez duten ordenagailuetan.</para></listitem>

    <listitem><para><guilabel>Erakutsi leiho nagusia abiaraztean</guilabel> &mdash; Aukera honen bidez, leiho nagusia bistaratzen da <application>GnomeICU</application> hastean.</para></listitem>

    <listitem><para><guilabel>Erakutsi automatikoki Erantzun elkarrizketa-koadro osoa</guilabel> &mdash; Mezu berri bat jasotzen duzunean, sarrerako mezuaren leihoan erantzunaren testu-koadroa zuzenean azalduko da, <guibutton>Erantzun</guibutton> botoian klik egin gabe.</para></listitem>

    <listitem><para><guilabel>Erakutsi automatikoki mezuen leihoak</guilabel> &mdash; Aukera honen bidez, sarrerako mezuen leihoak agertuko dira eta mezua jasotzen den unean fokatzen dute. Bestela, paneleko applet-eko edo kontaktu-zerrendako ikono keinukarian klik egiten duzunean bakarrik agertuko da mezuen leihoa.</para></listitem>

    <listitem><para><guilabel>Lineako jakinarazpenak Kanpotik eta E/Etik itzultzea barneratzen du</guilabel> &mdash; Aukera honen bidez, <application>GnomeICUk</application> <interface>Jakinarazpen-zerrendako</interface> kontaktuak Kanpoan edo E/E egoeretatik itzuli direla jakinaraziko dizu (kontaktua Linean egoeratik Lineaz kanpo aldatzen denean jakinarazteaz gain).</para></listitem>
    
    <listitem><para><guilabel>Ezkutatu argibideak</guilabel> &mdash; Aukera honek jaso dituzun argibideak ezkutatzen ditu, saguaren kurtsorea paneleko applet-aren gainean pare bat segundotan mugituz.</para></listitem></itemizedlist></para>

   <para> <guilabel>Sarea</guilabel> fitxaren propietateak hauek dira: <itemizedlist><listitem><para><guilabel>Errusierako Win1251&lt;-&gt;Koi8-r itzulpena</guilabel> &mdash;  Aukera honen bidez, Microsoft Windows plataformako ICQ erabiltzaileekin hizkuntza zirilikoetan komunika zaitezke.  Horren bidez, <application>GnomeICUk</application> sarrerako mezuak Win1251 kodeketatik ( 'cp1251' bezala ere ezagutua <application>Microsoft Windows-eko</application> kodeketa estandarrean) Koi8-R kodeketara (UNIXeko kodeketa ezaguna) itzultzen ditu, eta alderantziz irteerako mezuekin.
      
      <warning>
       <title>Eginbide hori ez erabili, beste hizkuntza batzuetan komunikatzean. </title>
       <para>Eginbide hori beste hizkuntzetako kodeketekin nahas daiteke eta sarrerako eta irteerako mezuetan hizkiak okerreko lekuan jar ditzake. Zirilikorako (Errusierako) bakarrik erabili.</para></warning>

     </para></listitem>
    
    <listitem><para><guilabel>Kanji EUC&lt;-&gt;SJIS itzulpena</guilabel> &mdash; Aukera honen bidez, <application>GnomeICUk</application> kodifikazio japoniarrak itzultzen ditu.</para></listitem>
    
    <listitem><para><guilabel>Irauli UDP paketeak</guilabel> &mdash; Aukera honek sarrerako eta irteerako UDP pakete guztien edukia irteera estandarrera (stdout) iraultzen du. Aukera hau garatzaileei zuzendua dago eta ez da erabilgarria erabiltzaile arruntentzat.</para></listitem>

    <listitem><para><guilabel>Irauli TCP paketeak</guilabel> &mdash; Aukera honek sarrerako eta irteerako TCP pakete guztien edukia irteera estandarrera (stdout) iraultzen du. Aukera hau garatzaileei zuzendua dago eta ez da erabilgarria erabiltzaile arruntentzat.</para></listitem>

    <listitem><para><guilabel>Zerbitzaria</guilabel> &mdash; Aukera honen bidez, <application>GnomeICUk</application> ICQren zer zerbitzari erabiltzea nahi duzun konfigura dezakezu.

      <warning><para> <guilabel>Zerbitzaria</guilabel>ren eta <guilabel>Ataka</guilabel>ren ezarpenetarako, lehenetsitakoak zuzena izan behar luke erabiltzaile gehienentzat. Ezarpen horiek aldatzen badituzu, <application>GnomeICU</application> ez da ICQren zerbitzariarekin konektatzeko gai izango.</para></warning>
   
     </para></listitem>

    <listitem><para><guilabel>TCP atakaren barrutia</guilabel> &mdash; Aukera honen bidez,  <application>GnomeICUk</application> kanpoko konexioetarako entzutea nahi duzun TCP atakaren barrutia zehatz dezakezu. Hori beharrezkoa da peer-to-peer mezuetarako (&quot;zerbitzariaren bitartez bidaltzen&quot; ez diren berehalako mezuak), berriketetarako eta fitxategi-transferentzietarako. Lehenespen horrek egokia izan behar luke erabiltzaile gehienentzat.</para></listitem>

   </itemizedlist></para>

  <para><guilabel>Kanpoko programak</guilabel> fitxaren propietateak hauek dira: <variablelist>

    <varlistentry><term><guilabel>Exekutatu programa gertaeran</guilabel></term> <listitem><para>Aukera honen bidez, edozein gertaera jasotakoan, esaterako mezu bat jasotzea, kanpoko programa bat exekuta dezakezu.</para></listitem></varlistentry>

   </variablelist></para>

  <para><guilabel>Kanpoan mezuak</guilabel> fitxak propietate hauek ditu: <itemizedlist>

    <listitem><para><guilabel>Kanpoan mezua 1..10</guilabel> &mdash; Aukera honen bidez, &quot;Kanpoan mezuak gero erabiltzeko utz ditzakezu. 'Linean' egoera 'Kanpoan', 'Ez dago erabilgarri', 'Okupatuta' edo 'Ez molestatu' egoeretako batera aldatzen baduzu, <application>GnomeICUk</application> ohar txiki bat eskatuko dizu, zuri mezuak bidaltzen saiatzen ari diren erabiltzaileei bistaratzeko. Orduan, mezu berri bat sartu edo aurrez definitutako 10 mezuetako bat erabil dezakezu.</para></listitem>
    
    <listitem><para><guilabel>Auto kanpoaren denbora-muga</guilabel>...  &mdash; Aukera honen bidez, <application>GnomeICUk</application> egoera <guilabel>Kanpoan</guilabel> egoerara aldatu aurretik zenbat minutu egon behar duen inaktibo (sarrera-gailuak erabili gabe, adib., sagua edo teklatua) defini dezakezu.</para></listitem>
    
    <listitem><para><guilabel>Auto E/Eren denbora-muga</guilabel>... &mdash; Aurreko aukeraren antzeko da, baina egoera <guilabel>Ez dago erabilgarri (E/E)</guilabel> egoerara aldatzen du (eta beste erabiltzaileei ez zarela kafe bat hartzera bakarrik joan eta hurrengo orduetan ez zarela egongo jakinarazten die).</para></listitem>

   </itemizedlist></para>

  <para><guilabel>Askotarikoak</guilabel> fitxak propietate hauek ditu: <itemizedlist>

    <listitem><para><guilabel>Baimendu besteei ni web guneetan ikustea</guilabel> &mdash; Aukera honek beste pertsonei zuri buruzko datuak ICQren web gunean ikusteko baimena ematen die.</para></listitem>

    <listitem><para><guilabel>Ezkutatu automatikoki leiho nagusia</guilabel>... &mdash; Aukera honen bidez, leiho nagusia segundo batzuk iragan ondoren ezkuta dezakezu.</para></listitem>

    <listitem><para><guilabel>Erabiltzaileari buruzko informazioa</guilabel> Atal honetan erabiltzaileari buruzko oinarrizko informazioa alda dezakezu.</para></listitem>

   </itemizedlist></para>

  <para>Klik egin <guibutton>Aplikatu</guibutton> botoian aldaketak elkarrizketa-koadroa itxi gabe aplikatzeko. Klik egin <guibutton>Ados</guibutton> botoian aldaketak aplikatu eta elkarrizketa-koadroa ixteko. Klik egin <guibutton>Utzi</guibutton> botoian elkarrizketa-koadroa egin dituzun aldaketak aplikatu gabe ixteko.</para></sect1>

 <sect1 id="userinfo">
  <title>Erabiltzaileari buruzko informazioa</title>
  <para>Zuri buruzko datuak ICQ erabiltzailearen direktorioan aldatzeko, hautatu <guimenuitem>Aldatu erabiltzaileari buruzko informazioa</guimenuitem> <guimenu>ICQ</guimenu> menuan. <xref    linkend="user-fig"> atalean erakusten den <interface>Erabiltzaileari buruzko informazioa</interface> elkarrizketa-koadroa agertuko da.

  <figure id="user-fig">
   <title>Erabiltzaileari buruzko informazioa elkarrizketa-koadroa</title>
   <screenshot><screeninfo>Erabiltzaileari buruzko informazioa elkarrizketa-koadroa</screeninfo> <graphic fileref="images/userprefs.png" format="png"
     srccredit="Ilya Konstantinov"></graphic></screenshot></figure>

  <para>Elkarrizketa-koadro honen bidez, zuri buruz gauza gehiago konta diezaizkiekezu besteei. Kontuan hartu datu pertsonal horiek ICQ erabiltzailearen direktorioan edonorentzat erabilgarri daudela, eta, beraz, edonork irakur ditzakeela aurrez zurekin kontaktatu gabe.</para>

  <para><guilabel>Orokorra</guilabel> fitxaren propietateak hauek dira: <itemizedlist>

    <listitem><para><guilabel>UINa</guilabel> &mdash; Erabiltzailearen UINa bistaratzen du. Ezin da editatu.
 
      <note><para>Saioa beste erabiltzaile baten moduan beste UIN batekin hasteko, ikus <guilabel>Askotarikoak</guilabel> fitxa <interface>Hobespenen elkarrizketa-koadroan</interface> (ikus <xref linkend="preferences">).</para></note></para></listitem>
    
    <listitem><para><guilabel>Goitizena</guilabel> &mdash; Eremu honetan ICQn duzun goitizena alda dezakezu.</para></listitem>
    
    <listitem><para><guilabel>Izena</guilabel> &mdash; Eremu honetan zure izena zehatz dezakezu.</para></listitem>

    <listitem><para><guilabel>Abizena</guilabel> &mdash; Eremu honetan zure abizena zehatz dezakezu.</para></listitem>
    
    <listitem><para><guilabel>Sexua</guilabel> &mdash; Eremu honetan zure sexua zehatz dezakezu.</para></listitem>

    <listitem><para><guilabel>Adina</guilabel> &mdash; Eremu honetan zure adina zehatz dezakezu.</para></listitem>
    
    <listitem><para><guilabel>Baimena beharrezkoa</guilabel> Kontrol-lauki honen bidez, beste erabiltzaileek zu haien ICQren kontaktu-zerrendan gehitzeko baimena behar duten erabaki dezakezu.

      <note><para>Baimenaren eskaera beste erabiltzaileak erabiltzen duen ICQ bezeroaren arabera egongo da. Bezero batzuek ez dute eginbidea inplementatzen eta erabiltzaileak gehi ditzakete 'Baimena beharrezkoa' kontuan hartu gabe.</para></note>

     </para></listitem>
    
    <listitem><para><guilabel>Urtebetetzea</guilabel> Eremu honetan zure jaioteguna zehatz dezakezu.</para></listitem>
    
   </itemizedlist></para>

  <para> <guilabel>Internet</guilabel> fitxaren propietateak hauek dira: <itemizedlist>

    <listitem><para><guilabel>IP</guilabel> &mdash; Erabiltzailearen IP helbidea bistaratzen du.  Ezin da editatu.</para></listitem>

    <listitem><para><guilabel>Ataka</guilabel> &mdash; Erabiltzen ari den TCP ataka bistaratzen du. Ezin da editatu.</para></listitem>

    <listitem><para><guilabel>h.el.</guilabel>, <guilabel>Beste h.el.</guilabel>, <guilabel>h.el. zaharra:</guilabel> &mdash; Eremu honetan zure helbide elektronikoak sar ditzakezu.</para></listitem>
 
    <listitem><para><guilabel>Ezkutatu h.el.</guilabel> &mdash; Kontrol-lauki honek helbide elektronikoak besteek ikus ditzaketen zehazten du.</para></listitem></itemizedlist></para>

  <para> <guilabel>Kokapena</guilabel> fitxaren bidez, zure kokapenari buruzko hainbat datu edita ditzakezu, esaterako telefono-zenbakia, herrialdea eta ordu-zona.</para>

  <caution><para>Kontu izan datu pertsonalak ematean, esaterako helbidea eta telefono-zenbakia.</para></caution>
  
  <para><guilabel>Honi buruz</guilabel> fitxaren propietateak hauek dira: <itemizedlist>

    <listitem><para><guilabel>Etxeko orria</guilabel> &mdash; Eremu honetan zure etxeko orriaren URLa zehatz dezakezu.</para></listitem>

    <listitem><para><guilabel>Honi buruz</guilabel> &mdash; Eremu honetan zuri buruzko informazio gehigarria gehi dezakezu. Beharbada lerro asko beharko dituzu.</para></listitem></itemizedlist></para>

  <para>Klik egin <guibutton>Aplikatu</guibutton> botoian aldaketak elkarrizketa-koadroa itxi gabe aplikatzeko. Klik egin <guibutton>Ados</guibutton> botoian aldaketak aplikatu eta elkarrizketa-koadroa ixteko. Klik egin <guibutton>Utzi</guibutton> botoian elkarrizketa-koadroa ixteko, egin dituzun aldaketak aplikatu gabe.</para></sect1>   



<!--// work needed here! To close the
    <interface>Properties</interface> dialog, click on 
    <guibutton>OK</guibutton>. To cancel the changes
    and return to previous values, click the
    <guibutton>Cancel</guibutton> button.
  </para> --> <!-- ============= Various Sections ============================= --> <!-- Here you should add, if necessary, several more sect1's,
 describing other windows (besides the main one), file formats,
 preferences dialogs,  etc. as appropriate. Try not to make any of
 these sections too long. -->


 <!-- ============= Bugs ================================== --> <!-- This section should describe known bugs and limitations of
      the program if there are any - please be frank and list all
      problems you know of --> <sect1 id="bugs">
  <title>Ezagutzen diren akatsak eta mugak</title>
  <para><itemizedlist>

    <listitem><para>Ez dago pertsona anitzen arteko berriketarik. Ezin duzu bi erabiltzaile baino gehiagoko berriketetan parte hartu.</para></listitem> <listitem><para>Fitxategi-transferentzia akastuna da.</para></listitem> <listitem><para>Ezinezkoa da erabiltzaileari buruzko informazioan &quot;Lanaren&quot; xehetasunak aldatzea.</para></listitem> <listitem><para>Ezinezkoa da beste erabiltzaile baten urtebetetzeari buruzko datuak ikustea.</para></listitem> <listitem><para>Ezin dira fitxategiak/berriketarako eskaerak bidali NATen (&quot;suebakiaren&quot; atzean dauden hainbat erabiltzaile) atzean zaudenean.</para></listitem></itemizedlist>
   
  </para></sect1>


<!-- ============= Authors ================================ -->

 <sect1 id="authors">
  <title>Egileak</title>
  <para><application>GnomeICU</application> Jeremy Wise-k (<email>jwise@pathwaynet.com</email>), David Tabachnikov-ek (<email>captain@isdn.net.il</email>) eta beste hainbat boluntariok idatzi dute.</para>
  
  <para><application>GnomeICUri</application> buruz informazio gehiago lortzeko, bisitatu <ulink url="http://gnomeicu.sourceforge.net" type="http">GnomeICUren web gunea</ulink>.</para>
 
  <para>Bidali zuen iruzkin, iradokizun eta akatsen berri-emateak <ulink url="http://bugs.gnome.org" type="http">GNOMEren akatsen segimendurako datu-basera</ulink>. (Akatsei buruzko berriak bidaltzeko argibideak <ulink    url="http://bugs.gnome.org/Reporting.html" type="http">linean</ulink> aurkituko dituzu.) GNOMEren 1.2 bertsioa eta berriagoa erabiltzen ari bazara, akatsen berri emateko <application>Akatsen berri emateko tresna</application> (<command>bug-buddy</command>), ere erabil dezakezu. Tresna hori <guimenu>Menu nagusiko</guimenu> <guisubmenu>Utilitateak</guisubmenu> azpimenuan duzu erabilgarri.</para>

  <para>Eskuliburu hau Thomas Canty-k (<email>tommydal@ihug.com.au</email>) eta Ilya Konstantinov-ek (<email>gnomeicu-docs@future.galanet.net</email>) idatzi dute. Eskuliburuari dagozkion iruzkinak eta iradokizunak <ulink type="http"    url="http://www.gnome.org/gdp">GNOMEren dokumentazio-proiektura</ulink> bidali, helbide elektroniko honetara idatziz: <email>docs@gnome.org</email>. Iruzkinak linean ere egin ditzakezu <ulink type="http"    url="http://www.gnome.org/gdp/doctable/">GNOMEren dokumentazio-egoeraren taula</ulink> erabiliz.</para>

  <!-- For translations: uncomment this:

  <para>
   Latin translation was done by ME
   (<email>MYNAME@MYADDRESS</email>). Please send all  comments  and
   suggestions regarding this translation to SOMEWHERE.
  </para>

  -->

 </sect1>


 <!-- ============= Application License ============================= -->

 <sect1 id="license">
  <title>Lizentzia</title>
  <para>Programa hau software librea da; birbana edota alda dezakezu Free Software Foundation-ek argitaratutako <citetitle>GNU Lizentzia Publiko Orokorra</citetitle>ren 2. bertsioan, edo (nahiago baduzu) bertsio berriago batean, jasotako baldintzak betez gero.</para> <para>Programa hau erabilgarria izango delakoan banatzen da, baina INOLAKO BERMERIK gabe; era berean, ez da bermatzen EGOKITASUNA MERKATURATZEKO  edo HELBURU PARTIKULARRETARAKO ERABILTZEKO.  Argibide gehiago nahi izanez gero, ikus <citetitle>GNU Lizentzia Publiko Orokorra</citetitle>.</para> <para> <citetitle>GNU Lizentzia Publiko Orokorra</citetitle>ren kopia bat <citetitle>GNOME erabiltzailearen gida</citetitle>ren eranskinean daukazu.  <citetitle>GNU Lizentzia Publiko Orokorra</citetitle>ren kopia Free Software Foundation-en bidez ere lor dezakezu, <ulink type="http"
   url="http://www.fsf.org">haren Web gunea</ulink> bisitatuz edo hona idatziz: 
   <address>
    Free Software Foundation, Inc. <street>59 Temple Place</street> - Suite 330 <city>Boston</city>, <state>MA</state> <postcode>02111-1307</postcode> <country>USA</country>
   </address>
  </para></sect1></article>

