/*****************************
 Handle file transfer
 (c) 1999 Jeremy Wise
 GnomeICU
******************************/

#ifndef __FILEXFER_H__
#define __FILEXFER_H__

#include "common.h"
#include "v7base.h"

#define XFER_DIRECTION_SENDING		0
#define XFER_DIRECTION_RECEIVING	1

#define XFER_STATUS_PENDING		0
#define XFER_STATUS_CONNECTING		1
#define XFER_STATUS_TRANSFERING		2

extern gint max_tcp_port;
extern gint min_tcp_port;

struct xfer_file {
	char *short_filename;
	char *full_filename;

	guint total_bytes;
	guint completed_bytes;

	time_t start_time;
};

typedef struct {
  GSList *file_queue;
  GSList *completed_files;

  int sock_fd;
  int file_fd;
  GIOChannel *gioc;

  int direction;
  CONTACT_PTR remote_contact;
  gchar *msgid;
  gchar *msg;
  int status;
  WORD port;
  V7Connection *conn;
  guint downcounter;

  void *dialog; /* Xfer dialog struct not yet defined */
  gchar *filename;

  int auto_transfer;
  gchar *auto_save_path;

  guint total_bytes;
  guint completed_bytes;
  time_t start_time;

  char *packet;
  unsigned short packet_offset;
  unsigned short packet_size;
} XferInfo;

void ft_cancel_transfer( GtkWidget *, XferInfo *xfer);

int ft_connectfile( XferInfo *xfer );
void ft_readytoreceive (XferInfo *xfer);
gboolean ft_listen( XferInfo *xfer);

XferInfo* ft_new(Contact_Member *contact, int direction,
		 char *filename, guint filesize);
void ft_addfile(XferInfo *xferinfo, gchar *longname, gchar *shortname,
		guint size);

#endif /* __FILEXFERDLG_H__ */

