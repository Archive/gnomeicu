/****************************
  Reception for ICQ v7-8 protocol (Oscar)
  Olivier Crete (c) 2001-2002
  GnomeICU
*****************************/

#include "common.h"
#include "gnomecfg.h"
#include "gnomeicu.h"
#include "gnomeicu-spinner.h"
#include "groups.h"
#include "gtkfunc.h"
#include "personal_info.h"
#include "changeinfo.h"
#include "response.h"
#include "search.h"
#include "sendcontact.h"
#include "showlist.h"
#include "util.h"
#include "v7recv.h"
#include "v7send.h"
#include "v7snac13.h"
#include "events.h"

#include <libgnomeui/libgnomeui.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h> 
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>

gboolean v7_connected (GIOChannel *channel, GIOCondition condition,
			     V7Connection *conn);
gboolean v7_handler (GIOChannel *iochannel, GIOCondition condition,
                     V7Connection *conn);
gboolean v7_error_handler (GIOChannel *iochannel, GIOCondition condition,
                     V7Connection *conn);

/*  Handlers for the different type of packets */
void   v7_handle_errors             (Snac *snac);
void   v7_snac_channel_handler      (V7Packet *packet, V7Connection *conn);
void   v7_newconn_channel_handler   (V7Packet *packet, V7Connection *conn);

void   v7_make_requests             (V7Connection *conn);
void   v7_ack_ack_rate              (Snac *snac, V7Connection *conn);
void   v7_receive_msg               (Snac *snac, V7Connection *conn);
void   v7_text_message              (UIN_T uin, V7Buffer *buf);
void   v7_special_message           (UIN_T uin, V7Buffer *buf);
void   v7_advanced_message          (UIN_T uin, V7Connection *conn,
				     V7Buffer *buf, const gchar *msgid);
void   v7_advanced_message_ack      (UIN_T uin, V7Connection *conn, 
				     const gchar *msgid, 
                                     const gchar *buf48, gboolean away);
void   v7_incoming_user             (Snac *snac);
void   v7_outgoing_user             (Snac *snac);
void   v7_acked_adv_msg             (Snac *snac, V7Connection *conn);

void   v7_rec_contactlist           (UIN_T uin, gchar *msg, int msglen,
                                     time_t msg_time);

void   v7_server_status             (Snac *snac, V7Connection *conn);
void   v7_rate_changed              (Snac *snac, V7Connection *conn);
void   v7_saved_data                (V7Connection *conn, Snac *snac);
void   v7_offline_message           (DWORD reqid, V7Buffer *buf);
void   v7_saved_data_extension      (DWORD reqid, V7Buffer *buf);
void   v7_search_reply              (DWORD reqid, BYTE result, V7Buffer *buf);
void   v7_end_of_search_reply       (DWORD reqid, BYTE result, V7Buffer *buf);

void   v7_parse_main_home_info      (Request *requestdat, USER_INFO_PTR info, 
                                     BYTE result, V7Buffer *buf);
void   v7_parse_more_info           (Request *requestdat, USER_INFO_PTR info, 
                                     BYTE result, V7Buffer *buf);
void   v7_parse_about_info          (Request *requestdat, USER_INFO_PTR info, 
                                     BYTE result, V7Buffer *buf);
void   v7_parse_work_info           (Request *requestdat, USER_INFO_PTR info, 
                                     BYTE result, V7Buffer *buf);
void   v7_parse_emails_info         (Request *requestdat, USER_INFO_PTR info, 
                                     BYTE result, V7Buffer *buf);
void   v7_parse_homepage_category_info(Request *requestdat, USER_INFO_PTR info, 
                                     BYTE result, V7Buffer *buf);
void   v7_parse_interests_info      (Request *requestdat, USER_INFO_PTR info, 
                                     BYTE result, V7Buffer *buf);
void v7_parse_past_and_affiliation_info(Request *requestdat, USER_INFO_PTR info, 
                                     BYTE result, V7Buffer *buf);


V7Connection *v7_connect(gchar *address, guint port, gchar *cookie, guint cookielen)
{
  V7Connection *conn;
  struct sockaddr_in serv_addr;
  struct hostent *hp;
  
#ifdef TRACE_FUNCTION
  g_print( "v7_connect\n" );
#endif

  conn = g_new0(V7Connection, 1);

  conn->cookie = g_memdup(cookie, cookielen);
  conn->cookielen = cookielen;

  
  conn->fd = socket(PF_INET, SOCK_STREAM, 0);

  conn->iochannel = g_io_channel_unix_new(conn->fd);

  conn->in_watch = g_io_add_watch(conn->iochannel, G_IO_IN|G_IO_HUP|G_IO_ERR, 
				  (GIOFunc)v7_connected, conn);


  hp = gethostbyname(address);

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);
  memcpy(&serv_addr.sin_addr, hp->h_addr, hp->h_length);

  fcntl(conn->fd, F_SETFL, fcntl(conn->fd, F_GETFL, 0)|O_NONBLOCK);

  connect(conn->fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

  conn->last_reqid = 1;
  conn->status = NOT_CONNECTED;

  return conn;
}



gboolean v7_connected (GIOChannel *channel, GIOCondition condition,
		       V7Connection *conn)
{

#ifdef TRACE_FUNCTION
  g_print( "v7_connected\n" );
#endif
  if (condition != G_IO_IN) {
    g_io_channel_shutdown(conn->iochannel, FALSE, NULL);
    g_free(conn);
    Current_Status = STATUS_OFFLINE;
    gnomeicu_set_status_button (FALSE);
    enable_online_events = FALSE;
    gnomeicu_spinner_stop ();


    return FALSE;
  }
  
  
  conn->status = CONNECTING;
  conn->in_watch = g_io_add_watch ( conn->iochannel, G_IO_IN|G_IO_PRI,
                                    (GIOFunc) v7_handler, conn);
  conn->err_watch = g_io_add_watch ( conn->iochannel,
                                     G_IO_ERR|G_IO_HUP|G_IO_NVAL,
                                     (GIOFunc) v7_error_handler, conn);

  return FALSE;
}

gboolean v7_error_handler (GIOChannel *iochannel, GIOCondition condition,
                     V7Connection *conn)
{
#ifdef TRACE_FUNCTION
  g_print( "v7_error_handler\n" );
#endif

  icq_set_status_offline( NULL, NULL);
  return FALSE;
}

gboolean v7_handler (GIOChannel *iochannel, GIOCondition condition,
                     V7Connection *conn)
{
  V7Packet *packet;
  
#ifdef TRACE_FUNCTION
  g_print( "v7_handler\n" );
#endif
  
  g_assert(conn != NULL);
  
  /* If we dont have the whole packet stop here */
  if ((packet = read_flap(conn)) == NULL)
    return TRUE;
  
  switch (packet->channel) {
    
  case CHANNEL_NEWCONN:
    v7_newconn_channel_handler (packet, conn) ;
    break;
  case CHANNEL_SNAC:
    v7_snac_channel_handler( packet, conn);
    break;
  case CHANNEL_ERROR:
    
    break;
  case CHANNEL_CLOSE:
    gnome_error_dialog(_("Server forced disconnect."));
/*    v7_quit();*/
      icq_set_status_offline (NULL, NULL);
    break;
  case CHANNEL_KEEPALIVE:
    
    break;
    
  default:
    g_warning(_("New unknown channel: %d"), packet->channel);
    break;
  }

  free_flap(packet);

  return TRUE;
}



void v7_snac_channel_handler(V7Packet *packet, V7Connection *conn) 
{
  Snac *snac;

#ifdef TRACE_FUNCTION
  g_print( "v7_snac_channel_handler\n" );
#endif

  snac = read_snac(packet);

  if (snac->flags[0] && 0x80) {
    int mini_header_len = 0;
    mini_header_len = v7_buffer_get_w_be (snac->buf, out_err);
    v7_buffer_skip(snac->buf, mini_header_len, out_err);
  }


  switch (snac->family) {
  case FAMILY_01_GENERIC:
    switch (snac->type) {
    case F01_BOTH_ERROR:
      v7_handle_errors(snac);
      break;
    case F01_SERVER_READY:
      v7_iam_icq(conn);
      break;
    case F01_SERVER_ACK_RATE:
      v7_ack_ack_rate(snac, conn);
      v7_make_requests(conn);
      break;
    case F01_SERVER_SENDING_TOO_FAST:
      v7_rate_changed(snac, conn);
      break;
    case F01_SERVER_PERSONAL_INFO:
      v7_server_status(snac, conn);
      break;
    case F01_SERVER_MOTD:
      v7_request_rate (conn);
      break;
    case F01_SERVER_ACK_I_AM_ICQ:
      /* We dont understand the data, ignore it */
      break;
    default:
      break;
    }
    break;
  case FAMILY_02_LOCATION:
    switch (snac->type) {
    case F02_BOTH_ERROR:
      v7_handle_errors(snac);
      break;
    case F02_SERVER_RIGHTS_LOCATION: 
      /* We dont understand the data, ignore it */
      break;
    default:
      break;
    }
    break;
  case FAMILY_03_CONTACTLIST:
    switch (snac->type) {
    case F03_BOTH_ERROR:
      v7_handle_errors(snac);
      break;
    case F03_SERVER_CL_INFO:
      /* We dont understand the data, ignore it */
      break;
    case F03_SERVER_INCOMING_USER:
      v7_incoming_user(snac);
      break;
    case F03_SERVER_OUTGOING_USER:
      v7_outgoing_user(snac);
      break;
    default:
      break;
    }
    break;
  case FAMILY_04_MESSAGING:
    switch (snac->type) {
    case F04_BOTH_ERROR:
      v7_handle_errors(snac);
      break;
    case F04_SERVER_RIGHTS_MSG:
      /* We dont understand the data, ignore it */
      break;
    case F04_SERVER_RECV_MESSAGE:
      v7_receive_msg(snac, conn);
      break;
    case F04_BOTH_ACK_ADV_MSG:
      v7_acked_adv_msg(snac, conn);
      break;
    default:
      break;
    }
    break;
  case FAMILY_09_LISTS:
    switch (snac->type) {
    case F09_BOTH_ERROR:
      v7_handle_errors(snac);
      break;
    case F09_SERVER_LIST_RIGHTS:
      /* We dont understand the data, ignore it */
      break;
    default:
      break;
    }
    break;
  case FAMILY_0B_UNKNOWN_1:
    switch (snac->type) {
    default:
      break;
    }
    break;
  case FAMILY_13_SAVED_LIST:
    switch (snac->type) {
    case F13_BOTH_ERROR:
      v7_handle_errors(snac);
      break;
    case F13_SERVER_REQ_RIGHTS_ANS: /* 0x03 */
      /* reply to F13_SERVER_UNKNOWN_1 */
      break;
    case F13_SERVER_SAVED_LIST: /* 0x06 */
      v7_read_contacts_list (snac, conn);
      break;
    case F13_SERVER_ACK: /* 0x0E */
      v7_snac13_check_srv_ack (conn, snac);
      break;
    case F13_SERVER_LIST_NO_CHANGE: /* 0x0F */
      /* we get this because the value sent didn't change */
      /* send 13,07 */
      snac_send (conn, NULL, 0, FAMILY_13_SAVED_LIST,
		 F13_CLIENT_RDY_TO_USE_LIST, NULL, ANY_ID);
      v7_set_user_info (conn);
      v7_add_icbm (conn);
      v7_setstatus (conn, preset_status);
      v7_client_ready (conn);
      break;
    case F13_SERVER_AUTH_REQUEST: /* 0x19 */
      v7_recv_auth_request (snac);
      break;
    case F13_SERVER_USER_GRANT_AUTH: /* 0x1B */
      v7_user_grant_auth (snac);
      break;
    case F13_SERVER_USER_ADDED_YOU: /* 0x1C */
      v7_recv_added_you (snac);
      break;
    default:
      break;
    }
    break;
  case FAMILY_15_SAVED_DATA:
    switch (snac->type) {
    case F15_BOTH_ERROR:
      v7_handle_errors(snac);
      break;
    case F15_SERVER_SAVED_DATA:
      v7_saved_data(conn, snac);
    default:
      break;
    }
    break;
  case FAMILY_17_NEW_USER:
    switch (snac->type) {
    default:
      break;
   }
    break;
  default:
    break;
    
  }

  free_snac(snac);

  return;
 out_err:
  g_warning ("Error parsing snc");
  
  free_snac(snac);
}


void v7_handle_errors(Snac *snac)
{
  GtkWidget *dialog;
  
   dialog = gtk_message_dialog_new(GTK_WINDOW (MainData->window), 
				   GTK_DIALOG_DESTROY_WITH_PARENT,
				   GTK_MESSAGE_WARNING,
				   GTK_BUTTONS_OK,
       v7_get_error_message(v7_buffer_get_w_be (snac->buf, out)));

  gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_destroy(dialog);

 out:
  return;

}


void v7_newconn_channel_handler (V7Packet *packet, V7Connection *conn) 
{
  TLVstack *cookietlv;

#ifdef TRACE_FUNCTION
  g_print( "v7_newconn_channel_handler\n" );
#endif

  if ( packet->len == 4  && !strncmp (packet->buf->here, "\x0\x0\x0\x1", 4)) {
    
    cookietlv  = new_tlvstack ("\0\0\0\x01", 4);
    
    add_tlv(cookietlv, 6, conn->cookie, conn->cookielen);
      
    if (! flap_send(conn, CHANNEL_NEWCONN, cookietlv->beg, cookietlv->len))
      conn->status = BROKEN;

    g_free(conn->cookie);
    conn->cookie = NULL;
    conn->cookielen = 0;

    free_tlvstack(cookietlv);

    
  } else
    g_warning (_("Unknown packet on login on main server\n"));
}



void v7_ack_ack_rate    (Snac *snac, V7Connection *conn)
{
  WORD classcount, i, j, classid, pairscount, family, subfamily;
  DWORD *familyandsub;
  BYTE state;
  GList *currentclass;
  V7RateClass* class;
  TLVstack *tlvs = new_tlvstack(NULL, 0);

#ifdef TRACE_FUNCTION
  g_print( "v7_ack_ack_rate\n" );
#endif

  classcount = v7_buffer_get_w_be(snac->buf, out_err);
  
  for(i=0; i < classcount; i++) {

    classid = v7_buffer_get_w_be(snac->buf, out_err);

    currentclass = conn->rateclasses;
    while(currentclass != NULL &&
	  ((V7RateClass*)currentclass->data)->id != classid)
      currentclass = currentclass->next;
    if (currentclass) 
      class = currentclass->data;
    else {
      class = g_new0(V7RateClass,1);
      class->id = classid;
      conn->rateclasses = g_list_append(conn->rateclasses, class);
    }

    class->windowsize = v7_buffer_get_dw_be(snac->buf, out_err);
    class->clearlevel = v7_buffer_get_dw_be(snac->buf, out_err);
    class->alertlevel = v7_buffer_get_dw_be(snac->buf, out_err);
    class->limitlevel = v7_buffer_get_dw_be(snac->buf, out_err);
    class->disconnectlevel = v7_buffer_get_dw_be(snac->buf, out_err);
    class->currentlevel = v7_buffer_get_dw_be(snac->buf, out_err);
    class->maxlevel = v7_buffer_get_dw_be(snac->buf, out_err);
    class->lasttime = v7_buffer_get_dw_be(snac->buf, out_err);
    class->state = v7_buffer_get_c(snac->buf, out_err);

    /* 
    g_print("Class: %d State: %d\n", class->id, class->state);
    g_print("WinSize: %d Clear: %d Alert: %d Limit: %d Discon: %d Avg: %d MaxAvg:%d LastTime: %d\n", class->windowsize, class->clearlevel, class->alertlevel, class->limitlevel, class->disconnectlevel, class->currentlevel, class->maxlevel, class->lasttime); 
    */ 
    add_nontlv_w_be(tlvs, classid);
  }
  
  if (classcount && !conn->familyclass) {
    conn->familyclass = g_hash_table_new_full(g_int_hash, g_int_equal, g_free,
					      NULL);
  }

  for(i=0; i < classcount; i++) {

    classid = v7_buffer_get_w_be(snac->buf, out_err);

    currentclass = conn->rateclasses;
    while(currentclass != NULL &&
	  ((V7RateClass*)currentclass->data)->id != classid)
      currentclass = currentclass->next;
    
    if (!currentclass) {
      g_warning("Trying to add family/subfamily pairs to non-existing rate class: %d\n", classid);
      continue;
    }

    pairscount = v7_buffer_get_w_be(snac->buf, out_err);
    
    for (j=0; j < pairscount; j++) {
      familyandsub = g_new0(DWORD,1);
      *familyandsub = v7_buffer_get_dw_be(snac->buf, out_err);
      
      g_hash_table_replace(conn->familyclass, familyandsub, 
			   currentclass->data);
      /* g_print("C: %d F: %X/%X\n", classid, family>>16, subfamily&0xFFFF); */
    }
    
  }

  if (classcount)
    snac_send(conn, tlvs->beg, tlvs->len, FAMILY_01_GENERIC,
	      F01_CLIENT_ACK_ACK_RATE, NULL, ANY_ID);

 out_err:
  free_tlvstack(tlvs);
}

void v7_receive_msg (Snac *snac, V7Connection *conn)
{
  WORD msgformat;
  UIN_T uin = NULL;
  int len = snac->datalen;
  TLV *blah;
  gchar *msgid;
  int tlvcount,i;

#ifdef TRACE_FUNCTION
  g_print( "v7_receive_msg\n" );
#endif

  msgid = v7_buffer_get(snac->buf, 8);
  if (!msgid)
    goto out_err;

  msgformat = v7_buffer_get_w_be(snac->buf, out_err);

  uin = v7_buffer_get_uin(snac->buf, out_err);
  
  v7_buffer_skip(snac->buf, 2, out_err); /* skip sender warning level */

  /* What are those tlvs we are are skipping ?? */
  
  tlvcount = v7_buffer_get_w_be(snac->buf, out_err);

  for(i=0; i<tlvcount; i++) {
    blah = read_tlv(snac->buf, out_err);
    free_tlv(blah); 
  }

  switch(msgformat) {
  case 0x01:
    v7_text_message(uin, snac->buf);
    break;
  case 0x02:
    v7_advanced_message(uin, conn, snac->buf, msgid);
    break;
  case 0x04:
    v7_special_message(uin, snac->buf);
    break;
  default:
    g_warning("unknown message format: %x\n", msgformat);
    break;
  }

 out_err:
  g_free(uin);
}

void v7_text_message(UIN_T uin, V7Buffer *buf)
{
  
  WORD msglen;
  gchar *msg = NULL, *msgtmp=NULL;
  TLV *msgtlv = NULL;
  time_t msg_time;
  int capslen, charset, charsubset;

#ifdef TRACE_FUNCTION
  g_print( "v7_text_message\n" );
#endif

  msgtlv = read_tlv (buf, out_err); 


  /* 
   * type = 4 == automated response
   * type = 0x13 == ??? (size=1 and content=6)
   * type = 6 == user status
   * type = 1 == user class
   * type = 0x0f == idle time
   * type = 3 == account creation time
   */
  while (msgtlv->type != 2) {

    free_tlv (msgtlv);
    msgtlv = read_tlv (buf, out_err);
  }
  


  /* fragment id & fragment version  (should be 5 and 1) */
  v7_buffer_skip (msgtlv->buf, 2, out_err); 
  capslen = v7_buffer_get_w_be (msgtlv->buf, out_err);

  /* fragment id & fragment version (should be 1 and 1) */
  v7_buffer_skip (msgtlv->buf, capslen+2, out_err);
  msglen = v7_buffer_get_w_be (msgtlv->buf, out_err);

  if (msglen < 4)
    goto out_err;

  msglen -= 4;                /* to skip the 4 zeros */
  
  charset = v7_buffer_get_w_be (msgtlv->buf, out_err);
  charsubset = v7_buffer_get_w_be (msgtlv->buf, out_err);

  msgtmp = v7_buffer_get (msgtlv->buf, msglen);
  if (!msgtmp)
    goto out_err;
  msg = g_malloc (msglen +1);
  msg[msglen] = 0;
  g_memmove (msg, msgtmp, msglen);


  if (charset == 2) {
    gchar *newmsg = NULL;

    newmsg = g_convert (msg, msglen, "UTF-8" ,"UTF-16BE", NULL, NULL, NULL);

    if (newmsg != NULL) {
      g_free(msg);
      msg = newmsg;
    }      
  }
      
  
  time (&msg_time);
  
  msg_received (uin, msg, msg_time);

 out_err:
  
  free_tlv(msgtlv); 
  g_free(msg);

}

void v7_special_message(UIN_T uin, V7Buffer *buf)
{

  WORD msglen;
  gchar *msg = NULL, *url, *nick, *first, *last, *email, *tmp=NULL, *conv_msg;
  TLV *msgtlv = NULL;
  BYTE msgtype, msgflags;
  time_t msg_time;

#ifdef TRACE_FUNCTION
  g_print( "v7_special_message\n" );
#endif
  
  msgtlv = read_tlv (buf, out_err);

  v7_buffer_skip (msgtlv->buf, 4, out_err);
  
  msgtype = v7_buffer_get_c (msgtlv->buf, out_err);
  msgflags = v7_buffer_get_c (msgtlv->buf, out_err);

  msglen = v7_buffer_get_w_le (msgtlv->buf, out_err);

  msg = v7_buffer_get (msgtlv->buf, msglen);
  if (!msg)
    goto out_err;

  msg = g_strndup (msg, msglen);

  time(&msg_time);
  
  switch(msgtype) {
  case 0x01:
    msg_received(uin, msg, msg_time);
    break;
  case 0x04:
    url = strchr( msg, '\xFE' ) + 1;
    if (!url) goto out_err;
    *(url-1) = '\0';
    conv_msg = convert_to_utf8(msg);
    url_received (uin, url, conv_msg, msg_time);
    g_free(conv_msg);
    break;
  case 0x13:
    v7_rec_contactlist(uin, msg, msglen, msg_time);
    break;
  case 0x0C:
    user_added_you(uin, msg_time);
    break;
  case 0x06:
    nick = msg;
    tmp = strchr( tmp, '\xFE' );
    if ( tmp == NULL ) 
      goto out_err; /* Bad Packet */
    *tmp = '\0';
    tmp++;

    first = tmp;
    tmp = strchr( tmp, '\xFE' );
    if ( tmp == NULL )
      goto out_err; /* Bad Packet */
    *tmp = '\0';
    tmp++;

    last = tmp;
    tmp = strchr( tmp, '\xFE' );
    if ( tmp == NULL )
      goto out_err; /* Bad Packet */
    *tmp = '\0';
    tmp++;

    email = tmp;
    tmp = strchr( tmp, '\xFE' );
    if ( tmp == NULL )
      goto out_err; /* Bad Packet */
    *tmp = '\0';
    tmp++;

    msg = tmp;
    tmp = strchr( tmp, '\xFE' );
    if ( tmp == NULL )
      goto out_err; /* Bad Packet */
    *tmp = '\0';
    tmp ++;

    authorization_request(uin, msg,msg_time);
    break;
  default:
    g_warning(_("Special message type not handled, type: %x, text: %s\n"), msgtype, msg);
    break;
  }
 out_err:
  free_tlv (msgtlv);
  g_free(msg);
}

void v7_advanced_message(UIN_T uin, V7Connection *conn, V7Buffer *buf, 
    const gchar *msgid)
{

  TLV *maintlv=NULL, *insidetlv=NULL, *tlv=NULL;
  WORD type1, msglen, fileport, downcounter, len;
  BYTE msgtype, msgflags;
  gchar *msg=NULL, *url, *nick, *first, *last, *email, *tmp=NULL, *file =NULL,
    *conv_msg, *mymsgid;
  DWORD fileip=0, filesize;
  time_t msg_time;
  gboolean isfileokack=FALSE;
  int i;
  DWORD skipme;
  gchar *buf48;
  gboolean away = FALSE;

#ifdef TRACE_FUNCTION
  g_print( "v7_advanced_message\n" );
#endif

  maintlv = read_tlv (buf, out);

  /* There is also type 19 with len=1 and content=11 */

  while (maintlv->type != 5) {
    free_tlv(maintlv);
    maintlv = read_tlv(buf, out);
  }

  type1 = v7_buffer_get_w_be (maintlv->buf, out);

  if (type1 != 0) {
    switch (type1) {
    case 1:
      g_warning("We have an abort request, we dont handle that right now!\n");
      break;
    case 2:
      g_warning("We have a file ack, we dont handle that right now!\n");
    default:
      g_warning("We have something else: %d\n", type1);
    }
    goto out;
  }

  v7_buffer_skip (maintlv->buf, 8, out); /* msgid */
  v7_buffer_skip (maintlv->buf, 16, out); /* cap */

  insidetlv = read_tlv(maintlv->buf, out); 

  while (insidetlv->type != 0x2711) {
    switch (insidetlv->type) {
    case 0x000A:  /* file stuff */
      if (v7_buffer_get_w_be (insidetlv->buf, out) == 2) {
	isfileokack = TRUE;
      }
      else
	isfileokack = FALSE;
      break;
    case 0x0005: /* port */
      fileport = v7_buffer_get_w_le (insidetlv->buf, out);
      break;
    case 0x0003: /* ip */
      fileip = v7_buffer_get_dw_be (insidetlv->buf, out);
      break;
    case 0x000F: /* always empty ? */
      break;
    case 0x0017: /* ??????????? size=2 content=0xFBC1 */
      break;
    default:
      g_warning("We have a tlv that we dont know of type: %x\n",
                insidetlv->type);
      print_data(insidetlv->buf->here, insidetlv->buf->len);
      goto out;

    }
    free_tlv(insidetlv);
    insidetlv = read_tlv(maintlv->buf, out);
  }

  if (insidetlv->buf->len < 48)
    goto out;
  buf48 = insidetlv->buf->here;

  /* reading tlv 0x2711 */

  len = v7_buffer_get_w_le (insidetlv->buf, out); /* len of whats left */
  if (len != 0x1B) {
    g_warning ("Malformed packet lenght of first part of 0x2711 = %u?\n",
        len);
    goto out;
  }

  v7_buffer_get_w_le (insidetlv->buf, out); /* protocol version */
  tmp = v7_buffer_get (insidetlv->buf, 16); /* another guid for plugins */
  if (tmp ==  NULL)
    goto out;
  for (i=0; i<16; i++) {
    if (tmp[i] != 0) {
      g_debug ("Unhandled plugin type: \n");
      print_data (tmp, 16);
      goto out;
    }
  }

  v7_buffer_skip (insidetlv->buf, 2, out); /* unknown */
  v7_buffer_get_dw_le (insidetlv->buf, out); /* client caps flag */
  v7_buffer_skip (insidetlv->buf, 1, out); /* unknown */
  downcounter = v7_buffer_get_w_le (insidetlv->buf, out); /* counter? */


  v7_buffer_skip (insidetlv->buf, 12, out); /* completely unknown */
  
  msgtype = v7_buffer_get_c (insidetlv->buf, out);
  msgflags = v7_buffer_get_c (insidetlv->buf, out);

  /* known flags are
   * 0x01 normal
   * 0x03 auto-message
   * 0x80 multiple recipieds
   */

  v7_buffer_get_w_le (insidetlv->buf, out); /* status code */
  v7_buffer_get_w_le (insidetlv->buf, out); /* priority code */
  msglen = v7_buffer_get_w_le (insidetlv->buf, out); /* msg len */

  time(&msg_time);

  switch(msgtype) {
    default: /* lets guess these are text messages too */
      g_warning("Advanced message type not known, type: %x, text: %s\n"
          "We will treat it as if it were a regular message\n",
          msgtype, msg);
    case 0x01: /* text message */
    case 0x41: /* another type of regular message.. from ICQ 5.1 */
      /* The following types are messages according to
       * Andreas Bie�mann <andreas@biessmann.org>
       * he also blames them on ICQ 5.1... 
       */
    case 0x52: 
    case 0x62: 
      msg = v7_buffer_get (insidetlv->buf, msglen);
      if (!msg)
        goto out;

      tmp = g_strndup (msg, msglen);
      msg_received(uin, tmp, msg_time);
      g_free (tmp);
      break;
    case 0x00: /* Automatic message */
    case 0xE8:  /* Request for away message */
    case 0xE9:  /* Request for occupied message */
    case 0xEA:  /* Request for N/A message */
    case 0xEB:  /* Request for DND message */
    case 0xEC:  /* Request for free for chat message */
      away = TRUE;
      break;
    case 0x02: /* Chat */
    case 0x07: /* Auth reject message */
    case 0x08: /* Auth accept message */
    case 0x0D: /* through www.icq.com's web pager */
    case 0x0E: /* from [uin]@pager.icq.com] */
    case 0x1A: /* Extended */
      break;
    case 0x03: /* File */ 
      if (isfileokack) {
        tlv = read_tlv (maintlv->buf, out);
        if (tlv->type == 4) {
          fileip = v7_buffer_get_dw_be (tlv->buf, out);
          g_debug( "fileip: %s\n", inet_ntoa(fileip));
        }
        free_tlv(tlv);
        tlv = NULL;
      }
      
      v7_buffer_skip (insidetlv->buf, 1, out);
      skipme = v7_buffer_get_w_le (insidetlv->buf, out);
      v7_buffer_skip (insidetlv->buf, 4, out);
      msglen =  v7_buffer_get_dw_le (insidetlv->buf, out);
      tmp = v7_buffer_get (insidetlv->buf, msglen);
      if (!tmp)
        goto out;
      msg = g_strndup (tmp, msglen);
      v7_buffer_skip (insidetlv->buf, 4, out);
      msglen = v7_buffer_get_w_le (insidetlv->buf, out);

      tmp = v7_buffer_get (insidetlv->buf, msglen);
      if (!tmp)
        goto out;
      file = g_strndup (tmp, msglen);
      filesize = v7_buffer_get_dw_le (insidetlv->buf, out);
      fileport = v7_buffer_get_w_le (insidetlv->buf, out);

      mymsgid = v7_buffer_get (insidetlv->buf, 8);
      if (!mymsgid)
        goto out;

      /* g_print("msg: %s\n file:%s\n size:%d ip:%lX port:%d count:0x%x\n", msg,
         file, filesize, fileip, fileport, downcounter); */

      if (! isfileokack)
        Do_File( msg, uin, mymsgid, file, filesize, conn, downcounter);
      else 
        start_transfer(uin, mymsgid, fileip, fileport);
      

      break;
    case 0x13: /* Contacts */

      /*
      msglen = Chars_2_Word(insidetlv->value+105);
    
      msg = g_malloc0(msglen +1);
    
      g_memmove(msg, insidetlv->value + 109, msglen);
    
      v7_rec_contactlist(uin, msg, msglen, msg_time); 
      */
      break;
    case 0x04: /* URL */
      msg = v7_buffer_get (insidetlv->buf, msglen);
      if (!msg)
        goto out;

      url = strchr( msg, '\xFE' ) + 1;
      if (!url)
        goto out;
      *(url-1) = '\0';
      conv_msg = convert_to_utf8(msg);
      url_received (uin, url, conv_msg, msg_time);
      g_free(conv_msg);
      break;
    case 0x0C: /* User Added you */
      user_added_you(uin, msg_time);
      break;
    case 0x06:  /* Auth request */
      msg = v7_buffer_get (insidetlv->buf, msglen);
      if (!msg)
        goto out;

      nick = msg;
      tmp = strchr( msg, '\xFE' );
      if ( tmp == NULL ) {
        goto out;
      }
      *tmp = '\0';
      tmp++;

      first = tmp;
      tmp = strchr( tmp, '\xFE' );
      if ( tmp == NULL ) {
        goto out;
      }
      *tmp = '\0';
      tmp++;

      last = tmp;
      tmp = strchr( tmp, '\xFE' );
      if ( tmp == NULL ) {
        goto out;
      }
      *tmp = '\0';
      tmp++;

      email = tmp;
      tmp = strchr( tmp, '\xFE' );
      if ( tmp == NULL ) {
        goto out;
      }
      *tmp = '\0';
      tmp++;


      msg = tmp;
      tmp = strchr( tmp, '\xFE' );
      if ( tmp == NULL ) {
        goto out;
      }
      *tmp = '\0';
      tmp ++;

      authorization_request(uin, msg, msg_time);
      break;
  }


  v7_advanced_message_ack(uin, conn, msgid, buf48, away); 

 out:

  g_free(msg);
  g_free(file);
  
  free_tlv(insidetlv);
  free_tlv(maintlv);
  free_tlv(tlv);
  
}

void v7_advanced_message_ack (UIN_T uin, V7Connection *conn,
    const gchar *msgid, const gchar *buf48, gboolean away) 
{

  TLVstack *tlvs;
  guchar len;

#ifdef TRACE_FUNCTION
  g_print( "v7_advanced_message_ack\n" );
#endif

  tlvs = new_tlvstack(msgid, 8);
  add_nontlv_w_be (tlvs, 2);
  len = strlen (uin);
  add_nontlv (tlvs, &len, 1);
  add_nontlv (tlvs, uin, len);

  add_nontlv_w_be(tlvs, 0x0003);

  /* I dont know what all of that means, but it works */
  
  add_nontlv(tlvs, buf48, 26);
  add_nontlv(tlvs, "\0", 1);
  add_nontlv(tlvs, buf48+27, 20);
  add_nontlv_dw_be(tlvs,0); /* Always send 0 as accept status */

  /* Add away message if away */
  if ( Current_Status == STATUS_AWAY ||
       Current_Status == STATUS_NA ||
       Current_Status == STATUS_OCCUPIED ||
       Current_Status == STATUS_DND) {
    add_nontlv_w_le(tlvs, strlen(Away_Message)+1);
    add_nontlv(tlvs, Away_Message, strlen(Away_Message)+1);
  }
  else
    add_nontlv_w_le(tlvs, 0);  

  if (!away) {  /* add this if not autoawaymessage reply */
    add_nontlv_dw_be(tlvs, 0);
    add_nontlv_dw_be(tlvs, 0xFFFFFF00);
  }
  
  snac_send(conn, tlvs->beg, tlvs->len, FAMILY_04_MESSAGING,
            F04_BOTH_ACK_ADV_MSG, NULL, ANY_ID);

  free_tlvstack(tlvs);
}

void v7_incoming_user (Snac *snac)
{

  UIN_T uin = NULL; 
  GSList *contact;
  int tlvcount, i, j;
  TLV *tlv = NULL;
  gboolean status_has_changed = FALSE;
  WORD tempstatus;
  gchar *tmp = NULL;

#ifdef TRACE_FUNCTION
  g_print( "v7_incoming_user\n");
#endif

  uin = v7_buffer_get_uin(snac->buf, out_err);

  contact = Find_User( uin );
  if( contact == NULL )
    goto out_err;

  v7_buffer_skip (snac->buf, 2, out_err); /* warning level ? */

  tlvcount = v7_buffer_get_w_be (snac->buf, out_err);
  
  for(i=0; i<tlvcount; i++) {
    tlv = read_tlv(snac->buf, out_err); 

    switch (tlv->type) {
    case 0x0C:
      kontakt->has_direct_connect = v7_buffer_get_dw_be (tlv->buf, out_err) ? 
          TRUE : FALSE;
      kontakt->port = v7_buffer_get_dw_be (tlv->buf, out_err);
      v7_buffer_get_c (tlv->buf, out_err); /* DC type */
      kontakt->version = v7_buffer_get_w_be(tlv->buf, out_err);
      kontakt->direct_cookie = v7_buffer_get_dw_be (tlv->buf, out_err);
      if (kontakt->version > TCP_VERSION)
        kontakt->used_version = TCP_VERSION;
      else
        kontakt->used_version  = kontakt->version;
      break;
    case 0x0A:
      kontakt->current_ip = v7_buffer_get_dw_be (tlv->buf, out_err);
      break;
    case 0x06:
      tempstatus = v7_buffer_get_w_be (tlv->buf, out_err);

      /* Check for birthday */
      if(tempstatus & 0x08)
	kontakt->has_birthday = TRUE;      
      else 
	kontakt->has_birthday = FALSE;

      tempstatus = v7_buffer_get_w_be (tlv->buf, out_err);

      if (kontakt->status != tempstatus)
      	status_has_changed = TRUE;
      /* getting rid of non-standard statuses here */
      switch (tempstatus) {
      case STATUS_NA_99A: 
	tempstatus = STATUS_NA;
	break;
      case STATUS_OCCUPIED_MAC:
	tempstatus = STATUS_OCCUPIED;
	break;
      case STATUS_DND_MAC:
	tempstatus = STATUS_DND;
	break;
      }
      
      /* the normal invisible status is online+invisible, other
	 status will be shown as normal */
      if (tempstatus & STATUS_INVISIBLE &&
	  tempstatus != STATUS_INVISIBLE)
	tempstatus &= ~STATUS_INVISIBLE;
	
      /* ignore custom status flag */
      if (tempstatus & STATUS_CUSTOM)
	tempstatus &= ~STATUS_CUSTOM;
	
      set_contact_status (contact, tempstatus, 
			  snac->packet->conn->readysince + 30 >
			  time(NULL));
      break;
    case 0x01:
      tempstatus = v7_buffer_get_w_be (tlv->buf, next1) & 0xBFFF;
      if (tempstatus == STATUS_AIM_ONLINE)
        set_contact_status (contact, STATUS_ONLINE,
            snac->packet->conn->readysince + 30 > time(NULL));
      else if (tempstatus == STATUS_AIM_AWAY)
        set_contact_status (contact, STATUS_AWAY, 
            snac->packet->conn->readysince + 30 > time(NULL));
    next1:
      break;
    case 0x0D: /* Capabilites */
      while (tmp = v7_buffer_get (tlv->buf, 16)) {
	if (!strncmp(tmp, "\x97\xB1\x27\x51" "\x24\x3C\x43\x34" "\xAD\x22\xD6\xAB" "\xF7\x3F\x14\x92", 16))
	  kontakt->has_rtf = TRUE;
      } 
      break;
    case 0x03:
      kontakt->online_since = v7_buffer_get_dw_be (tlv->buf, next3);
      update_personal_info(kontakt->uin);
    next3:
      break;
    case 0x05:
      kontakt->member_since = v7_buffer_get_dw_be (tlv->buf, next5);
      update_personal_info(kontakt->uin);
    next5:
      break;
    case 0x04:
      kontakt->idle_since = time(NULL) - 
          (v7_buffer_get_dw_be (tlv->buf, out_err)*60);
    }

    free_tlv(tlv);
    tlv = NULL;
  }

  if (kontakt->status == STATUS_OFFLINE) {
    status_has_changed = TRUE;
    set_contact_status (contact, STATUS_ONLINE, 
			snac->packet->conn->readysince + 30 > time(NULL));
  }

  kontakt->last_time = time( NULL );

  return;
 out_err:
  free_tlv (tlv);
  g_free (uin);
}

void v7_outgoing_user (Snac *snac)
{
  
  UIN_T uin = NULL;
  GSList *contact;
  gchar *tmp = NULL;
  
#ifdef TRACE_FUNCTION
  g_print( "v7_outgoing_user\n" );
#endif
  
  uin = v7_buffer_get_uin(snac->buf, out_err);

  contact = Find_User( uin );
  if( contact == NULL )
    return;
  
  kontakt->has_rtf = FALSE;

  User_Offline(contact);
 out_err:

  g_free (uin);
}


void v7_acked_adv_msg (Snac *snac, V7Connection *conn)
{

  BYTE type;
  gchar *awaymessage, *awaymessage_utf8;
  UIN_T sender_uin;
  WORD sender_status;

#ifdef TRACE_FUNCTION
  g_print( "v7_acked_adv_msg\n" );
#endif
  
  type = snac->buf->here[11+snac->buf->here[10]+2+26+1+18];

  switch (type) {
  case 0xE8:
    sender_status = STATUS_AWAY;
    break;
  case 0xE9:
    sender_status = STATUS_OCCUPIED;
    break;
  case 0xEA:
    sender_status = STATUS_NA;
    break;
  case 0xEB:
    sender_status = STATUS_DND;
    break;
  case 0xEC:
    sender_status = STATUS_FREE_CHAT;
    break;
  default:
    return;
  }

  sender_uin = snac->buf->here+11;

  awaymessage = g_strndup(snac->buf->here+11+snac->buf->here[10]+2+26+1+20+1+3+2,
                          Chars_2_Word(snac->buf->here+11+snac->buf->here[10]+2+26+1+20+1+3));
  
  awaymessage_utf8 = convert_to_utf8(awaymessage);
  g_free(awaymessage);

  recv_awaymsg( sender_uin, sender_status, awaymessage_utf8);
  g_free(awaymessage_utf8);
}

void v7_saved_data(V7Connection *conn, Snac *snac)
{
  TLV *tlv;
  WORD type, reqid;

#ifdef TRACE_FUNCTION
  g_print( "v7_saved_data\n" );
#endif

  tlv = read_tlv(snac->buf, out_err);
  
  /* 1 w_le .. which is the size
   *  1 dw_le which is my UIN
   */
  v7_buffer_skip (tlv->buf, 6, out_err);

  type = v7_buffer_get_w_be (tlv->buf, out_err);
  reqid = v7_buffer_get_w_le (tlv->buf, out_err);

  switch(type) {
  case 0x4100:
    v7_offline_message(snac->reqid, tlv->buf);
    break;
  case 0x4200: 
    v7_send_often(conn, snac->reqid);
    break; 
  case 0xDA07:
    v7_saved_data_extension(snac->reqid, tlv->buf);
  }

 out_err:
  free_tlv(tlv);

}

void v7_saved_data_extension (DWORD reqid, V7Buffer *buf)
{
  WORD subtype;
  BYTE result; 
  GSList *requestdat=NULL, *contact=Contacts;
  gchar *nick = NULL;
  USER_INFO_PTR info = NULL;

#ifdef TRACE_FUNCTION
  g_print( "v7_saved_data_extension\n" );
#endif
  
  subtype = v7_buffer_get_w_be (buf, out_err);
  result = v7_buffer_get_c (buf, out_err);


  /* We do these only if we have a full info request,
     see it as a type of "if" with 8 ORed conditions */
  switch (subtype) {
  case 0xC800:
    // nick = data + 2 /* WTF IS THAT ? */
  case 0xDC00:
  case 0xEB00:
  case 0x0E01:
  case 0xD200:
  case 0xE600:
  case 0xF000:
  case 0xFA00:
    
    requestdat = v7_get_request_by_reqid(reqid);

    if (requestdat == NULL)
      return;

    if (((Request*)requestdat->data)->type == OUR_INFO) {
      info = our_info;
      break;
    }

    contact = Find_User(((Request*)(requestdat->data))->uin);
    
    if (contact == NULL )
      return;

    /* Anti spam action here */
    if (((Request*)requestdat->data)->type == NEW_USER_VERIFICATION) {
      if (result != 0x0A) {
        remove_contact (contact, TRUE);
        g_free(((Request*)requestdat->data)->uin);
        g_free(requestdat->data);
        requests = g_slist_remove(requests, requestdat->data);
        return;
      }
      else {
        if (kontakt->confirmed == FALSE) {
          kontakt->confirmed = TRUE;
          kontakt->uid = contact_gen_uid();
          kontakt->gid = 0;
          gnomeicu_event (EV_MSGRECV, contact);
          if (nick != NULL) {
            g_free(kontakt->nick);
            kontakt->nick = g_strdup(nick);
	  }
          gnomeicu_tree_user_add (kontakt);
        }
      }
    }
    else if (((Request*)requestdat->data)->type != FULL_INFO)
      return;
        
    if (result != 0x0A)
      return;
    
    if( kontakt->info == NULL ) {
      if (!strcmp(kontakt->uin, our_info->uin))
	kontakt->info = our_info;
      else
	kontakt->info = g_malloc0( sizeof(USER_INFO_STRUCT));
    }

    g_free (kontakt->info->uin);
    kontakt->info->uin = g_strdup (kontakt->uin);
    info = kontakt->info;

    break;
  default:
    break;
  }

  switch (subtype) {
  case 0x9001:
  case 0xA401:
    v7_search_reply(reqid, result, buf);
    break;
  case 0x9A01:
  case 0xAE01:
    v7_end_of_search_reply(reqid, result, buf);
    break;
  case 0xC800:
    v7_parse_main_home_info(requestdat->data, info, result, buf);
    break;
  case 0xDC00:
    v7_parse_more_info(requestdat->data, info, result, buf);
    break;
  case 0xD200:
    v7_parse_work_info(requestdat->data, info, result, buf);
    break;
  case 0xE600:
    v7_parse_about_info(requestdat->data, info, result, buf);
    break;
  case 0xEB00:
    v7_parse_emails_info(requestdat->data, info, result, buf);
	 break;
  case 0x0E01:
    v7_parse_homepage_category_info(requestdat->data, info, result, buf);
	 break;
  case 0xF000:
    v7_parse_interests_info(requestdat->data, info, result, buf);
	 break;
  case 0xFA00:
    v7_parse_past_and_affiliation_info(requestdat->data, info, result, buf);

    /* This one seems to come last.. so we delete the requestdat here */
    if (requestdat) {      
      g_free(((Request*)requestdat->data)->uin);
      g_free(requestdat->data);
      requests = g_slist_remove(requests, requestdat->data);
    }
    break;
  }

 out_err:
  return;
}

void v7_offline_message(DWORD reqid, V7Buffer *buf)
{

  time_t msg_time;
  struct tm build_time;
  UIN_T uin;
  DWORD uindw;
  BYTE msgtype, msg_flags;
  WORD msglen;
  gchar *msg = NULL;
  gchar *url, *nick, *first, *last, *email, *tmp=NULL, *conv_msg;

#ifdef TRACE_FUNCTION
  g_print( "v7_offline_message\n" );
#endif
  
  uindw = v7_buffer_get_dw_le (buf, out_err);

  
  uin = g_strdup_printf ("%d", uindw);

  build_time.tm_year = v7_buffer_get_w_le (buf, out_err) - 1900;
  build_time.tm_mon = v7_buffer_get_c (buf, out_err) -1;  /* jan = 1 */
  build_time.tm_mday = v7_buffer_get_c (buf, out_err);
  build_time.tm_hour = v7_buffer_get_c (buf, out_err);
  build_time.tm_min = v7_buffer_get_c (buf, out_err);
  build_time.tm_sec = 0;
  msg_time = mktime(&build_time);

  msgtype  = v7_buffer_get_c (buf, out_err);
  msg_flags = v7_buffer_get_c (buf, out_err);
  msglen = v7_buffer_get_w_le (buf, out_err);

  tmp = v7_buffer_get (buf, msglen);
  if (!tmp)
    goto out_err;
  msg = g_strndup (tmp, msglen);
  tmp = NULL;

  switch(msgtype) {
  case 0x01:
    msg_received(uin, msg, msg_time);
    break;
  case 0x04:
    url = strchr( msg, '\xFE' ) + 1;
    if (!url)
      goto out_err;
    *(url-1) = '\0';
    conv_msg = convert_to_utf8(msg);
    url_received (uin, url, conv_msg, msg_time);
    g_free(conv_msg);
    break;
  case 0x0C:
    user_added_you(uin, msg_time);
    break;
  case 0x06:
    nick = msg;
    tmp = strchr( msg, '\xFE' );
    if ( tmp == NULL ) 
      goto out_err; /* Bad Packet */
    *tmp = '\0';
    tmp++;

    first = tmp;
    tmp = strchr( tmp, '\xFE' );
    if ( tmp == NULL )
      goto out_err; /* Bad Packet */
    *tmp = '\0';
    tmp++;

    last = tmp;
    tmp = strchr( tmp, '\xFE' );
    if ( tmp == NULL )
      goto out_err; /* Bad Packet */
    *tmp = '\0';
    tmp++;

    email = tmp;
    tmp = strchr( tmp, '\xFE' );
    if ( tmp == NULL )
      goto out_err; /* Bad Packet */
    *tmp = '\0';
    tmp++;

    msg = tmp;
    tmp = strchr( tmp, '\xFE' );
    if ( tmp == NULL )
      goto out_err; /* Bad Packet */
    *tmp = '\0';
    tmp ++;

    authorization_request(uin, msg, msg_time);
    break;
  case 0x13:
    v7_rec_contactlist(uin, msg, msglen, msg_time);
    break;
  default:
    g_warning("Offline message type not handled, type: %x, text: %s\n", msgtype, msg);
    break;
  }

 out_err:
  g_free(msg);
  g_free(uin);
}

/* 
   Returns the end of what has been read (a pointer to what's next)
   for the last packet in row type
*/


void v7_search_reply(DWORD reqid, BYTE result, V7Buffer *buf)
{

  WORD len, templen;
  UIN_T uin = NULL;
  gchar *nick=NULL, *first=NULL, *last=NULL, *email=NULL;
  gchar auth=0, status=2, sex=0, age=0;
 
#ifdef TRACE_FUNCTION
  g_print( "v7_search_reply\n" );
#endif 

  len = v7_buffer_get_w_le (buf, out_err);

  uin = g_strdup_printf ("%d", v7_buffer_get_dw_le (buf, out_err));

  templen = v7_buffer_get_w_le (buf, full);
  nick = v7_buffer_get_string (buf, templen, full);

  templen = v7_buffer_get_w_le (buf, full);
  first = v7_buffer_get_string (buf, templen, full);

  templen = v7_buffer_get_w_le (buf, full);
  last = v7_buffer_get_string (buf, templen, full);

  templen = v7_buffer_get_w_le (buf, full);
  email = v7_buffer_get_string (buf, templen, full);

  auth = !v7_buffer_get_c (buf, full);
  status = v7_buffer_get_w_le (buf, full);
  sex = v7_buffer_get_c (buf, full);
  age = v7_buffer_get_w_le (buf, full);

 full:
  
  Display_Search_Reply(reqid, uin, nick, first, last, email, auth, status,
                       sex, age);

 out_err:
  g_free (nick);
  g_free (first);
  g_free (last);
  g_free (email);
  g_free (uin);
  
}

void v7_end_of_search_reply(DWORD reqid, BYTE result, V7Buffer *buf)
{
  gint16 more_results = 0;

#ifdef TRACE_FUNCTION
  g_print( "v7_end_of_search_reply\n" );
#endif 
  if (result != 0x32) {
    v7_search_reply(reqid, result, buf);

    more_results = v7_buffer_get_dw_le (buf, complete);
  }
  
 complete:
  search_finished (reqid, more_results);

}


#define INFO_REPLACEME(field) ( {             \
  len = v7_buffer_get_w_le (buf, out);        \
  str = v7_buffer_get_string (buf, len, out); \
  g_free(field);                              \
  field =  convert_to_utf8( str );            \
  g_free (str); } )



/* Retrieves:
 * - Nickname
 * - First/Last Name
 * - Email
 * - City, State
 * - Phone, Fax
 * - Street Address
 * - Cellular phone
 * - Zip code, Country
 * - Time zone
 */
void v7_parse_main_home_info (Request *requestdat, USER_INFO_PTR info, 
                              BYTE result, V7Buffer *buf)
{
  int len;
  gchar *str;
     
#ifdef TRACE_FUNCTION
  g_print( "v7_parse_main_home_info\n" );
#endif 

  INFO_REPLACEME (info->nick);

  if (requestdat->type == NEW_USER_VERIFICATION) {
    GSList *contact = Contacts;
    
    contact = Find_User( info->uin );
    
    if (contact != NULL ) {
		g_free(kontakt->nick);
		kontakt->nick = g_strdup(info->nick);
	  }
  }

  INFO_REPLACEME (info->first);

  INFO_REPLACEME (info->last);

  INFO_REPLACEME (info->email);

  INFO_REPLACEME (info->city);

  INFO_REPLACEME (info->state);

  INFO_REPLACEME (info->phone);

  INFO_REPLACEME (info->fax);

  INFO_REPLACEME (info->street);

  INFO_REPLACEME (info->cellular);

  INFO_REPLACEME (info->zip);


  info->country = v7_buffer_get_w_le (buf, out);
  info->timezone = v7_buffer_get_c (buf, out);
  info->auth = v7_buffer_get_c (buf, out) ? 0 : 1;

 out:

  if (requestdat->type == OUR_INFO) {
    update_our_info();
  } else
    update_personal_info(info->uin);
}

/* Retrieves:
 * - Age
 * - Sex
 * - Homepage
 * - Birth year, month, day
 * - Languages (3)
 */
void v7_parse_more_info (Request *requestdat, USER_INFO_PTR info, 
                         BYTE result, V7Buffer *buf)
{
  int len;
  gchar *str;
  int tmp;

#ifdef TRACE_FUNCTION
  g_print( "v7_parse_more_info\n" );
#endif 
  
  tmp = v7_buffer_get_w_le (buf, out);
  if (tmp == -1)
    info->age = 0;
  else
    info->age = tmp;
  /* 0x01: FEMALE, 0x02: MALE */
  info->sex = v7_buffer_get_c (buf, out);;
  if ( info->sex > 2 || info->sex == 0)
    info->sex = NOT_SPECIFIED;

  INFO_REPLACEME (info->homepage);

  info->birth_year = v7_buffer_get_w_le (buf, out);
  info->birth_month = v7_buffer_get_c (buf, out);
  info->birth_day = v7_buffer_get_c (buf, out);

  /*
    WTF WAS THAT ??

  our_info->birth_year = info->birth_year;
  our_info->birth_month = info->birth_month;
  our_info->birth_day = info->birth_day;

  v7_setstatus2(mainconnection, Current_Status);
  */

  info->language1 = v7_buffer_get_c (buf, out);
  info->language2 = v7_buffer_get_c (buf, out);
  info->language3 = v7_buffer_get_c (buf, out);

 out:
  if (requestdat->type == OUR_INFO) {
    update_our_info();
  } else
    update_personal_info(info->uin);
}

/* Retrieves:
 * - About
 */
void v7_parse_about_info (Request *requestdat, USER_INFO_PTR info, 
                         BYTE result, V7Buffer *buf)
{
  int len;
  gchar *str;

#ifdef TRACE_FUNCTION
  g_print( "v7_parse_about_info\n" );
#endif 
  
  INFO_REPLACEME (info->about);

  if (requestdat->type == OUR_INFO) {
    update_our_info();
  } else
    update_personal_info(info->uin);

 out:
  return;
}

/* Retrieves:
 * - Work city, state
 * - Work phone, fax
 * - Work address, zip code
 * - Company name
 * - Department
 * - Job position
 * - Work homepage
 */
void v7_parse_work_info (Request *requestdat, USER_INFO_PTR info, 
                         BYTE result, V7Buffer *buf)
{
  int len;
  gchar *str;

#ifdef TRACE_FUNCTION
  g_print( "v7_parse_work_info\n" );
#endif 
  
  INFO_REPLACEME (info->work_city);

  INFO_REPLACEME (info->work_state);

  INFO_REPLACEME (info->work_phone);

  INFO_REPLACEME (info->work_fax);

  INFO_REPLACEME (info->work_address);
  
  INFO_REPLACEME (info->work_zip);

  info->work_country = v7_buffer_get_w_le (buf, out);

  INFO_REPLACEME (info->company_name);

  INFO_REPLACEME (info->department);

  INFO_REPLACEME (info->job_pos);

  info->occupation = v7_buffer_get_w_le (buf, out);

  INFO_REPLACEME (info->work_homepage);

 out:

  if (requestdat->type == OUR_INFO) {
    update_our_info();
  } else
    update_personal_info(info->uin);
}

#undef INFO_REPLACEME

    
void v7_parse_emails_info(Request *requestdat, USER_INFO_PTR info, 
                          BYTE result, V7Buffer *buf)
{
  int len_str, count, i;
  int len_temp;
  BYTE hide;
  gchar *email;
  gchar *temp, *temp2;

#ifdef TRACE_FUNCTION
  g_print( "v7_parse_emails_info\n" );
#endif 
  
  count = v7_buffer_get_c (buf, out);

  /* we free all the data in info->emails */
  g_list_foreach (info->emails, (GFunc)g_free, NULL);
  g_list_free(info->emails);
  info->emails = NULL;

  for (i=0; i<count; i++) {
    /* Skipping hide, if it was hidden, we wouldnt see it */
    hide = v7_buffer_get_c (buf, out);

    len_str = v7_buffer_get_w_le (buf, out);
    temp2 = v7_buffer_get_string (buf, len_str, out);

    temp = convert_to_utf8 (temp2);
    g_free (temp2);

    if (!temp)
      continue;

    len_temp = strlen(temp)+1;
    email = g_malloc0(len_temp+1);
    email[0] = hide; /* save the hide value for use if it's our info */
    if (temp)
      strncpy( email+1, temp , len_temp );
    else
      email[1] = 0;

    g_free(temp);
    
    info->emails = g_list_append(info->emails, email);
  }

 out:

  if (requestdat->type == OUR_INFO) {
    update_our_info();
  } else
    update_personal_info(info->uin);
}

void v7_parse_homepage_category_info(Request *requestdat, USER_INFO_PTR info, 
                          BYTE result, V7Buffer *buf)
{
  int len_str, count, i;
  int len_temp;
  DWORD cat;
  gchar *temp, *temp2;
  gchar *category;

#ifdef TRACE_FUNCTION
  g_print( "v7_parse_homepage_category_info\n" );
#endif 

  count = v7_buffer_get_c (buf, out);

  /* we free all the data in info->homepage_cat */
  g_list_foreach (info->homepage_cat, (GFunc)g_free, NULL);
  g_list_free(info->homepage_cat);
  info->homepage_cat = NULL;

  for (i=0; i<count; i++) {
    cat = v7_buffer_get_w_le (buf, out);

    len_str = v7_buffer_get_w_le (buf, out);
    temp2 = v7_buffer_get_string (buf, len_str, out);

    temp = convert_to_utf8(temp2);
    g_free (temp2);
    if (!temp) {
      continue;
    }

    len_temp = strlen(temp)+1;

    category = g_malloc0(len_temp+2);
  
    *(WORD *) category = cat;

    strncpy( category+2, temp, len_temp);

    g_free(temp);
    
    info->homepage_cat = g_list_append(info->homepage_cat, category);
  }

 out:

  if (requestdat->type == OUR_INFO) {
    update_our_info();
  } else
    update_personal_info(info->uin);
}

void v7_parse_interests_info(Request *requestdat, USER_INFO_PTR info, 
                          BYTE result, V7Buffer *buf)
{
  int len_str, count, i;
  int len_temp;
  gchar *interest;
  gchar *temp, *temp2;
  GList *element;
  WORD inter;

#ifdef TRACE_FUNCTION
  g_print( "v7_parse_interests_info\n" );
#endif 

  count = v7_buffer_get_c (buf, out);

  /* we free all the data in info->interests */
  g_list_foreach (info->interests, (GFunc)g_free, NULL);
  g_list_free(info->interests);
  info->interests = NULL;

  for (i=0; i<count; i++) {
    inter = v7_buffer_get_w_le (buf, out);

    len_str = v7_buffer_get_w_le (buf, out);
    temp2 = v7_buffer_get_string (buf, len_str, out);

    temp = convert_to_utf8(temp2);
    g_free (temp2);
    if (!temp) {
      continue;
    }

    len_temp = strlen(temp)+1;

    interest = g_malloc0(len_temp+2);
    *(WORD *) interest = inter;
    strncpy( interest+2, temp, len_temp );

    g_free(temp);
    
    info->interests = g_list_append(info->interests, interest);
  }

 out:
  if (requestdat->type == OUR_INFO) {
    update_our_info();
  } else
    update_personal_info(info->uin);
}

void v7_parse_past_and_affiliation_info(Request *requestdat, USER_INFO_PTR info, 
                          BYTE result, V7Buffer *buf)
{
  int len_str, count, i;
  int len_temp;
  gchar *buffer;
  gchar *temp, *temp2;
  WORD cat;

#ifdef TRACE_FUNCTION
  g_print( "v7_parse_past_and_affiliation_info\n" );
#endif 

  count = v7_buffer_get_c (buf, out);

  /* we free all the data in info->past_background */
  g_list_foreach (info->past_background, (GFunc) g_free, NULL);
  g_list_free(info->past_background);
  info->past_background = NULL;

  for (i=0; i<count; i++) {
    cat = v7_buffer_get_w_le (buf, out);

    len_str = v7_buffer_get_w_le (buf, out);
    temp2 = v7_buffer_get_string (buf, len_str, out);

    temp = convert_to_utf8(temp2);
    g_free (temp2);
    if (!temp) {
      continue;
    }

    len_temp = strlen(temp)+1;

    buffer = g_malloc0(len_temp+2);
    *(WORD *) buffer = cat;
    strncpy( buffer+2, temp, len_temp );
    g_free(temp);
    
    info->past_background = g_list_append(info->past_background, buffer);
  }

  count = v7_buffer_get_c (buf, out);

  /* we free all the data in info->affiliations */
  g_list_foreach (info->affiliations, (GFunc) g_free, NULL);
  g_list_free(info->affiliations);
  info->affiliations = NULL;

  for (i=0; i<count; i++) {
    cat = v7_buffer_get_w_le (buf, out);

    len_str = v7_buffer_get_w_le (buf, out);
    temp2 = v7_buffer_get_string (buf, len_str, out);

    temp = convert_to_utf8(temp2);
    g_free (temp2);
    if (!temp) {
      continue;
    }

    len_temp = strlen(temp)+1;

    buffer = g_malloc0(len_temp+2);
    *(WORD *) buffer = cat;
    strncpy( buffer+2, temp, len_temp );
    g_free(temp);
    
    info->affiliations = g_list_append(info->affiliations, buffer);
  }

 out:

  if (requestdat->type == OUR_INFO) {
    update_our_info();
  } else
    update_personal_info(info->uin);
}


void v7_rec_contactlist(UIN_T uin, gchar *msg, int msglen, time_t msg_time)
{
  gchar *countstr, *tmp;
  GList *contacts=NULL;
  ContactPair *cpair;
  int i, count;

#ifdef TRACE_FUNCTION
  g_print( "v7_rec_contactlist\n" );
#endif 

    countstr = msg;
    tmp = strchr( msg, '\xFE' );
    if ( tmp == NULL ) 
      return; /* Bad Packet */
    *tmp = '\0';
    tmp++;

    count = atoi(countstr);

    if (count == 0)
      return;

    for (i=0; i < count; i++) {
      cpair = g_new0(ContactPair, 1);
      
      cpair->textuin = tmp;
      tmp = strchr( tmp, '\xFE' );
      if ( tmp == NULL )
        return; /* Bad Packet */
      *tmp = '\0';
      cpair->textuin = g_strdup(cpair->textuin);
      tmp++;
      
      cpair->nick = tmp;
      tmp = strchr( tmp, '\xFE' );
      if ( tmp == NULL && i != count-1) 
        return; /* Bad Packet */
      if (tmp)
        *tmp = '\0';
      cpair->nick = g_strdup(cpair->nick);
      tmp++;

      contacts = g_list_append(contacts, cpair);
    }
    
    contact_list_received (uin, contacts, msg_time);
}

void  v7_rate_changed (Snac *snac, V7Connection *conn)
{

  WORD code, classid;
  V7RateClass *class;
  GList *currentclass;
  DWORD window_size, clear, alert, limit, disconnect, currentavg, maxavg;
  
#ifdef TRACE_FUNCTION
  g_print( "v7_rate_changed\n" );
#endif 

  /* Dont ask why there is a 8 */
  v7_buffer_skip (snac->buf, 8, out_err);

  code = v7_buffer_get_w_be (snac->buf, out_err);
  
  /*
    1 = Changed
    2 = Warning
    3 = Limit
    4 = Limit cleared
  */

  classid = v7_buffer_get_w_be (snac->buf, out_err);

  currentclass = conn->rateclasses;
  while(currentclass != NULL &&
	((V7RateClass*)currentclass->data)->id != classid)
    currentclass = currentclass->next;
  if (currentclass) 
    class = currentclass->data;
  else {
    g_warning("The server sent class changing information for an unknown class\n");
    return;
  }

  
  class->windowsize = v7_buffer_get_dw_be (snac->buf, out_err);
  class->clearlevel = v7_buffer_get_dw_be (snac->buf, out_err);
  class->alertlevel = v7_buffer_get_dw_be (snac->buf, out_err);
  class->limitlevel = v7_buffer_get_dw_be (snac->buf, out_err);
  class->disconnectlevel = v7_buffer_get_dw_be (snac->buf, out_err);
  class->currentlevel = v7_buffer_get_dw_be (snac->buf, out_err);
  class->maxlevel = v7_buffer_get_dw_be (snac->buf, out_err);
  class->lasttime = v7_buffer_get_dw_be (snac->buf, out_err);
  class->state = v7_buffer_get_c (snac->buf, out_err);

    //    g_print("Class:
  //  g_print("Code: %d Class: %d\n", code, class);
  // g_print("WinSize: %d Clear: %d Alert: %d Limit: %d Discon: %d Avg: %d MaxAvg:%d\n", window_size, clear, alert, limit, disconnect, currentavg, maxavg);

  //  if (code == 2)
  //    gnome_warning_dialog(_("You are sending too fast, please slow down your sending speed."));
  
  if (code == 3)
    gnome_warning_dialog(_("You are sending messages too fast, your last message has been dropped, please wait 10 seconds and resend. If you continue sending messages at that speed, you will be disconnected by the server. Please give it a break."));
 out_err:
  return;
}

void v7_server_status(Snac *snac, V7Connection *conn)
{

  int tlvcount, i;
  TLV *tlv = NULL;
  int uinlen;

#ifdef TRACE_FUNCTION
  g_print( "v7_server_status\n" );
#endif 

  /* lets skip our own uin */
  uinlen = v7_buffer_get_c (snac->buf, out_err);
  v7_buffer_skip (snac->buf, uinlen, out_err);

  /* Warning level ??? */
  v7_buffer_skip (snac->buf, 2, out_err);

  tlvcount = v7_buffer_get_w_be (snac->buf, out_err);

  for(i=0; i<tlvcount; i++) {
    
    tlv = read_tlv(snac->buf, out_err); 

    switch (tlv->type) {
    case 0x0A:
      conn->ourip = v7_buffer_get_dw_be (tlv->buf, out_err);
    }

    free_tlv(tlv); 
    tlv = NULL;
  }

 out_err:
  g_free (tlv);
  return;
}
