/*****************************
 Icon handling
 Author: Gediminas Paulauskas
 (c) 1999 Jeremy Wise
 GnomeICU
******************************/

#ifndef __ICONS_H__
#define __ICONS_H__

#include "datatype.h"
#include <gtk/gtkwidget.h>

/*** Pixbufs ***/
extern GdkPixbuf *still_eyes_pixbuf;
extern GdkPixbuf *icon_blank_pixbuf;
extern GdkPixbuf *icon_message_pixbuf;
extern GdkPixbuf *icon_url_pixbuf;
extern GdkPixbuf *icon_auth_pixbuf;
extern GdkPixbuf *icon_away_pixbuf;
extern GdkPixbuf *icon_na_pixbuf;
extern GdkPixbuf *icon_occ_pixbuf;
extern GdkPixbuf *icon_dnd_pixbuf;
extern GdkPixbuf *icon_ffc_pixbuf;
extern GdkPixbuf *icon_inv_pixbuf;
extern GdkPixbuf *icon_online_pixbuf;
extern GdkPixbuf *icon_offline_pixbuf;
extern GdkPixbuf *icon_chat_pixbuf;
extern GdkPixbuf *icon_file_pixbuf;
extern GdkPixbuf *icon_info_pixbuf;
extern GdkPixbuf *icon_contact_pixbuf;
extern GdkPixbuf *icon_hist_pixbuf;
extern GdkPixbuf *icon_ok_pixbuf;
extern GdkPixbuf *icon_cancel_pixbuf;
extern GdkPixbuf *icon_rename_pixbuf;
extern GdkPixbuf *icon_group_pixbuf;
extern GdkPixbuf *icon_lists_pixbuf;
extern GdkPixbuf *icon_groups_pixbuf;
extern GdkPixbuf *icon_birthday_pixbuf;

void icon_themes_init (void);
void icon_themes_load (void);
gchar *make_icon_path_theme (const gchar *file_name, const gchar *theme_name);
gchar *make_icon_path (gchar *file_name);

GdkPixbuf *get_pixbuf_for_status (DWORD status);

GdkPixbuf *get_pixbuf_for_message (gint type);

void set_window_icon (GtkWidget *window, gchar *icon_name);
GtkWidget* make_button_with_pixmap (const char *label_text, const char *pixmap_name);

#endif /* __ICONS_H__ */
