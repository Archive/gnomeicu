/****************************
  Spinner control
  Olivier Crete (c) 2002
  GnomeICU
*****************************/



#include "gnomeicu-spinner.h"
#include "gnomeicu.h"
#include "icons.h"


#include <gtk/gtk.h>

GdkPixbufAnimation *animation = NULL;
GdkPixbuf *stopped = NULL;
gint animationcount = 0;

void gnomeicu_spinner_init(void)
{

  GtkWidget *spinner = glade_xml_get_widget (MainData->xml, "animation");
  gchar *filename;

#ifdef TRACE_FUNCTION
  g_print("gnomeicu_spinner_init\n");
#endif

  filename = make_icon_path ("gnomeicu-animation.gif");

  if (!filename || !spinner)
    return;

  stopped = gdk_pixbuf_new_from_file(filename, NULL);

  g_free (filename);

  if (!stopped)
    return;

  gtk_image_set_from_pixbuf(GTK_IMAGE(spinner), stopped);

}

void gnomeicu_spinner_reload(void)
{

#ifdef TRACE_FUNCTION
  g_print("gnomeicu_spinner_reload\n");
#endif

  if (animation) {
    g_object_unref(G_OBJECT(animation));
    animation = NULL;
  }
  if (stopped) {
    g_object_unref(G_OBJECT(stopped));
    stopped = NULL;
  }

  if (animationcount) {
    gnomeicu_spinner_start();
    animationcount--;
  } else {
    gnomeicu_spinner_stop();
  }

}


void gnomeicu_spinner_start (void)
{

  GtkWidget *spinner = glade_xml_get_widget (MainData->xml, "animation");
  gchar *filename;

#ifdef TRACE_FUNCTION
  g_print("gnomeicu_spinner_start\n");
#endif

  animationcount++;

  filename = make_icon_path ("gnomeicu-animation.gif");
	
  if (!filename || !spinner)
    return;

  if (!animation)
    animation = gdk_pixbuf_animation_new_from_file(filename, NULL);

  g_free (filename);

  if (!animation)
    return;

  gtk_image_set_from_animation(GTK_IMAGE(spinner), animation);
}

void gnomeicu_spinner_stop (void)
{

  GtkWidget *spinner = glade_xml_get_widget (MainData->xml, "animation");
  gchar *filename;

#ifdef TRACE_FUNCTION
  g_print("gnomeicu_spinner_stop\n");
#endif
  
  if (animationcount > 0)  /* to make sure not to have a negative count */
    animationcount--;

  if(animationcount)
    return;

  filename = make_icon_path ("gnomeicu-animation.gif");
	
  if (!filename || !spinner)
    return;

  if (!stopped)
    stopped = gdk_pixbuf_new_from_file(filename, NULL);

  g_free (filename);

  if (!stopped)
    return;

  gtk_image_set_from_pixbuf(GTK_IMAGE(spinner), stopped);
}
