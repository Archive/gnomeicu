/**********************************
 Send various packets to the server
 (c) 1999 Jeremy Wise
 GnomeICU
************************************/

#include "response.h"
#include "common.h"
#include "events.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "history.h"
#include "icons.h"
#include "msg.h"
#include "rtf-reader.h"
#include "showlist.h"
#include "tcp.h"
#include "tray.h"
#include "util.h"
#include "v7send.h"
#include "gnomecfg.h"

#include <gtk/gtk.h>
#include <string.h>
#include <unistd.h>

void awaymsg_change_show( GtkWidget *widget, CONTACT_PTR contact );

/* Called when user goes Offline */
void User_Offline( GSList *contact )
{

#ifdef TRACE_FUNCTION
	g_print( "User_Offline\n" );
#endif

	g_assert(contact != NULL);

	set_contact_status (contact, STATUS_OFFLINE, FALSE);
	if( kontakt->sok )
	{
		if( kontakt->gioc )
			g_io_channel_shutdown( kontakt->gioc, FALSE, NULL );
		close( kontakt->sok );
		g_source_remove( kontakt->giocw );
		kontakt->gioc = 0;
		kontakt->sok = 0;
	}

	kontakt->last_time = time( NULL );
	kontakt->have_tcp_connection = NO;

	gnomeicu_event (EV_USEROFF, contact);

	gnomeicu_tree_user_update (kontakt);

	tray_update();

	kontakt->last_seen = time(NULL);
	kontakt->online_since = 0;
	kontakt->idle_since = 0;
	Save_RC();

}

void User_Online(GSList *contact, gboolean startup)
{
#ifdef TRACE_FUNCTION
	g_print( "User_Online (%s/%s)\n", kontakt->uin, kontakt->nick );
#endif

	g_assert(contact != NULL);

	if( /* enable_online_events && */ kontakt->online_notify &&
                !kontakt->ignore_list) 
	{
		GtkWidget *dialog;
		dialog = gtk_message_dialog_new (GTK_WINDOW (MainData->window),
		                                 GTK_DIALOG_DESTROY_WITH_PARENT,
		                                 GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
		                                 _("%s is online"),
		                                 kontakt->nick);
		g_signal_connect (G_OBJECT (dialog), "response",
		                  G_CALLBACK (gtk_widget_destroy), NULL);
		gtk_widget_show (dialog);
	}

	if (!startup)
	  gnomeicu_event (EV_USERON, contact);
	    
	tray_update();

}

void Do_File( const char *msg, UIN_T uin, const gchar *msgid, 
	      const gchar *file_name, 
	      int file_size , V7Connection *conn, int downcounter)
{
	GSList *contact;

	STORED_MESSAGE_PTR new_stored_message = NULL;

	XferInfo *xferinfo;

#ifdef TRACE_FUNCTION
	g_print( "Do_File\n" );
#endif

	contact = Contacts;

	if( strcmp(uin, "0") )
	{
		contact = Find_User( uin );

		if( contact == NULL ) {
		  if( toggles->no_new_users )
		    return;
		  contact = Add_User( uin, NULL, FALSE );
		  kontakt->confirmed = FALSE;
		} else {
		  if( kontakt->ignore_list )
		    return;
		}

		/* Add to personal history file */
		history_add_incoming( uin, msg, NULL );
		xferinfo = ft_new(kontakt, XFER_DIRECTION_RECEIVING,
				  g_strdup(file_name), file_size);

		xferinfo->conn = conn;
		xferinfo->msg = g_strdup(msg);
		xferinfo->msgid = g_memdup (msgid, 8);
		xferinfo->downcounter = downcounter;

		if (!preferences_get_bool (PREFS_GNOMEICU_AUTO_ACCEPT_FILE)) {
			new_stored_message = g_new0( stored_message, 1 );
			new_stored_message->type = MESSAGE_FILE_REQ;
			new_stored_message->data = xferinfo;
			new_stored_message->message = g_strdup( msg );
			kontakt->stored_messages = g_slist_append( kontakt->stored_messages, new_stored_message );
		} else {
		  v7_acceptfile(xferinfo);
		}

		gnomeicu_event (EV_FILEREQ, contact);
	}
}

void msg_received (UIN_T uin, gchar *msg, time_t msg_time)
{
  GSList *contact = NULL;
  STORED_MESSAGE_PTR new_stored_message;
  gchar *plaintext;

#ifdef TRACE_FUNCTION
  g_print( "msg_received\n" );
#endif
  if( !strcmp(uin, "0") )
    return;

  /* if we receive a "pong" from the server, flag the connection as alive */
	
  if(!strncmp(uin, our_info->uin, strlen(our_info->uin)) &&
     !strncmp(msg, "GnomeICU server ping!",21)) {
    connection_alive = TRUE;
    return;
  }

  contact = Find_User( uin );
	
  if( contact == NULL )
    {
      if( toggles->no_new_users )
	return;
      contact = Add_User( uin, NULL, FALSE );
      kontakt->confirmed = FALSE;
    }
  else
    {
      if( kontakt->ignore_list )
	return;
    }

  if ( strncmp( msg, "{\\rtf1", 6) ) {
    if (g_utf8_validate(msg, -1, NULL))
      plaintext = g_strdup(msg);
    else
      plaintext = convert_to_utf8(msg);
    
    if (!plaintext) {
      g_warning("Can't convert! Copying directly\n");
      plaintext = g_strdup(msg);
    } 
  } else
    plaintext = rtf_parse_only(msg);

	
  if (kontakt->confirmed)
    gnomeicu_event( EV_MSGRECV, contact );

  history_add_incoming( kontakt->uin, plaintext, &msg_time );
  g_free(plaintext);

  new_stored_message = g_new0( stored_message, 1 );
  new_stored_message->type = MESSAGE_TEXT;
  new_stored_message->time = msg_time;
  new_stored_message->message = g_strdup( msg );

  kontakt->stored_messages = g_slist_append( kontakt->stored_messages, new_stored_message );

  if (preferences_get_bool (PREFS_GNOMEICU_AUTO_POPUP) ||
      ( kontakt->msg_dlg_xml != NULL &&
	GTK_WIDGET_VISIBLE (glade_xml_get_widget (kontakt->msg_dlg_xml, "message_dialog"))))
    show_contact_message (contact->data);

}

void url_received (UIN_T uin, const gchar *url, const gchar *desc, time_t msg_time)
{

  GSList *contact = NULL;
  gchar *histtext;
  STORED_MESSAGE_PTR new_stored_message;

#ifdef TRACE_FUNCTION
  g_print( "url_received\n" );
#endif

  if( !strcmp(uin, "0") )
    return;

  contact = Find_User( uin );

  if( contact == NULL ) {
    if( toggles->no_new_users )
      return;
    contact = Add_User( uin, NULL, FALSE );
    kontakt->confirmed = FALSE;
  } else {
    if( kontakt->ignore_list )
      return;
  }
    
  gnomeicu_event( EV_URLRECV,contact );

  /* Add to personal history file */

  histtext = g_strdup_printf("%s\n%s", url, desc);
  history_add_incoming( uin, histtext, &msg_time );
  g_free(histtext);

  new_stored_message = g_new0( stored_message, 1 );
  new_stored_message->type = MESSAGE_URL;
  new_stored_message->time = msg_time;
  new_stored_message->message = g_strdup(desc);
  new_stored_message->data = g_strdup(url);

  kontakt->stored_messages = g_slist_append( kontakt->stored_messages, new_stored_message );
 
}

void contact_list_received (UIN_T uin, GList *contacts, time_t msg_time)
{
  GSList *contact = NULL;
  STORED_MESSAGE_PTR new_stored_message;

#ifdef TRACE_FUNCTION
  g_print( "contact_list_received\n" );
#endif

 
  if( !strcmp(uin, "0") )
    return;

  contact = Find_User( uin );

  if( contact == NULL ) {
    if( toggles->no_new_users )
      return;
    contact = Add_User( uin, NULL, FALSE );
    kontakt->confirmed = FALSE;
  } else {
    if( kontakt->ignore_list )
      return;
  }

  gnomeicu_event( EV_CONTLIST, contact );
  
  new_stored_message = g_new0( stored_message, 1 );
  new_stored_message->type = MESSAGE_CONT_LIST;
  new_stored_message->data = contacts;
  
  kontakt->stored_messages = g_slist_append( kontakt->stored_messages, new_stored_message );
}

void user_added_you (UIN_T uin, time_t msg_time)
{

  GSList *contact = NULL;
  STORED_MESSAGE_PTR new_stored_message;

#ifdef TRACE_FUNCTION
  g_print( "user_added_you\n" );
#endif


  if( !strcmp(uin, "0") )
    return;

  contact = Find_User( uin );

  if( contact == NULL ) {
    if( toggles->no_new_users )
      return;
    contact = Add_User( uin, NULL, FALSE );
    kontakt->confirmed = FALSE;
  } else {
    if( kontakt->ignore_list )
      return;
  }

  gnomeicu_event( EV_LISTADD,contact );
  
  history_add_incoming( uin, "User added you to contact list", &msg_time );
  
    new_stored_message = g_new0( stored_message, 1 );
    new_stored_message->type = MESSAGE_USER_ADD;
    new_stored_message->message = g_strdup( "User has added you" );
    
    kontakt->stored_messages = g_slist_append( kontakt->stored_messages,
                                               new_stored_message );
}

void authorization_request(UIN_T uin, gchar *msg, time_t msg_time)
{

  GSList *contact = NULL;
  STORED_MESSAGE_PTR new_stored_message;
  gchar *tempmsg;
  
#ifdef TRACE_FUNCTION
  g_print( "authorization_request\n" );
#endif

  if( !strcmp(uin, "0") )
    return;

  contact = Find_User( uin );

  if( contact == NULL ) {
    if( toggles->no_new_users )
      return;
    contact = Add_User( uin, NULL, FALSE );
    kontakt->confirmed = FALSE;
  } else {
    if( kontakt->ignore_list )
      return;
  }

  gnomeicu_event( EV_AUTHREQ,contact );

  tempmsg = g_strdup_printf(_("User asked for authorization:\n%s"), msg);
  history_add_incoming( uin, tempmsg, &msg_time );
  g_free(tempmsg);
  
  new_stored_message = g_new0( stored_message, 1 );
  new_stored_message->type = MESSAGE_AUTH_REQ;
  new_stored_message->message = g_strdup_printf( "User asked for auth:\n%s", msg);

  kontakt->stored_messages = g_slist_append( kontakt->stored_messages,
					     new_stored_message );
}

void recv_awaymsg( UIN_T uin, DWORD status, const gchar *message)
{

  gchar *away_title;
  gchar *away_wtitle;
  gchar *string;
  GtkWidget *window;
  GtkWidget *vbox;
  GtkWidget *label9;
  GtkWidget *scrolledwindow2;
  GtkWidget *text2;

  GSList *contact;

#ifdef TRACE_FUNCTION
  g_print( "recv_awaymsg\n" );
#endif

  contact = Find_User( uin );
  if( contact == NULL )
    return;

  switch( status ) {
    case STATUS_AWAY:
      away_title = g_strdup_printf( _("User %s is Away:"), kontakt->nick );
      break;
    case STATUS_NA:
      away_title = g_strdup_printf( _("User %s is Not Available:"), kontakt->nick );
      break;
    case STATUS_DND:
      away_title = g_strdup_printf( _("User %s cannot be disturbed:"), kontakt->nick );
      break;
    case STATUS_OCCUPIED:
      away_title = g_strdup_printf( _("User %s is Occupied:"), kontakt->nick );
      break;
    case STATUS_FREE_CHAT:
      away_title = g_strdup_printf( _("User %s is Free for Chat:"), kontakt->nick );
      break;
    default:
      away_title = g_strdup( _("User is in an unknown mode:") );
    }

  away_wtitle = g_strdup_printf(_("Away Message: %s"), kontakt->nick);

  window = gtk_dialog_new_with_buttons (away_wtitle,
                                        GTK_WINDOW (MainData->window),
                                        GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR, 
                                        GTK_STOCK_OK, GTK_RESPONSE_OK,
                                        NULL);

  gtk_window_set_default_size(GTK_WINDOW(window), 350, 250);

  gtk_dialog_set_default_response (GTK_DIALOG (window), GTK_RESPONSE_OK);

  gtk_window_set_role (GTK_WINDOW (window), "GnomeICU_AwayMessage");
  gtk_container_set_border_width (GTK_CONTAINER (window), 6);

  vbox = gtk_vbox_new (FALSE, 6);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (window)->vbox), vbox);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_widget_show (vbox);

  string = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n%s",
                            _("Away Message"), away_title);
  label9 = gtk_label_new (NULL);
  gtk_widget_show (label9);
  gtk_box_pack_start (GTK_BOX (vbox), label9, FALSE, FALSE, 0);
  gtk_label_set_markup (GTK_LABEL (label9), string);
  gtk_misc_set_alignment (GTK_MISC (label9), 0, 0.5);
  g_free (away_title);
  g_free (string);

  scrolledwindow2 = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_show (scrolledwindow2);
  gtk_box_pack_start (GTK_BOX (vbox), scrolledwindow2, TRUE, TRUE, 0);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow2), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolledwindow2), GTK_SHADOW_IN);

  text2 = gtk_text_view_new ();
  gtk_widget_show (text2);
  gtk_text_view_set_wrap_mode( GTK_TEXT_VIEW( text2 ), GTK_WRAP_WORD );
  gtk_text_view_set_editable( GTK_TEXT_VIEW( text2 ), FALSE );
  gtk_text_view_set_cursor_visible( GTK_TEXT_VIEW( text2 ), FALSE );
  gtk_container_add (GTK_CONTAINER (scrolledwindow2), text2);

  if( message != NULL && message[0] != '\0' ) {
      gtk_text_buffer_insert_at_cursor( gtk_text_view_get_buffer( GTK_TEXT_VIEW(text2) ), 
					message, -1 );
  }

  g_signal_connect (G_OBJECT (window), "response",
                    G_CALLBACK (gtk_widget_destroy), NULL);

  gtk_widget_show (window);
}


void start_transfer( UIN_T uin, const gchar *msgid, DWORD fileip,
		     WORD fileport)
{

  GSList *contact;
  GSList *xfer;
  XferInfo *transfer;

#ifdef TRACE_FUNCTION
  g_print( "start_transfer\n" );
#endif

  contact = Find_User( uin );
  if( contact == NULL )
    return;


  for(xfer = kontakt->file_xfers;xfer;xfer = xfer->next)
    if ( !memcmp(msgid, ((XferInfo*)xfer->data)->msgid, 8))
      break;

  if (memcmp(msgid, ((XferInfo*)xfer->data)->msgid, 8)) {
    g_warning("Some is trying to get a file that we didnt send\n");
    return;
  }
  if (xfer) {
    if 	 ( ((XferInfo*)xfer->data)->direction == XFER_DIRECTION_SENDING) {
      
      transfer = xfer->data;
      
      kontakt->file_xfers = g_slist_remove(kontakt->file_xfers, transfer);

      if( fileport > 0 && fileip != 0) {
	transfer->port = fileport;
	kontakt->current_ip = fileip;
	if (ft_connectfile( transfer ) == -1)
	  ft_cancel_transfer(0, transfer);
      }
    }
  }
}

void set_contact_status (GSList *contact, int status, gboolean startup)
{
	DWORD last_status;

#ifdef TRACE_FUNCTION
	g_print ("set_contact_status\n");
#endif

	last_status = kontakt->status;
	kontakt->status = status;

	if (last_status == STATUS_OFFLINE &&
	    kontakt->status != STATUS_OFFLINE)
	  User_Online (contact, startup);

	kontakt->last_seen = time(NULL);
	Save_RC();
	
	gnomeicu_tree_user_update(kontakt);

	
}

void auth_reply(UIN_T uin, int flag, char *reason, time_t msg_time)
{
  GtkWidget *dialog;
  GSList *contact;
  char *tempmsg;
  STORED_MESSAGE_PTR new_stored_message;

  contact = Find_User (uin);

  if (!contact)
    return;
  
  if (flag) {
    g_print("No longer waiting for auth\n");
    kontakt->wait_auth = FALSE;
    gnomeicu_tree_user_update (kontakt);
  } 
  
  

  gnomeicu_event( EV_AUTH,contact );

  if (flag)
    tempmsg = g_strdup_printf(_("%s (%s) has granted you authorization to add him to your contact list%s%s"),
			      kontakt->nick, kontakt->uin, 
			      reason ? ": " : "",
			      reason ? reason : "" );
  else
    tempmsg = g_strdup_printf(_("%s (%s) has denied you authorization to add him to your contact list%s%s"),
			      kontakt->nick, kontakt->uin,
			      reason ? ": " : "",
			      reason ? reason : "" );


  history_add_incoming( uin, tempmsg, &msg_time );
  g_free(tempmsg);

  new_stored_message = g_new0( stored_message, 1 );
  new_stored_message->type = MESSAGE_USER_AUTHD;
  new_stored_message->message = g_strdup(reason);
  new_stored_message->data = GINT_TO_POINTER(flag);
  

  kontakt->stored_messages = g_slist_append( kontakt->stored_messages,
					     new_stored_message );

}


void user_group_changed(UIN_T uin, guint gid, guint uid)
{
  GSList *contact = Find_User(uin);

  if (!contact)
    return;

  kontakt->gid = gid;
  kontakt->uid = uid;
  gnomeicu_tree_user_update (kontakt);

}
