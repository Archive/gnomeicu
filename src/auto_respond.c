/* GnomeICU
 * Copyright (C) 1998-2003 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * Manage auto respond related functions
 *
 * Copyright (C) 2002-2003 Patrick Sung
 */

#include "common.h"

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include "gnomeicu.h"
#include "util.h"
#include "v7send.h"

#include <X11/Xlib.h>
#ifdef USE_XSCREENSAVER
# include <X11/Xutil.h>
# include <X11/extensions/scrnsaver.h>
#endif

static void
auto_respond_msg_changed_cb (GtkOptionMenu *o, GtkTextView *tv)
{
  GtkTextBuffer *tb;
  gint num;
  gchar *key, *str;

  g_assert (GTK_IS_TEXT_VIEW (tv));

  num = gtk_option_menu_get_history (o) + 1;
  key = g_strdup_printf (PREFS_GNOMEICU_AWAY_MSG "%d", num);
  str = preferences_get_string (key);
  tb = gtk_text_view_get_buffer (tv);

  gtk_text_buffer_set_text (tb, str, -1);

  g_free (key);
  g_free (str);
}

static void
auto_respond_dialog_response_cb (GtkWidget *dialog, gint id, GtkTextView *tv)
{
  GtkTextBuffer *tb;
  GtkTextIter startIter, endIter;
  gchar *msg;

  g_assert (GTK_IS_TEXT_VIEW (tv));

  tb = gtk_text_view_get_buffer (tv);

  gtk_text_buffer_get_start_iter (tb, &startIter);
  gtk_text_buffer_get_end_iter (tb, &endIter);
  msg = gtk_text_buffer_get_text (tb, &startIter, &endIter, FALSE);

  g_free(Away_Message);
  Away_Message = g_strdup (msg);
  g_free (msg);

  if (Current_Status != STATUS_OFFLINE)
    v7_aimsetstatus (mainconnection, Away_Message);

  gtk_widget_destroy (dialog);
}

static gboolean
auto_respond_textview_key_press_cb (GtkWidget *textview, GdkEventKey *event,
                                    gpointer dialog)
{
  if ((event->state & GDK_CONTROL_MASK) && (event->keyval == GDK_Return)) {
    g_assert (GTK_IS_WIDGET (dialog) && GTK_IS_TEXT_VIEW (textview));
    auto_respond_dialog_response_cb (dialog, 0, GTK_TEXT_VIEW(textview));
    return TRUE;
  }
  return FALSE;
}

void
auto_respond_pick_msg_window_open (void)
{
  GladeXML *gxml;
  GtkWidget *dialog;
  GtkWidget *textview;
  GtkWidget *optionmenu, *menu;
  GtkWidget *msg_item;
  GtkTextBuffer *textbuf;
  GSList *list, *tmp;
  gchar *key, *str;

  gxml = gicu_util_open_glade_xml ("auto_respond.glade", "auto_respond_dialog");
  if (gxml == NULL)
    return;

  dialog = glade_xml_get_widget (gxml, "auto_respond_dialog");

  textview = glade_xml_get_widget (gxml, "auto_respond_textview");
  textbuf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview));

  gtk_text_buffer_insert_at_cursor (textbuf, Away_Message, -1);

  /* create the menu for Option Menu */
  menu = gtk_menu_new ();

  list = preferences_get_list (PREFS_GNOMEICU_AWAY_MSG_HEADINGS,
                               GCONF_VALUE_STRING);
  for (tmp = list; tmp != NULL; tmp = tmp->next) {
    if (tmp->data != NULL) {
      msg_item = gtk_menu_item_new_with_label (tmp->data);
      gtk_widget_show (msg_item);

      gtk_menu_shell_append (GTK_MENU_SHELL (menu), msg_item);
    }
  }
  gtk_widget_show (menu);

  optionmenu = glade_xml_get_widget (gxml, "auto_respond_optionmenu");
  gtk_option_menu_set_menu (GTK_OPTION_MENU (optionmenu), menu);


  g_signal_connect (G_OBJECT (optionmenu), "changed",
                    G_CALLBACK (auto_respond_msg_changed_cb), textview);

  g_signal_connect (G_OBJECT (textview), "key_press_event",
                    G_CALLBACK (auto_respond_textview_key_press_cb),
                    (gpointer)dialog);

  g_signal_connect (G_OBJECT (dialog), "response",
                    G_CALLBACK (auto_respond_dialog_response_cb),
                    (gpointer)textview);


  glade_xml_signal_autoconnect (gxml);
  g_object_unref (gxml);
}

static guint
auto_respond_timeout (void)
{
  static Display *display = NULL;
  /* This var is 0x9999 when not in auto away mode, 
     When in auto-away, it is the status that it was before going away
  */
  static int old_status = 0x9999;
  int status;
  int time_idle = 0;
  static unsigned long prev_idle = 0;
  static time_t last_idle_signal;

#ifdef USE_XSCREENSAVER
  static XScreenSaverInfo *info = NULL;
  int event_base, error_base; /* for XScreenSaverQuery */
#else
  static Window w1, w2, root;
  static int x = -1, y = -1, x1, y1, x2, y2, b;
  unsigned long last_idle = 0;
#endif

  if (!preferences_get_bool (PREFS_GNOMEICU_AUTO_AWAY))
    return TRUE;

  if (display == NULL)
    display = XOpenDisplay (gdk_get_display ());

  status = Current_Status & 0xffff;

  /* We return if we are not in an online state */
  if ( status != STATUS_ONLINE && status != STATUS_AWAY &&
       status != STATUS_FREE_CHAT )
    return TRUE;

  /* We return if we are away, but not auto-away */
  if (status == STATUS_AWAY && old_status == 0x9999)
    return TRUE;

#ifdef USE_XSCREENSAVER
  if (XScreenSaverQueryExtension(display, &event_base, &error_base) ) {
    if (info == NULL)
      info = XScreenSaverAllocInfo();
    XScreenSaverQueryInfo (display, DefaultRootWindow (display), info);
    time_idle = info->idle;
  }
#else
  if ( x == -1 && y == -1 ) {
    last_idle = time(0);
    root = RootWindow (display, DefaultScreen (display));
  }

  XQueryPointer (display, root, &w1, &w2, &x1, &y1, &x2, &y2, (uint *)&b);
  if ( !( x1 == x && y1 == y ) ) {
    x = x1;
    y = y1;
    last_idle = time (0);
  }
	
  time_idle = time (0) - last_idle;
  time_idle = time_idle < 0 ? 0 : time_idle * 1000;
#endif

  if (last_idle_signal + 5*60 > time(NULL))
    if (time_idle < 60*1000 && prev_idle > 60*1000) {
      v7_send_idletime(mainconnection, 0);
      last_idle_signal = time(NULL);
    }
    else if ((time_idle > 2*60*1000 && prev_idle < 60*1000) ||
	     (time_idle < prev_idle)) {
      v7_send_idletime(mainconnection, time_idle);
      last_idle_signal = time(NULL);
      }
      
  if (time_idle > 60*1000 * preferences_get_int (PREFS_GNOMEICU_AWAY_TIMEOUT)) {
    if (old_status == 0x9999)
      old_status = Current_Status;
    if (status != STATUS_AWAY) {
      /* uto away will use default away msg set from gconf */
      gchar *key, *str;
      key = g_strdup_printf (PREFS_GNOMEICU_AWAY_MSG "%d",
                             preferences_get_int (PREFS_GNOMEICU_AWAY_DEFAULT));
      str = preferences_get_string (key);
      g_free (Away_Message);
      Away_Message = g_strdup(str);

      v7_setstatus (mainconnection, STATUS_AWAY);

      g_free (key);
      g_free (str);
    }
  } else {
    if (old_status != 0x9999)
      v7_setstatus (mainconnection, old_status);
    old_status = 0x9999;
  }

  prev_idle = time_idle;

  return TRUE;
}

void
auto_respond_init (void)
{
  static guint timeout_id = 0;

  if (timeout_id == 0)
    timeout_id = g_timeout_add (3000, (GSourceFunc)auto_respond_timeout, NULL);
}

