#include "common.h"
#include "changenick.h"
#include "detach.h"
#include "gnomecfg.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "history.h"
#include "icons.h"
#include "msg.h"
#include "personal_info.h"
#include "response.h"
#include "sendcontact.h"
#include "showlist.h"
#include "tcp.h"
#include "user_popup.h"
#include "v7send.h"
#include "v7snac13.h"
#include "groups.h"
#include "grpmgr.h"
#include "auth.h"
#include "util.h"

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <string.h>

static void icq_sendmessage_window_from_menu( GtkWidget *widget, gpointer data );
static void send_url_window_default( GtkWidget *widget, gpointer data );
static void get_contact_list (GtkWidget *widget, gpointer data);
static void request_file_from_menu (GtkWidget *widget, gpointer data);
static void retrieve_away_message( GtkWidget *widget, gpointer data );
static void add_user_to_list( GtkWidget *widget, gpointer data );
static void request_auth (GtkWidget *widget, Contact_Member *data);
static void remove_user_question( GtkWidget *widget, gpointer data );
static void lists_change_list_ignore (GtkWidget *widget, gpointer data);
static void lists_change_list_visible (GtkWidget *widget, gpointer data);
static void lists_change_list_invisible (GtkWidget *widget, gpointer data);
static void lists_change_list_notify (GtkWidget *widget, gpointer data);
static void lists_change_list( GtkWidget *widget, gpointer data, gint type );
static void add_files( GtkWidget *widget, gpointer data);
static void remove_files( GtkWidget *widget, gpointer data);
static void remove_user (GtkDialog *dialog, int response, gpointer data);
static void user_change_group (GtkWidget *widget, GroupInfo *data);


GtkWidget *user_popup (Contact_Member *contact)
{
	GtkWidget *personal_menu, *lists_menu;
	GtkWidget *item, *image;
	GSList *ginfo;

	g_assert (contact != NULL);

	personal_menu = gtk_menu_new ();

	item = gtk_image_menu_item_new_with_label (contact->uin);
	gtk_container_add (GTK_CONTAINER (personal_menu), item);
	/*	g_signal_connect (G_OBJECT (item), "activate",
	                    G_CALLBACK (dump_contact_details),
	                    contact); */

	item = gtk_separator_menu_item_new ();
	gtk_container_add (GTK_CONTAINER (personal_menu), item);

	item = gtk_image_menu_item_new_with_mnemonic (_("_Message..."));
	image = gtk_image_new_from_pixbuf (icon_message_pixbuf);
	gtk_container_add (GTK_CONTAINER (personal_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_sendmessage_window_from_menu),
	                  contact);

	item = gtk_image_menu_item_new_with_mnemonic (_("_URL..."));
	image = gtk_image_new_from_pixbuf (icon_url_pixbuf);
	gtk_container_add (GTK_CONTAINER (personal_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (send_url_window_default), contact);

	item = gtk_image_menu_item_new_with_mnemonic (_("_Contact List..."));
	image = gtk_image_new_from_pixbuf (icon_contact_pixbuf);
	gtk_container_add (GTK_CONTAINER (personal_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (get_contact_list), contact);

#if 1 /* FXFER */
        /* file transfer */
	if (contact->used_version >= 8) {
		item = gtk_image_menu_item_new_with_mnemonic (_("Send _File..."));
		image = gtk_image_new_from_pixbuf (icon_file_pixbuf);
		gtk_container_add (GTK_CONTAINER (personal_menu), item);
		gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
		gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE);
		g_signal_connect (G_OBJECT (item), "activate",
		                  G_CALLBACK (request_file_from_menu), contact);
	}

	item = gtk_separator_menu_item_new ();
	gtk_container_add (GTK_CONTAINER (personal_menu), item);
#endif

        /* user info */
	item = gtk_image_menu_item_new_with_mnemonic (_("User _Info"));
	image = gtk_image_new_from_pixbuf (icon_info_pixbuf);
	gtk_container_add (GTK_CONTAINER (personal_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (show_personal_info), contact);

	if ((contact->status & 0xffff) != STATUS_ONLINE &&
	    contact->status != STATUS_OFFLINE &&
	    (contact->status & 0xffff) != STATUS_INVISIBLE &&
	    (contact->status & 0xffff) != STATUS_FREE_CHAT) {

		switch (contact->status & 0xffff) {
		case STATUS_AWAY:
			item = gtk_image_menu_item_new_with_mnemonic (_("_Read Away Message"));
			break;
		case STATUS_NA:
			item = gtk_image_menu_item_new_with_mnemonic (_("_Read Not Available Message"));
			break;
		case STATUS_OCCUPIED:
			item = gtk_image_menu_item_new_with_mnemonic (_("_Read Occupied Message"));
			break;
		case STATUS_DND:
			item = gtk_image_menu_item_new_with_mnemonic (_("_Read Do Not Disturb Message"));
			break;
		default:
			item = gtk_image_menu_item_new_with_mnemonic (_("_Read Message for Unknown Status"));
			break;
		}

		image = gtk_image_new_from_pixbuf (get_pixbuf_for_status (contact->status));
		gtk_container_add (GTK_CONTAINER (personal_menu), item);
		gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
		g_signal_connect (G_OBJECT (item), "activate",
		                  G_CALLBACK (retrieve_away_message),
		                  contact);
	}

        /* history */
	item = gtk_image_menu_item_new_with_mnemonic (_("_History"));
	image = gtk_image_new_from_pixbuf (icon_hist_pixbuf);
	gtk_container_add (GTK_CONTAINER (personal_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (history_display), contact);

	/* Lists menu */
	lists_menu = gtk_menu_new ();

	/* On ignore list... */
	item = gtk_check_menu_item_new_with_mnemonic (_("On _Ignore List"));
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (item),
					contact->ignore_list);
	gtk_check_menu_item_set_show_toggle (GTK_CHECK_MENU_ITEM (item), TRUE);
	gtk_container_add (GTK_CONTAINER (lists_menu), item);
	gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE);
	g_signal_connect (G_OBJECT (item), "toggled",
	                  G_CALLBACK (lists_change_list_ignore),
	                  contact);

	/* On visible list... */
	item = gtk_check_menu_item_new_with_mnemonic (_("On _Visible List"));
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (item),
					contact->vis_list);
	gtk_check_menu_item_set_show_toggle (GTK_CHECK_MENU_ITEM (item), TRUE);
	gtk_container_add (GTK_CONTAINER (lists_menu), item);
	gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE);
	g_signal_connect (G_OBJECT (item), "toggled",
	                  G_CALLBACK (lists_change_list_visible),
	                  contact);

	/* On invisible list... */
	item = gtk_check_menu_item_new_with_mnemonic (_("On Invi_sible List"));
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (item),
					contact->invis_list);
	gtk_check_menu_item_set_show_toggle (GTK_CHECK_MENU_ITEM (item), TRUE);
	gtk_container_add (GTK_CONTAINER (lists_menu), item);
	gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE);
	g_signal_connect (G_OBJECT (item), "toggled",
	                  G_CALLBACK (lists_change_list_invisible),
	                  contact);

	/* On online notify list... */
	item = gtk_check_menu_item_new_with_mnemonic (_("On _Notify List"));
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (item),
					contact->online_notify);
	gtk_check_menu_item_set_show_toggle (GTK_CHECK_MENU_ITEM (item), TRUE);
	gtk_container_add (GTK_CONTAINER (lists_menu), item);
	g_signal_connect (G_OBJECT (item), "toggled",
	                  G_CALLBACK (lists_change_list_notify),
	                  contact);

	/* End Lists Menu */

	item = gtk_image_menu_item_new_with_mnemonic (_("_Lists"));
	image = gtk_image_new_from_pixbuf (icon_lists_pixbuf);
	gtk_container_add (GTK_CONTAINER (personal_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), lists_menu);
	gtk_widget_show_all (lists_menu);

	item = gtk_separator_menu_item_new ();
	gtk_container_add (GTK_CONTAINER (personal_menu), item);

	/* Detach/Attach user */
	item = gtk_check_menu_item_new_with_mnemonic (_("Show _Shortcut"));

	if (GTK_IS_WIDGET (contact->detached_window)
	    && GTK_WIDGET_VISIBLE (contact->detached_window))
		gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (item), TRUE);
	else
		gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (item), FALSE);

	gtk_container_add (GTK_CONTAINER (personal_menu), item);

	if (GTK_IS_WIDGET (contact->detached_window))
		g_signal_connect (G_OBJECT (item), "activate",
		                  G_CALLBACK (detach_contact_hide),
		                  contact);
	else
		g_signal_connect (G_OBJECT (item), "activate",
		                  G_CALLBACK (detach_contact), contact);


	/* Separator */
	item = gtk_separator_menu_item_new ();
	gtk_container_add (GTK_CONTAINER (personal_menu), item);

	if (contact->inlist == FALSE) {
          /* Add to group menu */
	  lists_menu = gtk_menu_new ();

	  for (ginfo = Groups; ginfo != NULL; ginfo = ginfo->next) {
	    
	    item = gtk_menu_item_new_with_label (((GroupInfo *)ginfo->data)->name);
	    g_object_set_data(G_OBJECT(item), "contact", contact);
	    gtk_container_add (GTK_CONTAINER (lists_menu), item);
	    g_signal_connect (G_OBJECT (item), "activate",
			      G_CALLBACK (add_user_to_list),
			      GUINT_TO_POINTER((guint)((GroupInfo *)ginfo->data)->gid));
	    g_object_set_data (G_OBJECT (item), "contact", contact);
	  }

	  item = gtk_image_menu_item_new_with_mnemonic (_("_Add to Group"));
	  image = gtk_image_new_from_pixbuf (icon_groups_pixbuf);
	  gtk_container_add (GTK_CONTAINER (personal_menu), item);
	  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	  gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE);
	  gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), lists_menu);

	} else {
	  
	  /* Change to group menu */
	  lists_menu = gtk_menu_new ();

	  for (ginfo = Groups; ginfo != NULL; ginfo = ginfo->next) {
	    if (contact->gid == ((GroupInfo *)(ginfo->data))->gid)
	      continue;

	    item = gtk_menu_item_new_with_label (((GroupInfo *)(ginfo->data))->name);
	    gtk_container_add (GTK_CONTAINER (lists_menu), item);
	    g_signal_connect (G_OBJECT (item), "activate",
			      G_CALLBACK (user_change_group), ginfo->data);
	    g_object_set_data (G_OBJECT (item), "contact", contact);
	  }

	  /* seperator */
	  item = gtk_separator_menu_item_new ();
	  gtk_container_add (GTK_CONTAINER (lists_menu), item);

	  item = gtk_menu_item_new_with_mnemonic (_("_New Group..."));
	  gtk_container_add (GTK_CONTAINER (lists_menu), item);
	  g_signal_connect (G_OBJECT (item), "activate",
			    G_CALLBACK (grpmgr_window), contact);

	  item = gtk_image_menu_item_new_with_mnemonic (_("Change to _Group"));
	  image = gtk_image_new_from_pixbuf (icon_groups_pixbuf);
	  gtk_container_add (GTK_CONTAINER (personal_menu), item);
	  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	  gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE);
	  gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), lists_menu);

	}

	/* seperator */
        if (contact->inlist || contact->wait_auth) {
          item = gtk_separator_menu_item_new ();
          gtk_container_add (GTK_CONTAINER (personal_menu), item);
        }

        if (contact->wait_auth) {
          item = gtk_image_menu_item_new_with_mnemonic (_("Re-request a_uthorization"));
          image = gtk_image_new_from_pixbuf (icon_auth_pixbuf);
          gtk_container_add (GTK_CONTAINER (personal_menu), item);
          gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
          gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE);
          g_signal_connect (G_OBJECT (item), "activate",
                            G_CALLBACK (request_auth), contact);
        }

        if (contact->inlist) {
          item = gtk_image_menu_item_new_with_mnemonic (_("Re_name User..."));
          image = gtk_image_new_from_pixbuf (icon_rename_pixbuf);
          gtk_container_add (GTK_CONTAINER (personal_menu), item);
          gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
          gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE);
          g_signal_connect (G_OBJECT (item), "activate",
                            G_CALLBACK (change_nick_window), contact);

          item = gtk_image_menu_item_new_with_mnemonic (_("Re_move User"));
          image = gtk_image_new_from_pixbuf (icon_cancel_pixbuf);
          gtk_container_add (GTK_CONTAINER (personal_menu), item);
          gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
          gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE);
          g_signal_connect (G_OBJECT (item), "activate",
                            G_CALLBACK (remove_user_question), contact);
        }

	gtk_widget_show_all (personal_menu);

	return personal_menu;
}

static void icq_sendmessage_window_from_menu( GtkWidget *widget, gpointer data )
{
	GSList *contact;

#ifdef TRACE_FUNCTION
	g_print( "icq_sendmessage_window_from_menu\n" );
#endif

	contact = Contacts;
	while (contact != NULL && contact->data != data)
		contact = contact->next;

	if( contact == NULL )
		return;

	if (g_slist_length (kontakt->stored_messages))
		show_contact_message (kontakt);
	else
		open_message_dlg_with_message (kontakt, NULL);
}

static void send_url_window_default( GtkWidget *widget, gpointer data )
{
	g_object_set_data (G_OBJECT (widget), "contact", data);
	send_url_window( widget, "" );
}

static void get_contact_list( GtkWidget *widget, gpointer data )
{
#ifdef TRACE_FUNCTION
	g_print( "get_contact_list\n" );
#endif

	if( data == NULL )
		return;

	contact_list_window( data );
}

static void request_file_from_menu (GtkWidget *widget, gpointer data)
{
	request_file (widget, NULL, data);
}

void request_file( GtkWidget *widget, GList *files, gpointer data )
{
	GSList *contact;

	GladeXML *dialogxml;
	GtkWidget *dialog;
	GtkWidget *treeview;
	GtkWidget *textview;
	GtkWidget *addbutton;
	GtkWidget *removebutton;

	GtkTextBuffer* text_buffer;
	GtkTextIter start_iter;
	GtkTextIter end_iter;
	gchar *msg;

	GtkListStore *filelist;
	GtkTreeViewColumn *column;
	GtkCellRenderer   *renderer;
	GtkTreeIter iter;
	GtkTreeSelection *selection;

	GSList *filenames = NULL;

	int response;

	gchar *str;


#ifdef TRACE_FUNCTION
	g_print( "request_file\n" );
#endif

	contact = Contacts;
	while (contact != NULL && contact->data != data)
		contact = contact->next;

	if( contact == NULL )
		return;

	dialogxml = gicu_util_open_glade_xml("filexfer.glade",
					     "file_send_dialog");
	if (dialogxml == NULL) {
	  return;
	}

	
	dialog = glade_xml_get_widget(dialogxml, "file_send_dialog");
	treeview = glade_xml_get_widget(dialogxml, "files_treeview");
	addbutton = glade_xml_get_widget(dialogxml, "add_button");
	removebutton = glade_xml_get_widget(dialogxml, "remove_button");
	textview = glade_xml_get_widget(dialogxml, "reason_textview");

	set_window_icon( dialog, "gnomeicu-file.png" );

	str = g_strdup_printf( _("Send File to %s:"), kontakt->nick );
	gtk_window_set_title(GTK_WINDOW(dialog), str);
	g_free(str);


	filelist = gtk_list_store_new(2, G_TYPE_STRING, G_TYPE_POINTER);

	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), GTK_TREE_MODEL(filelist));

	renderer = gtk_cell_renderer_text_new ();

	column = gtk_tree_view_column_new_with_attributes("Filenames",
							  renderer,
							  "text", 0,
							  NULL);

	gtk_tree_view_append_column (GTK_TREE_VIEW(treeview), column);
	
	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));

	gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);


	/* for data passed from DnD */

	for (; files; files = files->next )
        {
            char *rawdata = files->data;
	    char *filename = NULL;
	    char *utf8filename = NULL;

            /* Since its a file, we can strip the protocol type */
            if (strncmp (data, "file:", 5) == 0) {
	      filename = g_filename_from_uri(rawdata, NULL,NULL);
	      if (filename == NULL)
		continue;
	    } else {
	      filename = g_strdup(rawdata);
	    }
	    
	    utf8filename = g_filename_to_utf8(filename, -1,
					      NULL, NULL, NULL);
					      
	    

	    

	    gtk_list_store_append(filelist, &iter);
	    gtk_list_store_set(filelist,
			       0, utf8filename,	
			       1, filename,
			       -1);

	    g_free(utf8filename);
	}

	g_object_set_data (G_OBJECT (dialog), "filelist", filelist);

	g_signal_connect (G_OBJECT (addbutton), "clicked",
			  G_CALLBACK (add_files), dialog);
	g_signal_connect (G_OBJECT (removebutton), "clicked",
			  G_CALLBACK (remove_files), treeview);



	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);


	response = gtk_dialog_run(GTK_DIALOG(dialog));

	if (response = GTK_RESPONSE_OK) {
	  if (contact->data) {
	  
	    text_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	    gtk_text_buffer_get_start_iter(text_buffer, &start_iter);
	    gtk_text_buffer_get_end_iter(text_buffer, &end_iter);
	    msg = gtk_text_buffer_get_text(text_buffer, &start_iter, &end_iter, 
					   FALSE);

	    /*
	     * get filenames
	     */
	    if (gtk_tree_model_get_iter_first(GTK_TREE_MODEL(filelist), &iter)) {

	      do {
		gchar *filename;

		gtk_tree_model_get(GTK_TREE_MODEL(filelist), &iter, 
				   1, &filename,
				   -1);

		filenames = g_slist_prepend(filenames, filename);

	      } while (gtk_tree_model_iter_next(GTK_TREE_MODEL(filelist), &iter));


	      v7_sendfile(mainconnection, kontakt->uin, msg, filenames);
	    }
	  }
	}
	
	g_slist_foreach(filenames, (GFunc)g_free, NULL);
	g_slist_free(filenames);

	gtk_widget_destroy(dialog);

}

static void retrieve_away_message( GtkWidget *widget, gpointer data )
{
	GSList *contact;

#ifdef TRACE_FUNCTION
	g_print( "retrieve_away_message\n" );
#endif

	if (!is_connected(GTK_WINDOW(MainData->window), _("You can not retrieve away messages while disconnected.")))
		return;

	contact = Contacts;
	while (contact != NULL && contact->data != data)
		contact = contact->next;

	if( contact == NULL )
		return;

        /* trillian reports version 0, does it do away messages?*/
        if (kontakt->version >= 8 || kontakt->version == 0)
          v7_request_away_msg(mainconnection, kontakt->uin, kontakt->status);
        else
          TCPRetrieveAwayMessage( contact, NULL );
}

static void add_user_to_list( GtkWidget *widget, gpointer data )
{

        CONTACT_PTR contact;

#ifdef TRACE_FUNCTION
	g_print( "add_user_to_list\n" );
#endif

	if (!is_connected(GTK_WINDOW(MainData->window), _("You can not add a user to a list while disconnected.")))
		return;

	contact = g_object_get_data(G_OBJECT(widget), "contact");
	if( contact == NULL)
		return;

	v7_try_add_uin (mainconnection, contact->uin, contact->nick, GPOINTER_TO_INT(data), FALSE);
  
	Save_RC();
}

static void
request_auth (GtkWidget *widget, Contact_Member *contact)
{
  g_assert (contact != NULL);

  if (!is_connected(GTK_WINDOW(MainData->window),
                    _("You can not authorize a contact while disconnected.")))
    return;

  auth_request_msg_box (contact->nick, contact->uin);
}

static void remove_user_question( GtkWidget *widget, gpointer data )
{
	GtkWidget *dialog;
	GtkWidget *vbox;
	GtkWidget *hbox;	
	GtkWidget *label;
	GtkWidget *image;
	GtkWidget *remhist_button;
	gchar *string, *message;

	GSList *contact;

#ifdef TRACE_FUNCTION
	g_print( "remove_user_question\n" );
#endif

	contact = Contacts;
	while (contact != NULL && contact->data != data)
		contact = contact->next;

	if( contact == NULL )
		return;

	dialog = gtk_dialog_new_with_buttons ("",
	                                      GTK_WINDOW (MainData->window),
	                                      GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR,
	                                      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	                                      _("_Remove"), GTK_RESPONSE_YES,
	                                      NULL);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_YES);
	gtk_window_set_wmclass (GTK_WINDOW (dialog), "Remove_Contact", "GnomeICU");
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (dialog), 6);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 5);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), hbox);

	image = gtk_image_new_from_stock (GTK_STOCK_DIALOG_QUESTION, GTK_ICON_SIZE_DIALOG);
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
	gtk_misc_set_alignment (GTK_MISC (image), 0.5, 0);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 0);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, FALSE, FALSE, 0);

	message = g_strdup_printf (_("Are you sure you wish to remove %s from your contact list?"),kontakt->nick);
	string = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n%s",
	                          _("Remove User?"), message);

	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), string);
	g_free (message);
	g_free (string);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_label_set_selectable (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0.5, 0);

	remhist_button = gtk_check_button_new_with_mnemonic (_("Remove User's _History file"));
	gtk_box_pack_start (GTK_BOX (vbox), remhist_button, FALSE, FALSE, 0);
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( remhist_button ), TRUE );

	g_object_set_data (G_OBJECT (dialog), "contact", contact);

	g_signal_connect (G_OBJECT (dialog), "response",
	                  G_CALLBACK (remove_user),
	                  remhist_button);

	gtk_widget_show_all (dialog);
}

static void lists_change_list_ignore (GtkWidget *widget, gpointer data)
{
	lists_change_list (widget, data, 0);
}

static void lists_change_list_visible (GtkWidget *widget, gpointer data)
{
	lists_change_list (widget, data, 1);
}

static void lists_change_list_invisible (GtkWidget *widget, gpointer data)
{
	lists_change_list (widget, data, 2);
}

static void lists_change_list_notify (GtkWidget *widget, gpointer data)
{
	lists_change_list (widget, data, 3);
}

static void lists_change_list( GtkWidget *widget, gpointer data, gint type )
{
    CONTACT_PTR contact;
    /*gint method = 0;
    GtkWidget *dialog;
    gchar *msg;*/

#ifdef TRACE_FUNCTION
    g_print( "lists_change_lists" );
#endif

    if (!is_connected(GTK_WINDOW(MainData->window), _("You can not change a list while disconnected.")))
        return;
    contact = data;

    switch(type)
    {
    case 0: /* ignore list */
      if ( contact->ignore_list != GTK_CHECK_MENU_ITEM( widget )->active) {
        contact->ignore_list = GTK_CHECK_MENU_ITEM( widget )->active;
        if (contact->ignore_list) {
          v7_ignore (mainconnection, contact->uin,
                     contact->ignorelist_uid = contact_gen_uid (),
                     TRUE); /* add */
        } else {
          v7_ignore (mainconnection, contact->uin, contact->ignorelist_uid,
                     FALSE); /* remove */
        }
      }
      break;
    case 1: /* visible list */
      if ( contact->vis_list != GTK_CHECK_MENU_ITEM( widget )->active) {
        contact->vis_list = GTK_CHECK_MENU_ITEM( widget )->active;
        if (contact->vis_list) {
          v7_visible (mainconnection, contact->uin,
                      contact->vislist_uid = contact_gen_uid (),
                      TRUE); /* add */
        } else {
          v7_visible (mainconnection, contact->uin, contact->vislist_uid,
                      FALSE); /* remove */
        }
      }
      break;
    case 2: /* invisible list */
      if ( contact->invis_list != GTK_CHECK_MENU_ITEM( widget )->active) {
        contact->invis_list = GTK_CHECK_MENU_ITEM( widget )->active;
        if (contact->invis_list) {
          v7_invisible (mainconnection, contact->uin,
                        contact->invlist_uid = contact_gen_uid (),
                        TRUE); /* add */
        } else {
          v7_invisible (mainconnection, contact->uin, contact->invlist_uid,
                        FALSE); /* remove */
        }
      }
      break;
    case 3: /* notify */
        contact->online_notify = GTK_CHECK_MENU_ITEM( widget )->active;
        break;
	 default:
		  g_warning("Unknown list: %d\n", type);
    }
}

static void add_files( GtkWidget *widget, gpointer data)
{
  GtkWidget *filechooser;
  GtkWidget *parent = data;
  GtkListStore *filelist;
  GSList *files, *file;
  gchar *utf8filename;
  GtkTreeIter iter;

  filelist = g_object_get_data(G_OBJECT(parent), "filelist");

  filechooser = gtk_file_chooser_dialog_new (_("Send Files..."),
					     GTK_WINDOW(parent),
					     GTK_FILE_CHOOSER_ACTION_OPEN,
					     GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					     GTK_STOCK_ADD, GTK_RESPONSE_ACCEPT,
					     NULL);

  gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(filechooser), TRUE);
  gtk_file_chooser_set_local_only(GTK_FILE_CHOOSER(filechooser), TRUE);

    
  if (gtk_dialog_run (GTK_DIALOG (filechooser)) == GTK_RESPONSE_ACCEPT)
  {
    files = gtk_file_chooser_get_filenames (GTK_FILE_CHOOSER (filechooser));

    for (file = files;
	 file;
	 file = g_slist_next(file)) {

      utf8filename = g_filename_to_utf8(file->data, -1,
					NULL, NULL, NULL);

      if (!utf8filename)
	continue;
	    

      gtk_list_store_append(filelist, &iter);
      gtk_list_store_set(filelist, &iter,
			 0, utf8filename,	
			 1, file->data,
			 -1);

      g_free(utf8filename);
    }
    g_slist_free(files);
  }

  gtk_widget_destroy(filechooser);

  
  
}

static void remove_files( GtkWidget *widget, gpointer data)
{
  GList *selected, *sel;
  GtkWidget *treeview = data;
  GtkTreeModel *model;
  GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
  GtkTreeIter *iters;
  int count, i;
  
	

  count = gtk_tree_selection_count_selected_rows(selection);

  if (count == 0)
    return;

  selected = gtk_tree_selection_get_selected_rows(selection, &model);

  iters = g_new0(GtkTreeIter, count);

  for(sel = selected, i=0;
      sel;
      sel = g_list_next(sel)) {
    if (gtk_tree_model_get_iter(model, &iters[i], sel->data))
      i++;
    
  }
  g_list_foreach (selected, (GFunc)gtk_tree_path_free, NULL);
  g_list_free (selected);
  
  count = i;
  for(i = 0; i < count; i++) {
    gtk_list_store_remove(GTK_LIST_STORE(model),
			  &iters[i]);
  }

  g_free(iters);
}


void remove_user (GtkDialog *dialog, int response, gpointer data)
{
	GSList *contact;

#ifdef TRACE_FUNCTION
	g_print( "remove_user\n" );
#endif

	if (response == GTK_RESPONSE_YES)
	{
		if (!is_connected(GTK_WINDOW(dialog), _("You can not remove a contact while disconnected.")))
			return;

		contact = g_object_get_data (G_OBJECT (dialog), "contact");
		if( contact == NULL )
			return;

                /* remove from status list, if she is on any of them */
                if (kontakt->vis_list)
                  v7_visible (mainconnection, kontakt->uin,
                              kontakt->vislist_uid, FALSE);
                if (kontakt->invis_list)
                  v7_invisible (mainconnection, kontakt->uin,
                                kontakt->invlist_uid, FALSE);
                if (kontakt->ignore_list)
                  v7_ignore (mainconnection, kontakt->uin,
                             kontakt->ignorelist_uid, FALSE);

                /* remove the contact */
		if (kontakt->inlist == TRUE)
			v7_remove_contact (mainconnection, kontakt->uin,
                                           kontakt->nick, kontakt->gid,
                                           kontakt->uid, kontakt->wait_auth);

		remove_contact (contact, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (data)));
		Save_RC();
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
user_change_group (GtkWidget *widget, GroupInfo *d)
{
	Contact_Member *contact;
	guint uid;

	contact = g_object_get_data (G_OBJECT (widget), "contact");

	if (contact) {
		if (!is_connected (GTK_WINDOW (MainData->window), _("You can not change the group of a user while disconnected.")))
			return;

		v7_user_change_group (mainconnection, contact, d->gid, d->name);
	}
}
