/****************************
 Drag & Drop support
 (c) 1999 Jeremy Wise
 GnomeICU
*****************************/

#ifndef __DRAGDROP_H__
#define __DRAGDROP_H__

#include <gtk/gtkselection.h>

extern GtkTargetEntry target_table[];


enum {
  DND_TARGET_WRONG,
  DND_TARGET_STRING,
  DND_TARGET_CONTACT,
  DND_TARGET_URL
};

void init_contact_list_drag_drop(GtkWidget *widget);

#endif /* __DRAGDROP_H__ */
