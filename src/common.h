/***********************************
 Common structs and values
 (c) 1999 Jeremy Wise
 GnomeICU
************************************/

#ifndef __COMMON_H__
#define __COMMON_H__

#include <config.h>

#include "datatype.h"
#include "prefs_string.h"
#include "prefs.h"

#include <libgnome/gnome-i18n.h>
#include <glade/glade-xml.h>
#include <gtk/gtkwidget.h>

#include <time.h>
#ifdef HAVE_SOCKS5
#define SOCKS
#include <socks.h>
#endif

#undef MIN
#undef MAX


/* This is because I am lazy */

#define n_(String1, String2, n) dngettext (GETTEXT_PACKAGE, String1, String2, n)


typedef struct toggles_t
{
	gboolean auto_popup;
	gboolean full_reply;
	gboolean connect_startup;
	gboolean beep;
	gboolean packet_tcp;
	gboolean packet_udp;
	gboolean connection_history;
	gboolean sound;
	gboolean webpresence;
	gboolean russian;
	gboolean latin2;
	gboolean kanji;
	gboolean no_new_users;
        gboolean status_notify;
	gboolean sort_contacts;
	gboolean sound_onlyonline;
	gboolean use_autoaway;
	gint auto_away;
	gint auto_na;
        gboolean check_spell;
	gboolean program_on_receive_message;
	gboolean program_on_receive_url;
	gboolean program_on_file_request;
	gboolean program_on_user_online;
	gboolean program_on_user_offline;
	gboolean program_on_auth;
	gboolean program_on_auth_request;
	gboolean program_on_list_add;
	gboolean program_on_contact_list;
	gboolean auto_accept_xfers;
	gchar *auto_accept_path;
        gchar *theme_name;
        gchar *emoticon_theme_name;
	gboolean auto_recon_forced;
	gboolean default_to_chat;
} _toggles;

typedef struct programs_t
{
	gchar *receive_message;
	gchar *receive_url;
	gchar *file_request;
	gchar *user_online;
	gchar *user_offline;
	gchar *auth;
	gchar *auth_request;
	gchar *list_add;
	gchar *contact_list;
} _programs;


#define TCP_VERSION 0x08

#define NOT_SPECIFIED 0
#define FEMALE 1
#define MALE 2

#define PACKET_TYPE_TCP          1

#define PACKET_DIRECTION_SEND    4
#define PACKET_DIRECTION_RECEIVE 8

#define STATUS_OFFLINE         0xffffffff
#define STATUS_ONLINE          0x00
#define STATUS_INVISIBLE       0x100
#define STATUS_NA_99A          0x04
#define STATUS_NA              0x05
#define STATUS_FREE_CHAT       0x20
#define STATUS_OCCUPIED_MAC    0x10
#define STATUS_OCCUPIED        0x11
#define STATUS_AWAY            0x01
#define STATUS_DND             0x13
#define STATUS_DND_MAC         0x02
#define STATUS_AIM_ONLINE      0x1000
#define STATUS_AIM_AWAY        0x3000
#define STATUS_CUSTOM          0x5000

#define USER_ADDED_MESS        0x000c
#define AUTH_REQ_MESS          0x0006
#define URL_MESS               0x0004
#define MASS_MESS_MASK         0x8000
#define MRURL_MESS             0x8004
#define NORM_MESS              0x0001
#define MRNORM_MESS            0x8001
#define CONTACT_MESS           0x0013
#define MRCONTACT_MESS         0x8013
#define AUTH_MESS              0x0008

typedef struct USER_INFO_T
{
	UIN_T uin;       /*General*/
	gchar *nick;     /*General*/
	gchar *alias;    /*General*/
	gchar *first;    /*General*/
	gchar *last;     /*General*/
	gchar age;       /*General*/
	gchar sex;       /*General*/
	guint auth        : 1; /*General*/
	guint webpresence : 1; /*********/
	guint hide_email  : 1; /*********/
	guint birth_day   : 5; /*********/
	guint birth_month : 4; /*********/
	guint birth_year;      /*********/
        BYTE language1;
        BYTE language2;
        BYTE language3;
        BYTE timezone;
        gchar *version;
	gchar *ip;       /*Internet*/
	gchar *port;     /*Internet*/
	gchar *status;   /*Internet*/
	gchar *email;    /*Internet*/
	gchar *phone;    /*Location*/
	gchar *fax;      /*Location*/
	gchar *cellular; /*Location*/
	gchar *street;   /*Location*/
	gchar *city;     /*Location*/
	gchar *state;    /*Location*/
	gchar *zip;       /*Location*/
	WORD country;    /*Location*/
	gchar *homepage; /*About*/
	gchar *about;    /*About*/
	WORD occupation;     /*Work*/
	gchar *job_pos;      /*Work*/
	gchar *department;   /*Work*/
	gchar *company_name; /*Work*/
	gchar *work_address; /*Work*/
	gchar *work_city;    /*Work*/
	gchar *work_state;   /*Work*/
	gchar *work_phone;   /*Work*/
	gchar *work_fax;     /*Work*/
	gchar *work_zip;     /*Work*/
	gchar *work_homepage;/*Work*/
	WORD work_country;   /*Work*/
	GList *emails;	    /*Emails*/
	GList *homepage_cat; /*Homepage categories*/
	GList *interests;    /*Interests*/
	GList *past_background; /*Past & Affiliations*/
	GList *affiliations; /*Past & Affiliations*/

} USER_INFO_STRUCT, *USER_INFO_PTR;

typedef enum
{
	MESSAGE_TEXT,        /* Normal text message */
	MESSAGE_URL,         /* URL message */
	MESSAGE_FILE_REQ,    /* Request to send you a file */
	MESSAGE_CHAT_REQ,    /* Request to start a chat */
	MESSAGE_AUTH_REQ,    /* Request for authorization */
	MESSAGE_USER_ADD,    /* User has added you to his/her list */
	MESSAGE_USER_AUTHD,  /* User has authorized/denied you to add him/her */
	MESSAGE_CONT_LIST    /* Received contact list */
} MESSAGE_TYPES;

typedef struct stored_message_t
{
	MESSAGE_TYPES type;
	time_t time;
	gchar *message;
	gpointer data;       /* Message type dependent data */
} stored_message, *STORED_MESSAGE_PTR;

typedef enum {
  NO, YES,
  OUTGOING1, OUTGOING2,
  INCOMING
} tcp_conn;

typedef struct Contact_Member_t
{
	UIN_T uin;
	DWORD status;
	DWORD lb_index;
	DWORD last_time; /* last time online or when came online */
	gboolean has_rtf;
	gboolean has_direct_connect;
	gboolean has_birthday;
	DWORD current_ip;
	DWORD port;
	DWORD direct_cookie;
	WORD  version;
	WORD  used_version;
	int sok;
	gchar* nick;
	USER_INFO_PTR info;
	GSList *stored_messages;
	guint invis_list     : 1;
	guint vis_list	     : 1;
	guint ignore_list    : 1;
	guint online_notify  : 1;
	guint inlist	     : 1;
	gboolean newmsg_blink : 1;
	GSList *tcp_msg_queue;
	WORD tcp_seq;
	tcp_conn have_tcp_connection;
	WORD tcp_buf_len;
	BYTE *tcp_buf;
	WORD tcp_buf_read;
	GIOChannel *gioc;
	guint giocw;
	GtkWidget *detached_window;
        GladeXML *history_xml;
	GladeXML *msg_dlg_xml;
	GladeXML *info_dlg_xml;
	gboolean confirmed;
	WORD gid;
	WORD uid;
	WORD vislist_uid;
	WORD invlist_uid;
	WORD ignorelist_uid;
	time_t last_seen;
	WORD idletime;
	DWORD online_since, member_since, idle_since;
	gboolean wait_auth; /* whether we need auth grant from this contact */
	GSList *file_xfers;
	gint newmsg_blink_timeout;
} Contact_Member, *CONTACT_PTR;

typedef struct
{
  WORD gid;
  gchar *name;
} GroupInfo;

/* This is so much used overall, it is easier to provide a macro */
#define kontakt ((CONTACT_PTR)contact->data)

#endif /* __COMMON_H__ */
