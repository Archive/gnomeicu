/* GnomeICU
 * Copyright (C) 1998-2003 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/**** Emoticon themes handling ****/

#include "common.h"
#include "gnomeicu.h"
#include "emoticons.h"

#include <gtk/gtk.h>
#include <string.h>

#define THEME_PATH GNOMEICU_DATADIR "/gnomeicu/emoticons/"
#define INDEX_FILE "emoticon-data"

GSList *Emoticons = NULL;

static gboolean emoticon_themes_initialized = FALSE;

static gchar *
find_theme_path_index (const gchar *themename)
{
  gchar *file;

  g_assert (themename != NULL);

  /* Local themes */
  file = g_strconcat (g_get_home_dir (), "/.icq/emoticons/", themename,
                      "/" INDEX_FILE, NULL);

  if (!g_file_test ((file), G_FILE_TEST_EXISTS)) {
    /* no such local theme, search in global themes */
    g_free (file);
    file = g_strconcat (THEME_PATH, themename, "/" INDEX_FILE, NULL);

    if (!g_file_test ((file), G_FILE_TEST_EXISTS)) {
      /* no such theme, use default */
      g_free (file);
      file = g_strconcat (THEME_PATH, "Default/" INDEX_FILE, NULL);

      if (!g_file_test ((file), G_FILE_TEST_EXISTS)) {
	/* Nothing exists, return NULL */
        g_free (file);
        file = NULL;
      }
    }
  }

  return file;
}

static GdkPixbuf *
emoticon_get_pixbuf (const gchar *path, const gchar *icon_name)
{
  GdkPixbuf *pixbuf = NULL;
  gchar *icon_filename;

  icon_filename = g_strconcat (path, "/", icon_name, NULL);
	
  if (icon_filename)
    pixbuf = gdk_pixbuf_new_from_file (icon_filename, NULL);

  g_free (icon_filename);
  return pixbuf;
}

static void
emoticon_parse_file (const gchar *index)
{
  gchar *buf;
  gchar **lines, **one_line;
  gint cx, cy;
  GdkPixbuf *pixbuf;
  gchar *theme_path;

  theme_path = g_path_get_dirname (index);

  if (g_file_get_contents (index, &buf, NULL, NULL)) {
    lines = g_strsplit (buf, "\n", 0);
    for (cx = 0; lines[cx] != NULL; cx++) {
      if (lines[cx] && lines[cx][0] != '\0') {
        one_line = g_strsplit (lines[cx], "\t", 0);
        pixbuf = emoticon_get_pixbuf (theme_path, one_line[0]);
        if (pixbuf) {
          for (cy = 1; one_line[cy] != NULL; cy++) {
            if (one_line[cy] && one_line[cy][0] != '\0') {
              Emoticon *e = g_new0 (Emoticon, 1);

              e->string = g_strdup (one_line[cy]);
              e->pixbuf = pixbuf;
              g_object_ref (e->pixbuf); /* ref on every copy */
              Emoticons = g_slist_append (Emoticons, e);
            }
          }
          g_object_unref (pixbuf); /* unref the one from emoticon_get_pixbuf() */
        }
        g_strfreev (one_line);
      }
    }
    g_strfreev (lines);
  }

  g_free (theme_path);
  g_free (buf);
}

static void
emoticon_clean_struct (void)
{
  GSList *emoticons;
  Emoticon *e;

  for (emoticons = Emoticons; emoticons != NULL; emoticons = g_slist_next (emoticons)) {
    e = (Emoticon *)emoticons->data;
    g_free (e->string);
    g_object_unref (e->pixbuf);
    g_free (e);
    emoticons->data = NULL;
  }

  g_slist_free (Emoticons);
  Emoticons = NULL;
}

static void
emoticon_themes_load (void)
{
  gchar *index, *theme_name;

  emoticon_clean_struct ();

  /* always load the "Always" theme */
  index = find_theme_path_index ("Always");
  if (index == NULL)
    return;

  emoticon_parse_file (index);
  g_free (index);

  /* load theme chose by user */
  theme_name = preferences_get_string (PREFS_GNOMEICU_EMOTICON_THEMES);
  if (strcmp (theme_name, "None") == 0) {
    /* if theme_name is None, nothing to load */
    g_free (theme_name);
    return;
  }

  index = find_theme_path_index (theme_name == NULL ? "Default" : theme_name);
  g_free (theme_name);
  if (index == NULL)
    return;

  emoticon_parse_file (index);
  g_free (index);
}

void
emoticon_themes_init (void)
{
  if (emoticon_themes_initialized)
    return;

  emoticon_themes_load ();
  preferences_watch_key (PREFS_GNOMEICU_EMOTICON_THEMES,
                         emoticon_themes_load);

  emoticon_themes_initialized = TRUE;
}
