/********************************
 TCP peer to peer functions
 (c) 1999 Jeremy Wise
 GnomeICU
*********************************/

#include "tcp.h"
#include "common.h"
#include "gnomeicu.h"
#include "gnomeicu-spinner.h"
#include "gtkfunc.h"
#include "history.h"
#include "icons.h"
#include "packetprint.h"
#include "response.h"
#include "sendcontact.h"
#include "showlist.h"
#include "util.h"
#include "v7send.h"
#include "v7base.h"

#include <gtk/gtk.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
gint min_tcp_port = 4000;
gint max_tcp_port = 5000;

/* 127.0.0.1 is localhost */
const DWORD LOCALHOST = 0x7F000001;

static unsigned char client_check_data[] = {
  "As part of this software beta version Mirabilis is "
  "granting a limited access to the ICQ network, "
  "servers, directories, listings, information and databases (\""
  "ICQ Services and Information\"). The "
  "ICQ Service and Information may databases (\""
  "ICQ Services and Information\"). The "
  "ICQ Service and Information may\0"
};

enum {
	ICQ_RESPONSE_SERVER,
	ICQ_RESPONSE_RETRY
};


/*** Local function declarations ***/
static void tcp_encrypt ( guchar *data, int size, int version);
static gboolean tcp_decrypt(guchar *data, int datalen, int version);

static tcp_message *tcp_prepare_message( CONTACT_PTR contact, BYTE *data,
                                         gchar *text, WORD seq);

static int TCPGainConnection( DWORD ip, WORD port, GSList *contact );
static int TCPClearQueue( GSList *contact );

static gboolean TCPSendPacket(UIN_T uin, int command, int subcommand,
                              int msgtype, int version, const gchar *data,
                              int datalen, const gchar *message, int status,
                              WORD seq, gboolean queue);

static int TCPInitChannel( GIOChannel *source, GIOCondition cond, gpointer data );
static int TCPAckPacket(UIN_T uin, int subcommand, const gchar *data,
                        int datalen, int status, WORD seq);
static void TCPProcessPacket( BYTE *packet, int packet_length, int sock, UIN_T uin);

static int TCPFinishConnection( GIOChannel *source, GIOCondition cond, gpointer data );
static int TCPReadPacket( GIOChannel *source, GIOCondition cond, gpointer data );
static int TCPTimeout( tcp_message *m );


/*** Global functions ***/

/***************************************************************
 * This function will put a message on the stack of a contact  *
 * contact - the destination contact                           *
 * data    - the actual packet to be sent                      *
 * text    - the text of the message, if applicable            *
 * seq     - sequence of the TCP packet                        *
 ***************************************************************/
tcp_message *tcp_prepare_message( CONTACT_PTR contact, BYTE *data,
                                  gchar *text, WORD seq)
{
  tcp_message *m;

  m = g_new0( tcp_message, 1 );
  m->seq = seq;
  m->text = text;
  m->data = g_memdup(data, Chars_2_Word(data)+2);
  m->uin = g_strdup (contact->uin);

  contact->tcp_msg_queue = g_slist_append( contact->tcp_msg_queue, m );

  return m;
}

gboolean TCPSendPacket(UIN_T uin, 
                       int command, int subcommand, int msgtype, int version,
                       const gchar *data, int datalen, const gchar *message,
                       int status, WORD seq, gboolean queue)
{
  TLVstack *tlvs;
  tcp_message *m;
  int sock;
  GSList *contact;
 
#ifdef TRACE_FUNCTION
  g_print( "TCPSendPacket\n" );
#endif

  contact = Find_User( uin );
  if( contact == NULL || kontakt->has_direct_connect != YES)
    return FALSE;
  
  
  /* If the user is offline, we use the server */
  if( kontakt->status == STATUS_OFFLINE &&
      kontakt->sok == 0 )
    return 0;

  tlvs = new_tlvstack("  ", 2);

  if (!seq)
    seq = kontakt->tcp_seq--;
  
  switch (kontakt->used_version) {
  case 2:    
  case 3:
    add_nontlv_dw_le(tlvs, atoi (our_info->uin));
    add_nontlv_w_le (tlvs, kontakt->used_version);
    add_nontlv_dw_le(tlvs, command);
    add_nontlv_dw_le(tlvs, atoi (our_info->uin));
    add_nontlv_w_le (tlvs, subcommand);
    add_nontlv_lnts (tlvs, message);
    add_nontlv_dw_le(tlvs, our_ip);
    add_nontlv_dw_le(tlvs, our_ip);
    add_nontlv_dw_le(tlvs, our_port);
    add_nontlv      (tlvs, "\x04", 1);  /* MODE We use 4*/
    /* Why are we exactly doing that ??? 
       if( kontakt->status == STATUS_DND || kontakt->status == STATUS_OCCUPIED )
       add_nontlv_w_le (tlvs, 0x00200000);
       else
       add_nontlv_w_le (tlvs, 0x00100000); 
       That's the code from the old one...
    */
    if (status)
      add_nontlv_w_le(tlvs, status);
    else
      add_nontlv_w_le (tlvs, Current_Status);
    add_nontlv_w_le (tlvs, msgtype);
    if (data) 
      add_nontlv    (tlvs, data, datalen);
    add_nontlv_dw_le(tlvs, seq); /* SEQUENCE NUMBER */
    break;
  case 4:
  case 5:
    add_nontlv_dw_le(tlvs, atoi (our_info->uin));
    add_nontlv_w_le (tlvs, kontakt->used_version);
    add_nontlv_dw_le(tlvs, 0);               /* CHECKSUM */
    add_nontlv_dw_le(tlvs, command);
    add_nontlv_dw_le(tlvs, atoi (our_info->uin));
    add_nontlv_w_le (tlvs, subcommand);
    add_nontlv_lnts (tlvs, message);
    add_nontlv_dw_le(tlvs, our_ip);
    add_nontlv_dw_le(tlvs, our_ip);
    add_nontlv_dw_le(tlvs, our_port);
    add_nontlv      (tlvs, "\x04", 1); /* MODE  (We use 4, done ask why*/
    if (status)
      add_nontlv_w_le(tlvs, status);
    else
      add_nontlv_w_le (tlvs, Current_Status);
    add_nontlv_w_le (tlvs, msgtype);
    if (data) 
      add_nontlv    (tlvs, data, datalen);
    add_nontlv_dw_le(tlvs, seq); /* SEQUENCE NUMBER */
    add_nontlv      (tlvs, "L", 1);
    add_nontlv_w_le (tlvs, kontakt->used_version);
    break;
  case 6:
  case 7:
  case 8:
    add_nontlv_dw_le(tlvs, 0); /* CHECKSUM */
    add_nontlv_w_le (tlvs, command); 
    add_nontlv_w_le (tlvs, 0x0E);  /* always 0x0E (copied from licq) */
    add_nontlv_w_le (tlvs, seq);
    add_nontlv_dw_le(tlvs, 0); /* Always zero */
    add_nontlv_dw_le(tlvs, 0); /* Always zero */
    add_nontlv_dw_le(tlvs, 0); /* Always zero */
    add_nontlv_w_le (tlvs, subcommand);
    if (status)
      add_nontlv_w_le(tlvs, status);
    else
      add_nontlv_w_le (tlvs, Current_Status);
    add_nontlv_w_le (tlvs, msgtype);
    add_nontlv_lnts (tlvs, message);
    if (data) 
      add_nontlv    (tlvs, data, datalen);
    break;
	default:
	 g_warning("Unknown version used by contact for sending packet: %d\n", kontakt->used_version);
  };

  tcp_encrypt(tlvs->beg+2, tlvs->len-2, kontakt->used_version); /* checksum */

  Word_2_Chars(tlvs->beg, tlvs->len-2);

  sock = TCPGainConnection( kontakt->current_ip, kontakt->port, contact );

  if (!queue) {
    if (sock >= 0) {
      write( sock , tlvs->beg, tlvs->len );
      packet_print( tlvs->beg, tlvs->len,
                    PACKET_TYPE_TCP | PACKET_DIRECTION_SEND,
                    "Sending ACK" );
    }
    free_tlvstack(tlvs);
    return TRUE;
  }

  /* Send packet */
  m = tcp_prepare_message( kontakt, tlvs->beg, g_strdup( message ), seq);
  
  free_tlvstack(tlvs);
  
  
  if( sock != -1 )
    m->timeout = gtk_timeout_add( 30000, (GtkFunction)TCPTimeout, m );
  
  if( sock == -2 ) /* We're waiting for a connection */
    return TRUE;
  
  if( sock != -1 )
    TCPClearQueue( contact );
  else
    return FALSE;
  
  gnomeicu_spinner_start();

  return TRUE;
}
  

/* 
   This function has been copied directly from Licq... Thanks a lot to them.
*/

void tcp_encrypt ( guchar *data, int datalen, int version) 
{
 
  unsigned long B1, M1, check;
  unsigned int i;
  unsigned char X1, X2, X3;
  unsigned char bak[6];
  unsigned long offset;
  unsigned long key;

#ifdef TRACE_FUNCTION
  g_print( "tcp_encrypt\n");
#endif

  if (version < 4)
    return;  /* no encryption necessary. */

  switch(version) {
  case 4:
  case 5:
    offset = 6; 
    break;
  case 6:
  case 7:
  case 8:
  default:
    offset = 0;
  }

  /* calculate verification data */
  M1 = (rand() % ((datalen < 255 ? datalen : 255)-10))+10;
  X1 = data[M1] ^ 0xFF;
  X2 = rand() % 220;
  X3 = client_check_data[X2] ^ 0xFF;
  if(offset) {
    for(i=0; i<6 ;i++)
      bak[i] = data[i];
    B1 = (data[offset+4]<<24)|(data[offset+6]<<16)|(data[2]<<8)|data[0];
  }
  else
    B1 = (data[4]<<24)|(data[6]<<16)|(data[4]<<8)|(data[6]);
  /* calculate checkcode */
  check = (M1 << 24) | (X1 << 16) | (X2 << 8) | X3;
  check ^= B1;
  
  /* main XOR key */
  key = 0x67657268 * datalen + check;
  
  /* XORing the actual data */
  for(i=0;i<(datalen+3)/4;i+=4) {
    unsigned long hex = key + client_check_data[i&0xFF];
    data[i+0] ^= hex&0xFF;data[i+1] ^= (hex>>8)&0xFF;
    data[i+2] ^= (hex>>16)&0xFF;data[i+3] ^= (hex>>24)&0xFF;
  }
  /*
    in TCPv4 are the first 6 bytes unencrypted
    so restore them
  */
  if(offset)
    for(i=0;i<6;i++)
      data[i] = bak[i];

  // storing the checkcode */
  data[offset+3] = (check>>24)&0xFF;
  data[offset+2] = (check>>16)&0xFF;
  data[offset+1] = (check>>8)&0xFF;
  data[offset+0] =  check    &0xFF;
}

/* 
   This function was also directly taken from Licq 
   Thanks to them for their great work!
*/

gboolean tcp_decrypt(guchar *data, int datalen, int version)
{
  unsigned long hex, key, B1, M1, check;
  unsigned int i;
  unsigned char X1, X2, X3;
  unsigned char bak[6];
  unsigned long offset;

  if(version < 4)
    return TRUE;  /* no decryption necessary. */

  switch(version) {
  case 4:
  case 5:
    offset = 6;
    break;
  case 6:
  case 7:
  case 8:
  default:
    offset = 0;
  }

  /* backup the first 6 bytes */
  if(offset)
    for(i=0;i<6;i++)
      bak[i] = data[i];

  /* retrieve checkcode */
  check = (data[offset+3]<<24) | 
    (data[offset+2]<<16) | 
    (data[offset+1]<<8) | 
    (data[offset+0]);

  /* main XOR key */
  key = 0x67657268 * datalen + check;

  for(i=4; i<(datalen+3)/4; i+=4) {
    hex = key + client_check_data[i&0xFF];
    data[i+0] ^= hex&0xFF;data[i+1] ^= (hex>>8)&0xFF;
    data[i+2] ^= (hex>>16)&0xFF;data[i+3] ^= (hex>>24)&0xFF;
  }

  /* retrive validate data */
  if(offset) {
    /*
      in TCPv4 are the first 6 bytes unencrypted
      so restore them
    */
    for(i=0;i<6;i++) data[i] = bak[i];
    B1 = (data[offset+4]<<24)|(data[offset+6]<<16)|(data[2]<<8)|data[0];
  }
  else
    B1 = (data[4]<<24) | (data[6]<<16) | (data[4]<<8) | (data[6]<<0);

  /* special decryption */
  B1 ^= check;
 
  /* validate packet */
  M1 = (B1 >> 24) & 0xFF;
  if(M1 < 10 || M1 >= datalen) 
    return FALSE;

  X1 = data[M1] ^ 0xFF;
  if(((B1 >> 16) & 0xFF) != X1) {
    return FALSE;
  }


  X2 = ((B1 >> 8) & 0xFF);
  if(X2 < 220) {
    X3 = client_check_data[X2] ^ 0xFF;
    if((B1 & 0xFF) != X3) 
      return FALSE;
  }

  return TRUE;
}

                     

int TCPGainConnection( DWORD ip, WORD port, GSList *contact )
{
  struct sockaddr_in local, remote;
  int sizeofSockaddr = sizeof( struct sockaddr );
  int sock;

#ifdef TRACE_FUNCTION
  g_print( "TCPGainConnection\n" );
#endif

  if( kontakt->have_tcp_connection == NO )
    kontakt->sok = 0;

  if( kontakt->sok > 0 )
    return kontakt->sok;

  if( ip == 0 )
    return -1;

  sock = socket( AF_INET, SOCK_STREAM, 0 );
  if( sock == -1 )
    return -1;

  fcntl( sock, F_SETFL, O_NONBLOCK );

  memset( &local.sin_zero, 0x00, 8 );
  memset( &remote.sin_zero, 0x00, 8 );
	
  local.sin_family = AF_INET;
  remote.sin_family = AF_INET;
  local.sin_port = g_htons( 0 );
  local.sin_addr.s_addr = g_htonl( INADDR_ANY );
  if( ( bind( sock, (struct sockaddr*)&local, sizeof( struct sockaddr ) ) )== -1 )
    return -1;

  getsockname( sock, (struct sockaddr*)&local, &sizeofSockaddr );

  remote.sin_port = g_htons( port );
  remote.sin_addr.s_addr = g_htonl( ip );

  connect( sock, (struct sockaddr *)&remote, sizeofSockaddr );

  kontakt->sok = sock;
  kontakt->gioc = g_io_channel_unix_new( sock );


  g_io_add_watch( kontakt->gioc,
                  G_IO_IN | G_IO_OUT | G_IO_ERR | G_IO_HUP,
                  TCPFinishConnection, NULL );

  return -2;
}

int TCPClearQueue( GSList *contact )
{
  WORD psize;
  GSList *packet;

  tcp_message *m;

#ifdef TRACE_FUNCTION
  g_print( "TCPClearQueue\n" );
#endif

  if( contact == NULL )
    return FALSE;

  packet = kontakt->tcp_msg_queue;
  while( packet != NULL )
    {
      m = (tcp_message*)packet->data;
      if( m->sent == FALSE )
        {
          psize = Chars_2_Word( m->data );

          write( kontakt->sok, m->data, psize + 2 );
          packet_print( m->data, psize + 2,
                        PACKET_TYPE_TCP | PACKET_DIRECTION_SEND,
                        (gchar*)((BYTE*)(m->data)+2+psize) );
          m->sent = TRUE;
        }
      if( m->seq == 0 )
        {
          g_free( m->data );
          g_free( m );
          kontakt->tcp_msg_queue = g_slist_remove( kontakt->tcp_msg_queue, m );
          packet = kontakt->tcp_msg_queue;
        }
      else
        packet = packet->next;
    }

  return FALSE;
}

int TCPAcceptIncoming( GIOChannel *iochan, GIOCondition cond, gpointer data )
{
  int sock = g_io_channel_unix_get_fd(iochan);
  struct sockaddr_in addr;
  int size = sizeof( struct sockaddr_in );
  int new_sock;
  GIOChannel *gioc;

#ifdef TRACE_FUNCTION
  g_print( "TCPAcceptIncoming\n" );
#endif
	
  new_sock = accept( sock, (struct sockaddr *)&addr, &size );
  if( new_sock == -1 )
    return 0;

  if( new_sock == -1 )
    return FALSE;

  gioc = g_io_channel_unix_new( new_sock );

  g_io_add_watch( gioc, G_IO_IN | G_IO_ERR | G_IO_HUP,
                  TCPInitChannel,  NULL);

  return 1;
}

int TCPRetrieveAwayMessage( GSList *contact, gpointer data )
{

  int status_type;

#ifdef TRACE_FUNCTION
  g_print( "TCPRetrieveAwayMessage\n" );
#endif

  switch( kontakt->status & 0xffff )
    {
    case STATUS_AWAY:
      status_type = ICQ_CMDxTCP_READxAWAYxMSG;
      break;
    case STATUS_NA:
      status_type = ICQ_CMDxTCP_READxNAxMSG;
      break;
    case STATUS_OCCUPIED:
      status_type = ICQ_CMDxTCP_READxOCCxMSG;
      break;
    case STATUS_DND:
      status_type = ICQ_CMDxTCP_READxDNDxMSG;
      break;
    default:
      status_type = ICQ_CMDxTCP_READxAWAYxMSG;
      break;
    }

  return TCPSendPacket(kontakt->uin, ICQ_CMDxTCP_START, status_type, 0,
                       0, NULL, 0, NULL, 0, 0, TRUE);

}

int TCPAcceptFile( XferInfo *xfer )
{
  TLVstack *tlvs;
  int returncode=0;

#ifdef TRACE_FUNCTION
  g_print( "TCPAcceptFile\n" );
#endif
  if (!ft_listen(xfer))
    return FALSE;

  tlvs = new_tlvstack(NULL, 0);

  add_nontlv_dw_be(tlvs, xfer->port);
  add_nontlv_lnts(tlvs, "");
  add_nontlv_dw_le(tlvs, 0);
  add_nontlv_dw_le(tlvs, xfer->port);

  
  if (preferences_get_bool (PREFS_GNOMEICU_AUTO_ACCEPT_FILE)) {
    if (xfer->auto_save_path)
      g_free (xfer->auto_save_path);

    xfer->auto_save_path = preferences_get_string (PREFS_GNOMEICU_DOWNLOAD_PATH);
  }
  
  //  returncode =  TCPAckPacket(xfer->remote_contact->uin,ICQ_CMDxTCP_FILE,
  //                             tlvs->beg, tlvs->len, ICQ_ACKxTCP_ONLINE,
  //                             xfer->seq);

  free_tlvstack(tlvs);

  return returncode;    

}

int TCPRefuseFile( XferInfo *xfer )
{

  int returncode=0;
  
#ifdef TRACE_FUNCTION
  g_print( "TCPRefuseFile\n" );
#endif
  
  xfer->remote_contact->file_xfers = g_slist_remove(xfer->remote_contact->file_xfers, xfer);

  
  //  returncode = TCPSendPacket(xfer->remote_contact->uin, ICQ_CMDxTCP_CANCEL,
			     //                             ICQ_CMDxTCP_FILE, 0, 0, NULL, 0, "",
			     //                             ICQ_ACKxTCP_REFUSE, xfer->seq, FALSE);
  g_free(xfer);

  return returncode;

}

int TCPSendFileRequest( UIN_T uin, const gchar *msg, GSList *files )
{
  int cx;

  struct stat file_stat;
  gchar *nopathfile = 0;

  XferInfo *xferinfo;

  GSList *file;
  GSList *contact;
  TLVstack *tlvs;

  int returncode=0;

#ifdef TRACE_FUNCTION
  g_print( "TCPSendFileRequest\n" );
#endif
	
  contact = Find_User( uin );
  if( !contact  || kontakt->has_direct_connect != YES)
    return FALSE;

  xferinfo = ft_new(kontakt, XFER_DIRECTION_SENDING, NULL, 0);

  //  xferinfo->seq = kontakt->tcp_seq--;

  for (file = files;file;file = file->next) {
    nopathfile = ((gchar*)file->data);

    for( cx = strlen((gchar*)file->data); cx; cx-- )
      if( ((gchar*)file->data)[ cx ] == '/' ) {
        nopathfile = &((gchar*)file->data)[ cx + 1 ];
        break;
      }

    if( !nopathfile || nopathfile[0] == '\0' )
      return FALSE;

    if( stat( (gchar*)file->data, &file_stat ) == -1 )
      return FALSE;

    ft_addfile(xferinfo, g_strdup(((gchar*)file->data)), g_strdup(nopathfile),
	       file_stat.st_size);
  }

  if (g_slist_length(xferinfo->file_queue) == 1)
    xferinfo->filename = g_strdup(nopathfile);
  else {
    char buf[15];
    snprintf(buf, sizeof (buf), "%u files", g_slist_length(xferinfo->file_queue));
    xferinfo->filename = g_strdup(buf);
  }


  tlvs = new_tlvstack("\0\0\0\0", 4);
  add_nontlv_lnts(tlvs, xferinfo->filename);
  add_nontlv_dw_le(tlvs, xferinfo->total_bytes);
  add_nontlv_dw_le(tlvs, 0); /* licq uses 0, old gnomeicu used 0x0100 */

        
  /*  returncode = TCPSendPacket(uin, ICQ_CMDxTCP_START, ICQ_CMDxTCP_FILE,
                             0, 0, tlvs->beg, tlvs->len, msg, 0,
                             xferinfo->seq, TRUE); */
  free_tlvstack(tlvs);

  if (!returncode)
    ft_cancel_transfer(0, xferinfo);

  return returncode;
}

/*** Local functions ***/
int TCPInitChannel( GIOChannel *source, GIOCondition cond, gpointer data )
{
  UIN_T uin;
  guint16 packet_size;
  BYTE *packet;
  int sock;
  int version;
  GSList *contact;
  gsize bytes_read = 0;
  GIOStatus iostatus;
	
#ifdef TRACE_FUNCTION
  g_print( "TCPInitChannel(" );
  switch( cond )
    {
    case G_IO_IN: g_print( "G_IO_IN)\n" ); break;
    case G_IO_OUT: g_print( "G_IO_OUT)\n" ); break;
    case G_IO_ERR: g_print( "G_IO_ERR)\n" ); break;
    case G_IO_HUP: g_print( "G_IO_HUP)\n" ); break;
    default: g_print( "default)\n" );
    }
#endif

  sock = g_io_channel_unix_get_fd( source );

  while( bytes_read < 2) {
    gsize read;
    iostatus = g_io_channel_read_chars( source, (gchar*)&packet_size + bytes_read,
					2 - bytes_read, &read , NULL);
    switch (iostatus) {
    case G_IO_STATUS_NORMAL:
      bytes_read += read;
      break;
    case G_IO_STATUS_ERROR:
    case G_IO_STATUS_EOF:
      g_io_channel_shutdown(source, 1, NULL);
      return FALSE;
    }
  }
  

  packet_size = GUINT16_FROM_LE (packet_size);
        
  packet = (BYTE *)g_malloc0( packet_size );
  read( sock, packet, packet_size );

  bytes_read = 0;
  while( bytes_read < packet_size) {
    gsize read;
    iostatus = g_io_channel_read_chars( source, packet + bytes_read,
					packet_size - bytes_read, &read , NULL);
    switch (iostatus) {
    case G_IO_STATUS_NORMAL:
      bytes_read += read;
      break;
    case G_IO_STATUS_ERROR:
    case G_IO_STATUS_EOF:
      g_io_channel_shutdown(source, 1, NULL);
      return FALSE;
    }
  }
  

  packet_print( packet, packet_size,
                PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "Init Packet" );
  
  //  version = Chars_2_Word(packet+1);
  version = packet[1];

  if (version <= 5)
    uin = g_strdup_printf ("%d", Chars_2_DW (packet + 9));
  else
    uin = g_strdup_printf ("%d", Chars_2_DW (packet + 15));

  contact = Find_User( uin );  

  if( contact == NULL )
    contact = Add_User( uin, NULL, FALSE );

  kontakt->sok = sock;

  if (kontakt->used_version != version)
    kontakt->used_version = version;

  fcntl( sock, F_SETFL, O_NONBLOCK );
  kontakt->gioc = source;
  kontakt->giocw =
    g_io_add_watch( kontakt->gioc,
                    G_IO_IN | G_IO_ERR | G_IO_HUP,
                    TCPReadPacket, NULL );

  if (version <= 3)
    TCPProcessPacket( packet, packet_size, sock, uin);

  
  if (version >= 6) {
    TCPSendHandShakeAck(kontakt->sok);
    TCPSendHandShake(kontakt, Chars_2_DW(packet+32), kontakt->sok);

    kontakt->have_tcp_connection = INCOMING;
  }
  else
    kontakt->have_tcp_connection = YES;

  g_free( data );
  g_free( packet );
  g_free (uin);
  return FALSE;

}

int TCPAckPacket(UIN_T uin, int subcommand, const gchar *data, int datalen, int status,  WORD seq)
{
  char *msg;
  int returncode;
        
#ifdef TRACE_FUNCTION
  g_print( "TCPAckPacket\n" );
#endif

  if (!status)
    switch( Current_Status & 0xffff ) {
    case STATUS_ONLINE:
      status = ICQ_ACKxTCP_ONLINE;
      break;
    case STATUS_AWAY:
      status = ICQ_ACKxTCP_AWAY;
      break;
    case STATUS_DND:
      status = ICQ_ACKxTCP_DND;
      break;
    case STATUS_OCCUPIED:
      /* Don't ask me why this is - I just follow precedent */
      status = ICQ_ACKxTCP_ONLINE;
      break;
    case STATUS_NA:
      status = ICQ_ACKxTCP_NA;
      break;
    case STATUS_INVISIBLE:
      status = ICQ_ACKxTCP_ONLINE;
      break;
    default:
      status = ICQ_ACKxTCP_ONLINE;
    }

  
  if (Current_Status == STATUS_ONLINE ||
      Current_Status == STATUS_OFFLINE)
    return TCPSendPacket(uin, ICQ_CMDxTCP_ACK, subcommand, 0, 0, data,
                         datalen, "", status, seq, FALSE);
  else {
    msg = convert_from_utf8(Away_Message);

    
    returncode = TCPSendPacket(uin, ICQ_CMDxTCP_ACK, subcommand, 0, 0, data,
                               datalen, msg, status, seq, FALSE);
    g_free(msg);
    return returncode;
  }
}

void TCPProcessPacket( BYTE *packet, int packet_length, int sock, UIN_T uin)
{
  BYTE *here;

  WORD command = 0;
  WORD subcmd = 0;
  WORD message_length;
  gchar *message=NULL;
  WORD sequence =0;
  
  WORD status = 0;
  WORD msgtype;

struct {
    WORD revport;
    WORD name_len;
    char *name;
    DWORD size;
    WORD port;
} tcp_file;

  gchar *desc;
  GSList *messages;
  tcp_message *m = NULL;
  GSList *contact = NULL;

  gchar *countstr, *tmp = NULL;
  ContactPair *cpair;
  GList *contacts = NULL;
  int i, count;

#ifdef TRACE_FUNCTION
  g_print( "TCPProcessPacket\n" );
#endif
  
  packet_print( packet, packet_length,
                PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "Encrypted packet" );
  contact = Find_User( uin );
  if (contact == NULL) {
    g_warning("We should always have a contact here.\n");
    return;
  }

  if (kontakt->have_tcp_connection == OUTGOING1) {
    if (packet_length == 4) {
      kontakt->have_tcp_connection = OUTGOING2;
      return;
    } else {
      g_warning("We have a non-ack while in outgoing1 state.\n");
      return;
    }
  }
  if (kontakt->have_tcp_connection == OUTGOING2) {
    
    /* Send hack for the hello packet*/
    TCPSendHandShakeAck(kontakt->sok);
    kontakt->have_tcp_connection = YES;
    TCPClearQueue(contact);
    return;
  }


  if (kontakt->have_tcp_connection == INCOMING) {
    if (packet_length == 4) {
      kontakt->have_tcp_connection = YES;
      return;
    } else {
      g_warning("We have a non-hack while in outgoing1 state.\n");
      return;
    }
  }
    
  
  if( packet[0] == 0xFF ) /* 0xFF means it's just a "Hello" packet; ignore */
    {
      packet_print( packet, packet_length,
                    PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "HELLO PACKET" );
      return;
    }

  /* We return if the checksum is wrong */
  /* Something is wrong with the checksum checking code... Please help!! */
  if (! tcp_decrypt(packet, packet_length, kontakt->used_version)) {
    g_warning("Wrong checksum\n");
    return;
  }
  
  packet_print( packet, packet_length,
                PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "Decrypted packet" );

  here = packet;

  switch (kontakt->used_version) {
  case 2:    
  case 3:
    
    here += 4+2; /* skipping uin and version*/

    command = Chars_2_DW(here);
    here += 4;

    here += 4; /* skipping our uin */

    subcmd = Chars_2_Word(here);
    here += 2;

    message_length = Chars_2_Word(here);
    here += 2;

    message = g_memdup(here, message_length);
    here += message_length;

    here += 4+4+4+1; /* skip our ip twice and a port and a mode
                        (that's always 4) */

    status = Chars_2_Word(here);
    here += 2;

    msgtype = Chars_2_Word(here);
    here += 2;

    break;

  case 4:
  case 5:
    here += 4+2+4; /* our_uin, version, checksum */
    
    command = Chars_2_DW(here);
    here += 4;
    
    here += 4;  /* our_info */
    
    subcmd = Chars_2_Word(here);
    here += 2;

    message_length = Chars_2_Word(here);
    here += 2;

    message = g_memdup(here, message_length);
    here += message_length;
    
    here += 4+4+4+1; /* skip our ip twice and a port and a mode
                        (that's always 4) */
    
    status = Chars_2_Word(here);
    here += 2;

    msgtype = Chars_2_Word(here);
    here += 2;

    break;
  case 6:
  case 7:
  case 8:
    here += 4; /* checksum(4), */
    
    command = Chars_2_Word(here);
    here += 2;
    
    here += 2; /* something where we always put 0x0E */

    sequence = Chars_2_Word(here);
    here += 2;

    here += 12;  /* 12 zeros */

    subcmd = Chars_2_Word(here);
    here += 2;

    status = Chars_2_Word(here);
    here += 2;

    msgtype = Chars_2_Word(here);
    here += 2;

    message_length = Chars_2_Word(here);
    here += 2;

    message = g_memdup(here, message_length);
    here += message_length;
    
    break;
	default:
	 g_warning("Unknown version used by contact for processing packet: %d\n", kontakt->used_version);
  }


  if ( subcmd == ICQ_CMDxTCP_FILE ) {
    tcp_file.revport = Chars_2_DW( here );
    here += 4;
    tcp_file.name_len = Chars_2_Word( here );
    here += 2;
    tcp_file.name = g_memdup(here, tcp_file.name_len );
    here += tcp_file.name_len;
    tcp_file.size = Chars_2_DW( here );
    here += 4;
    tcp_file.port = Chars_2_DW( here );
    here += 4;
  }

  if (kontakt->used_version <= 5) {

    sequence = Chars_2_Word(here);
  }
  
  /* What is that ? */

  if( toggles->no_new_users == TRUE){
    TCPAckPacket( kontakt->uin, command, NULL, 0, 0, sequence);
    return;
  }
  

  switch( command ) {
  case ICQ_CMDxTCP_START:
    switch( subcmd ) {
    case ICQ_CMDxTCP_MSG:
      packet_print( packet, packet_length,
                    PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "TCP MESSAGE(MSG)" );
      msg_received( uin, message, time( NULL ) );
      TCPAckPacket( uin, subcmd, NULL, 0, 0, sequence);
      break;
    case ICQ_CMDxTCP_READxAWAYxMSG:
    case ICQ_CMDxTCP_READxOCCxMSG:
    case ICQ_CMDxTCP_READxDNDxMSG:
    case ICQ_CMDxTCP_READxNAxMSG:
      packet_print( packet, packet_length,
                    PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "AWAY MSG REQUEST" );

      /* FIXME: Do nothing? */
      TCPAckPacket( uin, ICQ_CMDxTCP_READxAWAYxMSG, NULL, 0, 0, sequence);
      break;

    case ICQ_CMDxTCP_URL:  /* url sent */
      packet_print( packet, packet_length,
                    PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "TCP MESSAGE(URL)" );
 		     			
      desc = strchr( message, '\xFE' ) + 1;
      *(desc-1) = '\0';

      url_received( uin, message, desc, time( NULL ) );
      TCPAckPacket( uin, subcmd, NULL, 0, 0, sequence);
      break;

    case ICQ_CMDxTCP_CONT_LIST:
      packet_print( packet, packet_length,
                    PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "TCP MESSAGE(CONT_LIST)" );
      countstr = message;
      tmp = strchr( tmp, '\xFE' );
      if ( tmp == NULL )
        return; /* Bad Packet */
      *tmp = '\0';
      tmp++;

      count = atoi(countstr);

      if (count == 0)
        return;

      for (i=0; i < count; i++) {
        cpair = g_new0(ContactPair, 1);

        cpair->textuin = tmp;
        tmp = strchr( tmp, '\xFE' );
        if ( tmp == NULL )
          return; /* Bad Packet */
        *tmp = '\0';            
		cpair->textuin = g_strdup(cpair->textuin);
        tmp++;

        cpair->nick = tmp;
        tmp = strchr( tmp, '\xFE' );
        if ( tmp == NULL )
          return; /* Bad Packet */
        *tmp = '\0';
        cpair->nick = g_strdup(cpair->nick);
        tmp++;

        contacts = g_list_append(contacts, cpair);
      }

      contact_list_received (uin, contacts, time(NULL));


      TCPAckPacket( uin, subcmd, NULL, 0, 0, sequence);
      break;

    case ICQ_CMDxTCP_FILE:
      packet_print( packet, packet_length,
                    PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "FILE REQUEST" );
			
      //      Do_File( message, uin, sequence, tcp_file.name, tcp_file.size );
      break;

    default:
      break;
    }
    break;

  case ICQ_CMDxTCP_ACK:
    switch ( subcmd ) {
    case ICQ_CMDxTCP_MSG:
    case ICQ_CMDxTCP_CONT_LIST:
    case ICQ_CMDxTCP_READxAWAYxMSG:
    case ICQ_CMDxTCP_READxOCCxMSG:
    case ICQ_CMDxTCP_READxDNDxMSG:
    case ICQ_CMDxTCP_READxNAxMSG:
    case ICQ_CMDxTCP_URL:

      if( subcmd == ICQ_CMDxTCP_MSG ||
          subcmd == ICQ_CMDxTCP_URL ||
          subcmd == ICQ_CMDxTCP_CONT_LIST ) {
        packet_print( packet, packet_length,
                      PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "MSG/URL/CONT LIST ACK" );

	gnomeicu_spinner_stop();

        messages = kontakt->tcp_msg_queue;
        while( messages != NULL ) {
          m = messages->data;
          if( m->seq == sequence )
            break;
          messages = messages->next;
        }

        if( m != NULL ) {
          g_free( m->data );
          if( m->text != NULL )
            g_free( m->text );
          if( m->timeout )
            gtk_timeout_remove( m->timeout );
          g_free( m );
          kontakt->tcp_msg_queue = g_slist_remove( kontakt->tcp_msg_queue, m );
        }
      }
      else
        packet_print( packet, packet_length,
                      PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "RESPONSE AWAY MSG" );


      if( (status & 0xffff) == ICQ_ACKxTCP_AWAY ||
          (status & 0xffff) == ICQ_ACKxTCP_NA   ||
          (status & 0xffff) == ICQ_ACKxTCP_DND  ||
          (status & 0xffff) == ICQ_ACKxTCP_OCC ) {

        DWORD nstatus;
        switch( status ) {
        case ICQ_ACKxTCP_AWAY:
          nstatus = STATUS_AWAY;
          break;
        case ICQ_ACKxTCP_NA:
          nstatus = STATUS_NA;
          break;
        case ICQ_ACKxTCP_DND:
          nstatus = STATUS_DND;
          break;
        case ICQ_ACKxTCP_OCC:
          nstatus = STATUS_OCCUPIED;
          break;
        default:
          nstatus = STATUS_OFFLINE;
          break;
        }
	
        recv_awaymsg( kontakt->uin,
                      nstatus, message );
      }
      break;

    case ICQ_CMDxTCP_FILE:
      packet_print( packet, packet_length,
                    PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "ACK FILE REQUEST" );
      /*      for(xfer = kontakt->file_xfers;xfer;xfer = xfer->next)
        if ( ( ((XferInfo*)xfer->data)->seq == sequence) && 
             ( ((XferInfo*)xfer->data)->direction == XFER_DIRECTION_SENDING))
          break;

      if (xfer) {
        
        XferInfo *transfer = xfer->data;
        
        kontakt->file_xfers = g_slist_remove(kontakt->file_xfers, transfer);
        
        if( tcp_file.port > 0 ) {
          transfer->port = tcp_file.port;
          if (ft_connectfile( transfer ) == -1)
            ft_cancel_transfer(0, transfer);
        }
      }
      */
      break;

    default:
      break;
    }
    break;

  case ICQ_CMDxTCP_CANCEL:
    switch ( subcmd ) {

    case ICQ_CMDxTCP_FILE:
      packet_print( packet, packet_length,
                    PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "FILE REQ CANCEL" );

      /*      for(xfer = kontakt->file_xfers;xfer;xfer = xfer->next) {
        if (((XferInfo*)xfer->data)->seq == sequence)
          break;
      }

      if (xfer)
        ft_cancel_transfer(0, xfer->data);
      */
      break;

      /* cancel 
  packet_print( packet, packet_length,
                    PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "FILE REQ CANCEL" );

      for(xfer = kontakt->file_xfers;xfer;xfer = xfer->next) {
        if (((XferInfo*)xfer->data)->seq == sequence)
          break;
      }

      if (xfer)
        ft_cancel_transfer(0, xfer->data);

      break;

      */

    default:
      break;
    }
    break;

   default:
    break;
  }

  g_free( message );
}





int TCPFinishConnection( GIOChannel *source, GIOCondition cond, gpointer data )
{
  GSList *contact;
  int sock;

#ifdef TRACE_FUNCTION
  g_print( "TCPFinishConnection(%d)\n", cond );
#endif

  if( cond & G_IO_ERR || cond & G_IO_HUP )
    return FALSE;

  contact = Contacts;

  while( contact != NULL )
    {
      if( kontakt->gioc == source )
        break;
      contact = contact->next;
    }

  if( contact == NULL )
    return FALSE;

  sock = g_io_channel_unix_get_fd( source );

  TCPSendHandShake(kontakt, kontakt->direct_cookie, kontakt->sok);

  if (kontakt->used_version >= 6)
    kontakt->have_tcp_connection = OUTGOING1;
  else {
    TCPClearQueue( contact );
    kontakt->have_tcp_connection = YES;
  }
        
  kontakt->giocw = g_io_add_watch( kontakt->gioc,
                                   G_IO_IN | G_IO_ERR | G_IO_HUP,
                                   TCPReadPacket, NULL );

  return FALSE;
}



int TCPReadPacket( GIOChannel *source, GIOCondition cond, gpointer data )
{
  BYTE *packet;
  gsize bytes_read;
  int sock;
  GSList *contact;

#ifdef TRACE_FUNCTION
  g_print( "TCPReadPacket(" );
  switch( cond )
    {
    case G_IO_IN:
      g_print( "G_IO_IN)\n" );
      break;
    case G_IO_HUP:
      g_print( "G_IO_HUP)\n" );
      break;
    case G_IO_ERR:
      g_print( "G_IO_ERR)\n" );
      break;
    default: 
      g_print( "UNKNOWN)\n" );
    }
#endif

  contact = Contacts;

  while( contact != NULL ) {
    if( kontakt->gioc == source )
      break;
    contact = contact->next;
  }

  if( contact == NULL )
    return FALSE;

  if( kontakt->sok == 0 )
    return FALSE;

  if( cond != G_IO_IN )
    return FALSE; /* FIXME - This will be called if they close connection */

  sock = g_io_channel_unix_get_fd( source );

  if( kontakt->tcp_buf_len == 0 )
    {
      g_io_channel_read_chars( source, (gchar*)&kontakt->tcp_buf_len,
                         2, &bytes_read, NULL );

      if( !bytes_read )
        {
          kontakt->have_tcp_connection = NO;
          kontakt->sok = 0;
          close( sock );
          g_io_channel_shutdown( source, FALSE, NULL);
          kontakt->gioc = 0;
          return FALSE;
        }

      if( bytes_read == 1 )
        {
          bytes_read = 0;
          while( !bytes_read )
            g_io_channel_read_chars( source, (gchar*)&kontakt->tcp_buf_len + 1,
                               1, &bytes_read, NULL );
        }

      kontakt->tcp_buf_len = GINT16_FROM_LE(kontakt->tcp_buf_len); 
      
      kontakt->tcp_buf = (BYTE*)g_malloc0( kontakt->tcp_buf_len );

      return TRUE;
    }


  packet = kontakt->tcp_buf;

  if( packet == NULL )
    return TRUE;

  g_io_channel_read_chars( source, packet + kontakt->tcp_buf_read,
                     kontakt->tcp_buf_len - kontakt->tcp_buf_read,
                     &bytes_read, NULL );

  kontakt->tcp_buf_read += bytes_read;
  if( kontakt->tcp_buf_read != kontakt->tcp_buf_len )
    return FALSE;

  TCPProcessPacket( packet, kontakt->tcp_buf_len, sock, kontakt->uin);

  kontakt->tcp_buf_read = 0;
  kontakt->tcp_buf_len = 0;
  g_free( packet );
  kontakt->tcp_buf = NULL;

  return TRUE;
}

static int TCPTimeout( tcp_message *m )
{
  GSList *contact;

#ifdef TRACE_FUNCTION
  g_print( "TCPTimeout\n" );
#endif

  contact = Find_User( m->uin );
  if( contact == NULL )
    return TRUE;

  gnomeicu_spinner_stop();

  m->dialog = gtk_message_dialog_new (GTK_WINDOW (MainData->window),
                                     GTK_DIALOG_DESTROY_WITH_PARENT,
                                     GTK_MESSAGE_QUESTION, GTK_BUTTONS_OK,
                                     _("Unable to connect to %s."),
                                     kontakt->nick);
  gtk_widget_show( m->dialog );

  return TRUE;
}

void TCPSendHandShake(Contact_Member *contact, DWORD sessionid, int socket)
{
  TLVstack *tlvs;
 
#ifdef TRACE_FUNCTION
  g_print( "TCPSendHandShake\n" );
#endif

  tlvs = new_tlvstack("  ", 2);
  
  switch (contact->used_version) {
  case 2:    
  case 3:
  case 4:
  case 5:
    add_nontlv(tlvs, "\xFF", 1);
    add_nontlv_dw_le(tlvs, contact->used_version);
    if (contact->used_version < 4)
      add_nontlv_dw_le(tlvs, our_port);
    else
      add_nontlv_w_le(tlvs, 0);
    add_nontlv_dw_le(tlvs, atoi (our_info->uin));
    add_nontlv_dw_le(tlvs, our_ip);
    add_nontlv_dw_le(tlvs, our_ip);
    add_nontlv(tlvs, "\x04", 1); /* Mode */
    add_nontlv_dw_le(tlvs, our_port);
    break;
  case 6:
  case 7:
  case 8:
    
    add_nontlv(tlvs, "\xFF", 1);
    add_nontlv_dw_le(tlvs, contact->used_version);
    add_nontlv_dw_le(tlvs, atoi (contact->uin));
    add_nontlv_w_le(tlvs, 0);
    add_nontlv_dw_le(tlvs, our_port);
    add_nontlv_dw_le(tlvs, atoi (our_info->uin));
    add_nontlv_dw_le(tlvs, our_ip);
    add_nontlv_dw_le(tlvs, our_ip);
    add_nontlv(tlvs, "\x04", 1); /* Mode */
    add_nontlv_dw_le(tlvs, our_port);
    add_nontlv_dw_le(tlvs, sessionid); /* NEED SESSION ID */
    add_nontlv_dw_le(tlvs, 50);
    add_nontlv_dw_le(tlvs, 3);
    if (contact->used_version == 7);
    add_nontlv_dw_le(tlvs, 0);

    break;
  default:
    g_warning("Unknown version used by contact for processing packet: %d\n", contact->used_version);
  }

 
  Word_2_Chars(tlvs->beg, tlvs->len-2);
  
  
  /* Send packet */
  write( socket, tlvs->beg, tlvs->len );
  packet_print( tlvs->beg, tlvs->len,
                PACKET_TYPE_TCP | PACKET_DIRECTION_SEND,
                "Sending handshake" );
  free_tlvstack(tlvs);
}
  


void TCPSendHandShakeAck(int socket)
{
  
#ifdef TRACE_FUNCTION
  g_print( "TCPSendHandShakeAck\n" );
#endif

  /* Send packet */
  write( socket, "\x04\x00\x01\x00\x00\x00", 6 );
  packet_print( "\x01\x00\x00\x00", 4,
                PACKET_TYPE_TCP | PACKET_DIRECTION_SEND,
                "Sending handshake ack" );
}
