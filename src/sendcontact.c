#include "common.h"
#include "gtkfunc.h"
#include "listwindow.h"
#include "sendcontact.h"
#include "util.h"
#include "v7send.h"
#include "v7snac13.h"
#include "groups.h"
#include "gnomeicu.h"

#include <gtk/gtk.h>
#include <string.h>

static void move_contacts_to_list( GList *contactpairs, gpointer data );
static void send_contact_list( GList *contactpairs, gpointer data );

void contact_list_window( CONTACT_PTR contact )
{
	GtkWidget *dlg;
        gchar *msg;
	
	g_assert(contact != NULL);

        msg = g_strdup_printf( _("Drag the users you wish to send\n"
				 "to %s to this window, and then click the\n"
				 "\"Send\" button below."), contact->nick ?
				 contact->nick : contact->uin);
	dlg = list_window_new (
		_("Send Contact List"),
		msg, _("_Send"), GTK_STOCK_JUMP_TO,
		send_contact_list, contact,
		NULL);
	g_free(msg);
}

void received_contact_list_window( CONTACT_PTR contact, GList *contacts )
{
  
  GtkWidget *dlg;
  ContactPair *cpair;
  gchar *caption;

	
  g_assert(contact != NULL);

  caption = g_strdup_printf( _("The following contact list was sent by %s.\n"
			       "Please remove the users you don't want to add\n "
			       "and then click the \"Add\" button below."),
			     contact->nick ?  contact->nick : contact->uin);
  dlg = list_window_new ( _("Received Contact List"),
			  caption, GTK_STOCK_ADD, NULL,
			  move_contacts_to_list, NULL, contacts);

  g_free (caption);

	
  while (contacts != NULL) {
    cpair = contacts->data;
    g_free(cpair->textuin);
    g_free(cpair->nick);
    g_free(cpair);
    contacts = g_list_remove(contacts, cpair);
  }

}

void move_contacts_to_list( GList *contactpairs, gpointer data )
{
  GtkWidget *dialog;
  GtkWidget *dropdown;
  GtkWidget *label;
  GtkWidget *g_omenu_menu;
  guint counter, general=0;
  GSList *ginfo;
  GtkWidget *menuitem;
  GList *menuiteml;
  ContactPair *cpair;
  guint gid;


  dialog = gtk_dialog_new_with_buttons(_("Add to group"), NULL, 0,
			  GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			  GTK_STOCK_OK, GTK_RESPONSE_OK,
			  NULL);
	
  label = gtk_label_new (_("These contacts will be added to group:"));

  gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
		     label);

	
  g_omenu_menu = gtk_menu_new();
  
  counter = 0;
  for (ginfo = Groups; ginfo != NULL; ginfo = ginfo->next, counter++) {
    GroupInfo *group = (GroupInfo *) ginfo->data;
    menuitem = gtk_menu_item_new_with_label(group->name);
    g_object_set_data(G_OBJECT(menuitem), "gid",
		      GUINT_TO_POINTER((guint)group->gid));
    gtk_menu_shell_append(GTK_MENU_SHELL(g_omenu_menu), menuitem);
    if (!strcmp(group->name, "General"))
      general = counter;
  }

  dropdown = gtk_option_menu_new();
  gtk_option_menu_set_menu( GTK_OPTION_MENU(dropdown), g_omenu_menu);

  if (general)
    gtk_option_menu_set_history( GTK_OPTION_MENU(dropdown), general);

  gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
		     dropdown);

  gtk_widget_show_all(GTK_DIALOG(dialog)->vbox);

  if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK) {

    menuiteml = g_list_nth(GTK_MENU_SHELL(g_omenu_menu)->children,
			  gtk_option_menu_get_history (GTK_OPTION_MENU(dropdown)));
    gid = GPOINTER_TO_UINT (g_object_get_data(G_OBJECT(menuiteml->data),"gid"));
  

    while (contactpairs) {
      ContactPair *cpair = contactpairs->data;

      v7_try_add_uin (mainconnection, cpair->textuin, cpair->nick, gid, FALSE);

      contactpairs = g_list_next(contactpairs);
    }
  }
  gtk_widget_destroy(GTK_WIDGET(dialog));

}


void send_contact_list( GList *contactpairs, gpointer data )
{

  Contact_Member *contact = data;

#ifdef TRACE_FUNCTION
  g_print( "send_contact_list\n" );
#endif

  if (!is_connected(NULL, _("You can not send contacts while disconnected.")))
    return;

  if (contactpairs == NULL)
    return;
  
  printf("sending\n");
        
  v7_sendcontacts(mainconnection, contact->uin, contactpairs);

}
