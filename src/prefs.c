/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * Manage preferences using gconf
 *
 * Copyright (C) 2002-2003 Patrick Sung
 */

#include "common.h"
#include "gnomeicu.h"

#include <glib.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include <libgnomeui/gnome-color-picker.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "prefs_ui.h"
#include "util.h"

enum { /* for the icon list on the prefs dialog */
  PREFS_CATEGORY,
  PREFS_CAT_N_COL
};

typedef struct _GnomeICUPrefCategory GnomeICUPrefCategory;
typedef struct _GnomeICUPref GnomeICUPref;
typedef gint group_rel[]; /* sensitivities relationships */

#define PR_SENSITIVITY_BEGIN -20
#define PR_SENSITIVITY_BEGIN_INVERSE -21
#define PR_END -22

/* store each icon's info (gnomeicu, icq...) */
struct _GnomeICUPrefCategory {
  gchar *icon_text;
  gchar *icon_file_name;
  gchar *widget_name;
  GnomeICUPref *prefs_array;
  GnomeICUPrefData *prefs_data;
  GdkPixbuf *icon;
  GtkWidget *page_widget;
};

struct _GnomeICUPref {
  gchar *gconf_key;
  gchar *widget_name;
  enum GnomeICUPrefType type;
  gpointer pr; /* preference relations (sensitivities... etc... ) */
};

group_rel auto_away_group = { PR_SENSITIVITY_BEGIN, 1, PR_END };

GnomeICUPref gnomeicu_prefs[] = {
  { PREFS_GNOMEICU_STARTUP_CONNECT, "gicu_startup_connect", PREF_BOOLEAN, NULL },
  { PREFS_GNOMEICU_AUTO_RECONNECT, "gicu_auto_reconnect", PREF_BOOLEAN, NULL },
  { PREFS_GNOMEICU_AUTO_ACCEPT_FILE, "gicu_auto_accept_file", PREF_BOOLEAN, NULL },
  { PREFS_GNOMEICU_DOWNLOAD_PATH, "gicu_download_path_chooser", PREF_PATH, NULL },
  /*  { PREFS_ICQ_SERVER_NAME, "icq_server_name", PREF_STRING, NULL },
      { PREFS_ICQ_SERVER_PORT, "icq_server_port", PREF_UINT, NULL },*/
  /*
  { PREFS_GNOMEICU_DATA_PATH, "gicu_path_entry", PREF_PATH, (gpointer)"data_path_browse" },
  { PREFS_GNOMEICU_CONTACT_FILE, "gicu_contact_list_entry", PREF_PATH, (gpointer)"contact_list_browse" },
  */

  { PREFS_GNOMEICU_SOUND, "gicu_sounds", PREF_BOOLEAN, NULL },
  { PREFS_GNOMEICU_SOUND_ONLINE, "gicu_sounds_online", PREF_BOOLEAN, NULL },
  { PREFS_GNOMEICU_BEEPS, "gicu_beeps", PREF_BOOLEAN, NULL },
  { PREFS_GNOMEICU_GNOME_SOUND_PROPERTIES, "gicu_launch_gnome_sound", PREF_EXEC, NULL},
  { PREFS_MULTIKEY, "gicu_program_event_treeview", PREF_CUSTOM, &program_event_treeview },

  { PREFS_GNOMEICU_AUTO_AWAY, "gicu_auto_away", PREF_BOOLEAN, (gpointer)auto_away_group },
  { PREFS_GNOMEICU_AWAY_TIMEOUT, "gicu_away_timeout", PREF_UINT, NULL },
  { PREFS_GNOMEICU_AWAY_DEFAULT, "gicu_away_msg_combo", PREF_CUSTOM, &away_message_widgets },

  { PREFS_GNOMEICU_AUTO_POPUP, "gicu_auto_popup", PREF_BOOLEAN, NULL },
  { PREFS_GNOMEICU_SPELLCHECK, "gicu_spellcheck", PREF_BOOLEAN, NULL },
  { PREFS_GNOMEICU_DEFAULT_CHAT_MODE, "gicu_default_chat_mode", PREF_BOOLEAN, NULL },
  { PREFS_GNOMEICU_CHARSET_FALLBACK, "gicu_charset_fallback_combo", PREF_COMBO, &lang_combo_box },
  { PREFS_GNOMEICU_ENTER_SENDS, "gicu_enter_sends_radio", PREF_BOOLEAN, NULL},
  { PREFS_GNOMEICU_ICON_THEMES, "gicu_icon_theme_treeview", PREF_CUSTOM, &icon_theme_treeview },
  { PREFS_GNOMEICU_EMOTICON_THEMES, "gicu_emoticon_theme_treeview", PREF_CUSTOM, &emoticon_theme_treeview },

  { NULL, NULL, PREF_NONE, NULL }
};

GnomeICUPref icq_prefs[] = {
  /*  { PREFS_ICQ_TCP_PORT_MIN, "icq_tcp_port_min", PREF_UINT, NULL }, */
  /*  { PREFS_ICQ_TCP_PORT_MAX, "icq_tcp_port_max", PREF_UINT, NULL }, */

  { PREFS_ICQ_COLOR_ONLINE, "icq_online_color", PREF_COLOR, NULL },
  { PREFS_ICQ_COLOR_AWAY, "icq_away_color", PREF_COLOR, NULL },
  { PREFS_ICQ_COLOR_NA, "icq_na_color", PREF_COLOR, NULL },
  { PREFS_ICQ_COLOR_OCCUPIED, "icq_occupied_color", PREF_COLOR, NULL },
  { PREFS_ICQ_COLOR_DND, "icq_dnd_color", PREF_COLOR, NULL },
  { PREFS_ICQ_COLOR_INVISIBLE, "icq_invisible_color", PREF_COLOR, NULL },
  { PREFS_ICQ_COLOR_OFFLINE, "icq_offline_color", PREF_COLOR, NULL },
  { PREFS_ICQ_COLOR_FREEFORCHAT, "icq_freeforchat_color", PREF_COLOR, NULL },

  { NULL, NULL, PREF_NONE, NULL }
};

GnomeICUPrefCategory prefs_category[] = {
  { N_("GnomeICU"), GNOMEICU_DATADIR "/gnomeicu/prefs_icon_gnomeicu.png", "gnomeicu_pref_page", gnomeicu_prefs, NULL, NULL, NULL },
  { N_("ICQ"), GNOMEICU_DATADIR "/gnomeicu/prefs_icon_icq.png", "icq_pref_page", icq_prefs, NULL, NULL, NULL },
  { NULL, NULL, NULL, NULL, NULL, NULL }
};

static gboolean prefs_init = FALSE;
static GConfClient *client = NULL;
static GtkWidget *prefs_dialog = NULL;

static void set_spin_button (const GnomeICUPrefData *pd);
static void set_toggle_button (const GnomeICUPrefData *pd);
static void set_editable (const GnomeICUPrefData *pd);
static void set_color_picker (const GnomeICUPrefData *pd);
static void set_file_chooser (const GnomeICUPrefData *pd);

/*
 * Initialize preferences (gconf_client)
 */
void
preferences_init (void)
{
  if (prefs_init)
    return;

  client = gconf_client_get_default ();
  gconf_client_add_dir (client, GNOMEICU_GCONF, GCONF_CLIENT_PRELOAD_NONE,
                        NULL);

  prefs_init = TRUE;
}

static void
preferences_invalid_value (const gchar *gconf_key)
{
  GtkWidget *dialog;

  gchar msg[] = N_("You have reached this dialog because we detected you do "
                   "not have a proper configuration key installed for GnomeICU."
                   " The possible cause is that you forgot to install the "
                   "configuration keys from gnomeicu.schemas. Please contact "
                   "your package maintainer about that, or you can contact us "
                   "directly at gnomeicu-support@lists.sourceforge.net\n\n"

                   "The configuration key in question is: %s\n\n"

                   "The program will now close...");

  gchar *str;

  str = g_strdup_printf (_(msg), gconf_key);

  dialog = gtk_message_dialog_new (GTK_WINDOW (MainData->window),
                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                   GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, str);
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);

  g_free (str);

  exit(1);
  return;
}

GSList *
preferences_get_list (const gchar *key, GConfValueType ltype)
{
  GSList *ret_list;

  g_return_val_if_fail (key, NULL);

  if (!prefs_init)
    preferences_init ();

  ret_list = gconf_client_get_list (client, key, ltype, NULL);

  return ret_list;
}

gboolean
preferences_get_pair (const gchar *key, GConfValueType car_type,
                      GConfValueType cdr_type, gpointer car_retloc,
                      gpointer cdr_retloc)
{
  gboolean retval;

  g_return_val_if_fail (key || car_retloc || cdr_retloc, FALSE);

  if (!prefs_init)
    preferences_init ();

  retval = gconf_client_get_pair (client, key, car_type, cdr_type,
                                  car_retloc, cdr_retloc, NULL);

  return retval;
}

/* return a newly allocated string, need to free it after use */
gchar *
preferences_get_string (const gchar *gconf_key)
{
  gchar *str;

  g_return_val_if_fail (gconf_key, NULL);

  if (!prefs_init)
    preferences_init ();

  str = gconf_client_get_string (client, gconf_key, NULL);

  if (str == NULL)
    preferences_invalid_value (gconf_key);

  return str;
}

gint
preferences_get_int (const gchar *gconf_key)
{
  g_return_val_if_fail (gconf_key, 0);

  if (!prefs_init)
    preferences_init ();

  return gconf_client_get_int (client, gconf_key, NULL);
}

gboolean
preferences_get_bool (const gchar *gconf_key)
{
  g_return_val_if_fail (gconf_key, FALSE);

  if (!prefs_init)
    preferences_init ();

  return gconf_client_get_bool (client, gconf_key, NULL);
}

GdkColor *
preferences_get_color (const gchar *gconf_key)
{
  gchar *color_string;
  GdkColor *my_color;

  g_return_val_if_fail (gconf_key, NULL);

  if (!prefs_init)
    preferences_init ();

  color_string = gconf_client_get_string (client, gconf_key, NULL);
  my_color = g_new0 (GdkColor, 1);

  if (color_string == NULL)
    preferences_invalid_value (gconf_key);

  sscanf (color_string, "#%2hX%2hX%2hX",
          &my_color->red, &my_color->green, &my_color->blue);

  g_free (color_string);

  return my_color;
}

void
preferences_set_list (const gchar *gconf_key, GConfValueType car_type,
                      GSList *list)
{
  g_return_if_fail (gconf_key || list);

  if (!prefs_init)
    preferences_init ();

  gconf_client_set_list (client, gconf_key, car_type, list, NULL);
}

void
preferences_set_pair (const gchar *gconf_key, GConfValueType car_type,
                      GConfValueType cdr_type, gconstpointer addr_car,
                      gconstpointer addr_cdr)
{
  g_return_if_fail (gconf_key || addr_car || addr_cdr);

  if (!prefs_init)
    preferences_init ();

  gconf_client_set_pair (client, gconf_key, car_type, cdr_type,
                         addr_car, addr_cdr, NULL);
}

void
preferences_set_string (const gchar *gconf_key, const gchar *data)
{
  g_return_if_fail (gconf_key || data);

  if (!prefs_init)
    preferences_init ();

  gconf_client_set_string (client, gconf_key, data, NULL);
}

void
preferences_set_int (const gchar *gconf_key, gint data)
{
  g_return_if_fail (gconf_key);

  if (!prefs_init)
    preferences_init ();

  gconf_client_set_int (client, gconf_key, data, NULL);
}

void
preferences_set_bool (const gchar *gconf_key, gboolean data)
{
  g_return_if_fail (gconf_key);

  if (!prefs_init)
    preferences_init ();

  gconf_client_set_bool (client, gconf_key, data, NULL);
}

void
preferences_set_color (const gchar *gconf_key, const GdkColor *data)
{
  gchar *color_string;

  g_return_if_fail (gconf_key || data);

  if (!prefs_init)
    preferences_init ();

  color_string = g_strdup_printf ("#%02X%02X%02X", data->red, data->green,
                                  data->blue);

  gconf_client_set_string (client, gconf_key, color_string, NULL);

  g_free (color_string);
}

void 
preferences_unset(const gchar *gconf_key)
{
  g_return_if_fail (gconf_key);
  
  if (!prefs_init)
    preferences_init ();

  gconf_client_unset (client, gconf_key, NULL);
}

void
preferences_notify_cb (GConfClient *client, guint cnxn_id,
                       GConfEntry *entry, gpointer func)
{
  ((void (*)())func) ();
}

guint
preferences_watch_key (const gchar *gconf_key, void (*func)(void))
{
  return gconf_client_notify_add (client, gconf_key, preferences_notify_cb,
                                  (gpointer)func, NULL, NULL);
}

static void
preferences_set_pref_widget_relation (const GnomeICUPrefData *pd, gint i)
{
  gint *pr = pd[i].pr;
  gboolean active = FALSE;

  if (pr == NULL)
    return;

  if (GTK_IS_TOGGLE_BUTTON (pd[i].widget)) {
    active = (gboolean)GPOINTER_TO_INT(pd[i].value);
  }

  if (*pr == PR_SENSITIVITY_BEGIN_INVERSE)
    active = !active;
  pr++;

  while (*pr != PR_END) {
    gtk_widget_set_sensitive (pd[i+*pr].widget, active);
    pr++;
  }
}


static void
preferences_browse_button_changed_cb (GtkFileChooser *chooser, GnomeICUPrefData *pd)
{
  gchar *filename = NULL;

  filename = gtk_file_chooser_get_filename(chooser);

  if (pd->value)
    g_free (pd->value);

  pd->value = filename;

  preferences_set_string (pd->key, filename);

}

static void
preferences_spinner_value_changed_cb (GtkSpinButton *widget,
                                      GnomeICUPrefData *pd)
{
  gint value;

  value = gtk_spin_button_get_value_as_int (widget);

  pd->value = GINT_TO_POINTER(value);

  preferences_set_int (pd->key, value);
}

static void
preferences_toggle_button_toggled_cb (GtkToggleButton *widget,
                                      GnomeICUPrefData *pd)
{
  gboolean b;

  b = gtk_toggle_button_get_active (widget);

  preferences_set_bool (pd->key, b);
  pd->value = GINT_TO_POINTER(b);

  preferences_set_pref_widget_relation (pd, 0);
}

static void
preferences_editable_changed_cb (GtkEditable *widget, GnomeICUPrefData *pd)
{
  gchar *value;

  value = gtk_editable_get_chars (widget, 0, -1);

  switch (pd->type) {
  case PREF_STRING:
    preferences_set_string (pd->key, value);
    break;
  case PREF_UINT:
    preferences_set_int (pd->key, atoi (value));
    break;
  }

  g_free (value);
}

static void
preferences_button_clicked_exec_cb (GtkWidget *widget, GnomeICUPrefData *pd)
{
  if (pd->value)
    g_spawn_command_line_async (pd->value, NULL);
}

static void
preferences_color_picker_cb (GnomeColorPicker *colorpicker,
                             guint arg1, guint arg2, guint arg3, guint arg4,
                             GnomeICUPrefData *pd)
{
  GdkColor color;
  guint8 r, g, b, a;
  gchar *color_string;

  gnome_color_picker_get_i8 (colorpicker, &r, &g, &b, &a);

  color_string = g_strdup_printf ("#%02X%02X%02X", r, g, b);
  g_free (pd->value);
  pd->value = color_string;

  color.red = r;
  color.green = g;
  color.blue = b;
  preferences_set_color (pd->key, &color);
}

static void
preferences_connect_signal_spin_button (GnomeICUPrefData *pd)
{
  g_signal_connect (G_OBJECT (pd->widget), "value-changed",
                    G_CALLBACK (preferences_spinner_value_changed_cb),
                    (gpointer)pd);
}

static void
preferences_connect_signal_toggle_button (GnomeICUPrefData *pd)
{
  g_signal_connect (G_OBJECT (pd->widget), "toggled",
                    G_CALLBACK (preferences_toggle_button_toggled_cb),
                    (gpointer)pd);
}

static void
preferences_connect_signal_editable (GnomeICUPrefData *pd)
{
  g_signal_connect (G_OBJECT (pd->widget), "changed",
                    G_CALLBACK (preferences_editable_changed_cb),
                    (gpointer)pd);
}

static void
preferences_connect_signal_browse_button (GnomeICUPrefData *pd)
{

  g_signal_connect (G_OBJECT (pd->widget), "selection-changed",
                    G_CALLBACK (preferences_browse_button_changed_cb),
                    (gpointer)pd);
}

static void
preferences_connect_signal_color_picker (GnomeICUPrefData *pd)
{
  g_signal_connect (G_OBJECT (pd->widget), "color-set",
                    G_CALLBACK (preferences_color_picker_cb),
                    (gpointer)pd);
}

static void
preferences_connect_signal_button (GnomeICUPrefData *pd)
{
  switch (pd->type) {
  case PREF_EXEC:
    g_signal_connect (G_OBJECT (pd->widget), "clicked",
                      G_CALLBACK (preferences_button_clicked_exec_cb),
                      (gpointer)pd);
  }
}

/* connect signals for all prefs widget according to widget type */
static void
preferences_connect_signal (GnomeICUPrefData *pd)
{
  gint i;

  for (i = 0; pd[i].key != NULL; i++) {
    g_return_if_fail (pd[i].widget != NULL);

    if (GTK_IS_SPIN_BUTTON (pd[i].widget)) {
      preferences_connect_signal_spin_button (&pd[i]);
    } else if (GTK_IS_TOGGLE_BUTTON (pd[i].widget)) {
      preferences_connect_signal_toggle_button (&pd[i]);
    } else if (GTK_IS_EDITABLE (pd[i].widget)) {
      preferences_connect_signal_editable (&pd[i]);
    } else if (GTK_IS_FILE_CHOOSER_BUTTON(pd[i].widget)) {
      preferences_connect_signal_browse_button (&pd[i]);
    } else if (GNOME_IS_COLOR_PICKER (pd[i].widget)) {
      preferences_connect_signal_color_picker (&pd[i]);
    } else if (GTK_IS_BUTTON (pd[i].widget)) {
      preferences_connect_signal_button (&pd[i]);
    } else if (GTK_IS_COMBO (pd[i].widget)) {
      ((prefs_action *)(pd[i].pr))->connect_signal (&pd[i]);
    } else { /* for custom prefs UI (PREF_CUSTOM) */
      ((prefs_action *)(pd[i].pr))->connect_signal (&pd[i]);
    }
  }
}

void
preferences_default_init_value (GnomeICUPrefData *pd)
{
  pd->value = (gpointer)preferences_get_string (pd->key);
}

/* extract prefs from gconf to a private structure, called only once */
static GnomeICUPrefData *
init_prefs (GladeXML *gxml, const GnomeICUPref *prefs)
{
  GnomeICUPrefData *pd;
  GtkWidget *widget;
  gint i;

  if (!prefs_init)
    preferences_init ();

  for (i = 0; prefs[i].gconf_key != NULL; i++);

  pd = g_new0 (GnomeICUPrefData, i+1);
  
  for (i = 0; prefs[i].gconf_key != NULL; i++) {
    pd[i].key = prefs[i].gconf_key;
    pd[i].widget = glade_xml_get_widget (gxml, prefs[i].widget_name);
    pd[i].type = prefs[i].type;
    pd[i].pr = prefs[i].pr;

    switch (prefs[i].type) {
    case PREF_UINT:
      pd[i].value = GINT_TO_POINTER(gconf_client_get_int (client, 
							  prefs[i].gconf_key,
							  NULL));
      break;
    case PREF_BOOLEAN:
      pd[i].value = GINT_TO_POINTER(gconf_client_get_bool (client, 
							   prefs[i].gconf_key,
							   NULL));
      {
	gchar *name = g_strdup_printf("%s_other", prefs[i].widget_name);
	GtkWidget *tmpwidget;
	tmpwidget = glade_xml_get_widget (gxml, name);
	if (tmpwidget) {
	  g_object_set_data(G_OBJECT(pd[i].widget), "other", tmpwidget);
	}
      g_free(name);
      }
      break;
    case PREF_STRING:
    case PREF_COLOR:
    case PREF_EXEC:
      pd[i].value = (gpointer)gconf_client_get_string (client,
                                                       prefs[i].gconf_key, NULL);
      break;
    case PREF_PATH:
      pd[i].value = (gpointer)gconf_client_get_string (client,
                                                       prefs[i].gconf_key, NULL);
      if (!pd[i].value || *(gchar*)pd[i].value == 0 ||
	  !g_path_is_absolute(pd[i].value)) {
	/* default to home */
	g_free(pd[i].value);
	
	pd[i].value = g_strdup(g_get_home_dir());
      }
      break;
    case PREF_COMBO:
    case PREF_CUSTOM:
      pd[i].value = gxml;
      if ( ((prefs_action *)(pd[i].pr))->init_value == NULL )
        preferences_default_init_value (&pd[i]);
      else
        ((prefs_action *)(pd[i].pr))->init_value (&pd[i]);
      break;
    }
  }

  /* make sure the last item is NULL */
  pd[i].key = NULL;
  pd[i].widget = NULL;
  pd[i].pr = NULL;

  return pd;
}

/*
 * This function loads the preference from GConf to the global
 * GnomeICUPrefData structure, which holds the widget and the data associated
 * This also set widgets value with the value loaded from GConf
 */
static void
load_prefs (const GnomeICUPrefData *pd)
{
  gint i;

  for (i = 0; pd[i].key != NULL; i++) {
    g_return_if_fail (pd[i].widget != NULL);

    if (GTK_IS_SPIN_BUTTON (pd[i].widget)) {
      set_spin_button (&pd[i]);
    } else if (GTK_IS_TOGGLE_BUTTON (pd[i].widget)) {
      set_toggle_button (&pd[i]);
      preferences_set_pref_widget_relation (pd, i);
    } else if (GTK_IS_EDITABLE (pd[i].widget)) {
      set_editable (&pd[i]);
    } else if (GNOME_IS_COLOR_PICKER (pd[i].widget)) {
      set_color_picker (&pd[i]);
    } else if (GTK_IS_COMBO (pd[i].widget)) {
      ((prefs_action *)(pd[i].pr))->set_widget (&pd[i]);
    } else if (GTK_IS_FILE_CHOOSER(pd[i].widget)) {
      set_file_chooser(&pd[i]);
    } else if (GTK_IS_BUTTON (pd[i].widget)) {
    } else { /* for custom prefs UI (PREF_CUSTOM) */
      ((prefs_action *)(pd[i].pr))->set_widget (&pd[i]);
    }
  }
}

/* remove the current category (icon) selected, thus removing the whole notebook
 * widget
 */
static void
preferences_unload_current_page ()
{
  GtkWidget *hbox;
  GnomeICUPrefCategory *cat;

  hbox = g_object_get_data (G_OBJECT (prefs_dialog), "hbox");
  cat = g_object_get_data (G_OBJECT (prefs_dialog), "current_cat");

  if (cat && hbox)
    gtk_container_remove (GTK_CONTAINER (hbox), cat->page_widget);
}

/*
 * This will load a new category (i.e. General, ICQ, AIM etc), with notebook
 * pages for each category
 */
static void
preferences_load_page (GnomeICUPrefCategory *cat)
{
  GladeXML *gxml;

  if (!cat->page_widget) {
    gxml = gicu_util_open_glade_xml ("prefs.glade", cat->widget_name);
    if (gxml == NULL)
      return;

    cat->page_widget = glade_xml_get_widget (gxml, cat->widget_name);
    /* make a ref for each page created for the first time, so it won't be
     * deleted when calling gtk_container_remove() when unloading
     */
    g_object_ref (cat->page_widget);

    cat->prefs_data = init_prefs (gxml, cat->prefs_array);
    preferences_connect_signal (cat->prefs_data);
#ifndef WITH_GTKSPELL
    /* disable spell checking option if compiled without gtkspell */
    if (glade_xml_get_widget (gxml, "gicu_spellcheck"))
      gtk_widget_hide ( glade_xml_get_widget (gxml, "gicu_spellcheck"));
#endif

    g_object_unref (gxml);
  }

  gtk_box_pack_start (GTK_BOX (g_object_get_data(G_OBJECT(prefs_dialog),"hbox")),
                      cat->page_widget, TRUE, TRUE, 0);

  load_prefs (cat->prefs_data);

  g_object_set_data (G_OBJECT (prefs_dialog), "current_cat", cat);

  gtk_widget_show_all (cat->page_widget);
}

static void
preferences_change_page_cb (GtkTreeSelection *sel, gpointer d)
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  GnomeICUPrefCategory *cat;

  if (gtk_tree_selection_get_selected (sel, &model, &iter)) {
    gtk_tree_model_get (model, &iter, PREFS_CATEGORY, &cat, -1);

    preferences_unload_current_page ();
    preferences_load_page (cat);
  }
}

static void
column_set_func_pixbuf (GtkTreeViewColumn *column, GtkCellRenderer *rend,
                        GtkTreeModel *model, GtkTreeIter *iter, gpointer data)
{
  GnomeICUPrefCategory *cat;

  gtk_tree_model_get (model, iter, PREFS_CATEGORY, &cat, -1);

  g_object_set (GTK_CELL_RENDERER (rend), "pixbuf", cat->icon, NULL);
}

/* create the left side icon list and connect the signals */
static void
preferences_icon_list (GladeXML *gxml)
{
  GtkWidget *treeview;
  GtkListStore *store;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  GtkTreeSelection *select;
  GtkTreeIter iter;
  gint i;

  treeview = glade_xml_get_widget (gxml, "prefs_treeview");

  store = gtk_list_store_new (PREFS_CAT_N_COL, G_TYPE_POINTER);
  gtk_tree_view_set_model (GTK_TREE_VIEW (treeview), GTK_TREE_MODEL (store));

  for (i = 0; prefs_category[i].icon_text != NULL; i++) {
    prefs_category[i].icon = gdk_pixbuf_new_from_file (prefs_category[i].icon_file_name, NULL);

    gtk_list_store_append (store, &iter);
    gtk_list_store_set (store, &iter,
                        PREFS_CATEGORY, &prefs_category[i], -1);
  }

  renderer = gtk_cell_renderer_pixbuf_new ();
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_set_cell_data_func (column, renderer,
                                           column_set_func_pixbuf, NULL, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);

  select = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
  gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);

  g_signal_connect (G_OBJECT (select), "changed",
                    G_CALLBACK (preferences_change_page_cb),
                    NULL);

  /* select the first icon */
  gtk_tree_model_get_iter_first (GTK_TREE_MODEL (store), &iter);
  gtk_tree_selection_select_iter (select, &iter);

  gtk_widget_show_all (GTK_WIDGET (treeview));
}

static void
preferences_dialog_response_cb (GtkDialog *dialog, gint arg1, gpointer d)
{
  switch (arg1) {
  case GTK_RESPONSE_CLOSE:
  case GTK_RESPONSE_NONE:
  case GTK_RESPONSE_DELETE_EVENT:
    gtk_widget_hide (GTK_WIDGET (dialog));
    break;
  }
}

/* create (first time load) or show the dialog, when called */
void
preferences_dialog_show (void)
{
  GladeXML *gxml;
  GtkWidget *hbox;

  if (prefs_dialog) {
    /* just show it if we created it already */
    load_prefs (((GnomeICUPrefCategory*)g_object_get_data (G_OBJECT (prefs_dialog), 
					 "current_cat"))->prefs_data);
    gtk_window_present (GTK_WINDOW (prefs_dialog));
    return;
  }

  gxml = gicu_util_open_glade_xml ("prefs.glade", "prefs_dialog");
  if (gxml == NULL)
    return;

  prefs_dialog = glade_xml_get_widget (gxml, "prefs_dialog");
  hbox = glade_xml_get_widget (gxml, "prefs_hbox");
  g_object_set_data (G_OBJECT (prefs_dialog), "hbox", hbox);

  g_signal_connect (G_OBJECT (prefs_dialog), "response",
                    G_CALLBACK (preferences_dialog_response_cb), NULL);
  g_signal_connect (G_OBJECT (prefs_dialog), "delete-event",
                    G_CALLBACK (gtk_true), NULL);

  /* populate the icon list */
  preferences_icon_list (gxml);

  glade_xml_signal_autoconnect (gxml);
  g_object_unref (gxml);
}

void
preferences_dialog_show_cb (GtkWidget *w, gpointer d)
{
  preferences_dialog_show ();
}

void
set_spin_button (const GnomeICUPrefData *pd)
{
  switch (pd->type) {
  case PREF_UINT:
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (pd->widget), 
			       GPOINTER_TO_UINT(pd->value));
    break;
  }
}

void
set_toggle_button (const GnomeICUPrefData *pd)
{
  GtkWidget *widget;

  switch (pd->type) {
  case PREF_BOOLEAN:
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (pd->widget),
                                  GPOINTER_TO_INT(pd->value));
    widget = (GtkWidget *) g_object_get_data(G_OBJECT(pd->widget), "other");
    if (widget)
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget),
				    ! GPOINTER_TO_INT(pd->value));
      
    break;
  }
}

void
set_editable (const GnomeICUPrefData *pd)
{
  gchar *str;

  switch (pd->type) {
  case PREF_STRING:
    gtk_entry_set_text (GTK_ENTRY (pd->widget), pd->value);
    break;
  case PREF_UINT:
    str = g_strdup_printf ("%d", GPOINTER_TO_INT(pd->value));
    gtk_entry_set_text (GTK_ENTRY (pd->widget), str);
    g_free (str);
    break;
  }
}

void
set_color_picker (const GnomeICUPrefData *pd)
{
  gchar *color_string;
  guint r, g, b;

  if (pd->type != PREF_COLOR)
    return;

  color_string = (gchar *)pd->value;

  if (color_string == NULL)
    return;

  sscanf (color_string, "#%2X%2X%2X", &r, &g, &b);
  gnome_color_picker_set_i8 (GNOME_COLOR_PICKER (pd->widget), r, g, b, 0);
}


void
set_file_chooser (const GnomeICUPrefData *pd)
{
  switch (pd->type) {
  case PREF_PATH:
    gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(pd->widget), pd->value);
    break;
  }
}
