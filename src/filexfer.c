/********************************
 File transfer functions
 (c) 1999 Jeremy Wise
 GnomeICU
*********************************/

#include "common.h"
#include "filexfer.h"
#include "filexferdlg.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "history.h"
#include "packetprint.h"
#include "tcp.h"
#include "util.h"

#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <gtk/gtk.h>
#include <libgnomeui/libgnomeui.h>

static int ft_next_file( XferInfo *xfer );
static int ft_handshake( GIOChannel *source, GIOCondition condition,
                         XferInfo *xfer );
static int ft_filereadserver( GIOChannel *source, GIOCondition condition,
                       XferInfo *xfer );
static int ft_filereadclient( GIOChannel *source, GIOCondition condition,
                       XferInfo *xfer );
static void ft_sendfileinfo( XferInfo *xfer );
static void ft_skip_file( GtkWidget *, XferInfo *xfer);
static int ft_sendpartfile( GIOChannel *source, GIOCondition condition,
                     XferInfo *xfer );


int ft_connectfile( XferInfo *xfer )
{

  DWORD localport;
  struct sockaddr_in local, remote;
  int sizeofSockaddr = sizeof( struct sockaddr );
/*   BYTE *buffer; */
/*   int index, psize; */

/*   struct { */
/*     BYTE id; /\* 0xff *\/ */
/*     BYTE version[4]; /\* 0x00000003 *\/ */
/*     BYTE port[4]; /\* TCP listen port *\/ */
/*     BYTE uin[4]; /\* UIN *\/ */
/*     BYTE ipa[4]; /\* IP A *\/ */
/*     BYTE ipb[4]; /\* IP B *\/ */
/*     BYTE four; /\* 0x04 *\/ */
/*     BYTE listenport[4]; /\* File listen port *\/ */
/*   } handshake; */

#ifdef TRACE_FUNCTION
  g_print( "ft_connectfile\n" );
#endif

  xfer->status = XFER_STATUS_CONNECTING;

  if( xfer->sock_fd > 0 )
    return xfer->sock_fd;

  if (!xfer->remote_contact->current_ip )
    /* || !xfer->remote_contact->has_direct_connect) */
    return -1;

  xfer->sock_fd = socket( PF_INET, SOCK_STREAM, 0 );
  if( xfer->sock_fd == -1 )
    return -1;

  /* fcntl( xfer->sock_fd, O_NONBLOCK ); */

  local.sin_family = AF_INET;
  remote.sin_family = AF_INET;
  local.sin_port = g_htons( 0 );
  local.sin_addr.s_addr = g_htonl( INADDR_ANY );

  remote.sin_port = g_htons( xfer->port );

  remote.sin_addr.s_addr = g_htonl( xfer->remote_contact->current_ip );

  if( connect( xfer->sock_fd, (struct sockaddr *)&remote, sizeofSockaddr ) < 0 )
  {
    perror("connect");
    return -1;
  }

  getsockname( xfer->sock_fd, (struct sockaddr*)&local, &sizeofSockaddr );
  localport = g_ntohs( local.sin_port );


  TCPSendHandShake(xfer->remote_contact, xfer->remote_contact->direct_cookie,
		   xfer->sock_fd);

  /*
  handshake.id = 0xff;
  DW_2_Chars( handshake.version, 0x00000003 );
  DW_2_Chars( handshake.port, our_port );
  DW_2_Chars( handshake.uin, atoi (our_info->uin) );
  DW_2_CharsBE( handshake.ipa, our_ip );
  DW_2_CharsBE( handshake.ipb, LOCALHOST );
  handshake.four = 0x04;
  DW_2_Chars( handshake.listenport, localport );
  */

  /*   fcntl( xfer->sock_fd, O_NONBLOCK ); */

  xfer->gioc = g_io_channel_unix_new(xfer->sock_fd);

  g_io_add_watch_full( xfer->gioc, G_PRIORITY_DEFAULT_IDLE,
        G_IO_IN | G_IO_ERR | G_IO_HUP | G_IO_NVAL,
        (GIOFunc) ft_filereadserver,
        xfer, NULL );

  /*
  index = 0;
  psize = sizeof( handshake );
  buffer = (BYTE*)g_malloc( psize + 2 );

  Word_2_Chars (buffer, psize);
  index += 2;
  memcpy( buffer + index, &handshake, sizeof( handshake ) );

  packet_print( buffer, psize+2,
                PACKET_TYPE_TCP | PACKET_DIRECTION_SEND, "FILE(INIT)" );
  write( xfer->sock_fd, buffer, psize + 2 );
  g_free( buffer );
  */

  return xfer->sock_fd;
}

int ft_handshake( GIOChannel *source, GIOCondition condition,
                      XferInfo *xfer )
{
  int new_sock;
  int size = sizeof( struct sockaddr );
  struct sockaddr_in their_addr;

#ifdef TRACE_FUNCTION
  g_print( "ft_handshake\n" );
#endif

  new_sock = accept( xfer->sock_fd, ( struct sockaddr * )&their_addr, &size );

        if (xfer->gioc)
                g_io_channel_shutdown(xfer->gioc, TRUE, NULL);

  xfer->sock_fd = new_sock;
  xfer->gioc = g_io_channel_unix_new(new_sock);

  xfer->port = g_ntohs( their_addr.sin_port );

  /*fcntl( new_sock, O_NONBLOCK ); */

  g_io_add_watch_full( xfer->gioc, G_PRIORITY_DEFAULT_IDLE,
          G_IO_IN | G_IO_ERR | G_IO_HUP,
          (GIOFunc) ft_filereadclient,
          xfer, NULL );

  return FALSE;
}

int ft_filereadserver(GIOChannel *source, GIOCondition condition,
                      XferInfo *xfer)
{

  unsigned short packet_size;
  BYTE *packet;

  struct {
    BYTE psize[2];
    BYTE id; /* 0x00 */
    BYTE zero[4]; /* 0x00000000 */
    BYTE n_files[4]; /* Number of files */
    BYTE n_bytes[4]; /* Total number of bytes */
    BYTE speed[4]; /* Speed of transfer, 0x00 - 0x64 */
    BYTE nick_len[2]; /* Length of nick */
    /* Nickname */
  } xfer0;
  BYTE *buffer;
  int index, psize;


#ifdef TRACE_FUNCTION
  g_print( "ft_filereadserver\n" );
#endif

  /* We closed the socket, so the transfer's already been cancelled  */
  if (condition & G_IO_NVAL)
    return FALSE;
  
  if (read( xfer->sock_fd, (char*)(&packet_size), 2 ) != 2) {
    ft_cancel_transfer(0, xfer);
    return FALSE;
  }

  packet_size = GUINT16_FROM_LE (packet_size);

  packet = (BYTE *)g_malloc( packet_size );
  read( xfer->sock_fd, packet, packet_size );
  packet_print( packet, packet_size,
                PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "FILE(INIT(READ))" );

  switch( packet[ 0 ] ) {
  case 0xFF: /* handshake */

    TCPSendHandShakeAck(xfer->sock_fd);

    Word_2_Chars( xfer0.psize, sizeof( xfer0 ) - 2 + strlen( our_info->nick ) + 1 );
    xfer0.id = 0x00;
    DW_2_Chars( xfer0.zero, 0x00000000 );
    DW_2_Chars( xfer0.n_files, g_slist_length(xfer->file_queue) );
    DW_2_Chars( xfer0.n_bytes, xfer->total_bytes );
    DW_2_Chars( xfer0.speed, 0x00000064 );
    Word_2_Chars( xfer0.nick_len, strlen( our_info->nick ) + 1 );

    index = 0;
    psize = sizeof( xfer0 ) + strlen( our_info->nick ) + 1;
    buffer = (BYTE*)g_malloc( psize );
    
    memcpy( buffer, &xfer0, sizeof( xfer0 ) );
    index += sizeof( xfer0 );
    memcpy( buffer + index, our_info->nick, strlen( our_info->nick ) + 1 );
    
    packet_print( buffer, psize,
		  PACKET_TYPE_TCP | PACKET_DIRECTION_SEND, "FILE(INIT)" );
    write( xfer->sock_fd, buffer, psize );
    g_free( buffer );

    
    break;

  case 0x01: /* We need to send XFER2 now */
    if (packet_size != 4)
      ft_sendfileinfo( xfer );
    break;

  case 0x03: /* Start sending the file :) */
    /* The best way to do this is to set up an IO watch to send it so we don't
       freeze the program, especially on large files */
    ((struct xfer_file*)xfer->file_queue->data)->completed_bytes = Chars_2_DW (&packet[1]);

    xfer->file_fd = open( ((struct xfer_file*)xfer->file_queue->data)->full_filename, O_RDONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH );
    if( xfer->file_fd == -1 )
      {
	g_free(packet);
	return ft_next_file(xfer);
      }

    lseek( xfer->file_fd, ((struct xfer_file*)xfer->file_queue->data)->completed_bytes, SEEK_SET );

    xfer->dialog = create_file_xfer_dialog(xfer);
    if (xfer->dialog == NULL) {
      ft_cancel_transfer(0, xfer);
      return FALSE;
    }

    g_io_add_watch_full( xfer->gioc, G_PRIORITY_DEFAULT_IDLE,
			 G_IO_OUT | G_IO_ERR | G_IO_HUP | G_IO_NVAL,
			 (GIOFunc) ft_sendpartfile,
			 xfer, NULL );

    ((struct xfer_file *)xfer->file_queue->data)->start_time = time( NULL );

    if( xfer->start_time == 0 )
      xfer->start_time = time( NULL );

    xfer->status = XFER_STATUS_TRANSFERING;

    break;
  case 0x04:
    ft_skip_file(0, xfer);
    break;
  case 0x05:  /* looks like an ACK */
    break;
  default:
    g_warning("Unknown packet for file transfer: 0x%02x\n", packet[ 0 ]);
  }

  g_free( packet );

  return TRUE;
}

int ft_filereadclient( GIOChannel *source, GIOCondition condition,
                       XferInfo *xfer )
{
  int file_count, bytes_read, ret = TRUE;

  struct {
    BYTE psize[2]; /* Size of packet */
    BYTE id; /* 0x01 */
    BYTE speed[4]; /* 0x00 to 0x64 - speed of transfer */
    BYTE nick_len[2]; /* length of nick + NULL */
    /* Nickname */
  } xfer1;
  BYTE *buf;
  GSList *contact;

#ifdef TRACE_FUNCTION
  g_print( "ft_filereadclient\n" );
#endif

        /* We closed the socket, so the transfer's already been cancelled */
        if (condition & G_IO_NVAL)
                return FALSE;

  if (!xfer->packet_size) {
    /* We have to turn off blocking for just a bit, or else
    file transfers can get a bit flakey */

    /* fcntl( xfer->sock_fd, F_SETFL, 0 ); */

    if (read( xfer->sock_fd, (char*)(&xfer->packet_size), 2 ) != 2) {
      ft_cancel_transfer(0, xfer);
      return FALSE;
    }

    /* Alright, don't block anymore */
    /* fcntl( xfer->sock_fd, F_SETFL, O_NONBLOCK ); */ 

    xfer->packet_size = GUINT16_FROM_LE (xfer->packet_size);

    xfer->packet_offset = 0;
    xfer->packet = (BYTE *)g_malloc( xfer->packet_size );
  }


  bytes_read = read( xfer->sock_fd, xfer->packet + xfer->packet_offset, xfer->packet_size - xfer->packet_offset );

  if (bytes_read < 1) {
    ft_cancel_transfer(0, xfer);
    return FALSE;
  }

  xfer->packet_offset += bytes_read;

  if (xfer->packet_size != xfer->packet_offset)
    return TRUE;

  packet_print( xfer->packet, xfer->packet_size,
                PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE, "FILE PACKET" );

  switch( (guchar) xfer->packet[ 0 ] )
  {
    case 0x00:
      file_count = Chars_2_DW( &xfer->packet[ 5 ] );
      xfer->total_bytes = Chars_2_DW( &xfer->packet[ 9 ] );

      for(;file_count;file_count--)
        xfer->file_queue = g_slist_append(xfer->file_queue, g_malloc0(sizeof(struct xfer_file)));

      xfer1.id = 0x01;
      DW_2_Chars( xfer1.speed, 0x00000064 );
      Word_2_Chars( xfer1.nick_len, strlen( our_info->nick ) + 1 );
      buf = (BYTE*)g_malloc( sizeof( xfer1 ) + strlen( our_info->nick ) + 1 );
      Word_2_Chars( xfer1.psize, sizeof( xfer1 ) - 1 + strlen( our_info->nick ) );
      memcpy( buf, &xfer1, sizeof( xfer1 ) );
      memcpy( &buf[ sizeof( xfer1 ) ], our_info->nick, strlen( our_info->nick ) + 1 );
      write( xfer->sock_fd, buf, sizeof( xfer1 ) + strlen( our_info->nick ) + 1 );
      packet_print( &buf[2], sizeof( xfer1 ) + strlen( our_info->nick ) - 1,
                    PACKET_TYPE_TCP | PACKET_DIRECTION_SEND, "FILE(01)" );
      
      break;

    case 0x02:
      if (xfer->status == XFER_STATUS_TRANSFERING)
        if (!ft_next_file(xfer))
          return FALSE;

      ((struct xfer_file *)xfer->file_queue->data)->short_filename = g_strdup(&xfer->packet[ 4 ]);
      ((struct xfer_file *)xfer->file_queue->data)->total_bytes = Chars_2_DW(&xfer->packet[ 8 + strlen( &xfer->packet[4]) ]);

      file_place( xfer );     

      xfer->status = XFER_STATUS_TRANSFERING;

      break;

    case 0x06:
      if( xfer->file_fd <= 0 )
        break;

      write( xfer->file_fd, &xfer->packet[ 1 ], xfer->packet_size - 1);

      ((struct xfer_file *)xfer->file_queue->data)->completed_bytes += xfer->packet_size - 1;
      xfer->completed_bytes += xfer->packet_size - 1;

    break;
  case 0xFF:

    contact = Contacts;
    while( contact != NULL ) {
      if( contact->data == xfer->remote_contact )
        break;
      contact = contact->next;
    }

    if (contact == NULL)
      break;
    
    TCPSendHandShakeAck(xfer->sock_fd);
    TCPSendHandShake(kontakt, Chars_2_DW(xfer->packet+32), xfer->sock_fd);

    break;
  case 0x01:
    if (xfer->packet_size == 4)
      break;
  default:
    g_warning("Unknown transfer packet: 0x%02x\n", (guchar) xfer->packet[ 0 ]);
  }

  g_free(xfer->packet);
  xfer->packet = 0;
  xfer->packet_size = 0;

  return ret;
}

void ft_sendfileinfo( XferInfo *xfer )
{
  BYTE *buffer;
  unsigned short packet_size;
  int index;

  struct {
    BYTE id; /* 0x02 */
    BYTE zero; /* 0x00 */
    BYTE fn_len[2]; /* Length of filename + NULL */
  } xfer2a;

  struct {
    BYTE one[2]; /* 0x0001 */
    BYTE zero; /* 0x00 */
    BYTE file_size[4];
    BYTE longzero[4]; /* 0x00000000 */
    BYTE speed[4]; /* 0x00 to 0x64 - speed of transfer */
  } xfer2b;

#ifdef TRACE_FUNCTION
  g_print("ft_sendfileinfo\n");
#endif

  xfer2a.id = 0x02;
  xfer2a.zero = 0x00;
  Word_2_Chars( xfer2a.fn_len, strlen( ((struct xfer_file*)xfer->file_queue->data)->short_filename ) + 1 );
  Word_2_Chars( xfer2b.one, 0x0001 );
  xfer2b.zero = 0;
  DW_2_Chars( xfer2b.file_size, ((struct xfer_file*)xfer->file_queue->data)->total_bytes );
  DW_2_Chars( xfer2b.longzero, 0x00000000 );
  DW_2_Chars( xfer2b.speed, 0x00000064 );
    
  packet_size = sizeof( xfer2a ) + sizeof( xfer2b ) + strlen( ((struct xfer_file*)xfer->file_queue->data)->short_filename ) + 1;
  index = 0;
  buffer = g_malloc( packet_size + 2 );
  Word_2_Chars (buffer, packet_size);
  index += 2;
  memcpy( buffer + index, &xfer2a, sizeof( xfer2a ) );
  index += sizeof( xfer2a );
  memcpy( buffer + index, ((struct xfer_file*)xfer->file_queue->data)->short_filename, strlen( ((struct xfer_file*)xfer->file_queue->data)->short_filename ) + 1 );
  index += strlen( ((struct xfer_file*)xfer->file_queue->data)->short_filename ) + 1;
  memcpy( buffer + index, &xfer2b, sizeof( xfer2b ) );
  index += sizeof( xfer2b );

  packet_print( buffer+2, packet_size,
                PACKET_TYPE_TCP | PACKET_DIRECTION_SEND, "FILE(INIT(2))" );
  write( xfer->sock_fd, buffer, packet_size + 2 );
  g_free( buffer );
}

int ft_sendpartfile( GIOChannel *source, GIOCondition condition, XferInfo *xfer )
{
  int bytes;
  struct {
    BYTE psize[2]; /* size of packet */
    BYTE id; /* 0x06 */
    BYTE data[2048];
  } xfer6;

#ifdef TRACE_FUNCTION
  g_print("ft_sendpartfile\n");
#endif
  
  /* They hung up on us... <sniff> */
  if ( condition & (G_IO_HUP | G_IO_NVAL | G_IO_ERR) )
    return FALSE;
  
  /* The file we're sending was skipped by someone, commit suicide */
  if (xfer->status != XFER_STATUS_TRANSFERING)
    return FALSE;

  xfer6.id = 0x06;
  
  bytes = read( xfer->file_fd, xfer6.data, 2048 );

  Word_2_Chars( xfer6.psize, 1 + bytes );
    
  write( xfer->sock_fd, &xfer6, 3 + bytes );
  packet_print( &xfer6.id, bytes + 1, PACKET_TYPE_TCP | PACKET_DIRECTION_SEND, "FILE PACKET" );
      
  ((struct xfer_file *)xfer->file_queue->data)->completed_bytes += bytes;
  xfer->completed_bytes += bytes;

  if ((((struct xfer_file *)xfer->file_queue->data)->completed_bytes >= ((struct xfer_file *)xfer->file_queue->data)->total_bytes) || (bytes < 2048) ) {
    ft_next_file(xfer);
    return FALSE;
  }

  return TRUE;
}


void ft_cancel_transfer( GtkWidget *widget, XferInfo *xfer )
{
  gchar *msg;
  time_t timedate;

  GSList *file_queue;

#ifdef TRACE_FUNCTION
  g_print( "ft_cancel_transfer\n" );
#endif

  if( xfer->gioc )
    g_io_channel_shutdown(xfer->gioc, FALSE, NULL);

  destroy_file_xfer_dialog(xfer->dialog);

  if (xfer->total_bytes == xfer->completed_bytes) {
    if( xfer->direction == XFER_DIRECTION_RECEIVING ) {
      /* <kludge> */
      /*  Grrr, or else the last file won't be counted */
      struct xfer_file *completed_file = (struct xfer_file *)xfer->file_queue->data;

      xfer->file_queue = g_slist_remove( xfer->file_queue, completed_file );
      xfer->completed_files = g_slist_append( xfer->completed_files, completed_file );
      /* </kludge> */

      msg = g_strdup_printf( _("File transfer:\nCompleted transfer of %s from %s."), xfer->filename, xfer->remote_contact->nick );
      time( &timedate );
      history_add_incoming( xfer->remote_contact->uin, msg, &timedate );
      request_file_action(xfer->completed_files);
      g_free( msg );
    }
    else {
      msg = g_strdup_printf( _("File transfer:\nCompleted transfer of %s to %s."), xfer->filename, xfer->remote_contact->nick );
      history_add_outgoing( xfer->remote_contact->uin, msg );
      g_free( msg );

      msg = g_strdup_printf( _("File transfer completed: %s"), xfer->filename );
      gnome_ok_dialog( msg );
      g_free( msg );
    }
  }
  else{
    if (xfer->status == XFER_STATUS_PENDING) {
      xfer->remote_contact->file_xfers = g_slist_remove(xfer->remote_contact->file_xfers, xfer);
      msg = g_strdup_printf( _("File transfer refused: %s"), xfer->filename );
      gnome_ok_dialog( msg );
      g_free( msg );
    }
    else {
      msg = g_strdup_printf( _("File transfer terminated: %s"), xfer->filename );
      gnome_error_dialog( msg );
      g_free( msg );
    }
  }

  close( xfer->sock_fd );

  g_free( xfer->filename );
  g_free( xfer->auto_save_path );

  g_free( xfer->packet );
  xfer->packet = 0;

  if( xfer->file_fd )
    close( xfer->file_fd );

  for(file_queue = xfer->file_queue;file_queue;file_queue = file_queue->next) {
    g_free( ((struct xfer_file*)file_queue->data)->short_filename);
    g_free( ((struct xfer_file*)file_queue->data)->full_filename);
    g_free( (struct xfer_file*)file_queue->data );
  }

  g_slist_free(xfer->file_queue);

  for(file_queue = xfer->completed_files;file_queue;
      file_queue = file_queue->next) {
    g_free( ((struct xfer_file*)file_queue->data)->short_filename);
    g_free( ((struct xfer_file*)file_queue->data)->full_filename);
    g_free( (struct xfer_file*)file_queue->data );
  }

  g_slist_free(xfer->completed_files);

  g_free(xfer);

  return;
}

void ft_skip_file( GtkWidget *widget, XferInfo *xfer)
{

  struct {
    BYTE id;
    BYTE data[4];
  } xfer4;

#ifdef TRACE_FUNCTION
  g_print( "ft_skip_file\n" );
#endif

  if (xfer->direction == XFER_DIRECTION_SENDING)
    ft_next_file( xfer );
  else {
    xfer4.id = 0x04;
    DW_2_Chars(xfer4.data, g_slist_length(xfer->completed_files) + 1);

    write(xfer->sock_fd, &xfer4, sizeof(xfer4));
  }
}

int ft_next_file( XferInfo *xfer )
{

  struct xfer_file *completed_file = (struct xfer_file *)xfer->file_queue->data;

#ifdef TRACE_FUNCTION
  g_print( "ft_next_file\n" );
#endif

  xfer->file_queue = g_slist_remove( xfer->file_queue, completed_file );
  xfer->completed_files = g_slist_append( xfer->completed_files,
                                          completed_file );

  close(xfer->file_fd);

  if (!xfer->file_queue) {
    ft_cancel_transfer(0, xfer);
    return FALSE;
  }
  else
    xfer->status = XFER_STATUS_CONNECTING;

  xfer->completed_bytes += (completed_file->total_bytes - completed_file->completed_bytes);

  if (xfer->direction == XFER_DIRECTION_SENDING)
    ft_sendfileinfo( xfer );

  return TRUE;
}

void ft_readytoreceive (XferInfo *xfer)
{

  struct {
    BYTE psize[2];
    BYTE id; /* 0x03 */
    BYTE resume_loc[4]; /* Index of where to start file reading for
                           resume */
    BYTE longzero[4]; /* 0x00000000 */
    BYTE speed[4]; /* 0x00 to 0x64 - speed of transfer */
  } xfer3;
  
  struct stat file_stat;

#ifdef TRACE_FUNCTION
  g_print( "ft_readytoreceive\n" );
#endif

  if( xfer->file_fd <= 0 )
    return;

  Word_2_Chars( xfer3.psize, 0x0d );
  xfer3.id = 0x03;
  if( stat( ((struct xfer_file*)xfer->file_queue->data)->full_filename, &file_stat ) == -1 )
    ((struct xfer_file *)xfer->file_queue->data)->completed_bytes = 0;
  else
    ((struct xfer_file *)xfer->file_queue->data)->completed_bytes = file_stat.st_size;
        
  xfer->completed_bytes += ((struct xfer_file *)xfer->file_queue->data)->completed_bytes;
        
  DW_2_Chars( xfer3.resume_loc, ((struct xfer_file *)xfer->file_queue->data)->completed_bytes );
        
  DW_2_Chars( xfer3.longzero, 0x00000000 );
  DW_2_Chars( xfer3.speed, 0x00000064 );
  write( xfer->sock_fd, &xfer3, sizeof( xfer3 ) );
}

gboolean ft_listen( XferInfo *xfer)
{

	struct sockaddr_in my_addr;
	gint length = sizeof( struct sockaddr );

#ifdef TRACE_FUNCTION
  g_print( "ft_listen\n" );
#endif

	g_assert(xfer != NULL);

	xfer->remote_contact->file_xfers = g_slist_remove(xfer->remote_contact->file_xfers, xfer);
	xfer->status = XFER_STATUS_CONNECTING;
  
	xfer->sock_fd = socket( PF_INET, SOCK_STREAM, 0 );
  
	if( xfer->sock_fd <= 0 ) {
		perror("socket");
		return FALSE;
	}
  
	/* fcntl( xfer->sock_fd, O_NONBLOCK ); */

	my_addr.sin_family = AF_INET;
	my_addr.sin_port = 0;
	my_addr.sin_addr.s_addr = g_htonl (INADDR_ANY);
	
  /* NEW ONE */
  
	if( bind( xfer->sock_fd, (struct sockaddr *)&my_addr,
            sizeof(struct sockaddr ) ) == -1 ) {
		perror("bind");
		return FALSE;
	}
  
	listen( xfer->sock_fd, 1 );

	xfer->gioc = g_io_channel_unix_new(xfer->sock_fd);
  
  g_io_add_watch_full( xfer->gioc, G_PRIORITY_DEFAULT_IDLE,
                       G_IO_IN | G_IO_ERR | G_IO_HUP | G_IO_NVAL,
                       (GIOFunc) ft_handshake,
                       xfer, NULL );
  
	getsockname( xfer->sock_fd, ( struct sockaddr * ) &my_addr, &length );

	xfer->port = g_ntohs( my_addr.sin_port );

  return TRUE;
}

XferInfo* ft_new(Contact_Member *contact, int direction,
		 char *filename, guint filesize) 
{
  XferInfo *xferinfo;
  xferinfo = g_new0(XferInfo, 1);

  xferinfo->remote_contact = contact;
  
  xferinfo->direction = direction;
  xferinfo->status = XFER_STATUS_PENDING;

  xferinfo->filename = filename;
  xferinfo->total_bytes = filesize;

  contact->file_xfers = g_slist_append(contact->file_xfers, xferinfo);

  return xferinfo;
}

void ft_addfile(XferInfo *xferinfo, gchar *longname, gchar *shortname,
		guint size)
{

  struct xfer_file *fileinfo;
  
  fileinfo = g_malloc0(sizeof(struct xfer_file));
  
  fileinfo->full_filename = longname;
  fileinfo->short_filename = shortname;
  
  fileinfo->total_bytes = size;
  xferinfo->total_bytes += size;
  
  xferinfo->file_queue = g_slist_append(xferinfo->file_queue, fileinfo);
  
}
