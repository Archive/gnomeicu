#ifndef __ICU_DB_H__
#define __ICU_DB_H__

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define F_ALL   0x01
#define F_NEXT  0x02

#if defined(HAVE_LIBGDBM)
#include <gdbm.h>

#define DB_FILE GDBM_FILE
#define DB_INSERT GDBM_INSERT
#define DB_REPLACE GDBM_REPLACE
#define icudb_fetch(a,b)  gdbm_fetch(a,b)
#define icudb_store gdbm_store
#define icudb_exists(a,b)  gdbm_exists(a,b)
#define icudb_free(a) free(a)
#define icudb_firstkey(a) gdbm_firstkey(a)
#define icudb_nextkey(a,b) gdbm_nextkey(a,b)
#define icudb_fetch(a,b) gdbm_fetch(a,b)

#elif defined(HAVE_NDBM)

#include <db1/ndbm.h>

#define DB_FILE DBM*
#define DB_INSERT DBM_INSERT
#define DB_REPLACE DBM_REPLACE
#define icudb_fetch(a,b)  dbm_fetch(a,b)
#define icudb_store dbm_store
#define icudb_free(a)

extern int icudb_exists (DB_FILE file, datum key);
/* extern datum icudb_firstkey (DB_FILE file); */
#define icudb_firstkey(a) dbm_firstkey(a)
extern datum icudb_nextkey (DB_FILE file, datum key);
extern datum icudb_fetch (DB_FILE file, datum key);

#endif

#if (defined(HAVE_LIBGDBM) || defined(HAVE_NDMB))
#define HAVE_ICUDB
#define DB_READ	 0
#define DB_WRITE 1

#define MESG_ISREAD(mesg)  (mesg[0] & 1)
#define MESG_SETREAD(mesg) (mesg[0] |= 1)

#define MESG_ISRECV(mesg)  (mesg[0] & 2)
#define MESG_SETRECV(mesg) (mesg[0] |= 2)

extern DB_FILE icudb_open (const char *db_path, int mode);
extern int icudb_close_all (void);
extern int icudb_close (DB_FILE file);
extern int icudb_close_dbpath (const char *db_path);
#endif

#endif
