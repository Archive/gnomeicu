/* GnomeICU
 * Copyright (C) 1998-2004 Jeremy Wise, Olivier Crete
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * GnomeICU starts here
 */

#include "common.h"

#include "auto_respond.h"
#include "dragdrop.h"
#include "emoticons.h"
#include "flash.h"
#include "gnomecfg.h"
#include "gnomeicu.h"
#include "gnomeicu-spinner.h"
#include "groups.h"
#include "gtkfunc.h"
#include "icons.h"
#include "newsignup.h"
#include "showlist.h"
#include "tcp.h"
#include "userserver.h"
#include "util.h"
#include "v7login.h"
#include "v7newuser.h"
#include "v7send.h"
#include "tray.h"

#include <libbonoboui.h>
#include <bonobo/bonobo-dock.h>
#include <glade/glade-init.h>
#include <libgnome/gnome-url.h>
#include <gdk/gdkinput.h>
#include <gtk/gtk.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-help.h>
#include <libgnomeui/libgnomeui.h>

#include <netinet/in.h>
#include <signal.h>
#include <string.h>
#include <sys/wait.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <pwd.h>
#include <unistd.h>
#include <fcntl.h>

#ifdef WITH_GTKSPELL
# include <gtkspell/gtkspell.h>
#endif

DWORD our_ip = 0x7f000001; /* localhost */
DWORD our_port = 0; /* the port to make tcp connections on */
DWORD our_sok; /* The TCP socket */
GSList *Contacts = NULL;
DWORD Current_Status=STATUS_OFFLINE;
gchar *passwd = NULL;
gchar *server = NULL;
DWORD remote_port;
gboolean Done_Login=FALSE;
gchar *Away_Message = NULL;
gboolean search_in_progress = FALSE;

GtkWidget *status_button = NULL;

gchar *configfilename = NULL;

USER_INFO_PTR our_info;

_toggles *toggles;
_programs *programs;

gboolean is_new_user = FALSE;
gboolean connection_alive = TRUE;

_MainData *MainData;

static char *geometry; /* geometry string */

int preset_status = STATUS_ONLINE;

gboolean enable_online_events = FALSE;

/* server side contacts list, read from pref */
guint16 status_uid;
gboolean sane_cl = FALSE;
gboolean srvlist_exist = FALSE;
guint32 list_time_stamp;
guint16 record_cnt;
GSList *Groups = NULL;


static gint pinger(gpointer data);
static gint tray_not_yet(gpointer data);
static void child_died (int);
static void app_init (void);
static gboolean is_already_running(void);
static void source_drag_data_get (GtkWidget *widget, GdkDragContext *context,
                                  GtkSelectionData *selection_data, guint info,
                                  guint time, gpointer data);
static void gtkspell_change_status(void);


void create_tcp_line( void )
{
	gint cx;
	struct sockaddr_in addr;
	gint temp_sok;
	gint l = 1;
	gboolean have_tcp_port = FALSE;
	static GIOChannel *iochan = NULL;

	if( our_sok )
		close( our_sok );

	for( cx = min_tcp_port; cx <= max_tcp_port; cx ++ )
	{
		addr.sin_addr.s_addr = g_htonl( INADDR_ANY );
		addr.sin_family = AF_INET;
		addr.sin_port = g_htons( cx );

		temp_sok = socket( AF_INET, SOCK_STREAM, 0 );
		setsockopt( temp_sok, SOL_SOCKET, SO_REUSEADDR, &l, 4 );

		our_sok = bind( temp_sok, (struct sockaddr *)&addr, sizeof( addr ) );
		if( our_sok != -1 )
			have_tcp_port = TRUE;
		else
			continue;

		our_sok = temp_sok;
		our_port = cx;

		if( -1 == listen( our_sok, 10 ) )
		  g_error( _("Cannot listen to socket, port %u\n"), our_port );

		l = sizeof( addr );

		if (iochan)
		  g_io_channel_shutdown(iochan, FALSE, NULL);

		iochan = g_io_channel_unix_new(our_sok);
		g_io_add_watch(iochan, G_IO_IN , (GIOFunc) TCPAcceptIncoming, NULL );

		g_free( our_info->ip );

		/* FIXME: this should be freed sometime */
		our_info->ip = g_strdup_printf( "%u.%u.%u.%u",
		         (BYTE) (our_ip >> 24),
		         (BYTE) (our_ip >> 16),
		         (BYTE) (our_ip >> 8),
		         (BYTE) (our_ip) );

		g_free( our_info->port );

		our_info->port = g_strdup_printf( "%u", our_port );
		break;
	}

	if( have_tcp_port == FALSE )
		g_error( g_strdup_printf( "No TCP port available between %d and %d.\n",
		                          min_tcp_port, max_tcp_port ) );
}

void
gnomeicu_set_status_button (gboolean flash)
{
  static GtkWidget *status_im = NULL;
  GtkWidget *pb;
  GtkWidget *label;

  if (status_im != NULL)
    gtk_container_remove (GTK_CONTAINER (status_button), status_im);

  status_im = gtk_hbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (status_button), status_im);

  /* set pixbuf */
  pb = gtk_image_new_from_pixbuf (get_pixbuf_for_status (Current_Status));
  gtk_box_pack_start (GTK_BOX (status_im), pb, FALSE, FALSE, 0);

  /* set label */
  label = gtk_label_new (get_status_str (Current_Status));
  gtk_box_pack_start (GTK_BOX (status_im), label, TRUE, TRUE, 5);

  gtk_widget_show_all (status_im);

  tray_update ();
}

void
gnomeicu_done_login (void)
{
  static int flash_timeout_id = 0;
  static gint pinger_id = 0;
	
  Done_Login = TRUE;
  connection_alive = TRUE;

  /* setup timers */

  if(!pinger_id)
    pinger_id = g_timeout_add(60000, (GtkFunction)pinger, NULL);

  if (!flash_timeout_id)
    flash_timeout_id = g_timeout_add (500, (GtkFunction) flash_messages, NULL );

  auto_respond_init ();
}

/*** Local functions ***/

/* this will "ping" the server by sending ourselves a message,
	 and alert the user if the connection has been lost */

static gint pinger(gpointer data)
{

	if(Current_Status == STATUS_OFFLINE)
		return TRUE;

	if(!connection_alive) {
		icq_set_status_offline(NULL, NULL);
		return TRUE;
	}
	
	connection_alive = FALSE;
	v7_sendmsg_plain(mainconnection, our_info->uin, "GnomeICU server ping!");
	
	return TRUE;
}

/* This will check the path permission, to make sure it is ONLY owner accessible
 * if it is not, it will attempt to fix it using chmod
 */

static void
gnomeicu_check_path_permission (const gchar *filename)
{
  struct stat file_stat;

  if (stat (filename, &file_stat) != 0) {
    switch (errno) {
    case ENOENT:
      g_warning (PACKAGE ":stat:%s:%s:cannot check the proper permission", filename, g_strerror (errno));
      return;
    default:
      g_warning (PACKAGE ":stat:%s:%s", filename, g_strerror (errno));
      return;
    }
  }

  /* only allow owner access */
  if ( (file_stat.st_mode & ~S_IRWXU) != 0) {
    if (S_ISREG (file_stat.st_mode))
      chmod (filename, S_IRUSR | S_IWUSR); /* regular file */
    else if (S_ISDIR (file_stat.st_mode))
      chmod (filename, S_IRUSR | S_IWUSR | S_IXUSR); /* directory */
  }
}

/* check if we have the directories created and make sure they have the
 * proper permission (0700)
 */
static void
gnomeicu_check_data_dir ()
{
  gchar *filename;

  /* Create directories if they don't exist */

  filename = g_strdup_printf("%s/.icq", g_get_home_dir());
  if(mkdir(filename, 0700))
    switch(errno) {
    case EEXIST:
      gnomeicu_check_path_permission (filename);
      break;
    default:
      g_warning(PACKAGE "mkdir:%s:%s", filename,
                g_strerror(errno));
    }
  g_free(filename);

  filename = g_strdup_printf("%s/.icq/history", g_get_home_dir());
  if(mkdir(filename, 0700))
    switch(errno) {
    case EEXIST:
      gnomeicu_check_path_permission (filename);
      break;
    default:
      g_warning(PACKAGE "mkdir:%s:%s", filename,
                g_strerror(errno));
    }
  g_free(filename);

  filename = g_strdup_printf("%s/.icq/icons", g_get_home_dir());
  if(mkdir(filename, 0700))
    switch(errno) {
    case EEXIST:
      gnomeicu_check_path_permission (filename);
      break;
    default:
      g_warning(PACKAGE "mkdir:%s:%s", filename,
                g_strerror(errno));
    }
  g_free(filename);

  filename = g_strdup_printf("%s/.icq/emoticons", g_get_home_dir());
  if(mkdir(filename, 0700))
    switch(errno) {
    case EEXIST:
      gnomeicu_check_path_permission (filename);
      break;
    default:
      g_warning(PACKAGE "mkdir:%s:%s", filename,
                g_strerror(errno));
    }
  g_free(filename);
}

/* Terminates defunct child processes */
void child_died (int sig_num)
{
  waitpid (-1, NULL, WNOHANG | WUNTRACED);
}

static gint
app_save_state_cb (GnomeClient *client, gint phase, GnomeRestartStyle save_style,
                   gint shutdown, GnomeInteractStyle interact_style, gint fast,
                   gpointer client_data)
{
  gchar *argv[] = { client_data, "-s", NULL, "-g", NULL, NULL, NULL };
  gint x, y, window_w, window_h;

  /* Set commands to clone and restart this application.  Note that we
     use the same values for both -- the session management code will
     automatically add whatever magic option is required to set the
     session id on startup.  */
	
  switch (Current_Status & 0xffff) {
  case STATUS_ONLINE:
    argv[2] = "online";
    break;
  case STATUS_INVISIBLE:
    argv[2] = "invisible";
    break;
  case STATUS_NA:
    argv[2] = "na";
    break;
  case STATUS_FREE_CHAT:
    argv[2] = "freechat";
    break;
  case STATUS_OCCUPIED:
    argv[2] = "occupied";
    break;
  case STATUS_AWAY:
    argv[2] = "away";
    break;
  case STATUS_DND:
    argv[2] = "dnd";
    break;
  case STATUS_OFFLINE:
  default:
    argv[2] = "offline";
  }

  if (MainData->window == NULL) {
    /* can't do anything to save state */
    return TRUE;
  }

  gdk_drawable_get_size(GTK_WIDGET (MainData->window)->window,
			&window_w, &window_h);
  preferences_set_int (PREFS_GNOMEICU_WINDOW_WIDTH, window_w);
  preferences_set_int (PREFS_GNOMEICU_WINDOW_HEIGHT, window_h);

  if (MainData->hidden) {
    x = MainData->x;
    y = MainData->y;
    argv[5] = "--hide";
  } else
    gtk_window_get_position (GTK_WINDOW (MainData->window), &x, &y);

  preferences_set_int (PREFS_GNOMEICU_WINDOW_X, x);
  preferences_set_int (PREFS_GNOMEICU_WINDOW_Y, y);

  argv[4] = g_strdup_printf ("+%d+%d", x ,y);

  gnome_client_set_restart_command (client, 6, argv);

  g_free (argv[4]);

  return TRUE;
}

void app_init (void)
{
	GtkWidget *hbox;
	GtkWidget *app_win;
	BonoboDock* dock;
	BonoboDockItem* dockitem;
        gint window_w, window_h;

	MainData->xml = gicu_util_open_glade_xml ("main.glade", "app");
	if (MainData->xml == NULL) {
		g_error("Could not open main.glade");
		exit(1);
	}
	
	app_win = glade_xml_get_widget (MainData->xml, "app");

	MainData->window = app_win;

        /* set main window size */

	/* Revert to default if value is clearly wrong */
	/* Otherwise it crashes and even brings down xnest with it */
 beforewh:
        window_w = preferences_get_int (PREFS_GNOMEICU_WINDOW_WIDTH);
        window_h = preferences_get_int (PREFS_GNOMEICU_WINDOW_HEIGHT);
	if (window_w > 5 * gdk_screen_get_width(gdk_screen_get_default()) ||
	    window_h > 5 * gdk_screen_get_height(gdk_screen_get_default())) {
	  preferences_unset(PREFS_GNOMEICU_WINDOW_WIDTH);
	  preferences_unset(PREFS_GNOMEICU_WINDOW_HEIGHT);
	  goto beforewh;
	}

	MainData->x = preferences_get_int (PREFS_GNOMEICU_WINDOW_X);
	MainData->y = preferences_get_int (PREFS_GNOMEICU_WINDOW_Y);
	if (MainData->x > gdk_screen_get_width(gdk_screen_get_default()) ||
	    MainData->y > gdk_screen_get_height(gdk_screen_get_default())) {
	  MainData->x = 0;
	  MainData->y = 0;
	}
	
	if (MainData->x && MainData->y)
	  gtk_window_move (GTK_WINDOW (app_win), MainData->x, MainData->y);
	gtk_window_set_default_size (GTK_WINDOW (app_win), window_w, window_h);

	if (geometry) {
	  if (!gtk_window_parse_geometry (GTK_WINDOW (app_win), geometry))
	    g_printerr (_("Invalid geometry string \"%s\"\n"), geometry);
	}

	dock = BONOBO_DOCK(glade_xml_get_widget (MainData->xml, "bonobodock1"));
	dockitem = BONOBO_DOCK_ITEM(glade_xml_get_widget (MainData->xml, "bonobodockitem1"));
	/* GNOME2 FIXME : set the properties that libglade doesn't seem to
	 * handle correctly */
	bonobo_dock_allow_floating_items(dock, FALSE);
	dockitem->behavior = bonobo_dock_item_get_behavior(dockitem) 
				| BONOBO_DOCK_ITEM_BEH_EXCLUSIVE;
	gtk_widget_show(GTK_WIDGET(dock));
	gtk_widget_show(glade_xml_get_widget (MainData->xml, "main_vbox"));
	gnome_app_set_contents(GNOME_APP (app_win), glade_xml_get_widget (MainData->xml,"main_vbox"));

        /* setup tree view attributes */
        gnomeicu_tree_create ();

	MainData->notebook = glade_xml_get_widget (MainData->xml, "notebook");

	g_signal_connect (G_OBJECT (app_win), "delete_event",
			  G_CALLBACK (icq_quit), NULL);
	icon_themes_init();
	emoticon_themes_init();
	tray_init();

	hbox = glade_xml_get_widget (MainData->xml, "status_hbox");

	status_button = gtk_button_new ();

	gtk_button_set_relief (GTK_BUTTON (status_button), GTK_RELIEF_NONE);
	gtk_widget_show (status_button);
	gtk_box_pack_start (GTK_BOX (hbox), status_button, FALSE, TRUE, 0);

	g_signal_connect (G_OBJECT (status_button), "activate",
	                  G_CALLBACK (popup_status_menu), NULL);
	g_signal_connect (G_OBJECT (status_button), "clicked",
	                  G_CALLBACK (popup_status_menu), NULL);


#ifdef WITH_GTKSPELL
	preferences_watch_key(PREFS_GNOMEICU_SPELLCHECK, 
			      gtkspell_change_status);
#endif

	glade_xml_signal_autoconnect (MainData->xml);

	gnomeicu_spinner_init ();

	gnomeicu_set_status_button (FALSE);
}

int main( int argc, char *argv[] )
{
	int a,b,c,d;  /* vars for ip parsing */
	gchar *file;

	GnomeClient *client;
	static char *cmdline_status, *cmdline_ip;
	static gint hidden;

#ifdef GNOME_PARAM_GOPTION_CONTEXT

	GOptionContext *goptcontext;
	
	const GOptionEntry options[] = {
	  { "ipaddr",
	    'i',
	    0,
	    G_OPTION_ARG_STRING,
	    &cmdline_ip,
	    N_("Set IP address to send to ICQ"),
	    N_("<IP>")
	  },
	  { "status",
	    's',
	    0,
	    G_OPTION_ARG_STRING,
	    &cmdline_status,
	    N_("Initial status"),
	    N_("<STATUS>")
	  },
	  { "geometry",
	    'g',
	    0,
	    G_OPTION_ARG_STRING,
	    &geometry,
	    N_("X geometry specification (see \"X\" man page)"),
	    N_("<WIDTH>x<HEIGHT>+<X>+<Y>")
	  },
	  { "hide",
	    'h',
	    0,
	    G_OPTION_ARG_NONE,
	    &hidden,
	    N_("Hide the GnomeICU main window upon startup"),
	    NULL
	  },
	  { NULL, 0, 0, 0, NULL, NULL, NULL }
	};

#else /* GNOME_PARAM_GOPTION_CONTEXT */

	static struct poptOption arguments[] = {
		{ "ipaddr",
		  'i',
		  POPT_ARG_STRING,
		  &cmdline_ip,
		  0,
		  N_("Set IP address to send to ICQ"),
		  N_("IP")
		},
		{ "status",
		  's',
		  POPT_ARG_STRING,
		  &cmdline_status,
		  0,
		  N_("Initial status"),
		  N_("STATUS")
		},
		{ "geometry",
		  'g',
		  POPT_ARG_STRING,
		  &geometry,
		  0,
		  N_("X geometry specification (see \"X\" man page)"),
		  N_("WIDTHxHEIGHT+X+Y")
		},
		{ "hide",
		  'h',
		  POPT_ARG_NONE,
		  &hidden,
		  0,
		  N_("Hide the GnomeICU main window upon startup"),
		  NULL
		},
		{ NULL, 0, 0, NULL, 0, NULL, NULL }
        };

#endif /* GNOME_PARAM_GOPTION_CONTEXT */

	bindtextdomain(GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);

#ifdef HAVE_SOCKS5
	SOCKSinit(argv[0]);
#endif


	srand( time( NULL ) );

	our_info = g_new0( USER_INFO_STRUCT, 1 );
	our_info->country = 1; /* USA */

	toggles = g_new0( _toggles, 1 );
	programs = g_new0( _programs, 1 );
	MainData = g_new0 (_MainData, 1);


	configfilename = g_strdup( "/GnomeICU/" );

#ifdef GNOME_PARAM_GOPTION_CONTEXT
	goptcontext = g_option_context_new (_("GnomeICU ICQ Instant Messenger"));
	
	g_option_context_add_main_entries (goptcontext, options, GETTEXT_PACKAGE);
#endif
	
	gnome_program_init (PACKAGE, VERSION,
			    LIBGNOMEUI_MODULE,
			    argc, argv,
#ifdef GNOME_PARAM_GOPTION_CONTEXT
			    GNOME_PARAM_GOPTION_CONTEXT, goptcontext,
#else
			    GNOME_PARAM_POPT_TABLE, arguments,
#endif
			    GNOME_PARAM_GNOME_DATADIR, GNOMEICU_DATADIR,
			    GNOME_PARAM_HUMAN_READABLE_NAME, _("GnomeICU"),
			    GNOME_PARAM_APP_DATADIR, GNOMEICU_DATADIR,
			    NULL);



	if (is_already_running()) {
	  g_warning("GnomeICU is already running: showing window\n");
	  exit(1);
	}

	if (cmdline_ip) {
	  sscanf(cmdline_ip,"%d.%d.%d.%d",&a,&b,&c,&d);
	  our_ip = ((DWORD)(a)<<24) |
	    ((DWORD)(b)<<16) |
	    ((DWORD)(c)<<8) |
	    (DWORD)(d);
	}
	
        gnomeicu_check_data_dir ();

	app_init ();
	Read_RC_File();

	if ( !our_info->uin )
          new_sign_up();

	if (cmdline_status) {
	  if (!strcasecmp(cmdline_status,"offline"))
	    preset_status = STATUS_OFFLINE;
	  else if (!strcasecmp(cmdline_status,"online"))
	    preset_status = STATUS_ONLINE;
	  else if (!strcasecmp(cmdline_status,"na"))
	    preset_status = STATUS_NA;
	  else if (!strcasecmp(cmdline_status,"invisible"))
	    preset_status = STATUS_INVISIBLE;
	  else if (!strcasecmp(cmdline_status,"freechat"))
	    preset_status = STATUS_FREE_CHAT;
	  else if (!strcasecmp(cmdline_status,"occupied"))
	    preset_status = STATUS_OCCUPIED;
	  else if (!strcasecmp(cmdline_status,"away"))
	    preset_status = STATUS_AWAY;
	  else if (!strcasecmp(cmdline_status,"dnd"))
	    preset_status = STATUS_DND;
	}


	gnome_config_push_prefix( configfilename );


	/* These SIGPIPEs are irritating, ignore them */
	signal(SIGPIPE, SIG_IGN);
	/* Terminate defunct children */
	signal(SIGCHLD, child_died);

	client = gnome_master_client();

	g_signal_connect (G_OBJECT (client), "die",
	                  G_CALLBACK (icq_quit),
	                  NULL );

	g_signal_connect (G_OBJECT (client), "save_yourself",
	                  G_CALLBACK (app_save_state_cb),
	                  argv[0]);

	file = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_PIXMAP,
					  "gnomeicu.png", TRUE, NULL);
	if (file != NULL) {
	  gtk_window_set_default_icon_from_file( file, NULL );
	  g_free (file);
	}


        /* populate loaded data to the tree model */
	gnomeicu_tree_populate_data ();

	/* see if we want to start up hidden */
	if ( hidden ) {
	  MainData->hidden = TRUE;
	  if (!tray_exists()) 
	    g_timeout_add (15*1000, (GtkFunction) tray_not_yet, NULL );
	  
	} else {
	  gtk_widget_show(MainData->window);
	  MainData->hidden = FALSE;
	}

 	while( gtk_events_pending() )
 		gtk_main_iteration();

	create_tcp_line();

        if (is_new_user)
          v7_new_user(passwd);
        else {
          /* auto login */
          if (preferences_get_bool (PREFS_GNOMEICU_STARTUP_CONNECT) &&
	      preset_status != STATUS_OFFLINE) {
	    if(!strcmp(passwd, "")) /* check for empty password */
	      show_uin_pwd_dialog();

            if(strcmp(passwd, "")) /* check again */
              v7_new_login_session();
          }
          /* bootup unix socket for applet communication */
	  userserver_init();
	}

	gtk_main();

	/* shutdown unix socket */
	userserver_kill();


        /* FIXME This seems to crash on exit on my machine 
	 * stack/memory corruption of some sort, disabling for now
         */
	/* v7_quit(); */
	return 0;
}

void
source_drag_data_get  (GtkWidget	*widget, GdkDragContext *context,
                       GtkSelectionData *selection_data, guint info,
                       guint time, gpointer data)
{
	gchar *str;
	GSList *contact = NULL;

	if( contact == NULL )
		return;

	str = g_strdup_printf( "%s\n%s", kontakt->uin,
	                       kontakt->nick );
	gtk_selection_data_set( selection_data,
	                        selection_data->target,
	                        8, str, strlen( str ) );
	g_free( str );
}

/*
 * Display the About dialog from Help -> About
 */
void
gnomeicu_about (GtkWidget *widget, gpointer data)
{
  static GtkWidget *about = NULL;
  static GdkPixbuf *pixbuf = NULL;
  gchar *file;

  /* Authors Credits */
  const gchar *authors[] = {

    "Olivier Cr\xc3\xaate <tester@tester.ca>",
    "Gediminas Paulauskas <menesis@delfi.lt>",
    "Patrick Sung <iam@patricksung.com>",
    "David Tabachnikov <captain@bezeqint.net>",
    "Vincent Untz <vincent@vuntz.net>",
    "Jeremy Wise <jwise@pathwaynet.com>",
    NULL
  };

  /* Documenters Credits */
  const gchar *documenters[] = {
   "Thomas Canty <tommydal@ihug.com.au>",
   "Ilya Konstantinov <gnomeicu-docs@future.galanet.net>",
   "Christopher Ness <nesscg@mcmaster.ca>",
    NULL
  };

  /* Translator credits */
  gchar *translator_credits = _("translator_credits");


  if (pixbuf == NULL) {
    file = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_PIXMAP,
				      "gnomeicu.png", TRUE, NULL);
    if (file != NULL) {
      pixbuf = gdk_pixbuf_new_from_file (file, NULL);
      g_free (file);
    }
  }

  gtk_show_about_dialog(GTK_WINDOW(MainData->window),
			"authors", authors,
			"comments", _("GnomeICU is a small, fast and functional "
				      "clone of Mirabilis' ICQ program, specifically "
				      "designed for GNOME."),
			"copyright",  "Copyright \xc2\xa9 1999-2006 Jeremy Wise, Olivier Cr\xc3\xaate, Patrick Sung",
			"documenters", documenters,
			"license", _("This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.") ,
			"wrap-license", TRUE,
			"logo", pixbuf, 
			"version", VERSION,
			"website", "http://gnomeicu.sourceforge.net/",
			NULL);
  //			"logo-icon-name", "gnomeicu.png",

}

void go_url_home (void)
{
	gnome_url_show ("http://gnomeicu.sourceforge.net/index.php", NULL);
}

void go_url_updates (void)
{
	gnome_url_show ("http://gnomeicu.sourceforge.net/updates.php", NULL);
}

void icq_quit( GtkWidget *widget, gpointer data )
{
	GSList *contact;
        gint window_w, window_h, x, y;

#ifdef TRACE_FUNCTION
	g_print( "icq_quit\n" );
#endif

	contact = Contacts;


	while( contact != NULL )
	{
		if( kontakt->sok > 0 )
			close( kontakt->sok );
		contact = contact->next;
	}

	if (MainData->window != NULL) {
		gdk_drawable_get_size (GTK_WIDGET (MainData->window)->window,
                                       &window_w, &window_h);
                preferences_set_int (PREFS_GNOMEICU_WINDOW_WIDTH, window_w);
                preferences_set_int (PREFS_GNOMEICU_WINDOW_HEIGHT, window_h);

		if (MainData->hidden) {
		  x = MainData->x;
		  y = MainData->y;
		} else
		  gtk_window_get_position (GTK_WINDOW (MainData->window),
					   &x, &y);
		preferences_set_int (PREFS_GNOMEICU_WINDOW_X, x);
		preferences_set_int (PREFS_GNOMEICU_WINDOW_Y, y);
	}
	g_object_unref (G_OBJECT (MainData->xml));

	Save_RC();

	g_free(configfilename);

#ifdef HAVE_ICUDB
	icudb_close_all();
#endif

	gtk_main_quit();
}

void show_help (void)
{
	GError *error = NULL;

	gnome_help_display ("gnomeicu.xml", NULL, &error);

	if (error != NULL)
	{
		g_warning (error->message);

		g_error_free (error);
	}
}

gboolean is_already_running(void)
{

  gint socket_fd;
  struct sockaddr_un	addr;
  struct passwd		*pw;

  pw = getpwuid (getuid ());

  if (!pw) {
    fprintf (stderr, "Unable to get current login name, naming service down?\n"); 
    return FALSE;
  }
  
  sprintf (addr.sun_path, "/tmp/.gnomeicu-%s/ctl", pw->pw_name);

  addr.sun_family = AF_UNIX;

  if(access(addr.sun_path, F_OK) != 0)
    return FALSE;
  
  socket_fd = socket(AF_UNIX, SOCK_STREAM, 0);

  if(socket_fd >= 0) {
    if ( connect(socket_fd, (struct sockaddr *)&addr,
		 sizeof(addr.sun_family) + strlen(addr.sun_path) + 1) == 0) {

      gulong u = htonl(5);
      static const char c = 1;

      fcntl(socket_fd, F_SETFL, O_NONBLOCK);

      write (socket_fd, &c, 1);
      write (socket_fd, &u, sizeof (gulong));
      write (socket_fd, "show", u);

      close(socket_fd);
      return TRUE;
    }else{
      close(socket_fd);
      return FALSE;
    }
  }

  return FALSE;
}

#ifdef WITH_GTKSPELL

void gtkspell_change_status(void)
{
  GSList *contact = Contacts;
  GtkSpell *gtkspell;
  
  if (preferences_get_bool(PREFS_GNOMEICU_SPELLCHECK))
    while (contact != NULL ) {
      if (kontakt->msg_dlg_xml) {
	gtkspell = gtkspell_new_attach(GTK_TEXT_VIEW(glade_xml_get_widget (kontakt->msg_dlg_xml, "input")),
				       NULL, NULL);
	gtkspell_recheck_all(gtkspell);
      }
	contact = contact->next;
    }
  else
    while (contact != NULL ) {
      if ( kontakt->msg_dlg_xml && 
	   (gtkspell = gtkspell_get_from_text_view(GTK_TEXT_VIEW(glade_xml_get_widget (kontakt->msg_dlg_xml, "input")))))
	gtkspell_detach(gtkspell);
      contact = contact->next;
    }
}

#endif

gint tray_not_yet(gpointer data)
{
  if (!GTK_WIDGET_VISIBLE(MainData->window) && !tray_exists()) {
    gtk_widget_show(MainData->window);
    MainData->hidden = FALSE;
  }
  return FALSE;
}
