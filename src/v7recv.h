/****************************
  Reception for ICQ v6-7 protocol (Oscar)
  Olivier Crete (c) 2001-2002
  GnomeICU
*****************************/

#ifndef __V7RECV_H__
#define __V7RECV_H__

#include "v7base.h"

V7Connection *v7_connect(gchar *address, guint port, gchar *cookie, guint cookielen);

#endif /* __V7RECV_H__ */

