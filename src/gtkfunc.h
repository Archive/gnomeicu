/************************************
 Miscellaneous functions
 (c) 1999 Jeremy Wise
 GnomeICU  
*************************************/

#ifndef __GTKFUNC_H__
#define __GTKFUNC_H__

#include "common.h"

#include <gtk/gtk.h>

void animate_on( void );
void animate_off( void );
void icq_msgbox( STORED_MESSAGE_PTR message_text, UIN_T uin );

gboolean send_file_request( GtkWidget *widget, int response, gpointer data );
void send_url_window( GtkWidget *widget, char *defaulturl );
void remove_contact(GSList *contact, gboolean rem_history);
void icq_set_status( GtkWidget *widget, gpointer data );
void icq_set_status_online( GtkWidget *widget, gpointer data );
void icq_set_status_offline( GtkWidget *widget, gpointer data );
int icq_set_status_away( GtkWidget *widget, gpointer noask );
void icq_set_status_na( GtkWidget *widget, gpointer noask );
void icq_set_status_invisible( GtkWidget *widget, gpointer data );
void icq_set_status_dnd( GtkWidget *widget, gpointer noask );
void icq_set_status_occ( GtkWidget *widget, gpointer noask );
void icq_set_status_ffc( GtkWidget *widget, gpointer data );
void display_url( GtkWidget *widget, const char *url );
void display_url_from_entry( GtkWidget *widget, GtkEntry *entry );
gint hide_dont_kill( GtkWidget *widget );
gint hide_ch_window( GtkWidget *widget, GdkEvent *event, GtkWidget *window );
int popup_status_menu( GtkWidget *widget,  gpointer data );
gint ctrl_enter_cb( GtkWidget *widget, GdkEventKey *ev, int sock );
void request_file_action( GSList *completed_files );
gchar *get_foreground_for_status(gint status);
gboolean is_connected(GtkWindow *parent_window, const gchar *message);

#endif /* __GTKFUNC_H__ */
