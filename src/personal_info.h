#ifndef __PERSONAL_INFO_H__
#define __PERSONAL_INFO_H__

#include "datatype.h"

#include <gtk/gtkwidget.h>

void show_personal_info (GtkWidget *widget, gpointer data);
void dump_personal_info( UIN_T uin );
void update_personal_info ( UIN_T uin );
gboolean userinfo_close_clicked_callback (GtkWidget *widget, gpointer data);
gboolean userinfo_refresh_clicked_callback (GtkWidget *widget, gpointer data);

#endif /* __PERSONAL_INFO_H__ */
