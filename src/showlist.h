/* GnomeICU
 * Copyright (C) 1998-2003 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * Manage GnomeICU contact list with gtktreeview/treemodel/liststore
 */


#ifndef __SHOWLIST_H__
#define __SHOWLIST_H__

#include "common.h"

#include <gtk/gtk.h>

enum {
  ICON_COLUMN,      /* gdkpixbuf for the icon, if any */
  TEXT_COLUMN,      /* String displayed on the treeview */
  GROUP_COLUMN,     /* contains (GroupInfo *) */
  CONTACT_COLUMN,   /* contains (Contact_Member *) */
  SPECIAL_COLUMN,   /* special column contains special flag, workaround for
                       the poor group/contact structure for now */
  ICON2_COLUMN,
  N_COLUMNS
};

void gnomeicu_tree_create (void);
void gnomeicu_tree_populate_data (void);
void gnomeicu_tree_user_remove (Contact_Member *contact);
void gnomeicu_tree_user_add (Contact_Member *contact);
void gnomeicu_tree_user_update(Contact_Member *contact);
void gnomeicu_tree_set_contact_icon (Contact_Member *contact, GdkPixbuf *pixbuf);
void gnomeicu_tree_group_add (const GroupInfo *ginfo);
void gnomeicu_tree_group_rename (const GroupInfo *ginfo);
void gnomeicu_tree_group_remove (const GroupInfo *ginfo);
void gnomeicu_tree_refresh_visuals (void);

gboolean find_iter_for_contact_direct (GtkTreeModel *model, Contact_Member *contact,
				       GtkTreeIter *iter, GtkTreeIter *group);

/* for listwindow.c */
void gnomeicu_tree_setup_cell_layout(GtkCellLayout *layout);

#endif /* __SHOWLIST_H__ */
