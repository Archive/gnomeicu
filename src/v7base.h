/*******************************************
  Basic code for ICQ v7-8 protocol (Oscar)
  Olivier Crete (c) 2001-2007
  GnomeICU
 *******************************************/

#ifndef __V7BASE_H__
#define __V7BASE_H__

#include "v7buffer.h"

typedef struct _V7Connection V7Connection;
typedef struct _V7Packet     V7Packet;
typedef enum   _ConnStatus   ConnStatus;
typedef enum   _ReqType      ReqType;
typedef struct _TLVstack     TLVstack;
typedef struct _TLV          TLV;
typedef struct _Snac         Snac;
typedef struct _Request      Request;
typedef struct _V7RateClass  V7RateClass;
typedef struct _RateLimitedResendData RateLimitedResendData;

enum _ConnStatus {
  READY,
  NOT_CONNECTED,
  CONNECTING,
  BROKEN
};

enum _ReqType {
  FULL_INFO,
  SEARCH,
  OUR_INFO,
  NEW_USER_VERIFICATION
};

#define CHANNEL_NEWCONN      1
#define CHANNEL_SNAC         2
#define CHANNEL_ERROR        3
#define CHANNEL_CLOSE        4
#define CHANNEL_KEEPALIVE    5

#define FAMILY_01_GENERIC       0x01
#define FAMILY_02_LOCATION      0x02
#define FAMILY_03_CONTACTLIST   0x03
#define FAMILY_04_MESSAGING     0x04
#define FAMILY_09_LISTS         0x09
#define FAMILY_0B_UNKNOWN_1     0x0B
#define FAMILY_13_SAVED_LIST    0x13
#define FAMILY_15_SAVED_DATA    0x15
#define FAMILY_17_NEW_USER      0x17

/*              FAMILY_GENERIC       0x01  */
#define F01_BOTH_ERROR                    0x01 
#define F01_CLIENT_READY                  0x02  /* aim too */
#define F01_SERVER_READY                  0x03  /* aim too */
#define F01_CLIENT_REQ_RATE               0x06  /* aim too */
#define F01_SERVER_ACK_RATE               0x07  /* aim too */
#define F01_CLIENT_ACK_ACK_RATE           0x08  /* aim too */
#define F01_SERVER_SENDING_TOO_FAST       0x0A  /* aim too */
#define F01_CLIENT_REQ_PERSONAL_INFO      0x0E  /* aim too */
#define F01_SERVER_PERSONAL_INFO          0x0F  /* aim too */
#define F01_CLIENT_SET_IDLE               0x11
#define F01_SERVER_MOTD                   0x13  /* aim too */
#define F01_CLIENT_I_AM_ICQ               0x17
#define F01_SERVER_ACK_I_AM_ICQ           0x18
#define F01_CLIENT_STATUS_CODE            0x1E

/*              FAMILY_LOCATION      0x02  */

#define F02_BOTH_ERROR                    0x01
#define F02_CLIENT_REQ_RIGHTS_LOCATION    0x02 /* aim too */
#define F02_SERVER_RIGHTS_LOCATION        0x03 /* aim too */
#define F02_CLIENT_SET_CAPS               0x04 /* aim too user info? */
#define F02_CLIENT_GET_CAPS               0x05 /* from aim  */

/*              FAMILY_CONTACTLIST   0x03  */

#define F03_BOTH_ERROR                    0x01
#define F03_CLIENT_REQ_CL_INFO            0x02 /* aim too */
#define F03_SERVER_CL_INFO                0x03 /* aim too */
#define F03_CLIENT_ADD_TO_CL              0x04 /* aim too */
#define F03_CLIENT_REMOVE_FROM_CL         0x05 /* aim too */
#define F03_SERVER_REJECTED_NOT_REQ       0x0A /* aim too */
#define F03_SERVER_INCOMING_USER          0x0B /* aim too */
#define F03_SERVER_OUTGOING_USER          0x0C /* aim too */

/*              FAMILY_MESSAGING     0x04  */
#define F04_BOTH_ERROR                    0x01
#define F04_CLIENT_ADD_MSG_PARM           0x02 /* aim too */
#define F04_CLIENT_REQ_RIGHTS_MSG         0x04 /* aim too */
#define F04_SERVER_RIGHTS_MSG             0x05 /* aim too */
#define F04_CLIENT_SEND_MESSAGE           0x06 /* aim too */
#define F04_SERVER_RECV_MESSAGE           0x07 /* aim too */
#define F04_BOTH_ACK_ADV_MSG              0x0B /* aim is error?? */
#define F04_SERVER_ACK_ADV_MSG2           0x0C /* aim is ack too */

/*              FAMILY_LISTS         0x09  */
#define F09_BOTH_ERROR                    0x01
#define F09_CLIENT_REQ_LISTS_RIGHTS       0x02 /* aim too */
#define F09_SERVER_LIST_RIGHTS            0x03 /* aim too */
#define F09_CLIENT_ADD_VISIBLE_LIST       0x05 /* aim too */
#define F09_CLIENT_REM_VISIBLE_LIST       0x06 /* aim too */
#define F09_CLIENT_ADD_INVISIBLE_LIST     0x07 /* aim too */
#define F09_CLIENT_REM_INVISIBLE_LIST     0x08 /* aim too */
#define F09_CLIENT_ADD_SORT_OF_VIS_LIST   0x0A
#define F09_CLIENT_REM_SORT_OF_VIS_LIST   0x0B

/*              FAMILY_USAGE_STATS   0x0B  */
#define F0B_BOTH_ERROR                    0x01
#define F0B_SERVER_MIN_REPORT_INTERVAL    0x02
#define F0B_CLIENT_USAGE_STATS_REPORT     0x03
#define F0B_SERVER_USAGE_STATS_REPORT_ACK 0x03

/*              FAMILY_SAVED_LIST    0x13  */
#define F13_BOTH_ERROR                    0x01
#define F13_CLIENT_REQ_RIGHTS             0x02
#define F13_SERVER_REQ_RIGHTS_ANS         0x03
#define F13_CLIENT_QUERY_SAVED_LIST       0x04
#define F13_CLIENT_REQ_SAVED_LIST         0x05
#define F13_SERVER_SAVED_LIST             0x06
#define F13_CLIENT_RDY_TO_USE_LIST        0x07
#define F13_BOTH_ADD_TO_LIST              0x08
#define F13_BOTH_UPDATE_LIST              0x09
#define F13_BOTH_REMOVE_FROM_LIST         0x0A
#define F13_SERVER_ACK                    0x0E
#define F13_SERVER_LIST_NO_CHANGE         0x0F
#define F13_BOTH_START_MOD_LIST           0x11
#define F13_BOTH_END_MOD_LIST             0x12
#define F13_CLIENT_AUTH_REQUEST_1         0x14
#define F13_CLIENT_AUTH_REQUEST_2         0x18
#define F13_SERVER_AUTH_REQUEST           0x19
#define F13_CLIENT_SEND_GRANT_AUTH        0x1A
#define F13_SERVER_USER_GRANT_AUTH        0x1B
#define F13_SERVER_USER_ADDED_YOU         0x1C

/*              FAMILY_SAVED_DATA    0x15  */
#define F15_BOTH_ERROR                    0x01
#define F15_CLIENT_REQ_SAVE_DATA          0x02
#define F15_SERVER_SAVED_DATA             0x03

/*              FAMILY_NEW_USER      0x17  */
#define F17_BOTH_ERROR                    0x01
#define F17_SERVER_MOTD                   0x03
#define F17_CLIENT_REG_USER               0x04
#define F17_SERVER_ACK_NEW_USER           0x05


#define ANY_ID                             0

/* Capabilities */

#define CAP_AIM_SERVERRELAY "\x09\x46\x13\x49" "\x4C\x7F\x11\xD1\x82\x22\x44\x45\x53\x54\x00\x00"
#define CAP_UTF8 "\x09\x46\x13\x4e" "\x4c\x7f\x11\xd1\x82\x22\x44\x45\x53\x54\x00\x00"
#define CAP_RTFMSGS "\x97\xB1\x27\x51" "\x24\x3C\x43\x34\xAD\x22\xD6\xAB\xF7\x3F\x14\x92"
#define CAP_AIM_ISICQ "\x09\x46\x13\x44" "\x4c\x7f\x11\xd1\x82\x22\x44\x45\x53\x54\x00\x00"

struct _V7Connection {
  int fd;
  GIOChannel *iochannel;
  WORD sequence;
  ConnStatus status;
  char latest_head[10];
  V7Packet *latest_packet;
  int bytes_read_in_latest;
  int total_read;
  BYTE *cookie;
  guint cookielen;
  guint in_watch, err_watch;
  DWORD last_reqid;
  guint16 msg2_downcounter;
  DWORD ourip;
  GList *rateclasses;
  GHashTable *familyclass;
  GList *queued_packets;
  time_t readysince;
};

struct _V7Packet {
  V7Connection *conn;
  BYTE channel;
  WORD len;
  V7Buffer *buf;
};

struct _TLVstack {
  gchar *beg;
  gchar *end;
  int len;
};

struct _TLV {
  WORD type;
  WORD len;
  V7Buffer *buf;
};

struct _Snac {
  const V7Packet *packet;
  WORD  family;
  WORD  type;
  BYTE  flags[2];
  DWORD reqid;
  int   datalen;
  V7Buffer *buf;
};

struct _Request {
  WORD    reqid;
  ReqType  type;
  UIN_T    uin;
};

struct _V7RateClass {
  WORD id;
  DWORD windowsize;
  DWORD clearlevel;
  DWORD alertlevel;
  DWORD limitlevel;
  DWORD disconnectlevel;
  DWORD currentlevel;
  DWORD maxlevel;
  DWORD lasttime;
  BYTE state;
};

struct _RateLimitedResendData {
  V7Connection *conn;
  gchar *buffer;
  WORD length;
  WORD family;
  WORD subfamily;
  guint timerid;
};


gboolean  flap_send(V7Connection *conn, BYTE flap_channel, const gchar *data,
                    WORD len);
V7Packet *read_flap(V7Connection *conn);
void      free_flap(V7Packet *packet);
Snac     *read_snac(const V7Packet *packet);
void 	  free_snac (Snac *snac);

DWORD      snac_send(V7Connection *conn, const gchar *data, WORD len, 
                   WORD family, WORD subtype, BYTE flags[2], DWORD reqid);
TLV       *new_tlv(const BYTE *place);
void       free_tlv (TLV *tlv);
TLV 	  *_read_tlv (V7Buffer *buf);

#define read_tlv(buf, out_err) ( { \
  TLV *mytlv = _read_tlv(buf);     \
  if (!mytlv)                      \
    goto out_err;                  \
  mytlv; } )

 

void        display_tlv(const TLV *tlv);
void        packet_print_v7_snac (const Snac *snac);
void        print_data(const BYTE *data, guint len);


TLVstack   *new_tlvstack       (const gchar *initdata, guint init_len);
void        free_tlvstack      (TLVstack *tlvs);
void        add_tlv            (TLVstack *tlvs, WORD type, const gchar *data,
                                WORD len);
void        add_tlv_w_be       (TLVstack *tlvs, WORD type, WORD data);
void        add_tlv_w_le       (TLVstack *tlvs, WORD type, WORD data);
void        add_tlv_dw_be      (TLVstack *tlvs, WORD type, DWORD data);
void        add_tlv_dw_le      (TLVstack *tlvs, WORD type, DWORD data);
void        add_nontlv         (TLVstack *tlvs, const gchar *data, WORD len);
void        add_nontlv_w_be    (TLVstack *tlvs, WORD data);
void        add_nontlv_w_le    (TLVstack *tlvs, WORD data);
void        add_nontlv_dw_be   (TLVstack *tlvs, DWORD data);
void        add_nontlv_dw_le   (TLVstack *tlvs, DWORD data);
void        add_nontlv_lnts    (TLVstack *tlvs, const gchar *string);
void        add_nontlv_bws     (TLVstack *tlvs, const gchar *string);
gint        v7_reqs_compare_func(Request *req, DWORD *reqid);
#define v7_get_request_by_reqid(the_reqid) g_slist_find_custom(requests, &(the_reqid), (GCompareFunc) v7_reqs_compare_func)

extern V7Connection *mainconnection;

extern GSList *requests;
extern GSList *otherconnections;

extern gboolean isconnecting;

#define v7skip(here, len, skipme)    {(here) += (skipme); (len) -= (skipme);}

char *v7_get_error_message(WORD error);

#endif /* __V7BASE_H__ */
