/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * XML contacts management
 * First created by Patrick Sung (2002)
 */

/*
 * Current XML structure for contacts:
 *
 * <?xml?>
 * <gnomeicu xmlns="http://gnomeicu.sourceforge.net/xmlcontact">
 *   <version>0.94.1</version>
 *   <icq>
 *     <uin>11111111</uin>
 *     <password>mypasswd</password>
 *     <nick>my nick</nick>
 *     <require-auth>true</require-auth>
 *     <webpresence>true</webpresence>
 *     <timeStamp>10234354</timeStamp>
 *     <totalRecord>11</totalRecord>
 *     <status uid="222">normal</status>
 *     <group name="General" gid="111"/>
 *     ... repeat <group/>
 *     <contacts>
 *       <user gid="222" uid="2234">
 *          <uin>34335333</uin>
 *          <nick>general user</nick>
 *       </user>
 *       <user gid="123" uid="332112">
 *          <status uid="323">visible</status> <!-- your status to this user -->
 *          <wait_auth/> <!-- waiting for authorization grant from this user -->
 *          <notify/> <!-- online notification, gnomeicu client side feature -->
 *          <uin>23434343</uin>
 *          <nick>foo</nick>
 *          <lastseen>321313</lastseen> <!-- time_t when last seen (at UTC) -->
 *       </user>
 *       <user> <!-- anonymous users, usually on your ignore list -->
 *          <status uid="3454">ignore</status>
 *          <uin>23565675</uin>
 *       </user>
 *       ... repeat <user/>
 *     </contacts>
 *   </icq>
 * </gnomeicu>
 */

#include <libxml/parser.h>
#include <libxml/parserInternals.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>

#include "common.h"
#include "gnomeicu.h"
#include "groups.h"
#include "response.h"
#include "v7send.h"
#include "v7snac13.h"
#include "util.h"

#define CONTACT_FILE_VERSION "0.94.1"

static void xmlcontact_save_icq (xmlNodePtr icuNode);
static void icq_contact_set_status (xmlNodePtr node, const gchar *status,
                                    int status_id);
static int sort_compare (CONTACT_PTR c1, CONTACT_PTR c2);

typedef enum {
  PARSER_START,
  PARSER_FINISH,
  PARSER_GNOMEICU,
  PARSER_VERSION,
  PARSER_ICQ,
  PARSER_ICQ_UIN,
  PARSER_ICQ_PASSWORD,
  PARSER_ICQ_NICK,
  PARSER_ICQ_REQUIRE_AUTH,
  PARSER_ICQ_WEB_PRESENCE,
  PARSER_ICQ_TIMESTAMP,
  PARSER_ICQ_TOTALRECORD,
  PARSER_ICQ_STATUS,
  PARSER_ICQ_GROUP,
  PARSER_ICQ_CONTACT,
  PARSER_ICQ_USER,
  PARSER_ICQ_WAIT_AUTH,
  PARSER_ICQ_NOTIFY,
  PARSER_ICQ_LAST_SEEN,
  PARSER_UNKNOWN
} ParserState;

/*
static const char *states[] = {
  "START",
  "FINISH",
  "GNOMEICU",
  "VERSION",
  "ICQ",
  "ICQ_UIN",
  "ICQ_NICK",
  "ICQ_TIMESTAMP",
  "ICQ_TOTALRECORD",
  "ICQ_STATUS",
  "ICQ_GROUP",
  "ICQ_CONTACT",
  "ICQ_USER",
  "ICQ_WAIT_AUTH",
  "ICQ_NOTIFY",
  "UNKNOWN"
};
*/

typedef struct _XMLContactParseState XMLContactParseState;
struct _XMLContactParseState {
  ParserState state;

  gint unknown_depth; /* handle recursive unknown tags */
  ParserState prev_state; /* the previous state when first drop into unknown */

  gint gid; /* id of the current element/state */
  gint uid;
  GString *content;

  gboolean owner; /* switch between owner or user mode during parsing */

  UIN_T c_uin;
  gchar *c_nick;
  gint c_temp_status_id;
  gint c_vis_id, c_inv_id, c_ign_id;
  gboolean wait_auth;
  gboolean notify;
  time_t last_seen;
};

/* initialize the state */
static void
xmlContactStartDoc (XMLContactParseState *state)
{
  /* initial state is START state */
  state->state = PARSER_START;

  state->unknown_depth = 0;
  state->prev_state = PARSER_UNKNOWN;

  state->gid = state->uid = 0;
  state->content = g_string_sized_new (128);

  state->owner = TRUE;

  state->c_nick = NULL;
  state->c_temp_status_id = 0;
  state->c_vis_id = 0;
  state->c_inv_id = 0;
  state->c_ign_id = 0;
  state->wait_auth = FALSE;
  state->notify = FALSE;
}

/* free up what is allocated in xmlContactStartDoc() */
static void
xmlContactEndDoc (XMLContactParseState *state)
{
  g_string_free (state->content, TRUE);
  return;
}

/* called when encountered start of an element, also passed attributes, if any */
static void
xmlContactStartEle (XMLContactParseState *state, const xmlChar *name,
                    const xmlChar **atts)
{
  switch (state->state) {
  case PARSER_START:
    if (strcmp (name, "gnomeicu")) {
      g_warning ("Expecting <gnomeicu>. Got %s", name);

      /* go to unknown state, just in case */
      state->prev_state = state->state;
      state->state = PARSER_UNKNOWN;
      state->unknown_depth++;
    } else {
      state->state = PARSER_GNOMEICU;
    }
    break;
  case PARSER_FINISH:
    /* should not start any new element in this state */
    g_assert_not_reached ();
    break;
  case PARSER_GNOMEICU:
    if (!strcmp (name, "version")) {
      state->state = PARSER_VERSION;
    } else if (!strcmp (name, "icq")) {
      state->state = PARSER_ICQ;
    } else {
      state->prev_state = state->state;
      state->state = PARSER_UNKNOWN;
      state->unknown_depth++;
    }
    break;
  case PARSER_VERSION:
    /* should have no element inside <version></version> */
    state->prev_state = state->state;
    state->state = PARSER_UNKNOWN;
    state->unknown_depth++;
    break;
  case PARSER_ICQ: /* inside <icq> */
    if (!strcmp (name, "group")) {
      state->state = PARSER_ICQ_GROUP;
      /* parse attribute: group id (gid) */
      while (atts && *atts) {
        if (!strcmp (*atts, "gid")) {
          state->gid = atoi (*(atts+1));
        }
        atts += 2;
      }
    } else if (!strcmp (name, "status")) {
      state->state = PARSER_ICQ_STATUS;
      /* parse attribute: status uid */
      while (atts && *atts) {
        if (!strcmp (*atts, "uid")) {
          state->uid = atoi (*(atts+1));
          status_uid = state->uid;
        }
        atts += 2;
      }
    } else if (!strcmp (name, "contacts")) {
      state->state = PARSER_ICQ_CONTACT;
      state->owner = FALSE;
    } else if (!strcmp (name, "uin")) {
      state->state = PARSER_ICQ_UIN;
    } else if (!strcmp (name, "nick")) {
      state->state = PARSER_ICQ_NICK;
    } else if (!strcmp (name, "timeStamp")) {
      state->state = PARSER_ICQ_TIMESTAMP;
    } else if (!strcmp (name, "totalRecord")) {
      state->state = PARSER_ICQ_TOTALRECORD;
    } else if (!strcmp (name, "password")) {
      state->state = PARSER_ICQ_PASSWORD;
    } else {
      /* unknown element */
      state->prev_state = state->state;
      state->state = PARSER_UNKNOWN;
      state->unknown_depth++;
    }
    break;
  case PARSER_ICQ_UIN:
  case PARSER_ICQ_NICK:
  case PARSER_ICQ_TIMESTAMP:
  case PARSER_ICQ_TOTALRECORD:
  case PARSER_ICQ_STATUS:
  case PARSER_ICQ_GROUP:
  case PARSER_ICQ_WAIT_AUTH:
  case PARSER_ICQ_NOTIFY:
  case PARSER_ICQ_PASSWORD:
  case PARSER_ICQ_WEB_PRESENCE:
  case PARSER_ICQ_LAST_SEEN:
    /* should have no element after these states */
    state->prev_state = state->state;
    state->state = PARSER_UNKNOWN;
    state->unknown_depth++;
    break;
  case PARSER_ICQ_CONTACT: /* inside <contact> */
    if (!strcmp (name, "user")) {
      state->state = PARSER_ICQ_USER;

      state->gid = 0;
      state->uid = 0;
      /* read the user gid and uid */
      while (atts && *atts) {
        if (!strcmp (*atts, "gid")) {
          state->gid = atoi (*(atts+1));
        } else if (!strcmp (*atts, "uid")) {
          state->uid = atoi (*(atts+1));
        }
        atts += 2;
      }
    } else {
      state->prev_state = state->state;
      state->state = PARSER_UNKNOWN;
      state->unknown_depth++;
    }
    break;
  case PARSER_ICQ_USER: /* inside <user> */
    if (!strcmp (name, "uin")) {
      state->state = PARSER_ICQ_UIN;
    } else if (!strcmp (name, "nick")) {
      state->state = PARSER_ICQ_NICK;
    } else if (!strcmp (name, "lastseen")) {
      state->state = PARSER_ICQ_LAST_SEEN;
    } else if (!strcmp (name, "status")) {
      state->state = PARSER_ICQ_STATUS;
      /* parse status uid */
      while (atts && *atts) {
        if (!strcmp (*atts, "uid")) {
          state->c_temp_status_id = atoi (*(atts+1));
        }
        atts += 2;
      }
    } else if (!strcmp (name, "wait_auth")) {
      state->state = PARSER_ICQ_WAIT_AUTH;
      state->wait_auth = TRUE;
    } else if (!strcmp (name, "notify")) {
      state->state = PARSER_ICQ_NOTIFY;
      state->notify = TRUE;
    } else {
      state->prev_state = state->state;
      state->state = PARSER_UNKNOWN;
      state->unknown_depth++;
    }
    break;
  case PARSER_UNKNOWN:
    state->unknown_depth++;
    break;
  }
  /*g_message ("Start element %s (state %s)", name, states[state->state]);*/
}

static void
xmlContactEndEle (XMLContactParseState *state, const xmlChar *name)
{
  CONTACT_PTR contactdata;
  GSList *contact;
  gint len;

  switch (state->state) {
  case PARSER_UNKNOWN:
    state->unknown_depth--;
    if (state->unknown_depth == 0)
      state->state = state->prev_state;
    break;
  case PARSER_START:
    g_assert_not_reached ();
    break;
  case PARSER_FINISH:
    break;
  case PARSER_GNOMEICU:
    state->state = PARSER_FINISH;
    Contacts = g_slist_sort (Contacts, (GCompareFunc)sort_compare);
    break;
  case PARSER_VERSION:
    state->state = PARSER_GNOMEICU;
    /* compare the version string */
    if (strcmp (state->content->str, CONTACT_FILE_VERSION)) {
      /* there is version difference, force re-sync contacts list */
/*      printf ("see version difference\n");*/
      contacts_resync = TRUE;
    }
    g_string_truncate (state->content, 0);
    break;
  case PARSER_ICQ:
    state->state = PARSER_GNOMEICU;
    break;
  case PARSER_ICQ_UIN:
    if (state->owner) {
      state->state = PARSER_ICQ;
      if (our_info->uin == NULL) /* in case we got it in newsignup.c */
        our_info->uin = g_strdup (state->content->str);
    } else {
      state->state = PARSER_ICQ_USER;
      state->c_uin = g_strdup (state->content->str);
    }
    g_string_truncate (state->content, 0);
    break;
  case PARSER_ICQ_PASSWORD:
    state->state = PARSER_ICQ;
    if (passwd) {
      g_free (passwd);
    }
    passwd = g_strdup (state->content->str);
    g_string_truncate (state->content, 0);
    break;
  case PARSER_ICQ_NICK:
    len = strlen (state->content->str);
    if (state->owner) {
      state->state = PARSER_ICQ;
      /*g_message ("owner nick is: %s", state->content->str);*/
      our_info->nick = g_strdup (state->content->str);
    } else {
      state->state = PARSER_ICQ_USER;
      state->c_nick = g_strdup (state->content->str);
    }
    g_string_truncate (state->content, 0);
    break;
  case PARSER_ICQ_LAST_SEEN:
    if (state->content->len)
      state->last_seen = strtoul(state->content->str, NULL, 10);
    state->state = PARSER_ICQ_USER;
    g_string_truncate (state->content, 0);
    break;
  case PARSER_ICQ_REQUIRE_AUTH:
    state->state = PARSER_ICQ;
    if (!strcmp (state->content->str, "true")) {
      our_info->auth = TRUE;
    } else if (!strcmp (state->content->str, "false")) {
      our_info->auth = FALSE;
    }
    g_string_truncate (state->content, 0);
    break;
  case PARSER_ICQ_WEB_PRESENCE:
    state->state = PARSER_ICQ;
    if (!strcmp (state->content->str, "true")) {
      our_info->webpresence = TRUE;
    } else if (!strcmp (state->content->str, "false")) {
      our_info->webpresence = FALSE;
    }
    g_string_truncate (state->content, 0);
    break;
  case PARSER_ICQ_TIMESTAMP:
    state->state = PARSER_ICQ;
    /*g_message ("time stamp: %s", state->content->str);*/
    list_time_stamp = atoi (state->content->str);
    g_string_truncate (state->content, 0);
    break;
  case PARSER_ICQ_TOTALRECORD:
    state->state = PARSER_ICQ;
    /*g_message ("total record: %s", state->content->str);*/
    record_cnt = atoi (state->content->str);
    g_string_truncate (state->content, 0);
    break;
  case PARSER_ICQ_STATUS:
    if (state->owner) {
      state->state = PARSER_ICQ;

      preset_status = atoi (state->content->str);
    } else {
      state->state = PARSER_ICQ_USER;

      if (strcmp (state->content->str, "visible") == 0) {
        state->c_vis_id = state->c_temp_status_id;
      } else if (strcmp (state->content->str, "invisible") == 0) {
        state->c_inv_id = state->c_temp_status_id;
      } else if (strcmp (state->content->str, "ignore") == 0) {
        state->c_ign_id = state->c_temp_status_id;
      }
    }
    g_string_truncate (state->content, 0);
    break;
  case PARSER_ICQ_GROUP:
    state->state = PARSER_ICQ;
    /*g_message ("got group name %s, with id=%d", state->content->str, state->gid);*/
    groups_add (state->content->str, state->gid);
    g_string_truncate (state->content, 0);
    break;
  case PARSER_ICQ_CONTACT:
    state->state = PARSER_ICQ;
    state->owner = TRUE;
    break;
  case PARSER_ICQ_USER:
    state->state = PARSER_ICQ_CONTACT;
    /* add a new contact to gnomeicu contact structure */
    /*g_print ("Adding user: %s (uin: %d). gid=%d, uid=%d.\n", state->c_nick,
      state->c_uin, state->gid, state->uid);*/

    contactdata = g_new0 (Contact_Member, 1);

    contactdata->uin = g_strdup (state->c_uin);
    if (state->c_nick) {
		g_free(contactdata->nick);
		contactdata->nick = g_strdup(state->c_nick);
	  } else {
		g_free(contactdata->nick);
		contactdata->nick = g_strdup(state->c_uin);
	  }

    contactdata->gid = state->gid;
    contactdata->uid = state->uid;

    if (state->c_inv_id) {
      contactdata->invis_list = TRUE;
      contactdata->invlist_uid = state->c_inv_id;
    } else if (state->c_vis_id) {
      contactdata->vis_list = TRUE;
      contactdata->vislist_uid = state->c_vis_id;
    }

    if (state->c_ign_id) {
      contactdata->ignore_list = TRUE;
      contactdata->ignorelist_uid = state->c_ign_id;
    }

    contactdata->status = STATUS_OFFLINE;
    contactdata->last_time = -1L;
    contactdata->current_ip = -1L;
    contactdata->info = g_new0 (USER_INFO_STRUCT, 1);
    ((USER_INFO_STRUCT *)contactdata->info)->sex = -1;
    contactdata->inlist = TRUE;
    contactdata->tcp_seq = -1;
    contactdata->confirmed = TRUE;
    contactdata->wait_auth = state->wait_auth;
    contactdata->online_notify = state->notify;
    contactdata->last_seen = state->last_seen;

    contact = Find_User (contactdata->uin);
    while (contact && kontakt->gid == 0) {
      Contacts = g_slist_remove (Contacts, contact->data);
      contact = Find_User (contactdata->uin);
    }
    Contacts = g_slist_append (Contacts, contactdata);

	g_free (state->c_uin);
	state->c_uin = NULL;

    if (state->c_nick) {
      g_free (state->c_nick);
      state->c_nick = NULL;
    }
    state->c_vis_id = 0;
    state->c_inv_id = 0;
    state->c_ign_id = 0;
    state->wait_auth = FALSE;
    state->notify = FALSE;
    state->last_seen = 0;

    break;
  case PARSER_ICQ_WAIT_AUTH:
  case PARSER_ICQ_NOTIFY:
    state->state = PARSER_ICQ_USER;
    break;
  }
}

/* read the CDATA inside each <element></element> */
static void
xmlContactChars (XMLContactParseState *state, const xmlChar *chars, gint len)
{
  gint i;

  if (state->state == PARSER_VERSION ||
      state->state == PARSER_ICQ_UIN ||
      state->state == PARSER_ICQ_PASSWORD ||
      state->state == PARSER_ICQ_NICK ||
      state->state == PARSER_ICQ_REQUIRE_AUTH ||
      state->state == PARSER_ICQ_WEB_PRESENCE ||
      state->state == PARSER_ICQ_STATUS ||
      state->state == PARSER_ICQ_GROUP ||
      state->state == PARSER_ICQ_TIMESTAMP ||
      state->state == PARSER_ICQ_TOTALRECORD ||
      state->state == PARSER_ICQ_LAST_SEEN)
    for (i = 0; i < len; i++)
      g_string_append_c (state->content, chars[i]);
}

static void
xmlContactWarn (XMLContactParseState *state, const char *msg, ...)
{
  va_list args;

  va_start (args, msg);
  g_logv ("XML contacts", G_LOG_LEVEL_WARNING, msg, args);
  va_end (args);
}

static void
xmlContactErr (XMLContactParseState *state, const char *msg, ...)
{
  va_list args;

  va_start (args, msg);
  g_logv ("XML contacts", G_LOG_LEVEL_CRITICAL, msg, args);
  va_end (args);
}

static void
xmlContactFatalErr (XMLContactParseState *state, const char *msg, ...)
{
  va_list args;

  va_start (args, msg);
  g_logv ("XML contacts", G_LOG_LEVEL_ERROR, msg, args);
  va_end (args);
}

static xmlSAXHandler contactSAXParser = {
  0, /* internalSubset */
  0, /* isStandalone */
  0, /* hasInternalSubset */
  0, /* hasExternalSubset */
  0, /* resolveEntity */
  0, /* getEntity */
  0, /* entityDecl */
  0, /* notationDecl */
  0, /* attributeDecl */
  0, /* elementDecl */
  0, /* unparsedEntityDecl */
  0, /* setDocumentLocator */
  (startDocumentSAXFunc)xmlContactStartDoc, /* startDocument */
  (endDocumentSAXFunc)xmlContactEndDoc, /* endDocument */
  (startElementSAXFunc)xmlContactStartEle, /* startElement */
  (endElementSAXFunc)xmlContactEndEle, /* endElement */
  0, /* reference */
  (charactersSAXFunc)xmlContactChars, /* characters */
  0, /* ignorableWhitespace */
  0, /* processingInstruction */
  0, /* comment */
  (warningSAXFunc)xmlContactWarn, /* warning */
  (errorSAXFunc)xmlContactErr, /* error */
  (fatalErrorSAXFunc)xmlContactFatalErr, /* fatalError */
};

gboolean
xmlcontact_load (const gchar *filename)
{
  xmlParserCtxtPtr ctxt;
  XMLContactParseState state;

  state.content=NULL;

  ctxt = xmlCreateFileParserCtxt (filename);

  if (!ctxt) {
    contacts_resync = TRUE;
    return FALSE;
  }

  srvlist_exist = TRUE;

  ctxt->sax = &contactSAXParser;
  ctxt->userData = &state;

  xmlParseDocument (ctxt);

  ctxt->sax = NULL;
  xmlFreeParserCtxt (ctxt);

  return TRUE;
}

void
xmlcontact_save (const gchar *filename)
{
  xmlDocPtr doc;
  xmlNsPtr icuNs;
  xmlNodePtr node;
  gchar *tempfilename;

  doc = xmlNewDoc ("1.0");

  doc->xmlRootNode = xmlNewDocNode (doc, NULL, "gnomeicu", NULL);
  icuNs = xmlNewNs (doc->xmlRootNode,
                    "http://gnomeicu.sourceforge.net/xmlcontact", NULL);

  /* set file version */
  node = xmlNewChild (doc->xmlRootNode, NULL, "version", CONTACT_FILE_VERSION);

  /* save icq contacts */
  xmlcontact_save_icq (doc->xmlRootNode);

  tempfilename =  g_strdup_printf("%s.tmp",filename);

  if (xmlSaveFile (filename, doc) <= 0) {
    g_warning ("Cannot save XML contact file: %s", tempfilename);
  }
  else if (!rename (tempfilename, filename))
    g_warning("Cannot save XML contacts into %s, but saved it in %s", filename,
	      tempfilename);
  
  g_free(tempfilename);

  chmod(filename, S_IRUSR|S_IWUSR);

  xmlFreeDoc (doc);
}

/*
 * This function will create the <icq></icq> part of the contacts file
 */
void
xmlcontact_save_icq (xmlNodePtr icuNode)
{
  xmlNodePtr icqNode, contactNode, userNode;
  xmlNodePtr child;
  gchar *tmpstr;
  GSList *tlist, *contact;
  gint contact_count = 0;
  xmlChar *xmlEncoded;
	
  icqNode = xmlNewChild (icuNode, NULL, "icq", NULL);

  xmlNewChild (icqNode, NULL, "uin", our_info->uin);
	
  xmlEncoded = xmlEncodeEntitiesReentrant (NULL, g_markup_escape_text(passwd, strlen(passwd)));
  xmlNewChild (icqNode, NULL, "password", xmlEncoded);
  free (xmlEncoded);
  xmlEncoded = NULL;
	
  xmlEncoded = xmlEncodeEntitiesReentrant (NULL, g_markup_escape_text(our_info->nick, strlen(our_info->nick)));
  xmlNewChild (icqNode, NULL, "nick", xmlEncoded);
  free (xmlEncoded);
  xmlEncoded = NULL;

  xmlNewChild (icqNode, NULL, "require-auth", our_info->auth ? "true" : "false");
  xmlNewChild (icqNode, NULL, "webpresence", our_info->webpresence ? "true" : "false");

  tmpstr = g_strdup_printf ("%d", list_time_stamp);
  xmlNewChild (icqNode, NULL, "timeStamp", tmpstr);
  g_free (tmpstr);

  tmpstr = g_strdup_printf ("%d", record_cnt);
  xmlNewChild (icqNode, NULL, "totalRecord", tmpstr);
  g_free (tmpstr);

  tmpstr = g_strdup_printf ("%d", Current_Status == STATUS_OFFLINE ?
                            STATUS_ONLINE : Current_Status);
  child = xmlNewChild (icqNode, NULL, "status", tmpstr);
  g_free (tmpstr);

  tmpstr = g_strdup_printf ("%d", status_uid);
  xmlSetProp (child, "uid", tmpstr);
  g_free (tmpstr);

  /* save groups */
  tlist = Groups;
  while (tlist) {
    child = xmlNewChild (icqNode, NULL, "group", g_markup_escape_text(((GroupInfo *)tlist->data)->name, strlen(((GroupInfo *)tlist->data)->name)));

    tmpstr = g_strdup_printf ("%d", ((GroupInfo *)tlist->data)->gid);
    xmlSetProp (child, "gid", tmpstr);
    g_free (tmpstr);

    tlist = tlist->next;
  }

  /* save contacts */
  contact = Contacts;
  if (contact)
    contactNode = xmlNewChild (icqNode, NULL, "contacts", NULL);

  for (; contact ; contact = contact->next) {
    
    if (!kontakt->inlist)
      continue;

    contact_count++;

    userNode = xmlNewChild (contactNode, NULL, "user", NULL);

    /* wait_auth */
    if (kontakt->wait_auth) {
      xmlNewChild (userNode, NULL, "wait_auth", NULL);
    }

    /* notify, client side feature */
    if (kontakt->online_notify) {
      xmlNewChild (userNode, NULL, "notify", NULL);
    }

    /* visible state */
    if (kontakt->vis_list) {
      icq_contact_set_status (userNode, "visible", kontakt->vislist_uid);
    }

    /* invisible state */
    if (kontakt->invis_list) {
      icq_contact_set_status (userNode, "invisible", kontakt->invlist_uid);
    }

    /* ignore state */
    if (kontakt->ignore_list) {
      icq_contact_set_status (userNode, "ignore", kontakt->ignorelist_uid);
    }

    /* UIN */
    tmpstr = g_strdup (kontakt->uin);
    xmlNewChild (userNode, NULL, "uin", tmpstr);
    g_free (tmpstr);

    /* skip the users that are only on one of the status list,
       because they don't have gid/uid/nick saved on the contact list */
    if (!kontakt->uid)
      continue;

    /* gid */
    tmpstr = g_strdup_printf ("%d", kontakt->gid);
    xmlSetProp (userNode, "gid", tmpstr);
    g_free (tmpstr);

    /* uid */
    tmpstr = g_strdup_printf ("%d", kontakt->uid);
    xmlSetProp (userNode, "uid", tmpstr);
    g_free (tmpstr);

    /* nick */
    xmlNewChild (userNode, NULL, "nick", g_markup_escape_text(kontakt->nick, strlen(kontakt->nick)));

    /* Last seen online */
    if (kontakt->status != STATUS_OFFLINE)
      kontakt->last_seen = time(NULL);
    tmpstr = g_strdup_printf("%ld", kontakt->last_seen);
    xmlNewChild (userNode, NULL, "lastseen", tmpstr);
    g_free (tmpstr);

  }
}

void icq_contact_set_status (xmlNodePtr node, const gchar *status, int status_id)
{
  xmlNodePtr child;
  gchar *tmpstr;

  child = xmlNewChild (node, NULL, "status", status);
  tmpstr = g_strdup_printf ("%d", status_id);
  xmlSetProp (child, "uid", tmpstr);
  g_free (tmpstr);
}

int
sort_compare (CONTACT_PTR c1, CONTACT_PTR c2)
{
  return strcoll (c1->nick, c2->nick);
}
