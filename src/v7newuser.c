/****************************
  New user for ICQ v6-7 protocol (Oscar)
  Olivier Crete (c) 2001
  GnomeICU
*****************************/

#include "common.h"
#include "gnomeicu.h"
#include "gnomeicu-spinner.h"
#include "gtkfunc.h"
#include "userserver.h"
#include "util.h"
#include "v7newuser.h"
#include "gnomecfg.h"
#include "userserver.h"
#include "v7login.h"

#include <libgnomeui/libgnomeui.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

gboolean v7_newuser_connected (GIOChannel *channel, GIOCondition condition,
			       V7Connection *conn);
gboolean v7_newuser_handler (GIOChannel *source, GIOCondition condition,
                           V7Connection *conn);
gboolean v7_newuser_error_handler (GIOChannel *iochannel,
                                   GIOCondition condition, V7Connection *conn);



void v7_new_user(gchar *passwd)
{

  V7Connection *conn;
  gchar *login_server;
  gint login_port;
  struct sockaddr_in serv_addr;
  struct hostent *hp;
  GtkWidget *dialog;

#ifdef TRACE_FUNCTION
  g_print( "v7_new_user\n" );
#endif

  login_server = preferences_get_string (PREFS_ICQ_SERVER_NAME);
  login_port = preferences_get_int (PREFS_ICQ_SERVER_PORT);

  gnomeicu_spinner_start();

  conn = g_new0(V7Connection, 1);



  conn->status = NOT_CONNECTED;
  conn->last_reqid = 1;
  conn->cookie = g_strdup(passwd);
  otherconnections = g_slist_append(otherconnections, conn);


  conn->fd = socket(PF_INET, SOCK_STREAM, 0);

  if (conn->fd < 0)
    goto out_error;

  if (fcntl(conn->fd, F_SETFL, fcntl(conn->fd, F_GETFL, 0)|O_NONBLOCK) != 0)
    goto out_error;

  conn->iochannel = g_io_channel_unix_new(conn->fd);

  conn->in_watch = g_io_add_watch(conn->iochannel, G_IO_IN|G_IO_HUP|G_IO_ERR, 
				  (GIOFunc)v7_newuser_connected, conn);


  hp = gethostbyname(login_server);
  
  g_free (login_server);

  if (!hp)
    goto out_error;

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(login_port);
  memcpy(&serv_addr.sin_addr, hp->h_addr, hp->h_length);
  //serv_addr.sin_addr.s_addr = inet_addr(hp->h_addr);

  if (connect(conn->fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) != 0
      && errno != EINPROGRESS) {
    g_print("errno %d\n", errno);
    goto out_error;
  }

  return;

 out_error:
  dialog = gtk_message_dialog_new(GTK_WINDOW (MainData->window), 
				  GTK_DIALOG_DESTROY_WITH_PARENT,
				  GTK_MESSAGE_WARNING,
				  GTK_BUTTONS_OK,
				  _("Can't connect to server\nCreating new user failed\n Error: %s (%d)"), strerror(errno), errno);  

  otherconnections = g_slist_remove(otherconnections, conn);
  g_io_channel_shutdown(conn->iochannel, FALSE, NULL);
  g_free(conn);
  
  Current_Status = STATUS_OFFLINE;
  gnomeicu_set_status_button (FALSE);
  gnomeicu_spinner_stop ();

  gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_destroy(dialog);



}

gboolean v7_newuser_connected (GIOChannel *channel, GIOCondition condition,
			       V7Connection *conn)
{

#ifdef TRACE_FUNCTION
  g_print( "v7_newuser_connected\n" );
#endif

  g_assert(conn != NULL);

  if (condition & (G_IO_ERR|G_IO_HUP|G_IO_NVAL)) {
    g_io_channel_shutdown(conn->iochannel, FALSE, NULL);
    otherconnections = g_slist_remove(otherconnections, conn);
    g_free(conn);

    Current_Status = STATUS_OFFLINE;
    gnomeicu_set_status_button (FALSE);
    gnomeicu_spinner_stop ();
    gnome_warning_dialog(_("Can't connect to server\n Adding new user failed"));
    return FALSE;
  }

  conn->status = CONNECTING;

  conn->in_watch = g_io_add_watch ( conn->iochannel, G_IO_IN|G_IO_PRI,
                   (GIOFunc) v7_newuser_handler, conn);
  conn->err_watch = g_io_add_watch ( conn->iochannel,
                                     G_IO_ERR|G_IO_HUP|G_IO_NVAL,
                                     (GIOFunc) v7_newuser_error_handler, conn);
  return FALSE;
}
  
gboolean v7_newuser_error_handler (GIOChannel *iochannel,
                                   GIOCondition condition, V7Connection *conn)
{
#ifdef TRACE_FUNCTION
  g_print( "v7_newuser_error_handler\n" );
#endif

  return FALSE; /* not sure what to return -- menesis */
}

gboolean v7_newuser_handler (GIOChannel *iochannel, GIOCondition condition,
                     V7Connection *conn)
{
  V7Packet *packet;
  Snac *snac;
  
#ifdef TRACE_FUNCTION
  g_print( "v7_newuser_handler\n" );
#endif


  /* If we dont have the whole packet stop here */
  if ((packet = read_flap(conn)) == NULL)
    return TRUE;

  
  if (packet->channel == 1 &&
      packet->len == 4  &&
      !strncmp(packet->buf->here, "\0\0\0\x1", 4)) {
    const gchar buffer[4] = "\0\0\0\01";
    const gchar buffer2[44] = "\0\x01\0\0" "\0\0\0\0" "\x28\0\x03\0" "\0\0\0\0"
      "\0\0\0\0" "\x03\x46\0\0" "\x03\x46\0\0" "\0\0\0\0" "\0\0\0\0"
      "\0\0\0\0" "\0\0\0\0" ;
    const gchar buffer3[10] = "\x03\x46\0\0" "\0\0\0\0" "\xCF\x01";
    gchar *finalbuffer;
      
    if (! flap_send(conn, CHANNEL_NEWCONN, buffer, 4))
      conn->status = BROKEN;
    
    /* The desired password is stored in conn->cookie */
    
    finalbuffer = g_malloc0(44+2+strlen(conn->cookie)+1+10);
    
    g_memmove(finalbuffer, buffer2, 44);

    Word_2_CharsBE(finalbuffer+2, 40+2+strlen(conn->cookie)+1+10);
    
    Word_2_Chars(finalbuffer+44, strlen(conn->cookie)+1);
    
    g_memmove(finalbuffer+44+2, conn->cookie, strlen(conn->cookie)+1);
    g_memmove(finalbuffer+44+2+strlen(conn->cookie)+1, buffer3, 10);

    snac_send(conn, finalbuffer, 44+2+strlen(conn->cookie)+1+10,
              FAMILY_17_NEW_USER, F17_CLIENT_REG_USER, NULL, ANY_ID);
    
  } else if (packet->channel == 2) {
    snac = read_snac(packet);
    
    if (snac->family == FAMILY_17_NEW_USER &&
        snac->type == F17_SERVER_ACK_NEW_USER ) {

      v7_buffer_skip(snac->buf, 46, out_err);

      our_info->uin = g_strdup_printf ("%d", 
          v7_buffer_get_dw_le(snac->buf, out_err));
      passwd = conn->cookie;

      g_io_channel_shutdown(conn->iochannel, TRUE, NULL);
      otherconnections = g_slist_remove(otherconnections, conn);
      g_free(conn);

      gnomeicu_spinner_stop();

      /* Starting it here because we now have the UIN */
      userserver_init();

      v7_new_login_session();
    } else if (snac->family == FAMILY_17_NEW_USER &&
	       snac->type == F17_BOTH_ERROR ) {
      GtkWidget *errordialog;
      WORD error = v7_buffer_get_w_be (snac->buf, out_err);

      g_source_remove(conn->in_watch);
      g_io_channel_shutdown(conn->iochannel, TRUE, NULL);
      otherconnections = g_slist_remove(otherconnections, conn);
      g_free(conn);

      gnomeicu_spinner_stop();

      errordialog = gtk_message_dialog_new(GTK_WINDOW (MainData->window),
          GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
          GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
          _("The ICQ server does not allow you "
              "to create a new user\nExiting GnomeICU\n%s"), 
          v7_get_error_message(error));

      gtk_dialog_run(GTK_DIALOG(errordialog));
      gtk_main_quit();
      
    } else {
      g_warning(_("Bizzare things happened, we dont have the right snac!\n"));
      return TRUE;
    }  
  } else {
    g_warning(_("Something very abnormal happened\n"));
  }  
  
  return TRUE;

 out_err:
  g_warning("Badly formed packet discarded");
  return TRUE;
}

