#ifndef  __LISTWINDOW_H__
#define  __LISTWINDOW_H__

#include <glib.h>

typedef gboolean (*ContactFilterCallback) (Contact_Member *contact);
typedef void (*ContactCallback) (Contact_Member *contact);
typedef void (*ListCallback) (GList *contactpairs, gpointer user_data);


GtkWidget* list_window_new_filter( const gchar *title,
				   const gchar *label_text,	
				   ContactCallback add_contact,
				   ContactCallback remove_contact,
				   ContactFilterCallback is_in_list);

GtkWidget* list_window_new( const gchar *title,
			    const gchar *label_text,
			    const gchar *button_name,
			    const gchar *button_pixmap,
			    ListCallback button_clicked,
			    gpointer user_data, 
			    GList *contactpairs);


#endif /* __LISTWINDOW_H__ */
