/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Migrate local contacts list to server side contacts list (GUI code)
 * first created by Patrick Sung (2002)
 */

#ifndef __CL_MIGRATE_H__
#define __CL_MIGRATE_H__

#include <glib.h>

gboolean migration_dialog (gpointer data);

#endif /* __CL_MIGRATE_H__ */
