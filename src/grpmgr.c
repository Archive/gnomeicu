/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * group management dialog
 * first written by Patrick Sung (2002)
 */

#include "common.h"

#include "gnomeicu.h"
#include "groups.h"
#include "gtkfunc.h"
#include "showlist.h"
#include "util.h"
#include "v7base.h"
#include "v7snac13.h"

#include <gtk/gtkliststore.h>

enum {
  GROUP_NAME_COL,
  GROUP_ID_COL,
  GROUP_LIST_N_COLUMNS
};

static void grpmgr_close_button_clicked (GtkWidget *widget, GroupInfo *d);
static void grpmgr_newgrp_clicked (GtkWidget *w, GtkEntry *entry);
static void grpmgr_delgrp_clicked (GtkWidget *w, GroupInfo *sel_grp);
void grpmgr_new_grp_entry_changed (GtkWidget *widget, gpointer *data);
static void grpmgr_selection_changed_cb (GtkTreeSelection *sel, GroupInfo *d);
static void grpmgr_rename_button_clicked (GtkWidget *w, GtkTreeView *treeview);
static void grpmgr_dialog_build_list (GladeXML *dialog, GroupInfo *sel_grp);
static void grpmgr_rename (GtkWidget *widget, gchar *path_string, 
			   gchar *new_text, GtkTreeModel *store);


void
grpmgr_window (GtkWidget *widget, gpointer data)
{
  GladeXML *xml;
  GtkWidget *close_button, *newgrp_button, *delgrp_button, *new_grp_entry,
    *rename_button;
  GtkWidget *grpmgr_dlg;

  GroupInfo *selected_group_data;

  /* make sure we can load the glade file, or else give user a warning */
  xml = gicu_util_open_glade_xml ("grpmgr.glade", "grpmgr_dlg");
  if (xml == NULL)
    return;

  selected_group_data = g_new0 (GroupInfo, 1);

  grpmgr_dialog_build_list (xml, selected_group_data);

  grpmgr_dlg = glade_xml_get_widget (xml, "grpmgr_dlg");
  rename_button = glade_xml_get_widget (xml, "rename_grp_button");
  close_button = glade_xml_get_widget (xml, "close_button");
  newgrp_button = glade_xml_get_widget (xml, "new_grp_button");
  delgrp_button = glade_xml_get_widget (xml, "delete_grp_button");
  new_grp_entry = glade_xml_get_widget (xml, "new_grp_entry");

  /* set data for some of the object (for access by signal handler) */
  g_object_set_data (G_OBJECT (grpmgr_dlg), "treeview",
                     glade_xml_get_widget (xml,"group_treeview"));
  g_object_set_data (G_OBJECT (rename_button), "dlg", grpmgr_dlg);
  g_object_set_data (G_OBJECT (close_button), "dlg", grpmgr_dlg);
  g_object_set_data (G_OBJECT (newgrp_button), "dlg", grpmgr_dlg);
  g_object_set_data (G_OBJECT (delgrp_button), "dlg", grpmgr_dlg);
  g_object_set_data (G_OBJECT (new_grp_entry), "newgrp_button", newgrp_button);

  /* ---- connect signals ---- */
  /* renamebutton */
  g_signal_connect (G_OBJECT (rename_button), "clicked",
                    G_CALLBACK (grpmgr_rename_button_clicked),
                    glade_xml_get_widget (xml,"group_treeview"));
  /* closebutton */
  g_signal_connect (G_OBJECT (close_button), "clicked",
                    G_CALLBACK (grpmgr_close_button_clicked),
                    selected_group_data);
  /* Add New Group button */
  g_signal_connect (G_OBJECT (newgrp_button), "clicked",
                    G_CALLBACK (grpmgr_newgrp_clicked),
                    glade_xml_get_widget (xml, "new_grp_entry"));

  /* Delete Empty group button */
  g_signal_connect (G_OBJECT (delgrp_button), "clicked",
                    G_CALLBACK (grpmgr_delgrp_clicked),
                    selected_group_data);

  glade_xml_signal_autoconnect (xml);
  g_object_unref (G_OBJECT (xml));
}

void
grpmgr_close_button_clicked (GtkWidget *widget, GroupInfo *d)
{
  /* delete what we have allocated when the dialog is created */
  g_free (d);

  gtk_widget_destroy (g_object_get_data (G_OBJECT (widget), "dlg"));
}

void
grpmgr_new_grp_entry_changed (GtkWidget *widget, gpointer *data)
{
  GtkWidget *newgrp_button = g_object_get_data (G_OBJECT (widget), "newgrp_button");

  if (gtk_entry_get_text (GTK_ENTRY (widget))[0])
    gtk_widget_set_sensitive (newgrp_button, TRUE);
  else
    gtk_widget_set_sensitive (newgrp_button, FALSE);
}

void
grpmgr_selection_changed_cb (GtkTreeSelection *sel, GroupInfo *d)
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  GtkWidget *rename_button, *delgrp_button;
  guint gid;
  gchar *gname;

  if (gtk_tree_selection_get_selected (sel, &model, &iter)) {
    rename_button = g_object_get_data (G_OBJECT (sel), "rename_button");
    delgrp_button = g_object_get_data (G_OBJECT (sel), "delgrp_button");

    gtk_widget_set_sensitive (rename_button, TRUE);
    gtk_widget_set_sensitive (delgrp_button, TRUE);

    gtk_tree_model_get (model, &iter, GROUP_NAME_COL, &gname,
                        GROUP_ID_COL, &gid, -1);
    
    g_free(d->name);
    d->name = gname;
    d->gid = gid;
  }
}

void
grpmgr_newgrp_clicked (GtkWidget *w, GtkEntry *entry)
{
  guint gid;
  GtkWidget *grp_tree = NULL;
  GtkTreeModel *model = NULL;
  GtkTreeIter iter;

  if (!is_connected(GTK_WINDOW(g_object_get_data (G_OBJECT (w), "dlg")), _("You can not create a group while disconnected.")))
    return;
  gid = groups_gen_gid ();
  groups_add (gtk_entry_get_text (entry), gid);

  v7_add_new_group (mainconnection, gid);

  grp_tree = g_object_get_data (g_object_get_data (G_OBJECT (w), "dlg"),
                                "treeview");
  if (grp_tree == NULL) {
    g_print ("grp_tree == NULL in grpmgr_newgrp_clicked\n");
    return;
  }

  model = gtk_tree_view_get_model (GTK_TREE_VIEW (grp_tree));
  if (model == NULL) {
    g_print ("model == NULL in grpmgr_newgrp_clicked\n");
    return;
  }
  gtk_list_store_append (GTK_LIST_STORE (model), &iter);
  gtk_list_store_set (GTK_LIST_STORE (model), &iter,
                      GROUP_NAME_COL, gtk_entry_get_text (entry),
                      GROUP_ID_COL, gid,
                      -1);

  gtk_entry_set_text(entry, "");

}

void
grpmgr_delgrp_clicked (GtkWidget *w, GroupInfo *sel_grp)
{
  GSList *contact;
  GtkWidget *dialog;
  GtkWindow *parent = g_object_get_data (G_OBJECT (w), "dlg");
  GtkWidget *grp_tree;
  GtkTreeModel *model;
  GtkTreeIter iter;
  gint gid;
  GSList *ginfo;
  guint counter = 0;

  if (sel_grp->gid == 0) {
    /* g_print ("none selected\n"); */
    return;
  }

  /* check if this is the last group that it is trying to delete */
  for (ginfo = Groups; ginfo != NULL; ginfo = ginfo->next, counter++) {
    ;
  }
  if (counter <= 1) {
    /* alert user if group is not empty */
    dialog = gtk_message_dialog_new (parent, GTK_DIALOG_DESTROY_WITH_PARENT,
                                     GTK_MESSAGE_WARNING,
                                     GTK_BUTTONS_CLOSE,
                                     _("Cannot delete the last remaining group."));
    gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
    return;
  }

  /* check if the group is empty by searching thru the contact list for gid */
  for (contact = Contacts; contact != NULL; contact = contact->next) 
    if (kontakt->gid == sel_grp->gid) {
      /* alert user if group is not empty */
      dialog = gtk_message_dialog_new (parent, GTK_DIALOG_DESTROY_WITH_PARENT,
                                       GTK_MESSAGE_WARNING,
                                       GTK_BUTTONS_CLOSE,
                                       _("Cannot delete non-empty group."));
      gtk_dialog_run (GTK_DIALOG (dialog));
      gtk_widget_destroy (dialog);
      return;
    }

  if (!is_connected(GTK_WINDOW(g_object_get_data (G_OBJECT (w), "dlg")),
                    _("You can not remove a group while disconnected.")))
    return;

  /* delete the group */
  v7_delete_empty_group (mainconnection, sel_grp->gid);

  /* update group structure */
  groups_remove (sel_grp->gid);

  /* finalize the delete */
  v7_finalize_delete_group (mainconnection);

  /* update the list view on the dialog */
  grp_tree = g_object_get_data (g_object_get_data (G_OBJECT (w), "dlg"),
                                "treeview");
  model = gtk_tree_view_get_model (GTK_TREE_VIEW (grp_tree));

  if (!gtk_tree_model_get_iter_first (model, &iter))
    return;

  do {
    gtk_tree_model_get (model, &iter, GROUP_ID_COL, &gid, -1);
    if (gid == sel_grp->gid) {
      gtk_list_store_remove (GTK_LIST_STORE (model), &iter);
      break;
    }
  } while (gtk_tree_model_iter_next (model, &iter));

}

void
grpmgr_rename_button_clicked (GtkWidget *w, GtkTreeView *treeview)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  GtkTreePath *path;

  if (!is_connected(GTK_WINDOW(g_object_get_data (G_OBJECT (w), "dlg")), _("You can not rename a group while disconnected.")))
    return;

  model = gtk_tree_view_get_model(treeview);
  
  if (!gtk_tree_selection_get_selected(gtk_tree_view_get_selection(treeview), &model, &iter))
    return;

  path = gtk_tree_model_get_path(model, &iter);

  gtk_tree_view_set_cursor(treeview, path, gtk_tree_view_get_column(treeview, 0), TRUE);

  
  gtk_tree_path_free(path);

  
}

void grpmgr_dialog_build_list (GladeXML *grpmgr_dlg, GroupInfo *sel_grp)
{
  GtkWidget *grp_tree;
  GtkListStore *store;
  GtkTreeIter iter;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  GtkTreeSelection *select;
  GSList *ginfo;
  guint counter = 0;

  /* create the model with group name, and group id */
  store = gtk_list_store_new (GROUP_LIST_N_COLUMNS, G_TYPE_STRING, G_TYPE_UINT);
  for (ginfo = Groups; ginfo != NULL; ginfo = ginfo->next, counter++) {
    gtk_list_store_append (store, &iter);
    gtk_list_store_set (store, &iter,
                        GROUP_NAME_COL, ((GroupInfo *)(ginfo->data))->name,
                        GROUP_ID_COL, ((GroupInfo *)(ginfo->data))->gid,
                        -1);
  }

  if (counter > 4) {
    /* have many groups, make the window bigger */
    gint w,h;
    GtkWindow *dlg = GTK_WINDOW(glade_xml_get_widget (grpmgr_dlg, "grpmgr_dlg"));

    gtk_window_get_size (dlg, &w, &h);
    gtk_window_resize (dlg, w, h+100);
  }

  grp_tree = glade_xml_get_widget (grpmgr_dlg, "group_treeview");
  gtk_tree_view_set_model (GTK_TREE_VIEW (grp_tree), GTK_TREE_MODEL (store));

  /* renderer and tree_view_column */
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Group Name",
                                                     renderer,
                                                     "text", GROUP_NAME_COL,
                                                     NULL);
  g_object_set(G_OBJECT(renderer), "editable", TRUE, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (grp_tree), column);

  select = gtk_tree_view_get_selection (GTK_TREE_VIEW (grp_tree));
  gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);

  /* set data for the selection widget */
  g_object_set_data (G_OBJECT (select), "rename_button",
                     glade_xml_get_widget (grpmgr_dlg, "rename_grp_button"));
  g_object_set_data (G_OBJECT (select), "delgrp_button",
                     glade_xml_get_widget (grpmgr_dlg, "delete_grp_button"));
  g_object_set_data (G_OBJECT (select), "grp_entry",
                     glade_xml_get_widget (grpmgr_dlg, "new_grp_entry"));
  /*  g_object_set_data (G_OBJECT (store), "dlg", 
		     glade_xml_get_widget (grpmgr_dlg, "grpmgr_dlg")); 
  */
  g_signal_connect (G_OBJECT (select), "changed",
                    G_CALLBACK (grpmgr_selection_changed_cb),
                    sel_grp);
  g_signal_connect (G_OBJECT (renderer), "edited",
		    G_CALLBACK(grpmgr_rename), store);
}

void grpmgr_rename (GtkWidget *widget, gchar *path_string, gchar *new_text,
		    GtkTreeModel *store)
{

  GtkTreePath *path = gtk_tree_path_new_from_string(path_string);
  GtkTreeIter iter;
  gint gid;

  if (!is_connected(GTK_WINDOW(g_object_get_data (G_OBJECT (store), "dlg")), _("You can not rename a group while disconnected.")))
    return;



  gtk_tree_model_get_iter(store, &iter, path);

  gtk_tree_model_get (store, &iter, GROUP_ID_COL, &gid, -1);

  groups_rename_by_gid (gid, new_text);
  
  v7_update_group_info (mainconnection, gid);

  gtk_list_store_set (GTK_LIST_STORE(store), &iter,
		      GROUP_NAME_COL, new_text, -1);
}
