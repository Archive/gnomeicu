/******************************
 Configuration parser
 (c) 1999 Jeremy Wise
 GnomeICU
*******************************/

#include "common.h"

#include "gnomeicu.h"
#include "groups.h"
#include "history.h"
#include "v7snac13.h"
#include "xmlcontact.h"

#include <libgnome/gnome-config.h>
#include <stdlib.h>
#include <string.h>

static int sort_compare (CONTACT_PTR c1, CONTACT_PTR c2);


void
Read_RC_File (void)
{
	/* To be used in the new config section */
	void *iter;
	gchar *iterKey = NULL;
	gchar *iterData = NULL;

	CONTACT_PTR contactdata;

	gchar *version;

	gboolean have_group = FALSE;
        gboolean have_vis = FALSE;
        gboolean have_inv = FALSE;
        gboolean have_ign = FALSE;

        gchar *xml_fname;
	
	gnome_config_push_prefix( configfilename );

	/* Version */
	version = gnome_config_get_string( "User/Version=0" );
	if (strcmp (version, "0") != 0 && strcmp (version, VERSION)) {
          /* Do this if they've upgraded */

          /* resync the contacts list */
          contacts_resync = TRUE;
	}

	g_free( version );

	if( !passwd )
		passwd = gnome_config_private_get_string( "User/Password=" );

	if( !strcmp( passwd, "" ) )
	{
		passwd = gnome_config_get_string( "User/Password=" );
		if( strcmp( passwd, "" ) )
			gnome_config_clean_key( "User/Password" );
	}

	/* This is to convert the history to the new format */
	/* It will be gone users have converted */
	if ( (version=gnome_config_get_string( "Existence/exists" )) != NULL ){
	  if (gnome_config_get_int( "Existence/exists" ) == 1) {
	    convert_history();
	    gnome_config_set_int( "Existence/exists", 2);
	  }
	}
	
	g_free(version);
	
	if (!Away_Message) {
	  gchar *key = g_strdup_printf (PREFS_GNOMEICU_AWAY_MSG "%d",
			    preferences_get_int (PREFS_GNOMEICU_AWAY_DEFAULT));
	  Away_Message = preferences_get_string (key);
	  g_free (key);
	}


        /* GCONF_PREFS code below are preserve for backward compat */
        /******* stuffs below are for backward compatibility ********/
        xml_fname = g_strdup_printf ("%s/.icq/contacts.xml", g_get_home_dir());
        if (xmlcontact_load (xml_fname)) {
          g_free (xml_fname);

          gnome_config_pop_prefix();
          return;
        }
        g_free (xml_fname);

        /* read server side contacts list section (ServerCL) */
	srvlist_exist = gnome_config_get_bool ("ServerCL/exists=false");
	if (srvlist_exist) {
          /* need to re-read the list to correctly sync to contacts.xml */
	  contacts_resync = TRUE;

          gnome_config_pop_prefix();
          return;
	}

        /* Check if we have groups */
	have_group = gnome_config_has_section ("Groups");
        have_vis = gnome_config_has_section ("Visible");
        have_inv = gnome_config_has_section ("Invisible");
        have_ign = gnome_config_has_section ("Ignore");

	/* New Contacts section */
	if( gnome_config_has_section( "NewContacts" ) )
	{
	  gchar *tmpstr = NULL;
	  guint uinid = 0;
          void *iter2;
          gchar *iterKey2 = NULL;
          gchar *iterData2 = NULL;

		iter = gnome_config_init_iterator( "NewContacts" );
		while( 1 )
		{
			g_free( iterKey );
			g_free( iterData );

			iter = gnome_config_iterator_next( iter, &iterKey, &iterData );

			if( iter == NULL )
				break;
			if( atoi( iterKey ) == 0 )
				continue;
			contactdata = g_new0( Contact_Member, 1 );
			if( strstr( iterData, "Snot" ) ) /* Online Notification */
				contactdata->online_notify = TRUE;
			else
				contactdata->online_notify = FALSE;

                        if( strstr( iterData, "Sign" ) ) /* Ignore List */
                          contactdata->ignore_list = TRUE;
                        else
                          contactdata->ignore_list = FALSE;

                        if( strstr( iterData, "Sinv" ) ) /* Invisible List */
                          contactdata->invis_list = TRUE;
                        else if( strstr( iterData, "Svis" ) ) /* Visible List */
                          contactdata->vis_list = TRUE;
			else {
                          contactdata->invis_list = FALSE;
                          contactdata->vis_list = FALSE;
			}

			contactdata->uin = g_strdup (iterKey);

			if (have_group) {
			  /* have group, that means we have to read uin-id */
			  gchar *eptr;

			  uinid = (guint)strtol (iterData, &eptr, 10);
			  contactdata->uid = uinid;
			  tmpstr = eptr+1;
			} else {
			  tmpstr = iterData;
			}

			if( tmpstr[ 0 ] == ',' )
			  sprintf( contactdata->nick, "%s", iterKey );
			else {
                          if( strchr( tmpstr, ',' ) != NULL )
                            strncpy( contactdata->nick, tmpstr,
                                     ( strchr( tmpstr, ',' ) - tmpstr ) );
                          else
                            strncpy( contactdata->nick, tmpstr,
                                     sizeof( contactdata->nick ) );
                        }

                        /* check visible, invisible, and ignore list
                         * for the current user
                         */
                        if (have_vis) {
                          iter2 = gnome_config_init_iterator ("Visible");
                          while (iter2) {
                            iter2 = gnome_config_iterator_next (iter2, &iterKey2,
                                                                &iterData2);
                            if (iter2 && !strcmp (contactdata->uin, iterKey2)) {
                              contactdata->vis_list = TRUE;
                              contactdata->vislist_uid = atoi (iterData2);
                              g_free (iterKey2);
                              g_free (iterData2);
                              break;
                            }
                            g_free (iterKey2);
                            g_free (iterData2);
                          }
                        }

                        if (have_inv) {
                          iter2 = gnome_config_init_iterator ("Invisible");
                          while (iter2) {
                            iter2 = gnome_config_iterator_next (iter2, &iterKey2,
                                                                &iterData2);
                            if (iter2 && !strcmp (contactdata->uin, iterKey2)) {
                              contactdata->invis_list = TRUE;
                              contactdata->invlist_uid = atoi (iterData2);
                              g_free (iterKey2);
                              g_free (iterData2);
                              break;
                            }
                            g_free (iterKey2);
                            g_free (iterData2);
                          }
                        }

                        if (have_ign) {
                          iter2 = gnome_config_init_iterator ("Ignore");
                          while (iter2) {
                            iter2 = gnome_config_iterator_next (iter2, &iterKey2,
                                                                &iterData2);
                            if (iter2 && !strcmp (contactdata->uin, iterKey2)) {
                              contactdata->ignore_list = TRUE;
                              contactdata->ignorelist_uid = atoi (iterData2);
                              g_free (iterKey2);
                              g_free (iterData2);
                              break;
                            }
                            g_free (iterKey2);
                            g_free (iterData2);
                          }
                        }

			contactdata->sok = 0;
			contactdata->stored_messages = NULL;
			contactdata->status = STATUS_OFFLINE;
			contactdata->last_time = -1L;
			contactdata->current_ip = -1L;
			contactdata->info = g_new0( USER_INFO_STRUCT, 1 );
			((USER_INFO_STRUCT*)contactdata->info)->sex = -1;
			contactdata->inlist = TRUE;
			contactdata->tcp_seq = -1;
			contactdata->msg_dlg_xml = NULL;
			contactdata->confirmed = TRUE;

			Contacts = g_slist_insert_sorted( Contacts, contactdata, (GCompareFunc)sort_compare );
		}
	}

	if (have_group == FALSE) {
	  /* force a re-read of the server contacts list, to update group info */
	  if (srvlist_exist)
	    list_time_stamp = record_cnt = 0;
	} else { /* we have groups, read it */
	  /* format:
	   *   groupid=grpname,count,uinid1,uinid2...
	   */
	  gchar *grpname;
	  gint user_cnt, grp_cnt=0;
	  gint loop_cnt;
	  gchar *tmpstr;
	  guint gid, uid;

	  iter = gnome_config_init_iterator ("Groups");

	  g_free (iterKey);
	  g_free (iterData);
	  while ( (iter = gnome_config_iterator_next (iter, &iterKey, &iterData)) != NULL) {
	    grp_cnt++;
	    /* read group id */
	    grpname = g_strndup (iterData, strchr (iterData, ',') - iterData);
	    gid = atoi (iterKey);
	    groups_add (grpname, gid);
 
	    /* get number of user belong to this group */
	    user_cnt = strtol (strchr (iterData, ',') + 1, NULL, 10);

	    /* get all the uin-id relate to this group */
	    tmpstr = strchr (strchr (iterData, ',')+1, ',')+1;
	    for (loop_cnt = 0; loop_cnt < user_cnt; loop_cnt++) {
              GSList *contact;
	      uid = (guint) strtol (tmpstr, NULL, 10);
              
              /* search for users using uid */
              contact = Contacts;
              while (contact) {
                if (kontakt->uid == uid)
                  break;
                contact = contact->next;
              }
              if (contact)
                kontakt->gid = gid;
	      tmpstr = strchr (tmpstr, ',') + 1;
	    }

	    g_free (grpname);
	    g_free (iterKey);
	    g_free (iterData);
	  }
	  if (grp_cnt == 0)
	    have_group = FALSE;
	}

	gnome_config_pop_prefix();

	return;
}

void
Save_RC (void)
{
  gchar *fname;

  /* save contacts to the xml file */
  fname = g_strdup_printf ("%s/.icq/contacts.xml", g_get_home_dir());
  xmlcontact_save (fname);
  g_free (fname);

  return;
}

int
sort_compare (CONTACT_PTR c1, CONTACT_PTR c2)
{
  g_assert (c1 != NULL && c2 != NULL);
  return strcoll (c1->nick, c2->nick);
}
