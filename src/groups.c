/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * Groups related functions (data model)
 * first created by Patrick Sung (Jan 2002)
 */

#include "groups.h"

#include "common.h"
#include "gnomeicu.h"
#include "util.h"
#include "showlist.h"

#include <stdlib.h>
#include <string.h>

WORD contact_gen_uid ()
{
  static guint counter = 0;
  WORD id;

  if (counter > 32767) {
    g_warning ("We are running out of ids to add more users.");
    return 0;
  }

  do {
    id = (WORD)(1 +  (int)(32766.0 * rand()/ (RAND_MAX+1.0)));
  } while ((id == 0 || id == 1 || Find_User_uid (id))  &&
	   counter <= 32767);

  if (counter > 32767)
    return 0;

  counter = 0;

  return id;
} /* end contact_gen_uid () */

WORD groups_gen_gid ()
{
  static guint counter = 0;
  WORD id;

  if (counter > 32767) {
    g_warning ("We are running out of ids to add more group.");
    return 0;
  }

  do {
    id = (WORD)(1 +  (int)(32766.0 * rand()/ (RAND_MAX+1.0)));
  } while ((id == 0 || groups_name_by_gid (id))  &&
	   counter <= 32767);

  if (counter > 32767)
    return 0;

  counter = 0;

  return id;
} /* end groups_gen_gid () */

/*
 * Adds a group to the list of groups
 */
void groups_add (const gchar *name, WORD gid)
{
  GroupInfo *ginfo;

  if (groups_name_by_gid (gid))
    return; /* we don't want duplicate groups */ /* FIXME */

  ginfo = g_new0(GroupInfo, 1);

  ginfo->gid = gid;
  if (name)
    ginfo->name = g_strdup(name);
  else
    ginfo->name = g_strdup (_("ERROR"));

  Groups = g_slist_append(Groups, ginfo);
  gnomeicu_tree_group_add (ginfo);
}

/*
 * Removes a group from the list
 */
void groups_remove(WORD gid)
{
  GSList *ginfo;

  for (ginfo = Groups; ginfo != NULL; ginfo = ginfo->next)
    if (((GroupInfo *)ginfo->data)->gid == gid)
      break;
  if (ginfo == NULL)
    return;
  
  gnomeicu_tree_group_remove (ginfo->data);

  g_free(((GroupInfo *)ginfo->data)->name);

  g_free(ginfo->data);

  Groups = g_slist_delete_link(Groups, ginfo);
}

gchar *groups_name_by_gid(WORD gid)
{
  GSList *ginfo;

  if (gid == 0)
    return NULL;

  for (ginfo = Groups; ginfo != NULL; ginfo = ginfo->next)
    if (((GroupInfo *)ginfo->data)->gid == gid)
      return ((GroupInfo *)ginfo->data)->name;
  
  return NULL;
}

void groups_rename_by_gid (WORD gid, const gchar *name)
{
  GSList *ginfo;

  if (gid == 0)
    return;

  for (ginfo = Groups; ginfo != NULL; ginfo = ginfo->next)
    if (((GroupInfo *)ginfo->data)->gid == gid) {
      g_free (((GroupInfo *)ginfo->data)->name);
      ((GroupInfo *)ginfo->data)->name = g_strdup (name);
      break;
    }
  gnomeicu_tree_group_rename (ginfo->data);
}

guint groups_find_gid_by_name (const gchar *name)
{

  GSList *group;

  for (group = Groups; group != NULL; group = group->next)
    if (! strcmp(((GroupInfo *)group->data)->name, name))
      return ((GroupInfo *)group->data)->gid;
  
  return 0;


}
