#include "common.h"
#include "dragdrop.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "icons.h"
#include "listwindow.h"
#include "personal_info.h"
#include "util.h"
#include "v7send.h"
#include "showlist.h"
#include "sendcontact.h"

#include <gtk/gtk.h>
#include <glade/glade-xml.h>
#include <string.h>

static void remove_contact_list_candidate( GtkWidget *widget, gpointer data );
static void info_list_candidate( GtkWidget *widget, gpointer data );
static void ok_button_clicked( GtkWidget *widget, gpointer data);

static GladeXML *list_window_common ( const gchar *title,
						 const gchar *label_text );
static void remove_contact_list( GtkWidget *widget, gpointer data );
static void info_list( GtkWidget *widget, gpointer data );

static void drag_data_received (GtkWidget *widget, GdkDragContext *context,
				gint x, gint y, GtkSelectionData *data,
				guint info, guint time, gpointer user_data);
static void drag_data_received_candidate (GtkWidget *widget, GdkDragContext *context,
					  gint x, gint y, GtkSelectionData *data,
					  guint info, guint time, gpointer user_data);
static void response_cb(GtkWidget *widget, gint response, gpointer data);


enum { LIST_UIN_COLUMN,
       LIST_NICK_COLUMN,
       LIST_N_COLUMNS};



static void
list_window_expand_row(GtkTreeModel *model,
			 GtkTreePath *path,
			 GtkTreeIter *iter,
			 gpointer user_data)
{
  GtkWidget *treeview = user_data;

  gtk_tree_view_expand_row(GTK_TREE_VIEW(treeview), path, TRUE);

}

static void
list_count_children(GtkTreeModel *model,
			     GtkTreeIter *parent,
			     ContactFilterCallback is_in_list,
			     int *all, int *in) {
  g_assert(all);
  g_assert(in);
  GtkTreeIter child;
  Contact_Member *contact;
  
  *all = 0;
  *in = 0;
  
  if (!gtk_tree_model_iter_children(model, &child, parent))
    return;

  do {
    ++(*all);

    gtk_tree_model_get(model, &child,
		       CONTACT_COLUMN, &contact,
		       -1);
    if (contact && is_in_list(contact)) {
      ++(*in);
    }
    
  } while (gtk_tree_model_iter_next(model, &child));

}
static gboolean
list_filter_func (GtkTreeModel *model,
					GtkTreeIter *iter,
					gpointer data)
{
  Contact_Member *contact;
  GtkTreeIter parent;
  ContactFilterCallback is_in_list = data;
  int all, online;

  gtk_tree_model_get (model, iter, CONTACT_COLUMN, &contact, -1);

  if (contact) {
    return is_in_list(contact);

  } else { /* is group */
    if (!gtk_tree_model_iter_has_child(model, iter))
      return FALSE;

    list_count_children(model, iter, is_in_list, &all, &online);
    return online;
  }
}



GtkWidget* list_window_new_filter( const gchar *title,
				   const gchar *label_text,
				   ContactCallback add_contact,
				   ContactCallback remove_contact,
				   ContactFilterCallback is_in_list)
{
  GladeXML *dialogxml = NULL;

  GtkWidget *dialog = NULL;
  GtkWidget *treeview = NULL;
  GtkWidget *image = NULL;

  GtkCellRenderer *icon_renderer = NULL;
  GtkCellRenderer *text_renderer = NULL;
  GtkTreeModel *filtermodel = NULL;
  GtkTreeViewColumn *column = NULL;
  GtkWidget *remove_button = NULL;
  GtkWidget *info_button = NULL;

  dialogxml = list_window_common (title, label_text);
  if (!dialogxml)
    return NULL;

  dialog = glade_xml_get_widget(dialogxml, "list_dialog");
  treeview = glade_xml_get_widget(dialogxml, "treeview");
  image = glade_xml_get_widget(dialogxml, "info_image");
  remove_button = glade_xml_get_widget(dialogxml, "remove_button");
  info_button = glade_xml_get_widget(dialogxml, "infobutton");

	
  filtermodel = gtk_tree_model_filter_new(GTK_TREE_MODEL(MainData->contacts_store),
					  NULL);
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(filtermodel),
					 list_filter_func, is_in_list, NULL);
	
  gtk_tree_view_set_model (GTK_TREE_VIEW(treeview), filtermodel);


  g_signal_connect(G_OBJECT(filtermodel), "row-has-child-toggled",
		   G_CALLBACK(list_window_expand_row),
		   treeview);

  gtk_tree_view_expand_all(GTK_TREE_VIEW(treeview));


  icon_renderer = gtk_cell_renderer_pixbuf_new();
  text_renderer = gtk_cell_renderer_text_new();

  column = gtk_tree_view_column_new();
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeview), column);


  gnomeicu_tree_setup_cell_layout(GTK_CELL_LAYOUT(column));

  gtk_tree_view_set_expander_column (GTK_TREE_VIEW (treeview),
                                     column);

  gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(treeview), FALSE);

  g_object_set_data(G_OBJECT(treeview), "remove-row-func", remove_contact);

  g_signal_connect( G_OBJECT( remove_button ), "clicked",
		    G_CALLBACK( remove_contact_list ),
		    treeview );

  g_signal_connect( G_OBJECT( info_button ), "clicked",
		    G_CALLBACK( info_list ),
		    treeview );

  gtk_drag_dest_set( treeview, GTK_DEST_DEFAULT_ALL, &target_table[1], 1,
		     GDK_ACTION_COPY );
  g_signal_connect( G_OBJECT( treeview ), "drag-data-received",
		    G_CALLBACK( drag_data_received ), add_contact );

  return dialog;

}

GtkWidget* list_window_new( const gchar *title,
			    const gchar *label_text,
			    const gchar *button_name,
			    const gchar *button_pixmap,
			    ListCallback button_clicked,
			    gpointer user_data,
			    GList *contactpairs)
{
  GladeXML *dialogxml = NULL;
  GtkWidget *dialog = NULL;

  GtkListStore *liststore;
  GtkTreeIter iter;
  GtkTreeViewColumn *uincol;
  GtkTreeViewColumn *nickcol;
  GtkCellRenderer *uinrenderer;
  GtkCellRenderer *nickrenderer;

  GtkWidget *treeview;
  GtkWidget *ok_button;
  GtkWidget *close_button;
  GtkWidget *image;
  GtkWidget *remove_button;
  GtkWidget *info_button;

  GSList *contact = NULL;


  dialogxml = list_window_common (title, label_text);
  if (!dialogxml)
    return NULL;

  dialog = glade_xml_get_widget(dialogxml, "list_dialog");
  ok_button = glade_xml_get_widget(dialogxml, "ok_button");
  close_button = glade_xml_get_widget(dialogxml, "close_button");
  info_button = glade_xml_get_widget(dialogxml, "infobutton");
  remove_button = glade_xml_get_widget(dialogxml, "remove_button");
  treeview = glade_xml_get_widget(dialogxml, "treeview");


  gtk_button_set_label(GTK_BUTTON(ok_button), button_name);

  if (button_pixmap) {
    image = gtk_image_new_from_stock(button_pixmap, GTK_ICON_SIZE_BUTTON);
    gtk_button_set_image(GTK_BUTTON(ok_button), image);
  } else {
    gtk_button_set_use_stock(GTK_BUTTON(ok_button), TRUE);
  }
  gtk_widget_show(ok_button);

  gtk_button_set_label(GTK_BUTTON(close_button), GTK_STOCK_CANCEL);
  gtk_button_set_use_stock(GTK_BUTTON(close_button), TRUE);


  g_object_set_data(G_OBJECT(dialog), "clicked_func",
		    button_clicked);
  g_object_set_data(G_OBJECT(dialog), "clicked_func_data",
		    user_data);
  /*
  g_signal_connect (G_OBJECT (ok_button), "clicked",
		    G_CALLBACK(ok_button_clicked), treeview);

  */


  liststore = gtk_list_store_new(LIST_N_COLUMNS, G_TYPE_STRING,
				 G_TYPE_STRING,
				 G_TYPE_POINTER);


	
  gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), GTK_TREE_MODEL(liststore));


  uinrenderer = gtk_cell_renderer_text_new ();
  uincol = gtk_tree_view_column_new_with_attributes(_("UIN"),
						    uinrenderer,
						    "text", LIST_UIN_COLUMN,
						    NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), uincol);

  nickrenderer = gtk_cell_renderer_text_new ();
  nickcol = gtk_tree_view_column_new_with_attributes(_("Nickname"),
						     nickrenderer,
						     "text", LIST_NICK_COLUMN,
						     NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), nickcol);
  

  g_signal_connect( G_OBJECT( remove_button ), "clicked",
		    G_CALLBACK( remove_contact_list_candidate ),
		    treeview );

  g_signal_connect( G_OBJECT( info_button ), "clicked",
		    G_CALLBACK( info_list_candidate ),
		    treeview );


  if (contactpairs) {

    while (contactpairs != NULL) {
      
      gtk_list_store_append (liststore, &iter);
      gtk_list_store_set (liststore, &iter,
			  LIST_UIN_COLUMN, *((ContactPair*)(contactpairs->data))->textuin,
			  LIST_NICK_COLUMN, *((ContactPair*)(contactpairs->data))->nick,
			  -1);
      
       contactpairs = g_list_next(contactpairs);
    }


  } else {
    gtk_drag_dest_set( treeview, GTK_DEST_DEFAULT_ALL, &target_table[1], 1,
		       GDK_ACTION_COPY );
    g_signal_connect( G_OBJECT( treeview ), "drag-data-received",
		      G_CALLBACK( drag_data_received_candidate ), liststore );

  }

  return dialog;

}

static GladeXML *list_window_common ( const gchar *title,
				      const gchar *label_text )
{

  GladeXML *dialogxml = NULL;
  GtkWidget *dialog = NULL;
  GtkWidget *label = NULL;
  GtkWidget *treeview;


  dialogxml = gicu_util_open_glade_xml("listwindow.glade",
				       "list_dialog");
  
  if (dialogxml == NULL) {
    return NULL;
  }

  dialog = glade_xml_get_widget(dialogxml, "list_dialog");
  label = glade_xml_get_widget(dialogxml, "list_label");
  treeview = glade_xml_get_widget(dialogxml, "treeview");
	

  gtk_window_set_title (GTK_WINDOW (dialog), title);
  gtk_label_set_text(GTK_LABEL(label), label_text);


  gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);


  g_signal_connect (G_OBJECT (dialog), "delete_event",
		    G_CALLBACK (gtk_widget_destroy), NULL);

  g_signal_connect (G_OBJECT (dialog), "response",
		    G_CALLBACK(response_cb), treeview);


	
  return dialogxml;
}

static void response_cb(GtkWidget *widget, gint response, gpointer data)
{
  ListCallback button_clicked;
  GtkWidget *treeview;
  GtkTreeIter iter;
  GtkTreeModel *model;
  GtkTreeSelection *selection;
  GSList *contact;
  GList *contactpairs = NULL;
  ContactPair *cpair;
  gpointer mydata = NULL;
  int valid = 1;

  printf("response %d\n", response);
  switch(response) {
  case GTK_RESPONSE_OK:
    treeview = GTK_WIDGET( data );
    if( treeview == NULL || !GTK_IS_TREE_VIEW(treeview))
      return;
  
  
    model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));

    button_clicked = g_object_get_data(G_OBJECT(widget), "clicked_func");
    if (!button_clicked)
      break;
    mydata = g_object_get_data(G_OBJECT(widget), "clicked_func_data");


    for ( valid = gtk_tree_model_get_iter_first(model, &iter);
	  valid;
	  valid = gtk_tree_model_iter_next(model, &iter)) {

      cpair = g_new0(ContactPair, 1);

      gtk_tree_model_get (model, &iter,
			  LIST_UIN_COLUMN, &cpair->textuin,
			  LIST_NICK_COLUMN, &cpair->nick,
			  -1);
      contactpairs = g_list_append(contactpairs, cpair);
    }

    button_clicked(contactpairs, mydata);

    while (contactpairs != NULL) {
      cpair = contactpairs->data;
      g_free(cpair->textuin);
      g_free(cpair->nick);
      g_free(cpair);
      contactpairs = g_list_remove(contactpairs, cpair);
    }

    break;
  case GTK_RESPONSE_CLOSE:
    break;
  }
    gtk_widget_destroy(widget);

}

void remove_contact_list_candidate( GtkWidget *widget, gpointer data )
{
  GtkWidget *treeview;
  gchar *text;
  GtkTreeIter iter;
  GtkTreeModel *model;
  GtkTreeSelection *selection;

  treeview = GTK_WIDGET( data );
  if( treeview == NULL || !GTK_IS_TREE_VIEW(treeview))
    return;
	
  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));

  if (!gtk_tree_selection_get_selected (selection, &model, &iter))
    return;
	
  gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
}

void info_list_candidate( GtkWidget *widget, gpointer data )
{
  GtkWidget *treeview;
  GtkTreeIter iter;
  GtkTreeModel *model;
  GtkTreeSelection *selection;
  UIN_T uin;
  gchar *text;
  GSList *contact;

  if (!is_connected(GTK_WINDOW(g_object_get_data (G_OBJECT (widget), "dlg")), _("You can not request user information while disconnected.")))
    return;

  treeview = GTK_WIDGET( data );
  if( treeview == NULL || !GTK_IS_TREE_VIEW(treeview))
    return;

  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
	
  if (!gtk_tree_selection_get_selected (selection, &model, &iter))
    return;

  gtk_tree_model_get (model, &iter, LIST_UIN_COLUMN, &uin, -1);


  contact = Find_User( uin );
  if( contact == NULL )
    return;

  v7_request_info (mainconnection, uin);
  dump_personal_info (uin);
  g_free (uin);
}




void remove_contact_list( GtkWidget *widget, gpointer data )
{
  GtkWidget *treeview;
  gchar *text;
  GtkTreeIter iter;
  GtkTreeModel *model;
  GtkTreeSelection *selection;
  Contact_Member *contact;
  ContactCallback callback = NULL;

  treeview = GTK_WIDGET( data );
  if( treeview == NULL || !GTK_IS_TREE_VIEW(treeview))
    return;

  callback = g_object_get_data(G_OBJECT(treeview), "remove-row-func");
  
  if(!callback)
    return;
	
  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));

  if (!gtk_tree_selection_get_selected (selection, &model, &iter))
    return;

  gtk_tree_model_get (model, &iter, CONTACT_COLUMN, &contact, -1);

  if (!contact)
    return;

  callback(contact);

}


void info_list( GtkWidget *widget, gpointer data )
{
  GtkWidget *treeview;
  GtkTreeIter iter;
  GtkTreeModel *model;
  GtkTreeSelection *selection;
  Contact_Member *contact;

  if (!is_connected(GTK_WINDOW(g_object_get_data (G_OBJECT (widget), "dlg")), _("You can not request user information while disconnected.")))
    return;

  treeview = GTK_WIDGET( data );
  if( treeview == NULL || !GTK_IS_TREE_VIEW(treeview))
    return;

  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
	
  if (!gtk_tree_selection_get_selected (selection, &model, &iter))
    return;

  gtk_tree_model_get (model, &iter, CONTACT_COLUMN, &contact, -1);

  v7_request_info (mainconnection, contact->uin);
  dump_personal_info (contact->uin);
}


void drag_data_received (GtkWidget *widget, GdkDragContext *context,
			 gint x, gint y, GtkSelectionData *data,
			 guint info, guint time, gpointer user_data)
{
	Contact_Member *contact = NULL;
	ContactCallback callback = NULL;

	if( data == NULL || data->data == NULL )
		return;

	callback = user_data;;

	if (info == DND_TARGET_CONTACT &&
	    data->length == sizeof(void*)) {
	  contact = *((Contact_Member**)data->data);

	  callback(contact);
	  
	  gtk_drag_finish (context, TRUE, FALSE, time);
	} else {
	  gtk_drag_finish (context, FALSE, FALSE, time);
	
	}

	g_signal_stop_emission_by_name(G_OBJECT(widget), "drag-data-received");
}


void drag_data_received_candidate (GtkWidget *widget, GdkDragContext *context,
				   gint x, gint y, GtkSelectionData *data,
				   guint info, guint time, gpointer user_data)
{
	Contact_Member *contact = NULL;
	GtkListStore *liststore = user_data;
	GtkTreeIter iter;

	if( data == NULL || data->data == NULL )
		return;

	if (info == DND_TARGET_CONTACT &&
	    data->length == sizeof(void*)) {
	  contact = *((Contact_Member**)data->data);

	  
	  gtk_list_store_prepend (liststore, &iter);
	  gtk_list_store_set (liststore, &iter,
			      LIST_UIN_COLUMN, contact->uin,
			      LIST_NICK_COLUMN, contact->nick,
			      -1);

	  
	  gtk_drag_finish (context, TRUE, FALSE, time);
	} else {
	  gtk_drag_finish (context, FALSE, FALSE, time);
	
	}

	g_signal_stop_emission_by_name(G_OBJECT(widget), "drag-data-received");
}
