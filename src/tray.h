#ifndef __TRAY_H__
#define __TRAY_H__

//#if GTK_CHECK_VERSION(2,10,0)

//#endif

void tray_init (void);
void tray_update (void);
gboolean tray_exists (void);



#endif /* __TRAY_H__ */
