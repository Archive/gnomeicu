/* GnomeICU
 * Copyright (C) 1998-2004 Jeremy Wise, Olivier Crete
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GNOMEICU_H__
#define __GNOMEICU_H__

#include "common.h"

#include <gtk/gtktreestore.h>

typedef struct
{
  GladeXML *xml;
  GtkWidget *notebook;
  GtkWidget *head_online;
  GtkWidget *head_offline;
  GtkWidget *head_notinlist;
  GtkWidget *not_contents;
  GtkWidget *window;
  GtkTreeStore *contacts_store;
  GtkWidget *contacts_tree;
  GtkWidget *all_tree;
  gboolean hidden;
  gint x, y;
} _MainData;

extern _MainData *MainData;

extern GSList *Contacts;

extern DWORD our_ip;
extern DWORD our_port;
extern DWORD Current_Status;
extern gchar *passwd;
extern gchar *Away_Message;
extern gboolean Done_Login;

extern _programs *programs;

extern gboolean search_in_progress;

extern gboolean is_new_user;
extern gboolean connection_alive;

extern gchar *configfilename;

extern gint tcp_gdk_input;

extern USER_INFO_PTR our_info;

extern GtkWidget *app;

extern gboolean enable_online_events;

extern gboolean webpresence;

extern int preset_status;

extern _toggles *toggles;

/* for server side contacts list */
extern guint16 status_uid;       /* for change status on server list */
extern gboolean sane_cl;         /* is the contact list sane? or checked? */
extern gboolean srvlist_exist;   /* server side list exist for this user? */
extern guint32 list_time_stamp;  /* time stamp of the list */
extern guint16 record_cnt;       /* record count */

extern GSList *Groups;

void create_tcp_line (void);
void gnomeicu_set_status_button (gboolean flash);
void gnomeicu_done_login (void);
void gnomeicu_about (GtkWidget *widget, gpointer data);
void go_url_home (void);
void go_url_updates (void);
void icq_quit (GtkWidget *widget, gpointer data);
void show_help (void);

#endif /* __GNOMEICU_H__ */
