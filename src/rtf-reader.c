/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * RTF message parsing. Windows ICQ sends such messages
 * First created by Fr�d�ric Riss (2002)
 */

#include "gnomeicu.h"
#include "msg.h"
#include "rtf-reader.h"
#include "util.h"

#include <string.h>
#include <stdlib.h>
#include <ctype.h>

static int minimum_font_size = 12;

/***********************
 * RTF data structures *
 ***********************/

typedef enum { CHARSET_UNKNOWN, 
	       CHARSET_ANSI, CHARSET_MAC, CHARSET_PC, CHARSET_PCA } RTFCharset;

typedef struct _RTFAttributes {
    /* Colors as rank in the color table */
    gint foreground;
    gint background;

    /* Font as rank in the font table */
    gint font;

    /* Font attibutes */
    gint size;
    gboolean italic;
    gboolean bold;
    gboolean striked;
    gboolean underlined;

    GtkJustification justification;
    GtkTextDirection direction;

    gint unicode_skip;

    /* Text waiting for insertion */
    GString* text;
} RTFAttributes;

typedef struct _RTFParserContext {

    /* Header information */
    RTFCharset charset;
    gint codepage;
    gint default_codepage;
    gint default_font;
    gint default_language;
    
    gint font_index;

    /* Attributes stack management */
    gint group_nesting_level;
    GSList* attributes_stack;
    gboolean header_finished;

    /* Color and font tables */
    GSList* color_table;
    GSList* font_table;

    /* Text information */
    gchar* rtf;
    gchar* current_pos;
    GString* plain_text;

    /* Output references */
    GtkTextBuffer* text_buffer;
    GtkTextTagTable* tags;

} RTFParserContext;

typedef struct _RTFFontProperties {
    gint index;
    gint charset;
} RTFFontProperties;

struct codepage_to_locale {
    int codepage;
    gchar* locale;
};

struct codepage_to_locale ansicpgs[] = {
    { 943, "SJIS" },
    { 950, "BIG5" },
    { 709, "ASMO_449" },
    { 0, NULL }
};

/*******************************
 * Static functions prototypes *
 *******************************/

static void
rtf_parse(RTFParserContext* parser_context, gchar* rtf_text);

static RTFParserContext*
rtf_parser_context_init(gchar* rtf_text, GtkTextBuffer* text_buffer);

static void
rtf_parser_context_free(  RTFParserContext* parser_context );

static void
rtf_warning(RTFParserContext* parser_context, gchar* text);

static gboolean 
rtf_get_int_parameter(RTFParserContext* parser_context, gint* value);

static void
rtf_parse_font_table(RTFParserContext* parser_context);

static void
rtf_parse_font_table_control_word(RTFParserContext* parser_context);

static void
rtf_parse_charset(RTFParserContext* parser_context, gint charset);

static void
rtf_set_font_charset(RTFParserContext* parser_context, gint font_index, gint charset);

static void
rtf_set_font_attributes(RTFParserContext* parser_context, gint font_index);

static void
rtf_parse_lang(RTFParserContext* parser_context, gint lang);

static void
rtf_commit_pending_text(RTFParserContext* parser_context);

static void
rtf_parse_color_table(RTFParserContext* parser_context);

static void
rtf_parse_control_word(RTFParserContext* parser_context);

static void 
rtf_pop_state(RTFParserContext* parser_context);

static void 
rtf_push_state(RTFParserContext* parser_context);

static void
rtf_parse(RTFParserContext* parser_context, gchar* rtf_text);

/**********************
 * Exported Functions *
 **********************/

gchar*
rtf_parse_and_insert(gchar* rtf_text, GtkTextBuffer* text_buffer)
{

    RTFParserContext* parser_context;
    gchar* plain;
    
    parser_context = rtf_parser_context_init(rtf_text, text_buffer);
    
    if (text_buffer)
      minimum_font_size = pango_font_description_get_size(gtk_widget_get_style(MainData->window)->font_desc) / PANGO_SCALE;

    rtf_parse(parser_context, rtf_text);

    plain = parser_context->plain_text->str;

    rtf_parser_context_free(parser_context);

    return plain;
}


/*******************
 * Local functions *
 *******************/

/* 
 * Creates and inits a RTFParserContext structure with default values.
 * The attributes stack has one element with default text attributes values
 */

static RTFParserContext*
rtf_parser_context_init(gchar* rtf_text, GtkTextBuffer* text_buffer)
{
    RTFParserContext* context;
    RTFAttributes* base_attributes;

    g_assert(rtf_text != NULL);

    context = g_new(RTFParserContext, 1);
    base_attributes = g_new(RTFAttributes, 1);

    context->charset = CHARSET_UNKNOWN;
    context->codepage = -1;
    context->default_codepage = -1;
    context->font_index = 0;
    context->group_nesting_level = 0;
    context->attributes_stack = NULL;
    context->color_table = NULL;
    context->font_table = NULL;
    context->plain_text = g_string_new("");
    
    context->header_finished = FALSE;
    context->rtf = rtf_text;
    context->current_pos = rtf_text;

    context->text_buffer = text_buffer;

    if (text_buffer)
      context->tags = gtk_text_buffer_get_tag_table( text_buffer );

    base_attributes->background = -1;
    base_attributes->foreground = -1;
    base_attributes->font = -1;
    base_attributes->size = -1;
    base_attributes->italic = FALSE;
    base_attributes->bold = FALSE;
    base_attributes->striked = FALSE;
    base_attributes->underlined = FALSE;
    base_attributes->justification = -1;
    base_attributes->direction = -1;
    base_attributes->unicode_skip = 1;
    base_attributes->text = g_string_new("");

    context->attributes_stack = g_slist_prepend(context->attributes_stack, 
						base_attributes);

    return context;
}


/*
 * Try to create a GIconv converter for the specified codepage
 */

gchar *
get_charset_for_codepage(int codepage)
{
    GIConv converter = (GIConv) -1;
    gchar* locale;
    int i=0;

    if (codepage == -1)
      return g_strdup(preferences_get_string(PREFS_GNOMEICU_CHARSET_FALLBACK));

    /* First try the "CP<cpge>" locale */
    locale = g_strdup_printf("CP%i", codepage);
    converter = g_iconv_open("UTF-8", locale);

    if (converter != (GIConv) -1) {
      g_iconv_close(converter);
      return locale;
    }

    g_free(locale);

    /* If there is no such converter, try the hard-coded table */
    while (ansicpgs[i].codepage != 0) {
      if (ansicpgs[i].codepage == codepage) {
	converter = g_iconv_open("UTF-8", ansicpgs[i].locale);
	if (converter != (GIConv) -1) {
	  g_iconv_close(converter);
	  return g_strdup(ansicpgs[i].locale);
	}
      }
      i++;
    }
    return g_strdup(preferences_get_string(PREFS_GNOMEICU_CHARSET_FALLBACK));
}

/*
 * Frees all the data referenced by a RTFParserContext 
 */

static void
rtf_parser_context_free(  RTFParserContext* parser_context ) {
    GSList* current;
    g_assert(parser_context != NULL);
    g_string_free(parser_context->plain_text, FALSE);

    current =  parser_context->color_table;
    while (current) {
	g_free(current->data);
	current = current->next;
    }
    g_slist_free(parser_context->color_table);

    current =  parser_context->font_table;
    while (current) {
	g_free(current->data);
	current = current->next;
    }
    g_slist_free(parser_context->font_table);

    current =  parser_context->attributes_stack;
    while (current) {
	RTFAttributes* attributes 
	    = (RTFAttributes*) current->data;
	g_string_free(attributes->text, TRUE);
	g_free(attributes);
	current = current->next;
    }
    g_slist_free(parser_context->attributes_stack);

    g_free(parser_context);

}

/*
 * Prints a warning containing the RTF data and the context
 */
static void
rtf_warning(RTFParserContext* parser_context, gchar* text)
{
    g_assert(parser_context != NULL);
    g_warning("Error in RTF message parsing : %s.\nPlease report this "
	      "to http://bugzilla.gnome.org/enter_bug.cgi?product=GnomeICU "
	      "with the following log of the RTF encoded message : \n%s\n"
	      "The current position was : %s\n",
	      text,
	      parser_context->rtf,
	      parser_context->current_pos);

}

/*
 * Reads an integer at the current position. If there's no integer at that
 * position the function returns FALSE, otherwise TRUE. The value is stored 
 * in the location pointed by the 'value' parameter.
 */
static gboolean 
rtf_get_int_parameter(RTFParserContext* parser_context, gint* value)
{
    gchar* end_ptr;
    gboolean result;

    g_assert(parser_context != NULL && value != NULL);
    /* FIXME : add errno checking */
    *value = (gint) strtol(parser_context->current_pos, &end_ptr, 10);

    if (parser_context->current_pos != end_ptr)
	result = TRUE;
    else
	result = FALSE;

    parser_context->current_pos = end_ptr;
    return result;
}


/*
 * Should parse the font table... does nothing at the moment
 */
static void
rtf_parse_font_table(RTFParserContext* parser_context)
{
    gint nesting = 0;

    g_assert(parser_context != NULL);
    /* FIXME : do something with the font table */

    nesting = parser_context->group_nesting_level;

    do {
        if (*parser_context->current_pos == '{') {
            parser_context->current_pos++;
            rtf_push_state(parser_context);
        } else if (*parser_context->current_pos == '}') {
            parser_context->current_pos++;
            rtf_pop_state(parser_context);
        } else if (*parser_context->current_pos == '\\') {
            rtf_parse_font_table_control_word(parser_context);
        } else if (*parser_context->current_pos == '\r') {
            parser_context->current_pos++;
        } else {
            parser_context->current_pos++;
        }

    } while (parser_context->group_nesting_level >= nesting);
}

/*
 * Parses control words in a font table context
 */
static void
rtf_parse_font_table_control_word(RTFParserContext* parser_context)
{
    gint param;
    gchar* word;
    RTFAttributes* attributes;

    g_assert( parser_context != NULL && *(parser_context->current_pos) == '\\' );
    attributes = (RTFAttributes*) parser_context->attributes_stack->data;

    word = ++(parser_context->current_pos);

    if (! strncmp("fcharset", word, 8) ) {
        parser_context->current_pos += 8;
        if (! rtf_get_int_parameter(parser_context, &param))
            rtf_warning(parser_context,
        		"'\\fcharset' control word without parameter.");
        rtf_parse_charset(parser_context, param); 
    } else if (! strncmp("f", word, 1) ) {
      parser_context->current_pos += 1;
      if (! rtf_get_int_parameter(parser_context, &param))
	  if (isalpha(parser_context->current_pos[0]))
	    while (isalpha(parser_context->current_pos[0]))
	      parser_context->current_pos += 1;
	  else
	    rtf_warning(parser_context,
			"'\\f' control word without parameter.");
      parser_context->font_index = param;
    }

    if (*parser_context->current_pos == ' ')
      parser_context->current_pos++;
}

/*
 * Returns properties for font_index or NULL if such font does not exist
 */
static RTFFontProperties *
rtf_get_font_properties(GSList *font_table, gint font_index)
{
    gint i;
    RTFFontProperties *properties;

    for (i = 0; i < g_slist_length(font_table); i++) {
        properties = g_slist_nth_data(font_table, i);
        if (properties != NULL && properties->index == font_index)
            return properties;
    }

    return NULL;
}

static void
rtf_set_font_charset(RTFParserContext* parser_context, gint font_index, gint charset)
{
    RTFFontProperties *font_properties = g_new(RTFFontProperties, 1);
    font_properties->index = font_index;
    font_properties->charset = charset;
    parser_context->font_table = g_slist_append(parser_context->font_table, font_properties);
}

static void
rtf_set_font_attributes(RTFParserContext* parser_context, gint font_index)
{
    RTFFontProperties *font_properties = rtf_get_font_properties(parser_context->font_table,
                                                                 font_index);
    if (font_properties)
        parser_context->codepage = font_properties->charset;
}

/*
 * Parses the charset
 */
static void
rtf_parse_charset(RTFParserContext* parser_context, gint charset)
{
    gint nesting = 0;
    char warning[256];

    g_assert(parser_context != NULL);
    /* FIXME : do something with the font table */

    switch (charset)
    {
    case 0:
    case 1:
        rtf_set_font_charset(parser_context, parser_context->font_index, parser_context->default_codepage);
        return;
    case 128: /* ShiftJIS */
        parser_context->codepage = 943;
        break;
    case 130: /* Johab */
        parser_context->codepage = 1361;
        break;
    case 134: /* GB2312 */
      parser_context->codepage = 936;
      break;
    case 161: /* Greek */
        parser_context->codepage = 1253;
        break;
    case 162: /* Turkish */
        parser_context->codepage = 1254;
        break;
    case 163: /* Vietnamese */
        parser_context->codepage = 1258;
        break;
    case 177: /* Hebrew */
        parser_context->codepage = 1255;
        break;
    case 178: /* Arabic */
    case 179: /* Arabic Traditional */
    case 180: /* Arabic user */
        /*
         * FIXME: which one?
         * 864 Arabic
         * 708 Arabic (ASMO 708)
         * 709 Arabic (ASMO 449+, BCON V4)
         * 710 Arabic (transparent Arabic)
         * 711 Arabic (Nafitha Enhanced)
         * 720 Arabic (transparent ASMO)
         * 1256 Arabic
         */
        snprintf(warning, 256, "charset %d not supported", charset);
        rtf_warning(parser_context, warning);
        break;
    case 181: /* Hebrew user */
        parser_context->codepage = 862;
        break;
    case 186: /* Baltic */
        parser_context->codepage = 1257;
        break;
    case 204: /* Russian */
        parser_context->codepage = 1251;
        break;
    case 222: /* Thai */
        parser_context->codepage = 874;
        break;
    case 238: /* Eastern European */
        parser_context->codepage = 852;
        /* ?? 1250 Windows 3.1 (Eastern European) */
        break;
    case 254: /* PC 437 */
        parser_context->codepage = 437;
        break;
    case 255: /* OEM */
        /* FIXME! */
        /* ...
         * Other charsets should be recognized here
         * ...
         */
         /*
          * 819 Windows 3.1 (United States and Western Europe)
          * 850 IBM multilingual
          * 860 Portuguese
          * 863 French Canadian
          * 865 Norwegian
          * 866 Soviet Union
          * 1252 Western European
          */
    default:
        snprintf(warning, 256, "unknown charset: %d", charset);
        rtf_warning(parser_context, warning);
        break;
    }

    rtf_set_font_charset(parser_context, parser_context->font_index, parser_context->codepage);

    /* We don't parse the rest of this statement yet */

    while (nesting>=0) {
	if (*parser_context->current_pos == '{') 
	    nesting++;
	if (*parser_context->current_pos == '}')
	    nesting--;

	parser_context->current_pos++;
    }
    parser_context->current_pos--;
}

static void
rtf_parse_lang(RTFParserContext* parser_context, gint lang)
{
    gint nesting = 0;

    g_assert(parser_context != NULL);

    switch (lang)
    {
    case 9:
      /* A user said the person sending with lang=9 was using American WinXP */
    case 1033: /* English (US) */
    case 1036: /* French (France) */
    case 3084: /* French (Canada) */
    case 4105: /* English (Canada) */
    case 1031: /* German ? */
        parser_context->codepage = 1252;
        break;
    case 1049: /* Russian */
        parser_context->codepage = 1251;
        break;
    case 136:  /* Big5 */
    case 1028: /* Chinese (Taiwan) */
    case 3076: /* Chinese (Hong Kong) */
        parser_context->codepage = 950;
        break;
    case 1041: /* Japanese */
        parser_context->codepage = 943;
        break;
    case 1037: /* Hebrew */
        parser_context->codepage = 1255;
        break;
        /* ...
         * Other languages should be recognized here
         * ...
         */
    default:
        rtf_warning(parser_context, "unknown lang, please report the sender's locale and charset");
        break;
    }
}


/*
 * Inserts the pending text with the current attributes. This function is 
 * called  whenever a change occurs in the attributes.
 */
static void
rtf_commit_pending_text(RTFParserContext* parser_context)
{
    GtkTextIter text_begin;
    GtkTextIter text_end;
    GtkTextMark* insert_mark;
    RTFAttributes* attributes;
    int length;
    gchar* text;
    gchar* converted_text = NULL;
    GError *converterror = NULL;

    g_assert(parser_context != NULL);

    attributes = (RTFAttributes*) parser_context->attributes_stack->data;
    text = attributes->text->str;

    if (text[0] == '\0') return;

    length = strlen(text) - 1;
    if (! parser_context->group_nesting_level 
	&& text[length] == '\n')
	text[length] = '\0';


    if (parser_context->codepage == 1200) {
      if (g_utf8_validate(text, -1, NULL))
	converted_text = g_strdup(text);
      else {
	g_warning(_("Claims to be unicode but is not, trying current locale "
		  "instead\n"));
	converted_text = convert_to_utf8(text);
      }
    }
    else {
      gchar *charset;

      charset = get_charset_for_codepage(parser_context->codepage);
      converted_text = g_convert_with_fallback(text, strlen(text)+1, 
						 "UTF-8", charset, "?", 
					       NULL, NULL, &converterror);
      g_free(charset);
      if (converterror)
	g_warning("Conversion error: %s\n", converterror->message);
      g_clear_error(&converterror);
    }

    g_string_append(parser_context->plain_text, converted_text);

    if (parser_context->text_buffer) {

      insert_mark = gtk_text_buffer_get_mark(parser_context->text_buffer, 
					     "insert_mark");
      gtk_text_buffer_get_end_iter(parser_context->text_buffer, &text_begin);
      gtk_text_buffer_move_mark(parser_context->text_buffer, 
				insert_mark, &text_begin);

      gnomeicu_text_buffer_insert_with_emoticons (parser_context->text_buffer,
						  insert_mark, converted_text);
      gtk_text_buffer_get_iter_at_mark(parser_context->text_buffer, 
				       &text_begin, insert_mark);
      gtk_text_buffer_get_end_iter(parser_context->text_buffer, &text_end);

      if (attributes->foreground != -1) {
	gchar* color = g_slist_nth(parser_context->color_table,
				   attributes->foreground)->data;
	gchar* tag_name = g_strdup_printf("f%s", color);
	gtk_text_buffer_apply_tag_by_name( parser_context->text_buffer,
					   tag_name, &text_begin, &text_end );
	g_free( tag_name );
      }

      if (attributes->background != -1) {

	if (attributes->background == 0)
	  gtk_text_buffer_apply_tag_by_name( parser_context->text_buffer,
					     "bnone", &text_begin, 
					     &text_end );
	else {
	  gchar* color = g_slist_nth(parser_context->color_table,
				     attributes->background)->data;
	  gchar* tag_name = g_strdup_printf("b%s", color);
	  gtk_text_buffer_apply_tag_by_name( parser_context->text_buffer,
					     tag_name, &text_begin, 
					     &text_end );
	  g_free( tag_name );
	}
      }

      if (attributes->size != -1 && attributes->size>minimum_font_size) {
	gchar* tag_name = g_strdup_printf("fs%i", attributes->size);
	gtk_text_buffer_apply_tag_by_name( parser_context->text_buffer,
					   tag_name, &text_begin, &text_end );
	g_free( tag_name );
      }
    
      if (attributes->italic) {
	gtk_text_buffer_apply_tag_by_name( parser_context->text_buffer,
					   "italic" , &text_begin, &text_end );
      }
    
      if (attributes->bold) {
	gtk_text_buffer_apply_tag_by_name( parser_context->text_buffer,
					   "bold" , &text_begin, &text_end );
      }

      if (attributes->striked) {
	gtk_text_buffer_apply_tag_by_name( parser_context->text_buffer,
					   "striked", &text_begin, &text_end );
      }

      if (attributes->underlined) {
	gtk_text_buffer_apply_tag_by_name( parser_context->text_buffer,
					   "underlined", 
					   &text_begin, &text_end );
      }

      if (attributes->justification != -1) {
	/* FIXME: We should apply the tags here */
      }

      if (attributes->direction != -1) {
	if (attributes->direction == GTK_TEXT_DIR_RTL)
	  gtk_text_buffer_apply_tag_by_name( parser_context->text_buffer,
					     "right-to-left", 
					     &text_begin, &text_end );
	else
	  gtk_text_buffer_apply_tag_by_name( parser_context->text_buffer,
					     "left-to-right", 
					     &text_begin, &text_end );	
      }

    }
    g_free(converted_text);
    g_string_truncate(attributes->text, 0);
    
}

/*
 * Parses the color table
 */
static void
rtf_parse_color_table(RTFParserContext* parser_context)
{
    gint red = 0;
    gint green = 0;
    gint blue = 0;

    g_assert(parser_context != NULL);
    
    while (*parser_context->current_pos != '}') {
      if (*parser_context->current_pos == ';') {
	  if (parser_context->text_buffer) {
	    gchar *color, *foreground, *background, *color_name;
	    color = g_strdup_printf("%02x%02x%02x", red, green, blue);
	    foreground = g_strdup_printf("f%s", color);
	    background = g_strdup_printf("b%s", color);
	    color_name = g_strdup_printf("#%s", color);
	    parser_context->color_table
	      = g_slist_append(parser_context->color_table, color);
	    if(!gtk_text_tag_table_lookup(parser_context->tags,foreground)) {
	      GtkTextTag* tag;
	      tag = gtk_text_tag_new( foreground );
		g_object_set( tag, "foreground", color_name, NULL);
		gtk_text_tag_table_add( parser_context->tags, tag );
		tag = gtk_text_tag_new( background );
		g_object_set( tag, "background", color_name, 
			      "background-set", TRUE, NULL);
		gtk_text_tag_table_add( parser_context->tags, tag );
	    }
	    g_free(foreground);
	    g_free(background);
	  }
	  red = green = blue = 0;
	  parser_context->current_pos++;
	} else if (! strncmp("\\red", parser_context->current_pos, 4)) {
	    parser_context->current_pos+=4;
	    if (! rtf_get_int_parameter(parser_context, &red))
		rtf_warning(parser_context, 
			    "'\\red' control word without parameter.");
	}  else if (! strncmp("\\green", parser_context->current_pos, 6)) {
	    parser_context->current_pos+=6;
	    if (! rtf_get_int_parameter(parser_context, &green))
		rtf_warning(parser_context, 
			    "'\\green' control word without parameter.");
	}  else if (! strncmp("\\blue", parser_context->current_pos, 5)) {
	    parser_context->current_pos+=5;
	    if (! rtf_get_int_parameter(parser_context, &blue))
		rtf_warning(parser_context, 
			    "'\\blue' control word without parameter.");
	} else 
	    parser_context->current_pos++;
    }
}

/* 
 * Executes the action associated to the control word beginning at the
 * current parsing position
 */
static void
rtf_parse_control_word(RTFParserContext* parser_context)
{
    gint param;
    gchar* word;
    RTFAttributes* attributes;
    gboolean dontskip = FALSE;

    g_assert( parser_context != NULL && *(parser_context->current_pos) == '\\' );
    attributes = (RTFAttributes*) parser_context->attributes_stack->data;

    word = ++(parser_context->current_pos);
    
    if (! strncmp("rtf", word, 3) ) {
	parser_context->current_pos += 3;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\rtf' control word without parameter.");
	else if (param != 1)
	    rtf_warning(parser_context, "Unknown rtf version.");
    } else if (! strncmp("ansicpg", word, 7) ) {
	parser_context->current_pos += 7;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\ansicpg' control word without parameter.");
	parser_context->codepage = param;
        parser_context->default_codepage = param;
    } else if (! strncmp("lang", word, 4) ) {
	parser_context->current_pos += 4;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context,
			"'\\lang' control word without parameter.");
        rtf_commit_pending_text( parser_context );
        rtf_parse_lang(parser_context, param);
    } else if (! strncmp("ansi", word, 4) ) {
	parser_context->current_pos += 4;
	parser_context->charset = CHARSET_ANSI;
    } else if (! strncmp("mac", word, 3) ) {
	parser_context->current_pos += 3;
	parser_context->charset = CHARSET_MAC;
	rtf_warning(parser_context, "MAC charset not supported.");
    } else if (! strncmp("pca", word, 3) ) {
	parser_context->current_pos += 3;
	parser_context->charset = CHARSET_PCA;
	rtf_warning(parser_context, "PCA charset not supported.");
    } else if (! strncmp("pc", word, 2) ) {
	parser_context->current_pos += 2;
	parser_context->charset = CHARSET_PC;
	rtf_warning(parser_context, "PC charset not supported.");
    } else if (! strncmp("deff", word, 4) ) {
	parser_context->current_pos += 4;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\deff' control word without parameter.");
	parser_context->default_font = param;
    } else if (! strncmp("fbidis", word, 6) ) {
	parser_context->current_pos += 6;
    } else if (! strncmp("rtlpar", word, 6) ) {
	parser_context->current_pos += 6;
	rtf_commit_pending_text( parser_context );
	attributes->direction = GTK_TEXT_DIR_RTL;
    } else if (! strncmp("ltrpar", word, 6) ) {
	parser_context->current_pos += 6;
	rtf_commit_pending_text( parser_context );
	attributes->direction = GTK_TEXT_DIR_LTR;
   } else if (! strncmp("rtlch", word, 5) ) {
	parser_context->current_pos += 5;
	rtf_commit_pending_text( parser_context );
	attributes->direction = GTK_TEXT_DIR_RTL;
    } else if (! strncmp("ltrch", word, 5) ) {
	parser_context->current_pos += 5;
 	rtf_commit_pending_text( parser_context );
	attributes->direction = GTK_TEXT_DIR_LTR;
    } else if (! strncmp("qr", word, 2) ) {
	parser_context->current_pos += 2;
	rtf_commit_pending_text( parser_context );
	attributes->justification = GTK_JUSTIFY_RIGHT;
    } else if (! strncmp("ql", word, 2) ) {
	parser_context->current_pos += 2;
	rtf_commit_pending_text( parser_context );
	attributes->justification = GTK_JUSTIFY_LEFT;
    } else if (! strncmp("qc", word, 2) ) {
	parser_context->current_pos += 2;
	rtf_commit_pending_text( parser_context );
	attributes->justification = GTK_JUSTIFY_CENTER;
    } else if (! strncmp("qj", word, 2) ) {
	parser_context->current_pos += 2;
	rtf_commit_pending_text( parser_context );
	attributes->justification = GTK_JUSTIFY_FILL;
    } else if (! strncmp("deflangfe", word, 9) ) {
	parser_context->current_pos += 9;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\defflangfe' control word without parameter.");
	/* FIXME : What must we do with Far Eastern langages ? */
    } else if (! strncmp("deflang", word, 7) ) {
	parser_context->current_pos += 7;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\defflang' control word without parameter.");
    } else if (! strncmp("sb", word, 2) ) {
	parser_context->current_pos += 2;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\sb' control word without parameter.");
	/* ignore line spacing */
    } else if (! strncmp("sa", word, 2) ) {
	parser_context->current_pos += 2;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\sa' control word without parameter.");
	/* ignore line spacing */
    } else if (! strncmp("lang", word, 4) ) {
	parser_context->current_pos += 4;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\lang' control word without parameter.");
    } else if (! strncmp("fonttbl", word, 7) ) {
	parser_context->current_pos += 7;
	rtf_parse_font_table(parser_context);
    } else if (! strncmp("colortbl", word, 8) ) {
	parser_context->current_pos += 8;
	rtf_parse_color_table(parser_context);
    } else if (! strncmp("viewkind", word, 8) ) {
	parser_context->current_pos += 8;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\viewkind' control word without parameter.");
	if ( param != 4 )
	    rtf_warning( parser_context, "Unknown viewkind number.");
	parser_context->header_finished = TRUE;
    } else if (! strncmp("pard", word, 4) ) {
	parser_context->current_pos += 4;
	/* FIXME : resets paragraph state, but I don't know if this includes
	   font properties */
    } else if (! strncmp("keep", word, 4) ) {
      parser_context->current_pos += 5;
      /* different type of paragraph separation */
    } else if (! strncmp("keepn", word, 5) ) {
      parser_context->current_pos += 5;
      /* different type of paragraph separation */
    } else if (! strncmp("ulnone", word, 6) ) {
	parser_context->current_pos += 6;
	rtf_commit_pending_text( parser_context );
	attributes->underlined = FALSE;
    } else if (! strncmp("ul", word, 2) ) {
	parser_context->current_pos += 2;
	rtf_commit_pending_text( parser_context );
	if (! rtf_get_int_parameter(parser_context, &param))
	    attributes->underlined = TRUE;
	else
	    attributes->underlined = FALSE;
    } else if (! strncmp("strike", word, 6) ) {
	parser_context->current_pos += 6;
	rtf_commit_pending_text( parser_context );
	if (! rtf_get_int_parameter(parser_context, &param))
	    attributes->striked = TRUE;
	else
	    attributes->striked = FALSE;
    } else if (! strncmp("uc", word, 2) ) {
	parser_context->current_pos += 2;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\uc' control word without parameter.");
	attributes->unicode_skip = param;
    } else if (! strncmp("u", word, 1) ) {
	gchar buf[7];
	gint len;
	gint saved_codepage;
	parser_context->current_pos += 1;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\u' control word without parameter.");
	parser_context->current_pos += attributes->unicode_skip;
	len = g_unichar_to_utf8(param, buf);
	buf[len] = 0;
	rtf_commit_pending_text( parser_context );
	saved_codepage = parser_context->codepage;
	parser_context->codepage = 1200;
	g_string_append(attributes->text, buf);
	rtf_commit_pending_text( parser_context );
	parser_context->codepage = saved_codepage;
	dontskip = TRUE;
    } else if (! strncmp("par", word, 3) ) {
	parser_context->current_pos += 3;
    } else if (! strncmp("line", word, 4) ) {
	parser_context->current_pos += 4;
	g_string_append_c(attributes->text, '\n');
    } else if (! strncmp("cf", word, 2) ) {
	parser_context->current_pos += 2;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\cf' control word without parameter.");
	rtf_commit_pending_text( parser_context );
	attributes->foreground = param;
    } else if (! strncmp("cb", word, 2) ) {
	parser_context->current_pos += 2;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\cb' control word without parameter.");
	rtf_commit_pending_text( parser_context );
	attributes->background = param;
    }  else if (! strncmp("highlight", word, 9) ) {
	parser_context->current_pos += 9;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\highlight' control word without parameter.");
 	rtf_commit_pending_text( parser_context );
	attributes->background = param; /* I think this is the background */
    } else if (! strncmp("fs", word, 2) ) {
	gchar* tag_name;
	parser_context->current_pos += 2;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\fs' control word without parameter.");
	rtf_commit_pending_text( parser_context );
	attributes->size = param>>1;
	tag_name = g_strdup_printf("fs%i", attributes->size);
	if (parser_context->text_buffer &&
	    ! gtk_text_tag_table_lookup(parser_context->tags, tag_name)) {
	    GtkTextTag* tag = gtk_text_tag_new(tag_name);
	    g_object_set( tag, "size-points", (gdouble)attributes->size, NULL);
	    gtk_text_tag_table_add(parser_context->tags, tag);
	}
	g_free( tag_name );
    } else if (! strncmp("f", word, 1) ) {
	parser_context->current_pos += 1;
	parser_context->header_finished = TRUE;
	if (! rtf_get_int_parameter(parser_context, &param))
	    rtf_warning(parser_context, 
			"'\\f' control word without parameter.");
        rtf_commit_pending_text( parser_context );
        rtf_set_font_attributes(parser_context, param);
	/* FIXME : do something for fonts */
    } else if (! strncmp("b", word, 1) ) {
	parser_context->current_pos += 1;
	rtf_commit_pending_text( parser_context );
	if (! rtf_get_int_parameter(parser_context, &param))
	    attributes->bold = TRUE;
	else
	    attributes->bold = FALSE;
    } else if (! strncmp("i", word, 1) ) {
	parser_context->current_pos += 1;
	rtf_commit_pending_text( parser_context );
	if (! rtf_get_int_parameter(parser_context, &param))
	    attributes->italic = TRUE;
	else
	    attributes->italic = FALSE;
    } else if (! strncmp("'", word, 1) ) {
	gchar save = parser_context->current_pos[3];
	gchar new;
	parser_context->current_pos += 1;
	parser_context->current_pos[2] = 0;
	new = (gchar) strtol(parser_context->current_pos, NULL, 16);
	g_string_append_c(attributes->text, new);
	parser_context->current_pos[2] = save;
	parser_context->current_pos += 2;
	dontskip = TRUE;
    } else if (! strncmp("{", word, 1) ) {
	parser_context->current_pos += 1;
	g_string_append_c(attributes->text, '{');
    } else if (! strncmp("}", word, 1) ) {
	parser_context->current_pos += 1;
	g_string_append_c(attributes->text, '}');
    } else if (! strncmp("\\", word, 1) ) {
	parser_context->current_pos += 1;
	g_string_append_c(attributes->text, '\\');
    } else if (! strncmp("tab", word, 3) ) {
      parser_context->current_pos += 3;
      g_string_append_c(attributes->text, '\t');
    } else if (! strncmp("nowidctlpar", word, 11)) {
        /* ignore widow/orphan control */
        parser_context->current_pos += 11;
    } else if (! strncmp("widctlpar", word, 9)) {
        /* ignore widow/orphan control */
        parser_context->current_pos += 9;
    } else {
	rtf_warning(parser_context,
		    "Unknown control word.");
	while (g_ascii_isalpha(*parser_context->current_pos))
	    parser_context->current_pos++;
	rtf_get_int_parameter(parser_context, &param);
    }

    if (*parser_context->current_pos == ' ' && !dontskip )
	parser_context->current_pos++;
}


/*
 * When exiting an attibute group in the RTF description ('}'), this function
 * is called to pop one element from the attributes stack, hence restoring the
 * attributes state before entering the current attribute group.
 */
static void 
rtf_pop_state(RTFParserContext* parser_context) {
    RTFAttributes* attributes;

    g_assert(parser_context != NULL);

    attributes = (RTFAttributes*) parser_context->attributes_stack->data;
    parser_context->group_nesting_level--;
    rtf_commit_pending_text(parser_context);
    g_string_free(attributes->text, TRUE);
    parser_context->attributes_stack = parser_context->attributes_stack->next;
    g_free(attributes);
}

/*
 * Self explaining. Look above
 */
static void 
rtf_push_state(RTFParserContext* parser_context) {
    RTFAttributes* attributes;

    g_assert(parser_context != NULL);

    attributes = g_new(RTFAttributes, 1);
    rtf_commit_pending_text(parser_context);
    parser_context->group_nesting_level++;
    *attributes = *((RTFAttributes*) parser_context->attributes_stack->data);
    
    attributes->text = g_string_new("");

    parser_context->attributes_stack 
	= g_slist_prepend(parser_context->attributes_stack, attributes);
}

/*
 * Main loop of the parser.
 */
static void
rtf_parse(RTFParserContext* parser_context, gchar* rtf_text)
{
    gchar* current_pos = rtf_text;

    g_assert( parser_context != NULL && rtf_text && rtf_text[0] == '{' );
    current_pos++; parser_context->group_nesting_level = 0;
    
    g_assert( !strncmp(current_pos, "\\rtf1", 5) );

    do {

	if (*parser_context->current_pos == '{') {
	    parser_context->current_pos++;
	    rtf_push_state(parser_context);
	} else if (*parser_context->current_pos == '}') {
	    parser_context->current_pos++;
	    rtf_pop_state(parser_context);
	} else if (*parser_context->current_pos == '\\') {
	    rtf_parse_control_word(parser_context);
	} else if (*parser_context->current_pos == '\r') {
	    parser_context->current_pos++;
	} else {
	    if (parser_context->header_finished) {
		/* Nothing special, add character to current string */
		RTFAttributes* current_text;
		current_text = 
		    (RTFAttributes*) parser_context->attributes_stack->data;
		g_string_append_c(current_text->text, 
				  *parser_context->current_pos);
	    }
		parser_context->current_pos++;
	}
	
    } while (parser_context->group_nesting_level);

    

}
