/****************************
  Login for ICQ v6-7 protocol (Oscar)
  Olivier Crete (c) 2001
  GnomeICU
*****************************/

#include "common.h"
#include "gnomeicu.h"
#include "gnomeicu-spinner.h"
#include "gtkfunc.h"
#include "newsignup.h"
#include "util.h"
#include "v7login.h"
#include "v7recv.h"

#include <libgnomeui/libgnomeui.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>


gboolean v7_login_connected (GIOChannel *channel, GIOCondition condition,
			     V7Connection *conn);
gboolean v7_login_handler (GIOChannel *source, GIOCondition condition,
                           V7Connection *conn);
gboolean v7_login_error_handler (GIOChannel *iochannel, GIOCondition condition,
                                 V7Connection *conn);

gboolean isconnecting = FALSE;

V7Connection *v7_new_login_session(void)
{

  V7Connection *conn;
  gchar *login_server;
  gint login_port;
  struct sockaddr_in serv_addr;
  struct hostent *hp;
  GtkWidget *dialog;

#ifdef TRACE_FUNCTION
  g_print( "v7_new_login_session\n" );
#endif

  if (isconnecting)
    return NULL;

  isconnecting = TRUE;

  login_server = preferences_get_string (PREFS_ICQ_SERVER_NAME);
  login_port = preferences_get_int (PREFS_ICQ_SERVER_PORT);

  gnomeicu_spinner_start();

  /* This should probably be replace with getaddrinfo */
  if ((hp = gethostbyname(login_server)) == NULL){
    gnomeicu_spinner_stop();
    dialog = gtk_message_dialog_new (GTK_WINDOW(MainData->window),
				     GTK_DIALOG_MODAL | 
				     GTK_DIALOG_DESTROY_WITH_PARENT,
				     GTK_MESSAGE_ERROR,
				     GTK_BUTTONS_OK, 
				     _("Cannot resolve address of ICQ login server (%s):\n%s"), 
				     login_server, hstrerror(h_errno));
    g_free (login_server);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy (dialog);
    isconnecting = FALSE;
    return NULL;
  }
  g_free (login_server);

  conn = g_new0(V7Connection, 1);
  conn->last_reqid = 1;
  conn->status = NOT_CONNECTED;

  otherconnections = g_slist_append(otherconnections, conn);

  conn->fd = socket(PF_INET, SOCK_STREAM, 0);

  conn->iochannel = g_io_channel_unix_new(conn->fd);

  conn->in_watch = g_io_add_watch(conn->iochannel, G_IO_IN|G_IO_HUP|G_IO_ERR, 
				  (GIOFunc)v7_login_connected, conn);


  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(login_port);
  memcpy(&serv_addr.sin_addr, hp->h_addr, hp->h_length);
  
  fcntl(conn->fd, F_SETFL, fcntl(conn->fd, F_GETFL, 0)|O_NONBLOCK);
  connect(conn->fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));


  return conn;

}

gboolean v7_login_connected (GIOChannel *channel, GIOCondition condition,
			     V7Connection *conn)
{

#ifdef TRACE_FUNCTION
  g_print( "v7_login_connected\n" );
#endif
  g_assert(conn != NULL);

  if (condition != G_IO_IN) {

    otherconnections = g_slist_remove(otherconnections, conn);

    g_io_channel_shutdown(conn->iochannel, FALSE, NULL);
    g_free(conn);

    Current_Status = STATUS_OFFLINE;
    gnomeicu_set_status_button (FALSE);

    gnomeicu_spinner_stop ();
    isconnecting = FALSE;
    return FALSE;
  }

  conn->status = CONNECTING;

  conn->in_watch = g_io_add_watch ( conn->iochannel, G_IO_IN|G_IO_PRI,
                                    (GIOFunc) v7_login_handler, conn);
  conn->err_watch = g_io_add_watch ( conn->iochannel,
                                     G_IO_ERR|G_IO_HUP|G_IO_NVAL,
                                     (GIOFunc) v7_login_error_handler, conn);
  return FALSE;
}

gboolean v7_login_error_handler (GIOChannel *iochannel, GIOCondition condition,
                                 V7Connection *conn)
{
#ifdef TRACE_FUNCTION
  g_print( "v7_login_error_handler\n" );
#endif
  icq_set_status_offline( NULL, NULL);
  
  return FALSE; /* FIXME: not sure which value to return -- menesis */
}

gboolean v7_login_handler (GIOChannel *iochannel, GIOCondition condition,
                     V7Connection *conn)
{
  V7Packet *packet;
  TLVstack *tlvs;
  gchar *tempstr, *portstr, *errormsg=NULL, *url=NULL;
  TLV *tmp, *tlv1, *tlv2;
  gchar *addressport=NULL, *cookie=NULL;
  int cookielen = 0;
  WORD errorno=0;
  int i, port;
  gboolean show_dialog = FALSE;
  Snac *snac = NULL;
  GtkWidget *dialog;
  int length;
  gchar *here;
  gboolean has_uin = FALSE;
	
  /* Used to "encrypt" the pass (by xoring it) */
  static const unsigned char XORtable[] = { 0xf3, 0x26, 0x81, 0xc4,
                                            0x39, 0x86, 0xdb, 0x92,
                                            0x71, 0xa3, 0xb9, 0xe6,
                                            0x53, 0x7a, 0x95, 0x7c };
  
#ifdef TRACE_FUNCTION
  g_print( "v7_login_handler\n" );
#endif


  /* If we dont have the whole packet stop here */
  if ((packet = read_flap(conn)) == NULL) 
    return TRUE;
  
  switch(packet->channel) {
    
  case 1:
    if (packet->len == 4 && !strncmp("\0\0\0\x1", packet->buf->here, 0)) {

      /*  Lets create the packet */
      tlvs = new_tlvstack    ("\0\0\0\x01", 4);

      tempstr = g_strdup (our_info->uin); 
      add_tlv(tlvs, 0x01, tempstr, strlen(tempstr));
      g_free(tempstr);

      tempstr = g_strdup(passwd); 
      for(i = 0; i < strlen(passwd); i++)
	tempstr[i] = (tempstr[i] ^ XORtable[i%16]);
      add_tlv(tlvs, 0x02, tempstr, strlen(passwd));
      g_free(tempstr);

      /* tempstr = g_strdup_printf("ICQ Inc. - Product of ICQ (TM).2000b.4.63.1.3279.85"); */
      tempstr = g_strdup_printf("GnomeICU - Product of the GnomeICU hackers " VERSION);
      add_tlv(tlvs, 0x03, tempstr, strlen(tempstr));
      g_free(tempstr);

      add_tlv_w_be(tlvs, 0x16, 0x010A);      /* Type */
      add_tlv_w_be(tlvs, 0x17, 4);           /* Major Version */
      add_tlv_w_be(tlvs, 0x18, 63);          /* Minor Version */
      add_tlv_w_be(tlvs, 0x19,  1);          /* ICQ Number ??? */
      add_tlv_w_be(tlvs, 0x1A,  3279);       /* Build Major */
      add_tlv_dw_be(tlvs, 0x14, 85  );       /* Build Minor */
      add_tlv(tlvs, 0x0F, "en", 2);       /* Language ? */
      add_tlv(tlvs, 0x0E, "us", 2);       /* Country ? */
    
      if (! flap_send(conn, CHANNEL_NEWCONN, tlvs->beg, tlvs->len))
	conn->status = BROKEN;
    
      free_tlvstack(tlvs);

      free_flap(packet);
    
      return TRUE; 
    } else
      g_warning("Unknown packet on channel 1.\n");
    
    break;
  case 4:

    while (tmp = read_tlv(packet->buf, finished)) {

      switch(tmp->type) {
      case 1: 
	has_uin = TRUE;
	break;
      case 5:
	addressport = v7_buffer_get_string(tmp->buf, tmp->len, out_err);
	break;
      case 6:
	cookie = g_memdup(tmp->buf->here, tmp->len);
	cookielen =  tmp->len;
	break;
      case 0x08:
        errorno = v7_buffer_get_w_be(tmp->buf, out_err);
	break;
      case 0x0B:
      case 0x04:
        url = v7_buffer_get_string(tmp->buf, tmp->len, out_err);
        break;
      case 0x09:
      case 0x0C:
	break;
      default:
	errormsg = g_strdup_printf(_("Unknown TLV on login type: %d\n"), tmp->type);
      }
      free_tlv(tmp);
    }

  finished:

    if (errorno || url) {
      goto error_out;
    }

    if (!has_uin) {
      errormsg = g_strdup_printf(_("No UIN in login message"));
      goto error_out;
    } else if (!addressport) {
      errormsg = g_strdup_printf(_("No Server Address in login message"));
      goto error_out;
    } else if (!cookie) {
      errormsg = g_strdup_printf(_("No cookie in login message"));
      goto error_out;
    }
   


    portstr = strstr(addressport, ":");
    if (portstr == NULL) {
      errormsg = g_strdup_printf(_("Can't parse server name returned by server (%s)\n"),
		  addressport);
      goto error_out;
    }

    *portstr++ = 0;
      
    port = strtol(portstr, NULL, 10);
      
    mainconnection = v7_connect(addressport, port, cookie, cookielen);
         
  error_out:
    if (errorno) {
      switch (errorno) {
      case 0x01:
        show_dialog = TRUE;   /* Bad UIN */
        break;
      case 0x04:
      case 0x05:
        show_dialog = TRUE;   /* Wrong password */
        break;
      case 0x07:
      case 0x08:
        show_dialog = TRUE;   /* Unknown UIN */
        break;
      case 0x15:
      case 0x16:
        errormsg = g_strdup_printf(_("Error %x on connection:\n Too many clients from the same IP address"), errorno);
        break;
      case 0x18:
      case 0x1D:
        errormsg = g_strdup_printf(_("Error %x on connection: Rate exceeded"),
                                   errorno);
        break;
      case 0x1E:
        errormsg = g_strdup_printf(_("Error %x on connection: please try again"),
                                   errorno);
        break;
      case 0x00:
        break;
      default:
        errormsg = g_strdup_printf(_("Error %x on connection: Unknown"),
                                   errorno);
      }

      if(!show_dialog) {

        if (url) {
          if (errormsg) {
            gchar *tmperror;
            tmperror = errormsg;
            errormsg = g_strdup_printf("%s\n%s", errormsg, url);
            g_free(tmperror);
          }
          else
            errormsg = g_strdup_printf(_("Unknown error\n %s"), url);
        }
      
        if (errormsg) {
          dialog = gtk_message_dialog_new (GTK_WINDOW(MainData->window),
					   GTK_DIALOG_MODAL | 
					   GTK_DIALOG_DESTROY_WITH_PARENT,
					   GTK_MESSAGE_ERROR,
					   GTK_BUTTONS_OK, errormsg);
	  gtk_dialog_run(GTK_DIALOG(dialog));
	  gtk_widget_destroy (dialog);
          g_free(errormsg);
        }
        else {
          dialog = gtk_message_dialog_new (GTK_WINDOW(MainData->window),
					   GTK_DIALOG_MODAL | 
					   GTK_DIALOG_DESTROY_WITH_PARENT,
					   GTK_MESSAGE_ERROR,
					   GTK_BUTTONS_OK, 
					   _("Very very bizarre error when connecting.\n"));
	  gtk_dialog_run(GTK_DIALOG(dialog));
	  gtk_widget_destroy (dialog);
	}
      
        gnomeicu_spinner_stop();
    
      }
    }
    
    g_source_remove(conn->in_watch);
    g_source_remove(conn->err_watch);
    g_io_channel_shutdown(conn->iochannel,TRUE, NULL);
    otherconnections = g_slist_remove(otherconnections, conn);
    g_free(conn);
    isconnecting = FALSE;
    free_flap(packet);

    g_free(addressport);
    g_free(cookie);
    g_free(url);
    
    if(show_dialog) {
      if(show_uin_pwd_dialog() != GTK_RESPONSE_CANCEL)
        return TRUE;
      gnomeicu_spinner_stop();
    }

    return FALSE;

  case 2:
    snac = read_snac(packet);
    if (!snac)
      goto out_err;
    
    gnomeicu_spinner_stop();
    if (snac->family == 0x17 && snac->type == 1) {
      show_uin_pwd_dialog();
    } else
      g_warning("Unknown packet received\n");
    
    g_source_remove(conn->in_watch);
    g_source_remove(conn->err_watch);
    g_io_channel_shutdown(conn->iochannel, TRUE, NULL);
    otherconnections = g_slist_remove(otherconnections, conn);
    g_free(conn);
    isconnecting = FALSE;

    free_snac(snac);
    free_flap(packet);

    
    return FALSE;
    
  default:
    g_warning(_("Something very abnormal happened\n"));
  }

 out_err:
  g_free(addressport);
  g_free(cookie);
  free_flap(packet);
  return TRUE;

}

