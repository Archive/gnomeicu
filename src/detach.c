/************************************
 Contact detaching functions
 (c) 2002 Jeremy Wise
 GnomeICU
*************************************/

#include "common.h"
#include "detach.h"
#include "dragdrop.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "icons.h"
#include "response.h"
#include "user_popup.h"
#include "util.h"
#include "msg.h"

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

static void detach_initrow(Contact_Member *contact);

static gboolean
detach_window_click( GtkWidget *widget, GdkEventButton *event, CONTACT_PTR contact )
{
	if( event == NULL )
	       return FALSE;

	if( event->type == GDK_KEY_PRESS )
	{
 		if( ((GdkEventKey*)event)->keyval != GDK_Return && ((GdkEventKey*)event)->keyval != ' ' )
			return FALSE;
	}
	else if( ( event->type == GDK_2BUTTON_PRESS && event->button != 1 ) ||
		 ( event->type == GDK_BUTTON_PRESS && event->button != 3 ) )
			return FALSE;

	if( ( event->type == GDK_BUTTON_PRESS && event->button == 3 ) ||
	    ( event->type == GDK_KEY_PRESS && ((GdkEventKey*)event)->keyval == ' ' ) )
	{
		GtkWidget *personal_menu;
		GdkEventButton *bevent = (GdkEventButton *) event;

		personal_menu = user_popup (contact);
		gtk_menu_popup( GTK_MENU( personal_menu ), NULL, NULL, NULL,
				NULL, bevent->button, bevent->time );
		return TRUE;
	}

	if( g_slist_length( contact->stored_messages ) )
		show_contact_message (contact);
	else
	  if (contact->msg_dlg_xml != NULL) {
	    gtk_widget_show_all(gtk_bin_get_child(GTK_BIN(glade_xml_get_widget( contact->msg_dlg_xml, "message_dialog"))));
	    gtk_widget_grab_focus(glade_xml_get_widget( contact->msg_dlg_xml, "input"));
	    gtk_window_present(GTK_WINDOW(glade_xml_get_widget( contact->msg_dlg_xml, "message_dialog")));
	  }
	  else
	    open_message_dlg_with_message (contact, NULL);

	return TRUE;
}

static gboolean
detach_window_delete( GtkWidget *widget, GdkEvent *event, gpointer data )
{
	gtk_widget_hide( widget );
	return TRUE;
}

void detach_contact_hide( GtkWidget *widget, gpointer data )
{
    CONTACT_PTR contact = (CONTACT_PTR)data;

    if( !GTK_WIDGET_VISIBLE( contact->detached_window ) )
    {
        gtk_widget_show( GTK_WIDGET( contact->detached_window ) );
    }
    else
    {
        gtk_widget_hide( GTK_WIDGET( contact->detached_window ) );
    }
}

void detach_contact( GtkWidget *widget, gpointer data )
{
    CONTACT_PTR contact = (CONTACT_PTR)data;

    GtkWidget *window;
    GtkWidget *cellview;

    gchar *title;

    if( GTK_IS_WIDGET( contact->detached_window ) ) return;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    title = g_strdup_printf( _("%s status"), contact->nick );
    gtk_window_set_title( GTK_WINDOW( window ), title );
    g_free( title );
    contact->detached_window = window;

    cellview = gtk_cell_view_new();

    g_object_set_data(G_OBJECT(window), "cellview", cellview);

    gtk_cell_view_set_model (GTK_CELL_VIEW (cellview),
			     GTK_TREE_MODEL (MainData->contacts_store));

   
    gnomeicu_tree_setup_cell_layout(GTK_CELL_LAYOUT(cellview));

    gtk_widget_add_events(window, GDK_BUTTON_PRESS_MASK|GDK_KEY_PRESS_MASK);

    g_signal_connect( G_OBJECT( window ), "delete-event",
                      G_CALLBACK( detach_window_delete ), NULL );
    g_signal_connect( G_OBJECT( window ), "key-press-event",
                      G_CALLBACK( detach_window_click ), contact );
    g_signal_connect( G_OBJECT( window ), "button-press-event",
                      G_CALLBACK( detach_window_click ), contact );

    
    gtk_container_add( GTK_CONTAINER( window ), cellview );

    detach_initrow(contact);

    gtk_window_set_resizable( GTK_WINDOW( window ), FALSE );
    gtk_window_stick( GTK_WINDOW( window ) );


    gtk_widget_show_all( window );
}

void detach_initrow(Contact_Member *contact)
{

  GtkTreeIter iter, group;
  
  if (find_iter_for_contact_direct (GTK_TREE_MODEL(MainData->contacts_store),
				    contact, &iter, &group)) {
    detach_setrow(contact, &iter);
  }
}

void detach_setrow(Contact_Member *contact, GtkTreeIter *iter)
{
  GtkTreePath *path = NULL;
  GtkWidget *cellview;
  
  if (contact->detached_window == NULL)
    return;

  cellview = g_object_get_data(G_OBJECT(contact->detached_window), "cellview");
  
  path = gtk_tree_model_get_path(GTK_TREE_MODEL(MainData->contacts_store), iter);
  gtk_cell_view_set_displayed_row(GTK_CELL_VIEW(cellview), path);
  gtk_tree_path_free(path);
}
