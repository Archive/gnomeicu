/*****************************
 Dump a packet to the screen
 (c) 1999 Jeremy Wise
 GnomeICU
******************************/

#include "gnomeicu.h"
#include "packetprint.h"

#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define READ_TCP_COL  "\x1B[1;36m"
#define WRITE_TCP_COL "\x1B[0;34m"
#define REG_COL       "\x1B[0m"

void packet_print( BYTE *packet, int size, int type, gchar *charid )
{
	int cx;
	gchar condensed[17];
	time_t tm;

	g_assert(packet != NULL);

	if( !preferences_get_bool (PREFS_ICQ_DEBUG_FILE_TRANSFER) &&
            (type & PACKET_TYPE_TCP) )
		return;

	/* Moved this below -- above checks means I'm not interested */
#ifdef TRACE_FUNCTION
	g_print( "packet_print\n" );
#endif

	condensed[16] = 0x00;

	switch( type )
	{
		case ( PACKET_TYPE_TCP | PACKET_DIRECTION_SEND ):
			printf( WRITE_TCP_COL "Sending packet (TCP) %d:\n", size );
			break;
		case( PACKET_TYPE_TCP | PACKET_DIRECTION_RECEIVE ):
			printf( READ_TCP_COL "Recieved packet (TCP) %d:\n", size );
			break;
		default:
			g_warning("Unknown packet type for printing: %d\n", type);
	}

	strcpy( condensed, "" );
	tm = time(NULL);
	printf( "%s %s", charid, ctime(&tm) );

	for( cx = 0; cx < size; cx ++ )
	{
		if( isprint( packet[cx] ) )
			strncat( condensed, &packet[cx], 1 );
		else
			strcat( condensed, "." );

		if( cx % 16 == 4 || cx % 16 == 8 || cx % 16 == 12 )
			printf( "- " );

		printf("%02x ", packet[cx] );

		if( cx % 16 == 15 )
		{
			printf( "    %s\n", condensed );
			condensed[0] = '\x0';
		}
	}

	for( ; cx % 16; cx ++ )
	{
		printf( "   " );
		if( cx % 16 == 4 || cx % 16 == 8 || cx % 16 == 12 )
			printf( "  " );
	}
	printf( "    %s", condensed );
	printf( REG_COL "\n\n" );
	fflush( stdout );
}
