/**************************
 Configuration parser
 (c) 1999 Jeremy Wise
 GnomeICU
***************************/

#ifndef __GNOMECFG_H__
#define __GNOMECFG_H__

void Read_RC_File( void );
void Save_RC( void );

#endif /* __GNOMECFG_H__ */
