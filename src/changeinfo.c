/*******************************
 Change personal information
 (c) 1999 Jeremy Wise
 GnomeICU
********************************/

#include "common.h"
#include "changeinfo.h"
#include "gnomecfg.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "util.h"
#include "v7send.h"

#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <libgnomeui/gnome-dateedit.h>

/*** Toplevel header files ***/

/*** Local function declarations ***/
void homeinfo_change( GtkWidget *widget, gpointer pointer );
void workinfo_change( GtkWidget *widget, gpointer pointer );
void moreinfo_change( GtkWidget *widget, gpointer pointer );
void about_change( GtkWidget *widget, gpointer pointer );
void emails_change( GtkWidget *widget, gpointer pointer );
void interests_change( GtkWidget *widget, gpointer pointer );
void org_aff_change( GtkWidget *widget, gpointer pointer );
void birthday_change( GtkWidget *widget, gpointer pointer );
void perms_change( GtkWidget *widget, gpointer pointer );
static void homeinfo_save(void);
static void workinfo_save(void);
static void moreinfo_save(void);
static void about_save(void);
static void emails_save(void);
static void interests_save(void);
static void org_aff_save(void);
static void perms_save(void);
static void password_save(void);

void menus_entries_init(gchar *prefix, FIELD_T type, gint number);
void menus_entries_save(gchar *prefix, gint count, gchar **data, gint offset, gint other);
void menus_entries_update(gchar *prefix, GList *list, gint number, gint offset, gint other);

void email_hidden_toggled( GtkCellRendererToggle *cell, gchar *path_str, gpointer data );
void email_edited (GtkCellRendererText *cell, const gchar *path_string, const gchar *new_text, gpointer data); 
void email_selected (GtkTreeSelection *selection, gpointer user_data);
void on_reload_button_clicked ( GtkWidget *widget, gpointer pointer );
void on_save_button_clicked ( GtkWidget *widget, gpointer pointer );
void on_cancel_button_clicked ( GtkWidget *widget, gpointer pointer );
void on_add_other_email_button_clicked ( GtkWidget *widget, gpointer pointer );
void on_remove_other_email_button_clicked ( GtkWidget *widget, gpointer pointer );
gboolean on_our_info_dialog_delete_event (GtkWidget *widget, gpointer data);

static gboolean homeinfo_changed, 
  workinfo_changed,
  moreinfo_changed,
  about_changed,
  emails_changed,
  perms_changed,
  interests_changed,
  org_aff_changed,
  password_changed;

static GladeXML *our_info_xml = NULL;

enum
{
	HIDDEN_COLUMN,
	EMAIL_COLUMN,
	EDITABLE_COLUMN,
	EMAILS_N_COLUMNS
};


/*** Global functions ***/
void change_info_window( GtkWidget *widget, gpointer data )
{
  GtkListStore *store;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  GtkTreeView *treeview;

#ifdef TRACE_FUNCTION
  g_print( "change_info_window\n" );
#endif

  homeinfo_changed = FALSE;
  workinfo_changed = FALSE;
  moreinfo_changed = FALSE;
  about_changed    = FALSE;
  emails_changed   = FALSE;
  interests_changed= FALSE;
  perms_changed    = FALSE;
  org_aff_changed  = FALSE;
  password_changed = FALSE;
	
  if (our_info_xml != NULL) {
    gtk_window_present (GTK_WINDOW(glade_xml_get_widget (our_info_xml, "our_info_dialog")));
    if (is_connected(NULL, NULL))
      v7_request_our_info(mainconnection);
    return;
  }

  our_info_xml = gicu_util_open_glade_xml ("user_info.glade", "our_info_dialog");
  if (our_info_xml == NULL)
    return;

  gtk_combo_set_popdown_strings( GTK_COMBO( glade_xml_get_widget (our_info_xml, "work_country_combo" ) ), get_country_codes() );
  gtk_combo_set_popdown_strings( GTK_COMBO( glade_xml_get_widget (our_info_xml, "country_combo" ) ), get_country_codes() );
  gtk_combo_set_popdown_strings( GTK_COMBO( glade_xml_get_widget (our_info_xml, "timezone_combo" ) ), get_timezone_codes() );
  gtk_combo_set_popdown_strings( GTK_COMBO( glade_xml_get_widget (our_info_xml, "language1_combo" ) ), get_language_codes() );
  gtk_combo_set_popdown_strings( GTK_COMBO( glade_xml_get_widget (our_info_xml, "language2_combo" ) ), get_language_codes() );
  gtk_combo_set_popdown_strings( GTK_COMBO( glade_xml_get_widget (our_info_xml, "language3_combo" ) ), get_language_codes() ); 
  gtk_combo_set_popdown_strings( GTK_COMBO( glade_xml_get_widget (our_info_xml, "occupation_combo" ) ), get_work_codes() );

  g_signal_connect (G_OBJECT (gtk_text_view_get_buffer (GTK_TEXT_VIEW( glade_xml_get_widget (our_info_xml, "about_text" )))), "changed", G_CALLBACK (about_change), NULL);

  /* stuff for the GtkTreeView (list of emails) */
  treeview = GTK_TREE_VIEW( glade_xml_get_widget (our_info_xml, "emails_treeview") );
  store = gtk_list_store_new( EMAILS_N_COLUMNS,
                              G_TYPE_BOOLEAN,
                              G_TYPE_STRING,
                              G_TYPE_BOOLEAN);
  gtk_tree_view_set_model(treeview, GTK_TREE_MODEL(store));

  renderer = gtk_cell_renderer_toggle_new ();
  g_signal_connect (G_OBJECT (renderer), "toggled",
		    G_CALLBACK (email_hidden_toggled), store);
  column = gtk_tree_view_column_new_with_attributes (_("Hidden"),
						     renderer,
						     "active", HIDDEN_COLUMN,
						     NULL);
  gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column),
				   GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 60);
  gtk_tree_view_append_column (treeview, column);

  renderer = gtk_cell_renderer_text_new ();
  g_signal_connect (G_OBJECT (renderer), "edited", G_CALLBACK (email_edited), store);
  column = gtk_tree_view_column_new_with_attributes (_("Email"),
						     renderer,
						     "text", EMAIL_COLUMN,
						     "editable", EDITABLE_COLUMN,
						     NULL);

  gtk_tree_view_append_column (treeview, column);
  
  gtk_widget_hide_all(glade_xml_get_widget (our_info_xml, "interests_table")); /* required for the optionmenus to update */
	
  menus_entries_init("interest", INTEREST, 4);

  gtk_widget_show_all(glade_xml_get_widget (our_info_xml, "interests_table"));

  gtk_widget_hide_all(glade_xml_get_widget (our_info_xml, "affiliations_vbox"));
  
  menus_entries_init("org", AFFILIATION, 3);
  menus_entries_init("affil", PAST, 3);

  gtk_widget_show_all(glade_xml_get_widget (our_info_xml, "affiliations_vbox"));

	/* Doing this from glade doesn't seem to work... */
  gtk_label_set_mnemonic_widget (GTK_LABEL (glade_xml_get_widget (our_info_xml, "birthday_label")),
                                 GTK_WIDGET (glade_xml_get_widget (our_info_xml, "birthday_dateedit")));

  g_signal_connect (G_OBJECT (gtk_tree_view_get_selection(treeview)), "changed", G_CALLBACK (email_selected), NULL);

  g_signal_connect (G_OBJECT (gtk_text_view_get_buffer (GTK_TEXT_VIEW( glade_xml_get_widget (our_info_xml, "about_text" )))), "changed", G_CALLBACK (about_change), NULL);

  g_object_set_data (G_OBJECT (glade_xml_get_widget (our_info_xml, "reload_button")), "dlg", glade_xml_get_widget (our_info_xml, "our_info_dialog"));
  g_object_set_data (G_OBJECT (glade_xml_get_widget (our_info_xml, "cancel_button")), "dlg", glade_xml_get_widget (our_info_xml, "our_info_dialog"));
  g_object_set_data (G_OBJECT (glade_xml_get_widget (our_info_xml, "save_button")), "dlg", glade_xml_get_widget (our_info_xml, "our_info_dialog"));

  glade_xml_signal_autoconnect (our_info_xml);

  if (is_connected(NULL, NULL))
    v7_request_our_info(mainconnection);
}

/* initializes all optionmenu+entry settings */
void menus_entries_init(gchar *prefix, FIELD_T type, gint number)
{
  GList *list;
  gint counter;
  gchar *tmp_menu;
  gchar *tmp_entry;
  
#ifdef TRACE_FUNCTION
  g_print( "menus_entries_init\n" );
#endif

  for(counter = 1; counter <= number; counter++) {
    tmp_menu = g_strdup_printf("%s_optionmenu%d", prefix, counter);
    tmp_entry = g_strdup_printf("%s_entry%d", prefix, counter);

    for(list = get_codes(type); list != NULL; list = g_list_next(list)) {
      gtk_menu_shell_append(GTK_MENU_SHELL(gtk_option_menu_get_menu(GTK_OPTION_MENU(glade_xml_get_widget (our_info_xml, tmp_menu)))),
                            gtk_menu_item_new_with_label(list->data));
    }

    gtk_option_menu_set_history(GTK_OPTION_MENU(glade_xml_get_widget (our_info_xml, tmp_menu)), 0);

    gtk_widget_set_sensitive(glade_xml_get_widget(our_info_xml, tmp_entry), FALSE);

    g_free(tmp_menu);
    g_free(tmp_entry);
  }
}

void homeinfo_change( GtkWidget *widget, gpointer pointer )
{
#ifdef TRACE_FUNCTION
  g_print( "homeinfo_change\n" );
#endif

  homeinfo_changed = TRUE;
}

void workinfo_change( GtkWidget *widget, gpointer pointer )
{
#ifdef TRACE_FUNCTION
  g_print( "workinfo_change\n" );
#endif

  workinfo_changed = TRUE;
}

void moreinfo_change( GtkWidget *widget, gpointer pointer )
{
#ifdef TRACE_FUNCTION
  g_print( "moreinfo_change\n" );
#endif

  moreinfo_changed = TRUE;
}

void about_change( GtkWidget *widget, gpointer pointer )
{
#ifdef TRACE_FUNCTION
  g_print( "about_change\n" );
#endif

  about_changed = TRUE;
}

void perms_change( GtkWidget *widget, gpointer pointer )
{
#ifdef TRACE_FUNCTION
  g_print( "perms_change\n" );
#endif

  perms_changed = TRUE;
}

void emails_change( GtkWidget *widget, gpointer pointer )
{
#ifdef TRACE_FUNCTION
  g_print( "emails_change\n" );
#endif

  emails_changed = TRUE;  
}

void interests_change( GtkWidget *widget, gpointer pointer )
{
  gint count = 1;
  gchar *tmp_entry;
  gchar *tmp_menu;

#ifdef TRACE_FUNCTION
  g_print( "interests_change\n" );
#endif

  /* disables entries with "Not selected" optionmenus */
	
  for(count = 1; count <= 4; count++) {
    tmp_entry = g_strdup_printf("interest_entry%d", count);
    tmp_menu = g_strdup_printf("interest_optionmenu%d", count);
    gtk_widget_set_sensitive(glade_xml_get_widget(our_info_xml, tmp_entry), 
          gtk_option_menu_get_history(GTK_OPTION_MENU(glade_xml_get_widget (our_info_xml, tmp_menu))));
    g_free(tmp_entry);
    g_free(tmp_menu);
  }
		
  interests_changed = TRUE;
}

void org_aff_change( GtkWidget *widget, gpointer pointer )
{
  gint count = 1;
  gchar *tmp_entry;
  gchar *tmp_menu;

#ifdef TRACE_FUNCTION
  g_print( "org_aff_change\n" );
#endif

  for(count = 1; count <= 3; count++) {
    tmp_entry = g_strdup_printf("org_entry%d", count);
    tmp_menu = g_strdup_printf("org_optionmenu%d", count);
    gtk_widget_set_sensitive(glade_xml_get_widget(our_info_xml, tmp_entry), 
          gtk_option_menu_get_history(GTK_OPTION_MENU(glade_xml_get_widget (our_info_xml, tmp_menu))));
    g_free(tmp_entry);
    g_free(tmp_menu);

    tmp_entry = g_strdup_printf("affil_entry%d", count);
    tmp_menu = g_strdup_printf("affil_optionmenu%d", count);
    gtk_widget_set_sensitive(glade_xml_get_widget(our_info_xml, tmp_entry), 
          gtk_option_menu_get_history(GTK_OPTION_MENU(glade_xml_get_widget (our_info_xml, tmp_menu))));
    g_free(tmp_entry);
    g_free(tmp_menu);
  }
		
  org_aff_changed = TRUE;
}



void birthday_change( GtkWidget *widget, gpointer pointer )
{
  time_t birthday_t;
  struct tm *birthday_tm;
  GDate *date;
  GDateYear year;
#ifdef TRACE_FUNCTION
  g_print( "birthday_change\n" );
#endif

  moreinfo_change(widget, pointer);
  date = g_date_new();
  g_date_clear(date, 1);
  g_date_set_time (date, time (NULL));
  if (!g_date_valid(date)) {
	  g_date_free(date);
	  return;
  }

  birthday_t = gnome_date_edit_get_time(GNOME_DATE_EDIT(widget));
  birthday_tm = localtime(&birthday_t);
  
  if (!birthday_tm)
    return;

  g_date_subtract_years(date, birthday_tm->tm_year);
  g_date_subtract_months(date, birthday_tm->tm_mon);
  g_date_subtract_days(date, birthday_tm->tm_mday-1);

  year = g_date_get_year(date);
  if ((year != G_DATE_BAD_YEAR) && (year >= 1900)) {
	char *string;
	gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "age_entry")), (string =  g_strdup_printf("%d", year - 1900)));
	g_free(string);
  }

  g_date_free(date);
}

void email_edited (GtkCellRendererText *cell,
	     const gchar         *path_string,
	     const gchar         *new_text,
	     gpointer             data)
{
  GtkTreeModel *model;
  GtkTreePath *path;
  GtkTreeIter iter;
  gchar *old_text;

#ifdef TRACE_FUNCTION
  g_print( "email_edited\n" );
#endif

  if (!new_text || new_text[0] == '\0') {
	  GtkWidget *dialog;
	  dialog = gtk_message_dialog_new (GTK_WINDOW(glade_xml_get_widget (our_info_xml, "our_info_dialog")),
		                                GTK_DIALOG_DESTROY_WITH_PARENT,
		                                GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
		                                _("An email address can't be empty."));
	  gtk_dialog_run (GTK_DIALOG (dialog));
	  gtk_widget_destroy (dialog);
	  return;
  }

  model = (GtkTreeModel *)data;
  path = gtk_tree_path_new_from_string (path_string);

  gtk_tree_model_get_iter (model, &iter, path);

  gtk_tree_model_get (model, &iter, EMAIL_COLUMN, &old_text, -1);
  gtk_list_store_set (GTK_LIST_STORE (model), &iter, EMAIL_COLUMN,
                         new_text, -1);

  /* clean up */
  g_free (old_text);
  gtk_tree_path_free (path);

  emails_change(NULL, NULL);
}

void email_selected (GtkTreeSelection *selection, gpointer user_data)
{
#ifdef TRACE_FUNCTION
  g_print( "email_selected\n" );
#endif
  if (gtk_tree_selection_get_selected(selection, NULL, NULL)) {
	  gtk_widget_set_sensitive(GTK_WIDGET (glade_xml_get_widget (our_info_xml, "remove_other_email_button")), TRUE);
  } else {
	  gtk_widget_set_sensitive(GTK_WIDGET (glade_xml_get_widget (our_info_xml, "remove_other_email_button")), FALSE);
  }
}

void email_hidden_toggled( GtkCellRendererToggle *cell, gchar *path_str, gpointer data )
{
  GtkTreeModel *model;
  GtkTreeIter  iter;
  GtkTreePath *path;
  gboolean hidden;

#ifdef TRACE_FUNCTION
  g_print( "email_hidden_toggled\n" );
#endif

  model = (GtkTreeModel *)data;
  path = gtk_tree_path_new_from_string (path_str);
  
  /* get hidden iter */
  gtk_tree_model_get_iter (model, &iter, path);
  gtk_tree_model_get (model, &iter, HIDDEN_COLUMN, &hidden, -1);

  /* set new value */
  gtk_list_store_set (GTK_LIST_STORE (model), &iter, HIDDEN_COLUMN, !hidden, -1);

  /* clean up */
  gtk_tree_path_free (path);

  emails_change(NULL, NULL);
}

void password_entry_changed (GtkWidget *widget, gpointer p)
{
  GtkWidget *entry, *entry_confirm;
  GtkWidget *match_label;

  entry = glade_xml_get_widget (our_info_xml, "password_entry");
  entry_confirm = glade_xml_get_widget (our_info_xml, "password_confirm_entry");
  match_label = glade_xml_get_widget (our_info_xml, "match_label");

  if (strlen (gtk_entry_get_text (GTK_ENTRY (entry)))) {
    gtk_widget_set_sensitive (entry_confirm, TRUE);
  } else {
    gtk_widget_set_sensitive (entry_confirm, FALSE);
    gtk_widget_hide (match_label);
    return;
  }


  if (strcmp (gtk_entry_get_text (GTK_ENTRY (entry)),
              gtk_entry_get_text (GTK_ENTRY (entry_confirm))) == 0) {
    password_changed = TRUE;
    gtk_widget_hide (match_label);
  } else {
    gtk_widget_show (match_label);
    password_changed = FALSE;
  }
}

void on_reload_button_clicked ( GtkWidget *widget, gpointer pointer )
{
    if (!is_connected(GTK_WINDOW (g_object_get_data (G_OBJECT (widget), "dlg")), _("You can not update your information while disconnected.")))
        return;
    v7_request_our_info(mainconnection);
}

void on_save_button_clicked ( GtkWidget *widget, gpointer pointer )
{
  GtkWidget *window;

#ifdef TRACE_FUNCTION
  g_print( "on_save_button_clicked\n" );
#endif

  window = g_object_get_data (G_OBJECT (widget), "dlg");
  if (!is_connected(GTK_WINDOW (window), _("You can not save your informations while disconnected.")))
    return;

  if (homeinfo_changed) {
    homeinfo_save();
  }

  if (workinfo_changed) {
    workinfo_save();
  }
  
  if (moreinfo_changed) {
    moreinfo_save();
  }

  if (about_changed) {
    about_save();
  }

  if (emails_changed) {
    emails_save();
  }

  if (interests_changed) {
    interests_save();
  }

  if (org_aff_changed) {
    org_aff_save();
  }
	
  if (perms_changed) {
    perms_save();
  }

  if (password_changed) {
    password_save();
  }

  gtk_widget_hide (window);
}

void on_cancel_button_clicked ( GtkWidget *widget, gpointer pointer )
{
  GtkWidget *dialog;

  dialog = g_object_get_data (G_OBJECT (widget), "dlg");
  on_our_info_dialog_delete_event (dialog, NULL);
}

void on_add_other_email_button_clicked ( GtkWidget *widget, gpointer pointer )
{
  GtkTreeView *tree;
  GtkListStore *store;
  GtkTreeIter iter;
  GtkTreePath *path;
  GtkTreeViewColumn *column;

#ifdef TRACE_FUNCTION
  g_print( "on_add_other_email_button_clicked\n" );
#endif

  tree = GTK_TREE_VIEW (glade_xml_get_widget (our_info_xml, "emails_treeview"));
  store = GTK_LIST_STORE (gtk_tree_view_get_model (GTK_TREE_VIEW (glade_xml_get_widget (our_info_xml, "emails_treeview"))));

  /* add data to the list store */
  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter,
                      EMAIL_COLUMN, _("New e-mail address"),
                      HIDDEN_COLUMN, FALSE,
                      EDITABLE_COLUMN, TRUE,
                      -1);

  path = gtk_tree_model_get_path (GTK_TREE_MODEL (store), &iter);
  column = gtk_tree_view_get_column (tree, EMAIL_COLUMN);

  gtk_tree_view_set_cursor (tree, path, column, TRUE);
  emails_change(NULL, NULL);
}

void on_remove_other_email_button_clicked ( GtkWidget *widget, gpointer pointer )
{
  GtkTreeIter iter;
  GtkTreeView *treeview;
  GtkTreeModel *model;
  GtkTreeSelection *selection;

#ifdef TRACE_FUNCTION
  g_print( "on_remove_other_email_button_clicked\n" );
#endif

  treeview = GTK_TREE_VIEW( glade_xml_get_widget (our_info_xml, "emails_treeview") );
  selection = gtk_tree_view_get_selection (treeview);

  if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
      gtk_list_store_remove (GTK_LIST_STORE (model), &iter);
      emails_change(NULL, NULL);
  }
}

gboolean on_our_info_dialog_delete_event (GtkWidget *widget, gpointer data)
{
  GtkWidget *entry, *entry_confirm;

  gtk_widget_hide (widget);

  entry = glade_xml_get_widget (our_info_xml, "password_entry");
  entry_confirm = glade_xml_get_widget (our_info_xml, "password_confirm_entry");

  gtk_entry_set_text (GTK_ENTRY (entry), "");
  gtk_entry_set_text (GTK_ENTRY (entry_confirm), "");

  return TRUE;
}

static void homeinfo_save()
{

#ifdef TRACE_FUNCTION
  g_print( "homeinfo_save\n" );
#endif

  g_free( our_info->nick );
  our_info->nick = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "nick_entry") )));
  
  g_free( our_info->first );
  our_info->first = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "fname_entry" ))));
  
  g_free( our_info->last );
  our_info->last = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "lname_entry" ))));

  g_free( our_info->email );
  our_info->email = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "email_entry" ) ) ) );
  
  our_info->hide_email = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( glade_xml_get_widget (our_info_xml, "hide_email_checkbutton" )));
  
  g_free( our_info->phone );
  our_info->phone = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "phone_entry" ) ) ) );
  
  g_free( our_info->fax );
  our_info->fax = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "fax_entry" ) ) ) );
  
  g_free( our_info->cellular );
  our_info->cellular = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "cellular_entry" ) ) ) );
  
  g_free( our_info->street );
  our_info->street = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "street_entry" ) ) ) );
  
  g_free( our_info->city );
  our_info->city = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "city_entry" ) ) ) );

  g_free( our_info->state );
  our_info->state = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "state_entry" ) ) ) );

  g_free(our_info->zip);
  our_info->zip = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "zip_entry" ) ) ) );

  our_info->country = get_country_code( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "country_combo_entry"  ) ) ) );

  our_info->timezone = get_timezone_code( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "timezone_combo_entry"  ) ) ) );

  v7_modify_main_home_info (mainconnection, our_info->nick, our_info->first,
                            our_info->last, our_info->email,
                            our_info->city, our_info->state,
                            our_info->phone, our_info->fax,
                            our_info->street, our_info->cellular,
                            our_info->zip, our_info->country,
                            our_info->timezone, our_info->hide_email);
  homeinfo_changed = FALSE;;
}

static void workinfo_save()
{

#ifdef TRACE_FUNCTION
  g_print( "workinfo_save\n" );
#endif

  our_info->occupation = get_work_code( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "occupation_combo_entry" ) ) ) );

  g_free(our_info->job_pos);
  our_info->job_pos = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "work_position_entry" ) ) ) );

  g_free(our_info->department);
  our_info->department = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "work_department_entry" ) ) ) );

  g_free(our_info->company_name);
  our_info->company_name = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "work_company_entry" ) ) ) );

  g_free(our_info->work_address);
  our_info->work_address = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "work_street_entry" ) ) ) );

  g_free(our_info->work_city);
  our_info->work_city = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "work_city_entry" ) ) ) );

  g_free(our_info->work_state);
  our_info->work_state = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "work_state_entry" ) ) ) );

  g_free(our_info->work_zip);
  our_info->work_zip = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "work_zip_entry" ) ) ) );
  
  g_free( our_info->work_phone );
  our_info->work_phone = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "work_phone_entry" ) ) ) );

  g_free( our_info->work_fax );
  our_info->work_fax = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "work_fax_entry" ) ) ) );

  g_free(our_info->work_homepage);
  our_info->work_homepage = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "work_homepage_entry" ) ) ) );
  
  our_info->work_country = get_country_code( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "work_country_combo_entry" ) ) ) );

  v7_modify_work_info (mainconnection, our_info->work_city, our_info->work_state,
                       our_info->work_phone, our_info->work_fax,
                       our_info->work_address, our_info->work_zip, our_info->work_country,
                       our_info->company_name, our_info->department,
                       our_info->job_pos, our_info->occupation,
                       our_info->work_homepage);

  workinfo_changed = FALSE;
}

static void moreinfo_save()
{
  time_t birthday_t;
  struct tm *birthday_tm;

#ifdef TRACE_FUNCTION
  g_print( "moreinfo_save\n" );
#endif

  our_info->age = atoi( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "age_entry" ) ) ) );
  
  birthday_t = gnome_date_edit_get_time(GNOME_DATE_EDIT(glade_xml_get_widget (our_info_xml, "birthday_dateedit")));
  birthday_tm = localtime(&birthday_t);

  if (birthday_tm) {
    our_info->birth_day = birthday_tm->tm_mday;
    our_info->birth_month = birthday_tm->tm_mon + 1;
    our_info->birth_year = birthday_tm->tm_year + 1900;
  }

  if( !strcmp( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "sex_combo_entry"))), _("Male")))
    our_info->sex = MALE;
  else if( !strcmp( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "sex_combo_entry"))), _("Female")))
    our_info->sex = FEMALE;
  else
    our_info->sex = NOT_SPECIFIED;
  
  our_info->language1 = get_language_code( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "language1_combo_entry"  ) ) ) );
  our_info->language2 = get_language_code( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "language2_combo_entry"  ) ) ) );
  our_info->language3 = get_language_code( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "language3_combo_entry"  ) ) ) );

  g_free( our_info->homepage );
  our_info->homepage = g_strdup( gtk_entry_get_text( GTK_ENTRY( glade_xml_get_widget (our_info_xml, "homepage_entry" ) ) ) );

  v7_modify_more_info (mainconnection, our_info->age, our_info->sex, our_info->homepage,
                       our_info->birth_year, our_info->birth_month, our_info->birth_day,
		       our_info->language1, our_info->language2, our_info->language3);

  moreinfo_changed = FALSE;
}

static void about_save()
{
  GtkTextView* text_view;
  GtkTextBuffer* text_buffer;
  GtkTextIter start_iter, end_iter;

#ifdef TRACE_FUNCTION
  g_print( "about_save\n" );
#endif

  g_free( our_info->about );
  text_view = GTK_TEXT_VIEW(glade_xml_get_widget (our_info_xml, "about_text" ));
  text_buffer = gtk_text_view_get_buffer (text_view);
  gtk_text_buffer_get_bounds(text_buffer, &start_iter, &end_iter);
  our_info->about = gtk_text_buffer_get_text(text_buffer,
                                             &start_iter,&end_iter, FALSE);
  
  v7_modify_about (mainconnection, our_info->about);

  about_changed = FALSE;
}

static void emails_save()
{
	GList *email;
	gchar *address;
	gboolean hidden;
	gchar *data;
	GtkTreeModel *model;
	GtkTreeIter iter;
	gint len_str;

#ifdef TRACE_FUNCTION
  g_print( "emails_save\n" );
#endif

  /* we free all the data in info->emails */
  for (email = our_info->emails; email != NULL; email = email->next)
    g_free(email->data);
  g_list_free(our_info->emails);
  our_info->emails = NULL;

  model = gtk_tree_view_get_model(GTK_TREE_VIEW( glade_xml_get_widget (our_info_xml, "emails_treeview") ));
  if (gtk_tree_model_get_iter_first(model, &iter)) {
	  do {
		 gtk_tree_model_get(model, &iter,
		                    EMAIL_COLUMN, &address,
		                    HIDDEN_COLUMN, &hidden,
		                    -1);

		 len_str = strlen(address);
		 data = g_malloc0(len_str+1);
		 data[0] = hidden;
		 strncpy( data+1, address, len_str );
		 g_free(address);
		 our_info->emails = g_list_append(our_info->emails, data);
	  } while (gtk_tree_model_iter_next(model, &iter));
  }

  v7_modify_emails_info(mainconnection, our_info->emails);

  emails_changed = FALSE;
}


static void interests_save()
{
  GList *interest;
  gchar *data;
  gint count = 1;
  gchar *tmp_menu;

#ifdef TRACE_FUNCTION
  g_print( "interests_save\n" );
#endif

  /* we free all the data in info->interests */
  for (interest = our_info->interests; interest != NULL; interest = interest->next)
    g_free(interest->data);
  
  g_list_free(our_info->interests);
  our_info->interests = NULL;

  while(count <= 4) {
    tmp_menu = g_strdup_printf("interest_optionmenu%d", count);
    if (gtk_option_menu_get_history(GTK_OPTION_MENU(glade_xml_get_widget (our_info_xml, tmp_menu))) != 0) {
      menus_entries_save("interest", count, &data, 99, -1);
      our_info->interests = g_list_append(our_info->interests, data);
    }
    g_free(tmp_menu);
    count++;
  }
	
  v7_modify_interests_info(mainconnection, our_info->interests);

  interests_changed = FALSE;
}


static void org_aff_save()
{
  GList *list = NULL;
  gchar *data1 = NULL;
  gchar *data2 = NULL;
  gint count = 1;
  gchar *tmp_menu;

#ifdef TRACE_FUNCTION
  g_print( "org_aff_save\n" );
#endif

  for (list = our_info->affiliations; list != NULL; list = list->next)
    g_free(list->data);
	
  for (list = our_info->past_background; list != NULL; list = list->next)
    g_free(list->data);
		
  g_list_free(our_info->affiliations);
  g_list_free(our_info->past_background);
  
  our_info->affiliations = NULL;
  our_info->past_background = NULL;

  for(count = 1; count <= 3; count++) {
    tmp_menu = g_strdup_printf("org_optionmenu%d", count);
    if(gtk_option_menu_get_history(GTK_OPTION_MENU(glade_xml_get_widget (our_info_xml, tmp_menu))) != 0) {
      menus_entries_save("org", count, &data1, 199, 219);
      our_info->affiliations = g_list_append(our_info->affiliations, data1);
    }
    g_free(tmp_menu);

    tmp_menu = g_strdup_printf("affil_optionmenu%d", count);
    if(gtk_option_menu_get_history(GTK_OPTION_MENU(glade_xml_get_widget (our_info_xml, tmp_menu))) != 0) {
      menus_entries_save("affil", count, &data2, 299, 307);
      our_info->past_background = g_list_append(our_info->past_background, data2);
    }
    g_free(tmp_menu);
  }

  v7_modify_past_aff_info(mainconnection, our_info->past_background, our_info->affiliations);

  org_aff_changed = FALSE;
}

/* this function handles saving of all optionmenu+entry settings */
void menus_entries_save(gchar *prefix, gint count, gchar **data, gint offset, gint other)
{
  gint len_str, temp_val;
  gchar *tmp_entry;
  gchar *tmp_menu;
  
#ifdef TRACE_FUNCTION
  g_print( "menus_entries_save\n" );
#endif

  tmp_entry = g_strdup_printf("%s_entry%d", prefix, count);
  tmp_menu = g_strdup_printf("%s_optionmenu%d", prefix, count);

  len_str = strlen(gtk_entry_get_text(GTK_ENTRY(glade_xml_get_widget(our_info_xml, tmp_entry))));
	
  (*data) = g_malloc0(len_str+3);			/* three? */

  temp_val = gtk_option_menu_get_history(GTK_OPTION_MENU(glade_xml_get_widget (our_info_xml, tmp_menu))) + offset;

  temp_val = (temp_val == other) ? (100 + offset) : temp_val;		/* "Other" selection is special case */

  (*data)[1] = (temp_val & 0xff00) >> 8;
  (*data)[0] = (temp_val & 0x00ff);

  strncpy( (*data)+2, gtk_entry_get_text(GTK_ENTRY(glade_xml_get_widget(our_info_xml, tmp_entry))), len_str );

  g_free(tmp_entry);
  g_free(tmp_menu);
}	

void update_our_info ()
{
  gchar *string = NULL;
  struct tm birthday_tm;
  GList *email;
  GtkListStore *store;
  GtkTreeIter iter;
	
#ifdef TRACE_FUNCTION
  g_print( "update_our_info\n" );
#endif

  if (our_info_xml == NULL)
    return;

  if( our_info->nick != NULL ) {
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "nick_entry")), our_info->nick);
  }

  if( our_info->first != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "fname_entry")), our_info->first);

  if( our_info->last != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "lname_entry")), our_info->last);

  if( our_info->sex == MALE )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "sex_combo_entry")), _("Male"));
  else if( our_info->sex == FEMALE )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "sex_combo_entry")), _("Female"));
  else
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "sex_combo_entry")), _("Unknown"));

  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "age_entry")),
                      (string = our_info->age
                       ? g_strdup_printf( "%d", our_info->age % 1000 )
                       : g_strdup( _("Unknown") )));
  g_free( string );

  string = g_strdup (our_info->uin);
  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "uin_entry")), string);
  g_free (string);

  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "language1_combo_entry")), get_language_name (our_info->language1));
  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "language2_combo_entry")), get_language_name (our_info->language2));
  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "language3_combo_entry")), get_language_name (our_info->language3));

  if( our_info->email != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "email_entry")), our_info->email);

  gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( glade_xml_get_widget (our_info_xml, "hide_email_checkbutton" )), our_info->hide_email);

  if( our_info->phone != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "phone_entry")), our_info->phone);

  if( our_info->fax != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "fax_entry")), our_info->fax);

  if( our_info->homepage != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "homepage_entry")), our_info->homepage);

  if( our_info->about != NULL )
    {
      GtkTextView* text_view;
      GtkTextBuffer* text_buffer;

      text_view = GTK_TEXT_VIEW(glade_xml_get_widget (our_info_xml, "about_text"));
      text_buffer = gtk_text_view_get_buffer(text_view);
      gtk_widget_freeze_child_notify( GTK_WIDGET( glade_xml_get_widget (our_info_xml, "about_text") ) );
      gtk_text_buffer_set_text(text_buffer, "", -1);
      gtk_text_buffer_insert_at_cursor( gtk_text_view_get_buffer(GTK_TEXT_VIEW( glade_xml_get_widget (our_info_xml, "about_text") )), our_info->about, -1 );
      gtk_widget_thaw_child_notify( glade_xml_get_widget (our_info_xml, "about_text") ); 
    }

  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "occupation_combo_entry")), get_work_name (our_info->occupation));

  if( our_info->job_pos != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "work_position_entry")), our_info->job_pos);

  if( our_info->department != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "work_department_entry")), our_info->department);

  if( our_info->company_name != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "work_company_entry")), our_info->company_name);

  if( our_info->work_address != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "work_street_entry")), our_info->work_address);

  if( our_info->work_city != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "work_city_entry")), our_info->work_city);

  if( our_info->work_state != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "work_state_entry")), our_info->work_state);

  if( our_info->work_phone != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "work_phone_entry")), our_info->work_phone);

  if( our_info->work_fax != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "work_fax_entry")), our_info->work_fax);

  if( our_info->work_homepage != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "work_homepage_entry")), our_info->work_homepage);

  if( our_info->work_zip != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "work_zip_entry")), our_info->work_zip);
	
  if( our_info->work_country != 0 )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "work_country_combo_entry")), get_country_name (our_info->work_country));
	
  if( our_info->street != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "street_entry")), our_info->street);

  if( our_info->city != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "city_entry")), our_info->city);

  if( our_info->state != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "state_entry")), our_info->state);

  if( our_info->zip != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "zip_entry")), our_info->zip);

  if( our_info->country != 0 )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "country_combo_entry")), get_country_name (our_info->country));

  gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "timezone_combo_entry")), get_timezone_name (our_info->timezone));

  if( our_info->cellular != NULL )
    gtk_entry_set_text (GTK_ENTRY (glade_xml_get_widget (our_info_xml, "cellular_entry")), our_info->cellular);

  if (our_info->birth_year != 0 && 
      our_info->birth_month != 0 && 
      our_info->birth_day != 0)
    {

      memset(&birthday_tm, 0 ,sizeof(struct tm));
      birthday_tm.tm_mday = our_info->birth_day;
      birthday_tm.tm_mon = our_info->birth_month-1;
      birthday_tm.tm_year = our_info->birth_year-1900;

      gnome_date_edit_set_time(GNOME_DATE_EDIT(glade_xml_get_widget (our_info_xml, "birthday_dateedit")), mktime(&birthday_tm));

    }

	store = GTK_LIST_STORE( gtk_tree_view_get_model(GTK_TREE_VIEW( glade_xml_get_widget (our_info_xml, "emails_treeview") )) );
	gtk_list_store_clear(store);
	email =our_info->emails;
	while (email) {
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,
		                    EMAIL_COLUMN, email->data + 1,
		                    HIDDEN_COLUMN, *(gchar *)email->data,
		                    EDITABLE_COLUMN, TRUE,
		                    -1);
		email = g_list_next(email);
	}

  menus_entries_update("interest", our_info->interests, 4, 99, -1);
  menus_entries_update("org", our_info->affiliations, 3, 199, 20);
  menus_entries_update("affil", our_info->past_background, 3, 299, 8);

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(glade_xml_get_widget (our_info_xml, "web_aware" )), our_info->webpresence);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(glade_xml_get_widget (our_info_xml, "require_auth" )), our_info->auth);

  homeinfo_changed = FALSE;
  workinfo_changed = FALSE;
  moreinfo_changed = FALSE;
  about_changed    = FALSE;
  emails_changed   = FALSE;
  interests_changed= FALSE;
  org_aff_changed  = FALSE; 
}


/* updates all menu+entry settings */
void menus_entries_update(gchar *prefix, GList *list, gint number, gint offset, gint other)
{
  gint temp_val, counter = 1;
  gchar *tmp_entry;
  gchar *tmp_menu;
	
#ifdef TRACE_FUNCTION
  g_print( "menus_entries_update\n" );
#endif

  /* sets menus and entries */
  while((list != NULL) && (counter <= number)) {
    tmp_entry = g_strdup_printf("%s_entry%d", prefix, counter);
    tmp_menu = g_strdup_printf("%s_optionmenu%d", prefix, counter);

    temp_val = (* (WORD *) list->data) - offset;
				
    temp_val = ((temp_val > other) && (other != -1)) ? other : temp_val;		/* special case */
			
    gtk_option_menu_set_history(GTK_OPTION_MENU(glade_xml_get_widget (our_info_xml, tmp_menu)), temp_val);
    gtk_entry_set_text(GTK_ENTRY(glade_xml_get_widget(our_info_xml, tmp_entry)), list->data+2);

    /* insensitizes "Not selected" entries */
    gtk_widget_set_sensitive(glade_xml_get_widget(our_info_xml, tmp_entry), temp_val);
			
    counter++;
    list = g_list_next(list);

    g_free(tmp_entry);
    g_free(tmp_menu);
  }
	
  /* clears remaining settings */
  while(counter <= number) {
    tmp_entry = g_strdup_printf("%s_entry%d", prefix, counter);
    tmp_menu = g_strdup_printf("%s_optionmenu%d", prefix, counter);

    gtk_option_menu_set_history(GTK_OPTION_MENU(glade_xml_get_widget (our_info_xml, tmp_menu)), 0);
    gtk_entry_set_text(GTK_ENTRY(glade_xml_get_widget(our_info_xml, tmp_entry)), "");
    /* insensitizes "Not selected" entries */
    gtk_widget_set_sensitive(glade_xml_get_widget(our_info_xml, tmp_entry), FALSE);

    counter++;

    g_free(tmp_entry);
    g_free(tmp_menu);
  }
}


static void perms_save()
{
  
#ifdef TRACE_FUNCTION
  g_print( "perms_save\n" );
#endif

  our_info->auth = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(glade_xml_get_widget (our_info_xml, "require_auth" )));

  our_info->webpresence = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(glade_xml_get_widget (our_info_xml, "web_aware" )));

  v7_modify_permissions(mainconnection, our_info->auth, our_info->webpresence);

  perms_changed = FALSE;

  Save_RC();


}

void password_save()
{
  GtkWidget *entry, *entry_confirm;
  gchar *my_password;

  entry = glade_xml_get_widget (our_info_xml, "password_entry");
  entry_confirm = glade_xml_get_widget (our_info_xml, "password_confirm_entry");

  my_password = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));

  v7_modify_password(mainconnection, my_password);

  g_free (passwd);
  passwd = g_strdup (my_password);

  g_free (my_password);

  gtk_entry_set_text (GTK_ENTRY (entry), "");
  gtk_entry_set_text (GTK_ENTRY (entry_confirm), "");
}
