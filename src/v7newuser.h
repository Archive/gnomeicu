/****************************
  New user for ICQ v6-7 protocol (Oscar)
  Olivier Crete (c) 2001
  GnomeICU
*****************************/


#ifndef __V7NEWUSER_H__
#define __V7NEWUSER_H__

#include "v7base.h"

void v7_new_user(gchar *passwd);

#endif /* __V7NEWUSER_H__ */
