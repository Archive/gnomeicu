#ifndef __NEWMSG_H__
#define __NEWMSG_H__

#include "common.h"
#include <gtk/gtk.h>

void open_message_dlg_with_message (Contact_Member *contact,
                                    STORED_MESSAGE_PTR message_text);
void message_dialog_response_callback (GtkDialog *dialog, gint response, gpointer data);
gboolean on_message_dialog_key_press_event(GtkDialog *dlg,  GdkEventKey *event, gpointer data);
void show_contact_message (Contact_Member *contact );
gint message_input_key_callback (GtkWidget *widget, GdkEventKey *event);
void message_usermenu_clicked_callback (GtkWidget *widget, gpointer data);
void gnomeicu_text_buffer_insert_with_emoticons (GtkTextBuffer *text_buffer, 
                                                 GtkTextMark *mark,
                                                 const gchar *text);
void message_dialog_focus_callback(GtkWidget *widget, gpointer data);

#endif /* __NEWMSG_H__ */
