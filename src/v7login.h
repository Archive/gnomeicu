/****************************
  Login for ICQ v6-7 protocol (Oscar)
  Olivier Crete (c) 2001
  GnomeICU
*****************************/

#ifndef __V7LOGIN_H__
#define __V7LOGIN_H__

#include "v7base.h"

V7Connection *v7_new_login_session(void);

#endif /* __V7LOGIN_H__ */
