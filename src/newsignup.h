/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/**********************************
 Handle account creation (new or existing)
 (c) 1999 Jeremy Wise
 GnomeICU
***********************************/

#ifndef __NEWSIGNUP_H__
#define __NEWSIGNUP_H__

#include <gtk/gtk.h>

gint show_uin_pwd_dialog(void);
void new_sign_up(void);

#endif /* __NEWSIGNUP_H__ */
