/* gicustatusicon.h:
 *
 * Copyright (C) 2003 Sun Microsystems, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#ifndef __GICU_STATUS_ICON_H__
#define __GICU_STATUS_ICON_H__

#include <gtk/gtkimage.h>
#include <gtk/gtkmenu.h>

G_BEGIN_DECLS

#define GICU_TYPE_STATUS_ICON         (gicu_status_icon_get_type ())
#define GICU_STATUS_ICON(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), GICU_TYPE_STATUS_ICON, GicuStatusIcon))
#define GICU_STATUS_ICON_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST ((k), GICU_TYPE_STATUS_ICON, GicuStatusIconClass))
#define GICU_IS_STATUS_ICON(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), GICU_TYPE_STATUS_ICON))
#define GICU_IS_STATUS_ICON_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), GICU_TYPE_STATUS_ICON))
#define GICU_STATUS_ICON_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GICU_TYPE_STATUS_ICON, GicuStatusIconClass))

typedef struct _GicuStatusIcon	     GicuStatusIcon;
typedef struct _GicuStatusIconClass   GicuStatusIconClass;
typedef struct _GicuStatusIconPrivate GicuStatusIconPrivate;

struct _GicuStatusIcon
{
  GObject               parent_instance;

  GicuStatusIconPrivate *priv;
};

struct _GicuStatusIconClass
{
  GObjectClass parent_class;

  void     (* activate)     (GicuStatusIcon *status_icon);
  void     (* popup_menu)   (GicuStatusIcon *status_icon,
			     guint          button,
			     guint32        activate_time);
  gboolean (* size_changed) (GicuStatusIcon *status_icon,
			     gint           size);

  void (*__gtk_reserved1);
  void (*__gtk_reserved2);
  void (*__gtk_reserved3);
  void (*__gtk_reserved4);
  void (*__gtk_reserved5);
  void (*__gtk_reserved6);  
};

GType                 gicu_status_icon_get_type           (void) G_GNUC_CONST;

GicuStatusIcon        *gicu_status_icon_new                (void);
GicuStatusIcon        *gicu_status_icon_new_from_pixbuf    (GdkPixbuf          *pixbuf);
GicuStatusIcon        *gicu_status_icon_new_from_file      (const gchar        *filename);
GicuStatusIcon        *gicu_status_icon_new_from_stock     (const gchar        *stock_id);
void                  gicu_status_icon_set_from_pixbuf    (GicuStatusIcon      *status_icon,
							  GdkPixbuf          *pixbuf);
void                  gicu_status_icon_set_from_file      (GicuStatusIcon      *status_icon,
							  const gchar        *filename);
void                  gicu_status_icon_set_from_stock     (GicuStatusIcon      *status_icon,
							  const gchar        *stock_id);
GtkImageType          gicu_status_icon_get_storage_type   (GicuStatusIcon      *status_icon);

GdkPixbuf            *gicu_status_icon_get_pixbuf         (GicuStatusIcon      *status_icon);
G_CONST_RETURN gchar *gicu_status_icon_get_stock          (GicuStatusIcon      *status_icon);

gint                  gicu_status_icon_get_size           (GicuStatusIcon      *status_icon);

void                  gicu_status_icon_set_tooltip        (GicuStatusIcon      *status_icon,
							  const gchar        *tooltip_text);

void                  gicu_status_icon_set_visible        (GicuStatusIcon      *status_icon,
							  gboolean            visible);
gboolean              gicu_status_icon_get_visible        (GicuStatusIcon      *status_icon);

void                  gicu_status_icon_set_blinking       (GicuStatusIcon      *status_icon,
							  gboolean            blinking);
gboolean              gicu_status_icon_get_blinking       (GicuStatusIcon      *status_icon);

gboolean              gicu_status_icon_is_embedded        (GicuStatusIcon      *status_icon);

void                  gicu_status_icon_position_menu      (GtkMenu            *menu,
							  gint               *x,
							  gint               *y,
							  gboolean           *push_in,
							  gpointer            user_data);
gboolean              gicu_status_icon_get_geometry       (GicuStatusIcon      *status_icon,
							  GdkScreen         **screen,
							  GdkRectangle       *area,
							  GtkOrientation     *orientation);

G_END_DECLS

#endif /* __GICU_STATUS_ICON_H__ */
