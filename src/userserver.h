#ifndef __USERSERVER_H__
#define __USERSERVER_H__

typedef enum {
	USERSERVER_CMD_NOOP = 0,
	USERSERVER_CMD_DO  = 1,
	USERSERVER_CMD_READMSG = 2,
	USERSERVER_CMD_MSGCOUNT = 3,
        USERSERVER_CMD_GETSTATUS = 4,
        USERSERVER_ICUKRELL_ASK = 9,
} UServCmd;

typedef unsigned long UServCmdLen;

void userserver_init( void );
void userserver_kill( void );

#endif /* __USERSERVER_H__ */
