#include "common.h"
#include "gnomeicu.h"
#include "groups.h"
#include "group_popup.h"
#include "gtkfunc.h"
#include "icons.h"
#include "v7snac13.h"

#include <gtk/gtk.h>

static void group_popup_rename ( GtkWidget *widget, GroupInfo *ginfo );
static void group_popup_remove ( GtkWidget *widget, GroupInfo *ginfo );
static void rename_group_response (GtkDialog *dialog, gint response, GroupInfo *ginfo);

GtkWidget *group_popup (gint gid)
{
	GtkWidget *group_menu;
	GtkWidget *item, *image;
	GSList *ginfo;
	GSList *contact;
	gboolean empty = TRUE;

#ifdef TRACE_FUNCTION
	g_print( "group_popup\n" );
#endif

	for(ginfo = Groups; ginfo != NULL; ginfo = ginfo->next)
	  if (((GroupInfo*)ginfo->data)->gid == gid) break;

	g_assert(ginfo != NULL);

	for (contact = Contacts; contact ; contact = contact->next) 
	  if (kontakt->gid == gid) {
	    empty = FALSE;
	    break;
	  }


	group_menu = gtk_menu_new ();

	item = gtk_image_menu_item_new_with_mnemonic (_("Re_name Group..."));
	image = gtk_image_new_from_pixbuf (icon_rename_pixbuf);
	gtk_container_add (GTK_CONTAINER (group_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (group_popup_rename),
	                  ginfo->data);

	item = gtk_image_menu_item_new_with_mnemonic (_("Re_move Group"));
	image = gtk_image_new_from_pixbuf (icon_cancel_pixbuf);
	gtk_container_add (GTK_CONTAINER (group_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_widget_set_sensitive(item, Current_Status != STATUS_OFFLINE &&
				empty);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (group_popup_remove),
	                  ginfo->data);


	gtk_widget_show_all (group_menu);

	return group_menu;
}


static void group_popup_remove ( GtkWidget *widget, GroupInfo *ginfo )
{

#ifdef TRACE_FUNCTION
	g_print( "group_popup_remove\n" );
#endif

  v7_delete_empty_group (mainconnection, ginfo->gid);

  /* update group structure */
  groups_remove (ginfo->gid);

  /* finalize the delete */
  v7_finalize_delete_group (mainconnection);

}

static void group_popup_rename ( GtkWidget *widget, GroupInfo *ginfo )
{

	GtkWidget *dialog;
	GtkWidget *vbox;
	GtkWidget *label;
	GtkWidget *entry;
	gchar *str;

#ifdef TRACE_FUNCTION
	g_print( "group_popup_rename\n" );
#endif


	dialog = gtk_dialog_new_with_buttons (_("Rename Group"),
	                                          GTK_WINDOW (MainData->window),
	                                          GTK_DIALOG_DESTROY_WITH_PARENT,
	                                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	                                          _("_Rename"), GTK_RESPONSE_OK,
	                                          NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

	gtk_container_set_border_width (GTK_CONTAINER (dialog), 6);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 6);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), vbox, FALSE, FALSE, 0);

	str = g_strdup_printf (_("_Enter a new name for for group %s:"),
	                       ginfo->name);
	label = gtk_label_new_with_mnemonic (str);
	g_free (str);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);

	entry = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (entry), ginfo->name);
	gtk_entry_set_max_length (GTK_ENTRY (entry), 20);
	gtk_box_pack_start (GTK_BOX (vbox), entry, FALSE, FALSE, 0);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), entry);

	g_signal_connect_swapped (G_OBJECT (entry), "activate",
	                          G_CALLBACK (gtk_window_activate_default),
	                          dialog);
	
	g_object_set_data (G_OBJECT (dialog), "entry", entry);
	g_signal_connect (G_OBJECT (dialog), "response",
	                  G_CALLBACK (rename_group_response),
	                  ginfo);

	gtk_window_set_focus (GTK_WINDOW (dialog), entry);
	gtk_widget_show_all (dialog);
}


void rename_group_response (GtkDialog *dialog, gint response, GroupInfo *ginfo)
{
#ifdef TRACE_FUNCTION
  g_print( "rename_group_response\n" );
#endif

  if (response == GTK_RESPONSE_OK) {
    GtkWidget *entry;

    if (!is_connected(GTK_WINDOW(dialog), _("You can not change a nickname while disconnected.")))
      return;

    entry = g_object_get_data (G_OBJECT (dialog), "entry");

    groups_rename_by_gid (ginfo->gid, gtk_entry_get_text (GTK_ENTRY (entry)));
		
    v7_update_group_info (mainconnection, ginfo->gid);

  }
	
  gtk_widget_destroy (GTK_WIDGET (dialog));
}
