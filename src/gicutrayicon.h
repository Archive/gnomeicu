/* gicutrayicon.h
 * Copyright (C) 2002 Anders Carlsson <andersca@gnu.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GICU_TRAY_ICON_H__
#define __GICU_TRAY_ICON_H__

#include <gtk/gtkplug.h>

G_BEGIN_DECLS

#define GICU_TYPE_TRAY_ICON		(gicu_tray_icon_get_type ())
#define GICU_TRAY_ICON(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GICU_TYPE_TRAY_ICON, GicuTrayIcon))
#define GICU_TRAY_ICON_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GICU_TYPE_TRAY_ICON, GicuTrayIconClass))
#define GICU_IS_TRAY_ICON(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GICU_TYPE_TRAY_ICON))
#define GICU_IS_TRAY_ICON_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GICU_TYPE_TRAY_ICON))
#define GICU_TRAY_ICON_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GICU_TYPE_TRAY_ICON, GicuTrayIconClass))
	
typedef struct _GicuTrayIcon	   GicuTrayIcon;
typedef struct _GicuTrayIconPrivate GicuTrayIconPrivate;
typedef struct _GicuTrayIconClass   GicuTrayIconClass;

struct _GicuTrayIcon
{
  GtkPlug parent_instance;

  GicuTrayIconPrivate *priv;
};

struct _GicuTrayIconClass
{
  GtkPlugClass parent_class;

  void (*__gtk_reserved1);
  void (*__gtk_reserved2);
  void (*__gtk_reserved3);
  void (*__gtk_reserved4);
  void (*__gtk_reserved5);
  void (*__gtk_reserved6);
};

GType          gicu_tray_icon_get_type         (void) G_GNUC_CONST;

GicuTrayIcon   *_gicu_tray_icon_new_for_screen  (GdkScreen   *screen,
					       const gchar *name);

GicuTrayIcon   *_gicu_tray_icon_new             (const gchar *name);

guint          _gicu_tray_icon_send_message    (GicuTrayIcon *icon,
					       gint         timeout,
					       const gchar *message,
					       gint         len);
void           _gicu_tray_icon_cancel_message  (GicuTrayIcon *icon,
					       guint        id);

GtkOrientation _gicu_tray_icon_get_orientation (GicuTrayIcon *icon);
					    
G_END_DECLS

#endif /* __GICU_TRAY_ICON_H__ */
