/***************************
 TCP peer to peer functions
 (c) 1999 Jeremy Wise
 GnomeICU
****************************/

#ifndef __TCP_H__
#define __TCP_H__

#include "datatype.h"
#include "filexfer.h"

#define ICQ_CMDxTCP_START             0x07EE
#define ICQ_CMDxTCP_CANCEL            0x07D0
#define ICQ_CMDxTCP_ACK               0x07DA
#define ICQ_CMDxTCP_MSG               0x0001
#define ICQ_CMDxTCP_FILE              0x0003
#define ICQ_CMDxTCP_CHAT              0x0002
#define ICQ_CMDxTCP_URL               0x0004
#define ICQ_CMDxTCP_CONT_LIST         0x0013
#define ICQ_CMDxTCP_VERSION           0x0099
#define ICQ_CMDxTCP_READxAWAYxMSG     0x03E8
#define ICQ_CMDxTCP_READxOCCxMSG      0x03E9
#define ICQ_CMDxTCP_READxNAxMSG       0x03EA
#define ICQ_CMDxTCP_READxDNDxMSG      0x03EB
#define ICQ_CMDxTCP_HANDSHAKE         0x03FF
#define ICQ_CMDxTCP_HANDSHAKE2        0x04FF
#define ICQ_CMDxTCP_HANDSHAKE3        0x02FF

#define ICQ_ACKxTCP_ONLINE            0x0000
#define ICQ_ACKxTCP_AWAY              0x0004
#define ICQ_ACKxTCP_NA                0x000E
#define ICQ_ACKxTCP_DND               0x000A
#define ICQ_ACKxTCP_OCC               0x0009
#define ICQ_ACKxTCP_REFUSE            0x0001

#define FONT_PLAIN                    0x00000000
#define FONT_BOLD                     0x00000001
#define FONT_ITALICS                  0x00000002
#define FONT_UNDERLINE                0x00000004

extern const DWORD LOCALHOST;

/* All TCP packets will have this information */
typedef struct
{
	guint seq;
	gchar *text;
	BYTE *data;
	UIN_T uin;
	gint timeout;
	gboolean sent;
	GtkWidget *dialog;
} tcp_message;

int TCPAcceptIncoming( GIOChannel *iochan, GIOCondition cond, gpointer data);
int TCPRetrieveAwayMessage( GSList *contact, gpointer data );

int TCPAcceptFile( XferInfo *xfer );
int TCPRefuseFile( XferInfo *xfer );
int TCPSendFileRequest( UIN_T uin, const gchar *msg, GSList *files );

void TCPSendHandShake(Contact_Member *contact, DWORD sessionid, int socket);
void TCPSendHandShakeAck(int socket);

#endif /* __TCP_H__ */
