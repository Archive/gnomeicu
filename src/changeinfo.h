/****************************************
  Header file for changing personal info
  (c) 1999 Jeremy Wise
  GnomeICU
*****************************************/

#ifndef __CHANGEINFO_H__
#define __CHANGEINFO_H__

#include <gtk/gtkwidget.h>

void change_info_window( GtkWidget *widget, gpointer data );
void update_our_info (void);

#endif /* __CHANGEINFO_H__ */
