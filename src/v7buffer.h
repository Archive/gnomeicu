/************************************************
  Simple Buffer with count check implementation
  Olivier Crete (c) 2007
  GnomeICU
*************************************************/
#ifndef __V7_BUFFER_H__
#define __V7_BUFFER_H__

#include <glib.h>

#include "util.h"

typedef struct _V7Buffer {
  gchar *buf;
  gchar *here;
  int len;
} V7Buffer;;

V7Buffer *v7_buffer_new(gchar *data, int len, gboolean owned);
void v7_buffer_free(V7Buffer *buf);
guchar *v7_buffer_get(V7Buffer *buf, int len);

#define _v7_buffer_get_check(buf, len, error_target, converter) ( {  \
  gchar *mybuf = v7_buffer_get((buf), (len));                       \
  if (!mybuf)                                                       \
    goto error_target;                                              \
  converter(mybuf); } )


#define v7_buffer_get_c(buf, error_target) _v7_buffer_get_check((buf), \
    1, error_target, Chars_2_Byte)
#define v7_buffer_get_w_le(buf, error_target) _v7_buffer_get_check((buf), \
    2, error_target, Chars_2_Word)
#define v7_buffer_get_w_be(buf, error_target) _v7_buffer_get_check((buf), \
    2, error_target, CharsBE_2_Word)
#define v7_buffer_get_dw_le(buf, error_target) _v7_buffer_get_check((buf), \
    4, error_target, Chars_2_DW)
#define v7_buffer_get_dw_be(buf, error_target) _v7_buffer_get_check((buf), \
    4, error_target, CharsBE_2_DW)

#define v7_buffer_skip(buf, len, error_target)  { \
  if (!v7_buffer_get ((buf), (len))) \
    goto error_target; \
  }; \

#define v7_buffer_get_v7buf(buf, len, error_target) ( {  \
  gchar *mybuf = v7_buffer_get((buf), (len));            \
  if (!mybuf)                                            \
    goto error_target;                                   \
  v7_buffer_new(mybuf, (len), FALSE); } )

#define v7_buffer_get_string(buf, len, error_target) ( {  \
  gchar *mybuf = v7_buffer_get((buf), (len));             \
  if (!mybuf)                                             \
    goto error_target;                                    \
  g_strndup(mybuf, len); } )


#define v7_buffer_get_uin(buf, error_target) ( {    \
  gchar len = v7_buffer_get_c((buf), error_target); \
  v7_buffer_get_string(buf, len, error_target); } )

#endif /* __V7_BUFFER_H__ */
