/*******************************
 General utility functions
 (c) 2002 Jeremy Wise
 GnomeICU
********************************/

#include "common.h"
#include "gnomecfg.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "icons.h"
#include "newsignup.h"
#include "util.h"
#include "v7send.h"

#include <libgnome/gnome-config.h>

#include <string.h>

typedef struct
{
  const char *name;
  WORD code;
} FIELD_CODE;

static FIELD_CODE Country_Codes[] = {
  { N_("Afghanistan"), 93 },
  { N_("Albania"), 355 },
  { N_("Algeria"), 213 },
  { N_("American Samoa"), 684 },
  { N_("Andorra"), 376 },
  { N_("Angola"), 244 },
  { N_("Anguilla"), 101 },
  { N_("Antigua"), 102 },
  { N_("Argentina"), 54 },
  { N_("Armenia"), 374 },
  { N_("Aruba"), 297 },
  { N_("Ascension Island"), 247 },
  { N_("Australia"), 61 },
  { N_("Australian Antarctic Territory"), 6721 },
  { N_("Austria"), 43 },
  { N_("Azerbaijan"), 994 },
  { N_("Bahamas"), 103 },
  { N_("Bahrain"), 973 },
  { N_("Bangladesh"), 880 },
  { N_("Barbados"), 104 },
  { N_("Barbuda"), 120 },
  { N_("Belarus"), 375 },
  { N_("Belgium"), 32 },
  { N_("Belize"), 501 },
  { N_("Benin"), 229 },
  { N_("Bermuda"), 105 },
  { N_("Bhutan"), 975 },
  { N_("Bolivia"), 591 },
  { N_("Bosnia and Herzegovina"), 387 },
  { N_("Botswana"), 267 },
  { N_("Brazil"), 55 },
  { N_("British Virgin Islands"), 106 },
  { N_("Brunei"), 673 },
  { N_("Bulgaria"), 359 },
  { N_("Burkina Faso"), 226 },
  { N_("Burundi"), 257 },
  { N_("Cambodia"), 855 },
  { N_("Cameroon"), 237 },
  { N_("Canada"), 107 },
  { N_("Cape Verde Islands"), 238 },
  { N_("Cayman Islands"), 108 },
  { N_("Central African Republic"), 236 },
  { N_("Chad"), 235 },
  { N_("Chile"), 56 },
  { N_("China"), 86 },
  { N_("Christmas Island"), 672 },
  { N_("Cocos-Keeling Islands"), 6101 },
  { N_("Colombia"), 57 },
  { N_("Comoros"), 2691 },
  { N_("Congo"), 242 },
  { N_("Cook Islands"), 682 },
  { N_("Costa Rica"), 506 },
  { N_("Croatia"), 385 },
  { N_("Cuba"), 53 },
  { N_("Cyprus"), 357 },
  { N_("Czech Republic"), 42 },
  { N_("Dem. Rep. of the Congo"), 243 },
  { N_("Denmark"), 45 },
  { N_("Diego Garcia"), 246 },
  { N_("Djibouti"), 253 },
  { N_("Dominica"), 109 },
  { N_("Dominican Republic"), 110 },
  { N_("Ecuador"), 593 },
  { N_("Egypt"), 20 },
  { N_("El Salvador"), 503 },
  { N_("Equatorial Guinea"), 240 },
  { N_("Eritrea"), 291 },
  { N_("Estonia"), 372 },
  { N_("Ethiopia"), 251 },
  { N_("F.Y.R.O.M. (Former Yugoslav Republic of Macedonia)"), 389 },
  { N_("Faeroe Islands"), 298 },
  { N_("Falkland Islands"), 500 },
  { N_("Fiji Islands"), 679 },
  { N_("Finland"), 358 },
  { N_("France"), 33 },
  { N_("French Antilles"), 5901 },
  { N_("French Guiana"), 594 },
  { N_("French Polynesia"), 689 },
  { N_("Gabon"), 241 },
  { N_("Gambia"), 220 },
  { N_("Georgia"), 995 },
  { N_("Germany"), 49 },
  { N_("Ghana"), 233 },
  { N_("Gibraltar"), 350 },
  { N_("Greece"), 30 },
  { N_("Greenland"), 299 },
  { N_("Grenada"), 111 },
  { N_("Guadeloupe"), 590 },
  { N_("Guam"), 671 },
  { N_("Guantanamo Bay"), 5399 },
  { N_("Guatemala"), 502 },
  { N_("Guinea"), 224 },
  { N_("Guinea-Bissau"), 245 },
  { N_("Guyana"), 592 },
  { N_("Haiti"), 509 },
  { N_("Honduras"), 504 },
  { N_("Hong Kong"), 852 },
  { N_("Hungary"), 36 },
  { N_("INMARSAT (Atlantic-East)"), 871 },
  { N_("INMARSAT (Atlantic-West)"), 874 },
  { N_("INMARSAT (Indian)"), 873 },
  { N_("INMARSAT (Pacific)"), 872 },
  { N_("INMARSAT"), 870 },
  { N_("Iceland"), 354 },
  { N_("India"), 91 },
  { N_("Indonesia"), 62 },
  { N_("International Freephone Service"), 800 },
  { N_("Iran"), 98 },
  { N_("Iraq"), 964 },
  { N_("Ireland"), 353 },
  { N_("Israel"), 972 },
  { N_("Italy"), 39 },
  { N_("Ivory Coast"), 225 },
  { N_("Jamaica"), 112 },
  { N_("Japan"), 81 },
  { N_("Jordan"), 962 },
  { N_("Kazakhstan"), 705 },
  { N_("Kenya"), 254 },
  { N_("Kiribati Republic"), 686 },
  { N_("Korea (North)"), 850 },
  { N_("Korea (Republic of)"), 82 },
  { N_("Kuwait"), 965 },
  { N_("Kyrgyz Republic"), 706 },
  { N_("Laos"), 856 },
  { N_("Latvia"), 371 },
  { N_("Lebanon"), 961 },
  { N_("Lesotho"), 266 },
  { N_("Liberia"), 231 },
  { N_("Libya"), 218 },
  { N_("Liechtenstein"), 4101 },
  { N_("Lithuania"), 370 },
  { N_("Luxembourg"), 352 },
  { N_("Macau"), 853 },
  { N_("Madagascar"), 261 },
  { N_("Malawi"), 265 },
  { N_("Malaysia"), 60 },
  { N_("Maldives"), 960 },
  { N_("Mali"), 223 },
  { N_("Malta"), 356 },
  { N_("Marshall Islands"), 692 },
  { N_("Martinique"), 596 },
  { N_("Mauritania"), 222 },
  { N_("Mauritius"), 230 },
  { N_("Mayotte Island"), 269 },
  { N_("Mexico"), 52 },
  { N_("Micronesia, Federated States of"), 691 },
  { N_("Moldova"), 373 },
  { N_("Monaco"), 377 },
  { N_("Mongolia"), 976 },
  { N_("Montserrat"), 113 },
  { N_("Morocco"), 212 },
  { N_("Mozambique"), 258 },
  { N_("Myanmar"), 95 },
  { N_("Namibia"), 264 },
  { N_("Nauru"), 674 },
  { N_("Nepal"), 977 },
  { N_("Netherlands Antilles"), 599 },
  { N_("Netherlands"), 31 },
  { N_("Nevis"), 114 },
  { N_("New Caledonia"), 687 },
  { N_("New Zealand"), 64 },
  { N_("Nicaragua"), 505 },
  { N_("Niger"), 227 },
  { N_("Nigeria"), 234 },
  { N_("Niue"), 683 },
  { N_("Norfolk Island"), 6722 },
  { N_("Norway"), 47 },
  { N_("Oman"), 968 },
  { N_("Pakistan"), 92 },
  { N_("Palau"), 680 },
  { N_("Panama"), 507 },
  { N_("Papua New Guinea"), 675 },
  { N_("Paraguay"), 595 },
  { N_("Peru"), 51 },
  { N_("Philippines"), 63 },
  { N_("Poland"), 48 },
  { N_("Portugal"), 351 },
  { N_("Puerto Rico"), 121 },
  { N_("Qatar"), 974 },
  { N_("Reunion Island"), 262 },
  { N_("Romania"), 40 },
  { N_("Rota Island"), 6701 },
  { N_("Russia"), 7 },
  { N_("Rwanda"), 250 },
  { N_("Saint Lucia"), 122 },
  { N_("Saipan Island"), 670 },
  { N_("San Marino"), 378 },
  { N_("Sao Tome and Principe"), 239 },
  { N_("Saudi Arabia"), 966 },
  { N_("Senegal Republic"), 221 },
  { N_("Seychelle Islands"), 248 },
  { N_("Sierra Leone"), 232 },
  { N_("Singapore"), 65 },
  { N_("Slovak Republic"), 4201 },
  { N_("Slovenia"), 386 },
  { N_("Solomon Islands"), 677 },
  { N_("Somalia"), 252 },
  { N_("South Africa"), 27 },
  { N_("Spain"), 34 },
  { N_("Sri Lanka"), 94 },
  { N_("St. Helena"), 290 },
  { N_("St. Kitts"), 115 },
  { N_("St. Pierre and Miquelon"), 508 },
  { N_("St. Vincent and the Grenadines"), 116 },
  { N_("Sudan"), 249 },
  { N_("Suriname"), 597 },
  { N_("Swaziland"), 268 },
  { N_("Sweden"), 46 },
  { N_("Switzerland"), 41 },
  { N_("Syria"), 963 },
  { N_("Taiwan, Republic of China"), 886 },
  { N_("Tajikistan"), 708 },
  { N_("Tanzania"), 255 },
  { N_("Thailand"), 66 },
  { N_("Tinian Island"), 6702 },
  { N_("Togo"), 228 },
  { N_("Tokelau"), 690 },
  { N_("Tonga"), 676 },
  { N_("Trinidad and Tobago"), 117 },
  { N_("Tunisia"), 216 },
  { N_("Turkey"), 90 },
  { N_("Turkmenistan"), 709 },
  { N_("Turks and Caicos Islands"), 118 },
  { N_("Tuvalu"), 688 },
  { N_("USA"), 1 },
  { N_("Uganda"), 256 },
  { N_("Ukraine"), 380 },
  { N_("United Arab Emirates"), 971 },
  { N_("United Kingdom"), 44 },
  { N_("United States Virgin Islands"), 123 },
  { N_("Uruguay"), 598 },
  { N_("Uzbekistan"), 711 },
  { N_("Vanuatu"), 678 },
  { N_("Vatican City"), 379 },
  { N_("Venezuela"), 58 },
  { N_("Vietnam"), 84 },
  { N_("Wallis and Futuna Islands"), 681 },
  { N_("Western Samoa"), 685 },
  { N_("Yemen"), 967 },
  { N_("Yugoslavia"), 381 },
  { N_("Zambia"), 260 },
  { N_("Zimbabwe"), 263 },
  { N_("Not entered"), 0 },
  { N_("Unknown"), 0xffff } 
};

/* the code is equivalent to GMT offset */
static FIELD_CODE time_zones[] =
{
  /* { N_("UTC+1300 (Tonga)"), 1300 }, Unused */
{ N_("UTC+1200 (Fiji)"), -24 & 0xff },
{ N_("UTC+1130"), -23 & 0xff },
{ N_("UTC+1100 (Solomon Islands)"), -22 & 0xff },
{ N_("UTC+1030"), -21 & 0xff },
{ N_("UTC+1000 (Guam)"), -20 & 0xff },
{ N_("UTC+0930"), -19 & 0xff },
{ N_("UTC+0900 (Japan)"), -18 & 0xff },
{ N_("UTC+0830"), -17 & 0xff },
{ N_("UTC+0800 (China/Taiwan)"), -16 & 0xff },
{ N_("UTC+0730"), -15 & 0xff },
{ N_("UTC+0700 (Cambodia)"), -14 & 0xff },
{ N_("UTC+0630"), -13 & 0xff },
{ N_("UTC+0600 (Bangladesh)"), -12 & 0xff },
{ N_("UTC+0530"), -11 & 0xff },
{ N_("UTC+0500 (Turkmenistan)"), -10 & 0xff },
{ N_("UTC+0430"), -9 & 0xff },
{ N_("UTC+0400 (Yemen/Oman/Armenia)"), -8 & 0xff },
{ N_("UTC+0330"), -7 & 0xff },
{ N_("UTC+0300 (Eastern African)"), -6 & 0xff },
{ N_("UTC+0230"), -5 & 0xff },
{ N_("UTC+0200 (Eastern European)"), -4 & 0xff },
{ N_("UTC+0130"), -3 & 0xff },
{ N_("UTC+0100 (Middle European)"), -2 & 0xff },
{ N_("UTC+0030"), -1 & 0xff },
{ N_("UTC"), 0 },
{ N_("UTC-0030"), 1 & 0xff },
{ N_("UTC-0100"), 2 & 0xff },
{ N_("UTC-0130"), 3 & 0xff },
{ N_("UTC-0200 (Central African)"), 4 & 0xff },
{ N_("UTC-0230"), 5 & 0xff },
{ N_("UTC-0300 (Brazil)"), 6 & 0xff },
{ N_("UTC-0330"), 7 & 0xff },
{ N_("UTC-0400 (Atlantic)"), 8 & 0xff },
{ N_("UTC-0430"), 9 & 0xff },
{ N_("UTC-0500 (Eastern)"), 10 & 0xff },
{ N_("UTC-0530"), 11 & 0xff },
{ N_("UTC-0600 (Central)"), 12 & 0xff },
{ N_("UTC-0630"), 13 & 0xff },
{ N_("UTC-0700 (Mountain)"), 14 & 0xff },
{ N_("UTC-0730"), 15 & 0xff },
{ N_("UTC-0800 (Pacific)"), 16 & 0xff },
{ N_("UTC-0830"), 17 & 0xff },
{ N_("UTC-0900 (Alaska)"), 18 & 0xff },
{ N_("UTC-0930"), 19 & 0xff },
{ N_("UTC-1000 (Hawaii/Aleutian Islands)"), 20 & 0xff },
{ N_("UTC-1030"), 21 & 0xff },
{ N_("UTC-1100 (American Samoa)"), 22 & 0xff },
{ N_("UTC-1130"), 23 & 0xff },
{ N_("UTC-1200"), 24 & 0xff },
{ N_("UTC-1230"), 25 & 0xff },
{ N_("Unknown"), 100 & 0xff }
};

static FIELD_CODE interest_codes[] =
{
	{ N_("Art"), 100 },
	{ N_("Cars"), 101 },
	{ N_("Celebrity Fans"), 102 },
	{ N_("Collections"), 103 },
	{ N_("Computers"), 104 },
	{ N_("Culture & Literature"), 105 },
	{ N_("Fitness"), 106 },
	{ N_("Games"), 107 },
	{ N_("Hobbies"), 108 },
	{ N_("ICQ - Providing Help"), 109 },
	{ N_("Internet"), 110 },
	{ N_("Lifestyle"), 111 },
	{ N_("Movies/TV"), 112 },
	{ N_("Music"), 113 },
	{ N_("Outdoor Activities"), 114 },
	{ N_("Parenting"), 115 },
	{ N_("Pets/Animals"), 116 },
	{ N_("Religion"), 117 },
	{ N_("Science/Technology"), 118 },
	{ N_("Skills"), 119 },
	{ N_("Sports"), 120 },
	{ N_("Web Design"), 121 },
	{ N_("Nature and Environment"), 122 },
	{ N_("News & Media"), 123 },
	{ N_("Government"), 124 },
	{ N_("Business & Economy"), 125 },
	{ N_("Mystics"), 126 },
	{ N_("Travel"), 127 },
	{ N_("Astronomy"), 128 },
	{ N_("Space"), 129 },
	{ N_("Clothing"), 130 },
	{ N_("Parties"), 131 },
	{ N_("Women"), 132 },
	{ N_("Social science"), 133 },
	{ N_("60's"), 134 },
	{ N_("70's"), 135 },
	{ N_("80's"), 136 },
	{ N_("50's"), 137 },
	{ N_("Finance and Corporate"), 138 },
	{ N_("Entertainment"), 139 },
	{ N_("Consumer Electronics"), 140 },
	{ N_("Retail Stores"), 141 },
	{ N_("Health and Beauty"), 142 },
	{ N_("Media"), 143 },
	{ N_("Household Products"), 144 },
	{ N_("Mail Order Catalog"), 145 },
	{ N_("Business Services"), 146 },
	{ N_("Audio and Visual"), 147 },
	{ N_("Sporting and Athletic"), 148 },
	{ N_("Publishing"), 149 },
	{ N_("Home Automation"), 150 },
	{ N_("Unknown"), 0xff } 
};

static FIELD_CODE language_codes[] =
{
	{ N_("Arabic"), 1 },
	{ N_("Bhojpuri"), 2 },
	{ N_("Bulgarian"), 3 },
	{ N_("Burmese"), 4 },
	{ N_("Cantonese"), 5 },
	{ N_("Catalan"), 6 },
	{ N_("Chinese"), 7 },
	{ N_("Croatian"), 8 },
	{ N_("Czech"), 9 },
	{ N_("Danish"), 10 },
	{ N_("Dutch"), 11 },
	{ N_("English"), 12 },
	{ N_("Esperanto"), 13 },
	{ N_("Estonian"), 14 },
	{ N_("Farci"), 15 },
	{ N_("Finnish"), 16 },
	{ N_("French"), 17 },
	{ N_("Gaelic"), 18 },
	{ N_("German"), 19 },
	{ N_("Greek"), 20 },
	{ N_("Hebrew"), 21 },
	{ N_("Hindi"), 22 },
	{ N_("Hungarian"), 23 },
	{ N_("Icelandic"), 24 },
	{ N_("Indonesian"), 25 },
	{ N_("Italian"), 26 },
	{ N_("Japanese"), 27 },
	{ N_("Khmer"), 28 },
	{ N_("Korean"), 29 },
	{ N_("Lao"), 30 },
	{ N_("Latvian"), 31 },
	{ N_("Lithuanian"), 32 },
	{ N_("Malay"), 33 },
	{ N_("Norwegian"), 34 },
	{ N_("Polish"), 35 },
	{ N_("Portuguese"), 36 },
	{ N_("Romanian"), 37 },
	{ N_("Russian"), 38 },
	{ N_("Serbo-Croatian"), 39 },
	{ N_("Slovak"), 40 },
	{ N_("Slovenian"), 41 },
	{ N_("Somali"), 42 },
	{ N_("Spanish"), 43 },
	{ N_("Swahili"), 44 },
	{ N_("Swedish"), 45 },
	{ N_("Tagalog"), 46 },
	{ N_("Tatar"), 47 },
	{ N_("Thai"), 48 },
	{ N_("Turkish"), 49 },
	{ N_("Ukrainian"), 50 },
	{ N_("Urdu"), 51 },
	{ N_("Vietnamese"), 52 },
	{ N_("Yiddish"), 53 },
	{ N_("Yoruba"), 54 },
	{ N_("Afrikaans"), 55 },
	{ N_("Bosnian"), 56 },
	{ N_("Persian"), 57 },
	{ N_("Albanian"), 58 },
	{ N_("Not entered"), 0 },
	{ N_("Unknown"), 0xff } 
};

static FIELD_CODE past_codes[] =
{
	{ N_("Elementary School"), 300 },
	{ N_("High School"), 301 },
	{ N_("College"), 302 },
	{ N_("University"), 303 },
	{ N_("Military"), 304 },
	{ N_("Past Work Place"), 305 },
	{ N_("Past Organization"), 306 },
	{ N_("Other"), 399 },
	{ N_("Unknown"), 0xffff } 
};

static FIELD_CODE work_codes[] =
{
	{ N_("Academic"), 1 },
	{ N_("Administrative"), 2 },
	{ N_("Art/Entertainment"), 3 },
	{ N_("College Student"), 4 },
	{ N_("Computers"), 5 },
	{ N_("Community & Social"), 6 },
	{ N_("Education"), 7 },
	{ N_("Engineering"), 8 },
	{ N_("Financial Services"), 9 },
	{ N_("Government"), 10 },
	{ N_("High School Student"), 11 },
	{ N_("Home"), 12 },
	{ N_("ICQ - Providing Help"), 13 },
	{ N_("Law"), 14 },
	{ N_("Managerial"), 15 },
	{ N_("Manufacturing"), 16 },
	{ N_("Medical/Health"), 17 },
	{ N_("Military"), 18 },
	{ N_("Non-Government Organization"), 19 },
	{ N_("Professional"), 20 },
	{ N_("Retail"), 21 },
	{ N_("Retired"), 22 },
	{ N_("Science & Research"), 23 },
	{ N_("Sports"), 24 },
	{ N_("Technical"), 25 },
	{ N_("University Student"), 26 },
	{ N_("Web Building"), 27 },
	{ N_("Other Services"), 99 },
	{ N_("Unknown"), 0xffff } 
};

static FIELD_CODE affiliation_codes[] =
{
	{ N_("Alumni Org."), 200 },
	{ N_("Charity Org."), 201 },
	{ N_("Club/Social Org."), 202 },
	{ N_("Community Org."), 203 },
	{ N_("Cultural Org."), 204 },
	{ N_("Fan Clubs"), 205 },
	{ N_("Fraternity/Sorority"), 206 },
	{ N_("Hobbyists Org."), 207 },
	{ N_("International Org."), 208 },
	{ N_("Nature and Environment Org."), 209 },
	{ N_("Professional Org."), 210 },
	{ N_("Scientific/Technical Org."), 211 },
	{ N_("Self Improvement Group"), 212 },
	{ N_("Spiritual/Religious Org."), 213 },
	{ N_("Sports Org."), 214 },
	{ N_("Support Org."), 215 },
	{ N_("Trade and Business Org."), 216 },
	{ N_("Union"), 217 },
	{ N_("Voluntary Org."), 218 },
	{ N_("Other"), 299 },
	{ N_("Unknown"), 0xffff } 
};

/* Conversion tables for PC characters */

#define SP ' '

unsigned char iso1_to_pc[256]={
/*   0 */ SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,
/*  10 */ SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,
/*  20 */ SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,
/*  30 */ SP ,SP ,SP ,'!','\"','#','$','%','&','\'',
/*  40 */ '(',')','*','+',',','-','.','/','0','1',
/*  50 */ '2','3','4','5','6','7','8','9',':',';',
/*  60 */ '<','=','>','?','@','A','B','C','D','E',
/*  70 */ 'F','G','H','I','J','K','L','M','N','O',
/*  80 */ 'P','Q','R','S','T','U','V','W','X','Y',
/*  90 */ 'Z','[','\\',']','^','_','`','a','b','c',
/* 100 */ 'd','e','f','g','h','i','j','k','l','m',
/* 110 */ 'n','o','p','q','r','s','t','u','v','w',
/* 120 */ 'x','y','z','{','|','}','~',SP ,SP ,SP ,
/* 130 */ SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,
/* 140 */ SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,
/* 150 */ SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,
/* 160 */ SP ,173,189,156,207,190,221,245,249,184,
/* 170 */ 166,174,170,240,169,238,248,241,253,252,
/* 180 */ 239,230,244,250,247,251,167,175,172,171,
/* 190 */ 243,168,183,181,182,199,142,143,146,128,
/* 200 */ 212,144,210,211,222,214,215,216,209,165,
/* 210 */ 227,224,226,229,153,158,157,235,233,234,
/* 220 */ 154,237,232,225,133,160,131,198,132,134,
/* 230 */ 145,135,138,130,136,137,141,161,140,139,
/* 240 */ 208,164,149,162,147,228,148,246,155,151,
/* 250 */ 163,150,129,236,231,152 };

unsigned char pc_to_iso1[256]={
/*  00 */ SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,
/*  10 */ SP ,SP ,SP , 0 ,SP ,SP ,SP ,SP ,SP ,SP ,
/*  20 */ SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,SP ,
/*  30 */ SP ,SP ,SP ,'!','\"','#','$','%','&','\'',
/*  40 */ '(',')','*','+',',','-','.','/','0','1',
/*  50 */ '2','3','4','5','6','7','8','9',':',';',
/*  60 */ '<','=','>','?','@','A','B','C','D','E',
/*  70 */ 'F','G','H','I','J','K','L','M','N','O',
/*  80 */ 'P','Q','R','S','T','U','V','W','X','Y',
/*  90 */ 'Z','[','\\',']','^','_','`','a','b','c',
/* 100 */ 'd','e','f','g','h','i','j','k','l','m',
/* 110 */ 'n','o','p','q','r','s','t','u','v','w',
/* 120 */ 'x','y','z','{','|','}','~',SP ,199,252,
/* 130 */ 233,226,228,224,229,231,234,235,232,239,
/* 140 */ 238,236,196,197,201,230,198,244,246,242,
/* 150 */ 251,249,255,214,220,248,163,216,215,SP ,
/* 160 */ 225,237,243,250,241,209,170,186,191,174,
/* 170 */ 172,189,188,161,171,187,SP ,SP ,SP ,SP ,
/* 180 */ SP ,193,194,192,169,SP ,SP ,SP ,SP ,162,
/* 190 */ 165,SP ,SP ,SP ,SP ,SP ,SP ,SP ,227,195,
/* 200 */ SP ,SP ,SP ,SP ,SP ,SP ,SP ,164,240,208,
/* 210 */ 202,203,200,SP ,205,206,207,SP ,SP ,SP ,
/* 220 */ SP ,166,204,SP ,211,223,212,210,245,213,
/* 230 */ 181,254,222,218,219,217,253,221,175,180,
/* 240 */ 173,177,SP ,190,182,167,247,184,176,168,
/* 250 */ 183,185,179,178,SP ,SP };


/*** Global functions ***/


#ifdef TRACE_FUNCTION
void
ICUTrace(const gchar *format, ...)
{
  va_list args;
  gchar *str;

  g_return_if_fail (format != NULL);

  va_start (args, format);
  str = g_strdup_vprintf (format, args);
  va_end (args);

  fputs ("gnomeicu trace: ", stderr);
  fputs (str, stderr);

  g_free (str);
}
#endif

GList *get_codes( FIELD_T field )
{
	GList *l = NULL;
	int i;
	WORD last = 0xffff;
	FIELD_CODE *array;

	switch (field) {
		case TIMEZONE:
			last = 100;
			array = time_zones;
			break;
		case COUNTRY:
			array = Country_Codes;	
			break;
		case INTEREST:
			last = 255;
			array = interest_codes;	
			break;
		case LANGUAGE:
			last = 255;
			array = language_codes;	
			break;
		case PAST:
			array = past_codes;	
			break;
		case WORK:
			array = work_codes;	
			break;
		case AFFILIATION:
			array = affiliation_codes;	
			break;
		default:
			return NULL;
	}

	for( i = 0; array[i].code != last; i++ )
		l = g_list_prepend( l, _(array[i].name) );
	
	//	return g_list_reverse( l );
	return g_list_sort(l, (GCompareFunc) strcoll);
}

gchar *get_name( FIELD_T field, WORD code )
{
	int i;
	WORD last = 0xffff;
	FIELD_CODE *array;

	switch (field) {
		case TIMEZONE:
			last = 100;
			array = time_zones;
			break;
		case COUNTRY:
			array = Country_Codes;	
			break;
		case INTEREST:
			last = 255;
			array = interest_codes;	
			break;
		case LANGUAGE:
			last = 255;
			array = language_codes;	
			break;
		case PAST:
			array = past_codes;	
			break;
		case WORK:
			array = work_codes;	
			break;
		case AFFILIATION:
			array = affiliation_codes;	
			break;
		default:
			return NULL;
	}

	for( i = 0; array[i].code != last; i++ )
		if ( array[i].code == code )
			return _(array[i].name);

	return _(array[i].name);
}

WORD get_code( FIELD_T field, const gchar *name )
{
	int i;
	WORD last = 0xffff;
	FIELD_CODE *array;

	switch (field) {
		case TIMEZONE:
			last = 100;
			array = time_zones;
			break;
		case COUNTRY:
			array = Country_Codes;	
			break;
		case INTEREST:
			last = 255;
			array = interest_codes;	
			break;
		case LANGUAGE:
			last = 255;
			array = language_codes;	
			break;
		case PAST:
			array = past_codes;	
			break;
		case WORK:
			array = work_codes;	
			break;
		case AFFILIATION:
			array = affiliation_codes;	
			break;
		default:
			return 0xffff; /* err... 0xffff (for equivalent) seems to be the best */
	}

	for( i = 0; array[i].code != last; i++ )
		if( !strcmp( _(array[i].name), name ) )
			return array[i].code;

	return array[i].code;
}

const gchar *
get_status_str (DWORD status)
{
	if( status == STATUS_OFFLINE )
		return _("Offline");

	switch( status & 0xffff ) {
	case STATUS_ONLINE:
		return _("Online");
	case STATUS_AWAY:
		return _("Away");
	case STATUS_NA:
		return _("Not Available");
	case STATUS_FREE_CHAT:
		return _("Free For Chat");
	case STATUS_OCCUPIED:
		return _("Occupied");
	case STATUS_DND:
		return _("Do Not Disturb");
	case STATUS_INVISIBLE:
		return _("Invisible");
	default:
		return _("Unknown");
	}
}


/********************************************
Converts an intel endian character sequence to
a DWORD
*********************************************/
DWORD Chars_2_DW( const guchar buf[] )
{
	DWORD i;

	i= buf[3];
	i <<= 8;
	i+= buf[2];
	i <<= 8;
	i+= buf[1];
	i <<= 8;
	i+= buf[0];

	return i;
}

/********************************************
Converts an intel endian character sequence to
a WORD
*********************************************/
WORD Chars_2_Word( const guchar *buf )
{
	WORD i;

	i= buf[1];
	i <<= 8;
	i += buf[0];

	return i;
}

/********************************************
Converts a DWORD to
an intel endian character sequence 
*********************************************/
void DW_2_Chars( guchar buf[], DWORD num )
{
	buf[3] = ( guchar ) ((num)>>24)& 0x000000FF;
	buf[2] = ( guchar ) ((num)>>16)& 0x000000FF;
	buf[1] = ( guchar ) ((num)>>8)& 0x000000FF;
	buf[0] = ( guchar ) (num) & 0x000000FF;
}

/********************************************
Converts a WORD to
an intel endian character sequence 
*********************************************/
void Word_2_Chars( guchar buf[], WORD num )
{
	buf[1] = ( guchar ) (((unsigned)num)>>8) & 0x00FF;
	buf[0] = ( guchar ) ((unsigned)num) & 0x00FF;
}

/* Strip '\r' from text */
gchar *strip_cr( const gchar *str )
{
	gchar *new;
	gint cy = 0;

	if (str == NULL)
		return NULL;

	new = (gchar*)g_malloc0( strlen( str ) + 1 );

	while (*str != '\0')
	{
		if( *str != '\r' ) {
			new[cy] = *str;
			cy++;
		}
		str++;
	}

	return new;
}

/* Strip HTML out of AIM messages */
gchar *strip_html( const gchar *str )
{
	gchar *new;
	gint cy = 0;
	gboolean addchar = FALSE;

	if (str == NULL)
		return NULL;

	new = (gchar*)g_malloc0( strlen( str ) + 1 );

	if (!strncmp (str, "<HTML>", 6)) {
		while (*str != '\0')
		{
			if( *str == '<' )
				addchar = FALSE;

			if (addchar == TRUE) {
				new[cy] = *str;
				cy++;
			}
			if (*str == '>')
				addchar = TRUE;
			str++;
		}
	} else {
		return g_strdup(str);
	}

	return new;
}

gchar *add_cr( const gchar *text )
{
	const gchar* tmp = text;
	gchar* res;
	int cy;

	if (text == NULL)
		return NULL;

	cy = 0;

	while (*tmp != '\0')
	{
		if (*tmp == '\n') {
			cy ++;
		}
		tmp++;
	}

	res = g_malloc( strlen( text ) + cy + 1 );

	cy = 0;
	tmp = text;
	while (*tmp != '\0')
	{
		if( *tmp != '\n' ) {
			res[cy] = *tmp;
			cy++;
		} else {
			res[cy] = '\r';
			res[cy+1] = '\n';
			cy+=2;
		}
		tmp++;
	}
	res[cy] = '\0';

	return res;
}


GSList * Find_User( UIN_T uin )
{
	GSList *contact = Contacts;
	if (uin == NULL)
		return NULL;

	while( contact != NULL )
	{
		if( !strcmp (kontakt->uin, uin) )
			break;
		contact = contact->next;
	}
	return contact;
}

GSList * Find_User_uid( WORD uid )
{
	GSList *contact = Contacts;
	while( contact != NULL )
	{
		if( kontakt->uid == uid ||
                    kontakt->vislist_uid == uid ||
                    kontakt->invlist_uid == uid ||
                    kontakt->ignorelist_uid == uid )
			break;
		contact = contact->next;
	}
	return contact;
}


GSList * Add_User( UIN_T uin, const gchar *name, gboolean inlist )
{
	GSList *contact;
	CONTACT_PTR contactdata;

#ifdef TRACE_FUNCTION
	g_print( "Add_User\n" );
#endif

	contact = Find_User (uin);
	while (contact && kontakt->gid == 0) {
		Contacts = g_slist_remove (Contacts, contact->data);
		contact = Find_User (uin);
	}

	if( contact != NULL )
		return contact;

	contactdata = g_new0( Contact_Member, 1 );

	contactdata->uin = g_strdup (uin);
	contactdata->stored_messages = NULL;
	contactdata->status = STATUS_OFFLINE;
	contactdata->last_time = -1L;
	contactdata->current_ip = -1L;
        contactdata->has_direct_connect = FALSE;
	contactdata->port = 0;
	contactdata->sok = 0;
	contactdata->invis_list = FALSE;
	contactdata->msg_dlg_xml = NULL;
	contactdata->confirmed = inlist; /* Needs confirmation if not manually added*/

	if( !strcmp (uin, "10") ) {
		contactdata->nick = g_strdup(_("Web Message"));
	} else {
		if( name == NULL || name[0] == '\0' ) {
			contactdata->nick = g_strdup(uin);
		} else {
			contactdata->nick = g_strdup(name);
		}
	}

	contactdata->info = g_new0( USER_INFO_STRUCT, 1 );
	contactdata->info->sex = NOT_SPECIFIED;
	contactdata->info->country = 1;

	contactdata->tcp_seq = -1;

	contactdata->inlist = inlist;

	/* TODO: insert sorted */
	Contacts = g_slist_append( Contacts, contactdata );
	contact = g_slist_last (Contacts);

        if (!inlist && mainconnection && Current_Status != STATUS_OFFLINE) {
          GSList *requestdat;
          DWORD reqid;

          reqid = v7_request_info(mainconnection, uin);
          
          requestdat =  v7_get_request_by_reqid(reqid);
          ((Request*)requestdat->data)->type = NEW_USER_VERIFICATION;
        }

	Save_RC();
	
	/* TODO: and find it */
	return contact;
}

/********************************************
Converts an big endian character sequence to
a WORD 
*********************************************/
WORD CharsBE_2_Word( const guchar *buf )
{
	WORD i;

	i= buf[0];
	i <<= 8;
	i += buf[1];

	return i;
}

/********************************************
Converts a DWORD to
an intel endian character sequence 
*********************************************/
void DW_2_CharsBE( guchar buf[], DWORD num )
{
	buf[0] = ( guchar ) ((num)>>24)& 0x000000FF;
	buf[1] = ( guchar ) ((num)>>16)& 0x000000FF;
	buf[2] = ( guchar ) ((num)>>8)& 0x000000FF;
	buf[3] = ( guchar ) (num) & 0x000000FF;
}

/********************************************
Converts an intel endian character sequence to
a DWORD
*********************************************/
DWORD CharsBE_2_DW( const guchar buf[] )
{
	DWORD i;

	i= buf[0];
	i <<= 8;
	i+= buf[1];
	i <<= 8;
	i+= buf[2];
	i <<= 8;
	i+= buf[3];

	return i;
}

/********************************************
Converts a WORD to
an big endian character sequence 
*********************************************/
void Word_2_CharsBE( guchar buf[], WORD num )
{
	buf[0] = ( guchar ) (((unsigned)num)>>8) & 0x00FF;
	buf[1] = ( guchar ) ((unsigned)num) & 0x00FF;
}


gchar *convert_to_utf8(const gchar *text)
{
  gchar *converted_text;
  GError *converterror = NULL;

  converted_text = g_convert_with_fallback(text, strlen(text)+1, 
					   "UTF-8", 
		         preferences_get_string(PREFS_GNOMEICU_CHARSET_FALLBACK),

					   "?", NULL, NULL, &converterror);

  if (converterror)
    g_warning("Conversing error: %s\n", converterror->message);

  g_clear_error(&converterror);

  return converted_text;
}

gchar *convert_from_utf8(const gchar *text)
{
  gchar *converted_text;
  GError *converterror = NULL;
  
  converted_text = g_convert_with_fallback(text, strlen(text)+1, 
		         preferences_get_string(PREFS_GNOMEICU_CHARSET_FALLBACK),
					   "UTF-8", 
					   "?", NULL, NULL, &converterror);
  if (converterror)
    g_warning("Conversing error: %s\n", converterror->message);
  
  g_clear_error(&converterror);

  return converted_text;

}

GladeXML *
gicu_util_open_glade_xml (const gchar *glade_file, const gchar *root_name)
{
  GladeXML *gxml;
  gchar *gfile;

  g_assert (glade_file != NULL);
  g_assert (root_name != NULL);

  gfile = g_strdup_printf ("%s%s", GNOMEICU_GLADEDIR, glade_file);

  gxml = glade_xml_new (gfile, root_name, NULL);
  if (!gxml) {
    GtkWidget *dialog;
    gchar *err_str;

    err_str = g_strdup_printf (_("Unable to load Glade user interface file: "
                                 "%s.\nMake sure the file is accessible."),
                               gfile);

    dialog = gtk_message_dialog_new (GTK_WINDOW (MainData->window),
                                     GTK_DIALOG_DESTROY_WITH_PARENT,
                                     GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, err_str);
    gtk_dialog_run (GTK_DIALOG (dialog));

    gtk_widget_destroy (dialog);
    g_free (err_str);
  }

  g_free (gfile);

  return gxml;
}
