

#include "icu_db.h"
#include <glib.h>
#include <locale.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void dump_file(const gchar *filename, FILE *file, const char *charset);

int main( int argc, char **argv)
{

  FILE *file;
  GDir *dir;
  gchar *dirname, *oldfilename, *newfilename, *tempfilename;
  const gchar *filename;
  const gchar *charset;


  if (argc > 2) {
    g_print("Usage: %s [character set]\n"
	    "The character set will be taken from the current locale if its not specified\n", argv[0]);
    return 1;
  }

  setlocale(LC_ALL, "");

  if(argc==1)
    charset = argv[1];
  else
    g_get_charset(&charset);

  dirname = g_strdup_printf("%s/.icq/history", g_get_home_dir());

  dir = g_dir_open(dirname, 0, NULL);

  g_free(dirname);

  while (filename = g_dir_read_name(dir)) {
    

    if (g_str_has_suffix(filename,".db")) {

      oldfilename = g_strdup_printf("%s/.icq/history/%s", 
				    g_get_home_dir(),
				    filename);
      tempfilename = g_strndup(oldfilename, strlen(oldfilename)-3);
      
      newfilename = g_strdup_printf("%s.txt", tempfilename);

      g_free(tempfilename);

      if (!g_file_test(newfilename,G_FILE_TEST_EXISTS)) {
      
	file = fopen(newfilename, "a");
	
	dump_file(oldfilename, file, charset);
	
	fclose(file);
	
      } else
	g_print("File already exists: %s\n", newfilename);

      g_free(newfilename);
      g_free(oldfilename);

    }
  }

  return 0;
}

gint compare_dates(const gint *a,
		   const gint *b)
{
	g_assert(a != NULL && b != NULL);
	return( *b - *a );
}

void dump_file(const gchar *filename, FILE *file, const char *charset)
{

  DB_FILE db_file;
  datum firstkey, nextkey, key_data;
  GList *listed_history = NULL, *rev_list;
  int count = 0;
  
  
  db_file = icudb_open( filename, DB_READ );
  
  if( db_file == NULL ) {
    printf("Can't open file: %s\n", filename);
    return;
  }



  firstkey = icudb_firstkey ( db_file );

  /* First, create a list of keys from the history file */
  while ( firstkey.dptr ) {

    listed_history = g_list_append( listed_history, GINT_TO_POINTER(firstkey.dptr) );

    nextkey = icudb_nextkey ( db_file, firstkey );
    firstkey = nextkey;

  }

  /* Then sort the keys */
  listed_history = g_list_sort (listed_history, (GCompareFunc)compare_dates);
  rev_list = g_list_last( listed_history );

  firstkey.dsize = sizeof (time_t);

  while (rev_list != NULL) {

    firstkey.dptr = rev_list->data;
    key_data = icudb_fetch (db_file, firstkey);

    if (key_data.dptr != NULL) {
      gchar *newtext;

      if (g_utf8_validate(key_data.dptr + 2, key_data.dsize - 2, NULL))
	newtext = g_strndup(key_data.dptr + 2, key_data.dsize - 2);
      else
	newtext = g_convert_with_fallback(key_data.dptr + 2,
					  key_data.dsize - 2,
					  "UTF-8", charset,
					  "?" ,NULL, NULL, NULL);

      count++;
      
      fprintf(file, "\n*** %c %ld %s%s\n",
	      MESG_ISRECV (key_data.dptr) ? 'R' : 'S',
	      *((time_t*)rev_list->data),
	      ctime(rev_list->data),
	      newtext);

      g_free(newtext);

      icudb_free (key_data.dptr);
    }

    icudb_free (rev_list->data);
    rev_list = rev_list->prev;
  }

  /* If window was destroyed, free the rest of the list */
  while (rev_list != NULL) {
    icudb_free (rev_list->data);
    rev_list = rev_list->prev;
  }


  g_print("Converted %d messages to file %s\n", count, filename);

  /* Finally, close the db file */
  icudb_close ( db_file );

  g_list_free (listed_history);
  
}



