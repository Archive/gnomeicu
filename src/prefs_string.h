/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* Defines for the GConf keys used in GnomeICU  */


/****** remember to add the key to gnomeicu.schemas.in as well ******/

#ifndef __PREFS_STRING_H__
#define __PREFS_STRING_H__

#define GNOMEICU_GCONF "/apps/gnomeicu"

/* multiple keys for widget, need custom routines */
#define PREFS_MULTIKEY ""

#define PREFS_GNOMEICU_WINDOW_WIDTH GNOMEICU_GCONF "/main-window-width"
#define PREFS_GNOMEICU_WINDOW_HEIGHT GNOMEICU_GCONF "/main-window-height"
#define PREFS_GNOMEICU_WINDOW_X GNOMEICU_GCONF "/main-window-x"
#define PREFS_GNOMEICU_WINDOW_Y GNOMEICU_GCONF "/main-window-y"

#define PREFS_GNOMEICU_STARTUP_CONNECT GNOMEICU_GCONF "/general/connections/startup-connect"
#define PREFS_GNOMEICU_AUTO_RECONNECT GNOMEICU_GCONF "/general/connections/auto-reconnect"
#define PREFS_GNOMEICU_AUTO_ACCEPT_FILE GNOMEICU_GCONF "/general/connections/auto-accept-file"
#define PREFS_GNOMEICU_DATA_PATH GNOMEICU_GCONF "/general/files/data-path"
#define PREFS_GNOMEICU_DOWNLOAD_PATH GNOMEICU_GCONF "/general/files/download-path"
#define PREFS_GNOMEICU_CONTACT_FILE GNOMEICU_GCONF "/general/files/contact-list-file"
#define PREFS_GNOMEICU_SOUND GNOMEICU_GCONF "/general/events/enable-sounds"
#define PREFS_GNOMEICU_SOUND_ONLINE GNOMEICU_GCONF "/general/events/enable-sounds-online"
#define PREFS_GNOMEICU_BEEPS GNOMEICU_GCONF "/general/events/enable-beeps"
#define PREFS_GNOMEICU_GNOME_SOUND_PROPERTIES GNOMEICU_GCONF "/general/events/gnome-sound-properties"
#define PREFS_GNOMEICU_PROG_EVENT_RECV_MSG GNOMEICU_GCONF "/general/events/program-recv-msg"
#define PREFS_GNOMEICU_PROG_EVENT_RECV_URL GNOMEICU_GCONF "/general/events/program-recv-url"
#define PREFS_GNOMEICU_PROG_EVENT_FILE_REQ GNOMEICU_GCONF "/general/events/program-file-request"
#define PREFS_GNOMEICU_PROG_EVENT_USER_ONLINE GNOMEICU_GCONF "/general/events/program-user-online"
#define PREFS_GNOMEICU_PROG_EVENT_USER_OFFLINE GNOMEICU_GCONF "/general/events/program-user-offline"
#define PREFS_GNOMEICU_PROG_EVENT_AUTH_USER GNOMEICU_GCONF "/general/events/program-auth-by-user"
#define PREFS_GNOMEICU_PROG_EVENT_AUTH_REQ GNOMEICU_GCONF "/general/events/program-auth-request"
#define PREFS_GNOMEICU_PROG_EVENT_ADDED_TO_USER GNOMEICU_GCONF "/general/events/program-added-to-user-list"
#define PREFS_GNOMEICU_PROG_EVENT_CONTACT_LIST GNOMEICU_GCONF "/general/events/program-contact-list"

#define PREFS_GNOMEICU_AUTO_AWAY GNOMEICU_GCONF "/general/status/auto-away"
#define PREFS_GNOMEICU_AWAY_TIMEOUT GNOMEICU_GCONF "/general/status/away-timeout"
#define PREFS_GNOMEICU_AWAY_DEFAULT GNOMEICU_GCONF "/general/status/away-msg-default"
#define PREFS_GNOMEICU_AWAY_MSG_HEADINGS GNOMEICU_GCONF "/general/status/away-msg-headings"
#define PREFS_GNOMEICU_AWAY_MSG GNOMEICU_GCONF "/general/status/away-msg-"

#define PREFS_GNOMEICU_AUTO_POPUP GNOMEICU_GCONF "/general/ui/auto-popup"
#define PREFS_GNOMEICU_SPELLCHECK GNOMEICU_GCONF "/general/ui/spellcheck"
#define PREFS_GNOMEICU_DEFAULT_CHAT_MODE GNOMEICU_GCONF "/general/ui/default-chat-mode"
#define PREFS_GNOMEICU_CHARSET_FALLBACK GNOMEICU_GCONF "/general/ui/charset-fallback"
#define PREFS_GNOMEICU_ENTER_SENDS GNOMEICU_GCONF "/general/ui/enter-sends"
#define PREFS_GNOMEICU_ICON_THEMES GNOMEICU_GCONF "/general/themes/icons"
#define PREFS_GNOMEICU_EMOTICON_THEMES GNOMEICU_GCONF "/general/themes/emoticons"

#define PREFS_ICQ_DEBUG_SNAC GNOMEICU_GCONF "/icq/debug-snac"
#define PREFS_ICQ_DEBUG_FILE_TRANSFER GNOMEICU_GCONF "/icq/debug-file-transfer"

#define PREFS_ICQ_SERVER_NAME GNOMEICU_GCONF "/icq/network/server"
#define PREFS_ICQ_SERVER_PORT GNOMEICU_GCONF "/icq/network/server-port"
/*#define PREFS_ICQ_TCP_PORT_MIN GNOMEICU_GCONF "/icq/network/tcp-port-min"(4000)*/
/*#define PREFS_ICQ_TCP_PORT_MAX GNOMEICU_GCONF "/icq/network/tcp-port-max"(4100)*/


//#define PREFS_ICQ_AUTO_NA GNOMEICU_GCONF "/icq/status/auto-na"
//#define PREFS_ICQ_NA_TIMEOUT GNOMEICU_GCONF "/icq/status/na-timeout"

//#define PREFS_ICQ_NA_DEFAULT GNOMEICU_GCONF "/icq/status/na-msg-default"
//#define PREFS_ICQ_NA_MSG_HEADINGS GNOMEICU_GCONF "/icq/status/na-msg-headings"
//#define PREFS_ICQ_NA_MSG GNOMEICU_GCONF "/icq/status/na-msg-"

#define PREFS_ICQ_COLOR_ONLINE GNOMEICU_GCONF "/icq/color/online"
#define PREFS_ICQ_COLOR_AWAY GNOMEICU_GCONF "/icq/color/away"
#define PREFS_ICQ_COLOR_NA GNOMEICU_GCONF "/icq/color/not-available"
#define PREFS_ICQ_COLOR_OCCUPIED GNOMEICU_GCONF "/icq/color/occupied"
#define PREFS_ICQ_COLOR_DND GNOMEICU_GCONF "/icq/color/do-not-disturb"
#define PREFS_ICQ_COLOR_INVISIBLE GNOMEICU_GCONF "/icq/color/invisible"
#define PREFS_ICQ_COLOR_OFFLINE GNOMEICU_GCONF "/icq/color/offline"
#define PREFS_ICQ_COLOR_FREEFORCHAT GNOMEICU_GCONF "/icq/color/free-for-chat"


#endif /* __PREFS_STRING_H__ */
