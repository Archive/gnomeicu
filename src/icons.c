/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/**** Icon themes handling ****/

/*
 * Author: Gediminas Paulauskas
 * (c) 1999 Jeremy Wise
 * GnomeICU
 */

#include "common.h"
#include "gnomeicu.h"
#include "gnomeicu-spinner.h"
#include "icons.h"
#include "response.h"
#include "showlist.h"

#include <gtk/gtk.h>
#include <libgnome/gnome-url.h>
#include <libgnomeui/gnome-window-icon.h>

/* Global GdkPixbufs */
GdkPixbuf *still_eyes_pixbuf;
GdkPixbuf *icon_blank_pixbuf;
GdkPixbuf *icon_message_pixbuf;
GdkPixbuf *icon_url_pixbuf;
GdkPixbuf *icon_auth_pixbuf;
GdkPixbuf *icon_away_pixbuf;
GdkPixbuf *icon_na_pixbuf;
GdkPixbuf *icon_occ_pixbuf;
GdkPixbuf *icon_dnd_pixbuf;
GdkPixbuf *icon_ffc_pixbuf;
GdkPixbuf *icon_inv_pixbuf;
GdkPixbuf *icon_online_pixbuf;
GdkPixbuf *icon_offline_pixbuf;
GdkPixbuf *icon_chat_pixbuf;
GdkPixbuf *icon_file_pixbuf;
GdkPixbuf *icon_info_pixbuf;
GdkPixbuf *icon_contact_pixbuf;
GdkPixbuf *icon_hist_pixbuf;
GdkPixbuf *icon_ok_pixbuf;
GdkPixbuf *icon_cancel_pixbuf;
GdkPixbuf *icon_rename_pixbuf;
GdkPixbuf *icon_group_pixbuf;
GdkPixbuf *icon_lists_pixbuf;
GdkPixbuf *icon_groups_pixbuf;
GdkPixbuf *icon_birthday_pixbuf;

static gboolean icon_themes_initialized = FALSE;
/*** Local functions ***/
GdkPixbuf *init_one_pixmap (gchar * icon_name);

/*** Global functions ***/
gchar *make_icon_path_theme (const gchar * file_name, const gchar * theme_name)
{
	gchar *file;
        gchar *tname = NULL;

	if (theme_name == NULL) {
                tname = preferences_get_string (PREFS_GNOMEICU_ICON_THEMES);
		theme_name = tname;
        }

	/* Local themes */
	file =
	    g_strconcat (g_get_home_dir (), "/.icq/icons/", theme_name, "/",
			 file_name, NULL);

	if (!g_file_test ((file), G_FILE_TEST_EXISTS)) {   /* Local theme does not exist */
		/* Global themes */
		gchar *theme_path = GNOMEICU_DATADIR "/gnomeicu/icons/";

		g_free (file);
		file =
		    g_strconcat (theme_path, theme_name, "/", file_name, NULL);

		if (!g_file_test ((file), G_FILE_TEST_EXISTS)) {	/* Theme does not exist, use Default */
			g_free (file);
			file =
			    g_strconcat (theme_path, "Default/", file_name,
					 NULL);

			if (!g_file_test ((file), G_FILE_TEST_EXISTS)) {	/* Nothing exists */
				g_free (file);
				file = NULL;
			}
		}
	}

        g_free (tname);

	return file;
}

gchar *make_icon_path (gchar * file_name)
{
	return make_icon_path_theme (file_name, NULL);
}

GdkPixbuf *init_one_pixmap (gchar * icon_name)
{
	GdkPixbuf *im = NULL;
	gchar *icon_filename;

	icon_filename = make_icon_path (icon_name);
	
	if (icon_filename)
		im = gdk_pixbuf_new_from_file (icon_filename, NULL);

	g_free (icon_filename);
	return im;
}

void
icon_themes_refresh (void)
{
  icon_themes_load ();
  gnomeicu_tree_refresh_visuals ();
  gnomeicu_set_status_button (FALSE);
  gnomeicu_spinner_reload ();
}
  
void
icon_themes_init (void)
{
  if (icon_themes_initialized)
    return;

  icon_themes_load ();
  preferences_watch_key (PREFS_GNOMEICU_ICON_THEMES, icon_themes_refresh);

  icon_themes_initialized = TRUE;
}

void
icon_themes_load (void)
{
  icon_blank_pixbuf = init_one_pixmap ("gnomeicu-blank.png");
  still_eyes_pixbuf = init_one_pixmap ("gnomeicu-still.png");
  icon_message_pixbuf = init_one_pixmap ("gnomeicu-message.png");
  icon_url_pixbuf = init_one_pixmap ("gnomeicu-url.png");
  icon_auth_pixbuf = init_one_pixmap ("gnomeicu-auth.png");
  icon_away_pixbuf = init_one_pixmap ("gnomeicu-away.png");
  icon_occ_pixbuf = init_one_pixmap ("gnomeicu-occ.png");
  icon_na_pixbuf = init_one_pixmap ("gnomeicu-na.png");
  icon_dnd_pixbuf = init_one_pixmap ("gnomeicu-dnd.png");
  icon_ffc_pixbuf = init_one_pixmap ("gnomeicu-ffc.png");
  icon_inv_pixbuf = init_one_pixmap ("gnomeicu-inv.png");
  icon_online_pixbuf =init_one_pixmap ("gnomeicu-online.png");
  icon_offline_pixbuf = init_one_pixmap ("gnomeicu-offline.png");
  icon_chat_pixbuf = init_one_pixmap ("gnomeicu-chat.png");
  icon_file_pixbuf = init_one_pixmap ("gnomeicu-file.png");
  icon_info_pixbuf = init_one_pixmap ("gnomeicu-info.png");
  icon_contact_pixbuf = init_one_pixmap ("gnomeicu-contact.png");
  icon_hist_pixbuf = init_one_pixmap ("gnomeicu-hist.png");
  icon_rename_pixbuf = init_one_pixmap ("gnomeicu-rename.png");
  icon_cancel_pixbuf = init_one_pixmap ("gnomeicu-cancel.png");
  icon_ok_pixbuf = init_one_pixmap ("gnomeicu-ok.png");
  icon_group_pixbuf = init_one_pixmap ("gnomeicu-group.png");
  icon_lists_pixbuf = init_one_pixmap ("gnomeicu-lists.png");
  icon_groups_pixbuf = init_one_pixmap ("gnomeicu-groups.png");
  icon_birthday_pixbuf = init_one_pixmap ("gnomeicu-birthday.png");
}

GdkPixbuf *get_pixbuf_for_status (DWORD status)
{
	/* test offline separately, because -1 & 0xFFFF is not -1 */
	if (STATUS_OFFLINE == status)
		return icon_offline_pixbuf;

	switch (status & 0xffff) {
	case STATUS_ONLINE:
		return icon_online_pixbuf;
	case STATUS_DND:
		return icon_dnd_pixbuf;
	case STATUS_AWAY:
		return icon_away_pixbuf;
	case STATUS_OCCUPIED:
		return icon_occ_pixbuf;
	case STATUS_NA:
		return icon_na_pixbuf;
	case STATUS_INVISIBLE:
		return icon_inv_pixbuf;
	case STATUS_FREE_CHAT:
		return icon_ffc_pixbuf;
	default:
		g_warning("No icon for this status: 0x%x\n", status & 0xffff);
		return icon_offline_pixbuf;
	}
}

GdkPixbuf *get_pixbuf_for_message (gint type)
{
	switch (type)
	{
	case MESSAGE_TEXT:
		return icon_message_pixbuf;
	case MESSAGE_CONT_LIST:
		return icon_contact_pixbuf;
	case MESSAGE_CHAT_REQ:
		return icon_chat_pixbuf;
	case MESSAGE_FILE_REQ:
		return icon_file_pixbuf;
	case MESSAGE_URL:
		return icon_url_pixbuf;
	case MESSAGE_AUTH_REQ:
	case MESSAGE_USER_ADD:
	case MESSAGE_USER_AUTHD:
		return icon_auth_pixbuf;
	default:
		g_warning("No icon for this message type: %d\n", type);
		return icon_message_pixbuf;
	}
}

void set_window_icon (GtkWidget * window, gchar * name)
{
	gchar *path = make_icon_path (name);
	gnome_window_icon_set_from_file (GTK_WINDOW (window), path);
	g_free (path);
}

GtkWidget* make_button_with_pixmap (const char *label_text, const char *pixmap_name)
{
	GtkWidget *button;
	GtkWidget *label;
	GtkWidget *image;
	GtkWidget *hbox;
	GtkWidget *align;

	button = gtk_button_new ();

	label = gtk_label_new_with_mnemonic (label_text);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), GTK_WIDGET (button));

	image = gtk_image_new_from_stock (pixmap_name, GTK_ICON_SIZE_BUTTON);
	
	hbox = gtk_hbox_new (FALSE, 2);

	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
	gtk_box_pack_end (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	align = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);

	gtk_container_add (GTK_CONTAINER (button), align);
	gtk_container_add (GTK_CONTAINER (align), hbox);

	GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
	gtk_widget_show_all (button);

	return button;
}
