#ifndef __RTF_READER_H__
#define __RTF_READER_H__

#include <gtk/gtk.h>

gchar*
rtf_parse_and_insert(gchar* rtf_text, GtkTextBuffer* text_buffer);

#define rtf_parse_only(a) (rtf_parse_and_insert(a, NULL))

#endif
