#ifndef __SENDCONTACT__
#define __SENDCONTACT__

#include "common.h"

typedef struct _ContactPair ContactPair;

struct _ContactPair {
  gchar *textuin;
  gchar *nick;
};

void contact_list_window( CONTACT_PTR contact );
void received_contact_list_window( CONTACT_PTR contact, GList *contacts );

#endif /* __SENDCONTACT__ */
