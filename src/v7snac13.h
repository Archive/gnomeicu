/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * Functions for SNAC family 0x13, server side contacts list management
 * first created by Patrick Sung (2002)
 */

#ifndef __V7SNAC13_H__
#define __V7SNAC13_H__

#include "datatype.h"
#include "v7base.h"

extern gboolean contacts_resync;

void v7_read_contacts_list (Snac *snac, V7Connection *conn);

void v7_request_contacts_list (V7Connection *conn);
void v7_begin_CL_edit (V7Connection *conn, gboolean migrate);
void v7_end_CL_edit (V7Connection *conn);
void v7_migrate_contacts_list (V7Connection *conn);
guint v7_addto_contacts_list (V7Connection *conn, UIN_T uin,
			     const gchar *nick,
			     guint group, gboolean authtlv);
void v7_send_grant_auth (V7Connection *conn, UIN_T uin);
void v7_send_auth_message (V7Connection *conn, UIN_T uin,
			   const gchar *authmsg);
void v7_snac13_check_srv_ack (V7Connection *conn, Snac *snac);
void v7_send_status_server (V7Connection *conn, gboolean visible);
void v7_update_nickname (V7Connection *conn, UIN_T uin, const gchar *nick,
                         guint gid, guint uid, gboolean wait_auth);
void v7_update_group_info (V7Connection *conn, guint gid);
guint v7_try_add_uin (V7Connection *conn, UIN_T uin, const gchar *nick, 
		      guint gid, gboolean grant);
void v7_ask_uin_for_auth (V7Connection *conn, gchar *authmsg, UIN_T uin);
void v7_remove_contact (V7Connection *conn, UIN_T uin, const gchar *nick,
                        guint gid, guint uid, gboolean wait_auth);
void v7_recv_auth_request (Snac *snac);
void v7_recv_added_you (Snac *snac);
void v7_grant_auth_request (V7Connection *conn, UIN_T uin);
void v7_user_grant_auth (Snac *snac);
void v7_visible (V7Connection *conn, UIN_T uin, WORD luid, gboolean add);
void v7_invisible (V7Connection *conn, UIN_T uin, WORD luid, gboolean add);
void v7_ignore (V7Connection *conn, UIN_T uin, WORD luid, gboolean add);
void v7_user_change_group (V7Connection *conn, Contact_Member *c,
                           WORD new_gid, const gchar *new_grpname);
void v7_add_new_group (V7Connection *conn, guint gid);
void v7_delete_empty_group (V7Connection *conn, guint gid);
void v7_finalize_delete_group (V7Connection *conn);

#endif /* __V7SNAC13_H__ */
