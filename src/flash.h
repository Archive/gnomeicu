/***************************************
 Blink stuff, handle other timed events
 (c) 1999 Jeremy Wise
 GnomeICU
****************************************/

#ifndef __FLASH_H__
#define __FLASH_H__

#include <glib.h>

guint flash_messages( void );

#endif /* __FLASH_H__ */
