/**************************
 Server, accepting commands to running GnomeICU
 (c) 1999 Jeremy Wise
 (c) 2001 drJeckyll@hotmail.com
 GnomeICU
***************************/

#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <unistd.h>

#include "common.h"
#include "changeinfo.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "icons.h"
#include "ignore.h"
#include "invisible.h"
#include "notify.h"
#include "response.h"
#include "search.h"
#include "showlist.h"
#include "tcp.h"
#include "userserver.h"
#include "util.h"
#include "visible.h"
#include "v7send.h"
#include "showlist.h"

#include "msg.h"

static void userserver_handler( gint sock );
static void show_contact_message_client( GSList *contact, gint sock );
static void icq_msgbox_client( STORED_MESSAGE_PTR message_text, UIN_T uin,
			       gint sock );
static gboolean userserver_accept(GIOChannel *iochan, GIOCondition cond,
			      gpointer data);



int userserver_socket;
gchar *userserver_name;
gchar *userserver_sym;

GIOChannel *userserver_iochan;

#define USERSERVER_MAX_LENGTH 8192

#if 0
typedef struct _Commands {
  int cmd_num;
  char *cmd_name;
  char *cmd_desc;
} Commands;

Commands client_cmd[] = {
  { 0, "readmsg", "Read pending message" },
  { , "help", "Print this help message" },
  { 0, NULL, NULL }
};
#endif

void userserver_handler(gint sock)
{
  UServCmd cmdtype;
  UServCmdLen cmdlen;
  gchar *cmdbuf;
  gchar c;
  gchar *cmd, *args;
  GSList *contact;
  gchar msg_count = 0;
  gchar msg_count_buf[100];
  gchar icu_status[100];
  gchar online_users = 0;
  gchar online_list_[100];
  gchar online_list[5000];

  /* Read in the packet */

  if (read(sock,&c,1) != 1) {
    fprintf(stderr, "gnomeicu: userserver: Error reading from control socket, ignoring request.\n");
    return;
  }

  cmdtype = c;

  if (read(sock,&cmdlen,sizeof(UServCmdLen)) != sizeof(UServCmdLen)) {
    fprintf(stderr,"Unable to read length information from control socket, ignoring request.\n");
    return;
  }
	
  cmdlen = ntohl(cmdlen);

  if (cmdlen > USERSERVER_MAX_LENGTH)
    {
      fprintf(stderr,"Requested length of %ld exceeds hardcoded maximum of %ld, ignoring request.\n",
	      cmdlen, (unsigned long)USERSERVER_MAX_LENGTH);
      return;
    }
	
  cmdbuf=g_malloc0(cmdlen);

  if (!cmdbuf)
    {
      fprintf(stderr,"Unable to allocated %ld bytes for request, ignoring request, and boy you're in trouble.\n",
	      cmdlen);
      return;
    }


  if (read(sock,cmdbuf,cmdlen) != cmdlen)
    {
      fprintf(stderr,"Unable to read all %ld requested bytes from socket, ignoring request.\n",cmdlen);
      g_free(cmdbuf);
      return;
    }

  switch (cmdtype)
    {
    case USERSERVER_CMD_NOOP:
      break;
		      
    case USERSERVER_CMD_MSGCOUNT:
      contact = Contacts;
			
      while (contact != NULL)
	{
	  msg_count += g_slist_length(((CONTACT_PTR)contact->data)->stored_messages);
	  contact = contact->next;
	}
			
      g_free( cmdbuf );
      sprintf(msg_count_buf, "%d\n", msg_count);
      cmdbuf = g_strdup( msg_count_buf );
      cmdlen = g_htonl( strlen(cmdbuf) + 1);
      write( sock, &cmdlen, sizeof( unsigned long ) );
      write( sock, cmdbuf, strlen( cmdbuf ) + 1 );
      break;

    case USERSERVER_ICUKRELL_ASK:
      contact = Contacts;
		  
      while (contact != NULL) {
	msg_count += g_slist_length(kontakt->stored_messages);
	if (kontakt->status != STATUS_OFFLINE)
	  online_users++;
	contact = contact->next;
      }
		  
		  
      sprintf(icu_status, "Unknown");
      if (Current_Status == STATUS_OFFLINE)
	sprintf(icu_status, "Offline");
      else
	switch( Current_Status & 0xffff ) {
	case STATUS_ONLINE:
	  sprintf(icu_status, "Online");
	  break;
	case STATUS_INVISIBLE:
	  sprintf(icu_status, "Invisible");
	  break;
	case STATUS_NA:
	  sprintf(icu_status, "NotAvailable");
	  break;
	case STATUS_FREE_CHAT:
	  sprintf(icu_status, "FreeForChat");
	  break;
	case STATUS_OCCUPIED:
	  sprintf(icu_status, "Occupied");
	  break;
	case STATUS_AWAY:
	  sprintf(icu_status, "Away");
	  break;
	case STATUS_DND:
	  sprintf(icu_status, "DoNotDisturb");
	  break;
	default:
	  g_warning("Unknown current status: 0x%x\n", Current_Status & 0xffff);
	}
			
      g_free( cmdbuf );
      cmdbuf = preferences_get_string (PREFS_GNOMEICU_ICON_THEMES);
      sprintf(msg_count_buf, "%s %d %d %s\n", icu_status, msg_count, online_users, cmdbuf);
      g_free(cmdbuf);
      cmdbuf = g_strdup( msg_count_buf );
      cmdlen = g_htonl( strlen(cmdbuf) + 1 );
      write( sock, &cmdlen, sizeof( unsigned long ) );
      write( sock, cmdbuf, strlen( cmdbuf ) + 1 );
      break;
			
    case USERSERVER_CMD_READMSG:
      contact = Contacts;
      while( contact != NULL && g_slist_length( kontakt->stored_messages ) == 0 )
	contact = contact->next;

      if( contact == NULL )
	{
	  g_free( cmdbuf );
	  cmdbuf = g_strdup( _("No messages available.\n\n") );
	  cmdlen = g_htonl( strlen( cmdbuf ) + 1 );
	  write( sock, &cmdlen, sizeof( unsigned long ) );
	  write( sock, cmdbuf, strlen( cmdbuf ) + 1 );
	  break;
	}

      show_contact_message_client( contact, sock );
      break;
		
    case USERSERVER_CMD_GETSTATUS:
      sprintf(icu_status, "Unknown");
      if (Current_Status == STATUS_OFFLINE) sprintf(icu_status, "Offline");
      else
	switch( Current_Status & 0xffff )
	  {
	  case STATUS_ONLINE:
	    sprintf(icu_status, "Online");
	    break;
	  case STATUS_INVISIBLE:
	    sprintf(icu_status, "Invisible");
	    break;
	  case STATUS_NA:
	    sprintf(icu_status, "NotAvailable");
	    break;
	  case STATUS_FREE_CHAT:
	    sprintf(icu_status, "FreeForChat");
	    break;
	  case STATUS_OCCUPIED:
	    sprintf(icu_status, "Occupied");
	    break;
	  case STATUS_AWAY:
	    sprintf(icu_status, "Away");
	    break;
	  case STATUS_DND:
	    sprintf(icu_status, "DoNotDisturb");
	    break;
	  default:
	    g_warning("Unknown current status: 0x%x\n", Current_Status & 0xffff);
	  }

      g_free( cmdbuf );
      sprintf(msg_count_buf, "%s\n", icu_status);
      cmdbuf = g_strdup(msg_count_buf);
      cmdlen = g_htonl(strlen(cmdbuf) + 1);
      write( sock, &cmdlen, sizeof( unsigned long ) );
      write( sock, cmdbuf, strlen( cmdbuf ) + 1 );
      break;
		
    case USERSERVER_CMD_DO:
      cmd=strtok(cmdbuf," \t");
      if (!cmd) break;

      /* COMMAND: QUIT */
      else if (!strcasecmp(cmd,"quit"))
	icq_quit(NULL,NULL);

      /* COMMAND: HIDE */
      else if (!strcasecmp(cmd,"hide"))
	{
	  gtk_window_get_position (GTK_WINDOW (MainData->window),
				   &MainData->x, &MainData->y);
	  gtk_widget_hide(MainData->window);
	  MainData->hidden = TRUE;
	}
			
      /* COMMAND: SHOW */
      else if (!strcasecmp(cmd,"show"))
	{
	  gtk_window_move (GTK_WINDOW (MainData->window),
			   MainData->x, MainData->y);
	  gtk_window_present (GTK_WINDOW(MainData->window));
	  MainData->hidden = FALSE;
	}
      
      /* COMMAND: SHOWHIDE */
      else if (!strcasecmp(cmd,"showhide"))
	{
	  if (GTK_WIDGET_VISIBLE (MainData->window)) {
	    gtk_window_get_position (GTK_WINDOW (MainData->window),
				     &MainData->x, &MainData->y);
	    gtk_widget_hide (MainData->window);
	    MainData->hidden = TRUE;
	  } else {
	    gtk_window_move (GTK_WINDOW (MainData->window),
			     MainData->x, MainData->y);
	    gtk_window_present (GTK_WINDOW(MainData->window));
	    MainData->hidden = FALSE;
	  }
	}
			
      /* COMMAND: ADDCONTACT */
      else if (!strcasecmp(cmd,"addcontact"))
	search_window(NULL, 0);
				
      /* COMMAND: SHOWPREFS */
      else if (!strcasecmp(cmd,"showprefs"))
	preferences_dialog_show ();
			
      /* COMMAND: ISERINFO */
      else if (!strcasecmp(cmd,"changeinfo"))
	change_info_window(NULL, 0);
				
      /* COMMAND: IGNORELIST */
      else if (!strcasecmp(cmd,"ignorelist"))
	ignore_list_dialog();
				
      /* COMMAND: VISIBLELIST */
      else if (!strcasecmp(cmd,"visiblelist"))
	visible_list_dialog();
				
      /* COMMAND: INVISIBLELIST */
      else if (!strcasecmp(cmd,"invisiblelist"))
	invisible_list_dialog();
				
      /* COMMAND: NOTIFYLIST */
      else if (!strcasecmp(cmd,"notifylist"))
	notify_list_dialog();

      /* COMMAND: AWAY <MESSAGE> */
      else if (!strcasecmp(cmd,"away"))
	{
	  char *msg;
	
	  msg=strtok(NULL,"");
	  if (!msg)
	    break;

	  g_free( Away_Message );
	  Away_Message = g_strdup( msg );
	}

      /* COMMAND: USERADD <UIN> */
      else if (!strcasecmp(cmd,"adduser") || !strcasecmp(cmd,"useradd"))
	{
	  char *uin;

	  uin=strtok(NULL," ");
	  if (!uin) break;
	
	  /* FIXME for groups, etc..
	     Add_User(uin, NULL, TRUE);
	  */
	}

      /* COMMAND: CONTACTLIST */
      else if (!strcasecmp(cmd, "contactlist"))
	{
	  contact = Contacts;

	  online_list[ 0 ] = 0;
	  while (contact != NULL)
	    {
	      sprintf(online_list_, "%s\t%s\t", kontakt->nick, kontakt->uin );
	      strcat(online_list, online_list_);
	      switch (kontakt->status)
	    	{
		case STATUS_OFFLINE:
		  strcat(online_list, "Offline");
		  break;
		case STATUS_ONLINE:
	          strcat(online_list, "Online");
	          break;
	    	case STATUS_INVISIBLE:
	          strcat(online_list, "Invisible");
	          break;
	    	case STATUS_NA:
	          strcat(online_list, "NotAvailable");
	          break;
	    	case STATUS_FREE_CHAT:
	          strcat(online_list, "FreeForChat");
	          break;
	    	case STATUS_OCCUPIED:
	          strcat(online_list, "Occupied");
	          break;
	    	case STATUS_AWAY:
	          strcat(online_list, "Away");
	          break;
	    	case STATUS_DND:
	          strcat(online_list, "DoNotDisturb");
	          break;
	    	default:
	          g_warning("Unknown status: 0x%x\n", kontakt->status & 0xffff);
	   	 }
	      strcat(online_list, "\n");
	      contact = contact->next;
	    }
	  online_list[strlen(online_list) - 1] = 0;

	  g_free(cmdbuf);
	  cmdbuf = g_strdup(online_list);
	  cmdlen = g_htonl(strlen(cmdbuf) + 1);
	  write(sock, &cmdlen, sizeof(unsigned long));
	  write(sock, cmdbuf, strlen(cmdbuf) + 1);
	}

      /* COMMAND: ONLINELIST */
      else if (!strcasecmp(cmd, "onlinelist"))
	{
	  contact = Contacts;
			
	  online_list[0] = 0;
	  while (contact != NULL)
	    {
	      if (kontakt->status != STATUS_OFFLINE)
		{
		  sprintf(online_list_, "%s\t%s\t", kontakt->nick, kontakt->uin);
		  strcat(online_list, online_list_);
		  switch (kontakt->status & 0xffff)
		    {
		    case STATUS_ONLINE:
		      strcat(online_list, "Online");
		      break;
		    case STATUS_INVISIBLE:
		      strcat(online_list, "Invisible");
		      break;
		    case STATUS_NA:
		      strcat(online_list, "NotAvailable");
		      break;
		    case STATUS_FREE_CHAT:
		      strcat(online_list, "FreeForChat");
		      break;
		    case STATUS_OCCUPIED:
		      strcat(online_list, "Occupied");
		      break;
		    case STATUS_AWAY:
		      strcat(online_list, "Away");
		      break;
		    case STATUS_DND:
		      strcat(online_list, "DoNotDisturb");
		      break;
		    default:
		      g_warning("Unknown status: 0x%x\n", kontakt->status & 0xffff);
		    }
		  strcat(online_list, "\n");
		}
	      contact = contact->next;
	    }
	  online_list[strlen(online_list) - 1] = 0;

	  g_free(cmdbuf);
	  cmdbuf = g_strdup(online_list);
	  cmdlen = g_htonl(strlen(cmdbuf) + 1);
	  write(sock, &cmdlen, sizeof(unsigned long));
	  write(sock, cmdbuf, strlen(cmdbuf) + 1);
	}
			
      /* COMMAND: SENDMSGMENU */
      else if (strstr(cmd,"sendmsgmenu"))
	{
	  gchar *mycmd;
	  gchar *newcmd, *newuin;
	  int iuin;
			    
	  mycmd = strdup(cmd);
	  newcmd = strtok(mycmd, "_");
	  newuin = strtok(NULL, "_");
	  iuin = atoi(newuin);

	  contact = Contacts;

	  while( contact != NULL )
	    {
	      if (!strcmp(kontakt->uin, newuin))
		{
		  if (g_slist_length (kontakt->stored_messages))
		    show_contact_message (kontakt);
		  else
		    open_message_dlg_with_message (kontakt, NULL);
		}
			    
	      contact = contact->next;
	    }

	  g_free(mycmd);
	  break;
	}
			
      /* COMMAND: MSG <UIN> <MSG> */
      else if (!strcasecmp(cmd,"msg"))
	{
	  char *uin,*msg;

	  uin=strtok(NULL," ");
	  if (!uin || atoi(uin)==0) break;
	  msg=strtok(NULL,"");
	  if (!msg) break;

	  v7_sendmsg(mainconnection, uin, msg);
	  break;
	}

      /* COMMAND: STATUS <offline|online|na|invisible|freechat|occupied|away|dnd> */
      else if (!strcasecmp(cmd,"status"))
	{
	  args=strtok(NULL," \r\n");
	  if (!args) break;

	  if (!strcasecmp(args,"offline"))
	    icq_set_status_offline( NULL, NULL );
	  if (!strcasecmp(args,"online"))
	    icq_set_status_online( NULL, NULL );
	  if (!strcasecmp(args,"na"))
	    icq_set_status_na( NULL, NULL );
	  if (!strcasecmp(args,"invisible"))
	    icq_set_status_invisible( NULL, NULL );
	  if (!strcasecmp(args,"freechat"))
	    icq_set_status_ffc( NULL, NULL );
	  if (!strcasecmp(args,"occupied"))
	    icq_set_status_occ( NULL, NULL );
	  if (!strcasecmp(args,"away"))
	    icq_set_status_away( NULL, NULL );
	  if (!strcasecmp(args,"dnd"))
	    icq_set_status_dnd( NULL, NULL );
	}


      /* COMMAND: STATUS_NOASK <offline|online|na|invisible|freechat|occupied|away|dnd> */
      else if (!strcasecmp(cmd,"status_noask"))
	{
	  args=strtok(NULL," \r\n");
	  if (!args) break;

	  if (!strcasecmp(args,"offline"))
	    icq_set_status_offline( NULL, NULL );
	  if (!strcasecmp(args,"online"))
	    icq_set_status_online( NULL, NULL );
	  if (!strcasecmp(args,"na"))
	    icq_set_status_na( NULL, (gpointer)1 );
	  if (!strcasecmp(args,"invisible"))
	    icq_set_status_invisible( NULL, NULL );
	  if (!strcasecmp(args,"freechat"))
	    icq_set_status_ffc( NULL, NULL );
	  if (!strcasecmp(args,"occupied"))
	    icq_set_status_occ( NULL, (gpointer)1 );
	  if (!strcasecmp(args,"away"))
	    icq_set_status_away( NULL, (gpointer)1 );
	  if (!strcasecmp(args,"dnd"))
	    icq_set_status_dnd( NULL, (gpointer)1 );
	}


      /* COMMAND: READEVENT */
      else if (!strcasecmp(cmd,"readevent"))
	{
	  contact = Contacts;

	  while( contact != NULL && g_slist_length( kontakt->stored_messages ) == 0 )
	    contact = contact->next;
				
	  if( contact != NULL )
	    show_contact_message (kontakt);
	}
      break;
    default:
      g_warning("Unknown command type: %d\n", cmdtype);
    }

  free(cmdbuf);
  return;
}


gboolean userserver_accept(GIOChannel *iochan, GIOCondition cond, gpointer data)
{
	int fd;
	int sock = g_io_channel_unix_get_fd(iochan);

	fd = accept(sock, NULL,NULL);

	if (fd >= 0)
	{
		userserver_handler(fd);
		close(fd);
	}

	return TRUE;
}

/* This code is borrowed heavily (read: cut&paste) from sawmill.
 * Thanks John!
 */
void
userserver_init(void)
{
  gchar *tmp_dir;

  uid_t unix_id;
  struct passwd *pw;

  userserver_socket = -1;
  userserver_name = NULL;

  unix_id = getuid();
  pw = getpwuid(unix_id);

  if (!pw) {
    fprintf(stderr, "Unable to get user name information for uid %d, naming service problem (/etc/nsswitch.conf)?\n",
            getuid());

    /* fall back to use uid instead of unix user name */
    tmp_dir = g_strdup_printf("/tmp/.gnomeicu-%d", unix_id);
    fprintf(stderr, "TEMP directory name changed to: %s\n", tmp_dir);
  } else {
    tmp_dir = g_strdup_printf("/tmp/.gnomeicu-%s", pw->pw_name);
  }

  if (mkdir(tmp_dir,0700) != 0) {
    if (errno == EEXIST) {
      struct stat st;

      if (stat(tmp_dir, &st) == 0) {
        if ((st.st_uid != getuid()) || (st.st_mode & (S_IRWXG | S_IRWXO))) {
          fprintf(stderr, "The %s directory is not owned by uid %d, or permissions are not secure enough.\n",
                  tmp_dir, getuid());
        }
      } else {
        perror(tmp_dir);
        return;
      }
    } else {
      perror(tmp_dir);
      return;
    }
  }
  userserver_sym = g_strdup_printf("%s/ctl", tmp_dir);
  userserver_name = g_strdup_printf("%s/ctl-%s", tmp_dir, our_info->uin);

  /* truncate the socket file name if it is too long */
  if (strlen(userserver_name) + 1 > 108)
    userserver_name[strlen(userserver_name) - 1] = '\0';

  symlink (userserver_name, userserver_sym);

  /* Delete the socket if it exists */
  if(access(userserver_name, F_OK) == 0) {
    /* Socket already exists. Delete it */
    unlink(userserver_name);

    if (access (userserver_name, F_OK) == 0) {
      fprintf (stderr, "Can't delete %s\n", userserver_name);
      return;
    }
  }

  userserver_socket = socket(AF_UNIX, SOCK_STREAM, 0);

  if(userserver_socket >= 0) {
    struct sockaddr_un addr;

    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, userserver_name);

    if(bind(userserver_socket, (struct sockaddr *)&addr,
            sizeof(addr.sun_family) + strlen(addr.sun_path) + 1) == 0) {
      chmod (userserver_name, 0700);

      if( listen(userserver_socket, 5) == 0 ) {
        userserver_iochan = g_io_channel_unix_new(userserver_socket);
        g_io_add_watch(userserver_iochan,
                       G_IO_IN|G_IO_ERR|G_IO_NVAL,
                       userserver_accept, NULL);
        return;
      } else {
        perror ("listen");
        close(userserver_socket);
        unlink(userserver_name);
      }
    } else {
      perror ("bind");
      close(userserver_socket);
    }
  } else
    perror ("socket");

  userserver_socket = -1;
  g_free(userserver_name);
  userserver_name = NULL;

  g_free(tmp_dir);
}

void userserver_kill (void)
{
	gchar tmp[1024];
	gint size;

	if(userserver_socket > 0)
	{
		size = readlink( userserver_sym, tmp, 1023 );
		tmp[size] = 0;
		if( !strcmp( userserver_name, tmp ) )
			unlink( userserver_sym );
		g_io_channel_shutdown(userserver_iochan, TRUE, NULL);
		close(userserver_socket);
		userserver_socket = -1;
		unlink(userserver_name);
		g_free(userserver_name);
		g_free(userserver_sym);
	}
}

void show_contact_message_client( GSList *contact, gint sock )
{
	STORED_MESSAGE_PTR msg;

	g_assert(contact != NULL);
	msg = (STORED_MESSAGE_PTR)kontakt->stored_messages->data;

	if( g_slist_length( kontakt->stored_messages ) )
	{
		icq_msgbox_client( (msg), kontakt->uin, sock );
		g_free( msg->message );
		g_free( kontakt->stored_messages->data );
		kontakt->stored_messages = g_slist_remove( kontakt->stored_messages, kontakt->stored_messages->data );

	}
}

void icq_msgbox_client( STORED_MESSAGE_PTR message_text, UIN_T uin, gint sock )
{
	gint type;
	gchar *text;
	gchar *retmsg;
	GSList *contact;
	unsigned long u;

	g_assert(message_text != NULL);

	contact = Find_User( uin );
	if( contact == NULL )
		return;

	type = message_text->type;
	text = message_text->message;
	
	switch( type )
	{
		case MESSAGE_TEXT:
			retmsg = g_strdup_printf( "Message from %s (%s):\n%s\n",
			                          kontakt->nick,
			                          kontakt->uin,
			                          text );
			break;
		case MESSAGE_URL:
			retmsg = g_strdup_printf( "URL from %s (%s):\n%s\n%s\n",
			                          kontakt->nick,
			                          kontakt->uin,
			                          (char *)message_text->data,
			                          text );
			break;
		case MESSAGE_FILE_REQ:
			retmsg = g_strdup_printf( "File request from %s (%s):\nAuto refusing",
			                          kontakt->nick,
			                          kontakt->uin );
			TCPRefuseFile( message_text->data ); 
			break;
		case MESSAGE_AUTH_REQ:
			retmsg = g_strdup_printf( "Authorization request from %s (%s):\nAuto refusing",
			                          kontakt->nick,
			                          kontakt->uin );
			break;
		case MESSAGE_USER_ADD:
			retmsg = g_strdup_printf( "%s (%s) has added you to his/her list.\n",
			                          kontakt->nick,
			                          kontakt->uin );
			break;
		case MESSAGE_USER_AUTHD:
			retmsg = g_strdup_printf( "%s (%s) has authorized you to add him/her to your list.\n",
			                          kontakt->nick,
			                          kontakt->uin );
			break;
		case MESSAGE_CONT_LIST:
			retmsg = g_strdup_printf( "%s (%s) sent you a contact list.\n",
			                          kontakt->nick,
			                          kontakt->uin );
			break;
		default:
			retmsg = g_strdup_printf( "Unknown message from %s (%s).\n",
			                          kontakt->nick,
			                          kontakt->uin );
			break;
	}

	u = g_htonl( strlen( retmsg ) + 1 );
	write( sock, &u, sizeof( unsigned long ) );
	write( sock, retmsg, strlen( retmsg ) + 1 );
	g_free(retmsg);
}
