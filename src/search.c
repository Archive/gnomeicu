#include "common.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "groups.h"
#include "search.h"
#include "showlist.h"
#include "util.h"
#include "v7send.h"
#include "v7snac13.h"
#include "icons.h"

#include <libgnomeui/libgnomeui.h>
#include <gtk/gtk.h>

#define not_empty(x) (x != NULL && x[0] != '\0')

static void create_tree_view (GtkWidget *tree);
void clear_search (GtkButton *button, gpointer user_data);
void execute_search (GtkButton *button, gpointer user_data);
void add_from_search (GtkButton *button, gpointer user_data);
static gboolean animate_progressbar (gpointer data);
static void start_progressbar();
static void stop_progressbar();

typedef struct {
	GtkWidget *search_dlg;
	GtkWidget *search_uin;
	GtkWidget *search_nick;
	GtkWidget *search_first;
	GtkWidget *search_last;
	GtkWidget *search_email;
        GSList    *searchids;
        GtkWidget *found_tree_view;
        GtkListStore *search_store;
        GtkWidget *group_optionmenu;
        GtkWidget *progressbar;
        int progress_timeout;
} SearchWidgets;

SearchWidgets search_widgets;

static void
contact_selected (GtkTreeSelection *sel, GtkWidget *button)
{
  gtk_widget_set_sensitive (button,
                            gtk_tree_selection_get_selected (sel, NULL, NULL));
}

void search_window( GtkWidget *widget, gpointer data )
{
	GladeXML *xml;
	GSList *ginfo;
	guint counter, general=0;
	GtkWidget *g_omenu_menu, *menuitem;
	GtkTreeSelection *sel;

	/* Only one search window at a time */
	if (search_widgets.search_dlg) {
	  gtk_window_present (GTK_WINDOW(search_widgets.search_dlg));
	  return;
	}

	xml = gicu_util_open_glade_xml ("addcontact.glade", "dlg_add_contact");
	if (xml == NULL)
		return;

       	search_widgets.search_dlg = glade_xml_get_widget (xml, "dlg_add_contact");
	search_widgets.search_uin = glade_xml_get_widget (xml, "search_uin");
	search_widgets.search_nick = glade_xml_get_widget (xml, "search_nick");
	search_widgets.search_first = glade_xml_get_widget (xml, "search_first");
	search_widgets.search_last = glade_xml_get_widget (xml, "search_last");
	search_widgets.search_email = glade_xml_get_widget (xml, "search_email");

	search_widgets.found_tree_view = glade_xml_get_widget (xml, "found_tree_view");
	create_tree_view (search_widgets.found_tree_view);

        sel = gtk_tree_view_get_selection (GTK_TREE_VIEW(search_widgets.found_tree_view));
        g_signal_connect (G_OBJECT (sel), "changed",
                          G_CALLBACK (contact_selected),
                          glade_xml_get_widget (xml, "add_button"));

	search_widgets.group_optionmenu = glade_xml_get_widget (xml, "group_optionmenu");
	g_omenu_menu = gtk_option_menu_get_menu( GTK_OPTION_MENU(search_widgets.group_optionmenu));

	search_widgets.progressbar = glade_xml_get_widget (xml, "progressbar");

	counter = 0;
	for (ginfo = Groups; ginfo != NULL; ginfo = ginfo->next, counter++) {
		GroupInfo *group = (GroupInfo *) ginfo->data;
		menuitem = gtk_menu_item_new_with_label(group->name);
		g_object_set_data(G_OBJECT(menuitem), "gid",
		                  GUINT_TO_POINTER((guint)group->gid));
		gtk_menu_shell_append(GTK_MENU_SHELL(g_omenu_menu), menuitem);
		if (!strcmp(group->name, "General"))
			general = counter;
	}
	gtk_option_menu_set_history( GTK_OPTION_MENU(search_widgets.group_optionmenu), general);
	
	g_object_set_data (G_OBJECT (glade_xml_get_widget (xml, "search_button")),
			     "dlg", glade_xml_get_widget (xml, "dlg_add_contact"));
	g_object_set_data (G_OBJECT (glade_xml_get_widget (xml, "add_button")),
			     "dlg", glade_xml_get_widget (xml, "dlg_add_contact"));

	search_widgets.progress_timeout = 0;

	glade_xml_signal_autoconnect (xml);
	gtk_widget_show_all (search_widgets.search_dlg);
}

void create_tree_view (GtkWidget *tree)
{
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;
	GtkTreeSelection *selection;

	/* Create a model */
	search_widgets.search_store = gtk_list_store_new (SEARCH_N_COLUMNS,
	                                   GDK_TYPE_PIXBUF,
	                                   G_TYPE_STRING,
	                                   G_TYPE_STRING,
	                                   G_TYPE_STRING,
	                                   G_TYPE_STRING,
	                                   G_TYPE_STRING,
	                                   G_TYPE_STRING,
	                                   G_TYPE_STRING);

	/* Create a view */
	gtk_tree_view_set_model (GTK_TREE_VIEW (tree), GTK_TREE_MODEL (search_widgets.search_store));
	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (tree), TRUE);
	g_object_unref (G_OBJECT (search_widgets.search_store));

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

	/* Create a column, associating the "text" attribute of the
	 * cell_renderer to the first column of the model */

	/* Doesnt seem to work anyways...

	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1,
	                                             "", renderer,
	                                             "pixbuf", SEARCH_STATUS_COLUMN_ICON,
	                                             NULL);
	*/
	/* create the uin column */
	renderer = gtk_cell_renderer_text_new (); 
	column = gtk_tree_view_column_new_with_attributes (_("UIN"), renderer,
							   "text", SEARCH_UIN_COLUMN,
							   NULL);
	gtk_tree_view_column_set_sort_column_id (column, SEARCH_UIN_COLUMN); 
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);

	/* create the nickname column */
	column = gtk_tree_view_column_new_with_attributes (_("Nickname"), renderer,
							   "text", SEARCH_NICKNAME_COLUMN,
							   NULL);
	gtk_tree_view_column_set_sort_column_id (column, SEARCH_NICKNAME_COLUMN); 
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);

	/* create the first-name column */
	column = gtk_tree_view_column_new_with_attributes (_("First Name"), renderer,
							   "text", SEARCH_FIRST_COLUMN,
							   NULL);
	gtk_tree_view_column_set_sort_column_id (column, SEARCH_FIRST_COLUMN); 
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);

	/* create the last-name column */
	column = gtk_tree_view_column_new_with_attributes (_("Last Name"), renderer,
							   "text", SEARCH_LAST_COLUMN,
							   NULL);
	gtk_tree_view_column_set_sort_column_id (column, SEARCH_LAST_COLUMN); 
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);

	/* create the e-mail column */
	column = gtk_tree_view_column_new_with_attributes (_("E-Mail"), renderer,
							   "text", SEARCH_EMAIL_COLUMN,
							   NULL);
	gtk_tree_view_column_set_sort_column_id (column, SEARCH_EMAIL_COLUMN); 
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);

	/* create the sex column */

	/* Doesnt seem to work anyways...

	column = gtk_tree_view_column_new_with_attributes (_("Sex"), renderer,
							   "text", SEARCH_SEX_COLUMN,
							   NULL);
	gtk_tree_view_column_set_sort_column_id (column, SEARCH_SEX_COLUMN); 
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);
	*/
	/* create the age column */

	/* Doesnt seem to work anyways...

	column = gtk_tree_view_column_new_with_attributes (_("Age"), renderer,
							   "text", SEARCH_AGE_COLUMN,
							   NULL);
	gtk_tree_view_column_set_sort_column_id (column, SEARCH_AGE_COLUMN); 
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);
	*/

	gtk_widget_show_all (tree);
}

void clear_search (GtkButton *button, gpointer user_data)
{
  
        g_slist_free(search_widgets.searchids);
        search_widgets.searchids = NULL;
  
	gtk_entry_set_text (GTK_ENTRY (search_widgets.search_uin), "");
	gtk_entry_set_text (GTK_ENTRY (search_widgets.search_nick), "");
	gtk_entry_set_text (GTK_ENTRY (search_widgets.search_first), "");
	gtk_entry_set_text (GTK_ENTRY (search_widgets.search_last), "");
	gtk_entry_set_text (GTK_ENTRY (search_widgets.search_email), "");

	gtk_list_store_clear (GTK_LIST_STORE (search_widgets.search_store));

	gtk_progress_bar_set_text (GTK_PROGRESS_BAR(search_widgets.progressbar) , "");
	if (search_widgets.progress_timeout)
	  stop_progressbar();
}


void execute_search (GtkButton *button, gpointer user_data)
{
	UIN_T uin;
	DWORD id;
	gchar *nick, *first, *last, *email;

	if (!is_connected(GTK_WINDOW(g_object_get_data (G_OBJECT (button), "dlg")), _("You can not search for people while disconnected.")))
		return;


	uin = g_strdup (gtk_entry_get_text (GTK_ENTRY (search_widgets.search_uin)));
	nick = g_strdup (gtk_entry_get_text (GTK_ENTRY (search_widgets.search_nick)));
	first = g_strdup (gtk_entry_get_text (GTK_ENTRY (search_widgets.search_first)));
	last = g_strdup (gtk_entry_get_text (GTK_ENTRY (search_widgets.search_last)));
	email = g_strdup (gtk_entry_get_text (GTK_ENTRY (search_widgets.search_email)));

	gtk_list_store_clear (GTK_LIST_STORE (search_widgets.search_store));

        g_slist_free(search_widgets.searchids);
        search_widgets.searchids = NULL;
	if (search_widgets.progress_timeout) 
	  stop_progressbar();

	if (not_empty (uin)) {
	  id = v7_findbyuin (mainconnection, uin);
	  search_widgets.searchids = g_slist_append(search_widgets.searchids,
						    GUINT_TO_POINTER(id));
	  start_progressbar();
	}

	if (not_empty (nick) || not_empty (first) || not_empty (last) ||
	    not_empty (email) ){
	  id = v7_search (mainconnection, nick, first, last, email);
	  search_widgets.searchids = g_slist_append(search_widgets.searchids,
						    GUINT_TO_POINTER(id));
	  start_progressbar();
	}

	g_free(uin);
	g_free(nick);
	g_free(first);
	g_free(last);
	g_free(email);
}

void add_from_search (GtkButton *button, gpointer user_data)
{
  gchar *nick;
  UIN_T uin;
  GtkWidget *group_menu;
  GList *menuitem;
  guint gid;

  GtkTreeIter iter;
  GtkTreeModel *model;
  GtkTreeSelection *selection;

  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (search_widgets.found_tree_view));
  if( selection == NULL )
    return;

  if (!is_connected(GTK_WINDOW(g_object_get_data (G_OBJECT (button), "dlg")), _("You can not add a contact while disconnected.")))
    return;

  if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
    gtk_tree_model_get (model, &iter,
                        SEARCH_UIN_COLUMN, &uin,
                        SEARCH_NICKNAME_COLUMN, &nick,
                        -1);
    if( uin == NULL || uin[0] == '\0' )
    {
      g_free (nick);
      return;
    }

    if( nick == NULL || nick[0] == '\0' )
      nick = g_strdup (uin);

    if (Find_User(uin) != NULL && ((CONTACT_PTR)Find_User(uin)->data)->gid > 0) {
      GtkWidget *dialog;
      dialog = gtk_message_dialog_new (GTK_WINDOW (search_widgets.search_dlg),
                                       GTK_DIALOG_DESTROY_WITH_PARENT,
                                       GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
                                       _("The selected user is already in your contact list."));
      gtk_dialog_run (GTK_DIALOG (dialog));
      gtk_widget_destroy (dialog);

      g_free (uin);
      g_free (nick);
      return;
    }

    group_menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(search_widgets.group_optionmenu));
    menuitem = g_list_nth(GTK_MENU_SHELL(group_menu)->children,
                          gtk_option_menu_get_history (GTK_OPTION_MENU(search_widgets.group_optionmenu)));
    gid = GPOINTER_TO_UINT (g_object_get_data(G_OBJECT(menuitem->data),"gid"));


    v7_try_add_uin (mainconnection, uin, nick, gid, FALSE);

    g_free (uin);
    g_free (nick);
  }
}


void  Display_Search_Reply(WORD reqid, UIN_T uin, gchar *nick, gchar *first,
                           gchar *last, gchar *email, gchar auth,
                           gchar status, gchar sex, gchar age)
{
  gchar *str_sex=NULL, *str_age;
  GdkPixbuf *pb;
  GtkTreeIter iter;
  
#ifdef TRACE_FUNCTION
  g_print( "Display_Search_Reply\n" );
#endif

  if (!g_slist_find(search_widgets.searchids, GUINT_TO_POINTER((guint)reqid)))
    return;

  
  /* Status */
  switch (status) {
  case 0:
    pb = get_pixbuf_for_status (STATUS_OFFLINE);
    break;
  case 1:
    pb = get_pixbuf_for_status (STATUS_ONLINE);
    break;
  case 2:
    pb = icon_blank_pixbuf;
    break;
  default:
    g_warning("Unknown status here: %d\n", status);
    pb = icon_blank_pixbuf;
    break;
  }
  
  /* Sex */
  if (sex) {
    if (sex == 1)
      str_sex = _("Female");
    else if (sex == 2)
      str_sex  = _("Male");
  }
  else str_sex  = "";
  
  /* Age */
  str_age = (age) ? g_strdup_printf( "%u", age ) : "";
  
  gtk_list_store_append (GTK_LIST_STORE (search_widgets.search_store), &iter);
  gtk_list_store_set (GTK_LIST_STORE (search_widgets.search_store), &iter,
                      SEARCH_STATUS_COLUMN_ICON,pb,
                      SEARCH_UIN_COLUMN, 	uin,
                      SEARCH_NICKNAME_COLUMN, 	nick,
                      SEARCH_FIRST_COLUMN, 	first,
                      SEARCH_LAST_COLUMN, 	last,
                      SEARCH_EMAIL_COLUMN, 	email,
                      SEARCH_SEX_COLUMN, 	str_sex,
                      SEARCH_AGE_COLUMN, 	str_age,
                      -1);
}


void search_finished (WORD reqid, gint16 more_results)
{
  GtkTreeIter iter;
  gchar *str;

  if (more_results > 0) {

    str = g_strdup_printf (_("%d results not displayed"), more_results);
   
    gtk_progress_bar_set_text (GTK_PROGRESS_BAR(search_widgets.progressbar),
			       str);
    g_free(str);
  } else {
    if (gtk_tree_model_get_iter_first(
            GTK_TREE_MODEL(search_widgets.search_store), &iter)) {
      gtk_progress_bar_set_text (GTK_PROGRESS_BAR(search_widgets.progressbar),
				 _("Search complete"));
    } else {
      gtk_progress_bar_set_text (GTK_PROGRESS_BAR(search_widgets.progressbar),
				 _("No matches found"));
    }
  }

  search_widgets.searchids = g_slist_remove(search_widgets.searchids,
					    GUINT_TO_POINTER((guint)reqid));
  if (search_widgets.progress_timeout && search_widgets.searchids == NULL)
    stop_progressbar();

}



gboolean animate_progressbar (gpointer data)
{
  if (search_widgets.searchids == NULL) {
    return FALSE;
  }
  
  gtk_progress_bar_pulse(GTK_PROGRESS_BAR(search_widgets.progressbar));
  
  return TRUE;
}

void start_progressbar()
{

  gtk_progress_bar_set_pulse_step(GTK_PROGRESS_BAR(search_widgets.progressbar),
				  0.1);

  gtk_progress_bar_set_text (GTK_PROGRESS_BAR(search_widgets.progressbar), "");
  

  if (!search_widgets.progress_timeout)
    search_widgets.progress_timeout = gtk_timeout_add(100, (GtkFunction)animate_progressbar, NULL);
}


void stop_progressbar() 
{
  gtk_timeout_remove(search_widgets.progress_timeout);
  search_widgets.progress_timeout = 0;
  gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(search_widgets.progressbar),
				0);
}

void closed_searchdlg(GtkWidget *widget, gpointer data)
{

#ifdef TRACE_FUNCTION
  g_print( "closed_searchdlg\n" );
#endif

  search_widgets.search_dlg = NULL;

}
