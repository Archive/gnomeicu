/*******************************   
 Handling of ICQ (sound) events   
 (c) 1999 Jeremy Wise
 GnomeICU
********************************/
   
#ifndef __ICQ_EVENTS_H__
#define  __ICQ_EVENTS_H__

#include "common.h"

typedef enum 
{
  EV_MSGRECV,   /* Message received */
  EV_USERON,    /* User logon */
  EV_USEROFF,   /* User logoff */
  EV_URLRECV,   /* URL received */
  EV_AUTHREQ,   /* Request for authorization received */
  EV_AUTH,      /* Authorize notification */
  EV_FILEREQ,   /* Incoming file */
  EV_LISTADD,   /* Added to list */
  EV_CONTLIST   /* Received Contact List */
} ICQEvType;

void gnomeicu_event( ICQEvType event, GSList *contact );

#endif /* __ICQ_EVENTS_H__ */
