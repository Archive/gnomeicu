/**************************************
  Header file for changing one's nick
  (c) 1999 Jeremy Wise
  GnomeICU
***************************************/

#ifndef __CHANGENICK_H__
#define __CHANGENICK_H__

#include <gtk/gtkwidget.h>

void change_nick_window( GtkWidget *widget, gpointer data );

#endif /* __CHANGENICK_H__ */
