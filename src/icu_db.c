/*
** yp_db.c - database functions for the maps
**
** This file is part of the NYS YP Server.
**
** The NYS YP Server is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License as
** published by the Free Software Foundation; either version 2 of the
** License, or (at your option) any later version.
**
** The NYS YP Server is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** General Public License for more details.
**
** You should have received a copy of the GNU General Public
** License along with the NYS YP Server; see the file COPYING.  If
** not, write to the Free Software Foundation, Inc., 675 Mass Ave,
** Cambridge, MA 02139, USA.
**
** Author: Thorsten Kukuk <kukuk@suse.de>
*/

#include "icu_db.h"

#if defined(HAVE_ICUDB)

#include <fcntl.h>
#include <errno.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <sys/param.h>

#if defined(HAVE_LIBGDBM)
#include <gdbm.h>
#elif defined(HAVE_NDBM)
#include <db1/ndbm.h>
#endif

#if defined(HAVE_LIBGDBM)

/* Open a GDBM database */
static GDBM_FILE
_db_open (const char *db_path, int mode)
{
  GDBM_FILE dbp;

  if (db_path && (strlen (db_path) < MAXPATHLEN))
    {
      if (mode == DB_WRITE)
	dbp = gdbm_open ((char *)db_path, 0, GDBM_WRCREAT, 0600, NULL);
      else
	dbp = gdbm_open ((char *)db_path, 0, GDBM_READER, 0, NULL);

      if (dbp == NULL)
	fprintf (stderr, "gdbm_open: GDBM Error Code #%d, %s\n",
		 gdbm_errno, gdbm_strerror(gdbm_errno));
    }
  else
    {
      dbp = NULL;
      fprintf (stderr, "Path to long: %s\n", db_path);
    }

  return dbp;
}

static inline int
_db_close (GDBM_FILE file)
{
  gdbm_close (file);
  return 0;
}

#elif defined(HAVE_NDBM)

/*****************************************************
  The following stuff is for NDBM suport !
******************************************************/

/* Open a NDBM database */
static DB_FILE
_db_open (const char *db_path, mode)
{
  DB_FILE dbp;

  if (db_path && (strlen (db_path) < MAXPATHLEN))
    {
      if (mode == DB_WRITE)
	dbp = dbm_open (db_path, O_CREAT | O_RDWR, 0600);
      else
	dbp = dbm_open (db_path, O_RDONLY, 0600);

      if (dbp == NULL)
	fprintf (stderr, "dbm_open: NDBM Error Code #%d\n", errno);
    }
  else
    {
      dbp = NULL;
      fprintf (stderr, "Path to long: %s\n", db_path);
    }

  return dbp;
}

static inline int
_db_close (DB_FILE file)
{
  dbm_close (file);
  return 0;
}

int
icudb_exists (DB_FILE dbp, datum key)
{
  datum tmp = dbm_fetch (dbp, key);

  if (tmp.dptr != NULL)
    return 1;
  else
    return 0;
}

datum
icudb_nextkey (DB_FILE file, datum key)
{
  datum tkey;

  tkey = dbm_firstkey (file);
  while ((key.dsize != tkey.dsize) ||
	   (strncmp (key.dptr, tkey.dptr, tkey.dsize) != 0))
    {
      tkey = dbm_nextkey (file);
      if (tkey.dptr == NULL)
	return tkey;
    }
  tkey = dbm_nextkey (file);

  return tkey;
}

#endif

typedef struct _fopen
{
  char *db_path;
  DB_FILE dbp;
  int flag;
  int mode;
}
Fopen, *FopenP;

#define F_OPEN_FLAG 1
#define F_MUST_CLOSE 2

/* MAX_FOPEN:
   big -> slow list searching, we go 3 times through the list.
   little -> have to close/open very often.
   We now uses 30, because searching 3 times in the list is faster
   then reopening the database.
*/
#define MAX_FOPEN 30

static int fast_open_init = -1;
static Fopen fast_open_files[MAX_FOPEN];

int
icudb_close_all (void)
{
  int i;

#ifdef TRACE_FUNCTION
  g_print("icudb_close_all\n");
#endif

  if (fast_open_init == -1)
    return 0;

  for (i = 0; i < MAX_FOPEN; i++)
    {
      if (fast_open_files[i].dbp != NULL)
	{
	  if (fast_open_files[i].flag & F_OPEN_FLAG)
	    fast_open_files[i].flag |= F_MUST_CLOSE;
	  else
	    {
	      free (fast_open_files[i].db_path);
	      _db_close (fast_open_files[i].dbp);
	      fast_open_files[i].dbp = NULL;
	      fast_open_files[i].flag = 0;
	      fast_open_files[i].mode = 0;
	    }
	}
    }
  return 0;
}

int
icudb_close_dbpath (const char *db_path)
{
  int i;

#ifdef TRACE_FUNCTION
  g_print("icudb_close_dbpath\n");
#endif

  if (fast_open_init == -1)
    return 0;

  for (i = 0; i < MAX_FOPEN; i++)
    {
      if (fast_open_files[i].dbp != NULL)
	{
	  if (db_path && strcmp (db_path, fast_open_files[i].db_path) == 0){
	    if (fast_open_files[i].flag & F_OPEN_FLAG)
	      fast_open_files[i].flag |= F_MUST_CLOSE;
  	  else
	      {
	        free (fast_open_files[i].db_path);
	         _db_close (fast_open_files[i].dbp);
	        fast_open_files[i].dbp = NULL;
	        fast_open_files[i].flag = 0;
	        fast_open_files[i].mode = 0;
	      }
    	}
      }
    }
  return 0;
}

int
icudb_close (DB_FILE file)
{
  int i;

#ifdef TRACE_FUNCTION
  g_print("icudb_close\n");
#endif

  if (fast_open_init != -1)
    {
      for (i = 0; i < MAX_FOPEN; i++)
	{
	  if (fast_open_files[i].dbp == file)
	    {
	      if (fast_open_files[i].flag & F_MUST_CLOSE)
		{
		  free (fast_open_files[i].db_path);
		  _db_close (fast_open_files[i].dbp);
		  fast_open_files[i].dbp = NULL;
		  fast_open_files[i].flag = 0;
		  fast_open_files[i].mode = 0;
		}
	      else
		{
		  fast_open_files[i].flag &= ~F_OPEN_FLAG;
                }
	      return 0;
	    }
	}
    }
  fprintf (stderr, "ERROR: Could not close file!\n");
  return 1;
}

DB_FILE
icudb_open (const char *db_path, int mode)
{
  int x;

#ifdef TRACE_FUNCTION
  g_print("icudb_open\n");
#endif

  if (!db_path)
	  return NULL;

  /* First call, initialize the fast_open_init struct */
  if (fast_open_init == -1)
    {
      fast_open_init = 0;
      for (x = 0; x < MAX_FOPEN; x++)
	{
	  fast_open_files[x].db_path = NULL;
	  fast_open_files[x].dbp = (DB_FILE) NULL;
	  fast_open_files[x].flag = 0;
	  fast_open_files[x].mode = 0;
	}
    }

  /* Search if we have already open the db_path file */
  for (x = 0; x < MAX_FOPEN; x++)
    {
      if (fast_open_files[x].dbp != NULL)
	{

	  /* Make sure that you always refer to the history files in a uniform manner.
	   * The difference between //home/user/.icq/history/uin.db and
	   * /home/user/.icq/history/uin.db is enough to break this a lot.
	   */

	  if (strcmp (db_path, fast_open_files[x].db_path) == 0)
	    {
	      /* The file is open and we know the file handle */
	      if (0)
		fprintf (stderr, "Found: %s (%d)\n",
			 fast_open_files[x].db_path, x);
			 
	      if (fast_open_files[x].flag & F_OPEN_FLAG)
                {
		  /* The file is already in use, don't open it twice.
		     I think this could never happen. */
		  fprintf (stderr, "\t%s already open.\n", db_path);
		  return NULL;
                }
	      else
		{
		  /* The file is open but not in use.
		   * If the mode is the same, then it should be safe
		   * to return the file handle.  If it differs, close
		   * the file, and reopen it. */
		  if (fast_open_files[x].mode == mode)
		    {
		      /* Mark the file as open */
		      fast_open_files[x].flag |= F_OPEN_FLAG;
		      return fast_open_files[x].dbp;
		    }
		  else
		    {
		      free (fast_open_files[x].db_path);
		      _db_close (fast_open_files[x].dbp);
		      fast_open_files[x].dbp = NULL;
		      fast_open_files[x].flag = 0;
		      fast_open_files[x].mode = 0;
		      return( icudb_open(db_path, mode) );
		    }
		}
	    }
	}
    }

  /* Search for free entry. If we do not find one, close the LRU */
  for (x = 0; x < MAX_FOPEN; x++)
    {
      if (fast_open_files[x].dbp == NULL)
	{
	  /* Good, we have a free entry and don't need to close a db_path */
	  int j;
	  Fopen tmp;
	  fast_open_files[x].db_path = strdup (db_path);
	  fast_open_files[x].flag |= F_OPEN_FLAG;
	  fast_open_files[x].mode  = mode;
	  fast_open_files[x].dbp = _db_open (db_path, mode);

	  /* LRU: put this entry at the first position, move all the other
	     one back */
	  tmp = fast_open_files[x];
	  for (j = x; j >= 1; --j)
	    fast_open_files[j] = fast_open_files[j-1];

	  fast_open_files[0] = tmp;
	  return fast_open_files[0].dbp;
	}
    }

  /* The worst thing, which could happen: no free cache entrys.
     Search the last entry, which isn't in use. */
  for (x = (MAX_FOPEN - 1); x > 0; --x)
    if ((fast_open_files[x].flag & F_OPEN_FLAG) != F_OPEN_FLAG)
      {
        int j;
	Fopen tmp;

	if (0)
	  {
	    fprintf (stderr, "Closing %s (%d)\n",
		     fast_open_files[x].db_path, x);
	    fprintf (stderr, "Opening: %s (%d)\n", db_path, x);
	  }
	free (fast_open_files[x].db_path);
	_db_close (fast_open_files[x].dbp);

	fast_open_files[x].db_path = strdup (db_path);
	fast_open_files[x].flag = F_OPEN_FLAG;
	fast_open_files[x].mode = mode;
	fast_open_files[x].dbp = _db_open (db_path, mode);

	/* LRU: Move the new entry to the first positon */
	tmp = fast_open_files[x];
	for (j = x; j >= 1; --j)
	  fast_open_files[j] = fast_open_files[j-1];

	fast_open_files[j] = tmp;
	return fast_open_files[j].dbp;
      }

  fprintf (stderr, "ERROR: Couldn't find a free cache entry!\n");

#if 0
  for (x = 0; x < MAX_FOPEN; x++)
    {
      fprintf (stderr, "Open files: %s (%d) %x (%x)\n",
	       fast_open_files[x].db_path,
	       x, fast_open_files[x].dbp,
	       fast_open_files[x].flag);
    }
#endif
  return NULL;
}


#endif /* defined(HAVE_ICUDB) */
