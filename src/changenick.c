/****************************
 Change a contacts' nickname
 (c) 1999 Jeremy Wise
 GnomeICU
*****************************/

#include "common.h"
#include "changenick.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "response.h"
#include "showlist.h"
#include "util.h"
#include "v7snac13.h"

#include <gtk/gtk.h>

/*** Local function declarations ***/
static void change_nick_response (GtkDialog *dialog, gint response, GSList *contact);

/*** Global functions ***/
void change_nick_window( GtkWidget *widget, gpointer data )
{
	GtkWidget *dialog;
	GtkWidget *vbox;
	GtkWidget *label;
	GtkWidget *entry;
	GSList *contact;

	gchar *str;

#ifdef TRACE_FUNCTION
	g_print( "change_nick_window\n" );
#endif

	contact = Contacts;
	while (contact != NULL && contact->data != data)
		contact = contact->next;

	if (contact == NULL)
		return;

	dialog = gtk_dialog_new_with_buttons (_("Rename User"),
	                                          GTK_WINDOW (MainData->window),
	                                          GTK_DIALOG_DESTROY_WITH_PARENT,
	                                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	                                          _("_Rename"), GTK_RESPONSE_OK,
	                                          NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

	gtk_container_set_border_width (GTK_CONTAINER (dialog), 6);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 6);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), vbox, FALSE, FALSE, 0);

	str = g_strdup_printf (_("_Enter a new nickname for %s (%s):"),
	                       kontakt->nick, kontakt->uin);
	label = gtk_label_new_with_mnemonic (str);
	g_free (str);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);

	entry = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (entry), kontakt->nick);
	gtk_entry_set_max_length (GTK_ENTRY (entry), 20);
	gtk_box_pack_start (GTK_BOX (vbox), entry, FALSE, FALSE, 0);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), entry);

	g_signal_connect_swapped (G_OBJECT (entry), "activate",
	                          G_CALLBACK (gtk_window_activate_default),
	                          dialog);
	
	g_object_set_data (G_OBJECT (dialog), "entry", entry);
	g_signal_connect (G_OBJECT (dialog), "response",
	                  G_CALLBACK (change_nick_response),
	                  contact);

	gtk_window_set_focus (GTK_WINDOW (dialog), entry);
	gtk_widget_show_all (dialog);
}

/*** Local functions ***/
void change_nick_response (GtkDialog *dialog, gint response, GSList *contact)
{
#ifdef TRACE_FUNCTION
	g_print( "change_nick_response\n" );
#endif

	g_return_if_fail (contact != NULL);

	if (response == GTK_RESPONSE_OK) {
		GtkWidget *entry;

		if (!is_connected(GTK_WINDOW(dialog), _("You can not change a nickname while disconnected.")))
			return;

		entry = g_object_get_data (G_OBJECT (dialog), "entry");
		g_free(kontakt->nick);
		kontakt->nick = g_strdup(gtk_entry_get_text(GTK_ENTRY(entry)));

		set_contact_status (contact, kontakt->status, FALSE);

		v7_update_nickname (mainconnection, kontakt->uin, kontakt->nick,
                                    kontakt->gid, kontakt->uid,
		                    kontakt->wait_auth);
	}
	
	gtk_widget_destroy (GTK_WIDGET (dialog));
}
