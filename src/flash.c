/***************************************
 Blink stuff, handle other timed events
 (c) 1999 Jeremy Wise
 GnomeICU
****************************************/

#include "common.h"
#include "flash.h"
#include "gnomeicu.h"
#include "icons.h"
#include "gtkfunc.h"
#include "showlist.h"

#include <gtk/gtk.h>
/* this is for the urgency hint stuff */
#include <gdk/gdkx.h>


/*** Global functions ***/
guint flash_messages( void )
{
	static gboolean flash = FALSE;
	static char prev_status = 0;
	int have_message = 0;
	XWMHints *hints;

	GSList *contact;

	flash = !flash;

	if( (Current_Status & 0xffff) == STATUS_DND )
		flash = 1;

	for (contact = Contacts; contact; contact = contact->next )
	{
		if( g_slist_length( kontakt->stored_messages ) )
		{
			int type = ((STORED_MESSAGE_PTR)kontakt->stored_messages->data)->type;
			have_message = TRUE;
			if( flash )
				gnomeicu_tree_set_contact_icon (kontakt, get_pixbuf_for_message (type));
			else
				gnomeicu_tree_set_contact_icon (kontakt, icon_blank_pixbuf);
		}
	}

	if( have_message )
	{
		gnomeicu_set_status_button (flash);
	}

	if( have_message != prev_status )
	{

		prev_status = have_message;
		gtk_window_set_title (GTK_WINDOW (MainData->window),
		                      have_message ? _("GnomeICU: Msgs") : "GnomeICU");
		gnomeicu_set_status_button (have_message ? flash : FALSE);

#if GTK_CHECK_VERSION(2,8,0)
		gtk_window_set_urgency_hint(GTK_WINDOW(MainData->window), have_message);
#else
		if (GTK_WIDGET_REALIZED(MainData->window)) {
		  hints = XGetWMHints(GDK_WINDOW_XDISPLAY(MainData->window->window), 
				      GDK_WINDOW_XWINDOW(MainData->window->window));
		  if (have_message)
		    hints->flags |= XUrgencyHint;
		  else
		    hints->flags &= ~XUrgencyHint;
		  
		  XSetWMHints(GDK_WINDOW_XDISPLAY(MainData->window->window), 
			      GDK_WINDOW_XWINDOW(MainData->window->window),
			      hints);
		  XFree(hints);
		}
#endif
		
	}

	return TRUE;
}
