/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * Functions for facilitating authorization (GUI)
 */

/*
 * Copyright (c) 2002-2003 Patrick Sung
 */

#include "common.h"

#include "gnomeicu.h"
#include "auth.h"
#include "gtkfunc.h"
#include "icons.h"
#include "util.h"
#include "v7snac13.h"
#include "personal_info.h"

#include <gtk/gtk.h>
#include <string.h>

void auth_sendbutton_clicked (GtkWidget *w, gpointer d);
void auth_cancelbutton_clicked (GtkWidget *w, gpointer d);
void auth_grantbutton_clicked (GtkWidget *w, gpointer d);
void auth_denybutton_clicked (GtkWidget *w, gpointer d);

/* display an edit box asking for authorization request message */
void
auth_request_msg_box (const gchar *nick, UIN_T uin)
{
  GladeXML *authbox;
  GtkWidget *authlabel;
  GtkWidget *authmsg;
  GtkWidget *sendbutton;
  gchar *str, *message = NULL;

#ifdef TRACE_FUNCTION
  g_print( "auth_request_msg_box\n" );
#endif

  g_assert(uin != NULL);

  authbox = gicu_util_open_glade_xml ("auth.glade", "auth_request");
  if (authbox == NULL)
    return;

  authlabel = glade_xml_get_widget (authbox, "authlabel");
  

  message = g_strdup_printf (_("User %s (%s) needs authorization. GnomeICU is going "
                               "to send an authorization message to your friend."),
                             nick ? nick : "", uin);


  str = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n%s",
                         _("Request Authorization"), message);
  gtk_label_set_markup (GTK_LABEL (authlabel), str);
  g_free (message);
  g_free (str);

  str = g_strdup_printf(_("Request Authorization from %s (%s)"), 
			    nick ? nick : "", uin);
  gtk_window_set_title(GTK_WINDOW(glade_xml_get_widget (authbox, "auth_request")),str);
  g_free(str);

  authmsg = glade_xml_get_widget (authbox, "authmsgtext");
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (authmsg), 
			       GTK_WRAP_WORD);
  str = g_strdup_printf (_("Please authorize my request and add me to your "
			   "Contact List. %s (%s)."),
			 our_info->nick, our_info->uin);
  gtk_text_buffer_set_text(GTK_TEXT_BUFFER(gtk_text_view_get_buffer(GTK_TEXT_VIEW(authmsg))), str, -1);
  g_free (str);

  g_object_set_data (G_OBJECT (glade_xml_get_widget (authbox, "cancelbutton")),
                     "dlg", glade_xml_get_widget (authbox, "auth_request"));
  g_object_set_data (G_OBJECT (glade_xml_get_widget (authbox, "sendbutton")),
                     "dlg", glade_xml_get_widget (authbox, "auth_request"));
  g_object_set_data (G_OBJECT (glade_xml_get_widget (authbox, "sendbutton")),
                     "text", authmsg); 
  g_object_set_data (G_OBJECT (glade_xml_get_widget (authbox, "sendbutton")),
                     "uin", uin);
 

  /* connect the signals for the send button and cancel button */
  glade_xml_signal_autoconnect (authbox);

  sendbutton = glade_xml_get_widget (authbox, "sendbutton");
  gtk_widget_grab_focus (sendbutton);


  g_object_unref (G_OBJECT (authbox));
}

/* auth request message box signal handler - send button */
void
auth_sendbutton_clicked (GtkWidget *w, gpointer d)
{
  GtkWidget *dialog;
  GtkWidget *text;
  GtkTextBuffer* text_buffer;
  GtkTextIter start_iter, end_iter;
  gchar *msg;

#ifdef TRACE_FUNCTION
  g_print( "auth_sendbutton_clicked\n" );
#endif

  dialog = g_object_get_data (G_OBJECT (w), "dlg");

  if (!is_connected(GTK_WINDOW(dialog), _("You can not ask authorization while disconnected.")))
    return;

  text = g_object_get_data (G_OBJECT (w), "text");
  text_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text));
  gtk_text_buffer_get_bounds(text_buffer, &start_iter, &end_iter);  
  
  msg = gtk_text_buffer_get_text(text_buffer, &start_iter, &end_iter, FALSE);

  v7_ask_uin_for_auth (mainconnection, msg, g_object_get_data(G_OBJECT(w),
							      "uin"));

  g_free (msg);
  gtk_widget_destroy (dialog);
}

/* auth request message box signal handler - cancel button */
void
auth_cancelbutton_clicked (GtkWidget *w, gpointer d)
{
  GtkWidget *dialog;

#ifdef TRACE_FUNCTION
  g_print( "auth_cancelbutton_clicked\n" );
#endif

  dialog = g_object_get_data (G_OBJECT (w), "dlg");
  gtk_widget_destroy (dialog);
}

/* auth request received from other users, show the auth receive msg box */
void
auth_receive_request (UIN_T uin, const gchar *nick, const gchar *msg)
{
  GladeXML *authbox;
  GtkWidget *grantlabel;
  GtkWidget *reqmsg;
  GtkWidget *grantbutton;
  gchar *str, *message;
  GtkTextBuffer* text_buffer;
  GSList *contact;
  GSList *ginfo;
  guint counter, general=0;
  GtkWidget *g_omenu_menu, *menuitem, *group_optionmenu;

#ifdef TRACE_FUNCTION
  g_print( "auth_receive_request\n" );
#endif

  g_assert(uin != NULL);
  authbox = gicu_util_open_glade_xml ("auth.glade", "auth_recv");
  if (authbox == NULL)
    return;

  grantlabel = glade_xml_get_widget (authbox, "grantlabel");
  message = g_strdup_printf (_("User %s (%s) is requesting authorization. Do you want to "
                               "grant the authorization so he/she can add you?"),
                             nick ? nick : "", uin);

  str = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n%s",
                         _("Received Authorization Request"), message);
  gtk_label_set_markup (GTK_LABEL (grantlabel), str);
  g_free (message);
  g_free (str);

  str = g_strdup_printf(_("Authorization request from %s"), uin);
  gtk_window_set_title(GTK_WINDOW(glade_xml_get_widget (authbox, "auth_recv")),str);
  g_free(str);
  

  reqmsg = glade_xml_get_widget (authbox, "grantmsgtext");
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (reqmsg), 
			       GTK_WRAP_WORD);
  text_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(reqmsg));
  gtk_text_buffer_set_text(text_buffer, msg, -1);

  group_optionmenu = glade_xml_get_widget (authbox, "group_optionmenu");

  g_object_set_data (G_OBJECT (glade_xml_get_widget (authbox, "denybutton")),
                     "dlg", glade_xml_get_widget (authbox, "auth_recv"));
  g_object_set_data (G_OBJECT (glade_xml_get_widget (authbox, "grantbutton")),
                     "dlg", glade_xml_get_widget (authbox, "auth_recv")); 
  g_object_set_data (G_OBJECT (glade_xml_get_widget (authbox, "grantbutton")),
                     "uin", uin);

  g_object_set_data (G_OBJECT (glade_xml_get_widget (authbox, "adduserbutton")),
                     "label", glade_xml_get_widget (authbox, "adduserlabel"));
  g_object_set_data (G_OBJECT (glade_xml_get_widget (authbox, "adduserbutton")),
		     "uin", uin);
  g_object_set_data (G_OBJECT (glade_xml_get_widget (authbox, "adduserbutton")),
		     "optionmenu", group_optionmenu );
  g_object_set_data (G_OBJECT (glade_xml_get_widget(authbox,"userinfo_button")),
		     "uin", uin);

  if ((contact = Find_User (uin)) != NULL && kontakt->inlist ) {
    gtk_widget_set_sensitive (glade_xml_get_widget (authbox, "adduserbutton"),
			      FALSE);
    gtk_widget_set_sensitive (glade_xml_get_widget (authbox, "adduserlabel"),
			      FALSE);
    gtk_widget_set_sensitive (group_optionmenu, FALSE);
  }
  gtk_image_set_from_pixbuf(GTK_IMAGE(glade_xml_get_widget (authbox, 
							    "userinfo_image")),
			    icon_info_pixbuf);

  g_omenu_menu = gtk_option_menu_get_menu( GTK_OPTION_MENU(group_optionmenu));
  
  counter = 0;
  for (ginfo = Groups; ginfo != NULL; ginfo = ginfo->next, counter++) {
    GroupInfo *group = (GroupInfo *) ginfo->data;
    menuitem = gtk_menu_item_new_with_label(group->name);
    g_object_set_data(G_OBJECT(menuitem), "gid",
		      GUINT_TO_POINTER((guint)group->gid));
    gtk_menu_shell_append(GTK_MENU_SHELL(g_omenu_menu), menuitem);
    if (!strcmp(group->name, "General"))
      general = counter;
  }
  gtk_option_menu_set_history( GTK_OPTION_MENU(group_optionmenu), general);
	

  glade_xml_signal_autoconnect (authbox);


  gtk_widget_show_all(glade_xml_get_widget (authbox, "auth_recv"));

  g_object_unref (G_OBJECT (authbox));

  /* make sure we free the allocated str malloc from auth_receive_request() */

  /* Additional features: (TODO) */
  /*  - also get user info from server using the UIN? */
  /*  - provide button to add user as well? if user is not on your list already
   *    which should be doing something similar to add_from_search() in search.c
   */
}


void
auth_grantbutton_clicked (GtkWidget *w, gpointer d)
{
  GtkWidget *dialog;
  UIN_T uin;

  dialog = g_object_get_data (G_OBJECT (w), "dlg");
  uin = g_object_get_data (G_OBJECT (w), "uin");

  if (!is_connected(GTK_WINDOW(dialog),
		    _("You can not grant authorization while disconnected.")))
    return;

  v7_grant_auth_request (mainconnection, uin);

  gtk_widget_destroy (dialog);
}

void
auth_denybutton_clicked (GtkWidget *w, gpointer d)
{
  GtkWidget *dialog = g_object_get_data (G_OBJECT (w), "dlg");

  if (!is_connected(GTK_WINDOW(dialog), _("You can not deny authorization while disconnected.")))
    return;
  gtk_widget_destroy (dialog);
}

void
auth_adduserbutton_clicked (GtkWidget *w, gpointer d)
{
  GtkWidget *option;
  GtkWidget *menu;
  GList *item;
  guint gid;
  UIN_T uin;
  
  if (!is_connected(GTK_WINDOW(g_object_get_data (G_OBJECT (w), "dlg")), _("You can not add a contact while disconnected.")))
    return;
  
  option = g_object_get_data (G_OBJECT (w), "optionmenu");
  menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (option));
  item = g_list_nth (GTK_MENU_SHELL (menu)->children,
		     gtk_option_menu_get_history (GTK_OPTION_MENU (option)));
  gid = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (item->data), "gid"));
  uin = g_object_get_data (G_OBJECT (w), "uin");

  v7_try_add_uin (mainconnection, uin, uin, gid, FALSE);


  gtk_widget_set_sensitive(w, FALSE);
  gtk_widget_set_sensitive(g_object_get_data (G_OBJECT (w), "label"), FALSE);
  gtk_widget_set_sensitive(option, FALSE);  
}

void
auth_userinfobutton (GtkWidget *w, gpointer d)
{

  dump_personal_info(g_object_get_data(G_OBJECT(w), "uin"));

}
