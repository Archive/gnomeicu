#include "common.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "invisible.h"
#include "listwindow.h"
#include "v7send.h"
#include "v7snac13.h"
#include "groups.h"
#include "gnomecfg.h"

#include <gtk/gtk.h>
#include <string.h>

static gboolean is_invisible (CONTACT_PTR contact)
{
  return (contact ? (contact->invis_list && !contact->ignore_list) : FALSE);
}

static void invisible_add(Contact_Member *contact)
{
  v7_invisible (mainconnection, contact->uin,
		contact_gen_uid (),
		TRUE); /* add */ 
		
  if (contact->vis_list == TRUE) {
    v7_visible (mainconnection, contact->uin, 
		contact->vislist_uid, FALSE); /* remove */
  }                                
}

static void invisible_remove(Contact_Member *contact)
{
  v7_invisible (mainconnection, contact->uin, contact->invlist_uid,
		FALSE); /* remove */
}

void invisible_list_dialog( void )
{
	static GtkWidget *dlg = NULL;
	
	if( dlg == NULL )
	{
		dlg = list_window_new_filter(
			_("Invisible List"),
			_("Drag the users you wish to add to your\n"
			  "invisible list into this window"),
			invisible_add,
			invisible_remove,
			is_invisible);
		g_object_add_weak_pointer (G_OBJECT (dlg), (gpointer *)&dlg);
	} else {
		gtk_window_present (GTK_WINDOW (dlg));
	}
}
