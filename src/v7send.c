/****************************
  Sending stuff for ICQ v6-7 protocol (Oscar)
  Olivier Crete (c) 2001-2002
  GnomeICU
*****************************/

#include "common.h"
#include "gnomeicu.h"
#include "gnomeicu-spinner.h"
#include "gtkfunc.h"
#include "history.h"
#include "sendcontact.h"
#include "util.h"
#include "v7recv.h"
#include "v7send.h"
#include "v7snac13.h"

#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <libgnomeui/libgnomeui.h>
#include <sys/stat.h>

static void v7_sendmsg_rtf(V7Connection *conn, UIN_T uin, const guchar *text);
static void v7_send_type1_msg2(V7Connection *conn, UIN_T uin, const gchar *msg);
static void v7_sendmsg4(V7Connection *conn, UIN_T uin, gchar *msgcontent, int contentlen);
static DWORD v7_send_15_02_D007( V7Connection *conn, WORD subtype, gchar *buffer, int buffer_len);
static void v7_send_simple_msg2(V7Connection *conn, UIN_T uin, const gchar *msg, BYTE type, gboolean autoreq);
static void v7_send_file_msg2(V7Connection *conn, UIN_T uin, const gchar *msg,
			      WORD port, DWORD ip, gboolean filereq,
			      const gchar *filename, DWORD filesize, 
			      gchar *msgid, WORD downcounter);

const gchar zerobuf[20] = "\0\0\0\0\0\0\0\0\0\0" "\0\0\0\0\0\0\0\0\0\0";

void v7_sendmsg(V7Connection *conn, UIN_T uin, const guchar *text) 
{
  GSList *contact;

  contact = Find_User(uin);

  if (contact == NULL)
    return;

  history_add_outgoing( uin, text );

  if (kontakt->has_rtf)
    v7_sendmsg_rtf(conn, uin, text);
  else
    v7_sendmsg_plain(conn, uin, text);
}


void v7_sendmsg_plain(V7Connection *conn, UIN_T uin, const guchar *text) 
{

  TLVstack *mainpak, *message;
  guchar *tempstr, *convtext;

#ifdef TRACE_FUNCTION
  g_print( "v7_sendmsg\n" );
#endif

  mainpak = new_tlvstack( zerobuf,8); /* msgid */
  add_nontlv_w_be(mainpak, 1);        /* msg-format  1==simple*/

  tempstr = g_strdup_printf(" %s", uin);      /* uin */;
  tempstr[0] = strlen(tempstr) - 1;
  add_nontlv(mainpak, tempstr, tempstr[0]+1);
  g_free(tempstr);

  message = new_tlvstack( "\x05\x01\x00\x01\x01\x01\x01",7); /* fill ? */

  tempstr = convert_from_utf8(text);

  convtext = add_cr(tempstr);
  g_free(tempstr);
    

  add_nontlv_w_be(message, strlen(convtext)+4);           /* msg length +4*/
  add_nontlv(message, zerobuf, 4);                        /* fill ? */
  add_nontlv(message, convtext, strlen(convtext));        /* message */
  g_free(convtext);

  add_tlv(mainpak, 2, message->beg, message->len);
  free_tlvstack(message);
  
  add_tlv(mainpak, 6, NULL, 0);
  
  snac_send(conn, mainpak->beg, mainpak->len, FAMILY_04_MESSAGING,
            F04_CLIENT_SEND_MESSAGE, NULL, ANY_ID);

  free_tlvstack(mainpak);
}

void v7_sendmsg_rtf(V7Connection *conn, UIN_T uin, const guchar *text)
{
  GString *string;
  gunichar unicodetemp;
  const guchar *newtext;

  string = g_string_new("{\\rtf1\\ansi\\ansicpg1200\\deff0\\deflang4105{\\fonttbl {\\f0\\fmodern\\fprq1\\fcharset0 Sans;}}"
			"{\\colortbl ;\\red0\\green0\\blue0;\\red255\\green255\\blue255;}"
			"\\uc1\\pard\\cf1\\highlight0\\ulnone\\f0 ");
  
  for (newtext=text; *newtext != 0;newtext=g_utf8_next_char(newtext)) { 
    if (*newtext < 128)
      if (*newtext == '\\' )
	g_string_append(string, "\\\\ ");
      else if (*newtext == '{' )
	g_string_append(string, "\\{ ");
      else if (*newtext == '}' )
	g_string_append(string, "\\} ");
      else if (*newtext == '\n' )
	g_string_append(string, "\\par\r\n");
      else
	g_string_append_c(string, *newtext);
    else {
      unicodetemp = g_utf8_get_char(newtext);
      g_string_append_printf(string, "\\u%d?", unicodetemp);
    }
    
  } 
  g_string_append(string, "\\highlight0\\par\r\n}\r\n");

  v7_send_type1_msg2(conn, uin, string->str);

  g_string_free(string, TRUE);
}

void v7_setstatus(V7Connection *conn, DWORD newstatus)
{
#ifdef TRACE_FUNCTION
  g_print( "v7_setstatus\n" );
#endif

  if (newstatus == Current_Status)
    return;

  if (!isdigit (our_info->uin[0])) {
	if (newstatus == STATUS_ONLINE || newstatus == STATUS_FREE_CHAT)
	  v7_aimsetstatus (mainconnection, NULL);
	Current_Status = newstatus;
	gnomeicu_set_status_button (FALSE);
  	return;
  }

  /* Send visible/invisible list.. icq2001b style */
  if (newstatus == STATUS_INVISIBLE) {
    v7_send_status_server (conn, FALSE); /* invisible */
  } else {
    v7_send_status_server (conn, TRUE); /* visible */
  }

  Current_Status = newstatus;

  v7_setstatus2(conn, newstatus);
}

void v7_setstatus2(V7Connection *conn, DWORD newstatus)
{
  TLVstack *tlvs;
  gchar directconnectioninfo[37];
  time_t today;
  struct tm *today2;

  newstatus |= (our_info->webpresence ? 0x10000 : 0) |
    0x20000 | 0x10000000;


  /* Check birthday to set the flag */

  today = time(NULL);
  today2 = localtime(&today);

  if(our_info->birth_day == 0)
    v7_request_our_info(mainconnection);

  if((our_info->birth_day == today2->tm_mday) && 
     (our_info->birth_month == today2->tm_mon+1))
      newstatus |=  0x80000;

  tlvs = new_tlvstack(NULL,0);
  
  add_tlv_dw_be(tlvs, 6, newstatus);

  bzero(directconnectioninfo, 37);
  /*
    Direct connection aint working anyways... so we disable it by setting the
    IP to zero
  */
    
  DW_2_CharsBE(directconnectioninfo, our_ip);
  DW_2_CharsBE(directconnectioninfo+4, our_port);

  directconnectioninfo[8] = 4; /* is 1 is firewall -- 4 is normal*/


  //  directconnectioninfo[10] = 8; /* protocol v8=2001b v7=2000b */
  /* DW_2_CharsBE(directconnectioninfo+15, 0x50); no web front */
  //  DW_2_CharsBE(directconnectioninfo+19, 3);
  

  add_tlv(tlvs, 0x0C, directconnectioninfo, 37);
  
  snac_send(conn, tlvs->beg, tlvs->len, FAMILY_01_GENERIC, 
            F01_CLIENT_STATUS_CODE, NULL, ANY_ID);
  
  free_tlvstack(tlvs);

  gnomeicu_set_status_button (FALSE);
}

void v7_quit()
{

  GList *list;

#ifdef TRACE_FUNCTION
  g_print( "v7_quit\n" );
#endif 

  if (mainconnection) {
    
    if (mainconnection->in_watch)
      if (!g_source_remove(mainconnection->in_watch)) {
        gnome_error_dialog(_("Cannot remove main input watcher!"));
        return;
      }
    
    if (mainconnection->err_watch)
      if (!g_source_remove(mainconnection->err_watch)) {
        gnome_error_dialog(_("Cannot remove main error watcher"));
        return;
      }


    if (mainconnection->iochannel)
      g_io_channel_shutdown(mainconnection->iochannel, TRUE, NULL);

    list = mainconnection->queued_packets;
    while (list) {
      gtk_timeout_remove(((RateLimitedResendData*)list->data)->timerid);
      g_free(((RateLimitedResendData*)list->data)->buffer);
      g_free(list->data);
      list = list->next;
    }
    g_list_free(mainconnection->queued_packets);
    
    list = mainconnection->rateclasses;
    while (list) {
      g_free(list->data);
      list = list->next;
    }
    g_list_free(mainconnection->rateclasses);
    
    if (mainconnection->familyclass)
      g_hash_table_destroy(mainconnection->familyclass);
    
    g_free(mainconnection);
    
    mainconnection = NULL;
  }
  
  while (otherconnections) {
    
    if (((V7Connection*)otherconnections->data)->in_watch)
      if (!g_source_remove(((V7Connection*)otherconnections->data)->in_watch)){
        gnome_error_dialog(_("Cannot remove other input watcher!"));
        return;
      }
    if (((V7Connection*)otherconnections->data)->err_watch)
     if (!g_source_remove(((V7Connection*)otherconnections->data)->err_watch)){
        gnome_error_dialog(_("Cannot remove other error watcher!"));
        return;
     }
    if (((V7Connection*)otherconnections->data)->iochannel)
      g_io_channel_shutdown(((V7Connection*)otherconnections->data)->iochannel,
			    TRUE, NULL);
    
    g_free(otherconnections->data);
    otherconnections = g_slist_remove(otherconnections, otherconnections->data);
    
  }
  
  isconnecting = FALSE;

  gnomeicu_spinner_stop();
}

void v7_sendurl(V7Connection *conn, UIN_T uin, gchar *url, gchar *desc)
{
  gchar *buffer;
  int bufferlen;
  gchar *converteddesc, *convertedurl;
  
#ifdef TRACE_FUNCTION
  g_print( "v7_sendurl\n" );
#endif 

  converteddesc = convert_from_utf8(desc);
  convertedurl = convert_from_utf8(url);

  buffer = g_malloc0(strlen(convertedurl)+ strlen(converteddesc) + 10);

  DW_2_Chars(buffer, atoi (our_info->uin));

  buffer[4] = 4;  /* type 4 = url */
  Word_2_Chars(buffer+6, strlen(convertedurl)+strlen(converteddesc)+2);

  g_memmove(buffer+8, converteddesc, strlen(converteddesc));
  bufferlen = 8 + strlen(converteddesc);

  buffer[bufferlen] = 0xFE;
  bufferlen++;

  g_memmove(buffer+bufferlen, convertedurl, strlen(convertedurl));
  bufferlen += strlen(convertedurl) + 1;

  v7_sendmsg4(conn, uin, buffer, bufferlen);
  
  g_free(buffer);
  g_free(converteddesc);
  g_free(convertedurl);
}

void v7_grant_auth(V7Connection *conn, UIN_T uin)
{

#ifdef TRACE_FUNCTION
  g_print( "v7_grant_auth\n" );
#endif 

  /* type 8 = grant auth */

  v7_send_simple_msg2(conn, uin, zerobuf, 8, FALSE);
}

void v7_deny_auth(V7Connection *conn, UIN_T uin, gchar *reason)
{

  gchar *buffer;
  gchar *conv_text;

#ifdef TRACE_FUNCTION
  g_print( "v7_deny_auth\n" );
#endif 

  conv_text = convert_from_utf8(reason);

  buffer = g_malloc0(strlen(conv_text) + 9);
  DW_2_Chars(buffer, atoi(our_info->uin));

  buffer[4] = 7;  /* type 7 = deny auth */

  Word_2_Chars(buffer+6, strlen(conv_text));

  g_memmove(buffer+8, conv_text, strlen(conv_text));

  v7_sendmsg4(conn, uin, buffer, strlen(conv_text) + 9);

  g_free(conv_text);

  g_free(buffer);
}

void v7_request_away_msg(V7Connection *conn, UIN_T uin, WORD contactstatus)
{


#ifdef TRACE_FUNCTION
  g_print( "v7_request_away_msg\n" );
#endif 

  switch (contactstatus) { /* find which type of message to request */
  case STATUS_AWAY: 
    v7_send_simple_msg2(conn, uin, "\0", 0xE8, TRUE);
    break;
  case STATUS_OCCUPIED:
    v7_send_simple_msg2(conn, uin, "\0", 0xE9, TRUE);
    break;
  case STATUS_NA:
    v7_send_simple_msg2(conn, uin, "\0", 0xEA, TRUE);
    break;
  case STATUS_DND:
    v7_send_simple_msg2(conn, uin, "\0", 0xEB, TRUE);
    break;
  case STATUS_FREE_CHAT:
    v7_send_simple_msg2(conn, uin, "\0", 0xEC, TRUE);
    break;
  default:
	 g_warning("Did not request away message because of an unknown status: 0x%x\n", contactstatus);
    return;
  }
}


void v7_sendmsg4(V7Connection *conn, UIN_T uin, gchar *msgcontent, int contentlen)
{
  gchar *buffer;
  TLVstack *tlvs;
  int bufferlen;
  
#ifdef TRACE_FUNCTION
  g_print( "v7_sendmsg4\n" );
#endif 


  bufferlen =  11+strlen(uin);
  buffer = g_malloc0(bufferlen);

  Word_2_CharsBE(buffer+8, 4); /* msg-format=4 special messages */

  buffer[10] = strlen(uin);
  g_memmove(buffer+11, uin, buffer[10]);
  
    
  tlvs = new_tlvstack(buffer, bufferlen);

  g_free(buffer);

  add_tlv(tlvs, 5, msgcontent, contentlen);
  
  snac_send(conn, tlvs->beg, tlvs->len ,FAMILY_04_MESSAGING,
            F04_CLIENT_SEND_MESSAGE, NULL, ANY_ID);
  
  free_tlvstack(tlvs);
  
}


void v7_send_simple_msg2(V7Connection *conn, UIN_T uin, const gchar *msg, BYTE type, gboolean autoreq)
{
  gchar *buffer;
  /* We have three levels of TLVS one within the other, its not nice at all,
     it should be done in a cleaner way some day... */
  TLVstack *outertlvs, *middletlvs, *innertlvs;
  int bufferlen;
  
#ifdef TRACE_FUNCTION
  g_print( "v7_send_simple_msg2\n" );
#endif 
 
  middletlvs = new_tlvstack("\0\0" "\041\032\031\042" "\063\035\063\024"
                           CAP_AIM_SERVERRELAY, 16+8+2);

  
  add_tlv(middletlvs, 0x0A, "\x00\x01", 2);
  add_tlv(middletlvs, 0x0F, NULL, 0);
  
  innertlvs = new_tlvstack("\x1B\0", 2);
  
  add_nontlv(innertlvs, "\x08", 1); /* protocol version 8 */
  add_nontlv(innertlvs, zerobuf, 19);
  add_nontlv_w_le(innertlvs, 0x03);
  add_nontlv(innertlvs, zerobuf, 3);
  add_nontlv_w_le(innertlvs, --conn->msg2_downcounter);
  add_nontlv_w_be(innertlvs, 0x0E00);
  add_nontlv_w_le(innertlvs, conn->msg2_downcounter);
  add_nontlv(innertlvs, zerobuf, 12);
  add_nontlv(innertlvs, &type, 1);
  add_nontlv(innertlvs, autoreq ? "\x03": zerobuf, 1);
  /* first word of status code */
  add_nontlv_w_le(innertlvs, (our_info->webpresence ? 0x10000 : 0) 
		  | 0x20000 | 0x1000000);
  add_nontlv_w_le(innertlvs, 1);
  add_nontlv_w_le(innertlvs, strlen(msg)+1);
  add_nontlv(innertlvs, msg, strlen(msg)+1);

  add_tlv(middletlvs, 0x2711, innertlvs->beg, innertlvs->len);
  
  free_tlvstack(innertlvs);
  
  bufferlen =  11+strlen(uin);
  buffer = g_malloc0(bufferlen);


  g_memmove(buffer, middletlvs->beg+2, 8);  /* copy msgid */
  Word_2_CharsBE(buffer+8, 2); /* msg-format=2 advanced messages */

  buffer[10] = strlen(uin);  /* LNTS */
  g_memmove(buffer+11, uin, buffer[10]);
  
  outertlvs = new_tlvstack(buffer, bufferlen);

  g_free(buffer);

  add_tlv(outertlvs, 5, middletlvs->beg, middletlvs->len);

  free_tlvstack(middletlvs);
  
  add_tlv(outertlvs, 3, NULL, 0); /* ACK request */
  
  snac_send(conn, outertlvs->beg, outertlvs->len ,FAMILY_04_MESSAGING,
            F04_CLIENT_SEND_MESSAGE, NULL, ANY_ID);
  
  free_tlvstack(outertlvs);
  
}

void v7_send_type1_msg2(V7Connection *conn, UIN_T uin, const gchar *msg)
{
  gchar *buffer;
  /* We have three levels of TLVS one within the other, its not nice at all,
     it should be done in a cleaner way some day... */
  TLVstack *outertlvs, *middletlvs, *innertlvs;
  int bufferlen;
  
#ifdef TRACE_FUNCTION
  g_print( "v7_send_simple_msg2\n" );
#endif 
 
  middletlvs = new_tlvstack("\0\0" "\041\032\031\042" "\063\035\063\024"
                            CAP_AIM_SERVERRELAY, 16+8+2);

  
  add_tlv(middletlvs, 0x0A, "\x00\x01", 2);
  add_tlv(middletlvs, 0x0F, NULL, 0);
  
  innertlvs = new_tlvstack("\x1B\0", 2);
  
  add_nontlv(innertlvs, "\x08", 1); /* protocol version 8 */
  add_nontlv(innertlvs, zerobuf, 19);
  add_nontlv_w_le(innertlvs, 0x03);
  add_nontlv(innertlvs, zerobuf, 3);
  add_nontlv_w_le(innertlvs, --conn->msg2_downcounter);
  add_nontlv_w_be(innertlvs, 0x0E00);
  add_nontlv_w_le(innertlvs, conn->msg2_downcounter);
  add_nontlv(innertlvs, zerobuf, 12);
  add_nontlv(innertlvs, "\x01", 1);
  add_nontlv(innertlvs, zerobuf, 1);
  /* first word of status code */
  add_nontlv_w_le(innertlvs, (our_info->webpresence ? 0x10000 : 0) 
		  | 0x20000 | 0x1000000);
  add_nontlv_w_le(innertlvs, 1);
  add_nontlv_w_le(innertlvs, strlen(msg)+1);
  add_nontlv(innertlvs, msg, strlen(msg)+1);

  add_nontlv_dw_be(innertlvs,0);              /* foreground R,G,B,0 */
  add_nontlv_dw_be(innertlvs,0xFFFFFF00);     /* background R,G,B,0 */
  add_nontlv_dw_le(innertlvs,38);
  add_nontlv(innertlvs,"{97B12751-243C-4334-AD22-D6ABF73F1492}",38);

  add_tlv(middletlvs, 0x2711, innertlvs->beg, innertlvs->len);
  
  free_tlvstack(innertlvs);
  
  bufferlen =  11+strlen(uin);
  buffer = g_malloc0(bufferlen);


  g_memmove(buffer, middletlvs->beg+2, 8);  /* copy msgid */
  Word_2_CharsBE(buffer+8, 2); /* msg-format=2 advanced messages */

  buffer[10] = strlen(uin);  /* LNTS */
  g_memmove(buffer+11, uin, buffer[10]);
  
  outertlvs = new_tlvstack(buffer, bufferlen);

  g_free(buffer);

  add_tlv(outertlvs, 5, middletlvs->beg, middletlvs->len);

  free_tlvstack(middletlvs);
  
  //  add_tlv(outertlvs, 3, NULL, 0); /* ACK request */

  
  snac_send(conn, outertlvs->beg, outertlvs->len ,FAMILY_04_MESSAGING,
            F04_CLIENT_SEND_MESSAGE, NULL, ANY_ID);
  
  free_tlvstack(outertlvs);
  
}

void v7_send_file_msg2(V7Connection *conn, UIN_T uin, const gchar *msg,
		       WORD port, DWORD ip, gboolean filereq,
		       const gchar *filename, DWORD filesize, gchar *msgid,
		       WORD downcounter)
		    
{
  gchar *buffer;
  /* We have three levels of TLVS one within the other, its not nice at all,
     it should be done in a cleaner way some day... */
  TLVstack *outertlvs, *middletlvs, *innertlvs, *ininnertlvs;
  int bufferlen;
  
#ifdef TRACE_FUNCTION
  g_print( "v7_send_file_msg2\n" );
#endif 
 
  bufferlen =  11+strlen(uin);
  buffer = g_malloc0(bufferlen);


  g_memmove(buffer, msgid,  8);

  Word_2_CharsBE(buffer+8, 2); /* msg-format=2 advanced messages */

  buffer[10] = strlen(uin);  /* LNTS */
  g_memmove(buffer+11, uin, buffer[10]);
  
  outertlvs = new_tlvstack(buffer, bufferlen);

  g_free(buffer);


  middletlvs = new_tlvstack("\0\0", 2);  /* might be 0002 for file-ack */

  add_nontlv(middletlvs, outertlvs->beg, 8); /* copy msgid */
  add_nontlv(middletlvs, CAP_AIM_SERVERRELAY, 16);

  if (filereq)
    add_tlv(middletlvs, 0x0A, "\x00\x01", 2);
  else
    add_tlv(middletlvs, 0x0A, "\x00\x02", 2);
  add_tlv(middletlvs, 0x0F, NULL, 0);
  add_tlv(middletlvs, 0x0E, "en", 2);
  
  add_tlv_dw_be(middletlvs, 0x02, our_ip);; /* proxy ip */
  add_tlv_dw_be(middletlvs, 0x16, ~our_ip);; /* proxy ip check*/
  add_tlv_dw_be(middletlvs, 0x03, our_ip);
  //add_tlv(middletlvs, 0x03, "\xa1\xb8\xa5\x2a", 4);
  add_tlv_w_be(middletlvs, 0x05, port);  /* port */
  add_tlv_w_be(middletlvs, 0x17, ~port);  /* port check */

  innertlvs = new_tlvstack("\x1B\0", 2);
  
  add_nontlv_w_le(innertlvs, 8); /* protocol version 8 */
  add_nontlv(innertlvs, zerobuf, 18);
  add_nontlv_dw_le(innertlvs, 0x03);
  add_nontlv(innertlvs, "\x04", 1); /* 1==firewall 4==normal */
  if (filereq)
    add_nontlv_w_le(innertlvs, --conn->msg2_downcounter);
  else
    add_nontlv_w_le(innertlvs, downcounter);
  add_nontlv_w_be(innertlvs, 0x0E00);
  if (filereq)
    add_nontlv_w_le(innertlvs, conn->msg2_downcounter);
  else
    add_nontlv_w_le(innertlvs, downcounter);
  add_nontlv(innertlvs, zerobuf, 12);
  add_nontlv_w_le(innertlvs, 0x1A);
  /* first word of status code */
  add_nontlv_w_le(innertlvs, (our_info->webpresence ? 0x10000 : 0 ) 
		  | 0x20000 | 0x1000000);

  if (filereq)
    add_nontlv_w_le(innertlvs, 1);
  else
    add_nontlv_w_le(innertlvs, 0);
  add_nontlv_w_le(innertlvs, 1);
  add_nontlv(innertlvs, zerobuf, 1);

  add_nontlv_w_le(innertlvs, 0x29);
  add_nontlv(innertlvs, "\xf0\x2d\x12\xd9" "\x30\x91\xd3\x11"
	     "\x8d\xd7\x00\x10" "\x4b\x06\x46\x2e" "\x00\x00\x04"
	     "\x00\x00\x00" "File" "\x00\x00\x01" "\x00\x00\x01"
	     "\x00\x00\x00\x00" "\x00\x00\x00\x00\x00", 0x29);

  ininnertlvs = new_tlvstack(NULL, 0);

  add_nontlv_dw_le(ininnertlvs, strlen(msg));
  add_nontlv(ininnertlvs, msg, strlen(msg));
  if (filereq)
    add_nontlv (ininnertlvs, zerobuf, 4); /* data I dont understand */
  else {
    add_nontlv_w_be (ininnertlvs, port);
    add_nontlv (ininnertlvs, zerobuf, 2);
  }
  add_nontlv_lnts(ininnertlvs, filename);
  add_nontlv_dw_le(ininnertlvs, filesize);
  if (filereq)
    add_nontlv (ininnertlvs, zerobuf, 4); /* data I dont understand */
  else {
    add_nontlv_w_le(ininnertlvs, port);
    add_nontlv (ininnertlvs, zerobuf, 2);
  }

  add_nontlv_dw_le(innertlvs, ininnertlvs->len);
  add_nontlv(innertlvs, ininnertlvs->beg, ininnertlvs->len);

  free_tlvstack(ininnertlvs);

  add_tlv(middletlvs, 0x2711, innertlvs->beg, innertlvs->len);
  
  free_tlvstack(innertlvs);
  
  add_tlv(outertlvs, 5, middletlvs->beg, middletlvs->len);

  free_tlvstack(middletlvs);
  
  add_tlv (outertlvs, 3, NULL, 0); /* ACK request */
  
  snac_send(conn, outertlvs->beg, outertlvs->len ,FAMILY_04_MESSAGING,
            F04_CLIENT_SEND_MESSAGE, NULL, ANY_ID);
  
  free_tlvstack(outertlvs);
  
}




/* returns a reqid from inside the 15/2 packets */

DWORD v7_findbyuin(V7Connection *conn, UIN_T uin)
{
 
  BYTE buffer[8] = "\x36\x01\x04\0";
  
#ifdef TRACE_FUNCTION
  g_print( "v7_findbyuin\n" );
#endif 
  
  DW_2_Chars(buffer+4, atoi (uin));
  
  return v7_send_15_02_D007(conn,  0x6905, buffer, 8);
}

/* returns a reqid from inside the 15/2 packets */

DWORD v7_search (V7Connection *conn, gchar *nick, gchar *firstname,
		 gchar *lastname, gchar *email)
{
  DWORD reqid;
  TLVstack *tlvs;

#ifdef TRACE_FUNCTION
  g_print( "v7_search\n" );
#endif 

  tlvs = new_tlvstack(NULL, 0);

  if (firstname && firstname[0] != '\0') {
    add_nontlv_w_be(tlvs, 0x4001);
    add_nontlv_w_le(tlvs, strlen(firstname)+3);
    add_nontlv_w_le(tlvs, strlen(firstname)+1);
    add_nontlv(tlvs, firstname, strlen(firstname)+1);
  }
  
  if (lastname && lastname[0] != '\0') {
    add_nontlv_w_be(tlvs, 0x4A01);
    add_nontlv_w_le(tlvs, strlen(lastname)+3);
    add_nontlv_w_le(tlvs, strlen(lastname)+1);
    add_nontlv(tlvs, lastname, strlen(lastname)+1);
  }

  if (nick && nick[0] != '\0') {
    add_nontlv_w_be(tlvs, 0x5401);
    add_nontlv_w_le(tlvs, strlen(nick)+3);
    add_nontlv_w_le(tlvs, strlen(nick)+1);
    add_nontlv(tlvs, nick, strlen(nick)+1);
  }

  
  if (email && email[0] != '\0') {
    add_nontlv_w_be(tlvs, 0x5E01);
    add_nontlv_w_le(tlvs, strlen(email)+3);
    add_nontlv_w_le(tlvs, strlen(email)+1);
    add_nontlv(tlvs, email, strlen(email)+1);
  }
 
  reqid = v7_send_15_02_D007(conn, 0x5F05, tlvs->beg, tlvs->len);
  
  free_tlvstack(tlvs);
  return reqid;
}

/* returns reqid from inside the 15/2 packets */

DWORD v7_send_15_02_D007(V7Connection *conn, WORD subtype, gchar *buffer, int buffer_len)
{

  DWORD reqid;
  TLVstack *outer_tlvs, *inner_tlvs;

#ifdef TRACE_FUNCTION
  g_print( "v7_send_15_02_D007\n" );
#endif 
  if (!mainconnection)
    return 0;

  
  inner_tlvs = new_tlvstack("  ", 2);

  add_nontlv_dw_le(inner_tlvs, atoi (our_info->uin));
  add_nontlv_w_be(inner_tlvs, 0xD007);
  add_nontlv_w_le(inner_tlvs, 0);
  add_nontlv_w_be(inner_tlvs, subtype);
  add_nontlv(inner_tlvs, buffer, buffer_len);

  Word_2_Chars(inner_tlvs->beg, inner_tlvs->len-2);

  outer_tlvs = new_tlvstack(NULL, 0);
  add_tlv(outer_tlvs, 1, inner_tlvs->beg, inner_tlvs->len);

  free_tlvstack(inner_tlvs);
  
  reqid = snac_send(conn, outer_tlvs->beg, outer_tlvs->len,
            FAMILY_15_SAVED_DATA, F15_CLIENT_REQ_SAVE_DATA ,NULL, ANY_ID);
  
  free_tlvstack(outer_tlvs);

  return reqid;

}

DWORD v7_sendxml (V7Connection *conn, gchar *xmlstring)
{

  DWORD reqid;
  gchar *buffer;

#ifdef TRACE_FUNCTION
  g_print( "v7_sendxml\n" );
#endif 

  buffer = g_strdup_printf("  %s", xmlstring);

  Word_2_Chars(buffer, strlen(xmlstring)+1);

  reqid = v7_send_15_02_D007(conn,  0x9808, buffer, strlen(xmlstring)+3); 
  
  g_free(buffer);

  return reqid;
  
}

DWORD v7_request_info (V7Connection *conn, UIN_T uin)
{

  Request *inforeq;
  BYTE buffer[4];
  
#ifdef TRACE_FUNCTION
  g_print( "v7_request_info\n" );
#endif 

  DW_2_Chars(buffer, atoi (uin));
  
  inforeq = g_new0(Request, 1);
  
  requests = g_slist_append(requests ,inforeq);
  
  inforeq->type = FULL_INFO;
  inforeq->uin = g_strdup (uin);

  inforeq->reqid = v7_send_15_02_D007(conn,  0xB204, buffer, 4);
  

  return inforeq->reqid;

}

DWORD v7_request_our_info(V7Connection *conn)
{

  Request *inforeq;
  BYTE buffer[4];
  
#ifdef TRACE_FUNCTION
  g_print( "v7_request_our_info\n" );
#endif 
  
  DW_2_Chars(buffer, atoi (our_info->uin));
  
  inforeq = g_new0(Request, 1);
  
  requests = g_slist_append(requests ,inforeq);
  
  inforeq->type = OUR_INFO;
  inforeq->uin = g_strdup (our_info->uin);

  inforeq->reqid = v7_send_15_02_D007(conn, 0xD004, buffer, 4);

  return inforeq->reqid;

}

void v7_modify_about (V7Connection *conn, gchar *about)
{
  gchar *buffer;
  gchar *convabout;

#ifdef TRACE_FUNCTION
  g_print( "v7_modify_about\n" );
#endif

  convabout = convert_from_utf8(about);

  buffer = g_strdup_printf("  %s", convabout);
  
  Word_2_Chars(buffer, strlen(convabout)+1);

  v7_send_15_02_D007(conn, 0x0604, buffer, strlen(convabout)+3);
  
  g_free(convabout);

  g_free(buffer);
}

void v7_modify_work_info (V7Connection *conn, gchar *city, gchar *state,
			  gchar *phone, gchar *fax,
			  gchar *street, gchar *zip, WORD country,
			  gchar *company_name, gchar *department,
			  gchar *position, WORD occupation,
			  gchar *webpage)
{

  TLVstack *infotlvs;
  gchar *temp;

#ifdef TRACE_FUNCTION
  g_print( "v7_modify_work_info\n" );
#endif 

  infotlvs = new_tlvstack(NULL, 0);

  temp = convert_from_utf8(city);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(state);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(phone);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(fax);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(street);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(zip);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  add_nontlv_w_le(infotlvs, country);
  temp = convert_from_utf8(company_name);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(department);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(position);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  add_nontlv_w_le(infotlvs, occupation);
  temp = convert_from_utf8(webpage);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);

  v7_send_15_02_D007(conn,  0xF303, infotlvs->beg, infotlvs->len);

  free_tlvstack(infotlvs);
}

void v7_modify_main_home_info (V7Connection *conn, gchar *nick, 
			       gchar *first, gchar *last,
			       gchar *email, gchar *city, gchar *state, 
			       gchar *phone, gchar *fax, gchar *street, 
			       gchar *cellular, gchar *zip, WORD country,
			       BYTE timezone, BYTE publish_email)
{

  TLVstack *infotlvs;
  gchar *temp;

#ifdef TRACE_FUNCTION
  g_print( "v7_modify_main_home_info\n" );
#endif 

  infotlvs = new_tlvstack(NULL, 0);

  temp = convert_from_utf8(nick);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(first);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(last);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(email);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(city);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(state);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(phone);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(fax);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(street);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(cellular);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  temp = convert_from_utf8(zip);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  add_nontlv_w_le(infotlvs, country);
  add_nontlv(infotlvs, &timezone, 1);
  add_nontlv(infotlvs, &publish_email, 1);

  v7_send_15_02_D007(conn, 0xEA03, infotlvs->beg, infotlvs->len);

  free_tlvstack(infotlvs);
}

void v7_modify_more_info(V7Connection *conn, BYTE age, BYTE sex, 
			 gchar *homepage, WORD birthyear,
			 BYTE birthmonth, BYTE birthday, BYTE language1,
			 BYTE language2, BYTE language3)
{

  TLVstack *infotlvs;
  gchar *temp;

#ifdef TRACE_FUNCTION
  g_print( "v7_modify_more_info\n" );
#endif 

  infotlvs = new_tlvstack(NULL, 0);
  if (age > 13)
    add_nontlv(infotlvs, &age, 1);
  else
    add_nontlv(infotlvs, "\x0E", 1);
  add_nontlv_w_be(infotlvs, sex);
  temp = convert_from_utf8(homepage);
  add_nontlv_lnts(infotlvs, temp);
  g_free(temp);
  add_nontlv_w_le(infotlvs, birthyear);
  add_nontlv(infotlvs, &birthmonth, 1);
  add_nontlv(infotlvs, &birthday, 1);
  add_nontlv(infotlvs, &language1, 1);
  add_nontlv(infotlvs, &language2, 1);
  add_nontlv(infotlvs, &language3, 1);

  v7_send_15_02_D007(conn, 0xFD03, infotlvs->beg, infotlvs->len);

  free_tlvstack(infotlvs);
}

void v7_modify_password(V7Connection *conn, gchar *password)
{

  TLVstack *infotlvs;

#ifdef TRACE_FUNCTION
  g_print( "v7_modify_password\n" );
#endif 

  infotlvs = new_tlvstack(NULL, 0);
  add_nontlv_lnts(infotlvs, password);

  v7_send_15_02_D007(conn, 0x2E04, infotlvs->beg, infotlvs->len);

  free_tlvstack(infotlvs);
}

void v7_modify_permissions(V7Connection *conn, BYTE auth, BYTE webaware)
{

  BYTE buffer[4];

#ifdef TRACE_FUNCTION
  g_print( "v7_modify_permissions\n" );
#endif 

  buffer[0] = auth ? 0 : 1;
  buffer[1] = webaware ? 1 : 0;
  buffer[2] = 1;
  buffer[3] = 0;

  v7_send_15_02_D007(conn, 0x2404, buffer, 4);


}

void v7_modify_emails_info (V7Connection *conn, GList *emails)
{

  TLVstack *infotlvs;
  GList *email;

#ifdef TRACE_FUNCTION
  g_print( "v7_modify_emails_info\n" );
#endif

  infotlvs = new_tlvstack("\0", 1);

  for (email = emails; email != NULL; email = email->next) {
    add_nontlv(infotlvs, email->data, 1);
    add_nontlv_lnts(infotlvs, email->data+1);
    infotlvs->beg[0]++;
  }
  
  v7_send_15_02_D007(conn, 0x0B04, infotlvs->beg, infotlvs->len);
  
  free_tlvstack(infotlvs);
}

void v7_modify_interests_info (V7Connection *conn, GList *interests)
{
  TLVstack *infotlvs;
  GList *interest;
  gchar *temp;

#ifdef TRACE_FUNCTION
  g_print( "v7_modify_interests_info\n" );
#endif

  infotlvs = new_tlvstack("\0", 1);

  for (interest = interests; interest != NULL; interest = interest->next) {
    add_nontlv(infotlvs, interest->data, 2);
    temp = convert_from_utf8(interest->data+2);
    add_nontlv_lnts(infotlvs, temp);
    g_free(temp);
    infotlvs->beg[0]++;
  }

  v7_send_15_02_D007(conn, 0x1004, infotlvs->beg, infotlvs->len);

  free_tlvstack(infotlvs);
}

void v7_modify_past_aff_info (V7Connection *conn, GList *past, GList *affiliations)
{
  TLVstack *infotlvs, *listtlvs;
  GList *list;
  gchar  aff_count = 0;
  gchar *temp;

#ifdef TRACE_FUNCTION
  g_print( "v7_modify_past_aff_info\n" );
#endif

  infotlvs = new_tlvstack("\0", 1);

  for (list = past; list != NULL; list = list->next) {
    add_nontlv(infotlvs, list->data, 2);
    temp = convert_from_utf8(list->data+2);
    add_nontlv_lnts(infotlvs, temp);
    g_free(temp);
    infotlvs->beg[0]++;
  }

  listtlvs = new_tlvstack(NULL, 0);


  for (list = affiliations; list != NULL; list = list->next) {
    add_nontlv(listtlvs, list->data, 2);
    temp = convert_from_utf8(list->data+2);
    add_nontlv_lnts(infotlvs, temp);
    g_free(temp);
    aff_count++;
  }

  add_nontlv(infotlvs, &aff_count, 1);
  
  add_nontlv(infotlvs, listtlvs->beg, listtlvs->len);
  
  free_tlvstack(listtlvs);

  v7_send_15_02_D007(conn, 0x1A04, infotlvs->beg, infotlvs->len);

  free_tlvstack(infotlvs);
}

void v7_sendcontacts(V7Connection *conn, UIN_T uin, GList *contacts)
{

  GList *contact;
  TLVstack *msgtlvs;
  gchar *count;

#ifdef TRACE_FUNCTION
  g_print( "v7_sendcontacts\n" );
#endif 

  msgtlvs = new_tlvstack(zerobuf, 8);

  DW_2_Chars(msgtlvs->beg, atoi (our_info->uin));

  msgtlvs->beg[4] = 0x13;  /* type 0x13 = send contacts  */

  count = g_strdup_printf("%d", g_list_length(contacts));
  add_nontlv(msgtlvs, count, strlen(count));
  g_free(count);
  
  for (contact = contacts; contact != NULL; contact = contact->next) {
    add_nontlv(msgtlvs, "\xFE", 1);
    add_nontlv(msgtlvs, ((ContactPair *)contact->data)->textuin,
               strlen(((ContactPair *)contact->data)->textuin));
    add_nontlv(msgtlvs, "\xFE", 1);
    add_nontlv(msgtlvs, ((ContactPair *)contact->data)->nick,
               strlen(((ContactPair *)contact->data)->nick));
  }

  add_nontlv(msgtlvs, zerobuf, 1);


  Word_2_Chars(msgtlvs->beg+6, msgtlvs->len - 8);

  v7_sendmsg4(conn, uin, msgtlvs->beg, msgtlvs->len);
  
  free_tlvstack(msgtlvs);
}

void v7_iam_icq (V7Connection *conn)
{
  BYTE family_ver[40] = \
    "\x00\x01\x00\x03"
    "\x00\x13\x00\x02"
    "\x00\x02\x00\x01"
    "\x00\x03\x00\x01"
    "\x00\x15\x00\x01"
    "\x00\x04\x00\x01"
    "\x00\x06\x00\x01"
    "\x00\x09\x00\x01"
    "\x00\x0A\x00\x01"
    "\x00\x0B\x00\x01";

  snac_send(conn, family_ver, 40, FAMILY_01_GENERIC, F01_CLIENT_I_AM_ICQ,
            NULL, ANY_ID);
}

void v7_request_rate (V7Connection *conn)
{
  snac_send(conn, NULL, 0, FAMILY_01_GENERIC, F01_CLIENT_REQ_RATE,
            NULL, ANY_ID);
}

void v7_make_requests(V7Connection *conn)
{
#ifdef TRACE_FUNCTION
  g_print( "v7_make_requests\n" );
#endif

  snac_send(conn, NULL, 0, FAMILY_01_GENERIC,  F01_CLIENT_REQ_PERSONAL_INFO,
            NULL, ANY_ID);

  v7_request_contacts_list (conn);

  snac_send(conn, NULL, 0, FAMILY_02_LOCATION, F02_CLIENT_REQ_RIGHTS_LOCATION,
            NULL, ANY_ID);
  snac_send(conn, NULL, 0, FAMILY_03_CONTACTLIST, F03_CLIENT_REQ_CL_INFO,
            NULL, ANY_ID);
  snac_send(conn, NULL, 0, FAMILY_04_MESSAGING, F04_CLIENT_REQ_RIGHTS_MSG,
            NULL, ANY_ID);
  snac_send(conn, NULL, 0, FAMILY_09_LISTS, F09_CLIENT_REQ_LISTS_RIGHTS,
	    NULL, ANY_ID);
}


void v7_set_user_info (V7Connection *conn)
{
  TLVstack *tlv;
  BYTE caps[16*4] = CAP_AIM_SERVERRELAY CAP_UTF8 CAP_RTFMSGS CAP_AIM_ISICQ;

  tlv = new_tlvstack(NULL,0);
  add_tlv(tlv, 5, caps, 64);
  snac_send(conn, tlv->beg, tlv->len, FAMILY_02_LOCATION,
            F02_CLIENT_SET_CAPS, NULL, ANY_ID);
  free_tlvstack(tlv);
}


/* Pass away message to go away or NULL to come back */
void v7_aimsetstatus(V7Connection *conn, gchar *awaymessage)
{

  /* possible caps
Buddy icon

BYTE buddy_icon_cap[16] = { 0x09, 0x46, 0x13, 0x46, 0x4c, 0x7f, 0x11, 0xd1, 0x82, 0x22, 0x44, 0x45, 0x53, 0x54, 0x00, 0x00};
BYTE voice_cap[16]= {0x09, 0x46, 0x13, 0x41, 0x4c, 0x7f, 0x11, 0xd1, 0x82, 0x22, 0x44, 0x45, 0x53, 0x54, 0x00, 0x00 };
BYTE im_image_cap[16]= {0x09, 0x46, 0x13, 0x45, 0x4c, 0x7f, 0x11, 0xd1, 0x82, 0x22, 0x44, 0x45, 0x53, 0x54,0x00, 0x00};
BYTE chat_cap[16] = {0x74, 0x8f, 0x24, 0x20, 0x62, 0x87, 0x11, 0xd1, 0x82, 0x22, 0x44, 0x45, 0x53, 0x54, 0x00, 0x00};
BYTE get_file_cap[16] = {0x09, 0x46, 0x13, 0x48, 0x4c, 0x7f, 0x11, 0xd1, 0x82, 0x22, 0x44, 0x45, 0x53, 0x54, 0x00, 0x00};
BYTE send_file_cap[16] = {0x09, 0x46, 0x13, 0x43, 0x4c, 0x7f, 0x11, 0xd1, 0x82, 0x22, 0x44, 0x45, 0x53, 0x54, 0x00, 0x00};
  */

  BYTE buf[] = "text/x-aolrtf; charset=\"us-ascii\"";
  TLVstack *tlvs;

  tlvs = new_tlvstack(NULL, 0);

  add_tlv(tlvs, 1, buf, strlen(buf));
  /*   add_tlv(tlvs, 2, PROFILE); dont know what to put */
  add_tlv(tlvs, 3, buf, strlen(buf));
  if (awaymessage)
    add_tlv(tlvs, 4, awaymessage, strlen(awaymessage));
  else
    add_tlv(tlvs,4, NULL, 0);
  /* add_tlv_tlbs, 5, CAPS); We dont have any caps yer */

  snac_send(conn, tlvs->beg, tlvs->len, FAMILY_02_LOCATION,
            F02_CLIENT_SET_CAPS, NULL, ANY_ID);

  free_tlvstack(tlvs);
  
}


void v7_add_icbm (V7Connection *conn)
{
  BYTE msg_param[16] ="\x00\x00\x00\x00\x00\x03\x1F\x40\x03\xE7\x03\xE7\x00\x00\x00\x00";

  snac_send(conn, msg_param, 16, FAMILY_04_MESSAGING, F04_CLIENT_ADD_MSG_PARM,
	    NULL, ANY_ID);
}

void v7_client_ready (V7Connection *conn)
{
  gchar ready_data2001[80] = \
    "\x00\x01\x00\x03\x01\x10\x04\x7B"
    "\x00\x13\x00\x02\x01\x10\x04\x7B"
    "\x00\x02\x00\x01\x01\x01\x04\x7B"
    "\x00\x03\x00\x01\x01\x10\x04\x7B"
    "\x00\x15\x00\x01\x01\x10\x04\x7B"
    "\x00\x04\x00\x01\x01\x10\x04\x7B"
    "\x00\x06\x00\x01\x01\x10\x04\x7B"
    "\x00\x09\x00\x01\x01\x10\x04\x7B"
    "\x00\x0A\x00\x01\x01\x10\x04\x7B"
    "\x00\x0B\x00\x01\x01\x10\x04\x7B";

#ifdef TRACE_FUNCTION
  g_print ("v7_client_ready\n");
#endif

  snac_send(conn, ready_data2001, sizeof(ready_data2001),
            FAMILY_01_GENERIC, F01_CLIENT_READY, NULL, ANY_ID);

  conn->status = READY;
  conn->readysince = time(NULL);
  enable_online_events = TRUE;
  gnomeicu_spinner_stop();

  gnomeicu_set_status_button (FALSE);

  gnomeicu_done_login ();

  v7_reqofflinemsg(conn);

  /* Require our nickname if we dont have one locally */

  if (!our_info->nick  || our_info->nick == '\0')
    v7_request_our_info(conn);

}

void v7_send_often(V7Connection *conn, DWORD reqid)
{
  TLVstack *tlvs;
  gchar buffer[10];

#ifdef TRACE_FUNCTION
  g_print( "v7_send_often\n" );
#endif
  
  Word_2_Chars(buffer, 8);
  DW_2_Chars(buffer+2, atoi (our_info->uin));
  Word_2_CharsBE(buffer+6, 0x3E00);
  Word_2_CharsBE(buffer+8, reqid);

  tlvs = new_tlvstack(NULL,0);

  add_tlv(tlvs, 1, buffer, 10);
  
  snac_send(conn, tlvs->beg, tlvs->len, FAMILY_15_SAVED_DATA,
            F15_CLIENT_REQ_SAVE_DATA, NULL, ANY_ID);
  free_tlvstack(tlvs);

}

void v7_reqofflinemsg(V7Connection *conn)
{

  TLVstack *tlvs;
  gchar buffer[10];

#ifdef TRACE_FUNCTION
  g_print( "v7_reqofflinemsg\n" );
#endif
  
  Word_2_Chars(buffer, 8);
  DW_2_Chars(buffer+2, atoi (our_info->uin));
  Word_2_CharsBE(buffer+6, 0x3C00);

  tlvs = new_tlvstack(NULL,0);

  add_tlv(tlvs, 1, buffer, 10);
  
  snac_send(conn, tlvs->beg, tlvs->len, FAMILY_15_SAVED_DATA,
            F15_CLIENT_REQ_SAVE_DATA, NULL, ANY_ID);
  free_tlvstack(tlvs);

}

void v7_sendfile(V7Connection *conn, UIN_T uin, const gchar *msg,
		 GSList *files )
{
  int cx;

  struct stat file_stat;
  gchar *nopathfile = 0;

  XferInfo *xferinfo;

  GSList *file;
  GSList *contact;

#ifdef TRACE_FUNCTION
  g_print( "v7_sendfile\n" );
#endif
	
  contact = Find_User( uin );
  if( !contact)
    return;

  xferinfo = ft_new(kontakt, XFER_DIRECTION_SENDING, NULL, 0);
  
  xferinfo->msgid = g_malloc(8);

  DW_2_Chars(xferinfo->msgid, time(NULL));  /* broken way to get a varying msgid */
  DW_2_Chars(xferinfo->msgid+4, time(NULL));

  for (file = files;file;file = file->next) {
    nopathfile = ((gchar*)file->data);

    for( cx = strlen((gchar*)file->data); cx; cx-- )
      if( ((gchar*)file->data)[ cx ] == '/' ) {
        nopathfile = &((gchar*)file->data)[ cx + 1 ];
        break;
      }

    if( !nopathfile || nopathfile[0] == '\0' ) {
      ft_cancel_transfer(0, xferinfo);
      return;
    }
    if( stat( (gchar*)file->data, &file_stat ) == -1 ) {
      ft_cancel_transfer(0, xferinfo);
      return;
    }
    ft_addfile(xferinfo, g_strdup(((gchar*)file->data)), g_strdup(nopathfile),
	       file_stat.st_size);
  }

  if (g_slist_length(xferinfo->file_queue) == 1)
    xferinfo->filename = g_strdup(nopathfile);
  else {
    char buf[15];
    snprintf(buf, sizeof (buf), "%u files", g_slist_length(xferinfo->file_queue));
    xferinfo->filename = g_strdup(buf);
  }

  v7_send_file_msg2(conn, uin, msg, 0, conn->ourip, TRUE, xferinfo->filename, 
		    xferinfo->total_bytes, xferinfo->msgid, 0);

  return;
}

void v7_acceptfile(XferInfo *xfer)
{

#ifdef TRACE_FUNCTION
  g_print( "v7_acceptfile\n" );
#endif
  
  if (!ft_listen(xfer))
    return;

  if (preferences_get_bool (PREFS_GNOMEICU_AUTO_ACCEPT_FILE)) {
    g_free (xfer->auto_save_path);

    xfer->auto_save_path = preferences_get_string (PREFS_GNOMEICU_DOWNLOAD_PATH);
  }

  printf("OUR IP IS %lu %s\n", xfer->conn->ourip, inet_ntoa(xfer->conn->ourip));

  v7_send_file_msg2(xfer->conn, xfer->remote_contact->uin, xfer->msg, 
		    xfer->port, xfer->conn->ourip, FALSE, xfer->filename,
		    xfer->total_bytes, xfer->msgid, xfer->downcounter);
}

void v7_send_idletime(V7Connection *conn, DWORD idletime)
{

  snac_send(conn, (char*)&idletime, sizeof(DWORD), FAMILY_01_GENERIC, 
	    F01_CLIENT_SET_IDLE, NULL, ANY_ID);


}
