/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Migrate local contacts list to server side contacts list (GUI code)
 * first created by Patrick Sung (Jan 2002)
 */

#include "common.h"

#include "cl_migrate.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "v7snac13.h"

#include <gtk/gtkmessagedialog.h>

/* g_idle_add user function, must return false so only run once */
gboolean
migration_dialog (gpointer d)
{
  gchar msg_default[] =
    N_("Starting with ICQ protocol version 8. ICQ server now supports server "
       "side contacts list.\n\nGnomeICU is about to send your contacts list "
       "to the server.");
  gchar msg_rare[] =
    N_("Starting with ICQ protocol version 8. ICQ server now supports server "
       "side contacts list.\n\nGnomeICU is about to send your contacts list "
       "to the server.\n\n\n\nWarning: you have an existing server contacts "
       "list, GnomeICU will add your local users to this list. This will likely "
       "create duplicate contacts.");
  GtkWidget *dialog;

  if (!is_new_user) {
    dialog = gtk_message_dialog_new (GTK_WINDOW (MainData->window),
                                     GTK_DIALOG_DESTROY_WITH_PARENT,
                                     GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
                                     d ? _(msg_rare) : _(msg_default));
    gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
  }

  if (!is_connected(GTK_WINDOW(MainData->window), _("You can not migrate your contact list while disconnected.")))
    return FALSE;

  v7_begin_CL_edit (mainconnection, is_new_user ? FALSE : TRUE );
  v7_migrate_contacts_list (mainconnection);
  v7_end_CL_edit (mainconnection);

  return FALSE;
}
