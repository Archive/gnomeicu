/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/**********************************
 Handle account creation (new or existing)
 (it simply sets variables and lets other code
 take care of actual creation)

(c) 1999 Jeremy Wise
 GnomeICU
***********************************/ 

/**********************************
 This is a rewrite to use the Gnome druid.
 The druid API is quite buggy (and 
 otherwise hard to work with), so there 
 are lots of workarounds. Note that on 
 each page we listen for "exposed" events 
 on some widget to determine which
 buttons should be active. There are also 
 lots of functions which could probably be 
 written in some other way, but I believe 
 this is the clearest, and gives the most 
 room for future expansion.

 TODO: Make the druid take care of more
 of the new user setup, such as adding
 contacts and configure basic settings.
 Also, the druid should try to login
 with the user specified info, and allow
 the user to try again if they are wrong.

***********************************/


#include "common.h"
#include "newsignup.h"
#include "gnomeicu.h"
#include "util.h"
#include "v7base.h"
#include "v7login.h"
#include "v7newuser.h"

#include <gtk/gtk.h>
#include <libgnome/libgnome.h>
#include <libgnomeui/libgnomeui.h>
#include <glib-object.h>
#include <sys/wait.h>

#define NEW_UIN 			1
#define EXISTING_UIN 	2

/* the forward/back_* functions take care of user clicks on that button on their
   respective page. The set_* functions handle enabling/disabling the forward/back
	 buttons depending on what the user does.
*/

gboolean forward_new_existing(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data);
gboolean forward_uin_password(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data);
gboolean forward_new_password(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data);
gboolean back_new_password(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data);
gboolean back_finished(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data);

gboolean back_success(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data);
gboolean forward_success(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data);
gboolean back_failure(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data);

void druid_finished(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data);

gboolean set_new_existing(GtkWidget *widget, gpointer pointer);
gboolean set_uin_pwd_forward(GtkWidget *widget, gpointer pointer);
gboolean set_pwd_pwd_forward(GtkWidget *widget, gpointer pointer);
gboolean set_failure_forward(GtkWidget *widget, gpointer pointer);
gboolean set_connect_buttons(GtkWidget *widget, gpointer pointer);
gboolean set_success_buttons(GtkWidget *widget, gpointer pointer);

static GladeXML *welcome_druid_xml = NULL;
static GladeXML *error_dialog_xml = NULL;

static gint path;
static gboolean druid_running, we_own_gtk_main;

void new_sign_up(void)
{	
	path = 0;
	
	welcome_druid_xml = gicu_util_open_glade_xml ("welcome.glade", "druid_window");
        if (welcome_druid_xml == NULL)
          return;

	gtk_window_present(GTK_WINDOW(glade_xml_get_widget (welcome_druid_xml, "druid_window")));
	
	gtk_widget_show_all(glade_xml_get_widget (welcome_druid_xml, "druid_window"));  /* workaround */

	glade_xml_signal_autoconnect(welcome_druid_xml);

	
	druid_running = TRUE;
	we_own_gtk_main = TRUE;
	
	gtk_main();
	
	we_own_gtk_main = FALSE;
	
	while(druid_running)		/* if the druid is started a second time */
		sleep(1);
}

gboolean forward_new_existing(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data)
{
	if(path == NEW_UIN)
		gnome_druid_set_page(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
									GNOME_DRUID_PAGE(glade_xml_get_widget (welcome_druid_xml, "page_register")));
	
	if(path == EXISTING_UIN)
		gnome_druid_set_page(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
									GNOME_DRUID_PAGE(glade_xml_get_widget (welcome_druid_xml, "page_login")));
	
	return TRUE;
}

gboolean forward_uin_password(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data)
{
	our_info->uin = g_strdup(gtk_entry_get_text(GTK_ENTRY(glade_xml_get_widget(welcome_druid_xml, "uin_entry"))));
	passwd = g_strdup(gtk_entry_get_text(GTK_ENTRY(glade_xml_get_widget(welcome_druid_xml, "password_entry"))));
	our_info->nick = g_strdup("");
	gnome_druid_set_page(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
									GNOME_DRUID_PAGE(glade_xml_get_widget (welcome_druid_xml, "page_finish")));

	return TRUE;
}

gboolean forward_new_password(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data)
{
	gnome_druid_set_page(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
									GNOME_DRUID_PAGE(glade_xml_get_widget (welcome_druid_xml, "page_finish")));

	passwd = g_strdup(gtk_entry_get_text(GTK_ENTRY(glade_xml_get_widget(welcome_druid_xml, "password_entry1"))));
	
	return TRUE;
}

gboolean back_new_password(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data)
{
	gnome_druid_set_page(GNOME_DRUID(glade_xml_get_widget(welcome_druid_xml, "welcome_druid")), 
									GNOME_DRUID_PAGE(glade_xml_get_widget(welcome_druid_xml, "page_choose")));
	
	return TRUE;	
}

gboolean back_finished(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data)
{
	if(path == NEW_UIN)
		gnome_druid_set_page(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
									GNOME_DRUID_PAGE(glade_xml_get_widget (welcome_druid_xml, "page_register")));
	
	if(path == EXISTING_UIN)
		gnome_druid_set_page(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
									GNOME_DRUID_PAGE(glade_xml_get_widget (welcome_druid_xml, "page_login")));
	
	return TRUE;
}

void druid_finished(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data)
{
	gtk_widget_destroy(glade_xml_get_widget (welcome_druid_xml, "druid_window"));
	druid_running = FALSE;
	if(we_own_gtk_main)
		gtk_main_quit();
}

gboolean set_new_existing(GtkWidget *widget, gpointer pointer)
{
	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(glade_xml_get_widget (welcome_druid_xml, "new_radio"))))
	{
		is_new_user = TRUE;
		path = NEW_UIN;
	}
	else
  {
		is_new_user = FALSE;
		path = EXISTING_UIN;
	}
	
	return FALSE;
}

gboolean set_uin_pwd_forward(GtkWidget *widget, gpointer pointer)
{
	gboolean enabled = FALSE;
		
	if(strlen(gtk_entry_get_text(GTK_ENTRY(glade_xml_get_widget (welcome_druid_xml, "uin_entry")))))
		if(strlen(gtk_entry_get_text(GTK_ENTRY(glade_xml_get_widget (welcome_druid_xml, "password_entry")))))
			enabled = TRUE;
		
	gnome_druid_set_buttons_sensitive(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
							TRUE, enabled, TRUE, TRUE);
	return FALSE;
}

gboolean set_pwd_pwd_forward(GtkWidget *widget, gpointer pointer)
{
	gboolean enabled = FALSE;
		
	if(strlen(gtk_entry_get_text(GTK_ENTRY(glade_xml_get_widget (welcome_druid_xml, "password_entry1")))))
		if(strlen(gtk_entry_get_text(GTK_ENTRY(glade_xml_get_widget (welcome_druid_xml, "password_entry2")))))
			if(!strcmp(gtk_entry_get_text(GTK_ENTRY(glade_xml_get_widget (welcome_druid_xml, "password_entry1"))), 
				   gtk_entry_get_text(GTK_ENTRY(glade_xml_get_widget (welcome_druid_xml, "password_entry2")))))
				enabled = TRUE;
		
	gnome_druid_set_buttons_sensitive(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")),
							TRUE, enabled, TRUE, TRUE);
		
	return FALSE;
}

/* this is presently not used, but will probably be at some point */
gboolean set_failure_forward(GtkWidget *widget, gpointer pointer)
{
	if(path)
		gnome_druid_set_buttons_sensitive(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
							TRUE, FALSE, TRUE, TRUE);
	
	return FALSE;
}

gboolean set_connect_buttons(GtkWidget *widget, gpointer pointer)
{
/* not used, but will be */
	
/*	V7Connection *conn;

	
	g_print("connect_buttons\n");

	if(path) {
		gnome_druid_set_buttons_sensitive(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
							FALSE, FALSE, TRUE, TRUE);

		gtk_widget_queue_draw(glade_xml_get_widget(welcome_druid_xml, "label11"));
		gdk_window_process_all_updates();

		create_tcp_line();
    conn = v7_new_login_session();
		
		if(conn->status == READY)
			g_print("tjohoo!\n");

		gnome_druid_set_page(GNOME_DRUID(glade_xml_get_widget(welcome_druid_xml, "welcome_druid")), 
											GNOME_DRUID_PAGE(glade_xml_get_widget(welcome_druid_xml, "page_failure")));
	}
*/
	return FALSE;
}

/* not used */
gboolean set_success_buttons(GtkWidget *widget, gpointer pointer)
{
	if(path)
		gnome_druid_set_buttons_sensitive(GNOME_DRUID(glade_xml_get_widget(welcome_druid_xml, "welcome_druid")), 
							TRUE, TRUE, TRUE, TRUE);

	return FALSE;
}	

/* not used */
gboolean back_success(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data)
{
	if(path == NEW_UIN)
		gnome_druid_set_page(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
									GNOME_DRUID_PAGE(glade_xml_get_widget (welcome_druid_xml, "page_register")));
	
	if(path == EXISTING_UIN)
		gnome_druid_set_page(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
									GNOME_DRUID_PAGE(glade_xml_get_widget (welcome_druid_xml, "page_login")));
	
	return TRUE;
}

/* not used */
gboolean forward_success(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data)
{
	gnome_druid_set_page(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
									GNOME_DRUID_PAGE(glade_xml_get_widget (welcome_druid_xml, "page_finish")));
	return TRUE;
}

/* not used */
gboolean back_failure(GnomeDruidPage *druidpage, gpointer arg1, gpointer user_data)
{
	if(path == NEW_UIN)
		gnome_druid_set_page(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
									GNOME_DRUID_PAGE(glade_xml_get_widget (welcome_druid_xml, "page_register")));
	
	if(path == EXISTING_UIN)
		gnome_druid_set_page(GNOME_DRUID(glade_xml_get_widget (welcome_druid_xml, "welcome_druid")), 
									GNOME_DRUID_PAGE(glade_xml_get_widget (welcome_druid_xml, "page_login")));
	
	return TRUE;
}

/* shows a dialog allowing the user to rerun druid or retreive pwd from web */
gint show_uin_pwd_dialog(void)
{
	gint response = 0;
	
	if(error_dialog_xml == NULL) {
		error_dialog_xml = gicu_util_open_glade_xml ("welcome.glade", "uin_pwd_dialog");
                if (error_dialog_xml == NULL)
                  return GTK_RESPONSE_NONE;
        }

	response = gtk_dialog_run(GTK_DIALOG(glade_xml_get_widget(error_dialog_xml, "uin_pwd_dialog")));

	gtk_widget_hide(glade_xml_get_widget(error_dialog_xml, "uin_pwd_dialog"));
  
	if(response == GTK_RESPONSE_OK)
	{
		is_new_user = FALSE;
		new_sign_up();
		if(is_new_user)
			v7_new_user(passwd);
		else
			v7_new_login_session();
		
		return response;
	}

	if(response == GTK_RESPONSE_ACCEPT)
	{
		gnome_url_show("https://www.icq.com/password", NULL);
	}

	return response;
}
