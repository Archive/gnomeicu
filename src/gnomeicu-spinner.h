/****************************
  Spinner control
  Olivier Crete (c) 2002
  GnomeICU
*****************************/


void          gnomeicu_spinner_init           (void);
void          gnomeicu_spinner_start          (void);
void          gnomeicu_spinner_stop           (void);
void          gnomeicu_spinner_reload         (void);
