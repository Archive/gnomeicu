/************************************
 Message history
 (c) 1999 Jeremy Wise
 (c) 2001 Gediminas Paulauskas
 GnomeICU
*************************************/

#include "common.h"
#include "gnomeicu.h"
#include "gnomeicu-spinner.h"
#include "icons.h"
#include "util.h"
#include "icu_db.h"
#include "msg.h"

#include <gtk/gtk.h>
#include <libgnomeui/libgnomeui.h>

#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

typedef struct _history_parser HistoryParser;
struct _history_parser {
  GtkTextView *textview;
  gchar *buffer;
  gchar *current;
};

/*** Local function declarations ***/
static void search_cb (GtkWidget *widget, Contact_Member *data);
static void show_clear_dialog (GtkWidget *widget, Contact_Member *contact);
static void add_to_history (UIN_T uin, const char *statement, 
			    time_t time, gboolean received);
static void append_history_entry (GtkTextBuffer *text_buffer,
				  time_t time, const char *text, 
				  gboolean received);
static void dump_file (const gchar *filename, FILE *file);


/*** Global functions ***/
void history_add_incoming (UIN_T uin, const char *statement,
                           time_t *timedate)
{
	char *tmp;
	time_t my_time;

#ifdef TRACE_FUNCTION
	g_print ("history_add_incoming\n");
#endif

	g_return_if_fail (statement != NULL);
	if (statement[0] == '\0')
		return;

	/* No one strips \r before calling this function */
	tmp = strip_cr (statement);

	if ((timedate == NULL) || (*timedate > time (NULL)) ||
            (*timedate < 600000000)) { /* thats in 1989 */
          my_time = time (NULL);
	} else {
          my_time = *timedate;
	}

	add_to_history (uin, tmp, my_time, TRUE);

	g_free (tmp);
}

void history_add_outgoing (UIN_T uin, const char *statement)
{
	time_t timedate;

#ifdef TRACE_FUNCTION
	g_print ("history_add_outgoing\n");
#endif

	g_return_if_fail (statement != NULL);
	if (statement[0] == '\0')
		return;

	time (&timedate);

	add_to_history (uin, statement, timedate, FALSE);
}


void append_history_entry (GtkTextBuffer *text_buffer, time_t time,
                           const gchar *text, gboolean received)
{
  GtkTextIter end_iter;
  GtkTextMark *mark;
  char buf[80];
  gchar *pdate;
  gchar *p_tmp_date;
  struct tm *timetm;

  timetm = localtime(&time);
	
  strftime (buf, sizeof (buf), _("%c"), timetm);

  p_tmp_date = g_locale_to_utf8(buf, -1, NULL, NULL, NULL);	

  pdate = g_strdup_printf ("*** %s [ %s ] ***\n", p_tmp_date,
			   received ? _("Received") : _("Sent"));

  g_free (p_tmp_date);

  gtk_text_buffer_get_end_iter (text_buffer, &end_iter);
  gtk_text_buffer_insert_with_tags_by_name (text_buffer, &end_iter, pdate, -1,
                                            received ? "incoming_tag"
                                                     : "outgoing_tag", NULL);

  gtk_text_buffer_get_end_iter (text_buffer, &end_iter);
  mark = gtk_text_buffer_create_mark (text_buffer, NULL, &end_iter, FALSE);
  gnomeicu_text_buffer_insert_with_emoticons(text_buffer, mark, text);
  gtk_text_buffer_delete_mark (text_buffer, mark);
    
  gtk_text_buffer_get_end_iter (text_buffer, &end_iter);
  gtk_text_buffer_insert (text_buffer, &end_iter, "\n\n", 2);

  g_free (pdate);
}

static void
history_prepend (GtkTextView *tv, time_t time, gboolean received,
                 const gchar *msg, gint msg_len)
{
  struct tm *timetm;
  gchar buf[80];
  gchar *p_tmp_date;
  gchar *pdate;
  GtkTextBuffer *tb;
  GtkTextIter iter;
  GtkTextMark *mark;

  gchar *tmp;

  if (msg_len <= 0)
    return;

  tb = gtk_text_view_get_buffer (tv);

  timetm = localtime(&time);
  strftime (buf, sizeof (buf), _("%c"), timetm);
  p_tmp_date = g_locale_to_utf8 (buf, -1, NULL, NULL, NULL);
  pdate = g_strdup_printf ("*** %s [ %s ] ***\n", p_tmp_date,
                           received ? _("Received") : _("Sent"));
  g_free (p_tmp_date);

  gtk_text_buffer_get_start_iter (tb, &iter);
  mark = gtk_text_buffer_create_mark (tb, NULL, &iter, FALSE);
  gtk_text_buffer_insert_with_tags_by_name (tb, &iter, pdate, -1,
                                            received ? "incoming_tag"
                                                     : "outgoing_tag", NULL);

  gtk_text_buffer_get_iter_at_mark (tb, &iter, mark);

  tmp = g_strndup (msg, msg_len);
  //gtk_text_buffer_insert (tb, &iter, tmp, -1);
  gnomeicu_text_buffer_insert_with_emoticons (tb, mark, tmp);
  g_free (tmp);
 
  gtk_text_buffer_get_iter_at_mark (tb, &iter, mark);
  gtk_text_buffer_insert (tb, &iter, "\n\n", 2);

  gtk_text_buffer_delete_mark (tb, mark);
  g_free (pdate);

  gtk_text_buffer_get_end_iter (tb, &iter);
  gtk_text_view_scroll_to_iter (tv, &iter, 0, FALSE, 1, 1);
}

#define CHUNK_SIZE 10240

gboolean
history_parse_chunk (HistoryParser *hp)
{
  gchar *start_of_entry = NULL;
  gchar *end_of_entry = NULL;
  gchar *start_of_block = hp->current - CHUNK_SIZE;

  if (start_of_block < hp->buffer)
    start_of_block = hp->buffer;

  while(hp->current > start_of_block) {
    gchar *msg;
    gint msg_len;
    gboolean received;
    char direction;
    time_t time;

    end_of_entry = hp->current;

    start_of_entry = g_strrstr_len (hp->buffer,hp->current - hp->buffer, "\n*** ");
    if (start_of_entry == NULL) 
      hp->buffer = start_of_entry;

    
    /* read header info */
    sscanf (start_of_entry + 5, "%c %d", &direction, (int *)&time);

    if (direction == 'R' || direction == 'r')
      received = TRUE;
    else if (direction == 'S' || direction == 's')
      received = FALSE;
    else
      direction = 0;
    
    msg = strchr (start_of_entry+5, '\n')+1;
  
    msg_len = end_of_entry - msg;
    
    if (direction)
      history_prepend (hp->textview, time, received, msg, msg_len);

    hp->current = start_of_entry;

  }
   
  if (hp->current == hp->buffer) {
    g_free (hp->buffer);
    g_free (hp);
    return FALSE; /* reading finished, stop this idle function */
  }

  return TRUE;
}

void
history_display (GtkWidget *widget, Contact_Member *contact)
{
  gchar *filename;
  FILE *file;
  GladeXML *xml;
  GtkWidget *window;
  gchar *dialog_name;
  GtkTextBuffer *text_buffer;
  GtkTextIter iter;
  struct stat fs;
  char *buffer;
  HistoryParser *hp;

  if (contact->history_xml != NULL) {
    /* show the window if it has been shown previously */
    gtk_window_present (GTK_WINDOW (glade_xml_get_widget (contact->history_xml,
							  "history_dialog")));
    return;
  }

  /* Open file for reading */
  filename = g_strdup_printf ("%s/.icq/history/%s.txt", g_get_home_dir (),
                              contact->uin);

  file = fopen (filename, "r");

  if (file == NULL) {
    gnome_error_dialog (_("No message history available."));
    g_free (filename);
    return;
  }


  xml = gicu_util_open_glade_xml ("history.glade", "history_dialog");
  if (xml == NULL)
    return;

  contact->history_xml = xml;

  window = glade_xml_get_widget (xml,"history_dialog");
  set_window_icon (window, "gnomeicu-hist.png");

  dialog_name = g_strdup_printf (_("Message History: %s"), contact->nick);
  gtk_window_set_title (GTK_WINDOW (window), dialog_name);
  g_free (dialog_name);
	
  glade_xml_signal_autoconnect (xml);

  g_signal_connect(G_OBJECT (glade_xml_get_widget (xml,"clear_button")),
                   "clicked", G_CALLBACK (show_clear_dialog), contact);
  g_signal_connect(G_OBJECT (glade_xml_get_widget (xml,"search_button")),
                   "clicked", G_CALLBACK (search_cb), contact);
  g_signal_connect(G_OBJECT (glade_xml_get_widget (xml,"search_entry")),
                   "activate", G_CALLBACK (search_cb), contact);
  /* g_signal_connect(G_OBJECT(glade_xml_get_widget(xml,"search_entry")),
     "changed", G_CALLBACK(searchentry_changed), 
     glade_xml_get_widget(xml,"search_button")); */

  text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (glade_xml_get_widget (contact->history_xml, "history_textview")));

  gtk_text_buffer_create_tag(text_buffer, "incoming_tag",
                             "foreground", "red", NULL);
  gtk_text_buffer_create_tag(text_buffer, "outgoing_tag",
                             "foreground", "blue", NULL);

  /* obtain the buffer from the history file */
  stat (filename, &fs);
  g_free (filename);

  buffer = (char *)g_malloc (fs.st_size);
  if (!buffer) {
    g_warning ("Unable allocate buffer for history reading.\n");
    fclose (file);
    return;
  }

  fread (buffer, 1, fs.st_size, file);
  fclose (file);



  hp = g_new (HistoryParser, 1);
  hp->textview = GTK_TEXT_VIEW (glade_xml_get_widget (contact->history_xml,
                                                      "history_textview"));
  hp->buffer = buffer;
  hp->current = buffer + fs.st_size;


  //gnomeicu_spinner_start();

  g_idle_add ((GSourceFunc)history_parse_chunk, hp);

  /* ... do the parse */


  //gnomeicu_spinner_stop();

  gtk_widget_show (window);
}

#undef CHUNK_SIZE

/*** Local functions ***/

void show_clear_dialog (GtkWidget *widget, Contact_Member *contact)
{
	int reply;
	GtkWidget *dialog;
	GtkTextBuffer *tb;
	GtkTextIter start, end;
	gchar *filename;

	dialog = gtk_message_dialog_new (GTK_WINDOW (glade_xml_get_widget(contact->history_xml, "history_dialog")),
	                                 GTK_DIALOG_DESTROY_WITH_PARENT,
	                                 GTK_MESSAGE_ERROR, GTK_BUTTONS_YES_NO,
	                                 _("Clear all stored history?"));
	reply = gtk_dialog_run (GTK_DIALOG (dialog));
	if (reply == GTK_RESPONSE_YES) {
	  filename = g_strdup_printf( "%s/.icq/history/%s.txt", 
				      g_get_home_dir(), contact->uin );

	  /* clear the text widgets */
	  tb=gtk_text_view_get_buffer(GTK_TEXT_VIEW(glade_xml_get_widget(contact->history_xml,"history_textview")));
	  gtk_text_buffer_get_start_iter(tb, &start);
	  gtk_text_buffer_get_end_iter(tb, &end);
	  gtk_text_buffer_delete(tb, &start, &end);

	  unlink (filename);
	  g_free(filename);
	}
	gtk_widget_destroy (dialog);
}

void searchentry_changed (GtkWidget * widget, gpointer data)
{
	g_object_set_data (G_OBJECT (widget), "searched",
			   GINT_TO_POINTER (FALSE));

	gtk_button_set_label( GTK_BUTTON(widget), GTK_STOCK_FIND);
}

void search_cb(GtkWidget *widget, Contact_Member *contact)
{
	GtkTextIter start, end;
	GtkTextView *textview = GTK_TEXT_VIEW(glade_xml_get_widget(contact->history_xml,"history_textview"));
        GtkWidget *searchentry = glade_xml_get_widget(contact->history_xml,"search_entry");
	GtkWidget *searchbutton = glade_xml_get_widget(contact->history_xml,"search_button");
	gboolean searched;
	gchar *historytext;
	gchar *searchstring;
	//foundat is an offset (a number of chars) from the beginning 
	//of history      
	gint foundat = 0;
	gchar *foundat_ptr;

	searched = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (searchbutton), "searched"));

	if (searched) {
		foundat = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (searchbutton), "foundat"));
	} else {
		foundat = 0;
	}

	/* OK, lets search now! */

	searchstring = gtk_editable_get_chars (GTK_EDITABLE (searchentry), 0, -1);
	gtk_text_buffer_get_iter_at_offset(gtk_text_view_get_buffer(textview), 
					   &start, foundat);
	gtk_text_buffer_get_end_iter(gtk_text_view_get_buffer(textview), &end);
	historytext = gtk_text_buffer_get_text(gtk_text_view_get_buffer(textview),
					       &start, &end, FALSE);

	foundat_ptr = strstr (historytext, searchstring);
	searched = TRUE;
	if (foundat_ptr != NULL) {
		GtkTextBuffer* tb = gtk_text_view_get_buffer(textview);
		foundat += g_utf8_pointer_to_offset(historytext, foundat_ptr);
		gtk_text_buffer_get_iter_at_offset(tb, &start, foundat);
		gtk_text_view_scroll_to_iter(textview, &start, 0.0, 
					     FALSE, 0.5, 0.1);

		//selects the search string
		gtk_text_buffer_place_cursor(tb, &start);
		foundat += g_utf8_strlen(searchstring, -1);
		gtk_text_buffer_get_iter_at_offset(tb, &start, foundat);
		gtk_text_buffer_move_mark_by_name(tb, "insert", &start);
	} else {
	  GtkWidget *dialog;
	  dialog = gtk_message_dialog_new (GTK_WINDOW (glade_xml_get_widget(contact->history_xml, "history_dialog")), 
					   GTK_DIALOG_DESTROY_WITH_PARENT,
					   GTK_MESSAGE_INFO, 
					   GTK_BUTTONS_OK,
					   _("No more occurrences found."));
	  gtk_dialog_run (GTK_DIALOG (dialog));
	  gtk_widget_destroy(dialog);
	  searched = FALSE;
	}


	 
	g_object_set_data (G_OBJECT (searchbutton), "searched",
			   GINT_TO_POINTER (searched));
	g_object_set_data (G_OBJECT (searchbutton), "foundat",
			   GINT_TO_POINTER (foundat));
	if (searched)
	  gtk_button_set_label( GTK_BUTTON(searchbutton), "Find Next");
	else
	  gtk_button_set_label( GTK_BUTTON(searchbutton), GTK_STOCK_FIND);

	

	g_free (historytext);
	g_free (searchstring);

}

void add_to_history (UIN_T uin, const char *statement, const time_t time,
		     gboolean received)
{
	gchar *filename;

	int fd;
	FILE *file;
	gchar *pdate;
	gchar datebuf[80];

	struct tm *timetm;
	GSList *contact;

	g_assert(time != 0 && statement != NULL);

	contact = Find_User (uin);
	if (contact == NULL)
		return;

	filename = g_strdup_printf ("%s/.icq/history/%s.txt", g_get_home_dir (), uin);
	if ((fd = open (filename, O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR)) == -1)
	{
		g_free (filename);
		return;
	}
	g_free (filename);

	file = fdopen(fd, "a");

	if(!file) g_warning("Cant' fdopen\n");
	
	timetm = localtime(&time);
	
	strftime (datebuf, sizeof (datebuf), _("%c"), timetm);
	
	pdate = g_locale_to_utf8(datebuf, -1, NULL, NULL, NULL);

	/* TRANSLATOR: (Receive|Sent) at [date] */
	fprintf (file, "\n*** %c %d %s %s ***\n%s", received ? 'R' : 'S', 
		 (int)time, received ? _("Received at") : _("Sent at"), 
		 pdate, statement);
	g_free(pdate);

	fclose (file);

	if (kontakt->history_xml != NULL) {
	  GtkWidget *textview =
                glade_xml_get_widget(kontakt->history_xml,"history_textview");
	  GtkTextBuffer *buffer =
	                    gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	  GtkTextMark *position = gtk_text_buffer_get_insert(buffer);
	  GtkTextIter iter;
	  
	  position = gtk_text_buffer_get_insert(buffer);
	  
	  gtk_widget_freeze_child_notify (GTK_WIDGET (textview)); 
	  append_history_entry (buffer, time, statement, received);
	  gtk_widget_thaw_child_notify (GTK_WIDGET (textview)); 
	  
	  gtk_text_buffer_get_iter_at_mark(buffer, &iter, position);
	  gtk_text_buffer_place_cursor(buffer, &iter);
	  
	}
}


void convert_history()
{

#ifdef HAVE_ICUDB

  FILE *file;
  GDir *dir;
  gchar *dirname, *oldfilename, *newfilename, *tempfilename;
  const gchar *filename;
  GtkWidget *dialog;

  
  dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				  GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
				  _("Your history is now being converted to "
				  "the new format, this will happen only "
				  "once."));
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);

  dirname = g_strdup_printf("%s/.icq/history", g_get_home_dir());

  dir = g_dir_open(dirname, 0, NULL);

  g_free(dirname);

  while ((filename = g_dir_read_name(dir))) {
    
    if (!strcmp(filename+strlen(filename)-3,".db")) {

      oldfilename = g_strdup_printf("%s/.icq/history/%s", 
				    g_get_home_dir(),
				    filename);
      tempfilename = g_strndup(oldfilename, strlen(oldfilename)-3);
      
      newfilename = g_strdup_printf("%s.txt", tempfilename);

      g_free(tempfilename);

      if (!g_file_test(newfilename,G_FILE_TEST_EXISTS)) {
      
	file = fopen(newfilename, "a");
	
	dump_file(oldfilename, file);
	
	fclose(file);
	
      }

      g_free(newfilename);
      g_free(oldfilename);

    }
  }
 
#endif /* HAVE_ICUDB */

}

#ifdef HAVE_ICUDB

gint compare_dates(const gint *a,
		   const gint *b)
{
	g_assert(a != NULL && b != NULL);
	return( *b - *a );
}

void dump_file(const gchar *filename, FILE *file)
{

  DB_FILE db_file;
  datum firstkey, nextkey, key_data;
  GList *listed_history = NULL, *rev_list;
  
  db_file = icudb_open( filename, DB_READ );
  
  if( db_file == NULL ) {
    printf("Can't open file: %s\n", filename);
    return;
  }



  firstkey = icudb_firstkey ( db_file );

  /* First, create a list of keys from the history file */
  while ( firstkey.dptr ) {

    listed_history = g_list_append( listed_history, GINT_TO_POINTER(firstkey.dptr) );

    nextkey = icudb_nextkey ( db_file, firstkey );
    firstkey = nextkey;

  }

  /* Then sort the keys */
  listed_history = g_list_sort (listed_history, (GCompareFunc)compare_dates);
  rev_list = g_list_last( listed_history );

  firstkey.dsize = sizeof (time_t);

  while (rev_list != NULL) {

    firstkey.dptr = rev_list->data;
    key_data = icudb_fetch (db_file, firstkey);

    if (key_data.dptr != NULL) {
      gchar *newtext;

      if (g_utf8_validate(key_data.dptr + 2, key_data.dsize - 2, NULL))
	newtext = g_strndup(key_data.dptr + 2, key_data.dsize - 2);
      else
	newtext = g_convert_with_fallback(key_data.dptr + 2,
					  key_data.dsize - 2,
					  "UTF-8",
					  preferences_get_string(PREFS_GNOMEICU_CHARSET_FALLBACK),
					  "?" ,NULL, NULL, NULL);

      fprintf(file, "\n*** %c %ld %s%s\n",
	      MESG_ISRECV (key_data.dptr) ? 'R' : 'S',
	      *((time_t*)rev_list->data),
	      ctime(rev_list->data),
	      newtext);

      g_free(newtext);

      icudb_free (key_data.dptr);
    }

    icudb_free (rev_list->data);
    rev_list = rev_list->prev;
  }

  /* If window was destroyed, free the rest of the list */
  while (rev_list != NULL) {
    icudb_free (rev_list->data);
    rev_list = rev_list->prev;
  }

  /* Finally, close the db file */
  icudb_close ( db_file );

  g_list_free (listed_history);
  
}


#endif /* HAVE_ICUDB */
