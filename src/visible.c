#include "common.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "listwindow.h"
#include "v7send.h"
#include "v7snac13.h"
#include "visible.h"
#include "groups.h"
#include "gnomecfg.h"

#include <gtk/gtk.h>
#include <string.h>

static gboolean is_visible (CONTACT_PTR contact)
{
	return (contact ? contact->vis_list : FALSE);
}

static void visible_add(Contact_Member *contact)
{
  v7_visible (mainconnection, contact->uin,
	      contact_gen_uid (),
	      TRUE); /* add */ 
		
  if (contact->invis_list == TRUE) {
    v7_invisible (mainconnection, contact->uin, 
		  contact->invlist_uid, FALSE); /* remove */
  }

  Save_RC();
  gnomeicu_tree_user_update(contact);
}

static void visible_remove(Contact_Member *contact)
{
  v7_visible (mainconnection, contact->uin, contact->vislist_uid,
	      FALSE); /* remove */
}

void visible_list_dialog( void )
{

	static GtkWidget *dlg = NULL;
	
	if( dlg == NULL )
	{
		dlg = list_window_new_filter(
			_("Visible List"),
			_("Drag the users you wish to add to your\n"
			  "visible list into this window"),
			visible_add,
			visible_remove,
			is_visible);
		g_object_add_weak_pointer (G_OBJECT (dlg), (gpointer *)&dlg);
	} else {
		gtk_window_present (GTK_WINDOW (dlg));
	}

}
