/*****************************
 Drag & Drop support
 (c) 1999 Jeremy Wise
 GnomeICU
*****************************/

#include "common.h"
#include "dragdrop.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "user_popup.h"
#include "util.h"

#include <string.h>
#include <gtk/gtk.h>
#include <libgnomevfs/gnome-vfs-uri.h>


GtkTargetEntry target_table[] = {
  { "STRING", 0, DND_TARGET_STRING },
  { "CONTACT", GTK_TARGET_SAME_APP, DND_TARGET_CONTACT }
};

static GtkTargetEntry contact_list_drop_types[]=
{
  { "STRING", 0, DND_TARGET_URL },
  { "text/plain", 0, DND_TARGET_URL }
  /* ??  For some reason Netscape wants to send me stuff as text/plain */
};

static gint n_contact_list_drop_types=
  sizeof(contact_list_drop_types)/sizeof(contact_list_drop_types[0]);

/*** Local function declarations ***/
static void contact_list_dnd_drop(GtkWidget *widget, GdkDragContext *context,
                                  gint x, gint y, GtkSelectionData *selection_data,
                                  guint info, guint time, gpointer data);

/*** Global functions ***/
void init_contact_list_drag_drop(GtkWidget *list)
{
  g_signal_connect(G_OBJECT(list),
		   "drag_data_received",
		   G_CALLBACK(contact_list_dnd_drop),
		   NULL);
  gtk_drag_dest_set(GTK_WIDGET(list),
		    GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_HIGHLIGHT | GTK_DEST_DEFAULT_DROP,
		    contact_list_drop_types, n_contact_list_drop_types,
		    GDK_ACTION_COPY);
  gtk_drag_source_set(GTK_WIDGET(list),
                      GDK_BUTTON1_MASK,
                      target_table, 1,
                      GDK_ACTION_COPY | GDK_ACTION_MOVE);
}

/*** Local functions ***/
static void contact_list_dnd_drop(GtkWidget *widget,
				  GdkDragContext *context,
				  gint x,
				  gint y,
				  GtkSelectionData *selection_data,
				  guint info,
				  guint time,
				  gpointer data)
{
  CONTACT_PTR contact;

  g_return_if_fail(widget);
  contact = g_object_get_data (G_OBJECT (widget ), "contact" );
 
  if (contact == NULL)
    return;
   
  switch(info)
    {
    case DND_TARGET_URL:
      /* We got a bunch of URIs */
      {
          char *copy = g_malloc ( selection_data->length+1 );
          GList *uris;

          strncpy ( copy, selection_data->data, selection_data->length );
          copy [ selection_data->length ] = 0;

          /* Lets see if its a file list, or a URL */
          if (strncmp(copy, "file:", 5) == 0)
          {
              uris = gnome_vfs_uri_list_parse (copy);
              
              request_file(widget, uris, contact);
              g_free (copy);  
              gnome_vfs_uri_list_free(uris);
          } else if ( (strncmp(copy, "http://", 7) == 0) ||
                      (strncmp(copy, "ftp://", 6) == 0) ) 
          {
              send_url_window( widget, copy ); 
          }
      }
      break;
	 default:
		g_warning("Unknown info for drag and drop: %d\n", info);
    }
}
