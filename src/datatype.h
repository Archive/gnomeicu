#ifndef __DATATYPE_H__
#define __DATATYPE_H__

#include <glib.h>

typedef guint32 DWORD;
typedef guint16 WORD;
typedef guint8  BYTE;

typedef gchar *UIN_T;

#endif /* __DATATYPE_H__ */
