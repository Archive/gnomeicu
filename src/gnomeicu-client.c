/**************************
 A program used to send commands to running GnomeICU
 (c) 1999 Jeremy Wise
 (c) 2001 drJeckyll@hotmail.com
 GnomeICU

This thing interface with userserver.c
***************************/

#include <config.h>
#include <stdio.h>
#include <pwd.h>
#include <unistd.h>
#include <stdlib.h>
#include <locale.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>

#include <libgnome/gnome-i18n.h>
#include "userserver.h"

static int connect_to_gnomeicu(char *uinstr);
static void usage(void);

int main(int ac, char **av)
{
  char c = USERSERVER_CMD_DO;
  UServCmdLen u;
  char *buffer;
  int i;
  char *uinstr = NULL;
  int readv = 0;
  int socket_fd;

  setlocale(LC_ALL, "");
	
  bindtextdomain(PACKAGE, GNOMELOCALEDIR);
  textdomain(PACKAGE);

  if ( ac <= 1 || !strcmp(av[1], "-h") || !strcmp(av[1], "--help") ) {
    usage();
    return 0;
  }

  buffer = (gchar *)malloc( 8192 );

  for(i = 1; i < ac; i++) {
    if(strcmp(av[i],"-u") == 0) {
      i++;
      if (i >= ac) {
        usage();
        return 1;
      }
      uinstr = av[i++];
      if (i >= ac)
        break;
    }
    if(!strcmp(av[i], "readmsg")) {
      readv = i;
      c = USERSERVER_CMD_READMSG;
    } else if (!strcmp (av[i], "msgcount")) {
      readv = i;
      c = USERSERVER_CMD_MSGCOUNT;
    } else if (!strcmp (av[i], "getstatus")) {
      readv = i;
      c = USERSERVER_CMD_GETSTATUS;
    } else if (!strcmp (av[i], "icuask")) { /* for icukelle */
      readv = i;
      c = USERSERVER_ICUKRELL_ASK;
    } else if (!strcmp (av[i], "contactlist") || !strcmp (av[i], "onlinelist")) {
      readv = 1;
      c = USERSERVER_CMD_DO;
    }
    strcat(buffer,av[i]);
    strcat(buffer," ");
  }

  buffer[strlen(buffer)-1]=0;

  if ( uinstr != NULL && atol(uinstr)==0 ) {
    printf("%s", _("UIN must be a number.\n"));
    return 1;
  }

  if ( (socket_fd = connect_to_gnomeicu(uinstr)) == 0) {
    printf("%s", _("Unable to connect to running GnomeICU client.\n"));
    return 1;
  }

  u=htonl(strlen(buffer)+1);

  write(socket_fd,&c,1);
  write(socket_fd,&u,sizeof(UServCmdLen));
  write(socket_fd,buffer,strlen(buffer)+1);

  free( buffer );

  if (readv && (!strcmp(av[readv], "readmsg") || 
                !strcmp(av[readv], "msgcount") || 
                !strcmp(av[readv], "getstatus") || 
                !strcmp(av[readv], "icuask") || 
                !strcmp(av[readv], "contactlist") ||
                !strcmp(av[readv], "onlinelist")) ) {
    read(socket_fd,&u,sizeof(UServCmdLen));
    u = ntohl( u );
    if( u < 16384 ) {
      buffer = (gchar *)malloc (u + 1);
      read(socket_fd,buffer,u);
      printf( "%s\n\n", buffer );
      free( buffer );
    }
  }

  close(socket_fd);
  return 0;
}

/* return 0 if unsuccessful, socket fd otherwise */
int
connect_to_gnomeicu (char *uinstr)
{
  int socket_fd;
  struct sockaddr_un addr;
  struct passwd *pw;
  char path_name[108];

  pw = getpwuid(getuid());

  if (!pw) {
    /* fall back to use uid instead of unix user name */
    snprintf(path_name, 108, "/tmp/.gnomeicu-%d/ctl%s%s",
             getuid(),
             (uinstr == NULL) ? "" : "-",
             (uinstr == NULL) ? "" : uinstr );
    fprintf(stderr, "Unable to get current login name, naming service problem? (/etc/nsswitch.conf)\n");
    fprintf(stderr, "Using file %s\n", path_name);
  } else {
    snprintf(path_name, 108, "/tmp/.gnomeicu-%s/ctl%s%s",
             pw->pw_name,
             (uinstr == NULL) ? "" : "-",
             (uinstr == NULL) ? "" : uinstr );
  }

  addr.sun_family = AF_UNIX;
  strcpy (addr.sun_path, path_name);

  if (access (addr.sun_path, R_OK) != 0)
    return 0;

  socket_fd = socket(AF_UNIX, SOCK_STREAM, 0);
  if(socket_fd >= 0) {
    if(connect(socket_fd, (struct sockaddr *)&addr,
               sizeof(addr.sun_family) + strlen(addr.sun_path) + 1) == 0) {
      return socket_fd;
    } else {
      close (socket_fd);
      return 0;
    }
  } else
    perror ("gnomeicu-client: connect_to_gnomeicu: ");

  return 0;
}

void usage (void)
{
	printf ("%s", _("Usage: gnomeicu-client [-u YourUIN] command [DestinationUIN] [Data]\n\n"));
	printf ("%s", _("Commands are as follows:\n"));
	printf ("%s\t\t%s\n", "contactlist", _("show all contact list entries"));
	printf ("%s\t\t%s\n", "onlinelist", _("show online contact list entries"));
	printf ("%s\t\t%s\n", "msgcount", _("show number of incoming messages"));
	printf ("%s\t\t\t%s\n", "icuask", _("what IcuKrell ask GnomeICU"));
	printf ("%s\t\t\t%s\n", "readmsg", _("read message"));
	printf ("%s\t\t%s\n", "getstatus", _("get GnomeICU status"));
	printf ("%s\t\t\t%s\n", "quit", _("close GnomeICU"));
	printf ("%s\t\t\t%s\n", "hide", _("hide GnomeICU main window"));
	printf ("%s\t\t\t%s\n", "show", _("show GnomeICU main window"));
	printf ("%s\t\t%s\n", "showhide", _("show|hide GnomeICU main window"));
	printf ("%s\t\t%s\n", "addcontact", _("popup GnomeICU useradd dialog"));
	printf ("%s\t\t%s\n", "showprefs", _("popup GnomeICU preferences dialog"));
	printf ("%s\t\t%s\n", "changeinfo", _("popup GnomeICU user info dialog"));
	printf ("%s\t\t%s\n", "ignorelist", _("popup GnomeICU ignore list"));
	printf ("%s\t\t%s\n", "visiblelist",_("popup GnomeICU visible list"));
	printf ("%s\t\t%s\n", "invisiblelist", _("popup GnomeICU invisible list"));
	printf ("%s\t\t%s\n", "notifylist", _("popup GnomeICU notify list"));
	printf ("%s\t\t%s\n", "away \"msg\"", _("set away message"));
	printf ("%s\t%s\n", "adduser|useradd UIN", _("add user with UIN to the list"));
	// onlinelist
	printf ("%s\t\t%s\n", "msg UIN \"msg\"", _("send message to UIN"));
	printf ("%s\t\t%s\n", "status <status>",
		_("set status: online offline away na invisible freechat occupied dnd"));
	printf ("%s\t%s\n", "status_noask <status>",
		_("set status without asking the reason: online offline away na invisible freechat occupied dnd"));
	printf ("%s\t\t%s\n", "readevent", _("popup GnomeICU current event window"));
}
