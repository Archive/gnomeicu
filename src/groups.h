/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * Groups related functions
 * first created by Patrick Sung (Jan 2002)
 */

#ifndef __GROUPS_H__
#define __GROUPS_H__

#include "datatype.h"

WORD contact_gen_uid (void);
WORD groups_gen_gid (void);
void groups_add (const gchar *name, WORD gid);
void groups_remove(WORD gid);
gchar *groups_name_by_gid(WORD gid);
void groups_rename_by_gid (WORD gid, const gchar *name);

/* This function should not exist and should die ASAP */
/* I agree :) Patrick */
guint groups_find_gid_by_name (const gchar *name);


#endif /* __GROUPS_H__ */
