/* GnomeICU
 * Copyright (C) 1998-2004 Jeremy Wise, Olivier Crete
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * Support for freedesktop.org Notification Area
 */

/*
 * Copyright (c) 2003 Daniel Romberg
 * Copyright (c) 2006 Olivier Cr�te
 */

#include "common.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "util.h"
#include "icons.h"
#include "tray.h"
#include "msg.h"
#include "showlist.h"

#include <gtk/gtk.h>

#if !GTK_CHECK_VERSION(2,10,0)
#include "gicustatusicon.h"
#define GtkStatusIcon GicuStatusIcon
#define gtk_status_icon_new_from_pixbuf gicu_status_icon_new_from_pixbuf
#define gtk_status_icon_set_blinking gicu_status_icon_set_blinking 
#define gtk_status_icon_set_from_pixbuf gicu_status_icon_set_from_pixbuf
#define gtk_status_icon_set_tooltip gicu_status_icon_set_tooltip
#define gtk_status_icon_is_embedded gicu_status_icon_is_embedded 
#define gtk_status_icon_position_menu gicu_status_icon_position_menu
#endif


GtkStatusIcon *status_icon = NULL;


static void status_activated  (GtkStatusIcon *status_icon, gpointer user_data);
static void status_popup (GtkStatusIcon *status_icon, guint button, 
			  guint activate_time, gpointer user_data);

void tray_init (void)
{
	
  status_icon = gtk_status_icon_new_from_pixbuf(get_pixbuf_for_status (Current_Status));

 
  g_signal_connect (G_OBJECT (status_icon), "activate",
		    G_CALLBACK (status_activated), NULL );

  g_signal_connect (G_OBJECT (status_icon), "popup-menu",
		    G_CALLBACK (status_popup), NULL );

}




void tray_update (void)
{
  GSList *ginfo;
  GSList *contact;
  int num_messages = 0;
  gchar *nextmsg = NULL;
  gint type = 0;
  gchar *tooltips_str;
  gint online_user = 0;

  if (!status_icon)
    return;

  contact = Contacts;

  while( contact != NULL ) {
    guchar len = g_slist_length( kontakt->stored_messages );
    if( len ) {
      if( nextmsg == NULL ) {
	nextmsg = g_strdup_printf( _("Next message from %s"), kontakt->nick );
	type = ((STORED_MESSAGE_PTR)kontakt->stored_messages->data)->type;
      }
      num_messages += len;
    }
    if (kontakt->status != STATUS_OFFLINE)
      online_user++;
    contact = contact->next;
  }

  gtk_status_icon_set_blinking(status_icon, num_messages);
  if (num_messages) {
    gtk_status_icon_set_from_pixbuf(status_icon,
				    get_pixbuf_for_message(type));
  } else {
    gtk_status_icon_set_from_pixbuf(status_icon,
				    get_pixbuf_for_status(Current_Status));
  }

  if (nextmsg != NULL)
    tooltips_str = g_strdup_printf ("%s\n%d %s\n%d %s\n%s",
				    get_status_str (Current_Status),
				    online_user,
				    n_("contact online", 
				       "contacts online", 
				       online_user),
				    num_messages, 
				    n_("new message",
				       "new messages",
				       num_messages),
				    nextmsg );
  else
    tooltips_str = g_strdup_printf("%s\n%d %s",
				   get_status_str (Current_Status),
				   online_user,
				   n_("contact online", 
				      "contacts online",
				      online_user));

  gtk_status_icon_set_tooltip(status_icon, tooltips_str);
  g_free(tooltips_str);
}


static void status_activated  (GtkStatusIcon *status_icon, gpointer user_data)
{

  GSList *contact;
  gboolean pending_msg = FALSE;


  /* only reach here when double click occured */

  for (contact = Contacts; contact != NULL; contact = contact->next) {
    guchar len = g_slist_length (kontakt->stored_messages);

    if (len) {
      pending_msg = TRUE;
      break;
    }
  }

  if (pending_msg) {
    show_contact_message (kontakt);
  } else {
    if (GTK_WIDGET_VISIBLE (MainData->window)) {
      /* window visible, hide it */
      gtk_window_get_position (GTK_WINDOW (MainData->window),
			       &MainData->x, &MainData->y);
      gtk_widget_hide (MainData->window);
      MainData->hidden = TRUE;
    } else {
      /* unhide */
      gtk_window_move (GTK_WINDOW (MainData->window), MainData->x, MainData->y);
      gtk_window_present (GTK_WINDOW(MainData->window));
      MainData->hidden = FALSE;
    }
  }
}

static void status_popup (GtkStatusIcon *status_icon, guint button, 
			  guint activate_time, gpointer user_data)
{
	GtkWidget *status_menu;
	GtkWidget *item, *image;

#ifdef TRACE_FUNCTION
	g_print( "tray_menu\n" );
#endif
	status_menu = gtk_menu_new();

	item = gtk_image_menu_item_new_with_label (_("Online"));
	image = gtk_image_new_from_pixbuf (icon_online_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_online),
	                  NULL);

	item = gtk_image_menu_item_new_with_label (_("Away"));
	image = gtk_image_new_from_pixbuf (icon_away_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_away),
	                  NULL);

	item = gtk_image_menu_item_new_with_label (_("Not Available"));
	image = gtk_image_new_from_pixbuf (icon_na_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_na),
	                  NULL);

	item = gtk_image_menu_item_new_with_label (_("Free for Chat"));
	image = gtk_image_new_from_pixbuf (icon_ffc_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_ffc),
	                  NULL);

	item = gtk_image_menu_item_new_with_label (_("Occupied"));
	image = gtk_image_new_from_pixbuf (icon_occ_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_occ),
	                  NULL);

	item = gtk_image_menu_item_new_with_label (_("Do Not Disturb"));
	image = gtk_image_new_from_pixbuf (icon_dnd_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_dnd),
	                  NULL);

	item = gtk_image_menu_item_new_with_label (_("Invisible"));
	image = gtk_image_new_from_pixbuf (icon_inv_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_invisible),
	                  NULL);

	item = gtk_menu_item_new ();
	gtk_widget_set_sensitive (item, FALSE);
	gtk_container_add (GTK_CONTAINER (status_menu), item);

	item = gtk_image_menu_item_new_with_label (_("Offline"));
	image = gtk_image_new_from_pixbuf (icon_offline_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_offline),
	                  NULL);

	gtk_widget_show_all (status_menu);

	gtk_menu_popup (GTK_MENU(status_menu), NULL, NULL,
	                gtk_status_icon_position_menu, status_icon,
	                button, activate_time);

}

gboolean
tray_exists (void)
{
  return gtk_status_icon_is_embedded(status_icon);
}
