/************************************
 Message dialog routines
 (c) 1999 Jeremy Wise
 GnomeICU
*************************************/

#include "msg.h"
#include "common.h"
#include "emoticons.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "history.h"
#include "icons.h"
#include "response.h"
#include "rtf-reader.h"
#include "showlist.h"
#include "user_popup.h"
#include "util.h"
#include "v7send.h"

#include <gdk/gdkkeysyms.h>
#include <libgnome/libgnome.h>
#include <gtk/gtk.h>
#include <string.h>
/* this is for the urgency hint stuff */
#include <gdk/gdkx.h>

#ifdef WITH_GTKSPELL
# include <gtkspell/gtkspell.h>
#endif

static void message_cb_menu_position (GtkMenu *menu, gint *x, gint *y, gboolean* push_in, gpointer user_data);

static gboolean msg_url_motion_notify_event (GtkWidget * w, GdkEventMotion * event, gpointer user_data);
static gboolean msg_url_text_button_pressed (GtkWidget * w, GdkEventButton * event, gpointer unused);
static const char* find_next_url (const char * pch);
static char* get_url_from_location (GtkWidget * w, int x, int y);
static gboolean msg_blink_timeout(Contact_Member *contact);



static GtkTextTagTable* message_tags = NULL;

static void
init_message_tags() {

    GtkTextTag* tag;

#ifdef TRACE_FUNCTION
	g_print("init_message_tags\n");
#endif

    message_tags = gtk_text_tag_table_new();
    
    tag = gtk_text_tag_new("incoming_tag");
    g_object_set(tag, "foreground", "red", "weight", PANGO_WEIGHT_BOLD, NULL);
    gtk_text_tag_table_add( message_tags, tag );

    tag = gtk_text_tag_new("outgoing_tag");
    g_object_set( tag, "foreground", "blue", "weight", PANGO_WEIGHT_BOLD, NULL);
    gtk_text_tag_table_add( message_tags, tag );

    tag = gtk_text_tag_new("bold");
    g_object_set( tag, "weight", PANGO_WEIGHT_BOLD, NULL);
    gtk_text_tag_table_add( message_tags, tag );

    tag = gtk_text_tag_new("italic");
    g_object_set( tag, "style", PANGO_STYLE_ITALIC, NULL);
    gtk_text_tag_table_add( message_tags, tag );

    tag = gtk_text_tag_new("underlined");
    g_object_set( tag, "underline", PANGO_UNDERLINE_SINGLE, NULL);
    gtk_text_tag_table_add( message_tags, tag );

    tag = gtk_text_tag_new("striked");
    g_object_set( tag, "strikethrough", TRUE, NULL);
    gtk_text_tag_table_add( message_tags, tag );

    tag = gtk_text_tag_new("right-to-left");
    g_object_set( tag, "direction", GTK_TEXT_DIR_RTL, NULL);
    gtk_text_tag_table_add( message_tags, tag );

    tag = gtk_text_tag_new("left-to-right");
    g_object_set( tag, "direction", GTK_TEXT_DIR_LTR, NULL);
    gtk_text_tag_table_add( message_tags, tag );

    /* Special naming for the color tags : [fb]XXXXXX */
    /* f or b specifying foreground or background */
    /* The other chars must be the hex representation of the RGB color */
    tag = gtk_text_tag_new("f000000");
    g_object_set( tag, "foreground", "#000000", NULL);
    gtk_text_tag_table_add( message_tags, tag );
    tag = gtk_text_tag_new("b000000");
    g_object_set( tag, "background", "#000000","background-set", TRUE, NULL);
    gtk_text_tag_table_add( message_tags, tag );
    tag = gtk_text_tag_new("bnone");
    g_object_set( tag, "background-set", FALSE, NULL);
    gtk_text_tag_table_add( message_tags, tag );
    
    
    tag = gtk_text_tag_new("url");
    g_object_set( tag, "underline", PANGO_UNDERLINE_SINGLE, "foreground_gdk", "#0000ff", NULL);
    gtk_text_tag_table_add( message_tags, tag); 
}
    

void
gnomeicu_text_buffer_insert_with_emoticons (GtkTextBuffer *text_buffer,
                                            GtkTextMark *mark, const gchar *text)
{
  GSList *e;
  gchar **arr;
  GtkTextIter iter;
  gboolean found = FALSE;
  gchar *theme_name;

  g_assert (text_buffer != NULL);
  g_assert (text  != NULL);

  theme_name = preferences_get_string (PREFS_GNOMEICU_EMOTICON_THEMES);
  if (!theme_name)
    theme_name = g_strdup ("None");

  if (strcmp (theme_name, "None"))
    for (e = Emoticons; e != NULL; e = e->next) {
      if (g_strrstr (text, ((Emoticon*)e->data)->string)) {
        found = TRUE;
        arr = g_strsplit (text, ((Emoticon*)e->data)->string, 2);
        gnomeicu_text_buffer_insert_with_emoticons (text_buffer, mark, arr[0]);

        gtk_text_buffer_get_iter_at_mark (text_buffer, &iter, mark);
	gtk_text_buffer_insert_pixbuf (text_buffer, &iter,
				       ((Emoticon*)e->data)->pixbuf);
        gnomeicu_text_buffer_insert_with_emoticons (text_buffer, mark, arr[1]);
        g_strfreev (arr);
        break;
      }
    }

  g_free (theme_name);

  if (!found) {
    const char * pch;
    GtkTextIter url_start;	
    GtkTextIter url_end;
    GtkTextIter start;
    GtkTextMark *text_mark;

    gtk_text_buffer_get_iter_at_mark (text_buffer, &iter, mark);
    /* save a mark for beginning of text */
    text_mark = gtk_text_buffer_create_mark (text_buffer, NULL, &iter, TRUE);

    gtk_text_buffer_insert (text_buffer, &iter, text, -1);

    gtk_text_buffer_get_iter_at_mark (text_buffer, &start, text_mark);

    /* markup URLs only if url tag is present */
    if (gtk_text_tag_table_lookup(gtk_text_buffer_get_tag_table(text_buffer),
                                  "url")) {
      pch = text;
      while ((pch = find_next_url (pch))) {
        const char *end = pch + 1;

        /* find the end loc of the URL */
        while (*end && !g_unichar_isspace(g_utf8_get_char(end)))
          end = g_utf8_next_char(end);
        url_start = start;
        gtk_text_iter_forward_chars (&url_start, g_utf8_strlen (text, pch-text));
        url_end = url_start;
        gtk_text_iter_forward_chars (&url_end, g_utf8_strlen (pch,end-pch));
        gtk_text_buffer_remove_all_tags (text_buffer, &url_start, &url_end);
        gtk_text_buffer_apply_tag_by_name (text_buffer, "url",
                                           &url_start, &url_end);
        pch = *end ? end+1 : NULL;
      }

      gtk_text_buffer_delete_mark (text_buffer, text_mark);
    }
  }
}


void
gnomeicu_text_view_insert_text(GtkTextView* text_view, gchar* text)
{
    GtkTextBuffer* text_buffer;
    GtkTextIter iter;
    GtkTextMark *mark;
    gchar* result;

#ifdef TRACE_FUNCTION
	g_print("gnomeicu_text_view_insert_text\n");
#endif

   g_assert( text_view != NULL );
    g_assert( text  != NULL );

    text_buffer = gtk_text_view_get_buffer(text_view);

    if ( strncmp( text, "{\\rtf1", 6) )
	{
	    /* No RTF, take that as plain text */
	  /*	  if (isutf8)
	    result = g_strdup(text);
	    else */
	  
	  if (g_utf8_validate(text, -1, NULL))
	    result = g_strdup(text);
	  else
	    result = convert_to_utf8(text);

	  if (!result) {
	    g_warning("Can't convert! Copying directly\n");
	    result = g_strdup(text);
	  }
          gtk_text_buffer_get_end_iter (text_buffer, &iter);
          mark = gtk_text_buffer_create_mark (text_buffer, NULL, &iter, FALSE);
          gnomeicu_text_buffer_insert_with_emoticons (text_buffer, mark, result);
          gtk_text_buffer_delete_mark (text_buffer, mark);
	}
    else 
	{
	    int length;
	    gtk_widget_freeze_child_notify(GTK_WIDGET(text_view));

	    result = rtf_parse_and_insert(text, text_buffer);
	    /* Remove trailing \n's */
	    length = strlen(result)-1;
	    while (result[length] == '\n')
		result[length--] = '\0';
	    gtk_widget_thaw_child_notify(GTK_WIDGET(text_view));
	}

    g_free(result);
}




void
add_message_to_conversation( GtkTextView* conversation,
			     CONTACT_PTR contact,
			     time_t* time,
			     gchar* text ) {

    struct tm *my_tm;
    gchar short_msgtime[40];
    gchar* short_msgtime_utf8 = NULL;
    gchar* new_str, *stripped_str;
    GtkTextBuffer* text_buffer;
    GtkTextIter end_iter;
    GtkTextTag* tag;
    gchar* header;
    gchar *nick, *tag_name;

#ifdef TRACE_FUNCTION
	g_print("add_message_to_conversation\n");
#endif

    g_assert(text != NULL && time != NULL && conversation != NULL);

    if ( contact ) {
	nick = contact->nick;
	tag_name = "incoming_tag";
    } else {
	nick = our_info->nick;
	tag_name = "outgoing_tag";
    }
    
    /* Show the time of this message in the time widget */
    my_tm = localtime (time);
    if (my_tm) {
      strftime (short_msgtime, sizeof (short_msgtime), _("%X"), my_tm);
      short_msgtime_utf8 = g_locale_to_utf8(short_msgtime, -1, NULL, NULL, NULL);
    } else {
      short_msgtime_utf8 = NULL;
    }


    /* Strip the \r's out of the message... */
    stripped_str = strip_cr (text);
    new_str = strip_html (stripped_str);
    g_free(stripped_str);
    
    text_buffer = gtk_text_view_get_buffer(conversation);
    
    header = g_strdup_printf ("%s (%s): ", nick, short_msgtime_utf8 ? short_msgtime_utf8 :  _("Unknown") );
    
    tag = gtk_text_tag_table_lookup(message_tags, tag_name);

    gtk_text_buffer_get_end_iter (text_buffer, &end_iter);
    gtk_text_buffer_insert_with_tags(text_buffer, &end_iter, 
				     header, -1, tag, NULL);
    g_free (header);
    
    header = g_strdup (new_str);
    header = g_strchomp (header);
    
    /* make sure we are inserting at the end of the buffer */
    gtk_text_buffer_get_end_iter (text_buffer, &end_iter);
    gnomeicu_text_view_insert_text(conversation, header);
    gtk_text_buffer_get_end_iter (text_buffer, &end_iter);
    gtk_text_buffer_insert (text_buffer, &end_iter, "\n", 1);
    /* Scroll down to the end of the buffer */
    gtk_text_view_scroll_to_mark(conversation, 
				 gtk_text_buffer_get_mark(text_buffer, 
							  "end_mark"),
				 0.1,
				 FALSE, 0, 1);
    

    g_free (header);
    g_free(new_str);
    g_free(short_msgtime_utf8);
}


void open_message_dlg_with_message (Contact_Member *contact,
                                    STORED_MESSAGE_PTR message_text)
{
	GladeXML *xml;
	GtkWidget *msg_dlg;
	GtkWidget *chat_toggle;
	GtkWidget *conversation;
	GtkWidget *input;
	GtkWidget *history_button;
	GtkWidget *send_button;
	GtkWidget *usermenu_button;

	GtkTextBuffer* text_buffer;
	GtkTextIter end_iter;
	STORED_MESSAGE_PTR msg;
	GSList *message_temp;
#ifdef WITH_GTKSPELL
	GtkSpell *gtkspell;
#endif

        g_assert (contact != NULL);

	if (contact->msg_dlg_xml != NULL) {
          xml = contact->msg_dlg_xml;
	  msg_dlg = glade_xml_get_widget (xml, "message_dialog");
	  
	  if (message_text && contact->newmsg_blink_timeout == 0 &&
	      !gtk_window_is_active(GTK_WINDOW(msg_dlg)) ) {
	    contact->newmsg_blink_timeout = gtk_timeout_add(500,
							    (GtkFunction) msg_blink_timeout, 
							    contact);
#if GTK_CHECK_VERSION(2,8,0)
	    gtk_window_set_urgency_hint(GTK_WINDOW(msg_dlg), TRUE);
#else
	    if (GTK_WIDGET_REALIZED(msg_dlg)) {
	      XWMHints *hints;
	      hints = XGetWMHints(GDK_WINDOW_XDISPLAY(msg_dlg->window), 
				  GDK_WINDOW_XWINDOW(msg_dlg->window));
	      hints->flags |= XUrgencyHint;
	      XSetWMHints(GDK_WINDOW_XDISPLAY(msg_dlg->window), 
			  GDK_WINDOW_XWINDOW(msg_dlg->window), hints);
	      XFree(hints);
	    }
#endif
	  }
	} else {
          xml = gicu_util_open_glade_xml ("message.glade", "message_dialog");
          if (xml == NULL)
            return;

	  msg_dlg = glade_xml_get_widget (xml, "message_dialog");

          contact->msg_dlg_xml = xml;

          chat_toggle = glade_xml_get_widget (xml, "chat_toggle");
          if (preferences_get_bool (PREFS_GNOMEICU_DEFAULT_CHAT_MODE))
            gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (chat_toggle), TRUE);
          conversation = glade_xml_get_widget (xml, "conversation");

          g_signal_connect (conversation, "motion_notify_event",
                            G_CALLBACK(msg_url_motion_notify_event), NULL);

          g_signal_connect (conversation, "button_press_event",
                            G_CALLBACK(msg_url_text_button_pressed), NULL);

          gtk_window_set_type_hint (GTK_WINDOW(msg_dlg),
				    GDK_WINDOW_TYPE_HINT_NORMAL);

          /* Set the tags table to the global one */
          if ( ! message_tags )
            init_message_tags();
          gtk_text_view_set_buffer(GTK_TEXT_VIEW(conversation), 
                                   gtk_text_buffer_new(message_tags));

          /* Add a mark at the end of the converstaion */
          text_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(conversation));
          gtk_text_buffer_get_end_iter(text_buffer, &end_iter);
          gtk_text_buffer_create_mark(text_buffer, "end_mark",
                                      &end_iter, FALSE);
          gtk_text_buffer_create_mark(text_buffer, "insert_mark",
                                      &end_iter, FALSE);

	  gtk_window_set_role(GTK_WINDOW(msg_dlg), "GnomeICU_Message");

          glade_xml_signal_autoconnect (xml);
	}

        set_window_icon( msg_dlg, "gnomeicu-message.png");
	gtk_window_set_title (GTK_WINDOW (msg_dlg), contact->nick);
	chat_toggle = glade_xml_get_widget (xml, "chat_toggle");
	conversation = glade_xml_get_widget (xml, "conversation");
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (chat_toggle))) {
          gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(conversation)), "", -1);
	}
	input = glade_xml_get_widget (xml, "input");

	if (!GTK_WIDGET_VISIBLE (input)) {
	    gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(input)), "", -1);
	}
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (xml, "message_nick")), contact->nick);
	gtk_image_set_from_pixbuf(
				  GTK_IMAGE(glade_xml_get_widget (xml, "message_status")),
				  get_pixbuf_for_status(contact->status));

	history_button = glade_xml_get_widget (xml, "history_button");
	send_button = glade_xml_get_widget (xml, "send_button");
	usermenu_button = glade_xml_get_widget (xml, "usermenu_button");

	g_object_set_data (G_OBJECT (msg_dlg), "contact", contact);
	g_object_set_data (G_OBJECT (input), "contact", contact);
	g_object_set_data (G_OBJECT (input), "message_dialog", msg_dlg);
	g_object_set_data (G_OBJECT (usermenu_button), "contact", contact);
	g_signal_connect(G_OBJECT(history_button), "clicked", 
			 G_CALLBACK (history_display), contact);
#ifdef WITH_GTKSPELL
	gtkspell = gtkspell_get_from_text_view(GTK_TEXT_VIEW(input));
	if (!gtkspell && preferences_get_bool(PREFS_GNOMEICU_SPELLCHECK)) {
	  gtkspell = gtkspell_new_attach(GTK_TEXT_VIEW(input), NULL, NULL);
	}
#endif

	if (message_text != NULL) {
	    /* Show the time of this message in the time widget */
	    add_message_to_conversation(GTK_TEXT_VIEW(conversation),
					contact,
					&message_text->time,
					message_text->message);
	} 

	
	/* show all messages */

	if (contact->stored_messages) {
	msg_next_restart:
	  for (message_temp = contact->stored_messages;
	       message_temp; message_temp = message_temp->next) {
	    msg = (STORED_MESSAGE_PTR)contact->stored_messages->data;
	    if (msg->type == MESSAGE_TEXT) {
	      contact->stored_messages = g_slist_remove (contact->stored_messages, msg );		
	      add_message_to_conversation(GTK_TEXT_VIEW(conversation),
					  contact,
					  &msg->time,
					  msg->message);
		
	      g_free( msg->data );
	      g_free( msg->message );
	      g_free( msg );
	      goto msg_next_restart;
		
	    }
	  }
	  
	  if ( !contact->stored_messages) 
	    gnomeicu_tree_set_contact_icon (contact, get_pixbuf_for_status (contact->status));
	  
	}

	

	gtk_text_view_scroll_to_mark(GTK_TEXT_VIEW(conversation), 
				     gtk_text_buffer_get_mark(
                       gtk_text_view_get_buffer(GTK_TEXT_VIEW(conversation)), 
		       "end_mark"),
				     0.1, FALSE, 0, 1);
	

	gtk_widget_grab_focus (input);
	gtk_widget_show_all (msg_dlg);
}

gboolean msg_blink_timeout(Contact_Member *contact) 
{
  GtkWidget *msg_dlg = glade_xml_get_widget (contact->msg_dlg_xml, 
					     "message_dialog");

  if (!contact->newmsg_blink_timeout) {
    contact->newmsg_blink = 0;
    set_window_icon( msg_dlg, "gnomeicu-message.png");
#if GTK_CHECK_VERSION(2,8,0)
	    gtk_window_set_urgency_hint(GTK_WINDOW(msg_dlg), FALSE);
#else
    if (GTK_WIDGET_REALIZED(msg_dlg)) {
      XWMHints *hints;
      hints = XGetWMHints(GDK_WINDOW_XDISPLAY(msg_dlg->window), 
			  GDK_WINDOW_XWINDOW(msg_dlg->window));
      hints->flags &= ~XUrgencyHint;
      XSetWMHints(GDK_WINDOW_XDISPLAY(msg_dlg->window), 
		  GDK_WINDOW_XWINDOW(msg_dlg->window), hints);
      XFree(hints);
    }
#endif
    return FALSE;
  }
  
  contact->newmsg_blink = !contact->newmsg_blink;
  
  if (contact->newmsg_blink)
    set_window_icon( msg_dlg, "gnomeicu-blank.png");
  else
    set_window_icon( msg_dlg, "gnomeicu-message.png");
  
  return TRUE;
}




void send_message (GtkWidget *dlg)
{
	GtkWidget *entry;
	GtkWidget *conversation;
	Contact_Member *contact;
	gchar *buf, *tempstr, *convtext;
	GtkWidget *toggle;

	GtkTextBuffer* text_buffer;
	GtkTextIter start_iter;
	GtkTextIter end_iter;

	time_t now;

#ifdef TRACE_FUNCTION
	g_print("send_message\n");
#endif

	contact = g_object_get_data (G_OBJECT (dlg), "contact");
	if (contact == NULL)
		return;

	if (!is_connected(GTK_WINDOW(dlg), _("You can not send messages while disconnected.")))
		return;

	entry = glade_xml_get_widget (contact->msg_dlg_xml, "input");
	toggle = glade_xml_get_widget (contact->msg_dlg_xml, "chat_toggle");
	conversation = glade_xml_get_widget (contact->msg_dlg_xml, "conversation");

	if (toggle == NULL || entry == NULL || dlg == NULL || conversation == NULL)
		return;

	text_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(entry));
	gtk_text_buffer_get_start_iter(text_buffer, &start_iter);
	gtk_text_buffer_get_end_iter(text_buffer, &end_iter);
	buf = gtk_text_buffer_get_text(text_buffer, &start_iter, &end_iter, 
				       FALSE);
				       
	if (!buf || buf[0] == '\0') {
		g_free(buf);
		return;
	}

	tempstr = convert_from_utf8(buf);
	
	convtext = add_cr(tempstr);
	g_free(tempstr);
	
	if (contact->status == STATUS_OFFLINE && strlen(convtext) > 450) {
	  GtkWidget *dialog;
	  GtkWidget *button;
	  
	  g_free(convtext);

	  dialog = gtk_message_dialog_new (GTK_WINDOW (glade_xml_get_widget (contact->msg_dlg_xml, "message_dialog")),
					   GTK_DIALOG_DESTROY_WITH_PARENT,
					   GTK_MESSAGE_WARNING, GTK_BUTTONS_NONE,
					   _("You can not send messages over 450 characters to offline contacts. Your message was not sent."));
	  button = gtk_dialog_add_button(GTK_DIALOG(dialog), GTK_STOCK_OK, 
					 GTK_RESPONSE_OK);
	  g_signal_connect(G_OBJECT(dialog), "response",
				   G_CALLBACK(gtk_widget_destroy), NULL);
	  gtk_widget_show (dialog);
	  return;
	}
	g_free(convtext);
	  
	
	v7_sendmsg(mainconnection, contact->uin, buf);
	
	gtk_text_buffer_set_text(text_buffer, "", -1);
	
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle))) {
	  gtk_widget_hide_all (dlg);
	  gtk_widget_unrealize(dlg);
	}
	else
	  gtk_widget_grab_focus (glade_xml_get_widget (contact->msg_dlg_xml, "input"));
	
	/* Show the time of this message in the time widget */
	now = time (NULL);
	add_message_to_conversation( GTK_TEXT_VIEW(conversation),
				     NULL,
				     &now,
				     buf );
}

void message_dialog_response_callback (GtkDialog *dlg, gint response, gpointer data)
{

#ifdef TRACE_FUNCTION
	g_print("message_dialog_response_callback\n");
#endif


	switch (response)
	{
		case 0: /* Send */
			send_message (GTK_WIDGET (dlg));
			break;
		default:
			gtk_widget_hide_all (GTK_WIDGET (dlg));
			gtk_widget_unrealize (GTK_WIDGET (dlg));
			break;
	}
}

void
show_contact_message (Contact_Member *contact )
{
  STORED_MESSAGE_PTR msg;

  g_assert(contact != NULL);

  if (contact->stored_messages) {
    msg = (STORED_MESSAGE_PTR)contact->stored_messages->data;
    contact->stored_messages = g_slist_remove (contact->stored_messages, msg);		
    icq_msgbox (msg, contact->uin);
    
    g_free( msg->message );
    g_free( msg );

  }

  if (!contact->stored_messages) 
    gnomeicu_tree_set_contact_icon (contact,
                                    get_pixbuf_for_status (contact->status));
}

gint
message_input_key_callback (GtkWidget *widget, GdkEventKey *event)
{

#ifdef TRACE_FUNCTION
  g_print("message_input_key_callback\n");
#endif

  if ((event->keyval == GDK_Return || event->keyval == GDK_KP_Enter) && 
      (!(event->state & (GDK_CONTROL_MASK|GDK_SHIFT_MASK)) ^
       !preferences_get_bool(PREFS_GNOMEICU_ENTER_SENDS))) {

    send_message (g_object_get_data (G_OBJECT (widget), "message_dialog"));
    return TRUE;
  }
  
  return FALSE;
}

void message_usermenu_clicked_callback (GtkWidget *widget, gpointer data)
{
  Contact_Member *contact;
  GtkWidget *personal_menu;

  contact = g_object_get_data (G_OBJECT (widget), "contact");
  if (contact == NULL)
    return;
  personal_menu = user_popup (contact);
  gtk_menu_popup (GTK_MENU (personal_menu), NULL, NULL, 
                  message_cb_menu_position, widget, 0 ,0);
}


void message_cb_menu_position (GtkMenu *menu, gint *x, gint *y, gboolean* push_in, gpointer user_data)
{
	GtkWidget *widget, *button;
	gint screen_width, screen_height;

	GtkRequisition requisition;

#ifdef TRACE_FUNCTION
	g_print("message_cb_menu_position\n");
#endif

	widget = GTK_WIDGET (menu);
	button = GTK_WIDGET (user_data);

	gtk_widget_size_request (widget, &requisition);

	gdk_window_get_origin (button->window, x, y); 
	
	*x += button->allocation.x;
	*y += button->allocation.y;
	
	*y += button->allocation.height;
	
	screen_width = gdk_screen_width ();
	screen_height = gdk_screen_height ();

	if (*x + requisition.width > screen_width)
	  *x -= *x + requisition.width - screen_width;
	
}


/* 
   This function has been copied directly from pan... Thanks a lot to them.
*/

gboolean msg_url_motion_notify_event (GtkWidget *w, GdkEventMotion *event, 
			      gpointer user_data)
{
  static GdkCursor * cursor_current = NULL;
  static GdkCursor * cursor_ibeam = NULL;
  static GdkCursor * cursor_href = NULL;

#ifdef TRACE_FUNCTION
  g_print("motion_notify_event\n");
#endif

  if (event->window != NULL)
    {
      int x, y;
      char * url;
      GdkCursor * cursor_new;
      GdkModifierType state;

      // initialize static variables
      if (!cursor_ibeam)
	cursor_ibeam = gdk_cursor_new (GDK_XTERM);
      if (!cursor_href)
	cursor_href = gdk_cursor_new (GDK_HAND2);

      // pump out x, y, and state
      if (event->is_hint)
	gdk_window_get_pointer (event->window, &x, &y, &state);
      else {
	x = event->x;
	y = event->y;
	state = event->state;
      }

      // decide what cursor we should be using
      url = get_url_from_location (w, (int)event->x, (int)event->y);
      if (!url)
	cursor_new = cursor_ibeam;
      else {
	cursor_new = cursor_href;
	g_free (url);
      }

      // change the cursor if needed
      if (cursor_new != cursor_current)
	gdk_window_set_cursor (event->window, cursor_current=cursor_new);
    }

  return FALSE;
}


/* 
   This function has been copied from pan... Thanks a lot to them.
*/

gboolean msg_url_text_button_pressed (GtkWidget *w, GdkEventButton *event,
				  gpointer unused)
{
  g_return_val_if_fail (GTK_IS_TEXT_VIEW(w), FALSE);

  if (event->button==1 || event->button==2) {
    char * url = get_url_from_location (w, (int)event->x, (int)event->y);
    if (url != NULL) {

      if (!g_strrstr (url, "http://") && !g_strrstr (url, "https://") && !g_strrstr (url, "ftp://"))
      {
	char *tmpurl = NULL;
	tmpurl = g_strdup_printf("http://%s", url);
        g_free (url);
        url = tmpurl;
      }
      
      gnome_url_show(url, NULL);
      g_free (url);
    }
  }

  return FALSE;
}


/* 
   This function has been copied from pan... Thanks a lot to them.
*/

const char* find_next_url (const char *pch)
{
  for (; pch && *pch; ++pch) {
    if (*pch == 'h') {
      if (!strncmp (pch, "http://", 7))
	return pch;
      if (!strncmp (pch, "https://", 8))
	return pch;
    }

    if (*pch == 'f') {    
      if (!strncmp (pch, "ftp://", 6))
	return pch;
      if (!strncmp (pch, "ftp.", 4))
          return pch;
    }
    
    if (*pch == 'w') {
      if (!strncmp (pch, "www.", 4))
        return pch;
    }
  }

  return NULL;
}



/* 
   This function has been copied directly from pan... Thanks a lot to them.
*/

char*
get_url_from_location (GtkWidget * w, int x, int y)
{
	char * retval = NULL;
	gboolean clicked_on_url;
	GtkTextBuffer * text_buffer;
	static GtkTextTag * url_tag = NULL;
	GtkTextIter pos;
	int old_x = x, old_y = y;

	/* get the buffer */
	g_return_val_if_fail (GTK_IS_TEXT_VIEW(w), FALSE);
	text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW(w));

	/* translate coordinates */
	gtk_text_view_window_to_buffer_coords (GTK_TEXT_VIEW(w),GTK_TEXT_WINDOW_TEXT, old_x, old_y, &x, &y);

	/* did the user click on a url? */
	gtk_text_view_get_iter_at_location (GTK_TEXT_VIEW(w), &pos, x, y);
	if (url_tag == NULL) { 
		GtkTextTagTable * tag_table = gtk_text_buffer_get_tag_table (text_buffer);
		url_tag = gtk_text_tag_table_lookup (tag_table, "url");
	}
	clicked_on_url = gtk_text_iter_has_tag (&pos, url_tag);

	if (clicked_on_url)
	{
		GtkTextIter begin;
		GtkTextIter end;

		/* get the URL */
		begin = end = pos;
		gtk_text_iter_backward_to_tag_toggle (&begin, NULL);
		gtk_text_iter_forward_to_tag_toggle (&end, NULL);
		retval = gtk_text_iter_get_text (&begin, &end);
	}

	return retval;
}

void message_dialog_focus_callback(GtkWidget *widget, gpointer data)
{

  Contact_Member *contact = g_object_get_data (G_OBJECT (widget), "contact");
  GtkWidget *msg_dlg = glade_xml_get_widget (contact->msg_dlg_xml, 
					     "message_dialog");
  XWMHints *hints;

  if (contact->newmsg_blink_timeout) {
    gtk_timeout_remove(contact->newmsg_blink_timeout);
    
    contact->newmsg_blink_timeout = 0;
    contact->newmsg_blink = TRUE;

    set_window_icon( msg_dlg, "gnomeicu-message.png");
#if GTK_CHECK_VERSION(2,8,0)
    gtk_window_set_urgency_hint(GTK_WINDOW(msg_dlg), FALSE);
#else
    hints = XGetWMHints(GDK_WINDOW_XDISPLAY(msg_dlg->window), 
			GDK_WINDOW_XWINDOW(msg_dlg->window));
    hints->flags &= ~XUrgencyHint;
    XSetWMHints(GDK_WINDOW_XDISPLAY(msg_dlg->window), 
		GDK_WINDOW_XWINDOW(msg_dlg->window), hints);
    XFree(hints);
#endif

    msg_blink_timeout(contact);
  }
}

gboolean on_message_dialog_key_press_event(GtkDialog *dlg, GdkEventKey *event, gpointer data)
{
  if(event->keyval==GDK_Escape)
    {
      gtk_widget_hide_all(GTK_WIDGET(dlg));
    }

  return FALSE;
}

gboolean msg_focus_in_event(GtkWidget *input, GdkEventFocus *focus, 
			    GtkWidget *conversation)
{
  gtk_widget_grab_focus (input);

  return TRUE;
}
