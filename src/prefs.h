/* GnomeICU
 * Copyright (C) 1998-2003 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * Manage preferences using gconf
 *
 * Copyright (C) 2002-2003 Patrick Sung
 */

#ifndef __PREFS_H__
#define __PREFS_H__

#include <gdk/gdkcolor.h>
#include <gtk/gtkwidget.h>
#include <gconf/gconf-client.h>

enum GnomeICUPrefType {
  PREF_NONE,
  PREF_BOOLEAN,
  PREF_UINT,
  PREF_COLOR,
  PREF_CHOICE, /* choice (radio buttons) is not supported yet */
  PREF_COMBO,
  PREF_STRING,
  PREF_PATH,
  PREF_EXEC, /* button that launch an external program */
  PREF_CUSTOM
};

typedef struct _GnomeICUPrefData GnomeICUPrefData;
struct _GnomeICUPrefData {
  gchar *key;
  GtkWidget *widget;
  enum GnomeICUPrefType type;
  gpointer value;
  gpointer pr;
};

typedef struct _prefs_action prefs_action;
struct _prefs_action {
  void (*init_value)(GnomeICUPrefData *pd);
  void (*set_widget)(const GnomeICUPrefData *pd);
  void (*connect_signal)(GnomeICUPrefData *pd);
};

void preferences_dialog_show (void);
void preferences_dialog_show_cb (GtkWidget *w, gpointer d);

/* getters */
GSList *preferences_get_list (const gchar *key, GConfValueType ltype);
gboolean preferences_get_pair (const gchar *key, GConfValueType car_type,
                               GConfValueType cdr_type, gpointer car_retloc,
                               gpointer cdr_retloc);
gchar *preferences_get_string (const gchar *gconf_key);
gint preferences_get_int (const gchar *gconf_key);
gboolean preferences_get_bool (const gchar *gconf_key);
GdkColor *preferences_get_color (const gchar *gconf_key);

/* setters */
void preferences_set_list (const gchar *gconf_key, GConfValueType car_type,
			   GSList *list);
void preferences_set_pair (const gchar *key, GConfValueType car_type,
                           GConfValueType cdr_type, gconstpointer car,
                           gconstpointer cdr);
void preferences_set_string (const gchar *gconf_key, const gchar *data);
void preferences_set_int (const gchar *gconf_key, gint data);
void preferences_set_bool (const gchar *gconf_key, gboolean data);
void preferences_set_color (const gchar *gconf_key, const GdkColor *data);

void preferences_unset(const gchar *gconf_key);

/* watch key callback setup function */
guint preferences_watch_key (const gchar *gconf_key, void (*func)(void));

void preferences_default_init_value (GnomeICUPrefData *pd);

#endif /* __PREFS_H__ */
