/************************************
 Personal contact information
 (c) 1999 Jeremy Wise
 GnomeICU
*************************************/

#include "common.h"

#include "changenick.h"
#include "icons.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "personal_info.h"
#include "showlist.h"
#include "util.h"
#include "v7send.h"

#include <gtk/gtk.h>
#include <libgnomeui/gnome-href.h>
#include <string.h>

void show_personal_info (GtkWidget *widget, gpointer data)
{
	GSList *contact;

#ifdef TRACE_FUNCTION
	g_print( "show_personal_info\n" );
#endif

	contact = Contacts;
	while (contact != NULL && contact->data != data)
		contact = contact->next;

	if( contact == NULL )
		return;

        v7_request_info(mainconnection, kontakt->uin);

	dump_personal_info(kontakt->uin );
}

void dump_personal_info( UIN_T uin )
{
	GladeXML *xml;

	GSList *contact;

#ifdef TRACE_FUNCTION
	g_print( "dump_personal_info\n" );
#endif

	contact = Find_User( uin );
	if( contact == NULL )
		return;

	if (kontakt->info_dlg_xml) {
		v7_request_info(mainconnection, uin);

		xml = kontakt->info_dlg_xml;
		gtk_window_present (GTK_WINDOW(glade_xml_get_widget (xml, "userinfo_dialog")));
	} else {
		GtkListStore *store;
		GtkCellRenderer *renderer;
		GtkTreeViewColumn *column;
		GtkTreeView *treeview;

		xml = gicu_util_open_glade_xml ("user_info.glade", "userinfo_dialog");
                if (xml == NULL)
                  return;

                kontakt->info_dlg_xml = xml;

		g_object_set_data (G_OBJECT (glade_xml_get_widget (xml, "refresh_button")), "uin", uin);
		g_object_set_data (G_OBJECT (glade_xml_get_widget (xml, "refresh_button")), "dlg",
		                                 glade_xml_get_widget (xml, "userinfo_dialog"));
		g_object_set_data (G_OBJECT (glade_xml_get_widget (xml, "close_button")), "dlg",
		                                 glade_xml_get_widget (xml, "userinfo_dialog"));
		/* stuff for the GtkTreeView (list of emails) */
		treeview = GTK_TREE_VIEW( glade_xml_get_widget (xml, "emails_treeview") );
		store = gtk_list_store_new(1, G_TYPE_STRING);
		gtk_tree_view_set_model(treeview, GTK_TREE_MODEL(store));
		renderer = gtk_cell_renderer_text_new ();
		column = gtk_tree_view_column_new_with_attributes (_("E-mail"),
					renderer,
					"text", 0,
					NULL);
		gtk_tree_view_append_column (treeview, column);

		renderer = gtk_cell_renderer_text_new ();

		/* Interests TreeView */
		treeview = GTK_TREE_VIEW( glade_xml_get_widget (xml, "interests_treeview") );
		store = gtk_list_store_new(2, G_TYPE_STRING, G_TYPE_STRING);
		gtk_tree_view_set_model(treeview, GTK_TREE_MODEL(store));
		column = gtk_tree_view_column_new_with_attributes (_("Interests"),
					renderer,
					"text", 0,
					NULL);
		gtk_tree_view_append_column (treeview, column);
		column = gtk_tree_view_column_new_with_attributes (_("Desc."),
					renderer,
					"text", 1,
					NULL);
		gtk_tree_view_append_column (treeview, column);

		/* Affiliations TreeView */
		treeview = GTK_TREE_VIEW( glade_xml_get_widget (xml, "affiliations_treeview") );
		store = gtk_list_store_new(2, G_TYPE_STRING, G_TYPE_STRING);
		gtk_tree_view_set_model(treeview, GTK_TREE_MODEL(store));
		column = gtk_tree_view_column_new_with_attributes (_("Affiliation"),
					renderer,
					"text", 0,
					NULL);
		gtk_tree_view_append_column (treeview, column);
		column = gtk_tree_view_column_new_with_attributes (_("Desc."),
					renderer,
					"text", 1,
					NULL);
		gtk_tree_view_append_column (treeview, column);

		/* Past Backgrounds TreeView */
		treeview = GTK_TREE_VIEW( glade_xml_get_widget (xml, "past_background_treeview") );
		store = gtk_list_store_new(2, G_TYPE_STRING, G_TYPE_STRING);
		gtk_tree_view_set_model(treeview, GTK_TREE_MODEL(store));
		column = gtk_tree_view_column_new_with_attributes (_("Affiliation"),
					renderer,
					"text", 0,
					NULL);
		gtk_tree_view_append_column (treeview, column);
		column = gtk_tree_view_column_new_with_attributes (_("Desc."),
					renderer,
					"text", 1,
					NULL);
		gtk_tree_view_append_column (treeview, column);

		glade_xml_signal_autoconnect (xml);
	}

	/* gtk_widget_show_all (glade_xml_get_widget (xml, "userinfo_dialog")); */

	update_personal_info(uin);
}

void update_personal_info ( UIN_T uin )
{

  GSList *contact;
  gchar *string = NULL;
  gchar datebuf[30];
  GDate *date;
  GList *email;
  GList *list;
  GtkListStore *store;
  GtkTreeIter iter;

#ifdef TRACE_FUNCTION
	g_print( "update_personal_info\n" );
#endif

	contact = Find_User( uin );
	if( contact == NULL )
		return;

	if ( !strcmp (kontakt->nick, kontakt->uin) &&
	    kontakt->info->nick && kontakt->info->nick[0] != '\0') {
		strncpy (kontakt->nick, kontakt->info->nick, sizeof (kontakt->nick));
	}

	if (kontakt->info_dlg_xml == NULL)
		return;

	string = g_strdup_printf(_("User Info for %s"), (kontakt->info->nick != NULL) ? kontakt->info->nick : kontakt->uin);
	gtk_window_set_title (GTK_WINDOW (glade_xml_get_widget (kontakt->info_dlg_xml, "userinfo_dialog")), string);
	g_free(string);

	g_free(kontakt->info->ip);
	g_free(kontakt->info->port);

	if( kontakt->has_direct_connect)
	{
		kontakt->info->ip = g_strdup_printf( "%u.%u.%u.%u",
		         (BYTE) (kontakt->current_ip >> 24),
		         (BYTE) (kontakt->current_ip >> 16),
		         (BYTE) (kontakt->current_ip >> 8),
		         (BYTE) (kontakt->current_ip) );
		kontakt->info->port = g_strdup_printf( "%u", kontakt->port );
	}
	else
	{
		kontakt->info->ip = g_strdup( _("N/A") );
		kontakt->info->port = g_strdup( _("N/A") );
	}

	g_free( kontakt->info->version );
	kontakt->info->version = g_strdup_printf("%d", kontakt->version);

	g_free( kontakt->info->status );
	kontakt->info->status = g_strdup_printf(get_status_str( kontakt->status ));

	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "uin_entry")), kontakt->uin);

	if( kontakt->info->nick != NULL ) {
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "nick_entry")), kontakt->info->nick);
	}

	if( kontakt->info->first != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "fname_entry")), kontakt->info->first);

	if( kontakt->info->last != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "lname_entry")), kontakt->info->last);

	/*	if( kontakt->idletime > 0) {
	  char timebuf[100];
	  strftime(timebuf, 100, "%X", gmtime((time_t*)&kontakt->idletime));
	  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "idle_entry")), timebuf);
	} else
	  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "idle_entry")), _("Not Idle"));
	*/

	if( kontakt->status == STATUS_OFFLINE) 
	  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "onlinesince_entry")), _("Offline"));
	else {
	  char timebuf[100];
	  struct tm *mytm = localtime((time_t*)&kontakt->online_since);
	  if (mytm) {
	    strftime(timebuf,100,"%c",mytm);
	    gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "onlinesince_entry")), timebuf);
	  }
	}

	{
	  struct tm *mytm = NULL;
	  char timebuf[100];

	  if( kontakt->member_since) {
	    time_t tmp = kontakt->member_since;
	    mytm = localtime(&tmp);
	  }
	  if (mytm) {
	    strftime(timebuf,100,"%c",mytm);
	    gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "membersince_entry")), timebuf);
	    gtk_widget_show(glade_xml_get_widget (kontakt->info_dlg_xml, "membersince_label"));
	    gtk_widget_show(glade_xml_get_widget (kontakt->info_dlg_xml, "membersince_entry"));
	  } else if (GTK_WIDGET_VISIBLE(glade_xml_get_widget (kontakt->info_dlg_xml, "membersince_label"))) {
	    gtk_widget_hide(glade_xml_get_widget (kontakt->info_dlg_xml, "membersince_label"));
	    gtk_widget_hide(glade_xml_get_widget (kontakt->info_dlg_xml, "membersince_entry"));
	  }
	}

	if ( kontakt->idle_since) {
	  int idlefor = (time(NULL) - kontakt->idle_since)/60;
	  gchar *timestr;
	  if (kontakt->idle_since < 60)
	    timestr = g_strdup_printf(_("%d minutes"), idlefor); 
	  else if (kontakt->idle_since < 24*60)
	    timestr = g_strdup_printf(_("%d hours %d minutes"), 
				      idlefor / 60, 
				      idlefor % 60); 
	  else
	    timestr = g_strdup_printf(_("%d days %d hours %d minutes"), 
				      idlefor / (60*24),
				      (idlefor % (60*24))/60, 
				      idlefor % 60); 
	  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "idlefor_entry")), timestr);
	  g_free(timestr);
	  gtk_widget_show(glade_xml_get_widget (kontakt->info_dlg_xml, "idlefor_label"));
	  gtk_widget_show(glade_xml_get_widget (kontakt->info_dlg_xml, "idlefor_entry"));
	} else if (GTK_WIDGET_VISIBLE(glade_xml_get_widget (kontakt->info_dlg_xml, "idlefor_label"))){
	  gtk_widget_hide(glade_xml_get_widget (kontakt->info_dlg_xml, "idlefor_label"));
	  gtk_widget_hide(glade_xml_get_widget (kontakt->info_dlg_xml, "idlefor_entry"));
	}
	if (kontakt->status == STATUS_OFFLINE)
	  if( kontakt->last_seen) {
	    char timebuf[100];
	    struct tm *mytm = localtime(&kontakt->last_seen);
	    if (mytm) {
	      strftime(timebuf, 100, "%c", mytm);
	      gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "lastseen_entry")), timebuf);
	    }
	  } else
	    gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "lastseen_entry")), _("Never"));
	else
	  gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "lastseen_entry")), _("Now"));

	if( kontakt->info->sex == MALE )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "sex_entry")), _("Male"));
	else if( kontakt->info->sex == FEMALE )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "sex_entry")), _("Female"));
	else
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "sex_entry")), _("Unknown"));

	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "age_entry")),
	             (string = kontakt->info->age
			     ? g_strdup_printf( "%d", kontakt->info->age % 1000 )
			     : g_strdup( _("Unknown") )));
	g_free( string );

	string = g_strdup( get_language_name(kontakt->info->language1) );
	if( kontakt->info->language2 ) {
		gchar *temp = string;
		string = g_strdup_printf( "%s, %s", temp, get_language_name(kontakt->info->language2) );
		g_free( temp );
	}
	if( kontakt->info->language3 ) {
		gchar *temp = string;
		string = g_strdup_printf( "%s, %s", temp, get_language_name(kontakt->info->language3) );
		g_free( temp );
	}
	gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "languages_entry")), string);
	g_free( string );

	if( kontakt->info->auth )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "auth_entry")), _("Required"));
	else
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "auth_entry")), _("Not required"));

	if( kontakt->info->ip != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "ip_entry")), kontakt->info->ip);

	if( kontakt->info->port != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "port_entry")), kontakt->info->port);

	if (kontakt->version) 
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "protocol_entry")), kontakt->info->version);

	gtk_image_set_from_pixbuf (GTK_IMAGE (glade_xml_get_widget (kontakt->info_dlg_xml, "status_image")), NULL);
	gtk_image_set_from_pixbuf (GTK_IMAGE (glade_xml_get_widget (kontakt->info_dlg_xml, "status_image")), GDK_PIXBUF (get_pixbuf_for_status (kontakt->status)));

	if( kontakt->info->status != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "status_entry")), kontakt->info->status);

	if( kontakt->info->email != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "email_entry")), kontakt->info->email);

	if( kontakt->info->phone != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "phone_entry")), kontakt->info->phone);

	if( kontakt->info->fax != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "fax_entry")), kontakt->info->fax);

	if( (kontakt->info->homepage != NULL) && strlen(kontakt->info->homepage) )
	{
	    if( !strstr (kontakt->info->homepage, "://") )
		string = g_strdup_printf("http://%s", kontakt->info->homepage);
	    else
		string = g_strdup(kontakt->info->homepage);

	    gnome_href_set_text (GNOME_HREF(glade_xml_get_widget (kontakt->info_dlg_xml, "homepage_entry")), kontakt->info->homepage);
	    gnome_href_set_url (GNOME_HREF (glade_xml_get_widget (kontakt->info_dlg_xml, "homepage_entry")), string);
	    gtk_widget_show (GTK_WIDGET (glade_xml_get_widget (kontakt->info_dlg_xml, "homepage_entry")));
	    g_free( string );
	}

	if( kontakt->info->about != NULL )
	{
		GtkTextIter start, end;
		GtkTextBuffer* tb = gtk_text_view_get_buffer(GTK_TEXT_VIEW( glade_xml_get_widget (kontakt->info_dlg_xml, "about_text")));
		gtk_widget_freeze_child_notify( glade_xml_get_widget (kontakt->info_dlg_xml, "about_text") ); 
		gtk_text_buffer_get_start_iter(tb, &start);
		gtk_text_buffer_get_end_iter(tb, &end);
		gtk_text_buffer_delete(tb, &start, &end);
		gtk_text_buffer_insert_at_cursor(tb, kontakt->info->about, -1 );
		gtk_widget_thaw_child_notify( glade_xml_get_widget (kontakt->info_dlg_xml, "about_text") );
	}

	if( kontakt->info->occupation != 0 )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "occupation_entry")), get_work_name (kontakt->info->occupation));

	if( kontakt->info->job_pos != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "work_position_entry")), kontakt->info->job_pos);

	if( kontakt->info->department != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "work_department_entry")), kontakt->info->department);

	if( kontakt->info->company_name != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "work_company_entry")), kontakt->info->company_name);

	if( kontakt->info->work_address != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "work_street_entry")), kontakt->info->work_address);

	if( kontakt->info->work_city != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "work_city_entry")), kontakt->info->work_city);

	if( kontakt->info->work_state != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "work_state_entry")), kontakt->info->work_state);

	if( (kontakt->info->work_homepage != NULL) && strlen(kontakt->info->work_homepage) )
	{
	    if( !strstr (kontakt->info->work_homepage, "://") )
		string = g_strdup_printf("http://%s", kontakt->info->work_homepage);
	    else
		string = g_strdup(kontakt->info->work_homepage);

	    gnome_href_set_text (GNOME_HREF(glade_xml_get_widget (kontakt->info_dlg_xml, "work_homepage_entry")), kontakt->info->work_homepage);
	    gnome_href_set_url (GNOME_HREF (glade_xml_get_widget (kontakt->info_dlg_xml, "work_homepage_entry")), string);
	    gtk_widget_show (GTK_WIDGET (glade_xml_get_widget (kontakt->info_dlg_xml, "work_homepage_entry")));
	    g_free( string );
	}

	if( kontakt->info->work_zip != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "work_zip_entry")), kontakt->info->work_zip);
	
	if( kontakt->info->work_country != 0 )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "work_country_entry")), get_country_name (kontakt->info->work_country));
	
	if( kontakt->info->work_phone != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "work_phone_entry")), kontakt->info->work_phone);

	if( kontakt->info->work_fax != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "work_fax_entry")), kontakt->info->work_fax);
	
	if( kontakt->info->street != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "street_entry")), kontakt->info->street);

	if( kontakt->info->city != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "city_entry")), kontakt->info->city);

	if( kontakt->info->state != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "state_entry")), kontakt->info->state);

	if( kontakt->info->zip != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "zip_entry")), kontakt->info->zip);

	if( kontakt->info->country != 0 )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "country_entry")), get_country_name (kontakt->info->country));

	if( kontakt->info->timezone != 0 )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "timezone_entry")), get_timezone_name (kontakt->info->timezone));

	if( kontakt->info->cellular != NULL )
		gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "cellular_entry")), kontakt->info->cellular);

	if (kontakt->info->birth_year != 0 && 
	    kontakt->info->birth_month != 0 && 
	    kontakt->info->birth_day != 0)
	{
	    date = g_date_new_dmy(kontakt->info->birth_day, 
				  kontakt->info->birth_month,
				  kontakt->info->birth_year);
	    g_date_strftime(datebuf, 30, "%x", date);
	    g_date_free(date);
	    gtk_label_set_text (GTK_LABEL (glade_xml_get_widget (kontakt->info_dlg_xml, "birthday_entry")), datebuf);
	}

	store = GTK_LIST_STORE( gtk_tree_view_get_model(GTK_TREE_VIEW( glade_xml_get_widget (kontakt->info_dlg_xml, "emails_treeview") )) );
	gtk_list_store_clear(store);
	email = kontakt->info->emails;
	while (email) {
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter, 0, email->data + 1, -1);
		email = g_list_next(email);
	}


	store = GTK_LIST_STORE( gtk_tree_view_get_model(GTK_TREE_VIEW( glade_xml_get_widget (kontakt->info_dlg_xml, "interests_treeview") )) );
	gtk_list_store_clear(store);
	list = kontakt->info->interests;
	while (list) {
		if ((* (WORD *) list->data) != 0 ) {
			gtk_list_store_append (store, &iter);
			gtk_list_store_set (store, &iter,
			                    0, get_interest_name (* (WORD *) list->data),
			                    1, list->data+2,
			                    -1);
		}
		list = g_list_next(list);
	}
	gtk_tree_view_columns_autosize (GTK_TREE_VIEW( glade_xml_get_widget (kontakt->info_dlg_xml, "interests_treeview")));

	store = GTK_LIST_STORE( gtk_tree_view_get_model(GTK_TREE_VIEW( glade_xml_get_widget (kontakt->info_dlg_xml, "affiliations_treeview") )) );
	gtk_list_store_clear(store);
	list = kontakt->info->affiliations;
	while (list) {
		if ((* (WORD *) list->data) != 0 ) {
			gtk_list_store_append (store, &iter);
			gtk_list_store_set (store, &iter,
			                    0, get_affiliation_name (* (WORD *) list->data),
			                    1, list->data+2,
			                    -1);
		}
		list = g_list_next(list);
	}
	gtk_tree_view_columns_autosize (GTK_TREE_VIEW( glade_xml_get_widget (kontakt->info_dlg_xml, "affiliations_treeview")));

	store = GTK_LIST_STORE( gtk_tree_view_get_model(GTK_TREE_VIEW( glade_xml_get_widget (kontakt->info_dlg_xml, "past_background_treeview") )) );
	gtk_list_store_clear(store);
	list = kontakt->info->past_background;
	while (list) {
		if ((* (WORD *) list->data) != 0 ) {
			gtk_list_store_append (store, &iter);
			gtk_list_store_set (store, &iter,
			                    0, get_past_name (* (WORD *) list->data),
			                    1, list->data+2,
			                    -1);
		}
		list = g_list_next(list);
	}
	gtk_tree_view_columns_autosize (GTK_TREE_VIEW( glade_xml_get_widget (kontakt->info_dlg_xml, "past_background_treeview")));
}

gboolean userinfo_close_clicked_callback (GtkWidget *widget, gpointer data)
{
	GtkWidget *window;

	window = g_object_get_data (G_OBJECT (widget), "dlg");
	gtk_widget_hide (window);

	return TRUE;
}

gboolean userinfo_refresh_clicked_callback (GtkWidget *widget, gpointer data)
{
       UIN_T uin;

       uin = g_strdup (g_object_get_data (G_OBJECT (widget), "uin"));

       if (!is_connected(GTK_WINDOW(g_object_get_data (G_OBJECT (widget), "dlg")), _("You can not update user information while disconnected.")))
             return TRUE;
        
       v7_request_info(mainconnection, uin);

	   g_free (uin);
       return TRUE;
}

gboolean userinfo_delete_callback (GtkWidget *widget, gpointer data)
{
	gtk_widget_hide (widget);
	
	return TRUE;
}
