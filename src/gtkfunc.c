/************************************
 Miscellaneous functions
 (c) 1999 Jeremy Wise
 GnomeICU
*************************************/

#include "common.h"
#include "changenick.h"
#include "detach.h"
#include "gnomecfg.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "history.h"
#include "icons.h"
#include "msg.h"
#include "personal_info.h"
#include "response.h"
#include "sendcontact.h"
#include "showlist.h"
#include "tcp.h"
#include "user_popup.h"
#include "util.h"
#include "v7login.h"
#include "v7send.h"
#include "v7snac13.h"
#include "groups.h"
#include "group_popup.h"
#include "auto_respond.h"
#include "auth.h"
#include "newsignup.h"

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <libgnome/libgnome.h>
#include <libgnomeui/gnome-entry.h>
#include <libgnomevfs/gnome-vfs-mime-utils.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

#include <string.h>
#include <unistd.h>

struct URLInfo
{
	UIN_T uin;
	GtkWidget *url;
	GtkWidget *desc;
};

/*** Local function declarations ***/
static void icq_filetransfer_response (GtkDialog *dialog, int response, gpointer xfer);
static void icq_auth_request_response (GtkDialog *dialog, int response, gpointer data);
static void icq_send_url_response (GtkDialog *dialog, int response, struct URLInfo *urlinfo);

static void cb_menu_position (GtkMenu *menu, gint *x, gint *y, gboolean* push_in, gpointer user_data);

static void open_file( GtkWidget *widget, gpointer data );
static void del_file( GtkWidget *widget, gpointer data );
/* static void open_dir( GtkWidget *widget, gpointer data ); */

static void icq_user_added_you_msgbox(GSList *contact);
static void icq_user_authorized_you_msgbox(STORED_MESSAGE_PTR message_text, GSList *contact);
static void icq_filetransfer_window(STORED_MESSAGE_PTR message_text,
				    GSList* contact);
static void icq_authorization_request_window(STORED_MESSAGE_PTR message_text,
					     GSList* contact);
static void icq_url_window(STORED_MESSAGE_PTR message_text, GSList* contact);
static gboolean search_nick_timeout(GString *search_nick);
static void search_nick_in_treeview(GtkTreeView *widget, GdkEventKey *ev,
				    GString *search_nick, guint *timeout_source);

enum {
        ICQ_RESPONSE_SEND
};

static void icq_user_added_you_msgbox(GSList *contact)
{
	GtkWidget *dialog;
	gchar* sender;
	gint result;

	g_assert(contact != NULL);

	switch(kontakt->info->sex) {
	case FEMALE:
		sender = g_strdup_printf(_("%s has added you to her list"),
					 kontakt->nick);
		break;

	case MALE:
		sender = g_strdup_printf(_("%s has added you to his list"),
					 kontakt->nick);
		break;
	default:
		sender = g_strdup_printf(_("%s has added you to his/her list"),
					 kontakt->nick);
		break;
	}

	dialog = gtk_message_dialog_new (NULL, 0,
	                                 GTK_MESSAGE_INFO, GTK_BUTTONS_NONE,
	                                 sender);

	if (!kontakt->inlist)
	  gtk_dialog_add_button(GTK_DIALOG(dialog), _("Add User"), 1);
	
	gtk_dialog_add_button(GTK_DIALOG(dialog),
			      GTK_STOCK_OK ,GTK_RESPONSE_OK);
	
	result = gtk_dialog_run(GTK_DIALOG(dialog));
	
	if (result == 1 && 
	    is_connected(GTK_WINDOW(dialog),
			 _("You can not add a contact while disconnected."))) {
	  GtkWidget *adduser_dialog;
	  GtkWidget *label, *option, *menu, *box;
	  gchar *str;
	  GSList *g;

	  adduser_dialog = gtk_dialog_new_with_buttons (_("Pick a Group"),
							GTK_WINDOW (dialog),
							GTK_DIALOG_DESTROY_WITH_PARENT,
							GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
							GTK_STOCK_OK, GTK_RESPONSE_OK,
							NULL);

	  gtk_container_set_border_width(GTK_CONTAINER(adduser_dialog), 6);
	  gtk_box_set_spacing(GTK_BOX(GTK_DIALOG (adduser_dialog)->vbox), 12);

	  box = gtk_hbox_new (FALSE, 12);
	  gtk_container_set_border_width(GTK_CONTAINER(box), 6);
	  
	  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (adduser_dialog)->vbox), box);

	  label = gtk_label_new (_("Pick a group for this user:"));
	  gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 0);

	  option = gtk_option_menu_new ();
	  gtk_box_pack_start (GTK_BOX (box), option, FALSE, FALSE, 0);
	  menu = gtk_menu_new ();

	  for (g = Groups; g != NULL; g = g->next) {
	    label = gtk_menu_item_new_with_label (((GroupInfo *)g->data)->name);
	    g_object_set_data (G_OBJECT (label), "gid",
			       GUINT_TO_POINTER ((guint)((GroupInfo *)g->data)->gid));
	    gtk_menu_shell_append (GTK_MENU_SHELL (menu), label);
	  }

	  gtk_option_menu_set_menu (GTK_OPTION_MENU (option), menu);
	  gtk_option_menu_set_history (GTK_OPTION_MENU (option), 0);

	  gtk_widget_show_all (adduser_dialog);

	  result = gtk_dialog_run(GTK_DIALOG(adduser_dialog));

	  if ( result  == GTK_RESPONSE_OK  &&
	       is_connected(GTK_WINDOW(adduser_dialog),
			    _("You can not add a contact while disconnected."))) {
	    GList *item;
	    guint gid;

	    item = g_list_nth (GTK_MENU_SHELL (menu)->children,
			       gtk_option_menu_get_history (GTK_OPTION_MENU (option)));
	    gid = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (item->data),
						       "gid"));
	    v7_try_add_uin (mainconnection, kontakt->uin, kontakt->nick, 
			    gid, FALSE);
	  }

	  gtk_widget_destroy(adduser_dialog);
	    
	}

	gtk_widget_destroy(dialog);

	g_free(sender);
}


static void icq_user_authorized_you_msgbox(STORED_MESSAGE_PTR message_text, GSList *contact)
{
	GtkWidget *dialog;

	g_assert(contact != NULL);
	if (message_text->data) {
	  dialog = gtk_message_dialog_new (GTK_WINDOW (MainData->window),
					   GTK_DIALOG_DESTROY_WITH_PARENT,
					   GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
					   _("You are authorized to add %s to your list"),
					   kontakt->nick);
	} else {
	  dialog = gtk_message_dialog_new (GTK_WINDOW (MainData->window),
					   GTK_DIALOG_DESTROY_WITH_PARENT,
					   GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
					   _("You are NOT authorized to add %s to your list"),
					   kontakt->nick);
	}

	if (message_text->message) {
	  g_print(message_text->message);
	  gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(dialog),
						   "%s", 
						   message_text->message);
	}


	g_signal_connect (G_OBJECT (dialog), "response",
	                  G_CALLBACK (gtk_widget_destroy), NULL);

	gtk_widget_show (dialog);

}


static void icq_filetransfer_window(STORED_MESSAGE_PTR message_text,
				    GSList* contact)
{
	GSList *xfer;
	GtkWidget *window = NULL;
	GtkWidget *dialog_vbox1;
	GtkWidget *vbox1;
	GtkWidget *label1;
	GtkWidget *label2;
	GtkWidget *label3;
	GtkWidget *label4;
	GtkWidget *label5;
	GtkWidget *label6;
	GtkWidget *table;
	GtkWidget *scrolledwindow1;
	GtkWidget *text;
	GtkWidget *entry1;
	GtkWidget *entry2;
	gchar* sender;

	g_assert(message_text != NULL && contact != NULL);

	/* Make sure the file transfer is still there */
	for (xfer = kontakt->file_xfers;xfer;xfer=xfer->next)
		if (xfer->data == message_text->data)
			break;

	/* Nope, it's gone */
	if (!xfer) {
		GtkWidget *dialog;
		dialog = gtk_message_dialog_new (GTK_WINDOW (MainData->window),
		                                 GTK_DIALOG_DESTROY_WITH_PARENT,
		                                 GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
		                                 _("File transfer timed out."));
		gtk_widget_show(dialog);
		g_signal_connect (G_OBJECT (dialog), "response",
		                  G_CALLBACK (gtk_widget_destroy), NULL);
		return;
	}

	window = gtk_dialog_new_with_buttons (_("File Offer"),
                                              GTK_WINDOW (MainData->window),
                                              GTK_DIALOG_DESTROY_WITH_PARENT,
                                              GTK_STOCK_NO, GTK_RESPONSE_NO,
                                              GTK_STOCK_YES, GTK_RESPONSE_YES,
                                              NULL);

	gtk_window_set_role (GTK_WINDOW (window), "GnomeICU_FileOffer");
	gtk_dialog_set_default_response (GTK_DIALOG (window), GTK_RESPONSE_YES);

	set_window_icon( window, "gnomeicu-file.png" );

	dialog_vbox1 = GTK_DIALOG (window)->vbox;

	vbox1 = gtk_vbox_new (FALSE, 4);
	gtk_widget_show (vbox1);
	gtk_box_pack_start (GTK_BOX (dialog_vbox1), vbox1, TRUE, TRUE, 0);

	sender = g_strdup_printf( _("Accept file from %s?\n"), kontakt->nick );

	label1 = gtk_label_new ( sender );
	g_free( sender );
	gtk_widget_show (label1);
	gtk_box_pack_start (GTK_BOX (vbox1), label1, FALSE, FALSE, 0);

	table = gtk_table_new (4, 3, FALSE);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (vbox1), table, TRUE, TRUE, 0);

	label3 = gtk_label_new (_("Size:"));
	gtk_widget_show (label3);
	gtk_table_attach (GTK_TABLE (table), label3, 0, 1, 1, 2,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_label_set_justify (GTK_LABEL (label3), GTK_JUSTIFY_RIGHT);
	gtk_misc_set_alignment (GTK_MISC (label3), 1, 0.5);

	label2 = gtk_label_new (_("Name:"));
	gtk_widget_show (label2);
	gtk_table_attach (GTK_TABLE (table), label2, 0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_label_set_justify (GTK_LABEL (label2), GTK_JUSTIFY_RIGHT);
	gtk_misc_set_alignment (GTK_MISC (label2), 1, 0.5);

	label5 = gtk_label_new ("");
	gtk_widget_show (label5);
	gtk_table_attach (GTK_TABLE (table), label5, 1, 2, 2, 3,
			  (GtkAttachOptions) (0),
			  (GtkAttachOptions) (0), 0, 0);

	label4 = gtk_label_new (_("\nReason:"));
	gtk_widget_show (label4);
	gtk_table_attach (GTK_TABLE (table), label4, 0, 2, 2, 3,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_label_set_justify (GTK_LABEL (label4), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label4), 0, 0.5);

	label6 = gtk_label_new ("");
	gtk_widget_show (label6);
	gtk_table_attach (GTK_TABLE (table), label6, 2, 3, 2, 3,
			  (GtkAttachOptions) (0),
			  (GtkAttachOptions) (0), 0, 0);

	scrolledwindow1 = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwindow1);
	gtk_table_attach (GTK_TABLE (table), scrolledwindow1, 0, 3, 3, 4,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow1), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);

	text = gtk_text_view_new ();
	gtk_widget_show (text);
	gtk_text_view_set_wrap_mode( GTK_TEXT_VIEW( text ),
				     GTK_WRAP_WORD);
	gtk_text_view_set_editable( GTK_TEXT_VIEW( text ), FALSE );
	gtk_container_add (GTK_CONTAINER (scrolledwindow1), text);

	gtk_text_buffer_insert_at_cursor( gtk_text_view_get_buffer(GTK_TEXT_VIEW( text )), message_text->message, -1 );

	entry1 = gtk_entry_new ();
	gtk_widget_show (entry1);
	gtk_entry_set_editable( GTK_ENTRY( entry1 ), FALSE );
	sender = g_strdup_printf( "%d", ((XferInfo *)message_text->data)->total_bytes );
	gtk_entry_set_text( GTK_ENTRY( entry1 ), sender );

	g_free( sender );
	gtk_table_attach (GTK_TABLE (table), entry1, 1, 3, 1, 2,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);

	entry2 = gtk_entry_new ();
	gtk_widget_show (entry2);
	gtk_entry_set_editable( GTK_ENTRY( entry2 ), FALSE );
	gtk_entry_set_text( GTK_ENTRY( entry2 ), ((XferInfo *)message_text->data)->filename );

	gtk_table_attach (GTK_TABLE (table), entry2, 1, 3, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);

	g_signal_connect (G_OBJECT (window), "response",
			  G_CALLBACK (icq_filetransfer_response),
			  message_text->data);

	gtk_widget_show_all( window );
	return;
}



static void icq_authorization_request_window(STORED_MESSAGE_PTR message_text, 
					     GSList* contact)
{
  g_assert(message_text != NULL && contact != NULL);
  
  auth_receive_request (kontakt->uin, kontakt->nick, message_text->message);

}


static void icq_url_window(STORED_MESSAGE_PTR message_text, GSList* contact)
{
	GtkWidget *window = NULL;
	GtkWidget *dialog_vbox1;
	GtkWidget *label1;
	GtkWidget *label2;
	GtkWidget *label3;
	GtkWidget *hbox1;
	GtkWidget *table;
	GtkWidget *url, *desc;
	GtkWidget *button1;
	gchar* sender;

	g_assert(message_text != NULL && contact != NULL);

	sender = g_strdup_printf( _("Received URL from %s:"), kontakt->nick );
	
	window = gtk_dialog_new_with_buttons (_("Received URL"),
                                              GTK_WINDOW (MainData->window),
                                              GTK_DIALOG_DESTROY_WITH_PARENT,
                                              GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
                                              NULL);
	gtk_window_set_role (GTK_WINDOW (window), "GnomeICU_URL");
	gtk_window_set_default_size( GTK_WINDOW( window ), 500, 150 );
	gtk_dialog_set_default_response (GTK_DIALOG (window), GTK_RESPONSE_CLOSE);
	
	set_window_icon( window, "gnomeicu-url.png" );
	
	dialog_vbox1 = GTK_DIALOG (window)->vbox;
	
	label1 = gtk_label_new ( sender );
	g_free( sender);
	gtk_widget_show (label1);
	gtk_box_pack_start (GTK_BOX (dialog_vbox1), label1, FALSE, FALSE, 0);
	gtk_label_set_justify (GTK_LABEL (label1), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label1), 0, 0.5);
	
	table = gtk_table_new (2, 2, FALSE);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (dialog_vbox1), table, TRUE, TRUE, 0);
	gtk_table_set_row_spacings (GTK_TABLE (table), 4);
	gtk_table_set_col_spacings (GTK_TABLE (table), 4);
	
	hbox1 = gtk_hbox_new (FALSE, 4);
	gtk_widget_show (hbox1);
	gtk_table_attach (GTK_TABLE (table), hbox1, 1, 2, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);
	
	url = gtk_entry_new ();
	gtk_widget_show (url);
	gtk_box_pack_start (GTK_BOX (hbox1), url, TRUE, TRUE, 0);
	
	gtk_entry_set_text( GTK_ENTRY( url ), message_text->data );
	
	button1 = gtk_button_new_from_stock (GTK_STOCK_JUMP_TO);
	gtk_widget_show (button1);
	gtk_box_pack_start (GTK_BOX (hbox1), button1, FALSE, FALSE, 0);
	
	g_signal_connect( G_OBJECT( button1 ), "clicked",
			  G_CALLBACK( display_url ),
			  (gpointer) gtk_entry_get_text( GTK_ENTRY( url ) ));
	
	desc = gtk_entry_new ();
	gtk_widget_show (desc);
	gtk_table_attach (GTK_TABLE (table), desc, 1, 2, 1, 2,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	
	gtk_entry_set_text( GTK_ENTRY( desc ), message_text->message );
	
	label2 = gtk_label_new (_("URL:"));
	gtk_widget_show (label2);
	gtk_table_attach (GTK_TABLE (table), label2, 0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL), 0, 0);
	gtk_label_set_justify (GTK_LABEL (label2), GTK_JUSTIFY_RIGHT);
	gtk_misc_set_alignment (GTK_MISC (label2), 1, 0.5);
	
	label3 = gtk_label_new (_("Description:"));
	gtk_widget_show (label3);
	gtk_table_attach (GTK_TABLE (table), label3, 0, 1, 1, 2,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL), 0, 0);
	gtk_label_set_justify (GTK_LABEL (label3), GTK_JUSTIFY_RIGHT);
	gtk_misc_set_alignment (GTK_MISC (label3), 1, 0.5);
	
	g_signal_connect (G_OBJECT (window), "response",
	                  G_CALLBACK (gtk_widget_destroy), NULL);
	gtk_widget_show (window);

	g_free(message_text->data);
}


/* DONE: split this function by message type
 * TODO: maybe it would be better to load gui from glade file */
void icq_msgbox( STORED_MESSAGE_PTR message_text, UIN_T uin )
{
	GSList *contact;

#ifdef TRACE_FUNCTION
	g_print( "icq_msgbox\n" );
#endif

	g_assert(message_text != NULL);

	contact = Find_User( uin );
	if( contact == NULL )
		return;

	switch(message_text->type){
	case MESSAGE_TEXT:
		/*** User has received a TEXT message ***/
	        open_message_dlg_with_message (kontakt, message_text);
		break;
	case MESSAGE_CONT_LIST:
		/*** User has received a CONTACT LIST ***/
		received_contact_list_window( kontakt, message_text->data );
		break;
		
	case MESSAGE_USER_ADD:
		/*** User has added you to his/her list ***/
		icq_user_added_you_msgbox(contact);
		break;

	case MESSAGE_USER_AUTHD:
		/*** Sender has authorized you to add him to your list ***/
		icq_user_authorized_you_msgbox(message_text, contact);
		break;

	case MESSAGE_CHAT_REQ:
		/*** Sender has sent you a chat request ***/
		break;

	case MESSAGE_FILE_REQ:
		/*** Sender has offered you a file ***/
		icq_filetransfer_window(message_text, contact);
		break;
	case MESSAGE_AUTH_REQ:
		/* Sender wishes to add you to his/her list */
		icq_authorization_request_window(message_text, contact);
		break;
	case MESSAGE_URL:
		/* Sender has sent you a URL */
		icq_url_window(message_text, contact);
		break;

	default:
		g_warning("Unknown message type: %d\n", message_text->type);
	}	       
}

void send_url_window( GtkWidget *widget, char *defaulturl )
{
	GtkWidget *window;
	GtkWidget *vbox12;
	GtkWidget *label20;
	GtkWidget *table4;
	GtkWidget *label21;
	GtkWidget *label22;
	GtkWidget *entry2;
	GtkWidget *combo_entry2;
	GtkWidget *entry3;
	GtkWidget *combo_entry3;

	gchar *wtitle;

	struct URLInfo *urlinfo;

	GSList *contact;

#ifdef TRACE_FUNCTION
	g_print( "send_url_window\n" );
#endif

	if (g_object_get_data (G_OBJECT (widget), "contact") == NULL)
		return;
	else {
		contact = Contacts;
		while (contact != NULL && contact->data != g_object_get_data (G_OBJECT (widget), "contact"))
			contact = contact->next;
	}

	if( contact == NULL )
		return;

	window = gtk_dialog_new_with_buttons (_("Send URL"),
                                              GTK_WINDOW (MainData->window),
                                              GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR,
                                              NULL);
	gtk_window_set_default_size (GTK_WINDOW (window), 500, -1);
	gtk_window_set_role (GTK_WINDOW (window), "GnomeICU_SendURL");

	set_window_icon( window, "gnomeicu-url.png" );

	vbox12 = gtk_vbox_new (FALSE, 3);
	gtk_container_set_border_width (GTK_CONTAINER (vbox12), 6);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (window)->vbox), vbox12, TRUE, TRUE, 0);
	gtk_widget_show (vbox12);

	wtitle = g_strdup_printf( _("Send URL to: %s"), kontakt->nick );
	label20 = gtk_label_new ( wtitle );
	g_free( wtitle );
	gtk_widget_show (label20);
	gtk_box_pack_start (GTK_BOX (vbox12), label20, FALSE, FALSE, 0);
	gtk_label_set_justify (GTK_LABEL (label20), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label20), 0, 0.5);

	table4 = gtk_table_new (2, 2, FALSE);
	gtk_widget_show (table4);
	gtk_box_pack_start (GTK_BOX (vbox12), table4, TRUE, TRUE, 0);
	gtk_table_set_row_spacings (GTK_TABLE (table4), 4);
	gtk_table_set_col_spacings (GTK_TABLE (table4), 4);

	label21 = gtk_label_new_with_mnemonic (_("_URL:"));
	gtk_widget_show (label21);
	gtk_table_attach (GTK_TABLE (table4), label21, 0, 1, 0, 1,
	                  (GtkAttachOptions) (GTK_FILL),
	                  (GtkAttachOptions) (GTK_FILL), 0, 0);
	gtk_label_set_justify (GTK_LABEL (label21), GTK_JUSTIFY_RIGHT);
	gtk_misc_set_alignment (GTK_MISC (label21), 1, 0.5);

	label22 = gtk_label_new_with_mnemonic (_("_Description:"));
	gtk_widget_show (label22);
	gtk_table_attach (GTK_TABLE (table4), label22, 0, 1, 1, 2,
	                  (GtkAttachOptions) (GTK_FILL),
	                  (GtkAttachOptions) (GTK_FILL), 0, 0);
	gtk_label_set_justify (GTK_LABEL (label22), GTK_JUSTIFY_RIGHT);
	gtk_misc_set_alignment (GTK_MISC (label22), 1, 0.5);

	entry2 = gnome_entry_new ("gnomeicu_url");
	gtk_widget_show (entry2);
	gtk_table_attach (GTK_TABLE (table4), entry2, 1, 2, 0, 1,
	                  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
	                  (GtkAttachOptions) (0), 0, 0);

	combo_entry2 = gnome_entry_gtk_entry (GNOME_ENTRY (entry2));
	gtk_entry_set_text(GTK_ENTRY(combo_entry2), defaulturl);
	g_signal_connect_swapped (G_OBJECT (combo_entry2), "activate",
	                          G_CALLBACK (gtk_window_activate_default),
	                          window);

	entry3 = gnome_entry_new ("gnomeicu_history_desc");
	gtk_widget_show (entry3);
	gtk_table_attach (GTK_TABLE (table4), entry3, 1, 2, 1, 2,
	                  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
	                  (GtkAttachOptions) (0), 0, 0);

	combo_entry3 = gnome_entry_gtk_entry (GNOME_ENTRY (entry3));
	g_signal_connect_swapped (G_OBJECT (combo_entry3), "activate",
	                          G_CALLBACK (gtk_window_activate_default),
	                          window);

	gtk_dialog_add_button (GTK_DIALOG (window),
	                       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_action_widget (GTK_DIALOG (window),
	                              make_button_with_pixmap (_("_Send"), GTK_STOCK_JUMP_TO),
	                              ICQ_RESPONSE_SEND);
	gtk_dialog_set_default_response (GTK_DIALOG (window), ICQ_RESPONSE_SEND);
	
	urlinfo = g_new0( struct URLInfo, 1 );
	urlinfo->uin = kontakt->uin;
	urlinfo->url = combo_entry2;
	urlinfo->desc = combo_entry3;
	
	g_signal_connect (G_OBJECT (window), "response",
			  G_CALLBACK (icq_send_url_response), urlinfo);

	gtk_window_set_focus (GTK_WINDOW (window), combo_entry2);

	gtk_widget_show (window);
}

void remove_contact(GSList *contact, gboolean rem_history)
{
#ifdef TRACE_FUNCTION
	g_print( "remove_contact\n" );
#endif

	if( contact == NULL )
		return;

	if (rem_history)
	{
		gchar *path;

		path = g_strdup_printf( "%s/.icq/history/%s.txt", g_get_home_dir(),
		                        kontakt->uin );
		unlink( path );
		g_free( path );
	}

	if( kontakt->sok )
	{
		close( kontakt->sok );
		g_io_channel_shutdown( kontakt->gioc, TRUE, NULL );
		g_source_remove( kontakt->giocw );
	}

	if( kontakt->detached_window )
		gtk_widget_destroy( kontakt->detached_window );

	if (kontakt->msg_dlg_xml)
		gtk_widget_destroy (glade_xml_get_widget (kontakt->msg_dlg_xml, "message_dialog"));

	if (kontakt->info_dlg_xml)
		gtk_widget_destroy (glade_xml_get_widget (kontakt->info_dlg_xml, "userinfo_dialog"));

	gnomeicu_tree_user_remove (kontakt);

        g_free (contact->data);

	Contacts = g_slist_remove (Contacts, contact->data);	

	contact->data = NULL;

}

void icq_set_status( GtkWidget *widget, gpointer data )
{
	int status = GPOINTER_TO_INT( data );

	if ( status == STATUS_OFFLINE )
		icq_set_status_offline( NULL, NULL ); return;
	switch( status & 0xffff )
	{
		case STATUS_ONLINE:
			icq_set_status_online( NULL, NULL ); break;
		case STATUS_AWAY:
			icq_set_status_away( NULL, NULL ); break;
		case STATUS_NA:
			icq_set_status_na( NULL, NULL ); break;
		case STATUS_FREE_CHAT:
			icq_set_status_ffc( NULL, NULL ); break;
		case STATUS_OCCUPIED:
			icq_set_status_occ( NULL, NULL ); break;
		case STATUS_DND:
		case STATUS_DND_MAC:
			icq_set_status_dnd( NULL, NULL ); break;
		case STATUS_INVISIBLE:
			icq_set_status_invisible( NULL, NULL ); break;
		default:
			g_warning("Unknown status: 0x%x\n", status & 0xffff);
	}
}

void icq_set_status_online( GtkWidget *widget, gpointer data )
{
#ifdef TRACE_FUNCTION
	g_print( "icq_set_status_online\n" );
#endif

	if( Current_Status == STATUS_OFFLINE )
	{
/*       	create_tcp_line(); */

  		preset_status = STATUS_ONLINE; 
  		Done_Login = FALSE; 

		if(!strcmp(passwd, "")) 
		  show_uin_pwd_dialog();
		if(strcmp(passwd, "")) 
		  v7_new_login_session();
                
	}
	else
	{
		v7_setstatus(mainconnection,  STATUS_ONLINE );
	}
}

void icq_set_status_offline( GtkWidget *widget, gpointer data )
{
	GSList *contact;

#ifdef TRACE_FUNCTION
	g_print( "icq_set_status_offline\n" );
#endif

	contact = Contacts;
        
        v7_quit();

	if( Current_Status == STATUS_OFFLINE )
          return;

	Current_Status = STATUS_OFFLINE;

	while( contact != NULL )
	{
/*		set_contact_status (contact, STATUS_OFFLINE);*/
		if (kontakt->status != STATUS_OFFLINE)
			User_Offline (contact);
		kontakt->have_tcp_connection = NO;
		if( kontakt->sok )
		{
			g_io_channel_shutdown( kontakt->gioc, FALSE, NULL );
			close( kontakt->sok );
			g_source_remove( kontakt->giocw );
			kontakt->gioc = 0;
			kontakt->sok = 0;
		}
		contact = contact->next;
	}

        enable_online_events = FALSE;

	gnomeicu_set_status_button (FALSE);
}

int icq_set_status_away( GtkWidget *widget, gpointer noask )
{
#ifdef TRACE_FUNCTION
	g_print( "icq_set_status_away\n" );
#endif

	if( Current_Status == STATUS_OFFLINE )
	{
/*  		create_tcp_line(); */
                preset_status = STATUS_AWAY;
  		Done_Login = FALSE; 

		if(!strcmp(passwd, "")) 
		  show_uin_pwd_dialog();
		else
		  v7_new_login_session();  
	}
	else
	{
		v7_setstatus(mainconnection,  STATUS_AWAY );
	}
	if(!noask)
	  auto_respond_pick_msg_window_open ();

	return FALSE;
}

void icq_set_status_na( GtkWidget *widget, gpointer noask )
{
#ifdef TRACE_FUNCTION
	g_print( "icq_set_status_na\n" );
#endif

	if( Current_Status == STATUS_OFFLINE )
	{
/*  		create_tcp_line(); */
		preset_status = STATUS_NA;
                Done_Login = FALSE; 

		if(!strcmp(passwd, "")) 
		  show_uin_pwd_dialog();		
		else
		  v7_new_login_session();  
	}
	else
	{
		v7_setstatus(mainconnection,  STATUS_NA );
	}
	if(!noask)
	  auto_respond_pick_msg_window_open ();
}

void icq_set_status_invisible( GtkWidget *widget, gpointer data )
{
#ifdef TRACE_FUNCTION
	g_print( "icq_set_status_invisible\n" );
#endif

	if( Current_Status == STATUS_OFFLINE )
	{
/*  		create_tcp_line(); */
		preset_status = STATUS_INVISIBLE;
                Done_Login = FALSE; 

		if(!strcmp(passwd, "")) 
		  show_uin_pwd_dialog();
		else
		  v7_new_login_session();   
	}
	else
	{
		v7_setstatus(mainconnection,  STATUS_INVISIBLE );
	}
}

void icq_set_status_ffc( GtkWidget *widget, gpointer data )
{
#ifdef TRACE_FUNCTION
	g_print( "icq_set_status_ffc\n" );
#endif

	if( Current_Status == STATUS_OFFLINE )
	{
/*  		create_tcp_line(); */
		preset_status = STATUS_FREE_CHAT;
                Done_Login = FALSE; 

		if(!strcmp(passwd, "")) 
		  show_uin_pwd_dialog();	       
		else
		  v7_new_login_session();  
	}
	else
	{
		v7_setstatus(mainconnection,  STATUS_FREE_CHAT );
	}
}

void icq_set_status_occ( GtkWidget *widget, gpointer noask )
{
#ifdef TRACE_FUNCTION
	g_print( "icq_set_status_occ\n" );
#endif

	if( Current_Status == STATUS_OCCUPIED )
		return;

	if( Current_Status == STATUS_OFFLINE )
	{
/*  		create_tcp_line(); */
		preset_status = STATUS_OCCUPIED;
                Done_Login = FALSE;

		if(!strcmp(passwd, "")) 
		  show_uin_pwd_dialog();
		else
		  v7_new_login_session();  
	}
	else
	{
		v7_setstatus(mainconnection,  STATUS_OCCUPIED );
	}
		if(!noask)
                  auto_respond_pick_msg_window_open ();
}

void icq_set_status_dnd( GtkWidget *widget, gpointer noask )
{
#ifdef TRACE_FUNCTION
	g_print( "icq_set_status_dnd\n" );
#endif

	if( Current_Status == STATUS_DND )
		return;

	if( Current_Status == STATUS_OFFLINE )
	{
		/*  create_tcp_line(); */
  		preset_status = STATUS_DND; 
  		Done_Login = FALSE; 

		if(!strcmp(passwd, "")) 
		  show_uin_pwd_dialog();
		else
		  v7_new_login_session();  
	}
	else
	{
		v7_setstatus(mainconnection,  STATUS_DND );
	}
	if(!noask)
	  auto_respond_pick_msg_window_open ();
}

void display_url( GtkWidget *widget, const char *url )
{
	gnome_url_show( url, NULL );
}

void display_url_from_entry( GtkWidget *widget, GtkEntry *entry )
{
	display_url( NULL, gtk_entry_get_text( entry ) );
}

int hide_ch_window( GtkWidget *widget, GdkEvent *event, GtkWidget *window )
{
#ifdef TRACE_FUNCTION
	g_print( "hide_ch_window\n" );
#endif
	gtk_widget_hide( window );
	return TRUE;
}

/* Callback for status menu positioning */
static void cb_menu_position (GtkMenu *menu, gint *x, gint *y, gboolean* push_in, gpointer user_data)
{
	GtkWidget *widget, *button;
	gint screen_width, screen_height;
	gint window_width, window_height;

	GtkRequisition requisition;

	widget = GTK_WIDGET (menu);
	button = GTK_WIDGET (user_data);

	gtk_widget_size_request (widget, &requisition);

	gdk_window_get_origin (button->window, x, y); 
	
	*x += button->allocation.x;
	*y += button->allocation.y;
	
	gdk_drawable_get_size(button->window, &window_width, &window_height);
	*y -= requisition.height;
	
	screen_width = gdk_screen_width ();
	screen_height = gdk_screen_height ();

	if (*x + requisition.width > screen_width) {
	  *x -= *x + requisition.width - screen_width;
	}
}

int popup_status_menu( GtkWidget *widget, gpointer data )
{
	GtkWidget *status_menu;
	GtkWidget *item, *image;

#ifdef TRACE_FUNCTION
	g_print( "popup_status_menu\n" );
#endif
	status_menu = gtk_menu_new();

	item = gtk_image_menu_item_new_with_label (_("Online"));
	image = gtk_image_new_from_pixbuf (icon_online_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_online),
	                  NULL);

	item = gtk_image_menu_item_new_with_label (_("Away"));
	image = gtk_image_new_from_pixbuf (icon_away_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_away),
	                  NULL);

	item = gtk_image_menu_item_new_with_label (_("Not Available"));
	image = gtk_image_new_from_pixbuf (icon_na_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_na),
	                  NULL);

	item = gtk_image_menu_item_new_with_label (_("Free for Chat"));
	image = gtk_image_new_from_pixbuf (icon_ffc_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_ffc),
	                  NULL);

	item = gtk_image_menu_item_new_with_label (_("Occupied"));
	image = gtk_image_new_from_pixbuf (icon_occ_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_occ),
	                  NULL);

	item = gtk_image_menu_item_new_with_label (_("Do Not Disturb"));
	image = gtk_image_new_from_pixbuf (icon_dnd_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_dnd),
	                  NULL);

	item = gtk_image_menu_item_new_with_label (_("Invisible"));
	image = gtk_image_new_from_pixbuf (icon_inv_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_invisible),
	                  NULL);

	item = gtk_menu_item_new ();
	gtk_widget_set_sensitive (item, FALSE);
	gtk_container_add (GTK_CONTAINER (status_menu), item);

	item = gtk_image_menu_item_new_with_label (_("Offline"));
	image = gtk_image_new_from_pixbuf (icon_offline_pixbuf);
	gtk_container_add (GTK_CONTAINER (status_menu), item);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect (G_OBJECT (item), "activate",
	                  G_CALLBACK (icq_set_status_offline),
	                  NULL);

	gtk_widget_show_all (status_menu);

	gtk_menu_popup( GTK_MENU( status_menu ), NULL, NULL,
	                cb_menu_position, widget, 0,
			gtk_get_current_event_time());

	return TRUE;
}

/*** Local functions ***/

void icq_filetransfer_response (GtkDialog *dialog, int response, gpointer xfer)
{
#ifdef TRACE_FUNCTION
	g_print ("icq_filetransfer_response\n");
#endif

	/* FIXME: WE NEED TO REFUSE IN A V7 WAY TOO */
	if (response == GTK_RESPONSE_YES)
		v7_acceptfile (xfer);
	else if (response == GTK_RESPONSE_NO)
		TCPRefuseFile (xfer);

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

void icq_auth_request_response (GtkDialog *dialog, int response, gpointer data)
{
#ifdef TRACE_FUNCTION
	g_print( "icq_auth_request_response\n" );
#endif

	if (!is_connected(GTK_WINDOW(dialog), _("You can not answer a authorization request while disconnected.")))
		return;
	if (response == GTK_RESPONSE_YES)
		v7_grant_auth(mainconnection, data);
	else if (response == GTK_RESPONSE_NO)
		v7_deny_auth(mainconnection, data, "Authorization denied");

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

void icq_send_url_response (GtkDialog *dialog, int response, struct URLInfo *urlinfo)
{

#ifdef TRACE_FUNCTION
	g_print ("icq_send_url_response\n");
#endif

	if (response == ICQ_RESPONSE_SEND)
	{
		char *url;
		char *desc;
		char *text;

		g_assert(urlinfo != NULL);
		if (!is_connected(GTK_WINDOW(dialog), _("You can not send an URL while disconnected.")))
			return;

		url = gtk_editable_get_chars (GTK_EDITABLE (urlinfo->url), 0, -1);
		desc = gtk_editable_get_chars (GTK_EDITABLE (urlinfo->desc), 0, -1);

		/* Add statement to personal history file */
		text = g_strdup_printf ("URL: %s \nDescription: %s", url, desc);
		history_add_outgoing (urlinfo->uin, text);
		g_free (text);

		v7_sendurl (mainconnection, urlinfo->uin, url, desc);
		
		g_free (desc);
		g_free (url);
	}
	
	gtk_widget_destroy (GTK_WIDGET (dialog));
	g_free(urlinfo);
}

void request_file_action ( GSList *completed_files )
{
    GtkWidget *dialog, *dialog_vbox, *main_vbox, *label;
    GtkWidget *label_name, *hseparator, *button_file, *button_del; /* , *button_dir; */
    GtkWidget *hbox;
    gchar *full_filename = NULL;

    GSList *file;

    dialog = gtk_dialog_new_with_buttons (_("Received Files"),
                                          GTK_WINDOW (MainData->window),
                                          GTK_DIALOG_DESTROY_WITH_PARENT,
                                          GTK_STOCK_OK, GTK_RESPONSE_OK,
                                          NULL);

    gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

    dialog_vbox = GTK_DIALOG( dialog )->vbox;

    main_vbox = gtk_vbox_new( FALSE, 0 );
    gtk_box_pack_start ( GTK_BOX( dialog_vbox ), main_vbox, TRUE, TRUE, 0 );

    label = gtk_label_new( _("The following files have been successfully retrieved.") );
    gtk_box_pack_start( GTK_BOX( main_vbox ), label, FALSE, TRUE, 0 );
    gtk_label_set_line_wrap( GTK_LABEL( label ), TRUE );

    hseparator = gtk_hseparator_new();
    gtk_box_pack_start( GTK_BOX( main_vbox ), hseparator, FALSE, TRUE, 0 );

    for ( file = completed_files; file; file = file->next )
    {
        hbox = gtk_hbox_new( FALSE, 3 );
        gtk_box_pack_start( GTK_BOX( main_vbox ), hbox, TRUE, TRUE, 0 );
        gtk_container_set_border_width( GTK_CONTAINER( hbox ), 3 );

        label_name = gtk_label_new( ((struct xfer_file*)file->data)->short_filename );

        gtk_box_pack_start( GTK_BOX( hbox ), label_name, TRUE, TRUE, 0 );
        gtk_misc_set_alignment( GTK_MISC( label_name ), 0, 0.5 );

        full_filename = g_strdup( ((struct xfer_file*)file->data)->full_filename );
        button_file = gtk_button_new_with_label( _("Open File") );
        gtk_box_pack_start( GTK_BOX( hbox ), button_file, FALSE, FALSE, 0 );
        g_object_set_data( G_OBJECT( button_file ), "filename", full_filename );

	button_del = gtk_button_new_with_label( _("Delete") );
	gtk_box_pack_start( GTK_BOX( hbox ), button_del, FALSE, FALSE, 0 );
	g_object_set_data( G_OBJECT( button_del ), "filename", full_filename );
	g_object_set_data( G_OBJECT( button_del ), "hbox", hbox );
	g_object_set_data( G_OBJECT( button_del ), "button_file", button_file );

	/* FIXME: open_file crashes */
	gtk_widget_set_sensitive(button_file, FALSE);


        /*        button_dir = gtk_button_new_with_label( _("Open Dir") );
        gtk_box_pack_start( GTK_BOX( hbox ), button_dir, FALSE, FALSE, 0 );
        g_object_set_data( G_OBJECT( button_file ), "filename", full_filename ); */

        g_signal_connect( G_OBJECT( button_file ), "clicked",
                          G_CALLBACK( open_file ),
                          NULL );

        g_signal_connect( G_OBJECT( button_del ), "clicked",
                          G_CALLBACK( del_file ),
                          NULL );

        /*        g_signal_connect( G_OBJECT( button_dir ), "clicked",
                            G_CALLBACK( open_dir ),
                            NULL ); */

    }

    g_signal_connect (G_OBJECT (dialog), "response",
                      G_CALLBACK (gtk_widget_destroy), NULL);

    g_free(full_filename);
    gtk_widget_show_all( dialog );
}

static void open_file( GtkWidget *widget, gpointer data )
{
    const gchar *filename = NULL, *mime_type = NULL;
    gchar *prgm, *prgm_orig, *cmd = NULL, *argv[4];
    int loc;

    filename = g_object_get_data( G_OBJECT( widget ), "filename" );
    mime_type = gnome_vfs_get_mime_type( filename );
    /* GNOME2 FIXME : maybe we can do much better with the info we get in
       the GnomeVFSMimeApplication struct */
    prgm = g_strdup(gnome_vfs_mime_get_default_application(mime_type)->command);
    prgm_orig = prgm;

    if (prgm)
    {
        loc = (long int)strstr(prgm, "%f") - (long int)prgm;
        cmd = g_strdup_printf("%s\"%s\"", g_strndup(prgm, loc), filename);
        prgm += loc + 2;
        cmd = g_strdup_printf("%s%s", cmd, prgm);

        argv[0] = "/bin/sh";
        argv[1] = "-c";
        argv[2] = cmd;
        argv[3] = NULL;

        gnome_execute_async( NULL, 4, argv );
        g_free( prgm_orig );
        g_free( cmd );
    }
}

static void del_file( GtkWidget *widget, gpointer data )
{
     const gchar *filename = NULL;
     GtkWidget *hbox, *button_file, *label;

     filename = g_object_get_data( G_OBJECT( widget ), "filename" );
     hbox = g_object_get_data( G_OBJECT( widget ), "hbox" );
     button_file = g_object_get_data( G_OBJECT( widget ), "button_file" );

     if (unlink(filename) == 0) {

	  gtk_widget_hide(hbox);

	  gtk_widget_destroy(widget);
	  gtk_widget_destroy(button_file);

	  label = gtk_label_new(_("DELETED"));

	  gtk_box_pack_start( GTK_BOX( hbox ), label, FALSE, FALSE, 0 );

	  gtk_widget_show_all(hbox);

     }

}

/* static void open_dir( GtkWidget *widget, gpointer data )
{
    g_print("Should open the dir...\n");
} */

/* Returns foreground color used for specified status. Do not free the return
 * value */
gchar *get_foreground_for_status( gint status)
{
	GdkColor *color;
	/* static because it is returned */
	static gchar str[8];

	if( status == STATUS_OFFLINE ) {
		color = preferences_get_color (PREFS_ICQ_COLOR_OFFLINE);
	} else {

		switch ( status & 0xffff )
		{
		case STATUS_ONLINE:
			color = preferences_get_color (PREFS_ICQ_COLOR_ONLINE);
			break;
		case STATUS_FREE_CHAT:
			color = preferences_get_color (PREFS_ICQ_COLOR_FREEFORCHAT);
			break;
		case STATUS_AWAY:
			color = preferences_get_color (PREFS_ICQ_COLOR_AWAY);
			break;
		case STATUS_NA:
			color = preferences_get_color (PREFS_ICQ_COLOR_NA);
			break;
		case STATUS_OCCUPIED:
			color = preferences_get_color (PREFS_ICQ_COLOR_OCCUPIED);
			break;
		case STATUS_DND:
		case STATUS_DND_MAC:
			color = preferences_get_color (PREFS_ICQ_COLOR_DND);
			break;
		case STATUS_INVISIBLE:
			color = preferences_get_color (PREFS_ICQ_COLOR_INVISIBLE);
			break;
		default:
			color = preferences_get_color (PREFS_ICQ_COLOR_OFFLINE);
			g_warning("Unknown status: 0x%x\n", status & 0xffff);
		}
	}

	snprintf(str, sizeof(str), "#%02hhX%02hhX%02hhX",
                 color->red, color->green, color->blue);
        g_free (color);

	return str;
}

gboolean is_connected(GtkWindow *parent_window, const gchar *message)
{
	if (!mainconnection || Current_Status == STATUS_OFFLINE) {
		GtkWidget *dialog;

		if (message == NULL)
			return FALSE;

		dialog = gtk_message_dialog_new (parent_window,
						 GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_OK,
						 message);
		g_signal_connect (G_OBJECT (dialog), "response",
				  G_CALLBACK (gtk_widget_destroy), NULL);

		gtk_window_set_title (GTK_WINDOW (dialog), _("GnomeICU Error"));
		gtk_widget_show_all (dialog);
		return FALSE;
	}
	return TRUE;
}



