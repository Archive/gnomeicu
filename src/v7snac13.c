/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/*
 * Functions for SNAC family 0x13, server side contacts list management
 * first created by Patrick Sung (2002)
 */


#include "common.h"
#include "auth.h"
#include "cl_migrate.h"
#include "gnomecfg.h"
#include "gnomeicu.h"
#include "groups.h"
#include "response.h"
#include "showlist.h"
#include "util.h"
#include "v7recv.h"
#include "v7send.h"
#include "v7snac13.h"

#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>

/*** enum and structs ***/

typedef enum {
  SNAC13_NONE,
  SNAC13_ADD_USER_NO_AUTH,
  SNAC13_ADD_USER_AUTH,
  SNAC13_REMOVE_USER,
  SNAC13_UPDATE_GROUP,
  SNAC13_ADD_VISIBLE,
  SNAC13_REMOVE_VISIBLE,
  SNAC13_ADD_INVISIBLE,
  SNAC13_REMOVE_INVISIBLE,
  SNAC13_ADD_IGNORE,
  SNAC13_REMOVE_IGNORE
} Snac13State;

/* structure holding infos of the current state (for snac family 0x13 only)
 *
 * uins are added without asking for auth for the first time
 * if fails gnomeicu will try to add again but with auth req
 * This struct holds the data that is needed in case the first try fails.
 */
struct _snac13_state {
  Snac13State state;

  gboolean change_grp;

  UIN_T uin;
  gchar *nick;

  guint gid, uid;
  guint new_gid; /* for change group */
  gboolean with_grant; /* whether send with a auth grant when adding new user */
};

/*** variables ***/

gboolean contacts_resync = FALSE; /* whether resync contacts list when startup */
static struct _snac13_state snac13_state = {
  state: SNAC13_NONE,
  change_grp: FALSE,
  uin: NULL,
  nick: NULL
};

static gboolean contact_no_gid = FALSE;
static gboolean contact_list_inconsistent = FALSE;

/*** local functions ***/

gboolean v7_got_contact_list (V7Connection *conn);

static void snac13_construct_nickname (TLVstack *t, UIN_T uin, guint16 gid,
                                       guint16 uid, const gchar *nick,
                                       gboolean needauth);
static void snac13_construct_id_ingroup (TLVstack *t, WORD gid);
static void snac13_construct_visible (TLVstack *t, UIN_T uin, guint16 uid);
static void snac13_construct_invisible (TLVstack *t, UIN_T uin, guint16 uid);
static void snac13_construct_ignore (TLVstack *t, UIN_T uin, guint16 uid);
static void snac13_construct_status (TLVstack *t, gboolean status);
static void snac13_construct_authreq (TLVstack *t, UIN_T uin, const gchar *msg);

static void snac13_send_add_to_list (V7Connection *conn, const TLVstack *tlvs);
static void snac13_send_update_list (V7Connection *conn, const TLVstack *tlvs);
static void snac13_send_remove_from_list (V7Connection *conn, const TLVstack *tlvs);
static void snac13_check_contacts_list_sanity (V7Connection *conn);
static gboolean v7_send_contacts_list (V7Connection *conn, TLVstack *t, gboolean force);
static void snac13_fix_inconsistent_list (V7Connection *conn);
static void snac13_fix_wrong_gid_contacts (V7Connection *conn);

/*
 * sends 0x13,0x02 and 0x13,0x04/0x13,0x05
 * request server side contacts list
 */
void
v7_request_contacts_list (V7Connection *conn)
{
  guchar stamp[6];

  /* request rights (0x13, 0x02) */
  snac_send (conn, NULL, 0, FAMILY_13_SAVED_LIST,
             F13_CLIENT_REQ_RIGHTS, NULL, ANY_ID);

  if (contacts_resync) {
    list_time_stamp = 0;
    record_cnt = 0;
  }

  /* check pref setting, if server list pref not set, check from server*/
  if (!srvlist_exist) {
    snac_send (conn, NULL, 0, FAMILY_13_SAVED_LIST, 
               F13_CLIENT_QUERY_SAVED_LIST, NULL, ANY_ID);
  } else {
    /* send request */
    DW_2_CharsBE (stamp, list_time_stamp);
    Word_2_CharsBE (&stamp[4], record_cnt);

    snac_send (conn, stamp, 6, FAMILY_13_SAVED_LIST, 
               F13_CLIENT_REQ_SAVED_LIST, NULL, ANY_ID);
  }
} /* end v7_request_contacts_list() */


/* read snac 0x13, 0x06 content
 * replace local contact list with the received one */
void
v7_read_contacts_list (Snac *snac, V7Connection *conn)
{
  static gint total_record = 0; /* total record count in user CL */
  static gboolean need_reset = TRUE;
  static gint group_count = 0, /* number of groups need to read */
              uin_count = 0; /* number of uin need to read for current group */
  static gint group_seen = 0, uin_seen = 0;
  static gboolean done_reading_list = FALSE;
  static guint timer_handle = 0;
  gchar *xml_str = NULL; /* used to save the BWS string on each record */
  gint rec_count = 0; /* number of contact list record in current snac */
  gint i;
  gint len_str, gid, uid;
  TLV *tlv = NULL;
  guint32 timestamp;
  GSList *contact;
  gchar *tmp;
  TLV *tlv2 = NULL;

  Current_Status = STATUS_ONLINE;

  /* reset flags and counters so we can receive contact list again */
  if (done_reading_list == TRUE) {
    group_count = 0;
    uin_count = 0;
    group_seen = 0;
    uin_seen = 0;
    done_reading_list = FALSE;
    need_reset = TRUE;
  }

  v7_buffer_skip(snac->buf, 2, out_err);

  rec_count = v7_buffer_get_c(snac->buf, out_err);

  total_record += rec_count;

  /* reset all because we are reading them anyway */
  contact = Contacts;
  while (contact && need_reset) {
    kontakt->vis_list = FALSE;
    kontakt->invis_list = FALSE;
    kontakt->ignore_list = FALSE;
    contact = contact->next;
  }
  need_reset = FALSE;

  for (i = 0; i < rec_count; i++) {
    /* read the BWS string (Group name, UIN for icq, or AIM name) on each CL
       record */
    len_str = v7_buffer_get_w_be(snac->buf, out_err);
    if (len_str != 0) {
      tmp = v7_buffer_get(snac->buf, len_str);
      if (!tmp)
	goto out_err;
      xml_str = g_markup_escape_text (tmp, len_str);
      tmp = NULL;
      if (!xml_str) {
        g_warning ("%s %s\n", "v7snac13.c: unable to allocate space for nickname", "(xml_str)");
        break;
      }
    }

    /* read group-id and uin-id */
    gid = v7_buffer_get_w_be(snac->buf, out_err);
    uid = v7_buffer_get_w_be(snac->buf, out_err);

    /* read the TLV, find out the type */
    tlv = read_tlv (snac->buf, out_err);

    switch (tlv->type) {

    case 0:  /* TLV 0: nickname, for each user */
      if (gid == 0) {
        /* the contact does not belong to any group, causing problems to
         * many other clients.
         */
        contact_no_gid = TRUE; /* set it so we can fix it later */
        /*printf ("gid is 0. uin: %s\n", rec_str);*/
      }

      uin_seen++;
      /* g_print ("+++uin_seen: %d\n", uin_seen); */

      /* create a user (if not exists) without specifying nickname */
      if ( (contact = Find_User (xml_str)) == NULL ) {
        contact = Add_User (xml_str, NULL, TRUE);
      }

      while (tlv2 = read_tlv (tlv->buf, over)) {
        switch (tlv2->type) {
        case 0x131: /* nickname */
          g_free(kontakt->nick);
	  kontakt->nick = v7_buffer_get_string(tlv2->buf, tlv2->len, out_err);
          break;
        case 0x66: /* with auth */
          kontakt->wait_auth = TRUE;
          break;
        case 0x13a: /* unknown tlv type, icq added after 2003a (jan 2004)*/
        case 0x137: /* Users email, added by icqlite */
	case 0x144: /* unknown.. length is 1 .. content is 0x31 */
        case 0x145: /* a message timestamp from icqlite */
	case 0x6d: /* Unknown tlv.. comes from Mark (Mac ICQ? Gaim? Other) */
          break;
	case 0x15c: /* Unknown, but related to the ICQ-MDIR stuff */
	case 0x15d: /*  same as previous  */
	  break;
        default:
          g_warning ("Unknown tlv type (0x%x) inside type 0", tlv2->type);
	  packet_print_v7_snac(snac);
          break;
        }

        free_tlv (tlv2);
	tlv2 = NULL;
      }
    over:

      kontakt->uid = uid;
      kontakt->gid = gid;

      gnomeicu_tree_user_add (kontakt);

      break;
    case 1:  /* contains id's (group or uin) */
      /* group id is not zero, contains uin id */
      if (gid) {
        groups_add (xml_str, gid);
        group_seen++;
        /* g_print ("***group_seen: %d\n", group_seen); */
      }

      /* no uin-id in this group, do nothing */
      if (tlv->len == 0) {
        uin_seen = 0;
        uin_count = 0;
        break;
      }

      tlv2 = read_tlv (tlv->buf, out_err);
      if (tlv2->type != 0xC8) {
        g_warning ("expecting TLV(C8): got TLV(0x%x).",tlv2->type);
      }
      if (gid) {
        uin_seen = 0; /* reset it because we are reading another group now */
        uin_count = tlv2->len / 2;
        /* g_print ("uin_count: %d\n", uin_count); */
      } else {
        group_count = tlv2->len / 2;
        /* g_print ("group_count: %d\n",group_count); */
      }
      free_tlv (tlv2);
      tlv2 = NULL;
      break;
    case 2:  /* zero length; vis to uin */
      if ( (contact = Find_User (xml_str)) == NULL ) {
        contact = Add_User (xml_str, NULL, TRUE);
      }
      kontakt->vislist_uid = uid;
      kontakt->vis_list = TRUE;
      break;
    case 3:  /* zero length; inv to uin */
      if ( (contact = Find_User (xml_str)) == NULL ) {
        contact = Add_User (xml_str, NULL, TRUE);
      }
      kontakt->invlist_uid = uid;
      kontakt->invis_list = TRUE;
      break;
    case 4:  /* last online status (invis or not invis) */
      tlv2 = read_tlv (tlv->buf, out_err);
      if (tlv2->type != 0xCA) {
        g_warning ("expecting TLV(CA) inside TLV(4): got TLV(0x%x).",tlv2->type);
      } else {
        status_uid = uid;
        if (v7_buffer_get_c(tlv2->buf, out_err) == 0x03) {
          if (preset_status != STATUS_INVISIBLE)
            Current_Status = STATUS_INVISIBLE;
        } else
          Current_Status = preset_status;
      }
      free_tlv (tlv2);
      tlv2 = NULL;
      break;
    case 9:  /* unknown meaning, ignore */
      break;
    case 0xE:  /* zero length; ignore to uin */
      if ( (contact = Find_User (xml_str)) == NULL ) {
        contact = Add_User (xml_str, NULL, TRUE);
      }
      kontakt->ignorelist_uid = uid;
      kontakt->ignore_list = TRUE;
      break;
    case 0xF:  /* store LastUpdateDate, ignore */
      break;
    case 0x11: /* found when user used lite.icq.com before server list exists */
      break;
    case 0x13:  /* CL first import time, not really useful to us, ignore */
      break;
    case 0x14:  /* Own  icon information  its an avatar ID stored as text */
      break;
    case 0x15:  /* Recent buddy info */
      break;
    case 0x19:  /* Unknown ? */
      /* Contains a 0x006d TLV which a 64bit number ? */
      /* neither gaim nor miranda know about it as of
       * feb 19, 2006 */
      break;
    case 0x1d: /* Unknown emnpty */
      break;
    case 0x20: /* Uknown is also 0x20 bytes long and 
		* is associated with ICQ-MDIR ?? */
      break;
    default:
      g_warning ("Unknown TLV from server contact list: type 0x%x. It also "
                 "contains this string \"%s\".\n\n"
                 "Please send the following packet dump to \n"
		 "http://bugzilla.gnome.org/enter_bug.cgi?product=GnomeICU \n"
                 "for debugging, thanks.\n\n", tlv->type, xml_str);
      packet_print_v7_snac (snac);
      break;
    }

    g_free (xml_str);
    xml_str = NULL;
    free_tlv (tlv);
    tlv = NULL;
  } /* end for(rec_count) */

  /* read the 4 bytes timestamp at the end of the packet */
  timestamp = v7_buffer_get_dw_be(snac->buf, out_err);

  if ( (group_seen == group_count && uin_seen == uin_count) ||
       timestamp != 0 ) {
    done_reading_list = TRUE;

    /* check if there is existing timer, if so, cancel it */
    if (timer_handle) {
      gtk_timeout_remove (timer_handle);
      timer_handle = 0;
    }
  } else {
    /* set a timer so inconsistent list can still go thru, but we will fix it */
    if (timer_handle == 0)
      timer_handle = gtk_timeout_add (3000, (GtkFunction)v7_got_contact_list ,conn);
  }

  /* set a flag so we can fix the inconsistent list */
  if (group_seen == group_count && uin_seen == uin_count)
    contact_list_inconsistent = FALSE;
  else
    contact_list_inconsistent = TRUE;

  list_time_stamp = timestamp;
  record_cnt = total_record;

  /* no record found, and previous request is 'query contacts list'
     therefore we migrate */
  if (record_cnt == 0 && !srvlist_exist) {
    /* check if there is existing timer, if so, cancel it */
    if (timer_handle) {
      gtk_timeout_remove (timer_handle);
      timer_handle = 0;
    }

    snac_send (conn, NULL, 0, FAMILY_13_SAVED_LIST, 
               F13_CLIENT_RDY_TO_USE_LIST, NULL, ANY_ID);
    v7_set_user_info (conn);
    v7_add_icbm (conn);
    v7_setstatus2 (conn, Current_Status);
    v7_client_ready (conn);

    /* normal migration */
    g_idle_add (migration_dialog, (gpointer)FALSE);
    return;
  }

  if (done_reading_list)
    v7_got_contact_list (conn);

  return;

 out_err:
  g_warning("Error parsing packet\n");

  g_free(tlv);
  g_free(tlv2);

}

gboolean
v7_got_contact_list (V7Connection *conn)
{
  /* finished all reading */
  /* signal ready to use the contacts list */
  snac_send (conn, NULL, 0, FAMILY_13_SAVED_LIST, 
             F13_CLIENT_RDY_TO_USE_LIST, NULL, ANY_ID);

  v7_set_user_info (conn);
  v7_add_icbm (conn);
  v7_setstatus2 (conn, Current_Status);
  v7_client_ready (conn);

  srvlist_exist = TRUE;
  Save_RC ();

  /* check sanity of contacts list after reading it */
  snac13_check_contacts_list_sanity (conn);

  return FALSE;
}

/*
 * send snac 0x13, 0x11
 * begin contacts list change
 */
void
v7_begin_CL_edit (V7Connection *conn, gboolean migrate)
{
  gchar data[4] = "\x00\x01\x00\x00";

  if (migrate)
    snac_send (conn, data, 4, FAMILY_13_SAVED_LIST,
               F13_BOTH_START_MOD_LIST, NULL, ANY_ID);
  else
    snac_send (conn, NULL, 0, FAMILY_13_SAVED_LIST,
               F13_BOTH_START_MOD_LIST, NULL, ANY_ID);

}

/*
 * send snac 0x13, 0x12
 * end contacts list change
 */
void
v7_end_CL_edit (V7Connection *conn)
{
  snac_send (conn, NULL, 0, FAMILY_13_SAVED_LIST,
             F13_BOTH_END_MOD_LIST, NULL, ANY_ID);
}


/* send the contact list (0x13, 0x08)
 *  force - force sending contact, instead of wait until enough records
 * return:
 *  TRUE - if snac is sent
 *  FALSE - snac is not sent
 */
gboolean
v7_send_contacts_list (V7Connection *conn, TLVstack *t, gboolean force)
{
  static gint rec_cnt = 0;

#ifdef TRACE_FUNCTION
  g_print("v7_send_contacts_list\n");
#endif

  rec_cnt++;

  if (force || rec_cnt >= 0x50) {
    rec_cnt = 0;

    if (t->len > 0)
      /* send the tlv stack */
      snac13_send_add_to_list (conn, t);
    return TRUE;
  }

  return FALSE;
}


/* construct the contacts list */
void
v7_migrate_contacts_list (V7Connection *conn)
{
  GSList *contact;
  guint id, main_gid = 0;
  gint ignore_cnt = 0;
  gint vis_cnt = 0;
  gint inv_cnt = 0;
  TLVstack *tlvs = NULL;

#ifdef TRACE_FUNCTION
  g_print("v7_migrate_contacts_list\n");
#endif

  main_gid = groups_find_gid_by_name ("General");

  if (!main_gid) {
    main_gid = groups_gen_gid ();
    groups_add ("General", main_gid);
  }

  tlvs = new_tlvstack (NULL, 0);

  contact = Contacts;
  /* add contacts first */
  while (contact) {
    /* skip ignore list */
    if (kontakt->ignore_list) {
      contact = contact->next;
      ignore_cnt++;
      continue;
    }

    /* count visible and invisible user */
    if (kontakt->vis_list) {
      vis_cnt++;
    } else if (kontakt->invis_list) {
      inv_cnt++;
    }

    id = contact_gen_uid ();

    snac13_construct_nickname (tlvs, kontakt->uin, main_gid, id, kontakt->nick,
                               FALSE);

    kontakt->gid = main_gid;
    kontakt->uid = id;

    contact = contact->next;

    if (v7_send_contacts_list (conn, tlvs, FALSE)) {
      free_tlvstack (tlvs);
      tlvs = new_tlvstack (NULL, 0);
    }

  }

  /* send special users */
  contact = Contacts;
  while (contact && (ignore_cnt != 0 || vis_cnt != 0 || inv_cnt != 0)) {
    /* only ignore, vis, invis user */
    if (kontakt->ignore_list) {
      snac13_construct_ignore (tlvs, kontakt->uin, id = contact_gen_uid ());
      kontakt->ignorelist_uid = id;
      ignore_cnt--;
    } else if (kontakt->vis_list) {
      snac13_construct_visible (tlvs, kontakt->uin, id = contact_gen_uid ());
      kontakt->vislist_uid = id;
      vis_cnt--;
    } else if (kontakt->invis_list) {
      snac13_construct_invisible (tlvs, kontakt->uin, id =contact_gen_uid ());
      kontakt->invlist_uid = id;
      inv_cnt--;
    }

    contact = contact->next;

    if (v7_send_contacts_list (conn, tlvs, FALSE)) {
      free_tlvstack (tlvs);
      tlvs = new_tlvstack (NULL, 0);
    }
  }

  v7_send_contacts_list (conn, tlvs, TRUE);  /* force a send */
  free_tlvstack (tlvs);
  tlvs = new_tlvstack (NULL, 0);


  /* send the default group - "General" */
  snac13_construct_id_ingroup (tlvs, main_gid);
  v7_send_contacts_list (conn, tlvs, TRUE);
  free_tlvstack (tlvs);
  tlvs = new_tlvstack (NULL, 0);

  /* send master group */
  snac13_construct_id_ingroup (tlvs, 0);
  v7_send_contacts_list (conn, tlvs, TRUE);
  free_tlvstack (tlvs);
  tlvs = new_tlvstack (NULL, 0);

  /* send current status */
  snac13_construct_status (tlvs, TRUE);
  v7_send_contacts_list (conn, tlvs, TRUE);
  free_tlvstack (tlvs);
  tlvs = new_tlvstack (NULL, 0);

  free_tlvstack (tlvs);

} /* end v7_migrate_contacts_list () */


/* send snac 0x13,0x08
 * add _one_ uin to the server contacts list
 *  - authtlv: TRUE add request authorization TLV(0x66)
 *             FALSE don't add the TLV(0x66)
 * snac13_state.state needs to be in either of these states:
 *   SNAC13_ADD_USER_NO_AUTH
 *   SNAC13_ADD_USER_AUTH
 *
 * snac13_state.uid will be updated with the newly generated uid
 */
guint
v7_addto_contacts_list (V7Connection *conn, UIN_T uin, const gchar *nick,
                        guint gid, gboolean needauth)
{
  guint uid;
  TLVstack *tlvs;

#ifdef TRACE_FUNCTION
  g_print("v7_addto_contacts_list\n");
#endif

  if (snac13_state.state != SNAC13_ADD_USER_AUTH &&
      snac13_state.state != SNAC13_ADD_USER_NO_AUTH)
    return 0;
  
  if (!snac13_state.change_grp) {
    snac13_state.change_grp = FALSE;
    v7_begin_CL_edit (conn, FALSE);
  }

  /* generate a new uid for this new user */
  uid = contact_gen_uid ();
  snac13_state.uid = uid;

  tlvs = new_tlvstack (NULL, 0);

  snac13_construct_nickname (tlvs, uin, gid, snac13_state.uid, nick, needauth);
  snac13_send_add_to_list (conn, tlvs);

  free_tlvstack (tlvs);

  return uid;
}

/*
 * send snac 0x13,0x14
 */
void
v7_send_grant_auth (V7Connection *conn, UIN_T uin)
{
  TLVstack *tlvs;
  
  tlvs = new_tlvstack (NULL, 0);
  snac13_construct_authreq (tlvs, uin, NULL);
  
  snac_send (conn, tlvs->beg, tlvs->len, FAMILY_13_SAVED_LIST,
             F13_CLIENT_AUTH_REQUEST_1, NULL, ANY_ID);

  free_tlvstack (tlvs);
}

/*
 * read snac 0x13,0x0E
 * server ack for serveral snac 0x13 request
 */
void
v7_snac13_check_srv_ack (V7Connection *conn, Snac *snac)
{
  WORD state = 0;
  BYTE smallstate;

  GSList *contact;
  Contact_Member *contactdata;
  static gchar *nick;

#ifdef TRACE_FUNCTION
  g_print("v7_snac13_check_srv_ack\n");
#endif

  g_print("Current state is %d\n", snac13_state.state);

  /* read the status */
  state = v7_buffer_get_w_be(snac->buf, out_err);
  smallstate = state & 0xFF;

  if (smallstate == 0x00) { /* no error */
    switch (snac13_state.state) {
    case SNAC13_ADD_USER_AUTH:
    case SNAC13_ADD_USER_NO_AUTH:
      /* add the user */
      if ( (contact = Find_User (snac13_state.uin)) == NULL ) {
        contactdata = g_new0 (Contact_Member, 1);
        contactdata->info = g_new0 (USER_INFO_STRUCT, 1);

        /* Only set for newly created contacts */
        contactdata->status = STATUS_OFFLINE;
      } else {
        contactdata = contact->data;
      }

      g_free(contactdata->uin);
      contactdata->uin = g_strdup (snac13_state.uin);
      g_free(contactdata->nick);
      contactdata->nick = g_strdup (snac13_state.nick);
      contactdata->inlist = TRUE;
      contactdata->tcp_seq = -1;
      contactdata->confirmed = TRUE;
      contactdata->gid = snac13_state.gid;
      contactdata->uid = snac13_state.uid;

      if (contact == NULL) /* if this is a new contact, we add them to the list*/
        Contacts = g_slist_append (Contacts, contactdata);

      /* set the wait_auth flag if we are on SNAC13_ADD_USER_AUTH state */
      if (snac13_state.state == SNAC13_ADD_USER_AUTH) {
        contactdata->wait_auth = TRUE;
      }

      /* update group list */
      v7_update_group_info (conn, snac13_state.gid);
      v7_end_CL_edit (conn);

      /* refresh the gui */

      if (contact)
	gnomeicu_tree_user_update (contactdata);
      else
	gnomeicu_tree_user_add (contactdata);

      nick = contactdata->nick;
      break;
    case SNAC13_UPDATE_GROUP:
      g_print("We have a update group success\n");
      if (snac13_state.change_grp) {
	g_print("We have a change group for %s %s\n", snac13_state.uin, snac13_state.nick);
	guint uid;
	UIN_T tmpuin;
	gchar *tmpnick;

	tmpuin = g_strdup(snac13_state.uin);
	tmpnick = g_strdup(snac13_state.nick);

        /* if we are changing group, we start the remove contact here */
        snac13_state.state = SNAC13_NONE;

	uid = v7_try_add_uin (conn, tmpuin, tmpnick, 
			      snac13_state.new_gid, FALSE);

	g_free(tmpuin);
	g_free(tmpnick);

	user_group_changed(snac13_state.uin, snac13_state.new_gid, uid);
	
      } else {
        /* just finish up, clear the state */
	v7_end_CL_edit (conn);

        snac13_state.state = SNAC13_NONE;
      }
      break;
    case SNAC13_REMOVE_USER:
      /* user already removed in the local copy */
      /* user removal successful, update group */
      v7_update_group_info (conn, snac13_state.gid);

      if (!snac13_state.change_grp) {
	v7_end_CL_edit (conn);
      }
      break;
    case SNAC13_ADD_VISIBLE:
      if ( (contact = Find_User (snac13_state.uin))) {
	contactdata = contact->data;
	contactdata->vis_list = 1;
	contactdata->vislist_uid = snac13_state.uid;
	gnomeicu_tree_user_update (contactdata);
	Save_RC();
      }
      snac13_state.state = SNAC13_NONE;
      break;
    case SNAC13_REMOVE_VISIBLE:
      if ( (contact = Find_User (snac13_state.uin))) {
	contactdata = contact->data;
	contactdata->vis_list = 0;
	contactdata->vislist_uid = 0;
	gnomeicu_tree_user_update (contactdata);
	Save_RC();
      }
      snac13_state.state = SNAC13_NONE;
      break;
    case SNAC13_ADD_INVISIBLE:
      if ( (contact = Find_User (snac13_state.uin))) {
	contactdata = contact->data;
	contactdata->invis_list = 1;
	contactdata->invlist_uid = snac13_state.uid;
	gnomeicu_tree_user_update (contactdata);
	Save_RC();
      }
      snac13_state.state = SNAC13_NONE;
      break;
    case SNAC13_REMOVE_INVISIBLE:
      if ( (contact = Find_User (snac13_state.uin))) {
	contactdata = contact->data;
	contactdata->invis_list = 0;
	contactdata->invlist_uid = 0;
	gnomeicu_tree_user_update (contactdata);
	Save_RC();
      }
      snac13_state.state = SNAC13_NONE;
      break;
    case SNAC13_ADD_IGNORE:
      if ( (contact = Find_User (snac13_state.uin))) {
	contactdata = contact->data;
	contactdata->ignore_list = 1;
	contactdata->ignorelist_uid = snac13_state.uid;
	gnomeicu_tree_user_update (contactdata);
	Save_RC();
      }
      snac13_state.state = SNAC13_NONE;
      break;
    case SNAC13_REMOVE_IGNORE:
      if ( (contact = Find_User (snac13_state.uin))) {
	contactdata = contact->data;
	contactdata->ignore_list = 0;
	contactdata->ignorelist_uid = 0;
	gnomeicu_tree_user_update (contactdata);
	Save_RC();
      }
      snac13_state.state = SNAC13_NONE;
      break;
    default:
      snac13_state.state = SNAC13_NONE;
      break;

    }
    return;
  } 

  
  /* 
   * Handled error 
   */
  switch (snac13_state.state) {
  case SNAC13_ADD_USER_NO_AUTH:
    if (smallstate == 0x0E) { /* error: requires auth */
      v7_end_CL_edit (conn);
      auth_request_msg_box (snac13_state.nick, snac13_state.uin);
      return;
    }
    break;
  case SNAC13_REMOVE_USER:
    /* Trying to change user to another group, but he wasn't in the list */
    if (snac13_state.change_grp && smallstate == 0x02) {
      guint uid;
      UIN_T tmpuin;
      gchar *tmpnick;

      //v7_end_CL_edit (conn);

      /* if we are changing group, we start the remove contact here */
      snac13_state.state = SNAC13_NONE;
      snac13_state.change_grp = FALSE;

      g_print("Remove failed, adding UIN: %s Nick: %s\n", snac13_state.uin, snac13_state.nick);

      tmpuin = g_strdup(snac13_state.uin);
      tmpnick = g_strdup(snac13_state.nick);

      uid = v7_try_add_uin (conn, tmpuin, tmpnick, 
			    snac13_state.new_gid, FALSE);

      g_free(tmpuin);
      g_free(tmpnick);

      user_group_changed(snac13_state.uin, snac13_state.new_gid, uid);

      return;
    }
    break;
      default:
    break;
  }
  
  /* 
   * Other errors fall through)
   */

  switch (snac13_state.state) {
  case SNAC13_ADD_USER_NO_AUTH:
    /* add user with no auth request unsuccessful, need to send auth req */
    g_warning ("User adding failed: uin(%s) error:0x%x", snac13_state.uin,
	       state);
    snac13_state.state = SNAC13_NONE;
    break;
  case SNAC13_ADD_USER_AUTH:
    g_warning ("User adding failed: uin(%s) error:0x%x", snac13_state.uin,
	       state);
    break;
  case SNAC13_REMOVE_USER:
    g_warning ("User removal failed: uin(%s) error:0x%x", snac13_state.uin,
	       state);
    snac13_state.state = SNAC13_NONE;
    break;
  case SNAC13_UPDATE_GROUP:
    g_warning ("Update group failed: uin(%s) error:0x%x", snac13_state.uin,
	       state);
    snac13_state.state = SNAC13_NONE;
    break;
  case SNAC13_ADD_VISIBLE:
    g_warning ("Adding to visible failed: uin(%s) error:0x%x", snac13_state.uin,
	       state);
    snac13_state.state = SNAC13_NONE;
    break;
  case SNAC13_REMOVE_VISIBLE:
    g_warning ("Removing from visible failed: uin(%s) error:0x%x", 
	       snac13_state.uin, state);
    snac13_state.state = SNAC13_NONE;
    break;
  case SNAC13_ADD_INVISIBLE:
    g_warning ("Adding to invisible failed: uin(%s) error:0x%x", 
	       snac13_state.uin, state);
    snac13_state.state = SNAC13_NONE;
    break;
  case SNAC13_REMOVE_INVISIBLE:
    g_warning ("Removing from invisible failed: uin(%s) error:0x%x", 
	       snac13_state.uin, state);
    snac13_state.state = SNAC13_NONE;
    break;
  case SNAC13_ADD_IGNORE:
    g_warning ("Adding to ignore failed: uin(%s) error:0x%x", snac13_state.uin,
	       state);
    snac13_state.state = SNAC13_NONE;
    break;
  case SNAC13_REMOVE_IGNORE:
    snac13_state.state = SNAC13_NONE;
    g_warning ("Removing from ignore failed: uin(%s) error:0x%x", 
	       snac13_state.uin, state);
    break;

  }

  return;
 out_err:
  g_warning("%s: Error parsing packet\n", __FUNCTION__);
} /* end void v7_snac13_check_srv_ack (Snac *snac) */


/*
 * send user status
 *
 * update stauts, TRUE = visible, FALSE = invisible
 */
void
v7_send_status_server (V7Connection *conn, gboolean visible)
{
  TLVstack *tlvs;

  tlvs = new_tlvstack (NULL, 0);

  snac13_construct_status (tlvs, visible);
  snac13_send_update_list (conn, tlvs);

  free_tlvstack (tlvs);
}

/*
 * send snac 0x13,0x09
 */
void
v7_update_nickname (V7Connection *conn, UIN_T uin, const gchar *nick,
                    guint gid, guint uid, gboolean wait_auth)
{
  TLVstack *tlvs;

  tlvs = new_tlvstack (NULL, 0);

  snac13_construct_nickname (tlvs, uin, gid, uid, nick, wait_auth);
  snac13_send_update_list (conn, tlvs);

  free_tlvstack (tlvs);
}

/*
 * send snac 0x13,0x09
 */
void
v7_update_group_info (V7Connection *conn, guint gid)
{
  TLVstack *tlvs;

#ifdef TRACE_FUNCTION
  g_print("v7_update_group_info\n");
#endif

  snac13_state.state = SNAC13_UPDATE_GROUP;

  tlvs = new_tlvstack (NULL, 0);

  snac13_construct_id_ingroup (tlvs, gid);
  snac13_send_update_list (conn, tlvs);

  free_tlvstack (tlvs);
}

/* helper functions to add uin */

/* try to add user without using authorization */
guint
v7_try_add_uin (V7Connection *conn, UIN_T uin, const gchar *nick, guint gid,
                gboolean grant)
{
  /* we can also send out authorization grant while adding the target user */
  if (grant) {
    v7_send_grant_auth (conn, uin);
  }

  snac13_state.state = SNAC13_ADD_USER_NO_AUTH;

  g_free (snac13_state.uin);
  snac13_state.uin = g_strdup (uin);

  g_free (snac13_state.nick);
  snac13_state.nick = g_strdup(nick);

  snac13_state.gid = gid;
  snac13_state.with_grant = grant;

  /* add new user without auth grant (i.e. no TLV(0x66) set) */
  return v7_addto_contacts_list (conn, uin, nick, gid, FALSE);
}

/* send an auth request to the user, according to the data in snac13_state
 * This function should work in conjunction with v7_try_add_uin(), will get
 * called if the adding was not sucessful (see v7_snac13_check_srv_ack(),
 * and auth.c
 */
void
v7_ask_uin_for_auth (V7Connection *conn, gchar *authmsg, UIN_T uin)
{
  TLVstack *tlvs;

  // uin = g_strdup (snac13_state.uin);

  /* send the auth message first */
  tlvs = new_tlvstack (NULL, 0);
  snac13_construct_authreq (tlvs, uin, authmsg);
  snac_send (conn, tlvs->beg, tlvs->len, FAMILY_13_SAVED_LIST,
             F13_CLIENT_AUTH_REQUEST_2, NULL, ANY_ID);
  free_tlvstack (tlvs);

  /* send the auth grant again, if the user has intended */
  if (snac13_state.with_grant)
    v7_send_grant_auth (conn, uin);

  snac13_state.state = SNAC13_ADD_USER_AUTH;
  /* now add again, but with the awaiting auth bit set (TLV(0x66) */
  if (snac13_state.nick)
    v7_addto_contacts_list (conn, uin, snac13_state.nick, snac13_state.gid, TRUE);
}

/*
 * send snac 0x13,0x0A
 */
void
v7_remove_contact (V7Connection *conn, UIN_T uin, const gchar *nick,
                   guint gid, guint uid, gboolean wait_auth)
{
  TLVstack *tlvs;

#ifdef TRACE_FUNCTION
  g_print("v7_remove_contact\n");
#endif

  v7_begin_CL_edit (conn, FALSE);

  snac13_state.state = SNAC13_REMOVE_USER;

  snac13_state.gid = gid;
  snac13_state.uid = uid;
  g_free (snac13_state.nick);
  snac13_state.nick = g_strdup (nick);
  g_free (snac13_state.uin);
  snac13_state.uin = g_strdup (uin);

  tlvs = new_tlvstack (NULL, 0);

  snac13_construct_nickname (tlvs, uin, gid, uid, snac13_state.nick, wait_auth);
  snac13_send_remove_from_list (conn, tlvs);

  free_tlvstack (tlvs);
}

/*
 * read snac 0x13,0x19
 */
void
v7_recv_auth_request (Snac *snac)
{
  UIN_T uin = NULL;
  gint str_len;
  gchar *str = NULL;

#ifdef TRACE_FUNCTION
  g_print("v7_recv_auth_request\n");
#endif

  /* read requester uin */
  uin = v7_buffer_get_uin(snac->buf, out_err);

  /* read request msg length */
  str_len = v7_buffer_get_w_be(snac->buf, out_err);

  /* read the request msg */
  str = v7_buffer_get_string(snac->buf, str_len, out_err);

  /* got all the info, now call the client auth handler */
  authorization_request(uin, str, time(NULL));

 out_err:
  g_free (str);
}

/*
 * send snac 0x13,0x1A
 */
void
v7_grant_auth_request (V7Connection *conn, UIN_T uin)
{
  gint len;
  gchar *strtmp;
  TLVstack *t = NULL;

  t = new_tlvstack (NULL, 0);

  strtmp = g_strdup_printf ("?%s", uin);
  len = strlen (strtmp) - 1;
  strtmp[0] = (gchar)len;
  add_nontlv (t, strtmp, len+1);
  g_free (strtmp);

  add_nontlv (t, "\x1\0\0\0\0", 5);

  snac_send (conn, t->beg, t->len, FAMILY_13_SAVED_LIST,
             F13_CLIENT_SEND_GRANT_AUTH, NULL, ANY_ID);

  free_tlvstack (t);
}

/*
 * user grant your auth request to add to your list (0x1B)
 */
void
v7_user_grant_auth (Snac *snac)
{
  UIN_T uin = NULL;
  int flag;
  char *reason = NULL, *r2=NULL;
  int str_len;

  /* read sender uin */
  uin = v7_buffer_get_uin(snac->buf, out_err);

  flag = v7_buffer_get_c(snac->buf, out_err);

  str_len = v7_buffer_get_w_be(snac->buf, out_err);

  if (str_len) {
    r2 = v7_buffer_get_string(snac->buf, str_len, out_err);
    if (r2) {
      reason = convert_to_utf8(r2);
      g_free(r2);
    }
  }

  auth_reply(uin, flag, reason, time(NULL));

 out_err:
  g_free (uin);
  g_free (reason);
}

/*
 *  user added you (snac 0x13,0x1C)
 */
void
v7_recv_added_you (Snac *snac)
{
  UIN_T uin = NULL;
  time_t msg_time;

  /* read sender uin */
  uin = v7_buffer_get_uin(snac->buf, out_err);

  time(&msg_time);

  user_added_you(uin, msg_time);

 out_err:
  g_free (uin);
}

/*
 * visible list, add or remove a uin
 */
void
v7_visible (V7Connection *conn, UIN_T uin, WORD luid, gboolean add)
{
  TLVstack *tlvs;

  tlvs = new_tlvstack (NULL, 0);

  snac13_construct_visible (tlvs, uin, luid);

  snac13_state.uin = uin;
  snac13_state.uid = luid;

  if (add) {
    snac13_state.state = SNAC13_ADD_VISIBLE;
    snac13_send_add_to_list (conn, tlvs);
  } else {
    snac13_state.state = SNAC13_REMOVE_VISIBLE;
    snac13_send_remove_from_list (conn, tlvs);
  }

  free_tlvstack (tlvs);
}

/*
 * invisible list, add or remove a uin
 */
void
v7_invisible (V7Connection *conn, UIN_T uin, WORD luid, gboolean add)
{
  TLVstack *tlvs;

  tlvs = new_tlvstack (NULL, 0);

  snac13_construct_invisible (tlvs, uin, luid);

  snac13_state.uin = uin;
  snac13_state.uid = luid;

  if (add) {
    snac13_state.state = SNAC13_ADD_INVISIBLE;
    snac13_send_add_to_list (conn, tlvs);
  } else {
    snac13_state.state = SNAC13_REMOVE_INVISIBLE;
   snac13_send_remove_from_list (conn, tlvs);
  }

  free_tlvstack (tlvs);
}

/*
 * ignore list, add or remove a uin
 */
void
v7_ignore (V7Connection *conn, UIN_T uin, WORD luid, gboolean add)
{
  TLVstack *tlvs;

  tlvs = new_tlvstack (NULL, 0);

  snac13_construct_ignore (tlvs, uin, luid);

  snac13_state.uin = uin;
  snac13_state.uid = luid;

  if (add) {
    snac13_state.state =  SNAC13_ADD_IGNORE;
    snac13_send_add_to_list (conn, tlvs);
  } else {
    snac13_state.state =  SNAC13_REMOVE_IGNORE;
    snac13_send_remove_from_list (conn, tlvs);
  }

  free_tlvstack (tlvs);
}

/*
 * move user around groups
 */
void
v7_user_change_group (V7Connection *conn, Contact_Member *c,
                      WORD new_gid, const gchar *new_grpname)
{
  snac13_state.change_grp = TRUE;

  snac13_state.new_gid = new_gid;

  v7_remove_contact (conn, c->uin, c->nick, c->gid, c->uid, c->wait_auth);
}

void
v7_add_new_group (V7Connection *conn, guint gid)
{
  TLVstack *tlvs;

  v7_begin_CL_edit (conn, FALSE);

  tlvs = new_tlvstack (NULL, 0);

  snac13_construct_id_ingroup (tlvs, gid);
  snac13_send_add_to_list (conn, tlvs);

  free_tlvstack (tlvs);

  /* update master group */
  tlvs = new_tlvstack (NULL, 0);

  snac13_construct_id_ingroup (tlvs, 0);
  snac13_send_update_list (conn, tlvs);

  free_tlvstack (tlvs);

  v7_end_CL_edit (conn);
}

void
v7_delete_empty_group (V7Connection *conn, guint gid)
{
  TLVstack *tlvs;

  v7_begin_CL_edit (conn, FALSE);

  tlvs = new_tlvstack (NULL, 0);

  snac13_construct_id_ingroup (tlvs, gid);
  snac13_send_remove_from_list (conn, tlvs);

  free_tlvstack (tlvs);

  /* delete the group from the group structure, then finalize the delete */
  /* MUST call v7_finalize_delete_group () after this function */
}

void
v7_finalize_delete_group (V7Connection *conn)
{
  TLVstack *tlvs;

  tlvs = new_tlvstack (NULL, 0);

  /* update the master group */
  snac13_construct_id_ingroup (tlvs, 0);
  snac13_send_update_list (conn, tlvs);

  free_tlvstack (tlvs);

  v7_end_CL_edit (conn);
}


/********************** private functions *************************/

/* helpers for composing the snac packet */

/* construct TLV(0) (type 0) packet */
/*   needauth - append 0x66 at the end, flagging the user in queustion is
 *              waiting for authorization
 */
void
snac13_construct_nickname (TLVstack *t, UIN_T uin, guint16 gid, guint16 uid,
                           const gchar *nick, gboolean needauth)
{
  TLVstack *tmptlv = NULL;

  /* uin */
  add_nontlv_bws (t, uin);
  
  /* group id, uin id */
  add_nontlv_w_be (t, gid);
  add_nontlv_w_be (t, uid);
  
  /* nickname */
  tmptlv = new_tlvstack (NULL, 0);

  add_tlv (tmptlv, 0x131, nick, strlen (nick));
  if (needauth) {
    add_tlv (tmptlv, 0x66, NULL, 0);
  }

  add_tlv (t, 0, tmptlv->beg, tmptlv->len);

  free_tlvstack (tmptlv);
}

void
snac13_construct_id_ingroup (TLVstack *t, WORD gid)
{
  TLVstack *idstlv, *ids;
  gchar *grpname;
  GSList *contact;
  GSList *ginfo;

  grpname = groups_name_by_gid(gid);
  
  /* group name */
  add_nontlv_bws (t, grpname);

  /* group id, zero uin id */
  add_nontlv_w_be (t, gid);
  add_nontlv_w_be (t, 0);

  /* add uinid's */

  ids = new_tlvstack (NULL, 0);

  if (gid == 0) { /* master group */
    for (ginfo = Groups; ginfo != NULL; ginfo = ginfo->next)
      add_nontlv_w_be (ids, ((GroupInfo *)(ginfo->data))->gid);
  } else {
    /* Look up the contact list for members of the group and add them */
    for (contact = Contacts; contact != NULL; contact = contact->next) 
      if (kontakt->gid == gid)
        add_nontlv_w_be (ids, kontakt->uid);
  }

  idstlv = new_tlvstack (NULL, 0);

  add_tlv (idstlv, 0xC8, ids->beg, ids->len);

  free_tlvstack(ids);

  add_tlv (t, 0x1, idstlv->beg, idstlv->len);

  free_tlvstack (idstlv);
}

void
snac13_construct_visible (TLVstack *t, UIN_T uin, guint16 uid)
{

  /* uin */
  add_nontlv_bws (t, uin);

  /* zero gid, uin id */
  add_nontlv_w_be (t, 0);
  add_nontlv_w_be (t, uid);

  add_tlv (t, 0x02, NULL, 0);
}

void
snac13_construct_invisible (TLVstack *t, UIN_T uin, guint16 uid)
{

  /* uin */
  add_nontlv_bws (t, uin);

  /* zero gid, uin id */
  add_nontlv_w_be (t, 0);
  add_nontlv_w_be (t, uid);

  add_tlv (t, 0x03, NULL, 0);
}

void
snac13_construct_ignore (TLVstack *t, UIN_T uin, guint16 uid)
{

  /* uin */
  add_nontlv_bws (t, uin);

  /* zero gid, uin id */
  add_nontlv_w_be (t, 0);
  add_nontlv_w_be (t, uid);

  add_tlv (t, 0x0E, NULL, 0);
}

/*
 * status =
 *   TRUE: visible
 *   FALSE: invisible
 */
void
snac13_construct_status (TLVstack *t, gboolean status)
{
  TLVstack *sts;
  gchar buf;

  status_uid = status_uid ? status_uid : contact_gen_uid ();

  add_nontlv_bws (t, NULL);

  add_nontlv_w_be (t, 0);
  add_nontlv_w_be (t, status_uid);

  sts = new_tlvstack (NULL, 0);

  buf = status ? '\x4' : '\x3';

  add_tlv (sts, 0xCA, &buf, 1);
  add_tlv (t, 0x4, sts->beg, sts->len);

  free_tlvstack (sts);
}


void
snac13_construct_authreq (TLVstack *t, UIN_T uin, const gchar *msg)
{
  gint len;
  gchar *strtmp;

  /* uin */
  strtmp = g_strdup_printf ("?%s", uin);
  len = strlen (strtmp) - 1; /* length of the string uin */
  strtmp[0] = (guint8)len;
  add_nontlv (t, strtmp, len+1);
  g_free (strtmp);

  add_nontlv_bws (t, msg);

  add_nontlv (t, "\0\0", 2);
}

void
snac13_send_add_to_list (V7Connection *conn, const TLVstack *t)
{
  snac_send (conn, t->beg, t->len, FAMILY_13_SAVED_LIST,
             F13_BOTH_ADD_TO_LIST, NULL, ANY_ID);
}

void
snac13_send_update_list (V7Connection *conn, const TLVstack *t)
{
  snac_send (conn, t->beg, t->len, FAMILY_13_SAVED_LIST,
             F13_BOTH_UPDATE_LIST, NULL, ANY_ID);
}

void
snac13_send_remove_from_list (V7Connection *conn, const TLVstack *t)
{
  snac_send (conn, t->beg, t->len, FAMILY_13_SAVED_LIST,
             F13_BOTH_REMOVE_FROM_LIST, NULL, ANY_ID);
}

void
snac13_check_contacts_list_sanity (V7Connection *conn)
{
  if (contact_no_gid) {
    snac13_fix_wrong_gid_contacts (conn);
  }

  if (contact_list_inconsistent) {
    snac13_fix_inconsistent_list (conn);
  }

  /* check status_uid record, if 0, that means we need to create one */
  if (status_uid == 0) {
    TLVstack *tlvs;

    tlvs = new_tlvstack (NULL, 0);
    snac13_construct_status (tlvs, TRUE);
    snac13_send_add_to_list (conn, tlvs);
    free_tlvstack (tlvs);
  }

  sane_cl = TRUE;
}

void
snac13_fix_inconsistent_list (V7Connection *conn)
{
  GSList *ginfo;
  TLVstack *tlvs;

  tlvs = new_tlvstack (NULL, 0);

  /* update each group */
  for (ginfo = Groups; ginfo != NULL; ginfo = ginfo->next) {
    tlvs = new_tlvstack (NULL, 0);
    snac13_construct_id_ingroup (tlvs, ((GroupInfo *)(ginfo->data))->gid);
    snac13_send_update_list (conn, tlvs);
    free_tlvstack (tlvs);
  }

  /* master group */
  tlvs = new_tlvstack (NULL, 0);
  snac13_construct_id_ingroup (tlvs, 0);
  snac13_send_update_list (conn, tlvs);
  free_tlvstack (tlvs);
}

void
snac13_fix_wrong_gid_contacts (V7Connection *conn)
{
  GSList *contact;
  struct _zero_gid_contact {
    UIN_T uin;
    guint16 gid, uid;
    gchar *nick;
    gboolean wait_auth;
  };
  GSList *aList = NULL, *temp_aList = NULL;
  struct _zero_gid_contact *aEle;
  TLVstack *tlvs;
  guint16 gid;

  tlvs = new_tlvstack (NULL, 0);

  /* find a suitable gid to use */
  if ( (gid = groups_find_gid_by_name ("General")) == 0) {
    /* no "General" group, find any existing group and use it */
    GSList *group = Groups;
    if (group)
      gid = ((GroupInfo *)group->data)->gid;
  }

  if (gid == 0) {
    return; /* for now */
  }
    
  printf ("using gid [%d]; group name: [%s]\n", gid, groups_name_by_gid (gid));
  /* add user with correct gid */
  contact = Contacts;
  while (contact) {
    if (groups_name_by_gid (kontakt->gid)) {
      /* gid is pointing to a valid group */
      contact = contact->next;
      continue;
    }

    /* save the old data */
    aEle = g_new (struct _zero_gid_contact, 1);
    aEle->uin = kontakt->uin;
    aEle->gid = kontakt->gid;
    aEle->uid = kontakt->uid;
    aEle->nick = kontakt->nick; /* just a copy, because nothing get destroyed in kontakt */
    aEle->wait_auth = kontakt->wait_auth;

    printf ("saving... uin: %s, old gid: %d, old uid: %d, nick: %s\n",
            aEle->uin, aEle->gid, aEle->uid, aEle->nick);
    aList = g_slist_append (aList, aEle);

    /* update the main structure with new data */
    kontakt->gid = gid;
    kontakt->uid = contact_gen_uid ();

    snac13_construct_nickname (tlvs, kontakt->uin, gid, kontakt->uid,
                               kontakt->nick, kontakt->wait_auth);


    contact = contact->next;
  }

  snac13_send_add_to_list (conn, tlvs);

  free_tlvstack (tlvs);

  /* update the group */
  tlvs = new_tlvstack (NULL, 0);

  snac13_construct_id_ingroup (tlvs, gid);
  snac13_send_update_list (conn, tlvs);

  free_tlvstack (tlvs);

  /* remove the users with zero gid */
  tlvs = new_tlvstack (NULL, 0);

  temp_aList = aList;
  while (temp_aList) {
    aEle = (struct _zero_gid_contact *)temp_aList->data;

    printf ("adding to remove list... uin: %s, old gid: %d, old uid: %d, nick:"
            " %s\n", aEle->uin, aEle->gid, aEle->uid, aEle->nick);

    snac13_construct_nickname (tlvs, aEle->uin, aEle->gid, aEle->uid,
                               aEle->nick, aEle->wait_auth);

    temp_aList = temp_aList->next;
  }

  snac13_send_remove_from_list (conn, tlvs);
  free_tlvstack (tlvs);

  printf ("cleaning up ...\n");
  /* clean up */
  g_slist_free (aList);
  printf ("done snac13_fix_wrong_gid_contacts()\n");
}
