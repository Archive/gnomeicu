#ifndef __DETACH_H__
#define __DETACH_H__

#include "common.h"
#include <gtk/gtk.h>

void detach_contact_hide( GtkWidget *widget, gpointer data );
void detach_contact( GtkWidget *widget, gpointer data );

void detach_setrow(Contact_Member *contact, GtkTreeIter *iter);

#endif /* __DETACH_H__ */
