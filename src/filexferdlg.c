/**************************
 Handle file transfer
 (c) 1999 Jeremy Wise
 GnomeICU
***************************/

#include "common.h"
#include "tcp.h"
#include "filexferdlg.h"
#include "dirbrowser.h"

#include <gtk/gtk.h>

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

static void set_dir(gchar *dir, XferInfo *xfer);
static void save_incoming_file( GtkWidget *widget, gpointer data );
static gboolean update_file_transfer_dialog( FileXferDlg *dlg );


/*** Global functions ***/
FileXferDlg *create_file_xfer_dialog( XferInfo *xfer )
{
	FileXferDlg *dlg = g_new0( FileXferDlg, 1 );
	GtkWidget *table;
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *frame;
	GtkWidget *label;

	char string[21];

	if( dlg == NULL )
		return dlg;

        dlg->xfer = xfer;

	dlg->window = gtk_window_new( GTK_WINDOW_TOPLEVEL );
	gtk_window_set_title( GTK_WINDOW( dlg->window ), _("File Transfer") );
	gtk_container_set_border_width( GTK_CONTAINER( dlg->window ), 10 );

	table = gtk_table_new( 4, 2, FALSE );
	gtk_container_add( GTK_CONTAINER( dlg->window ), table );
	
	hbox = gtk_hbox_new( FALSE, 0 );

	label = gtk_label_new( _("Current File:") );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.5 );
	gtk_box_pack_start( GTK_BOX( hbox ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );
	
	snprintf( string, sizeof( string ), "1/%d", g_slist_length(xfer->file_queue) );

	dlg->tot_files = gtk_entry_new();
	gtk_entry_set_text( GTK_ENTRY( dlg->tot_files ), string );
	gtk_widget_set_usize( dlg->tot_files, 50, 0 );
	gtk_box_pack_end( GTK_BOX( hbox ), dlg->tot_files, FALSE, FALSE, 0 );
	gtk_widget_show( dlg->tot_files );

	dlg->current_file = gtk_entry_new();
	gtk_widget_set_usize( dlg->current_file, 137, 0 );
	gtk_entry_set_text( GTK_ENTRY( dlg->current_file ), ((struct xfer_file *)xfer->file_queue->data)->short_filename );
	gtk_box_pack_end( GTK_BOX( hbox ), dlg->current_file, TRUE, TRUE, 0 );
	gtk_widget_show( dlg->current_file );

	gtk_table_attach_defaults( GTK_TABLE( table ), hbox, 0, 2, 0, 1 );
	gtk_widget_show( hbox );

	hbox = gtk_hbox_new( FALSE, 0 );

	label = gtk_label_new( _("Local File Name:") );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.5 );
	gtk_box_pack_start( GTK_BOX( hbox ), label, FALSE, FALSE, 0 );
	gtk_widget_set_sensitive( label, xfer->direction == XFER_DIRECTION_RECEIVING );
	gtk_widget_show( label );

	dlg->local_filename = gtk_entry_new();
	gtk_box_pack_start( GTK_BOX( hbox ), dlg->local_filename, TRUE, TRUE, 0 );
	gtk_widget_set_sensitive( dlg->local_filename, xfer->direction == XFER_DIRECTION_RECEIVING );
	gtk_widget_show( dlg->local_filename );

	gtk_table_attach_defaults( GTK_TABLE( table ), hbox, 0, 2, 1, 2 );
	gtk_widget_show( hbox );

	frame = gtk_frame_new( _("Current File") );
	gtk_widget_set_usize( frame, 200, 0 );
	gtk_container_set_border_width( GTK_CONTAINER( frame ), 7 );
	vbox = gtk_vbox_new( TRUE, 0 );
	gtk_container_set_border_width( GTK_CONTAINER( vbox ), 7 );
	gtk_container_add( GTK_CONTAINER( frame ), vbox );
	gtk_table_attach_defaults( GTK_TABLE( table ), frame, 0, 1, 2, 3 );
	gtk_widget_show( frame );

	hbox = gtk_hbox_new( FALSE, 0 );

	label = gtk_label_new( _("Size:") );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.5 );
	gtk_box_pack_start( GTK_BOX( hbox ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	snprintf( string, sizeof( string ), "%d",  ((struct xfer_file *)xfer->file_queue->data)->total_bytes );

	dlg->cf_size = gtk_entry_new();
	gtk_entry_set_text( GTK_ENTRY( dlg->cf_size ), string );
	gtk_widget_set_usize( dlg->cf_size, 100, 0 );
	gtk_box_pack_end( GTK_BOX( hbox ), dlg->cf_size, FALSE, FALSE, 0 );
	gtk_widget_show( dlg->cf_size );

	gtk_box_pack_start( GTK_BOX( vbox ), hbox, FALSE, FALSE, 0 );
	gtk_widget_show( hbox );

	hbox = gtk_hbox_new( FALSE, 0 );

	label = gtk_label_new( _("Completed:") );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.5 );
	gtk_box_pack_start( GTK_BOX( hbox ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	dlg->cf_trans = gtk_entry_new();
	gtk_widget_set_usize( dlg->cf_trans, 100, 0 );
	gtk_box_pack_end( GTK_BOX( hbox ), dlg->cf_trans, FALSE, FALSE, 0 );
	gtk_widget_show( dlg->cf_trans );

	gtk_box_pack_start( GTK_BOX( vbox ), hbox, FALSE, FALSE, 0 );
	gtk_widget_show( hbox );

	hbox = gtk_hbox_new( FALSE, 0 );

	label = gtk_label_new( _("Time Left:") );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.5 );
	gtk_box_pack_start( GTK_BOX( hbox ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	dlg->cf_time = gtk_entry_new();
	gtk_widget_set_usize( dlg->cf_time, 100, 0 );
	gtk_box_pack_end( GTK_BOX( hbox ), dlg->cf_time, FALSE, FALSE, 0 );
	gtk_widget_show( dlg->cf_time );

	gtk_box_pack_start( GTK_BOX( vbox ), hbox, FALSE, FALSE, 0 );
	gtk_widget_show( hbox );

	hbox = gtk_hbox_new( FALSE, 0 );

	label = gtk_label_new( _("Speed:") );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.5 );
	gtk_box_pack_start( GTK_BOX( hbox ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	dlg->cf_bps = gtk_entry_new();
	gtk_widget_set_usize( dlg->cf_bps, 100, 0 );
	gtk_box_pack_end( GTK_BOX( hbox ), dlg->cf_bps, FALSE, FALSE, 0 );
	gtk_widget_show( dlg->cf_bps );

	gtk_box_pack_start( GTK_BOX( vbox ), hbox, FALSE, FALSE, 0 );
	gtk_widget_show( hbox );

	hbox = gtk_hbox_new( FALSE, 0 );

	gtk_box_pack_start( GTK_BOX( vbox ), hbox, FALSE, FALSE, 0 );
	gtk_widget_show( hbox );

	dlg->cf_progress = gtk_progress_bar_new();
	gtk_box_pack_start( GTK_BOX( vbox ), dlg->cf_progress, FALSE, FALSE, 0 );
	gtk_widget_show( dlg->cf_progress );

	gtk_widget_show( vbox );

	frame = gtk_frame_new( _("Batch") );
	gtk_widget_set_usize( frame, 200, 0 );
	gtk_container_set_border_width( GTK_CONTAINER( frame ), 7 );
	vbox = gtk_vbox_new( TRUE, 0 );
	gtk_container_set_border_width( GTK_CONTAINER( vbox ), 7 );
	gtk_container_add( GTK_CONTAINER( frame ), vbox );
	gtk_table_attach_defaults( GTK_TABLE( table ), frame, 1, 2, 2, 3 );
	gtk_widget_show( frame );

	hbox = gtk_hbox_new( FALSE, 0 );

	label = gtk_label_new( _("Size:") );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.5 );
	gtk_box_pack_start( GTK_BOX( hbox ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	snprintf( string, sizeof( string ), "%d", xfer->total_bytes );

	dlg->b_size = gtk_entry_new();
	gtk_entry_set_text( GTK_ENTRY( dlg->b_size ), string );
	gtk_widget_set_usize( dlg->b_size, 100, 0 );
	gtk_box_pack_end( GTK_BOX( hbox ), dlg->b_size, FALSE, FALSE, 0 );
	gtk_widget_show( dlg->b_size );

	gtk_box_pack_start( GTK_BOX( vbox ), hbox, FALSE, FALSE, 0 );
	gtk_widget_show( hbox );

	hbox = gtk_hbox_new( FALSE, 0 );

	label = gtk_label_new( _("Completed:") );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.5 );
	gtk_box_pack_start( GTK_BOX( hbox ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	dlg->b_trans = gtk_entry_new();
	gtk_widget_set_usize( dlg->b_trans, 100, 0 );
	gtk_box_pack_end( GTK_BOX( hbox ), dlg->b_trans, FALSE, FALSE, 0 );
	gtk_widget_show( dlg->b_trans );

	gtk_box_pack_start( GTK_BOX( vbox ), hbox, FALSE, FALSE, 0 );
	gtk_widget_show( hbox );

	hbox = gtk_hbox_new( FALSE, 0 );

	label = gtk_label_new( _("Time Left:") );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.5 );
	gtk_box_pack_start( GTK_BOX( hbox ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	dlg->b_time = gtk_entry_new();
	gtk_widget_set_usize( dlg->b_time, 100, 0 );
	gtk_box_pack_end( GTK_BOX( hbox ), dlg->b_time, FALSE, FALSE, 0 );
	gtk_widget_show( dlg->b_time );

	gtk_box_pack_start( GTK_BOX( vbox ), hbox, FALSE, FALSE, 0 );
	gtk_widget_show( hbox );

	hbox = gtk_hbox_new( FALSE, 0 );

	label = gtk_label_new( _("Speed:") );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.5 );
	gtk_box_pack_start( GTK_BOX( hbox ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	dlg->b_bps = gtk_entry_new();
	gtk_widget_set_usize( dlg->b_bps, 100, 0 );
	gtk_box_pack_end( GTK_BOX( hbox ), dlg->b_bps, FALSE, FALSE, 0 );
	gtk_widget_show( dlg->b_bps );

	gtk_box_pack_start( GTK_BOX( vbox ), hbox, FALSE, FALSE, 0 );
	gtk_widget_show( hbox );

	hbox = gtk_hbox_new( FALSE, 0 );

	gtk_box_pack_start( GTK_BOX( vbox ), hbox, FALSE, FALSE, 0 );
	gtk_widget_show( hbox );

	dlg->b_progress = gtk_progress_bar_new();
	gtk_box_pack_start( GTK_BOX( vbox ), dlg->b_progress, FALSE, FALSE, 0 );
	gtk_widget_show( dlg->b_progress );

	gtk_widget_show( vbox );

	dlg->cancel = gtk_button_new_with_label( _("Cancel") );
	gtk_table_attach_defaults( GTK_TABLE( table ), dlg->cancel, 0, 2, 3, 4 );
	gtk_widget_show( dlg->cancel );

	gtk_widget_show( table );
	gtk_widget_show( dlg->window );

	g_signal_connect( G_OBJECT( dlg->cancel ), "clicked",
	                  G_CALLBACK( ft_cancel_transfer ),
	                  xfer );

	g_signal_connect( G_OBJECT( dlg->window ), "delete_event",
	                  G_CALLBACK( ft_cancel_transfer ),
	                  xfer );
       dlg->timer = g_timeout_add(250, (GSourceFunc)&update_file_transfer_dialog, (gpointer) dlg);

       return dlg;
}

gboolean update_file_transfer_dialog( FileXferDlg *dlg )
{
	char *message;
	struct xfer_file *current_file;
	int t;
	int finish_time;
	
#ifdef TRACE_FUNCTION
	g_print("update_file_transfer_dialog\n");
#endif
	
	current_file = (struct xfer_file *)dlg->xfer->file_queue->data;

	message = g_strdup_printf("%d/%d", g_slist_length(dlg->xfer->completed_files) + 1, g_slist_length(dlg->xfer->file_queue) + g_slist_length(dlg->xfer->completed_files));

	gtk_entry_set_text( GTK_ENTRY( dlg->tot_files ), message );
	g_free(message);
	gtk_entry_set_text( GTK_ENTRY( dlg->current_file ),
					current_file->short_filename );

	message = g_strdup_printf("%d",  current_file->total_bytes );
	gtk_entry_set_text( GTK_ENTRY( dlg->cf_size ), message );
	g_free(message);

	message = g_strdup_printf("%d", current_file->completed_bytes );
	gtk_entry_set_text( GTK_ENTRY( dlg->cf_trans ), message );
	g_free(message);

	message = g_strdup_printf("%d", dlg->xfer->completed_bytes );
	gtk_entry_set_text( GTK_ENTRY( dlg->b_trans ), message );
	g_free(message);

	t = time( NULL ) - current_file->start_time;

	if ((current_file->completed_bytes >= t) && t) {
		finish_time = (current_file->total_bytes - current_file->completed_bytes) / (current_file->completed_bytes / t);

		message = g_strdup_printf("%02d:%02d:%02d", finish_time/3600, (finish_time%3600)/60, finish_time%60 );
		gtk_entry_set_text( GTK_ENTRY( dlg->cf_time ), message );
		g_free(message);
	}

	t = time( NULL ) - current_file->start_time;

	if ((current_file->completed_bytes >= t) && t) {
		finish_time = (dlg->xfer->total_bytes - dlg->xfer->completed_bytes) / (dlg->xfer->completed_bytes / t);

		message = g_strdup_printf("%02d:%02d:%02d", finish_time/3600, (finish_time%3600)/60, finish_time%60 );
		gtk_entry_set_text( GTK_ENTRY( dlg->b_time ), message );
		g_free(message);
	}

	if( current_file->completed_bytes &&
	    current_file->start_time != time( NULL ) ) {
		message = g_strdup_printf("%.1f KB/sec",
					  current_file->completed_bytes /
					  ( time( NULL ) - current_file->start_time ) /1024.0);
		gtk_entry_set_text( GTK_ENTRY( dlg->cf_bps ), message );
		g_free(message);
	} else 
	  gtk_entry_set_text( GTK_ENTRY( dlg->cf_bps ), "0 KB/sec");


	if( dlg->xfer->completed_bytes && dlg->xfer->start_time != time( NULL ) ) {
		message = g_strdup_printf("%.1f KB/sec",
					  (dlg->xfer->completed_bytes /
					   ( time( NULL ) - dlg->xfer->start_time )) / 1024.0 );
		gtk_entry_set_text( GTK_ENTRY( dlg->b_bps ), message );
		g_free(message);
	} else
	  gtk_entry_set_text( GTK_ENTRY( dlg->cf_bps ), "0 KB/sec");
	if( current_file->completed_bytes == 0 ||
		 current_file->total_bytes == 0 )
		gtk_progress_bar_update( GTK_PROGRESS_BAR( dlg->cf_progress ), 0.0F );
	else
		gtk_progress_bar_update( GTK_PROGRESS_BAR( dlg->cf_progress ),
					(float)( (float)current_file->completed_bytes / (float)current_file->total_bytes ) );
	if( dlg->xfer->completed_bytes == 0 ||
            dlg->xfer->total_bytes == 0 )
		gtk_progress_bar_update( GTK_PROGRESS_BAR( dlg->b_progress ), 0.0F );
	else
		gtk_progress_bar_update( GTK_PROGRESS_BAR( dlg->b_progress ),
					(float)( (float)dlg->xfer->completed_bytes / (float)dlg->xfer->total_bytes ) );
	
	return TRUE;
}

void destroy_file_xfer_dialog( FileXferDlg *xferdlg) {
        if( xferdlg ) {
                if ( xferdlg->window )
                        gtk_widget_destroy( xferdlg->window );
        
	        if (xferdlg->timer)
        	        g_source_remove(xferdlg->timer);
	}
        
        g_free( xferdlg );
}

void file_place( XferInfo *xfer )
{
	GtkWidget *filesel;
#ifdef TRACE_FUNCTION
	g_print( "file_place\n" );
#endif

	if (xfer->auto_save_path) {
            /* We have been given a directory to dump this stuff in to; don't prompt the user */
            ((struct xfer_file*)xfer->file_queue->data)->full_filename = g_strdup_printf("%s%s", xfer->auto_save_path, ((struct xfer_file*)xfer->file_queue->data)->short_filename);
            save_incoming_file(0, xfer);
            return;
	}

	if (g_slist_length(xfer->file_queue) > 1) {
          filesel = create_dir_browser(_("Select destination folder"), (void (*)(gchar*, gpointer))&set_dir, (gpointer)xfer);
	}
	else {
		filesel = gtk_file_selection_new( _("File Transfer") );

		gtk_file_selection_set_filename( GTK_FILE_SELECTION( filesel ), ((struct xfer_file*)xfer->file_queue->data)->short_filename );
		g_signal_connect (G_OBJECT (GTK_FILE_SELECTION(filesel)->ok_button), "clicked",
		                  G_CALLBACK (save_incoming_file), xfer);
		g_signal_connect (G_OBJECT (GTK_FILE_SELECTION(filesel)->cancel_button), "clicked",
		                  G_CALLBACK (ft_cancel_transfer), xfer);
		g_signal_connect_swapped (G_OBJECT (GTK_FILE_SELECTION (filesel)->cancel_button), "clicked",
				          G_CALLBACK (gtk_widget_destroy),
	  	                          filesel);
	}

	gtk_widget_show(filesel);
}

void set_dir(gchar *dir, XferInfo *xfer) {
	xfer->auto_save_path = g_strdup(dir);
	file_place(xfer);
}

void save_incoming_file( GtkWidget *widget, gpointer data )
{
	XferInfo *xfer = data;

	if (widget)
          ((struct xfer_file*)xfer->file_queue->data)->full_filename = g_strdup( gtk_file_selection_get_filename( GTK_FILE_SELECTION( widget->parent->parent->parent ) ) );

	if (!((struct xfer_file*)xfer->file_queue->data)->full_filename) {
         /* we don't want to call open(NULL,...) */
         ft_cancel_transfer(0, xfer);
         return;
        }

        xfer->dialog = create_file_xfer_dialog(xfer);
        if (xfer->dialog == NULL) {
         ft_cancel_transfer(0, xfer);
         return;
        }

	gtk_entry_set_text( GTK_ENTRY( ((FileXferDlg*)xfer->dialog)->local_filename ), ((struct xfer_file*)xfer->file_queue->data)->full_filename );

	if (widget)
		gtk_widget_destroy( GTK_WIDGET( widget->parent->parent->parent ) );

	xfer->file_fd = open( ((struct xfer_file*)xfer->file_queue->data)->full_filename, O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH );
	if( xfer->file_fd == -1 ) {
         ft_cancel_transfer(0, xfer);
         return;
	}

        ft_readytoreceive(xfer);

 	((struct xfer_file *)xfer->file_queue->data)->start_time = time( NULL );

 	if( xfer->start_time == 0 )
          xfer->start_time = time( NULL );

	return;
}

