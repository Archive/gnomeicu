/* GnomeICU
 * Copyright (C) 1998-2002 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* XML contacts list storage management */

#ifndef __XMLCONTACT_H__
#define __XMLCONTACT_H__

#include <glib.h>

/* globally accessible functions */
gboolean xmlcontact_load (const gchar *filename);
void xmlcontact_save (const gchar *filename);

#endif /* __XMLCONTACT_H__ */
