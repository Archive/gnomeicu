#ifndef __USER_POPUP_H__
#define __USER_POPUP_H__

#include "common.h"

GtkWidget *user_popup (CONTACT_PTR contact);
void request_file( GtkWidget *widget, GList *files, gpointer data);

#endif /* __USER_POPUP_H__ */
