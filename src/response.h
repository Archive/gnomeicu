/**************************************
 Send various packets to to the server
 (c) 1999 Jeremy Wise
 GnomeICU
***************************************/

#ifndef __RESPONSE_H__
#define __RESPONSE_H__

#include "common.h"
#include "filexfer.h"
#include "v7base.h"

void User_Online( GSList *contact, gboolean startup);
void User_Offline( GSList *contact );
void Do_File( const char *msg, UIN_T uin, const gchar *msgid, 
	      const gchar *file_name, int file_size , V7Connection *conn, 
	      int downcounter);
void msg_received (UIN_T uin, gchar *msg, time_t msg_time);
void url_received (UIN_T uin, const gchar *url, const gchar *desc, time_t msg_time);
void contact_list_received (UIN_T uin, GList *contacts, time_t msg_time);
void user_added_you (UIN_T uin, time_t msg_time);
void authorization_request(UIN_T uin, gchar *msg, time_t msg_time);
void recv_awaymsg( UIN_T uin, DWORD status, const gchar *message);
void start_transfer( UIN_T uin, const gchar *msgid, DWORD fileip, 
		     WORD fileport);
void set_contact_status (GSList *contact, int status, gboolean startup);
void auth_reply(UIN_T uin, int flag, char *reason, time_t msg_time);

void user_group_changed(UIN_T uin, guint gid, guint uid);

#endif /* __RESPONSE_H__ */
