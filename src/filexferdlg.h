/*****************************
 Handle file transfer dialogs functions
 (c) 1999 Jeremy Wise
 GnomeICU
******************************/

#ifndef __FILEXFERDLG_H__
#define __FILEXFERDLG_H__

#include "common.h"
#include "filexfer.h"

#include <time.h>

typedef struct
{
	GtkWidget *window;
	GtkWidget *current_file;
	GtkWidget *tot_files;
	GtkWidget *local_filename;
	GtkWidget *cf_size;
	GtkWidget *cf_trans;
	GtkWidget *cf_time;
	GtkWidget *cf_bps;
	GtkWidget *cf_progress;
	GtkWidget *b_size;
	GtkWidget *b_trans;
	GtkWidget *b_time;
	GtkWidget *b_bps;
	GtkWidget *b_progress;
	GtkWidget *cancel;

        guint timer;

        XferInfo  *xfer;
} FileXferDlg;

FileXferDlg *create_file_xfer_dialog( XferInfo *xfer);
void destroy_file_xfer_dialog( FileXferDlg *xferdlg );
void file_place( XferInfo *xfer );

#endif /* __FILEXFERDLG_H__ */
