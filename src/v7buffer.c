
#include "v7buffer.h"

V7Buffer *v7_buffer_new(gchar *data, int len, gboolean owned)
{
  V7Buffer *buf = NULL;
  
  buf = g_new0 (V7Buffer, 1);

  buf->here = data;

  if (owned)
    buf->buf = data;

  buf->len = len;

  return buf;
}

void v7_buffer_free(V7Buffer *buf) 
{
  if (!buf)
    return;

  g_free(buf->buf);
  g_free(buf);
}


guchar *v7_buffer_get(V7Buffer *buf, int len)
{
  guchar *data;

  if (!buf)
    return NULL;

  if (buf->len < len) {
    g_debug ("Buffer does not hold %d, only had %d\n", len, buf->len);
    return NULL;
  }

  data = buf->here;

  buf->here += len;
  buf->len -= len;

  return data;
}
