

#ifndef __V7SEND_H__
#define __V7SEND_H__

#include "v7base.h"
#include "filexfer.h"


void v7_sendmsg(V7Connection *conn, UIN_T uin, const guchar *text); 
void v7_sendurl(V7Connection *conn, UIN_T uin, gchar *url, gchar *desc);
void v7_grant_auth(V7Connection *conn, UIN_T uin);
void v7_deny_auth(V7Connection *conn, UIN_T uin, gchar *reason);
void v7_sendcontacts(V7Connection *conn, UIN_T uin, GList *contacts);

void v7_request_away_msg(V7Connection *conn, UIN_T uin, WORD contactstatus);
void v7_reqofflinemsg(V7Connection *conn);

void v7_aimsetstatus(V7Connection *conn, gchar *awaymessage);

void v7_setstatus(V7Connection *conn, DWORD newstatus);
void v7_setstatus2(V7Connection *conn, DWORD newstatus);

void v7_quit(void);

void v7_send_often(V7Connection *conn, DWORD reqid);

DWORD v7_findbyuin(V7Connection *conn, UIN_T uin);
DWORD v7_search(V7Connection *conn, gchar *nick, gchar *firstname,
		gchar *lastname, gchar *email);
DWORD v7_sendxml (V7Connection *conn, gchar *xmlstring);
DWORD v7_request_info (V7Connection *conn, UIN_T uin);
DWORD v7_request_our_info(V7Connection *conn);

void v7_modify_main_home_info (V7Connection *conn, gchar *nick,
			       gchar *first, gchar *last,
			       gchar *email, gchar *city, gchar *state, 
			       gchar *phone, gchar *fax, gchar *street, 
			       gchar *cellular, gchar *zip, WORD country,
			       BYTE timezone, BYTE publish_email);
void v7_modify_more_info (V7Connection *conn, BYTE age, BYTE sex, 
			  gchar *homepage, WORD birthyear,
                          BYTE birthmonth, BYTE birthday, BYTE language1,
                          BYTE language2, BYTE language3);
void v7_modify_work_info (V7Connection *conn, gchar *city, gchar *state, 
			  gchar *phone, gchar *fax,
			  gchar *street, gchar *zip, WORD country,
			  gchar *company_name, gchar *department,
			  gchar *position, WORD occupation,
			  gchar *webpage);
void v7_modify_about (V7Connection *conn, gchar *about);
void v7_modify_password(V7Connection *conn, gchar *password);
void v7_modify_permissions(V7Connection *conn, BYTE auth, BYTE webaware);
void v7_modify_emails_info (V7Connection *conn, GList *emails);
void v7_modify_interests_info (V7Connection *conn, GList *interests);
void v7_modify_past_aff_info (V7Connection *conn, GList *past, GList *affiliations);

void v7_iam_icq (V7Connection *conn);
void v7_request_rate (V7Connection *conn);
void v7_make_requests(V7Connection *conn);
void v7_set_user_info (V7Connection *conn);
void v7_add_icbm (V7Connection *conn);
void v7_client_ready (V7Connection *conn);

void v7_sendfile(V7Connection *conn, UIN_T uin, const gchar *msg,
		 GSList *files );
void v7_acceptfile(XferInfo *xfer);

/* Exported only for ping */
void v7_sendmsg_plain(V7Connection *conn, UIN_T uin, const guchar *text);

void v7_send_idletime(V7Connection *conn, DWORD idletime);
#endif /* __V7SEND_H__ */
