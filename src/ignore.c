#include "common.h"
#include "gnomeicu.h"
#include "gtkfunc.h"
#include "ignore.h"
#include "listwindow.h"
#include "showlist.h"
#include "v7send.h"
#include "v7snac13.h"
#include "groups.h"
#include "gnomecfg.h"

#include <gtk/gtkstock.h>
#include <string.h>

static gboolean is_ignore (CONTACT_PTR contact)
{
	return (contact ? contact->ignore_list : FALSE);
}

static void ignore_add(Contact_Member *contact)
{
  v7_ignore (mainconnection, contact->uin,
	     contact_gen_uid (),
	     TRUE); /* add */

  if (contact->vis_list == TRUE) {
    v7_visible (mainconnection, contact->uin, 
		contact->vislist_uid, FALSE); /* remove */		    
  }
}

static void ignore_remove(Contact_Member *contact)
{
  v7_ignore (mainconnection, contact->uin,
	     contact->ignorelist_uid,
	     FALSE); /* add */
}

void ignore_list_dialog( void )
{
  
	static GtkWidget *dlg = NULL;
	
	if( dlg == NULL )
	{
		dlg = list_window_new_filter(
			_("Ignore List"),
			_("Drag the users you wish to add to your\n"
			  "ignore list into this window."),
			ignore_add,
			ignore_remove,
			is_ignore);
		g_object_add_weak_pointer (G_OBJECT (dlg), (gpointer *)&dlg);
	} else {
		gtk_window_present (GTK_WINDOW (dlg));
	}
  
}
