/***************************
 Contact search
 (c) 1999 Jeremy Wise
 GnomeICU
****************************/

#ifndef __SEARCH_H__
#define __SEARCH_H__

#include <gtk/gtkwidget.h>
#include <gtk/gtkliststore.h>

enum
{
	SEARCH_STATUS_COLUMN_ICON,
	SEARCH_UIN_COLUMN,
	SEARCH_NICKNAME_COLUMN,
	SEARCH_FIRST_COLUMN,
	SEARCH_LAST_COLUMN,
	SEARCH_EMAIL_COLUMN,
	SEARCH_SEX_COLUMN,
	SEARCH_AGE_COLUMN,
	SEARCH_N_COLUMNS
};

void search_window (GtkWidget *widget, gpointer data);

void Display_Search_Reply(WORD reqid, UIN_T uin, gchar *nick, gchar *first,
                          gchar *last, gchar *email, gchar auth,
                          gchar status, gchar sex, gchar age);

typedef enum { 
  COMPLETE,
  TOO_MANY_RESULTS,
  NO_RESULTS
} SearchEnd;

void  search_finished (WORD reqid, gint16 more_results);


#endif /* __SEARCH_H__ */
