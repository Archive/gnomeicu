/********************************
 Message history
 (c) 1999 Jeremy Wise
 (c) 2001 Gediminas Paulauskas
 GnomeICU
*********************************/

#ifndef __HISTORY_H__
#define __HISTORY_H__

#include "datatype.h"

#include <gtk/gtkwidget.h>
#include <time.h>

void history_add_incoming( UIN_T uin, const char *statement, time_t *timedate );
void history_add_outgoing( UIN_T uin, const char *statement );

void history_display (GtkWidget *widget, Contact_Member *contact);
void convert_history ();

#endif /* __HISTORY_H__ */
