/* GnomeICU
 * Copyright (C) 1998-2003 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*******************************
 Handling of ICQ (sound) events
 (c) 1999 Jeremy Wise
 GnomeICU
********************************/

#include "common.h"
#include "events.h"
#include "gnomeicu.h"
#include "util.h"

#include <pwd.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <libgnome/gnome-triggers.h>

static void gnomeicu_fire_external_program( ICQEvType event, CONTACT_PTR contact );
	

/*** Global functions ***/
void gnomeicu_event(ICQEvType event, GSList *contact)
{
  gboolean enable_sound = FALSE;

#ifdef TRACE_FUNCTION
  g_print( "gnomeicu_event(%d)\n", event );
#endif

  if (contact && !kontakt->confirmed)
    return;

  if( contact )
    gnomeicu_fire_external_program(event, kontakt);

  if( preferences_get_bool (PREFS_GNOMEICU_SOUND_ONLINE) == FALSE ||
      (Current_Status & 0xffff) == STATUS_ONLINE ||
      (Current_Status & 0xffff) == STATUS_FREE_CHAT ) {
    enable_sound = TRUE;
  }

  if( !( enable_online_events && enable_sound) )
    return;

  if( preferences_get_bool (PREFS_GNOMEICU_BEEPS) && event != EV_USEROFF )
    gdk_beep();

  if (!preferences_get_bool (PREFS_GNOMEICU_SOUND))
    return;

  switch(event) {
  case EV_MSGRECV:
    gnome_triggers_do("", "program", "GnomeICU", "message", NULL);
    break;
  case EV_USERON:
    gnome_triggers_do("", "program", "GnomeICU", "online", NULL);
    break;
  case EV_USEROFF:
    gnome_triggers_do("", "program", "GnomeICU", "offline", NULL);
    break;
  case EV_URLRECV:
    gnome_triggers_do("", "program", "GnomeICU", "url", NULL);
    break;
  case EV_AUTHREQ:
    gnome_triggers_do("", "program", "GnomeICU", "authreq", NULL);
    break;
  case EV_AUTH:
    gnome_triggers_do("", "program", "GnomeICU", "auth", NULL);
    break;
  case EV_FILEREQ:
    gnome_triggers_do("", "program", "GnomeICU", "file", NULL);
    break;
  case EV_LISTADD:
    gnome_triggers_do("", "program", "GnomeICU", "notify", NULL);
    break;
  case EV_CONTLIST:
    gnome_triggers_do("", "program", "GnomeICU", "contlist", NULL);
    break;
  default:
    g_warning("Unknown event: %d\n", event);
  }
}

static const gchar *get_status(DWORD status)
{
  gchar *s;

  if (status == STATUS_OFFLINE) {
    s = "OFF";
  } else {
    switch (status & 0xffff) {
    case STATUS_ONLINE:
      s = "ON";
      break;
    case STATUS_DND:
      s = "DND";
      break;
    case STATUS_AWAY:
      s = "AWAY";
      break;
    case STATUS_OCCUPIED:
      s = "OCC";
      break;
    case STATUS_NA:
      s = "NA";
      break;
    case STATUS_INVISIBLE:
      s = "INV";
      break;
    case STATUS_FREE_CHAT:
      s = "FFC";
      break;
    default:
      g_warning("Unknown status: 0x%x\n", status & 0xffff);
      s = "UNK";
    }
  }

  return s;
}

static gchar *expand_command(gchar *command, CONTACT_PTR contact)
{
  gchar *foo, *ip;
  GString *expanded;

  expanded = g_string_sized_new(128);

  /* skip whitespaces at start */
  for (; *command && g_unichar_isspace(*command); command = g_utf8_next_char(command));

  /* expand ~/ to the user's home dir */
  if (command[0] == '~' && command[1] == G_DIR_SEPARATOR) {
    g_string_append(expanded, g_get_home_dir());
    g_string_append_c(expanded, G_DIR_SEPARATOR);
    command += 2;
  } else if (command[0] == '~') { /* expand ~login to the home dir */
    struct passwd *pwd_ptr;
    gchar *login;

    for (foo = command + 1; *foo && *foo != G_DIR_SEPARATOR; foo = g_utf8_next_char(foo));

    login = g_strndup(command + 1, foo - command - 1);
    while (((pwd_ptr = getpwent ()) != NULL) && strcmp(login, pwd_ptr->pw_name));

    /* we close /etc/passwd */
    endpwent();

    /* we have found the user */
    if (pwd_ptr != NULL) {
      g_string_append(expanded, pwd_ptr->pw_dir);
      command = foo;
    }

    g_free(login);
  }

  /* now, expand the % stuff */
  while (*command) {
    /* find next '%' */
    for (foo = command; *foo && *foo != '%'; foo = g_utf8_next_char(foo));

    if (!(*foo)) {
      /* no more '%' found => we can end the loop */
      g_string_append(expanded, command);
      break;
    } else {
      /* '%' found, add everything before it and parse the % option */

      g_string_append_len(expanded, command, foo - command);
      foo = g_utf8_next_char(foo);

      switch (*foo) {
      case '%':
	g_string_append_c(expanded, '%');
	break;
      case 'S': /* our status */
	g_string_append(expanded, get_status(Current_Status));
	break;
      case 's': /* contact's status */
	g_string_append(expanded, get_status(contact->status));
	break;
      case 'U': /* our uin */
	g_string_append(expanded, our_info->uin);
	break;
      case 'u': /* contact's uin */
	g_string_append(expanded, contact->uin);
	break;
      case 'N': /* our nick */
	g_string_append(expanded, our_info->nick);
	break;
      case 'n': /* contact's nick */
	g_string_append(expanded, contact->nick);
	break;
      case 'i': /* contact's IP address */
	if( !contact->has_direct_connect) {
	  ip = g_strdup_printf( "%u.%u.%u.%u",
				(BYTE) (contact->current_ip >> 24),
				(BYTE) (contact->current_ip >> 16),
				(BYTE) (contact->current_ip >> 8), 
				(BYTE) (contact->current_ip) );    
	}  else
	  ip = g_strdup ( _("N/A") );
	g_string_append(expanded, ip);
	g_free(ip);
	break;
      case 0:
	/* end of string */
	break;
      default:
	/* unknown parameter, just skip it */
	break;
      }

      if (*foo)
        foo = g_utf8_next_char(foo);
    }

    command = foo;
  }

  foo = g_strdup(expanded->str);
  g_string_free(expanded, TRUE);

  return foo;
}

/* remember to free the return value */
gchar *
events_grab_gconf_command(const gchar *key)
{
  return preferences_get_string (key);
}

void gnomeicu_fire_external_program( ICQEvType event, CONTACT_PTR contact )
{
  gchar *command = NULL;
  gchar *exp_command = NULL;

  ICUTrace("gnomeicu_fire_external_program(%d,%x)\n", event, (int)contact);

  g_return_if_fail ( contact );

  if( !enable_online_events )
    return;

  switch(event) {
  case EV_MSGRECV:
    command = events_grab_gconf_command (PREFS_GNOMEICU_PROG_EVENT_RECV_MSG);
    break;
  case EV_URLRECV:
    command = events_grab_gconf_command (PREFS_GNOMEICU_PROG_EVENT_RECV_URL);
    break;
  case EV_FILEREQ:
    command = events_grab_gconf_command (PREFS_GNOMEICU_PROG_EVENT_FILE_REQ);
    break;
  case EV_USERON:
    command = events_grab_gconf_command (PREFS_GNOMEICU_PROG_EVENT_USER_ONLINE);
    break;
  case EV_USEROFF:
    command = events_grab_gconf_command (PREFS_GNOMEICU_PROG_EVENT_USER_OFFLINE);
    break;
  case EV_AUTH:
    command = events_grab_gconf_command (PREFS_GNOMEICU_PROG_EVENT_AUTH_USER);
    break;
  case EV_AUTHREQ:
    command = events_grab_gconf_command (PREFS_GNOMEICU_PROG_EVENT_AUTH_REQ);
    break;
  case EV_LISTADD:
    command = events_grab_gconf_command (PREFS_GNOMEICU_PROG_EVENT_ADDED_TO_USER);
    break;
  case EV_CONTLIST:
    command = events_grab_gconf_command (PREFS_GNOMEICU_PROG_EVENT_CONTACT_LIST);
    break;
  default:
    g_warning("Unknown event: %d\n", event);
    return;
  }

  if( command && *command ) {
    exp_command = expand_command(command, contact);
    g_free(command);
    g_spawn_command_line_async(exp_command, NULL);
    g_free(exp_command);
  }
}
