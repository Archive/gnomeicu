/* GnomeICU
 * Copyright (C) 1998-2003 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * Manage GnomeICU contact list with gtktreeview/treemodel/liststore
 */

#include "common.h"
#include "gtkfunc.h"
#include "dragdrop.h"
#include "events.h"
#include "gnomecfg.h"
#include "gnomeicu.h"
#include "icons.h"
#include "response.h"
#include "showlist.h"
#include "user_popup.h"
#include "groups.h"
#include "group_popup.h"
#include "util.h"
#include "msg.h"

#include <gdk/gdkkeysyms.h>
#include <gtk/gtknotebook.h>
#include <string.h>

#define GROUP_NORMAL 0
#define GROUP_WAIT_FOR_AUTH 1
#define GROUP_NOT_IN_LIST 2


static void gnomeicu_tree_user_add_online (Contact_Member *contact);
static void gnomeicu_tree_user_remove_online (Contact_Member *contact);
static gint tree_sort_compare (GtkTreeModel *model,
                               GtkTreeIter *a, GtkTreeIter *b,
                               gpointer user_data);
void
gnomeicu_tree_icon_cell_data_func (GtkCellLayout *layuout,
                                   GtkCellRenderer *cell, GtkTreeModel *model,
                                   GtkTreeIter *iter, gpointer d)
{
  GdkPixbuf *pixbuf = NULL;

  gtk_tree_model_get (model, iter, ICON_COLUMN, &pixbuf, -1);

  if (pixbuf == NULL)
    g_object_set (GTK_CELL_RENDERER (cell), "visible", FALSE, NULL);
  else {
    g_object_set (GTK_CELL_RENDERER (cell), "visible", TRUE, NULL);
    g_object_set (GTK_CELL_RENDERER (cell), "pixbuf", pixbuf, NULL);
    g_object_unref (pixbuf);
  }
}

static void
gnomeicu_tree_icon2_cell_data_func (GtkCellLayout *layout,
                                   GtkCellRenderer *cell, GtkTreeModel *model,
                                   GtkTreeIter *iter, gpointer d)
{
  GdkPixbuf *pixbuf = NULL;

  gtk_tree_model_get (model, iter, ICON2_COLUMN, &pixbuf, -1);

  if (pixbuf == NULL)
    g_object_set (GTK_CELL_RENDERER (cell), "visible", FALSE, NULL);
  else {
    g_object_set (GTK_CELL_RENDERER (cell), "visible", TRUE, NULL);
    g_object_set (GTK_CELL_RENDERER (cell), "pixbuf", pixbuf, NULL);
    g_object_unref (pixbuf);
  }
}


static void
gnomeicu_tree_count_children(GtkTreeModel *model,
			     GtkTreeIter *parent,
			     int *all, int *online) {
  g_assert(all);
  g_assert(online);
  GtkTreeIter child;
  Contact_Member *contact;
  
  *all = 0;
  *online = 0;
  
  if (!gtk_tree_model_iter_children(model, &child, parent))
    return;

  do {
    ++(*all);

    gtk_tree_model_get(model, &child,
		       CONTACT_COLUMN, &contact,
		       -1);
    if (contact && contact->status != STATUS_OFFLINE) {
      ++(*online);
    }
    
  } while (gtk_tree_model_iter_next(model, &child));

}
static gboolean
gnomeicu_tree_is_online (GtkTreeModel *model,
					GtkTreeIter *iter,
					gpointer data)
{
  Contact_Member *contact;
  GtkTreeIter parent;
  int all, online;

  gtk_tree_model_get (model, iter, CONTACT_COLUMN, &contact, -1);

  if (contact) {
    return (contact->status != STATUS_OFFLINE);

  } else { /* is group */
    if (!gtk_tree_model_iter_has_child(model, iter))
      return FALSE;

    gnomeicu_tree_count_children(model, iter, &all, &online);
    return online;
  }
}

static void
gnomeicu_tree_expand_row(GtkTreeModel *model,
			 GtkTreePath *path,
			 GtkTreeIter *iter,
			 gpointer user_data)
{
  GtkWidget *treeview = user_data;

  gtk_tree_view_expand_row(GTK_TREE_VIEW(treeview), path, TRUE);

}

void gnomeicu_tree_setup_cell_layout(GtkCellLayout *layout)
{
  GtkCellRenderer *icon_renderer, *icon2_renderer;
  GtkCellRenderer *text_renderer;

  icon_renderer = gtk_cell_renderer_pixbuf_new();
  text_renderer = gtk_cell_renderer_text_new();
  icon2_renderer = gtk_cell_renderer_pixbuf_new();

  g_object_set(text_renderer,
	       "ellipsize-set", TRUE,
	       "ellipsize", PANGO_ELLIPSIZE_END,
	       NULL);

  gtk_cell_layout_pack_start (layout,
			      icon_renderer, FALSE);
  gtk_cell_layout_pack_start (layout,
			      text_renderer, TRUE);
  gtk_cell_layout_pack_start (layout,
			      icon2_renderer, FALSE);

  gtk_cell_layout_set_attributes (layout,
				  text_renderer,
				  "markup", TEXT_COLUMN, NULL);

  gtk_cell_layout_set_cell_data_func (layout,
				      icon_renderer,
				      gnomeicu_tree_icon_cell_data_func,
				      NULL,
				      NULL);

  gtk_cell_layout_set_cell_data_func (layout,
				      icon2_renderer,
				      gnomeicu_tree_icon2_cell_data_func,
				      NULL,
				      NULL);
}

void
gnomeicu_tree_create ()
{
  GtkTreeViewColumn *column;
  GtkTreeModel *visiblesorted, *visible;

  /* all contacts treeview/model */
  MainData->contacts_store = gtk_tree_store_new (N_COLUMNS,
                                                 GDK_TYPE_PIXBUF,
                                                 G_TYPE_STRING,
                                                 G_TYPE_POINTER,
                                                 G_TYPE_POINTER,
                                                 G_TYPE_INT,
						 GDK_TYPE_PIXBUF);


  MainData->all_tree = glade_xml_get_widget (MainData->xml, "alltreeview");
  gtk_tree_view_set_model (GTK_TREE_VIEW (MainData->all_tree),
                           GTK_TREE_MODEL (MainData->contacts_store));

  gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (MainData->contacts_store),
                                   TEXT_COLUMN,
                                   tree_sort_compare,
                                   NULL, NULL);
  gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE (MainData->contacts_store),
                                       TEXT_COLUMN,
                                       GTK_SORT_ASCENDING);

 
  column = gtk_tree_view_column_new();
  gtk_tree_view_append_column (GTK_TREE_VIEW(MainData->all_tree), column);

  gnomeicu_tree_setup_cell_layout(GTK_CELL_LAYOUT(column));


  gtk_tree_view_set_expander_column (GTK_TREE_VIEW (MainData->all_tree), column);


  /* online contacts treeview/model */
  
  visible = gtk_tree_model_filter_new (GTK_TREE_MODEL(MainData->contacts_store),
						       NULL);
  gtk_tree_model_filter_set_visible_func(GTK_TREE_MODEL_FILTER(visible),
					 gnomeicu_tree_is_online, NULL, NULL);

 
  MainData->contacts_tree = glade_xml_get_widget (MainData->xml, "maintreeview");
  gtk_tree_view_set_model (GTK_TREE_VIEW (MainData->contacts_tree),
                           visible);

  g_signal_connect(G_OBJECT(visible), "row-has-child-toggled",
		   G_CALLBACK(gnomeicu_tree_expand_row),
		   MainData->contacts_tree);


  column = gtk_tree_view_column_new();
  gtk_tree_view_append_column (GTK_TREE_VIEW(MainData->contacts_tree), column);

  gnomeicu_tree_setup_cell_layout(GTK_CELL_LAYOUT(column));


  gtk_tree_view_set_expander_column (GTK_TREE_VIEW (MainData->contacts_tree),
                                     column);

}

static void
drag_data_get_cb(GtkWidget *widget, GdkDragContext *drag_context,
                 GtkSelectionData *data,
                 guint info, guint time, gpointer user_data)
{
  GtkTreeSelection *sel;
  GtkTreeIter iter;
  GtkTreeModel *model;
  Contact_Member *contact;
  gchar *str;

  sel = gtk_tree_view_get_selection (GTK_TREE_VIEW(widget));

  if (gtk_tree_selection_get_selected (sel, &model, &iter)) {
    gtk_tree_model_get (model, &iter, CONTACT_COLUMN, &contact, -1);

    if (contact && contact->uid) {
      if (info == DND_TARGET_STRING) {
	str = g_strdup_printf ("%s\n%s", contact->uin, contact->nick );
	gtk_selection_data_set (data, data->target, 8, str, strlen (str));
	g_free (str);
      } else if (info == DND_TARGET_CONTACT) {
	gtk_selection_data_set (data, data->target, sizeof(void*), (guchar*)&contact, sizeof(void*));
      }
    } 
  }
}

void
gnomeicu_tree_populate_data (void)
{
  GSList *contact, *list;
  GtkWidget *maintreeview, *alltreeview;

  for (list = Groups; list != NULL; list = list->next)
    gnomeicu_tree_group_add (list->data);

  for(contact = Contacts; contact != NULL; contact = contact->next)
    gnomeicu_tree_user_add(kontakt);

  maintreeview = glade_xml_get_widget(MainData->xml, "maintreeview");
  alltreeview = glade_xml_get_widget(MainData->xml, "alltreeview");
	
  gtk_drag_source_set(GTK_WIDGET(maintreeview), GDK_BUTTON1_MASK,
                      target_table, 2, GDK_ACTION_COPY | GDK_ACTION_MOVE);
  gtk_drag_source_set(GTK_WIDGET(alltreeview), GDK_BUTTON1_MASK,
                      target_table, 2, GDK_ACTION_COPY | GDK_ACTION_MOVE);

  g_signal_connect (G_OBJECT (maintreeview), "drag_data_get",
                    G_CALLBACK (drag_data_get_cb), NULL);

  g_signal_connect (G_OBJECT (alltreeview), "drag_data_get",
                    G_CALLBACK (drag_data_get_cb), NULL);
}

static gboolean
find_iter_for_group (GtkTreeModel *model, gint gid, gint special,
                     GtkTreeIter *iter)
{
  gboolean valid;
  GroupInfo *g;
  gint s;

  GTK_IS_TREE_MODEL (model);
  g_assert (iter != NULL);

  valid = gtk_tree_model_get_iter_first (model, iter);
  while (valid) {
    gtk_tree_model_get (model, iter, GROUP_COLUMN, &g, SPECIAL_COLUMN, &s, -1);
    if (g != NULL && g->gid == gid) {
      return TRUE;
    } else if (special != GROUP_NORMAL && s == special) {
      return TRUE;
    }
    valid = gtk_tree_model_iter_next (model, iter);
  }

  return FALSE;
}

static gboolean
find_iter_for_contact (GtkTreeModel *model, Contact_Member *contact,
                       GtkTreeIter *iter, GtkTreeIter *parent)
{
  gboolean valid;
  Contact_Member *c;

  g_assert (model != NULL);
  g_assert (contact != NULL);
  g_assert (iter != NULL);
  g_assert (parent != NULL);

  valid = gtk_tree_model_iter_children (model, iter, parent);
  while (valid) {
    gtk_tree_model_get (model, iter, CONTACT_COLUMN, &c, -1);
    if (c->uin && contact->uin && strcmp (c->uin, contact->uin) == 0) {
      return TRUE;
    }
    valid = gtk_tree_model_iter_next (model, iter);
  }

  return FALSE;
}

/* 
 * returns iter and group from a Contact_Member *
 */

gboolean
find_iter_for_contact_direct (GtkTreeModel *model, Contact_Member *contact,
			      GtkTreeIter *iter, GtkTreeIter *group)
{
  gboolean valid;

  g_assert (model != NULL);
  g_assert (contact != NULL);
  g_assert (iter != NULL);

  if (find_iter_for_group (model, contact->gid, GROUP_NORMAL, group) && 
      find_iter_for_contact (model, contact, iter, group)) {
    return TRUE;

  } else if (find_iter_for_group (model, 0, GROUP_WAIT_FOR_AUTH, group) &&
	     find_iter_for_contact (model, contact,iter, group)) {
    return TRUE;

  } else if (find_iter_for_group (model, 0, GROUP_NOT_IN_LIST, group) &&
	     find_iter_for_contact (model, contact, iter, group)) {
    return TRUE;
  }

  return FALSE;
}

/*
 * This internal function will add a new group to the tree store.
 * This function will assume there is no such group exists before. Therefore
 * caller may want to use find_iter_for_group() first
 */
static GtkTreeIter
set_group (GtkTreeStore *store, gint gid, gint special)
{
  GtkTreeIter iter;
  gchar *str;

  g_assert (store != NULL);

  if (gid == 0) {
    if (special == GROUP_WAIT_FOR_AUTH) {
      str = g_strdup_printf ("<b>%s</b>", _("Waiting for authorization"));
    } else if (special == GROUP_NOT_IN_LIST) {
      str = g_strdup_printf ("<b>%s</b>", _("Not in list"));
    } else
      g_assert_not_reached ();

    gtk_tree_store_prepend (store, &iter, NULL);
    gtk_tree_store_set (store, &iter,
                        ICON_COLUMN, NULL,
                        TEXT_COLUMN, str,
                        GROUP_COLUMN, NULL,
                        CONTACT_COLUMN, NULL,
                        SPECIAL_COLUMN, special,
			ICON2_COLUMN, NULL,
                        -1);
  } else {
    GSList *ginfo;
    GroupInfo *g;
    gchar *group_name;

    for (ginfo = Groups; ginfo != NULL; ginfo = ginfo->next)
      if (ginfo->data && ((GroupInfo *)ginfo->data)->gid == gid)
        break;

    g = (GroupInfo *)ginfo->data;

    if (g->name)
      group_name = g_markup_escape_text (g->name, strlen(g->name));
    else
      group_name = g_strdup(_("Unknown Group Name"));

    str = g_strdup_printf ("<b>%s</b>", group_name);
    g_free(group_name);
    gtk_tree_store_prepend (store, &iter, NULL);
    gtk_tree_store_set (store, &iter,
                        ICON_COLUMN, NULL,
                        TEXT_COLUMN, str,
                        GROUP_COLUMN, g,
                        CONTACT_COLUMN, NULL,
                        SPECIAL_COLUMN, GROUP_NORMAL,
			ICON2_COLUMN, NULL,
                        -1);
  }
  g_free (str);

  return iter;
}

static void
set_contact (GtkTreeStore *store, GtkTreeIter *parent, Contact_Member *contact)
{
  gchar *str=NULL, *nick=NULL;
  GtkTreeIter child;

  g_assert (store != NULL && parent != NULL && contact != NULL);

  nick = g_markup_escape_text(contact->nick, strlen(contact->nick));

  str = g_strdup_printf("<span foreground=\"%s\">%s</span>",
                        get_foreground_for_status(contact->status),
                        nick);

  g_free(nick);

  gtk_tree_store_prepend (store, &child, parent);
  gtk_tree_store_set (store, &child,
                      ICON_COLUMN, get_pixbuf_for_status(contact->status),
                      TEXT_COLUMN, str,
                      GROUP_COLUMN, NULL,
                      CONTACT_COLUMN, contact,
                      -1);

  if(contact->has_birthday == TRUE) 
    gtk_tree_store_set (store, &child,
			ICON2_COLUMN, icon_birthday_pixbuf,
			-1);
  g_free (str);
}

static void
update_group (GtkTreeStore *store, GtkTreeIter *iter)
{
  GroupInfo *group;
  gint special;
  gchar *str, *str2;
  int all, online;

  g_assert (store != NULL && iter != NULL);

  gtk_tree_model_get (GTK_TREE_MODEL (store), iter,
                      GROUP_COLUMN, &group,
                      SPECIAL_COLUMN, &special,
                      -1);
  if (special == GROUP_NORMAL) {
    str2 = g_markup_escape_text(group->name, strlen(group->name));

    gnomeicu_tree_count_children(GTK_TREE_MODEL(store), iter, &all, &online);

    str = g_strdup_printf ("<b>%s</b> (%d/%d)",
                           str2, online, all);
    g_free(str2);
    gtk_tree_store_set (store, iter, TEXT_COLUMN, str, -1);
    g_free (str);

  } else if (special == GROUP_WAIT_FOR_AUTH || special == GROUP_NOT_IN_LIST) {

    if (!gtk_tree_model_iter_has_child(GTK_TREE_MODEL(store), iter)) {
      gtk_tree_store_remove (store, iter);
    } else {
      GtkTreePath *path = gtk_tree_model_get_path(GTK_TREE_MODEL(store), iter);
      gtk_tree_model_row_changed (GTK_TREE_MODEL(store), path, iter);
      gtk_tree_path_free(path);
    }
  } else
    g_assert_not_reached ();

}

static void
update_contact (GtkTreeStore *store, GtkTreeIter *iter)
{
  Contact_Member *contact;
  gchar *str=NULL, *nick=NULL;

  g_assert (store != NULL && iter != NULL);

  gtk_tree_model_get (GTK_TREE_MODEL (store), iter,
                      CONTACT_COLUMN, &contact, -1);

  nick = g_markup_escape_text(contact->nick, strlen(contact->nick));

  str = g_strdup_printf("<span foreground=\"%s\">%s</span>",
                        get_foreground_for_status(contact->status),
                        nick);
  gtk_tree_store_set (store, iter,
                      ICON_COLUMN, get_pixbuf_for_status (contact->status),
                      TEXT_COLUMN, str,
                      -1);

  if(contact->has_birthday == TRUE)
    gtk_tree_store_set (store, iter,
			ICON2_COLUMN, icon_birthday_pixbuf,
			-1);
  g_free(nick);
  g_free (str);
}

void
gnomeicu_tree_user_update(Contact_Member *contact)
{
  GtkTreeIter iter, piter;
  GroupInfo *group;
  int special;

  if (find_iter_for_contact_direct(GTK_TREE_MODEL(MainData->contacts_store),
				   contact, &iter, &piter)) {
    gtk_tree_model_get (GTK_TREE_MODEL(MainData->contacts_store), &piter,
			GROUP_COLUMN,  &group,
			SPECIAL_COLUMN, &special,
			-1);

    if ((!contact->wait_auth != !(special == GROUP_WAIT_FOR_AUTH)) ||
	((!contact->inlist  && contact->gid == 0 && contact->uid != 0) &&
	 special != GROUP_NOT_IN_LIST) ||
	(group && contact->gid != group->gid)) {
      /* Its not in the right group, lets move it */
      g_print("change group for %s\n", contact->nick);
      gtk_tree_store_remove (MainData->contacts_store, &iter);
      gnomeicu_tree_user_add (contact);

      detach_setrow(contact, &iter);
    } else {
      g_print("DONT change group for %s contactgid %x treegid %x wait:%d inl:%d\n", 
	      contact->nick,
	      contact->gid, group ? group->gid : 0, contact->wait_auth, contact->inlist);
      update_contact (MainData->contacts_store, &iter);
      update_group (MainData->contacts_store, &piter);
    }
  }
}


/*
 * Remove a user completely from the tree models (both online and all contacts)
 */
void
gnomeicu_tree_user_remove (Contact_Member *contact)
{
  GtkTreeIter piter, iter;
  GroupInfo *g;

  g_assert (contact != NULL);

 if (find_iter_for_contact_direct(GTK_TREE_MODEL(MainData->contacts_store),
				  contact, &iter, &piter)) {
   gtk_tree_store_remove (MainData->contacts_store, &iter);
 }

 update_group(MainData->contacts_store, &piter);

}

void
gnomeicu_tree_user_add (Contact_Member *contact)
{
  GroupInfo *g;
  GtkTreeIter p_iter, c_iter;

  if (!contact->nick || !g_utf8_validate (contact->nick, -1, NULL)) {
    g_free(contact->nick);
    contact->nick = g_strdup(contact->uin);
  }

  if (contact->wait_auth) {
    /* Wait for auth group */
    if (!find_iter_for_group (GTK_TREE_MODEL (MainData->contacts_store),
                             0, GROUP_WAIT_FOR_AUTH, &p_iter)) {
      p_iter = set_group (MainData->contacts_store, 0, GROUP_WAIT_FOR_AUTH);
    }
    if (!find_iter_for_contact (GTK_TREE_MODEL (MainData->contacts_store),
                               contact, &c_iter, &p_iter))
      set_contact (MainData->contacts_store, &p_iter, contact);
  } else if (!contact->inlist && contact->gid == 0 && contact->uid != 0) {
    /* Not in list group */
    if (!find_iter_for_group (GTK_TREE_MODEL (MainData->contacts_store),
                              0, GROUP_NOT_IN_LIST, &p_iter)) {
      p_iter = set_group (MainData->contacts_store, 0, GROUP_NOT_IN_LIST);
    }
    if (!find_iter_for_contact (GTK_TREE_MODEL (MainData->contacts_store),
                               contact, &c_iter, &p_iter))
      set_contact (MainData->contacts_store, &p_iter, contact);
  } else if (contact->gid != 0 &&
             find_iter_for_group (GTK_TREE_MODEL (MainData->contacts_store),
                                  contact->gid, GROUP_NORMAL, &p_iter)) {
    /* normal contact in normal group */

    /* look for existing contact */
    if (find_iter_for_contact (GTK_TREE_MODEL (MainData->contacts_store),
                               contact, &c_iter, &p_iter)) {
      gnomeicu_tree_user_update(contact);
      return;
    }

    gtk_tree_model_get (GTK_TREE_MODEL (MainData->contacts_store), &p_iter,
                        GROUP_COLUMN, &g,
                        -1);
    set_contact (MainData->contacts_store, &p_iter, contact);
    update_group (MainData->contacts_store, &p_iter);
  }
}



/*
 * This function basically is for flashing icon on contacts
 */
void
gnomeicu_tree_set_contact_icon (Contact_Member *contact, GdkPixbuf *pixbuf)
{
  GtkTreeIter p_iter, iter;

  g_assert (contact != NULL);
  g_assert (pixbuf != NULL);

  
  if (find_iter_for_contact_direct(GTK_TREE_MODEL(MainData->contacts_store),
				   contact, &iter, &p_iter)) {
    gtk_tree_store_set (MainData->contacts_store, &iter,
			ICON_COLUMN, pixbuf,
			-1);
  }
}


void gnomeicu_tree_refresh_visuals (void)
{
  Contact_Member *contact;
  GtkTreeIter p_iter, c_iter;
  gboolean valid;
  gchar *str;

  /* for the All contacts list */
  valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(MainData->contacts_store),
                                        &p_iter);
  while (valid) {
    valid =gtk_tree_model_iter_children(GTK_TREE_MODEL(MainData->contacts_store),
                                        &c_iter, &p_iter);
    while (valid) {
      gtk_tree_model_get (GTK_TREE_MODEL (MainData->contacts_store), &c_iter,
			  CONTACT_COLUMN, &contact, -1);
      if (contact) {
	str = g_strdup_printf("<span foreground=\"%s\">%s</span>",
			      get_foreground_for_status (contact->status),
			      contact->nick);
	gtk_tree_store_set (MainData->contacts_store, &c_iter,
			    ICON_COLUMN, get_pixbuf_for_status (contact->status),
			    TEXT_COLUMN, str,
			    -1);
	g_free (str);
      }
      valid = gtk_tree_model_iter_next (GTK_TREE_MODEL(MainData->contacts_store),
                                        &c_iter);
    }
    valid = gtk_tree_model_iter_next (GTK_TREE_MODEL (MainData->contacts_store),
                                      &p_iter);
  }
}

void
gnomeicu_tree_group_add (const GroupInfo *ginfo)
{
  GtkTreeIter iter;

  g_assert (ginfo != NULL);

  if (!find_iter_for_group (GTK_TREE_MODEL(MainData->contacts_store),
                            ginfo->gid, GROUP_NORMAL, &iter))
    set_group (MainData->contacts_store, ginfo->gid, GROUP_NORMAL);
}

void
gnomeicu_tree_group_rename (const GroupInfo *ginfo)
{
  GtkTreeIter iter;

  if (!find_iter_for_group (GTK_TREE_MODEL (MainData->contacts_store),
                            ginfo->gid, GROUP_NORMAL, &iter))
    set_group (MainData->contacts_store, ginfo->gid, GROUP_NORMAL);
  else {
    update_group (MainData->contacts_store, &iter);
  }
}

void
gnomeicu_tree_group_remove (const GroupInfo *ginfo)
{
  GtkTreeIter iter;

  if (find_iter_for_group (GTK_TREE_MODEL (MainData->contacts_store),
                           ginfo->gid, GROUP_NORMAL, &iter))
    gtk_tree_store_remove (MainData->contacts_store, &iter);

}

static gint
tree_sort_compare (GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b,
                   gpointer user_data)
{
  Contact_Member *contacta, *contactb;
  gchar *texta=NULL, *textb=NULL;
  gint s1, s2;
  int result = 0;

  gtk_tree_model_get(model, a,
		     CONTACT_COLUMN, &contacta, 
		     TEXT_COLUMN, &texta, 
		     SPECIAL_COLUMN, &s1,
		     -1); 
  gtk_tree_model_get(model, b,
		     CONTACT_COLUMN, &contactb, 
		     TEXT_COLUMN, &textb, 
		     SPECIAL_COLUMN, &s2,
		     -1);



  if (s1 == GROUP_NOT_IN_LIST && s2 == GROUP_WAIT_FOR_AUTH) {
    result = -1;
  } else if (s1 == GROUP_WAIT_FOR_AUTH && s2 == GROUP_NOT_IN_LIST) {
    result = 1;
  /* either s1 or s2 will be GROUP_NORMAL in the cases below */
  } else if (s1 == GROUP_NOT_IN_LIST || s1 == GROUP_WAIT_FOR_AUTH) {
    result = 1;
  } else if (s2 == GROUP_NOT_IN_LIST || s2 == GROUP_WAIT_FOR_AUTH) {
    result = -1;
  } else if (!contacta || !contactb) {
    result = strcoll (texta, textb);
  } else {
    gboolean offlinea = (contacta->status == STATUS_OFFLINE);
    gboolean offlineb = (contactb->status == STATUS_OFFLINE);

    if (offlinea && !offlineb) {
      result = 1;
    } else if (!offlinea && offlineb) {
      result= -1;
    } else if (!contacta->nick) {
      result = 1;
    } else if (!contactb->nick) {
      return -1;
    } else {
      result = strcoll(contacta->nick, contactb->nick);
    }

  }

  g_free(texta);
  g_free(textb);

  return result;
}

static void
launch_context_menu (GtkTreeView *widget, GdkEventButton *ev)
{
  GtkTreeSelection *sel;
  GtkTreeIter iter;
  GtkTreeModel *model;
  GroupInfo *group;
  Contact_Member *contact;
  gboolean cont;

  sel = gtk_tree_view_get_selection (widget);
  cont = gtk_tree_selection_get_selected (sel, &model, &iter);

  if (ev) {
    GtkTreePath *path;

    gtk_tree_view_get_path_at_pos (widget, ev->x, ev->y, &path,
                                   NULL, NULL, NULL);
    if (!path)
      return;
    else {
      cont = gtk_tree_model_get_iter (model, &iter, path);
      gtk_tree_path_free (path);
    }
  }

  if (cont) {
    gtk_tree_model_get (model, &iter,
                        GROUP_COLUMN, &group,
                        CONTACT_COLUMN, &contact,
                        -1);

    if (contact) {
      /* pop up the user menu */
      GtkWidget *personal_menu;

      personal_menu = user_popup (contact);
      gtk_menu_popup (GTK_MENU (personal_menu), NULL, NULL, NULL, NULL,
                      ev != NULL ? ev->button : 3,
                      ev != NULL ? ev->time : gtk_get_current_event_time ());
    } else if (group && group->gid != 0 &&
               strcmp (gtk_widget_get_name(GTK_WIDGET(widget)),"maintreeview")) {
      /* pop up the group menu */
      GtkWidget *group_menu;

      group_menu = group_popup (group->gid);
      gtk_menu_popup (GTK_MENU (group_menu), NULL, NULL, NULL, NULL,
                      ev != NULL ? ev->button : 3,
                      ev != NULL ? ev->time : gtk_get_current_event_time ());
    }
  }
}

static gboolean search_nick_timeout(GString *search_nick)
{
	/* search_nick can't be NULL here because it is created before we add
	   the first timeout */
	g_string_erase(search_nick, 0, -1);
	return FALSE;
}

static void search_nick_in_treeview(GtkTreeView *widget, GdkEventKey *ev,
				    GString *search_nick, guint *timeout_source)
{
	GtkTreeModel *tree_model;
	GtkTreeIter iter, child;
	GtkTreePath *tree_path;
	gboolean valid;
	Contact_Member *contact;
	gchar *nick_collated, *search_collated;
	gboolean child_valid = FALSE;

	/* we cancel the current timeout if it exists */
	if (*timeout_source) {
		g_source_remove(*timeout_source);
		*timeout_source = 0;
	}

	g_string_append_unichar(search_nick, gdk_keyval_to_unicode(ev->keyval));
	search_collated = g_utf8_collate_key(search_nick->str, search_nick->len);

	tree_model = gtk_tree_view_get_model(widget);

	gtk_tree_view_get_cursor(widget, &tree_path, NULL);
	if (tree_path == NULL) {
		/* there is no selection => we take the first thing */
		valid = gtk_tree_model_get_iter_first(tree_model, &iter);
		child_valid = FALSE;
	} else {
		/* there's already a selection => we'll start there */
		gtk_tree_model_get_iter(tree_model, &child, tree_path);
		if (!gtk_tree_model_iter_parent(tree_model, &iter, &child)) {
			/* child has no parent
			   => it's a group, and not a contact */
			iter = child;
			child_valid = FALSE;
		} else {
			/* we have the child where we'll start */
			child_valid = TRUE;
		}
		/* we have a current iter, so we know it's valid */
		valid = TRUE;
	}
	gtk_tree_path_free(tree_path);

	/* now we loop on the treeview to find the first nickname that
	   could match */
	while (valid) {
		if (child_valid) {
			/* we already have a valid child, so we don't
			   start at the beginning */
			child_valid = FALSE;
			valid = TRUE;
		} else {
			valid = gtk_tree_model_iter_children(tree_model, &child, &iter);
		}
		while (valid) {
			gtk_tree_model_get(tree_model, &child, CONTACT_COLUMN, &contact, -1);

			nick_collated = g_utf8_collate_key(contact->nick, -1);
			if (!g_ascii_strncasecmp(nick_collated, search_collated, search_nick->len)) {
				/* this nickname matches the search
				   => we select the contact */
				tree_path = gtk_tree_model_get_path(tree_model, &child);
				gtk_tree_view_expand_to_path(widget, tree_path);
				gtk_tree_view_set_cursor(widget, tree_path, NULL, FALSE);
				gtk_tree_path_free(tree_path);
				g_free(nick_collated);
				break;
			}

			g_free(nick_collated);
			valid = gtk_tree_model_iter_next(tree_model, &child);
		}

		if (valid) /* we have found someone => stop */
			break;
		valid = gtk_tree_model_iter_next(tree_model, &iter);
	}

	g_free(search_collated);

	/* this will erase search_nick 1 second later if no other
	   useful key is pressed */
	*timeout_source = g_timeout_add(1000, (GSourceFunc) search_nick_timeout, search_nick);
}

void
gnomeicu_tree_activated_cb (GtkTreeView *treeview, GtkTreePath *path,
                            GtkTreeViewColumn *arg2, gpointer user_data)
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  GroupInfo *group;
  Contact_Member *contact;
  gint special;

  model = gtk_tree_view_get_model (treeview);
  gtk_tree_model_get_iter (model, &iter, path);

  gtk_tree_model_get (model, &iter,
                      GROUP_COLUMN, &group,
                      CONTACT_COLUMN, &contact,
                      SPECIAL_COLUMN, &special,
                      -1);

  if (contact) {
    if (g_slist_length (contact->stored_messages))
      show_contact_message (contact);
    else
      if (contact->msg_dlg_xml != NULL) {
        gtk_widget_show_all(gtk_bin_get_child(GTK_BIN(glade_xml_get_widget(contact->msg_dlg_xml, "message_dialog"))));
        gtk_widget_grab_focus (glade_xml_get_widget (contact->msg_dlg_xml,
        "input"));
        gtk_window_present(GTK_WINDOW(glade_xml_get_widget (contact->msg_dlg_xml,
                                                            "message_dialog")));
      } else
        open_message_dlg_with_message (contact, NULL);
  } else if (group || special == GROUP_WAIT_FOR_AUTH ||
             special == GROUP_NOT_IN_LIST) {
    if (gtk_tree_view_row_expanded (treeview, path))
      gtk_tree_view_collapse_row(treeview, path);
    else
      gtk_tree_view_expand_row(treeview, path, TRUE);
  }
}

gint
gnomeicu_tree_key_cb (GtkTreeView *widget, GdkEventKey *ev, gpointer data)
{
  static GString *search_nick = NULL;
  static guint timeout_source = 0;

  if (ev->type == GDK_KEY_PRESS && ev->keyval == GDK_Return) {
    return FALSE;
  } else if (ev->type == GDK_KEY_PRESS && gdk_keyval_to_unicode(ev->keyval) != 0) {
    if (search_nick == NULL) {
      search_nick = g_string_sized_new(12);
    }
    /* we search the nickname in the treeview */
    search_nick_in_treeview(widget, ev, search_nick, &timeout_source);
  } else if (ev->type == GDK_KEY_PRESS) {
    /* the key is not a valid character
       => we'll start a new search next time */
    if (search_nick != NULL) {
      g_string_erase(search_nick, 0, -1);
    }

    /* we cancel the current timeout if it exists */
    if (timeout_source) {
      g_source_remove(timeout_source);
      timeout_source = 0;
    }
  }

  return FALSE;
}

gint
gnomeicu_tree_popup_cb (GtkTreeView *widget, gpointer data)
{
  launch_context_menu(widget, NULL);

  return FALSE;
}

gint
gnomeicu_tree_button_cb (GtkTreeView *widget, GdkEventButton *ev, gpointer data)
{
  if (ev->type == GDK_BUTTON_PRESS && ev->button == 3) {
    launch_context_menu(widget, ev);
  }

  return FALSE;
}
