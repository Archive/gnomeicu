/****************************
  Basic code for ICQ v7-8 protocol (Oscar)
  Olivier Crete (c) 2001
  GnomeICU
*****************************/

#include "common.h"
#include "util.h"
#include "v7snac13.h"
#include "gnomeicu.h"

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>

#include <gtk/gtkmain.h>

#define REG_COL       "\x1B[0m"


static void        packet_print_v7 (const V7Packet *packet);
static gboolean    check_rate(V7Connection *conn, WORD family, WORD subfamily);
static gboolean    rate_limited_resend(RateLimitedResendData *data);

V7Connection *mainconnection = NULL;
GSList       *requests       = NULL;
GSList       *otherconnections = NULL;

gboolean flap_send(V7Connection *conn, BYTE flap_channel, const gchar *data,
                   WORD len)
{
  guchar buffer[6];

#ifdef TRACE_FUNCTION
  g_print( "flap_send\n" );
#endif
  
  if (!conn || conn->status == NOT_CONNECTED || conn->status==BROKEN)
    return FALSE;

  buffer[0] = 0x2a;
  buffer[1] = flap_channel;
  Word_2_CharsBE(buffer+2, conn->sequence++);
  Word_2_CharsBE(buffer+4, len);


  if (write(conn->fd, buffer, 6) < 0)
    return FALSE;

  if (preferences_get_bool (PREFS_ICQ_DEBUG_SNAC))
    print_data(data, len);

  if (len)
    if (write(conn->fd, data, len) < 0)
      return FALSE;
  
  return TRUE;
}

DWORD snac_send(V7Connection *conn, const gchar *data, WORD len, 
                   WORD family, WORD subtype, BYTE flags[2], DWORD reqid)
{
  gchar *buffer;

#ifdef TRACE_FUNCTION
  g_print( "snac_send\n" );
#endif

  if (!conn || conn->status == NOT_CONNECTED || conn->status==BROKEN)
    return 0;

  if (reqid == ANY_ID)
    reqid = conn->last_reqid++;

  buffer = g_malloc0(len+10);

  Word_2_CharsBE(buffer, family);
  Word_2_CharsBE(buffer+2, subtype);
  if (flags) {
    buffer[4] = flags[0];
    buffer[5] = flags[1];
  } else 
    buffer[4] = buffer[5] = 0;
  DW_2_CharsBE(buffer+6, reqid);
  
  g_memmove(buffer+10, data, len);

  if (check_rate(conn, family, subtype)) {

    if (! flap_send(conn, CHANNEL_SNAC, buffer, len+10)) {
      g_free(buffer);
      return 0;
    }
    
    g_free(buffer);
  } else  {
    RateLimitedResendData* dat = g_new0(RateLimitedResendData,1);
    dat->conn = conn;
    dat->buffer = buffer;
    dat->length = len+10;
    dat->family = family;
    dat->subfamily = subtype;
    dat->timerid = g_timeout_add(1000, (GSourceFunc)rate_limited_resend, dat);
    conn->queued_packets = g_list_append(conn->queued_packets, dat);
  }

  return reqid;


}


V7Packet *read_flap(V7Connection *conn)
{
  guint nr;

#ifdef TRACE_FUNCTION
  g_print( "read_flap\n" );
#endif

  if (!conn)
    return NULL;
  if (conn->latest_packet == NULL) {

    if ((nr = read(conn->fd,
		   conn->latest_head + conn->bytes_read_in_latest,
		   6-conn->bytes_read_in_latest)) == -1)
	  return NULL;

    conn->bytes_read_in_latest += nr;
    conn->total_read +=nr;
    
    /* If we have the whole header */
    if (conn->bytes_read_in_latest == 6) {
      if (conn->latest_head[0] != 0x2A) {
	g_error("Error reading packet, the first byte of a FLAP is 0x%x, "
		"it should be 0x2A\n", conn->latest_packet[0]);
      }
      conn->latest_packet = g_new0(V7Packet, 1);
      conn->latest_packet->conn = conn;
      conn->latest_packet->channel = conn->latest_head[1];
      /* sequence number not read */
      conn->latest_packet->len = CharsBE_2_Word (conn->latest_head+4);
      

      conn->latest_packet->buf = v7_buffer_new(
          g_malloc(conn->latest_packet->len), 
          conn->latest_packet->len, 
          TRUE);
      
      conn->latest_head[0] = 0;
      conn->bytes_read_in_latest = 0;
    } else
      return NULL;
  }

  if ((nr = read(conn->fd, 
		 conn->latest_packet->buf->here + 
		 conn->bytes_read_in_latest,
		 conn->latest_packet->len - 
		 conn->bytes_read_in_latest)) < 0) 
    return NULL;
  
  conn->bytes_read_in_latest += nr;
  conn->total_read +=nr;

  if (conn->bytes_read_in_latest == conn->latest_packet->len) {
    V7Packet *packet;
    packet = conn->latest_packet;
    conn->latest_packet = NULL;
    conn->bytes_read_in_latest = 0;
    if (preferences_get_bool (PREFS_ICQ_DEBUG_SNAC))
      packet_print_v7(packet);
    return packet;
  }
  
  return NULL;
  
}

void free_flap (V7Packet *packet)
{
  v7_buffer_free (packet->buf);
  g_free (packet);
}

Snac *read_snac(const V7Packet *packet)
{
  Snac *snac = NULL;

  g_assert(packet != NULL);

  snac = g_new0(Snac, 1);

  snac->family = v7_buffer_get_w_be (packet->buf, error_out);
  snac->type   = v7_buffer_get_w_be (packet->buf, error_out);
  snac->flags[0] = v7_buffer_get_c (packet->buf, error_out);
  snac->flags[1]  = v7_buffer_get_c (packet->buf, error_out);
  snac->reqid  = v7_buffer_get_dw_be (packet->buf, error_out);
  snac->datalen = packet->len;
  snac->buf = packet->buf;
  snac->packet = packet;

  return snac;

 error_out:
  g_free (snac);
  return NULL;
}

void free_snac (Snac *snac)
{
  g_assert (snac);

  g_free (snac);
}

TLV *_read_tlv (V7Buffer *buf)
{
  TLV *tlv = NULL;

  g_assert (buf);

  tlv = g_new0(TLV, 1);

  tlv->type = v7_buffer_get_w_be (buf, out_err);
  tlv->len = v7_buffer_get_w_be (buf, out_err);
  tlv->buf = v7_buffer_get_v7buf(buf, tlv->len, out_err);

  if (preferences_get_bool (PREFS_ICQ_DEBUG_SNAC))
    g_print("newtlv type:0x%x len: %d\n", tlv->type, tlv->len);

  return tlv;

 out_err:
  g_free (tlv);
  return NULL;
}


void free_tlv (TLV *tlv) 
{
  if (!tlv)
    return;
  
  v7_buffer_free (tlv->buf);
  g_free(tlv);
}



void add_tlv_w_be(TLVstack *tlvs, WORD type, WORD data)
{
  gchar buffer[2];
  
#ifdef TRACE_FUNCTION
  g_print( "add_tlv_w_be\n" );
#endif

  g_assert(tlvs != NULL);
  Word_2_CharsBE(buffer, data);
  add_tlv (tlvs, type, buffer, 2);
}

void add_tlv_w_le(TLVstack *tlvs, WORD type, WORD data)
{
  gchar buffer[2];
  
#ifdef TRACE_FUNCTION
  g_print( "add_tlv_w_le\n" );
#endif

  g_assert(tlvs != NULL);
  Word_2_Chars(buffer, data);
  add_tlv (tlvs, type, buffer, 2);
}

void add_tlv_dw_be(TLVstack *tlvs, WORD type, DWORD data)
{
  gchar buffer[4];

#ifdef TRACE_FUNCTION
  g_print( "add_tlv_dw_be\n" );
#endif
  
  g_assert(tlvs != NULL);
  DW_2_CharsBE(buffer, data);
  add_tlv (tlvs, type, buffer, 4);
}


void add_tlv_dw_le(TLVstack *tlvs, WORD type, DWORD data)
{
  gchar buffer[4];

#ifdef TRACE_FUNCTION
  g_print( "add_tlv_dw_le\n" );
#endif
  
  g_assert(tlvs != NULL);
  DW_2_Chars(buffer, data);
  add_tlv (tlvs, type, buffer, 4);
}


void add_tlv (TLVstack *tlvs, WORD type, const gchar *data, WORD len)
{

#ifdef TRACE_FUNCTION
  g_print( "add_tlv\n" );
#endif

  g_assert(tlvs != NULL);

  if(tlvs->beg == NULL)
    tlvs->beg = g_malloc0(len + 4);
  else
    tlvs->beg = g_realloc(tlvs->beg, tlvs->len + len + 4);
  tlvs->end = tlvs->beg + tlvs->len;

  Word_2_CharsBE(tlvs->end, type);
  Word_2_CharsBE(tlvs->end+2, len);
  g_memmove(tlvs->end+4, data, len);

  tlvs->len += len + 4;
  tlvs->end = tlvs->beg + tlvs->len;
}

void add_nontlv_w_be(TLVstack *tlvs, WORD data)
{
  gchar buffer[2];
  
#ifdef TRACE_FUNCTION
  g_print( "add_nontlv_w\n" );
#endif

  g_assert(tlvs != NULL);
  Word_2_CharsBE(buffer, data);
  add_nontlv (tlvs, buffer, 2);
}

void add_nontlv_w_le(TLVstack *tlvs, WORD data)
{
  gchar buffer[2];
  
#ifdef TRACE_FUNCTION
  g_print( "add_nontlv_w\n" );
#endif

  g_assert(tlvs != NULL);
  Word_2_Chars(buffer, data);
  add_nontlv (tlvs, buffer, 2);
}

void add_nontlv_dw_be(TLVstack *tlvs, DWORD data)
{
  gchar buffer[4];

#ifdef TRACE_FUNCTION
  g_print( "add_nontlv_dw_be\n" );
#endif
  
  g_assert(tlvs != NULL);
  DW_2_CharsBE(buffer, data);
  add_nontlv (tlvs, buffer, 4);
}

void add_nontlv_dw_le(TLVstack *tlvs, DWORD data)
{
  gchar buffer[4];

#ifdef TRACE_FUNCTION
  g_print( "add_nontlv_dw_le\n" );
#endif
  
  g_assert(tlvs != NULL);
  DW_2_Chars(buffer, data);
  add_nontlv (tlvs, buffer, 4);
}

/* add LNTS type (little endian) string to tlvs, null terminated */
void add_nontlv_lnts (TLVstack *tlvs, const gchar *string)
{

#ifdef TRACE_FUNCTION
  g_print( "add_nontlv_lnts\n" );
#endif

  g_assert(tlvs != NULL);
  if (string) {
    add_nontlv_w_le (tlvs, strlen(string)+1);
    add_nontlv (tlvs, string, strlen(string)+1);
  } else
    add_nontlv_w_le (tlvs, 0);
}

/* add BWS type (big endian) string to tlvs, excluding null */
void add_nontlv_bws (TLVstack *tlvs, const gchar *string)
{

#ifdef TRACE_FUNCTION
  g_print( "add_nontlv_bws\n" );
#endif

  g_assert(tlvs != NULL);
  if (string) {
    add_nontlv_w_be (tlvs, strlen(string));
    add_nontlv (tlvs, string, strlen(string));
  } else
    add_nontlv_w_be (tlvs, 0);
}


void add_nontlv (TLVstack *tlvs, const gchar *data, WORD len)
{

#ifdef TRACE_FUNCTION
  g_print( "add_nontlv\n" );
#endif

  g_assert(tlvs != NULL);
  if(tlvs->beg == NULL)
    tlvs->beg = g_malloc0(len);
  else
    tlvs->beg = g_realloc(tlvs->beg, tlvs->len + len);
  tlvs->end = tlvs->beg + tlvs->len;

  g_memmove(tlvs->end, data, len);
  
  tlvs->len += len;
  tlvs->end = tlvs->beg + tlvs->len;
  
}



TLVstack *new_tlvstack (const gchar *initdata, guint init_len)
{
  
  TLVstack *tlvs;

#ifdef TRACE_FUNCTION
  g_print( "new_tlvstack\n" );
#endif

  tlvs = g_new0(TLVstack, 1);
  
  if (initdata) {
    tlvs->len = init_len;
    tlvs->beg = g_malloc(init_len);
    g_memmove(tlvs->beg, initdata, init_len);
    tlvs->end = tlvs->beg + init_len;
  } 

  return tlvs;
}


void free_tlvstack (TLVstack *tlvs)
{

#ifdef TRACE_FUNCTION
  g_print( "free_tlvstack\n" );
#endif
  
  if (tlvs != NULL) {
    g_free(tlvs->beg);
    g_free(tlvs);
  }

}

void display_tlv(const TLV *tlv)
{

#ifdef TRACE_FUNCTION
  g_print( "display_tlv\n" );
#endif

  g_assert(tlv != NULL);
  g_print ("T: %d L: %d V(w:%d dw:%d): %s\n",
             tlv->type, tlv->len,
             (tlv->len==2) ? CharsBE_2_Word(tlv->buf->here) : 0,
             (tlv->len==4) ? CharsBE_2_DW(tlv->buf->here)   : 0,
             tlv->buf->here);

}

static void packet_print_v7 (const V7Packet *packet)
{
  V7Buffer buf;

#ifdef TRACE_FUNCTION
  g_print( "packet_print_v7\n" );
#endif

  g_assert(packet != NULL);
  g_print("Channel: %d Length: %d\n", packet->channel, packet->len);
  if (packet->channel == 2) {
    Snac *snac;

    memcpy(&buf, packet->buf, sizeof(V7Buffer));

    snac = read_snac(packet);

    memcpy(packet->buf, &buf, sizeof(V7Buffer));

    packet_print_v7_snac(snac);

    g_free(snac);
  }  else
    print_data(packet->buf->buf, packet->len);

}

void packet_print_v7_snac (const Snac *snac) 
{

#ifdef TRACE_FUNCTION
  g_print( "packet_print_v7_snac\n" );
#endif

  g_assert(snac != NULL);
  g_print("Fam:0x%x Type:0x%x Flags:0x%x%x ReqID:0x%x\n", snac->family, snac->type,
          snac->flags[0], snac->flags[1], snac->reqid);
  
  print_data(snac->buf->here, snac->datalen);

}

void print_data(const BYTE *data, guint len)
{
  int cx;
  gchar condensed[17];
  condensed[0] = condensed[16] = 0x00;

  /*  return; */

  g_assert(data != NULL);
  g_print("Length: %d\n", len);

  for( cx = 0; cx < len + (cx%16); cx ++ ) {
    if (cx < len) {
      if( isprint( data[cx] ) )
        strncat( condensed, &data[cx], 1 );
      else
        strcat( condensed, "." );
    } else
      strcat (condensed, " ");

    if (cx < len) {
      if( cx % 16 == 4 || cx % 16 == 8 || cx % 16 == 12 )
        g_print( "- " );
      g_print("%02x ", data[cx]);
    } else {
      if( cx % 16 == 4 || cx % 16 == 8 || cx % 16 == 12 )
        g_print( "  " );
      g_print("   ");
    }
    
    if( cx % 16 == 15 ) {
      printf( "    %s\n", condensed );
      condensed[0] = '\x0';
    }
  }

  g_print("\n"); 
}


gint v7_reqs_compare_func(Request *req, DWORD *reqid)
{

  g_assert(req != NULL && reqid != NULL);
  if (req->reqid == *reqid)
    return 0;
  else
    return 1;
}


gboolean check_rate(V7Connection *conn, WORD family, WORD subfamily)
{
  V7RateClass *class;
  DWORD familyandsub = family << 16 | subfamily;
  DWORD currenttimediff, currenttime;
  struct timeval tv;
  gettimeofday(&tv, NULL);
  currenttime = tv.tv_sec*1000 + tv.tv_usec/1000;

  /* Levels:
     1: limited
     2: alert
     3: clear
  */

  if (!conn->familyclass)
    return TRUE;
  
  class = g_hash_table_lookup(conn->familyclass, &familyandsub);

  if (!class)
    return TRUE;

  currenttimediff = currenttime - class->lasttime;
  if (currenttimediff > 10000)
    currenttimediff = 10000;

  /*  g_print("c:%u = (((w:%u -1 )/w:%u)*c:%u)+(d:%u/w:%u)\n",
	  (((class->windowsize-1)/class->windowsize) * 
	   class->currentlevel) + (currenttimediff/class->windowsize),
	  class->windowsize,class->windowsize, class->currentlevel, 
	  currenttimediff, class->windowsize); */
  class->currentlevel = (((class->windowsize-1.0)/class->windowsize) * 
			 class->currentlevel) + (currenttimediff/class->windowsize);
  
  /*  g_print("Class: %u lasttime: %u now: %u diff: %u Current Level: %u Max %u State: %u Clear: %u Limit: %u Alert: %u\n",
	  class->id, class->lasttime, currenttime,currenttimediff ,class->currentlevel, class->maxlevel, class->state,
	  class->clearlevel, class->limitlevel, class->alertlevel); */

  if (class->currentlevel > class->maxlevel)
    class->currentlevel = class->maxlevel;
    
  if (class->state == 1) {
    if (class->currentlevel > class->clearlevel)
      class->state = 3;
  } else {
    if (class->currentlevel < class->limitlevel)
      class->state = 1;
    else if (class->currentlevel < class->alertlevel)
      class->state = 2;
    else
      class->state = 3;
  }


  if (class->state == 1)
    return FALSE;
  else {
    class->lasttime = currenttime;
    return TRUE;
  }
  
}


gboolean rate_limited_resend(RateLimitedResendData *data)
{

  if (check_rate(data->conn, data->family, data->subfamily)) {

    flap_send(data->conn, CHANNEL_SNAC, data->buffer, data->length);
    
    data->conn->queued_packets = g_list_remove(data->conn->queued_packets,
					       data);
    g_free(data->buffer);
    g_free(data);
    return FALSE;
  } else 
    return TRUE;

}

char *v7_get_error_message(WORD error)
{
  switch (error) {
  case 0x01:
    return _("Invalid SNAC header.");
  case 0x02:
    return _("Server rate limit exceeded");
  case 0x03:
    return _("Client rate limit exceeded");
  case 0x04:
    return _("Recipient is not logged in");
  case 0x05:
    return _("Requested service unavailable");
  case 0x06:
    return _("Requested service not defined");
  case 0x07:
    return _("You sent obsolete SNAC");
  case 0x08:
    return _("Not supported by server");
  case 0x09:
    return _("Not supported by client");
  case 0x0A:
    return _("Refused by client");
  case 0x0B:
    return _("Reply too big");
  case 0x0C:
    return _("Responses lost");
  case 0x0D:
    return _("Request denied");
  case 0x0E:
    return _("Incorrect SNAC format");
  case 0x0F:
    return _("Insufficient rights");
  case 0x10:
    return _("In local permit/deny (recipient blocked)");
  case 0x11:
    return _("Sender too evil");
  case 0x12:
    return _("Receiver too evil");
  case 0x13:
    return _("User temporarily unavailable");
  case 0x14:
    return _("No match");
  case 0x15:
    return _("List overflow");
  case 0x16:
    return _("Request ambiguous");
  case 0x17:
    return _("Server queue full");
  case 0x18:
    return _("Not while on AOL");
  default:
    g_warning("Unkown error %d\n", error);
    return _("Unknown error\n");
  }



}
