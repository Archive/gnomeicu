#include "common.h"
#include "gnomeicu.h"
#include "listwindow.h"
#include "notify.h"
#include "gnomecfg.h"

#include <gtk/gtk.h>
#include <string.h>

static gboolean is_notify (Contact_Member *contact)
{
	return (contact ? contact->online_notify : FALSE);
}


static void notify_add(Contact_Member *contact)
{
  contact->online_notify = TRUE;

  Save_RC();
  gnomeicu_tree_user_update(contact);
}

static void notify_remove(Contact_Member *contact)
{
  contact->online_notify = FALSE;

  Save_RC();
  gnomeicu_tree_user_update(contact);
}


void notify_list_dialog( void )
{
	static GtkWidget *dlg = NULL;
	
	if( dlg == NULL )
	{
		dlg = list_window_new_filter(
			_("Online Notify List"),
			_("Drag the users you wish to add to your\n"
			  "notify list into this window"),
			notify_add,
			notify_remove,
			is_notify);
		if (!dlg)
		  return;
		g_object_add_weak_pointer (G_OBJECT (dlg), (gpointer *)&dlg);
	} else {
		gtk_window_present (GTK_WINDOW (dlg));
	}
}
