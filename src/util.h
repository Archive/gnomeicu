/***********************************
 Various utility functions
 (c) 1999 Jeremy Wise
 GnomeICU
************************************/

#ifndef __UTIL_H__
#define __UTIL_H__

#include "datatype.h"
#include <glade/glade-xml.h>

typedef enum {
	TIMEZONE,
	COUNTRY,
	INTEREST,
	LANGUAGE,
	PAST,
	WORK,
	AFFILIATION
} FIELD_T;

#ifdef TRACE_FUNCTION
void ICUTrace(const gchar *format, ...);
#else
#define ICUTrace(...)
#endif

GList *get_codes( FIELD_T field );
gchar *get_name( FIELD_T field, WORD code );
WORD get_code( FIELD_T field, const gchar *name );

#define get_country_codes()	get_codes(COUNTRY)
#define get_timezone_codes()	get_codes(TIMEZONE)
#define get_interest_codes()	get_codes(INTEREST)
#define get_language_codes()	get_codes(LANGUAGE)
#define get_past_codes()	get_codes(PAST)
#define get_work_codes()	get_codes(WORK)
#define get_affiliation_codes()	get_codes(AFFILIATION)

#define get_country_name(x)	get_name(COUNTRY, x)
#define get_timezone_name(x)	get_name(TIMEZONE, x & 0xff)
#define get_interest_name(x)	get_name(INTEREST, x)
#define get_language_name(x)	get_name(LANGUAGE, x & 0xff)
#define get_past_name(x)	get_name(PAST, x)
#define get_work_name(x)	get_name(WORK, x)
#define get_affiliation_name(x)	get_name(AFFILIATION, x)

#define get_country_code(x)	get_code(COUNTRY, x)
#define get_timezone_code(x)	(BYTE) get_code(TIMEZONE, x)
#define get_interest_code(x)	get_code(INTEREST, x)
#define get_language_code(x)	(BYTE) get_code(LANGUAGE, x)
#define get_past_code(x)	get_code(PAST, x)
#define get_work_code(x)	get_code(WORK, x)
#define get_affiliation_code(x)	get_code(AFFILIATION, x)

gchar *convert_to_utf8(const gchar *text);
gchar *convert_from_utf8(const gchar *text);

const gchar *get_status_str( DWORD status );
DWORD Chars_2_DW( const guchar buf[] );
WORD Chars_2_Word( const guchar buf[] );
void DW_2_Chars( guchar buf[], DWORD num );
void Word_2_Chars( guchar buf[], WORD num );
gchar *strip_cr( const gchar *str );
gchar *strip_html( const gchar *str );
gchar *add_cr( const gchar *text );
GSList * Find_User( UIN_T uin );
GSList * Find_User_uid( WORD uid );
GSList *Add_User( UIN_T uin, const gchar *name, gboolean inlist );

WORD CharsBE_2_Word( const guchar *buf );
void Word_2_CharsBE( guchar buf[], WORD num );
DWORD CharsBE_2_DW( const guchar buf[] );
void DW_2_CharsBE( guchar buf[], DWORD num );

#define Chars_2_Byte(x) (*(x))


GladeXML *gicu_util_open_glade_xml (const gchar *glade_file,
                                    const gchar *root_name);

#endif /* __UTIL_H__ */
