/*****************************
 Dump a packet to the screen
 (c) 1999 Jeremy Wise
 GnomeICU
******************************/

#ifndef __PACKETPRINT_H__
#define __PACKETPRINT_H__

#include "datatype.h"

void packet_print( BYTE *packet, int size, int type, gchar *charid );

#endif /* __PACKETPRINT_H__ */
