/* GnomeICU
 * Copyright (C) 1998-2003 Jeremy Wise
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/**** Provide custom preferences UI ****/

/* Copyright (C) 2002-2003 Patrick Sung */


#include "common.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>

#include <gtk/gtk.h>

#include "prefs_ui.h"

void combo_lang_set_widget (const GnomeICUPrefData *pd);
void combo_lang_connect_signal (GnomeICUPrefData *pd);

void themes_treeview_connect_signal (GnomeICUPrefData *pd);
void icon_treeview_set_widget (const GnomeICUPrefData *pd);
void emoticon_treeview_set_widget (const GnomeICUPrefData *pd);

void program_event_init_value (GnomeICUPrefData *pd);
void program_event_set_widget (const GnomeICUPrefData *pd);
void program_event_connect_signal (GnomeICUPrefData *pd);

void away_message_init_value (GnomeICUPrefData *pd);
void message_set_widget (const GnomeICUPrefData *pd);
void message_connect_signal (GnomeICUPrefData *pd);

prefs_action lang_combo_box = {
  0, /* use default function (string type pref) */
  combo_lang_set_widget,
  combo_lang_connect_signal
};

prefs_action icon_theme_treeview = {
  0,
  icon_treeview_set_widget,
  themes_treeview_connect_signal
};

prefs_action emoticon_theme_treeview = {
  0,
  emoticon_treeview_set_widget,
  themes_treeview_connect_signal
};

prefs_action program_event_treeview = {
  program_event_init_value,
  program_event_set_widget,
  program_event_connect_signal
};

prefs_action away_message_widgets = {
  away_message_init_value,
  message_set_widget,
  message_connect_signal
};

/*******************************************************************************
 * character set combo box
 *******************************************************************************/
static gchar* possible_charsets[] = { 
  "ISO-8859-1",
  "ISO-8859-2",
  "ISO-8859-3",
  "ISO-8859-4",
  "ISO-8859-5",
  "ISO-8859-6",
  "ISO-8859-7",
  "ISO-8859-8",
  "ISO-8859-9",
  "ISO-8859-10",
  "CP1250",
  "CP1251",
  "SJIS",
  "BIG-5",
  "GB2312",
  NULL
};

void
combo_lang_set_widget (const GnomeICUPrefData *pd)
{
  GtkCombo *combo = GTK_COMBO (pd->widget);
  GList *verified_charsets = NULL;
  GIConv converter = (GIConv) -1;
  int i;
  gchar *value;
  

  value = g_strdup(pd->value);

  /* Populate list */
  for (i = 0; possible_charsets[i]; i++) {

    converter = g_iconv_open("UTF-8", possible_charsets[i]);

    if (converter == (GIConv) -1)
      continue;

    g_iconv_close(converter);

    converter = g_iconv_open(possible_charsets[i], "UTF-8");

    if (converter == (GIConv) -1)
      continue;

    g_iconv_close(converter);

    verified_charsets = g_list_append(verified_charsets, possible_charsets[i]);
  }

  gtk_combo_set_popdown_strings(combo, verified_charsets);

  gtk_entry_set_text (GTK_ENTRY (combo->entry), value);

  g_free(value);
  g_list_free (verified_charsets);
}

/* will be called when the entry receive "changed", not the combo box */
gboolean
combo_lang_changed_cb (GtkWidget *entry, GnomeICUPrefData *pd)
{
  const gchar *text;
  GIConv converter = (GIConv) -1;
  GIConv converter2 = (GIConv) -1;

  text = gtk_entry_get_text (GTK_ENTRY(entry));

  if (*text == 0)
    return FALSE;

  /* Lets check if its right... */

  converter = g_iconv_open("UTF-8", text);
  converter2 = g_iconv_open(text, "UTF-8");

  /* We save it only if its right */
  if (converter != (GIConv) -1 && converter2 != (GIConv) -1) {
    g_free (pd->value);
    pd->value = g_strdup (text);
    preferences_set_string (pd->key, pd->value);
  }

  if (converter != (GIConv) -1)
    g_iconv_close(converter);
  if (converter2 != (GIConv) -1)
    g_iconv_close(converter2);

  return FALSE;
}

gboolean
combo_lang_out_cb (GtkWidget *entry,  GdkEventFocus *event, GnomeICUPrefData *pd)
{
  const gchar *text;
  GIConv converter = (GIConv) -1;
  GIConv converter2 = (GIConv) -1;

  text = gtk_entry_get_text (GTK_ENTRY(entry));
  
  if (*text == 0)
    return FALSE;

  converter = g_iconv_open("UTF-8", text);
  converter2 = g_iconv_open(text, "UTF-8");

  /* We save if its right, otherwise we reload the default from GConf */
  if (converter != (GIConv) -1 && converter2 != (GIConv) -1) {
    g_free (pd->value);
    pd->value = g_strdup (text);
    preferences_set_string (pd->key, pd->value);
  } else {
    GtkWidget *error;

    error = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL, 
				   GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
				   _("%s is not a valid character set for "
				     "conversion to and from UTF-8 so it "
				     "was reset with your previous setting"), 
				   text);
    
    gtk_entry_set_text(GTK_ENTRY(entry), preferences_get_string (pd->key));

    /* gtk_dialog_run (GTK_DIALOG (error));
       gtk_widget_destroy (error); */
    g_signal_connect_swapped (G_OBJECT (error), "response",
			      G_CALLBACK (gtk_widget_destroy),
			      G_OBJECT (error));
    gtk_widget_show(error);
  }

  if (converter != (GIConv) -1)
    g_iconv_close(converter);
  if (converter2 != (GIConv) -1)
    g_iconv_close(converter2);

  return FALSE;
}


void
combo_lang_connect_signal (GnomeICUPrefData *pd)
{
  GtkCombo *combo = GTK_COMBO (pd->widget);

  g_signal_connect (G_OBJECT (combo->entry), "changed",
                    G_CALLBACK (combo_lang_changed_cb), (gpointer)pd);
  g_signal_connect (G_OBJECT (combo->entry), "focus-out-event",
                    G_CALLBACK (combo_lang_out_cb), (gpointer)pd);
}

/******************************************************************************
 * Icon/emoticon themes
 ******************************************************************************/

static GtkListStore *
themes_init_treeview (GtkTreeView *treeview)
{
  GtkListStore *store;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;

  store = gtk_list_store_new (1, G_TYPE_STRING);
  gtk_tree_view_set_model (treeview, GTK_TREE_MODEL (store));

  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes (_("Theme"), renderer,
                                                     "text", 0,
                                                     NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);

  return store;
}

static GList *
themes_fill_list (GList *olist, const gchar *themes_path)
{
  DIR *theme_dir;
  struct dirent *dir;
  struct stat ent_sbuf;
  GList *theme_list = NULL;

  theme_list = olist;

  if ((theme_dir = opendir (themes_path)) == NULL) /* No such dir */
    return theme_list;

  while ((dir = readdir (theme_dir)) != NULL) {	/* Dir found */
    if (dir->d_ino > 0 && strcmp ("Default", dir->d_name)) {
      gchar *name;
      gchar *path;

      name = dir->d_name;
      path = g_strconcat (themes_path, "/", name, NULL);

      if (stat (path, &ent_sbuf) >= 0 && S_ISDIR (ent_sbuf.st_mode)) {
        if (name[0] != '.' && strcmp(name, "Always")) /* skip Always emoticon*/
          theme_list = g_list_insert (theme_list, g_strdup (name), -1);
      }
      g_free (path);
    }
  }
  closedir (theme_dir);

  return theme_list;
}

static void
themes_reload_treeview (GtkTreeView *treeview, const gchar *which_theme)
{
  GtkListStore *store;
  GtkTreeIter iter;
  gchar *themes_path;
  GList *list, *themes_list = NULL;

  if ( (store = (GtkListStore *)gtk_tree_view_get_model (treeview)) == NULL )
    store = themes_init_treeview (treeview);

  gtk_list_store_clear (store);

  if (strcmp (which_theme, "emoticons") == 0) {
    gtk_list_store_append (store, &iter);
    gtk_list_store_set (store, &iter, 0, "None", -1);
  }

  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter, 0, "Default", -1);

  /* load global themes name */
  themes_path = g_strconcat (GNOMEICU_DATADIR "/gnomeicu/", which_theme, NULL);
  themes_list = themes_fill_list (themes_list, themes_path);
  g_free (themes_path);

  /* load local themes name in ~/.icq/which_theme */
  themes_path = g_strconcat (g_get_home_dir (), "/.icq/", which_theme, NULL);
  themes_list = themes_fill_list (themes_list, themes_path);
  g_free (themes_path);

  themes_list = g_list_sort (themes_list, (GCompareFunc)g_ascii_strcasecmp);

  for (list = themes_list; list != NULL; list = g_list_next (list)) {
    gtk_list_store_append (store, &iter);
    gtk_list_store_set (store, &iter, 0, list->data, -1);

    g_free (list->data);
  }

  g_list_free (themes_list);
}

void
themes_highlight (GtkTreeView *treeview, const gchar *theme_name)
{
  GtkTreeIter iter;
  GtkListStore *store;
  gchar *string;

  store = (GtkListStore *)gtk_tree_view_get_model (treeview);
  gtk_tree_model_get_iter_first (GTK_TREE_MODEL (store), &iter);

  do {
    gtk_tree_model_get (GTK_TREE_MODEL (store), &iter, 0, &string, -1);
    if (strcmp (theme_name, string) == 0) {
      GtkTreePath *path;

      path = gtk_tree_model_get_path (GTK_TREE_MODEL (store), &iter);
      if (path) {
        gtk_tree_view_set_cursor (treeview, path, NULL, FALSE);
        gtk_tree_path_free (path);
      }

      g_free (string);
      break;
    }
    g_free (string);
  } while (gtk_tree_model_iter_next (GTK_TREE_MODEL (store), &iter));
}

void
themes_treeview_change_cb (GtkTreeSelection *sel, GnomeICUPrefData *pd)
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  gchar *string;

  if (gtk_tree_selection_get_selected (sel, &model, &iter)) {
    gtk_tree_model_get (model, &iter, 0, &string, -1);

    preferences_set_string (pd->key, string);
    g_free (pd->value);
    pd->value = (gpointer)g_strdup (string);

    g_free (string);
  }
}

void
themes_treeview_connect_signal (GnomeICUPrefData *pd)
{
  GtkTreeSelection *select;

  select = gtk_tree_view_get_selection (GTK_TREE_VIEW (pd->widget));

  g_signal_connect (G_OBJECT (select), "changed",
                    G_CALLBACK (themes_treeview_change_cb),
                    pd);
}

void
icon_treeview_set_widget (const GnomeICUPrefData *pd)
{
  themes_reload_treeview (GTK_TREE_VIEW (pd->widget), "icons");
  themes_highlight (GTK_TREE_VIEW (pd->widget), pd->value);
}

void
emoticon_treeview_set_widget (const GnomeICUPrefData *pd)
{
  themes_reload_treeview (GTK_TREE_VIEW (pd->widget), "emoticons");
  themes_highlight (GTK_TREE_VIEW (pd->widget), pd->value);
}

/******************************************************************************
 * Program events treeview
 ******************************************************************************/

struct _program_event_values {
  gchar *key;
  gchar *event_desc;
};

static struct _program_event_values program_events[] = {
  { PREFS_GNOMEICU_PROG_EVENT_RECV_MSG, N_("Receive message") },
  { PREFS_GNOMEICU_PROG_EVENT_RECV_URL, N_("Receive URL") },
  { PREFS_GNOMEICU_PROG_EVENT_FILE_REQ, N_("File request") },
  { PREFS_GNOMEICU_PROG_EVENT_USER_ONLINE, N_("User online") },
  { PREFS_GNOMEICU_PROG_EVENT_USER_OFFLINE, N_("User offline") },
  { PREFS_GNOMEICU_PROG_EVENT_AUTH_USER, N_("Authorized/Rejected by user") },
  { PREFS_GNOMEICU_PROG_EVENT_AUTH_REQ, N_("Authorization request") },
  { PREFS_GNOMEICU_PROG_EVENT_ADDED_TO_USER, N_("Added to user's list") },
  { PREFS_GNOMEICU_PROG_EVENT_CONTACT_LIST, N_("Contact list received") },
  { NULL, NULL }
};

/*
 * - Load multiple key values into struct
 * - initialize the tree view columns/renderer
 */
void
program_event_init_value (GnomeICUPrefData *pd)
{
  GtkTreeView *treeview;
  GtkListStore *store;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;

  treeview = GTK_TREE_VIEW (pd->widget);

  /* 3 data in each store record:
   *  0: Event description
   *  1: command to execute
   *  2: gconf key
   */
  store = gtk_list_store_new (3, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
  gtk_tree_view_set_model (treeview, GTK_TREE_MODEL (store));

  /* setup 2 columns */
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes (_("Event"),
                                                     renderer,
                                                     "text", 0,
                                                     NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);

  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes (_("Program to Execute"),
                                                     renderer,
                                                     "text", 1,
                                                     NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);

  /* set 2nd column as editable */
  g_object_set (G_OBJECT (renderer), "editable", TRUE, NULL);
}

void
program_event_set_widget (const GnomeICUPrefData *pd)
{
  GtkListStore *store;
  GtkTreeIter iter;
  gchar *cmd_line;
  gint i;

  store = (GtkListStore *)gtk_tree_view_get_model (GTK_TREE_VIEW (pd->widget));

  gtk_list_store_clear (store);

  /* load struct to treeview list store */
  for (i = 0; program_events[i].key != NULL; i++) {
    cmd_line = preferences_get_string (program_events[i].key);
 
    if (cmd_line == NULL)
      continue;

    gtk_list_store_append (store, &iter);
    gtk_list_store_set (store, &iter, 0, _(program_events[i].event_desc),
                        1, cmd_line, 2, program_events[i].key, -1);
  }
}

void
cell_edited (GtkCellRendererText *cell, gchar *path,
             gchar *cmd_line, GnomeICUPrefData *pd)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  gchar *key;

  model = gtk_tree_view_get_model (GTK_TREE_VIEW (pd->widget));

  gtk_tree_model_get_iter_from_string (model, &iter, path);

  gtk_list_store_set (GTK_LIST_STORE (model), &iter, 1, cmd_line, -1);
  gtk_tree_model_get (model, &iter, 2, &key, -1);

  preferences_set_string (key, cmd_line);

  g_free (key);
}

void
program_event_connect_signal (GnomeICUPrefData *pd)
{
  GtkTreeViewColumn *column;
  GList *list;

  column = gtk_tree_view_get_column (GTK_TREE_VIEW (pd->widget), 1);
  list = gtk_tree_view_column_get_cell_renderers (column);

  g_signal_connect (G_OBJECT (list->data), "edited",
                    G_CALLBACK (cell_edited), pd);

  g_list_free (list);
}

/******************************************************************************
 * Away and Not Available Message
 ******************************************************************************/

struct _msg_widgets {
  gint current_choice;
  gchar *msg_key;
  gchar *default_key;
  gchar *msg_head_key;
  GtkWidget *combo;
  GtkWidget *button;
  GtkWidget *textview;
};

void
away_message_init_value (GnomeICUPrefData *pd)
{
  struct _msg_widgets *mw;
  GladeXML *xml;

  xml = (GladeXML *)pd->value;
  mw = g_new (struct _msg_widgets, 1);

  mw->current_choice = 1;
  mw->msg_key = PREFS_GNOMEICU_AWAY_MSG;
  mw->default_key = PREFS_GNOMEICU_AWAY_DEFAULT;
  mw->msg_head_key = PREFS_GNOMEICU_AWAY_MSG_HEADINGS;

  mw->combo = pd->widget;
  mw->button = glade_xml_get_widget (xml, "gicu_away_default_button");
  mw->textview = glade_xml_get_widget (xml, "gicu_away_msg_textview");

  pd->value = mw;

  gtk_combo_disable_activate (GTK_COMBO (pd->widget));
}

static void
show_message (const GnomeICUPrefData *pd, gint i)
{
  struct _msg_widgets *mw;
  GtkTextBuffer *buffer;
  gchar *string;
  gchar *key;
  gint defmsg;

  mw = (struct _msg_widgets *)(pd->value);

  mw->current_choice = i;

  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (mw->textview));

  key = g_strdup_printf ("%s%d", mw->msg_key, i);
  string = preferences_get_string (key);

  gtk_text_buffer_set_text (buffer, string, -1);

  g_free (string);
  g_free (key);

  /* check if away message 'i' is the default */
  defmsg = preferences_get_int (mw->default_key);
  gtk_widget_set_sensitive(mw->button, !(defmsg == i));
}

void
message_set_widget (const GnomeICUPrefData *pd)
{
  struct _msg_widgets *mw;
  GSList *list, *tmp;
  GList *combo_list = NULL;

  mw = (struct _msg_widgets *)(pd->value);

  /* populate the combo list */
  list = preferences_get_list (mw->msg_head_key, GCONF_VALUE_STRING);
  tmp = list;
  while (tmp != NULL) {
    combo_list = g_list_append (combo_list, tmp->data);
    tmp = tmp->next;
  }

  gtk_combo_set_popdown_strings (GTK_COMBO (pd->widget), combo_list);

  /* free memory */
  g_list_free (combo_list);
  tmp = list;
  while (tmp != NULL) {
    g_free (tmp->data);
    tmp = tmp->next;
  }
  g_slist_free (list);

  /* show away message 1 */
  show_message (pd, 1);
}

static void
switch_message_cb (GtkWidget *ed, GnomeICUPrefData *pd)
{
  struct _msg_widgets *mw;
  const gchar *string;
  GSList *list, *tmp;
  gint i = 1;
  GtkWidget *menu, *item, *label;
  GList *items;

  mw = (struct _msg_widgets *)(pd->value);

  string = gtk_entry_get_text (GTK_ENTRY (ed));

  list = preferences_get_list (mw->msg_head_key, GCONF_VALUE_STRING);
  tmp = list;
  while (tmp != NULL) {
    if (strcmp (string, tmp->data) == 0) {
      show_message (pd, i);
      break;
    }
    i++;
    tmp = tmp->next;
  }

  if (tmp == NULL && string[0] != '\0') {

    /* update gconf */

    tmp = g_slist_nth(list, mw->current_choice-1);

    g_free(tmp->data);
    tmp->data = g_strdup(string);
    preferences_set_list (mw->msg_head_key, GCONF_VALUE_STRING, list);
    
    /* update list widget */
    
    menu = g_object_get_data(G_OBJECT(ed), "menu");
    items = gtk_container_get_children(GTK_CONTAINER(menu));
    item = GTK_WIDGET(g_list_nth_data(items, mw->current_choice-1));
    label = gtk_bin_get_child(GTK_BIN(item));

    gtk_label_set_text(GTK_LABEL(label), string);
    
  }

  tmp = list;
  while (tmp != NULL) {
    g_free (tmp->data);
    tmp = tmp->next;
  }
  g_slist_free (list);
}

static void
textview_changed_cb (GtkWidget *w, GnomeICUPrefData *pd)
{
  struct _msg_widgets *mw;
  GtkTextBuffer *buffer;
  GtkTextIter start_iter, end_iter;
  gchar *str, *key;

  mw = (struct _msg_widgets *)(pd->value);

  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW(mw->textview));

  gtk_text_buffer_get_start_iter (buffer, &start_iter);
  gtk_text_buffer_get_end_iter (buffer, &end_iter);

  str = gtk_text_buffer_get_text (buffer, &start_iter, &end_iter, FALSE);

  key = g_strdup_printf ("%s%d", mw->msg_key, mw->current_choice);
  preferences_set_string (key, str);

  g_free (key);
  g_free (str);
}

static void
set_default_cb (GtkWidget *b, GnomeICUPrefData *pd)
{
  preferences_set_int (((struct _msg_widgets *)(pd->value))->default_key,
                       ((struct _msg_widgets *)(pd->value))->current_choice);
  gtk_widget_set_sensitive(((struct _msg_widgets *)(pd->value))->button, FALSE);
}

void
message_connect_signal (GnomeICUPrefData *pd)
{
  GtkCombo *combo;
  GtkTextBuffer *buffer;

  combo = GTK_COMBO (pd->widget);
  g_signal_connect (G_OBJECT (combo->entry), "changed",
                    G_CALLBACK (switch_message_cb), pd);
  g_object_set_data(G_OBJECT(combo->entry), "menu", combo->list);

  g_signal_connect (G_OBJECT (((struct _msg_widgets *)(pd->value))->button),
                    "clicked", G_CALLBACK (set_default_cb), pd);

  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW(((struct _msg_widgets *)(pd->value))->textview));
  g_signal_connect (G_OBJECT (buffer), "changed",
                    G_CALLBACK (textview_changed_cb), pd);
}
